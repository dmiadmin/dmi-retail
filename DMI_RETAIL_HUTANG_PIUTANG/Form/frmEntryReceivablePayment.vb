﻿Public Class frmReceivablePayment
    Dim tmpDataGridViewLoaded, tmpChange As Boolean
    Dim tmpPositionCustomer, tmpReceipt, tmpGrandTotal As String
    Dim tmpMaturity As Date
    Dim tmpTotal, tmpBalance As Integer
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    Private Sub frmReceivablePayment_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If tmpChange = True Then
            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah Anda ingin menyimpan perubahan " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If
            If tmpvar = 6 Then
                cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        End If
    End Sub


    Private Sub frmReceivablePayment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_PAYMENT_TRANSACTION.PAYMENT_METHOD' table. You can move, or remove it, as needed.
        Me.PAYMENT_METHODTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.PAYMENT_METHOD)
        'TODO: This line of code loads data into the 'DS_PAYMENT_TRANSACTION.RECEIVABLE_PAYMENT' table. You can move, or remove it, as needed.
        Me.RECEIVABLE_PAYMENTTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.RECEIVABLE_PAYMENT)
        'TODO: This line of code loads data into the 'DS_PAYMENT_TRANSACTION.PAYMENT_METHOD' table. You can move, or remove it, as needed.

        'Me.SP_RECEIVABLE_PAYMENT_DETAILTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_RECEIVABLE_PAYMENT_DETAIL, _
        '                                                   SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("RECEIPT_ID"))

        ''Me.SP_RECEIVABLE_PAYMENT_HEADERTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.'SP_RECEIVABLE_PAYMENT_HEADER, "")

        ''tmpDataGridViewLoaded = True

        accFormName = Me.Text

        If Language = "Indonesian" Then
            lblCustomer.Text = "Nama Customer :"
            lblReceiptNo.Text = "No Faktur :"
            lblMarturity.Text = "Jatuh Tempo :"
            lblPayMethod.Text = "Cara Pembayaran :"
            lblPayAmount.Text = "Jumlah Pembayaran :"
            lblPayDate.Text = "Tgl Pembayaran :"
            lblBalance.Text = "Saldo :"
            lblDescription.Text = "Keterangan :"
            cmdAdd.Text = "Tambah"
            cmdEdit.Text = "Ubah"
            cmdUndo.Text = "Batal"
            cmdSave.Text = "Simpan"
            cmdDelete.Text = "Hapus"
            Me.Text = "Input Pembayaran Piutang"
        End If

        DisableInputBox(Me)

        SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.Enabled = True

        GroupBox1.Enabled = True

        PAYMENT_METHOD_IDComboBox.DataSource = SPRECEIVABLEPAYMENTHEADERSPRECEIVABLEPAYMENTDETAILBindingSource
        PAYMENT_METHOD_IDComboBox.DisplayMember = "PAYMENT_METHOD_ID"


        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = False
        cmdSave.Enabled = False

        If RECEIPT_NOTextBox.Text <> "" Then
            cmdDelete.Enabled = True
        Else
            cmdDelete.Enabled = False
        End If

        If PAYMENT_METHODBindingSource Is Nothing Or PAYMENT_METHODBindingSource.Count < 1 Then
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data Metode Pembayaran." & vbCrLf & _
                  "Masukkan setidaknya satu data Metode Pembayaran!", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Payment Method data." & vbCrLf & _
                  "Please input at least one Payment Method data!", MsgBoxStyle.Critical, "DMI Retail")
            End If
        End If

        tmpChange = False
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        DisableInputBox(Me)

        If RECEIPT_NOTextBox.Text <> "" Then
            SP_RECEIVABLE_PAYMENT_DETAILTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_RECEIVABLE_PAYMENT_DETAIL, _
                                                          SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("RECEIPT_ID"))

            tmpBalance = 0
            tmpTotal = 0
            'If PAYMENT_AMOUNTTextBox.Text <> "" Then
            '    For x As Integer = 1 To SP_RECEIVABLE_PAYMENT_DETAILBindingSource.Count
            '        SP_RECEIVABLE_PAYMENT_DETAILBindingSource.Position = x
            '        tmpTotal = tmpTotal + SP_RECEIVABLE_PAYMENT_DETAILBindingSource.Current("PAYMENT_AMOUNT")
            '    Next

            '    tmpBalance = CInt(SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("TOTAL_RECEIVABLE")) - tmpTotal
            '    BALANCETextBox.Text = tmpBalance
            'Else
            '    BALANCETextBox.Text = SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("TOTAL_RECEIVABLE")
            'End If
            BALANCETextBox.Text = FormatNumber(SP_RECEIVABLE_PAYMENT_DETAILTableAdapter.SP_RECEIVABLE_PAYMENT_GET_BALANCE(SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("RECEIVABLE_ID")), 0)

        Else

            SP_RECEIVABLE_PAYMENT_DETAILTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_RECEIVABLE_PAYMENT_DETAIL, 0)

            BALANCETextBox.Text = ""

        End If

            PAYMENT_METHOD_IDComboBox.Text = ""

            SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.Enabled = True

            cmdAdd.Enabled = True
            cmdEdit.Enabled = False
            cmdUndo.Enabled = False
            cmdSave.Enabled = False
            cmdDelete.Enabled = True

            tmpChange = False
    End Sub

    Private Sub cmdSearchCustomer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchCustomer.Click
        mdlGeneral.tmpSearchMode = "Customer - Customer Name"
        frmSearchReceivableInvoice.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        'SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Position = SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Find("RECEIPT_ID", tmpSearchResult)
        SP_RECEIVABLE_PAYMENT_HEADERTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_RECEIVABLE_PAYMENT_HEADER, tmpSearchResult)
        dtpMATURITY_DATE.Value = SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("MATURITY_DATE")
        cmdAdd.Enabled = True
        tmpChange = False
    End Sub


    'Private Sub dgvReceivablePayment_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
    '    Dim tmpTotal As Integer
    '    If tmpDataGridViewLoaded = True Then

    '        If tmpFillingDGV = False Then

    '            For x As Integer = 0 To dgvReceivablePayment.RowCount - 1
    '                tmpTotal = tmpTotal + CInt(dgvReceivablePayment.Item("PAYMENT_AMOUNT", x).Value)
    '            Next
    '            BALANCETextBox.Text = CInt(TOTAL_RECEIVABLETextBox.Text) - tmpTotal

    '            tmpFillingDGV = True
    '        End If
    '    End If
    'End Sub


    Private Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        'For N As Integer = 0 To dgvReceivablePayment.RowCount - 1
        '    RECEIVABLE_PAYMENTTableAdapter.SP_RECEIVABLE_PAYMENT("I", _
        '                                                          0, _
        '                                                          RECEIVABLE_PAYMENTBindingSource.Current("RECEIVABLE_ID"), _
        '                                                          dgvReceivablePayment.Item("PAYMENT_DATE", N).Value, _
        '                                                          dgvReceivablePayment.Item("PAYMENT_AMOUNT", N).Value, _
        '                                                          dgvReceivablePayment.Item("PAYMENT_METHOD_NAME", N).Value, _
        '                                                          mdlGeneral.USER_ID, _
        '                                                          Now, _
        '                                                          0, _
        '                                                          DateSerial(4000, 12, 31))
        'Next

        Dim TMPDATE As Date
        TMPDATE = DateSerial(PAYMENT_DATEDateTimePicker.Value.Year, PAYMENT_DATEDateTimePicker.Value.Month, PAYMENT_DATEDateTimePicker.Value.Day)

        'If CUSTOMER_NAMETextBox.Text = "" Then
        '    If Language = "Indonesian" Then
        '        MsgBox("Tolong isi Nama Customer !", MsgBoxStyle.Critical, "DMI Retail")
        '    Else
        '        MsgBox("Please fill Customer Name !", MsgBoxStyle.Critical, "DMI Retail")
        '    End If
        '    Exit Sub
        If RECEIPT_NOTextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Tolong isi No Faktur !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please fill Receipt No !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        ElseIf PAYMENT_AMOUNTTextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Tolong isi Jumlah Pembayaran !" & vbCrLf & _
                      "atau tekan Batal jika anda ingin membatalkan pembayaran ini !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please fill Paymount Amount !" & vbCrLf & _
                      "Or click Undo if you want to cancel this payment !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        ElseIf PAYMENT_METHOD_IDComboBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Tolong isi Cara Pembayaran !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please fill Payment Method !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        ElseIf CInt(PAYMENT_AMOUNTTextBox.Text) > CInt(BALANCETextBox.Text) Then
            If Language = "Indonesian" Then
                MsgBox("Pembayaran lebih besar dari Saldo Piutang !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Payment is bigger than the Balance !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        Try
            tmpBalance = CInt(BALANCETextBox.Text) - CInt(PAYMENT_AMOUNTTextBox.Text)
        Catch
        End Try

        RECEIVABLE_PAYMENTTableAdapter.SP_RECEIVABLE_PAYMENT("I", _
                                                             0, _
                                                             txtPaymentNo.Text, _
                                                             SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("RECEIVABLE_ID"), _
                                                             PAYMENT_DATEDateTimePicker.Value, _
                                                             CInt(PAYMENT_AMOUNTTextBox.Text), _
                                                             PAYMENT_METHODBindingSource.Current("PAYMENT_METHOD_ID"), _
                                                             DESCRIPTIONTextBox.Text, _
                                                             tmpBalance, _
                                                             mdlGeneral.USER_ID, _
                                                             Now, _
                                                             0, _
                                                             DateSerial(4000, 12, 31))


        tmpPositionCustomer = CUSTOMER_NAMETextBox.Text
        tmpReceipt = RECEIPT_NOTextBox.Text
        tmpGrandTotal = TOTAL_RECEIVABLETextBox.Text
        tmpMaturity = DateSerial(dtpMATURITY_DATE.Value.Year, dtpMATURITY_DATE.Value.Month, dtpMATURITY_DATE.Value.Day)

        If Language = "Indonesian" Then
            MsgBox("Data berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data Succesfully Saved.", MsgBoxStyle.Information, "DMI Retail")
        End If

        Me.PAYMENT_METHODTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.PAYMENT_METHOD)
        Me.RECEIVABLE_PAYMENTTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.RECEIVABLE_PAYMENT)
        Me.SP_RECEIVABLE_PAYMENT_DETAILTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_RECEIVABLE_PAYMENT_DETAIL, _
                                                         SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("RECEIPT_ID"))

        CUSTOMER_NAMETextBox.Text = tmpPositionCustomer
        RECEIPT_NOTextBox.Text = tmpReceipt
        dtpMATURITY_DATE.Value = tmpMaturity
        TOTAL_RECEIVABLETextBox.Text = tmpGrandTotal


        'If RECEIPT_NOComboBox.Text <> "" Then
        '    SP_RECEIVABLE_PAYMENT_DETAILTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_RECEIVABLE_PAYMENT_DETAIL, _
        '                                                  SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("RECEIPT_ID"))
        '    tmpTotal = 0
        '    tmpBalance = 0
        '    For x As Integer = 1 To SP_RECEIVABLE_PAYMENT_DETAILBindingSource.Count
        '        SP_RECEIVABLE_PAYMENT_DETAILBindingSource.Position = x
        '        tmpTotal = tmpTotal + SP_RECEIVABLE_PAYMENT_DETAILBindingSource.Current("PAYMENT_AMOUNT")
        '    Next

        '    tmpBalance = CInt(SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("TOTAL_RECEIVABLE")) - tmpTotal
        '    BALANCETextBox.Text = tmpBalance

        'Else

        '    BALANCETextBox.Text = ""

        'End If


        GroupBox1.Enabled = True
        'CUSTOMER_NAMEComboBox.Enabled = True
        'RECEIPT_NOComboBox.Enabled = True
        'dtpMATURITY_DATE.Enabled = True
        'TOTAL_RECEIVABLETextBox.Enabled = True

        txtPaymentNo.Enabled = False
        PAYMENT_DATEDateTimePicker.Enabled = False
        PAYMENT_AMOUNTTextBox.Enabled = False
        PAYMENT_METHOD_IDComboBox.Enabled = False
        DESCRIPTIONTextBox.Enabled = False

        SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.Enabled = True
        SP_RECEIVABLE_PAYMENT_DETAILTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_RECEIVABLE_PAYMENT_DETAIL, SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("RECEIPT_ID"))
        SP_RECEIVABLE_PAYMENT_DETAILBindingSource.Position = SP_RECEIVABLE_PAYMENT_DETAILBindingSource.Find("RECEIVABLE_ID", SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("RECEIPT_ID"))
        'frmReceivablePayment_Load(Nothing, Nothing)
        BALANCETextBox.Text = SP_RECEIVABLE_PAYMENT_DETAILTableAdapter.SP_RECEIVABLE_PAYMENT_GET_BALANCE(SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("RECEIVABLE_ID"))

        cmdAdd.Enabled = True
        cmdEdit.Enabled = False
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True

        tmpChange = False
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If Trim(RsAccessPrivilege.Item("ALLOW_ADD").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        EnableInputBox(Me)

        SP_RECEIVABLE_PAYMENT_DETAILBindingSource.AddNew()

        SP_RECEIVABLE_PAYMENT_DETAILTableAdapter.SP_RECEIVABLE_PAYMENT_GENERATE_NUMBER(txtPaymentNo.Text)

        PAYMENT_METHOD_IDComboBox.DataSource = PAYMENT_METHODBindingSource
        PAYMENT_METHOD_IDComboBox.ValueMember = "PAYMENT_METHOD_ID"
        PAYMENT_METHOD_IDComboBox.DisplayMember = "PAYMENT_METHOD_NAME"

        SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.Enabled = False

        If BALANCETextBox.Text = "" Then
            BALANCETextBox.Text = FormatNumber(TOTAL_RECEIVABLETextBox.Text, 0)
        End If

        PAYMENT_DATEDateTimePicker.Value = Now

        'If PAYMENT_AMOUNTTextBox.Text <> "" Then
        '    For x As Integer = 1 To SP_RECEIVABLE_PAYMENT_DETAILBindingSource.Count
        '        SP_RECEIVABLE_PAYMENT_DETAILBindingSource.Position = x
        '        tmpTotal = tmpTotal + CInt(SP_RECEIVABLE_PAYMENT_DETAILBindingSource.Current("PAYMENT_AMOUNT"))
        '    Next

        '    tmpBalance = CInt(SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("TOTAL_RECEIVABLE")) - tmpTotal
        '    BALANCETextBox.Text = tmpBalance
        'Else
        '    BALANCETextBox.Text = SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("TOTAL_RECEIVABLE")
        'End If


        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False

        tmpChange = False
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim tmpCheck As String = ""
        Dim tmpCheckDP As DateTime
        RECEIVABLE_PAYMENTTableAdapter.SP_CHECK_CLOSING_PERIOD(PAYMENT_DATEDateTimePicker.Value, tmpCheck)

        If Trim(RsAccessPrivilege.Item("ALLOW_DELETE").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpCheck = "TRUE" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menghapus transaksi in!" & vbCrLf & "Transaksi ini telah masuk dalam proses Tutup Buku", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Delete this Transaction!" & vbCrLf & "This Transaction has been in Closed Period", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpCheckDP = RECEIVABLE_PAYMENTTableAdapter.SP_CHECK_DOWN_PAYMENT(PAYMENT_DATEDateTimePicker.Value, RECEIVABLE_PAYMENTBindingSource.Current("RECEIVABLE_ID"))
        If tmpCheckDP = PAYMENT_DATEDateTimePicker.Value Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menghapus transaksi ini !" & vbCrLf & "Pembayaran ini adalah DP !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot delete this transaction !" & vbCrLf & "This tansaction is DP", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If SP_RECEIVABLE_PAYMENT_DETAILBindingSource.Count < 1 Then Exit Sub

        If Language = "Indonesian" Then
            If MsgBox("Apakah anda yakin ingin menghapus pembayaran ini ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, _
                      "DMI Retail") = MsgBoxResult.No Then Exit Sub
        Else
            If MsgBox("Are you sure want to delete this payment ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, _
                      "DMI Retail") = MsgBoxResult.No Then Exit Sub
        End If

        RECEIVABLE_PAYMENTTableAdapter.SP_RECEIVABLE_PAYMENT("D", _
                                                             RECEIVABLE_PAYMENTBindingSource.Current("RECEIVABLE_PAYMENT_ID"), _
                                                             RECEIVABLE_PAYMENTBindingSource.Current("RECEIVABLE_PAYMENT_NUMBER"), _
                                                             RECEIVABLE_PAYMENTBindingSource.Current("RECEIVABLE_ID"), _
                                                             RECEIVABLE_PAYMENTBindingSource.Current("PAYMENT_DATE"), _
                                                             RECEIVABLE_PAYMENTBindingSource.Current("PAYMENT_AMOUNT"), _
                                                             RECEIVABLE_PAYMENTBindingSource.Current("PAYMENT_METHOD_ID"), _
                                                             0, _
                                                             0, _
                                                             RECEIVABLE_PAYMENTBindingSource.Current("USER_ID_INPUT"), _
                                                             RECEIVABLE_PAYMENTBindingSource.Current("INPUT_DATE"), _
                                                             RECEIVABLE_PAYMENTBindingSource.Current("USER_ID_UPDATE"), _
                                                             RECEIVABLE_PAYMENTBindingSource.Current("UPDATE_DATE"))

        RECEIVABLE_PAYMENTBindingSource.RemoveCurrent()

        RECEIVABLE_PAYMENTBindingSource.Position = 0

        RECEIVABLE_PAYMENTTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.RECEIVABLE_PAYMENT)

        SP_RECEIVABLE_PAYMENT_DETAILTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_RECEIVABLE_PAYMENT_DETAIL, _
                                                      SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("RECEIPT_ID"))

        tmpChange = False
    End Sub

    'Private Sub CUSTOMER_NAMEComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    'Try
    '    '    CUSTOMERBindingSource.Position = CUSTOMERBindingSource.Find("CUSTOMER_NAME", CUSTOMER_NAMEComboBox.Text)
    '    '    SP_RECEIVABLE_PAYMENT_HEADER1TableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_RECEIVABLE_PAYMENT_HEADER1, CUSTOMERBindingSource.Current("CUSTOMER_ID"))
    '    'Catch

    '    'End Try
    '    'Me.SP_RECEIVABLE_PAYMENT_DETAILTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_RECEIVABLE_PAYMENT_DETAIL, _
    '    '                                                 SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("RECEIPT_ID"))

    '    tmpChange = True
    'End Sub



    'Private Sub RECEIPT_NOComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    'tmpTotal = 0
    'tmpBalance = 0

    'If PAYMENT_AMOUNTTextBox.Text <> "" Then
    '    For x As Integer = 1 To SP_RECEIVABLE_PAYMENT_DETAILBindingSource.Count
    '        SP_RECEIVABLE_PAYMENT_DETAILBindingSource.Position = x
    '        tmpTotal = tmpTotal + SP_RECEIVABLE_PAYMENT_DETAILBindingSource.Current("PAYMENT_AMOUNT")
    '    Next

    '    tmpBalance = CInt(SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("TOTAL_RECEIVABLE")) - tmpTotal
    '    BALANCETextBox.Text = tmpBalance
    'Else
    '    BALANCETextBox.Text = SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("TOTAL_RECEIVABLE")
    'End If

    'End Sub



    Private Sub cmdSearchReceipt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchReceipt.Click
        mdlGeneral.tmpSearchMode = "Customer - Receipt No"
        frmSearchReceivableInvoice.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        'SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Position = SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Find("RECEIPT_ID", tmpSearchResult)
        SP_RECEIVABLE_PAYMENT_HEADERTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_RECEIVABLE_PAYMENT_HEADER, tmpSearchResult)
        dtpMATURITY_DATE.Value = SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("MATURITY_DATE")
        tmpChange = False
        cmdAdd.Enabled = True
    End Sub


    Private Sub PAYMENT_AMOUNTTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PAYMENT_AMOUNTTextBox.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""
        tmpChange = True
    End Sub


    Private Sub PAYMENT_AMOUNTTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PAYMENT_AMOUNTTextBox.TextChanged
        'Try
        '    If SP_RECEIVABLE_PAYMENT_DETAILBindingSource.Count < 1 Then
        '        BALANCETextBox.Text = tmpBalance - CInt(PAYMENT_AMOUNTTextBox.Text)
        '    Else
        '        tmpBalance = CInt(SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("TOTAL_RECEIVABLE")) - CInt(PAYMENT_AMOUNTTextBox.Text)
        '        BALANCETextBox.Text = tmpBalance
        '    End If
        'Catch
        'End Try


        'If TOTAL_RECEIVABLETextBox.Text <> "" Then
        '    tmpTotal = 0
        '    tmpBalance = 0

        '    If PAYMENT_AMOUNTTextBox.Text <> "" Then
        '        tmpBalance = CInt(SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("TOTAL_RECEIVABLE")) - CInt(PAYMENT_AMOUNTTextBox.Text)
        '        BALANCETextBox.Text = tmpBalance
        '    Else
        '        BALANCETextBox.Text = SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("TOTAL_RECEIVABLE")
        '    End If
        'End If

        If PAYMENT_AMOUNTTextBox.Text = "" Then
            PAYMENT_AMOUNTTextBox.Text = "0"
        End If


    End Sub

    Private Sub txtPaymentNo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPaymentNo.KeyDown
        If e.KeyCode = Keys.Delete Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtPaymentNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPaymentNo.KeyPress
        e.KeyChar = ""
    End Sub

    Private Sub SP_RECEIVABLE_PAYMENT_DETAILBindingSource_CurrentChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SP_RECEIVABLE_PAYMENT_DETAILBindingSource.CurrentChanged
        'Try
        '    BALANCETextBox.Text = SP_RECEIVABLE_PAYMENT_DETAILBindingSource.Current("BALANCE")
        'Catch
        'End Try
        'tmpChange = False
    End Sub

    Private Sub RECEIPT_NOTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RECEIPT_NOTextBox.TextChanged
        'Me.SP_RECEIVABLE_PAYMENT_DETAILTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_RECEIVABLE_PAYMENT_DETAIL, _
        '                                                 SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("RECEIPT_ID"))
        'BALANCETextBox.Text = SP_RECEIVABLE_PAYMENT_DETAILTableAdapter.SP_RECEIVABLE_PAYMENT_GET_BALANCE(SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("RECEIVABLE_ID"))

        'If RECEIPT_NOTextBox.Text <> "" Then
        '    cmdDelete.Enabled = True
        'Else
        '    cmdDelete.Enabled = False
        'End If

        'tmpChange = True
    End Sub

    Private Sub SP_RECEIVABLE_PAYMENT_HEADERBindingSource_BindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.BindingCompleteEventArgs) Handles SP_RECEIVABLE_PAYMENT_HEADERBindingSource.BindingComplete
        Try
            Me.SP_RECEIVABLE_PAYMENT_DETAILTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_RECEIVABLE_PAYMENT_DETAIL, _
                                                            SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("RECEIPT_ID"))
            BALANCETextBox.Text = SP_RECEIVABLE_PAYMENT_DETAILTableAdapter.SP_RECEIVABLE_PAYMENT_GET_BALANCE(SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("RECEIVABLE_ID"))
        Catch
        End Try
    End Sub

    Private Sub SP_RECEIVABLE_PAYMENT_HEADERBindingSource_CurrentChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SP_RECEIVABLE_PAYMENT_HEADERBindingSource.CurrentChanged
        Try
            Me.SP_RECEIVABLE_PAYMENT_DETAILTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_RECEIVABLE_PAYMENT_DETAIL, _
                                                            SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("RECEIPT_ID"))
            BALANCETextBox.Text = SP_RECEIVABLE_PAYMENT_DETAILTableAdapter.SP_RECEIVABLE_PAYMENT_GET_BALANCE(SP_RECEIVABLE_PAYMENT_HEADERBindingSource.Current("RECEIVABLE_ID"))
        Catch
        End Try

        If RECEIPT_NOTextBox.Text <> "" Then
            cmdDelete.Enabled = True
        Else
            cmdDelete.Enabled = False
        End If

        tmpChange = True
    End Sub
End Class