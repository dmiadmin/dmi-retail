﻿Public Class frmRepPurchaseMonthly
    Public tmpPeriod1 As Date
    Public tmpPeriod2 As Date
    Private Sub frmRepPurchaseMonthly_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DB_DMI_RETAILDataSetALL.COMPANY_INFORMATION' table. You can move, or remove it, as needed.
        Me.COMPANY_INFORMATIONTableAdapter.Fill(Me.DB_DMI_RETAILDataSetALL.COMPANY_INFORMATION)
        'TODO: This line of code loads data into the 'DB_DMI_RETAILDataSetALL.SP_PURCHASE_REPORT_MONTHLY' table. You can move, or remove it, as needed.
        Me.SP_PURCHASE_REPORT_MONTHLYTableAdapter.Fill(Me.DB_DMI_RETAILDataSetALL.SP_PURCHASE_REPORT_MONTHLY, tmpPeriod1, tmpPeriod2)

        Me.ReportViewer1.RefreshReport()

        Dim newPageSettings As New System.Drawing.Printing.PageSettings
        newPageSettings.Margins = New System.Drawing.Printing.Margins(30, 30, 35, 35)
        newPageSettings.PaperSize = New System.Drawing.Printing.PaperSize("A4", 827, 1169)
        newPageSettings.Landscape = True
        Me.ReportViewer1.SetPageSettings(newPageSettings)

        If Not tmpReport Is Nothing Then tmpReport.Close()
        tmpReport = Me
    End Sub
End Class