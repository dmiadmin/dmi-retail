﻿Public Class frmEntryReturnPurchase
    Dim tmpTotal, tmpStatus As Integer
    Dim tmpProductId, tmpReceiptId As Integer
    Dim tmpDataGridViewLoaded, tmpChange As Boolean
    Dim tmpDescription As String
    Public tmpSavemode, tmpWarehouseName As String
    Public getPurchaseId, getQuantity, getPurchaseReturnId, getNoOfItem As Integer
    Dim tmpPRAccount, tmpPaymentGroup, tmpPayableNo, tmpReceivableNo As String
    Dim tmpPRPrice, tmpPRAmount, tmpPRBalance, tmpPRAccountID, tmpPaymentMethodID As Integer
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    Private Sub cmdSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        Dim popSearch As New frmSearchPurchaseReturn
        dgvPurchaseReturn.Rows.Clear()
        popSearch.ShowDialog()

        txtVendor.Text = popSearch.tmpVendorName
        txtReceiptNo.Text = popSearch.tmpReceiptNo
        txtDate.Text = Format(popSearch.tmpPurchaseDate, "dd-MMM-yyyy")
        txtPaymentMethod.Text = popSearch.tmpPaymentMethhod
        tmpReceiptId = popSearch.tmpPurchaseID
        If popSearch.dgvSearcReturnPurchase.RowCount <> 0 Then
            tmpPaymentMethodID = popSearch.SP_SEARCH_PURCHASE_RETURNBindingSource.Current("PAYMENT_METHOD_ID")
        End If

        'PURCHASE_RETURNTableAdapter.SP_GENERATE_DESCRIPTION_PURCHASE_RETURN(popSearch.tmpPurchaseID, tmpDescription)


        For x As Integer = 0 To popSearch.tmpTotal - 1
            Me.SP_PURCHASE_RETURN_DETAIL_LISTTableAdapter.Fill(Me.DS_RETURN_PURCHASE.SP_PURCHASE_RETURN_DETAIL_LIST, popSearch.tmpPurchaseID)
            dgvPurchaseReturn.Rows.Add()
            SP_PURCHASE_RETURN_DETAIL_LISTBindingSource.Position = x
            dgvPurchaseReturn.Item("PRODUCT_CODE", x).Value = SP_PURCHASE_RETURN_DETAIL_LISTBindingSource.Current("PRODUCT_CODE")
            dgvPurchaseReturn.Item("PRODUCT_NAME", x).Value = SP_PURCHASE_RETURN_DETAIL_LISTBindingSource.Current("PRODUCT_NAME")
            dgvPurchaseReturn.Item("RETURNED_QUANTITY", x).Value = SP_PURCHASE_RETURN_DETAIL_LISTBindingSource.Current("RETURNED_QUANTITY")
            dgvPurchaseReturn.Item("QUANTITY", x).Value = SP_PURCHASE_RETURN_DETAIL_LISTBindingSource.Current("QUANTITY")

        Next
        tmpChange = False
    End Sub

    Private Sub frmEntryReturnPurchase_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If tmpChange = True Then
            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah Anda ingin menyimpan perubahan " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If
            If tmpvar = 6 Then
                Call cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub frmEntryReturnPurchase_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_RETURN_PURCHASE.PURCHASE_RETURN_DETAIL' table. You can move, or remove it, as needed.
        Me.PURCHASE_RETURN_DETAILTableAdapter.Fill(Me.DS_RETURN_PURCHASE.PURCHASE_RETURN_DETAIL)
        'TODO: This line of code loads data into the 'DS_RETURN_PURCHASE.WAREHOUSE' table. You can move, or remove it, as needed.
        Me.WAREHOUSETableAdapter.Fill(Me.DS_RETURN_PURCHASE.WAREHOUSE)
        'TODO: This line of code loads data into the 'DS_RETURN_PURCHASE.PURCHASE_RETURN' table. You can move, or remove it, as needed.
        Me.PURCHASE_RETURNTableAdapter.Fill(Me.DS_RETURN_PURCHASE.PURCHASE_RETURN)


        If Language = "Indonesian" Then

            Me.Text = "Input Retur Pembelian"

            lblReceipNo.Text = "No Faktur"
            lblReturnDate.Text = "Tanggal Retur"
            lblReturnNo.Text = "No Retur"
            lblPayment.Text = "Cara Pembayaran"
            lblPurchaseDate.Text = "Tanggal"
            lblVendor.Text = "Supplier"
            lblWarehouse.Text = "Warehouse"
            lblReturnType.Text = "Tipe Retur"
            Label1.Text = "Keterangan"
            REUSABLE_STATUSCheckBox.Text = "Dapat Dipakai"

            cmdAdd.Text = "Tambah"
            cmdEdit.Text = "Ubah"
            cmdSave.Text = "Simpan"
            cmdUndo.Text = "Batal"
            cmdDelete.Text = "Hapus"

            dgvPurchaseReturn.Columns("PRODUCT_CODE").HeaderText = "Kode Produk"
            dgvPurchaseReturn.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            dgvPurchaseReturn.Columns("QUANTITY").HeaderText = "Jumlah"
            dgvPurchaseReturn.Columns("RETURNED_QUANTITY").HeaderText = "Jumlah yg telah di Retur"
            dgvPurchaseReturn.Columns("RETURN_QUANTITY").HeaderText = "Jumlah Retur"
            dgvPurchaseReturn.Columns("DESCRIPTION").HeaderText = "Keterangan"

        End If

        If tmpSavemode = "Update" Then
            If tmpWarehouseName = "" Then
                WAREHOUSE_NAMEComboBox.Text = ""
            Else
                WAREHOUSEBindingSource.Position = WAREHOUSEBindingSource.Find("WAREHOUSE_NAME", tmpWarehouseName)
            End If

            For x As Integer = 0 To getNoOfItem - 1

                Me.SP_PURCHASE_RETURN_DETAIL_LISTTableAdapter.Fill(Me.DS_RETURN_PURCHASE.SP_PURCHASE_RETURN_DETAIL_LIST, getPurchaseId)
                Me.SP_SHOW_LIST_RETURN_PURCHASETableAdapter.Fill(Me.DS_RETURN_PURCHASE.SP_SHOW_LIST_RETURN_PURCHASE, getPurchaseReturnId)
                dgvPurchaseReturn.Rows.Add()
                SP_PURCHASE_RETURN_DETAIL_LISTBindingSource.Position = x
                SP_SHOW_LIST_RETURN_PURCHASEBindingSource.Position = x
                dgvPurchaseReturn.Item("PRODUCT_CODE", x).Value = SP_SHOW_LIST_RETURN_PURCHASEBindingSource.Current("PRODUCT_CODE")
                dgvPurchaseReturn.Item("PRODUCT_NAME", x).Value = SP_SHOW_LIST_RETURN_PURCHASEBindingSource.Current("PRODUCT_NAME")
                dgvPurchaseReturn.Item("RETURNED_QUANTITY", x).Value = SP_PURCHASE_RETURN_DETAIL_LISTBindingSource.Current("RETURNED_QUANTITY")
                dgvPurchaseReturn.Item("QUANTITY", x).Value = SP_PURCHASE_RETURN_DETAIL_LISTBindingSource.Current("QUANTITY")
                dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value = SP_SHOW_LIST_RETURN_PURCHASEBindingSource.Current("QUANTITY")
            Next

            dgvPurchaseReturn.ReadOnly = True
            WAREHOUSE_NAMEComboBox.Enabled = False
            cmdSearch.Enabled = False
            cmdUndo.Enabled = False
            cmdSave.Enabled = False
            RETURN_TYPEComboBox.Enabled = False
            txtDescription.Enabled = False
            REUSABLE_STATUSCheckBox.Enabled = False
            dgvPurchaseReturn.AllowUserToAddRows = False
            dgvPurchaseReturn.AllowUserToDeleteRows = False
            dgvPurchaseReturn.AllowUserToOrderColumns = False
        Else
            txtReturnDate.Text = Format(dtpReturn.Value, "dd-MMM-yyyy")
            PURCHASE_RETURNTableAdapter.SP_GENERATE_PURCHASE_RETURN_NO(RETURN_NOTextBox.Text)
            WAREHOUSE_NAMEComboBox.Enabled = True
            tmpDataGridViewLoaded = True
            txtVendor.Text = ""
            txtReceiptNo.Text = ""
            txtDate.Text = ""
            txtDescription.Text = ""
            txtPaymentMethod.Text = ""

            RETURN_TYPEComboBox.Text = ""
            WAREHOUSE_NAMEComboBox.Text = ""
            'REUSABLE_STATUSCheckBox.Checked = False

            SP_PURCHASE_RETURN_DETAIL_LISTTableAdapter.Fill(Me.DS_RETURN_PURCHASE.SP_PURCHASE_RETURN_DETAIL_LIST, 0)
        End If

        tmpChange = False
    End Sub


  

    Sub save_return()
        Dim tmpWarehouseID As Integer
        If WAREHOUSE_NAMEComboBox.Text = "" Then
            tmpWarehouseID = 0
        Else
            tmpWarehouseID = WAREHOUSEBindingSource.Current("WAREHOUSE_ID")
        End If
        'Dim tmpWarehouseID As Integer = IIf(WAREHOUSE_NAMEComboBox.Text = "", 0, WAREHOUSEBindingSource.Current("WAREHOUSE_ID"))
        PURCHASE_RETURNTableAdapter.SP_PURCHASE_RETURN("I", 0, _
                                                                    RETURN_NOTextBox.Text, _
                                                                   dtpReturn.Value, _
                                                                   tmpReceiptId, _
                                                                   tmpTotal, _
                                                                   tmpWarehouseID, _
                                                                   RETURN_TYPEComboBox.Text, _
                                                                   txtDescription.Text, _
                                                                   USER_ID, _
                                                                   Now, _
                                                                   0, _
                                                                   DateSerial(4000, 12, 31))


        For x As Integer = 0 To dgvPurchaseReturn.RowCount - 1
            PURCHASE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvPurchaseReturn.Item("PRODUCT_CODE", x).Value, tmpProductId)
            If dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value <> 0 Then
                PURCHASE_RETURNTableAdapter.SP_PURCHASE_RETURN_DETAIL("I", 0, 0, _
                                                                                tmpProductId, _
                                                                                CInt(dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value), _
                                                                                tmpStatus, _
                                                                                dgvPurchaseReturn.Item("DESCRIPTION", x).Value, _
                                                                                USER_ID, _
                                                                                Now, _
                                                                                0, _
                                                                                DateSerial(4000, 12, 31))
            End If
        Next

        tmpChange = False
    End Sub

 

    Function Check_Warehouse() As Byte
        Dim tmpWarehouseID As Integer = -1
        ' tmpWarehouseID = IIf(WAREHOUSE_NAMEComboBox.Text = "", 0, WAREHOUSEBindingSource.Current("WAREHOUSE_ID"))

        '' Check Quantity Product in Warehouse
        For I As Integer = 0 To dgvPurchaseReturn.RowCount - 1
            PURCHASE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvPurchaseReturn.Item("PRODUCT_CODE", I).Value, tmpProductId)
            SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_RETURN_PURCHASE.SP_PRODUCT_WAREHOUSE_DETAIL, _
                                                      tmpProductId)
            SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Filter = "WAREHOUSE_ID = " & tmpWarehouseID

            If Language = "Indonesian" Then
                If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Count < 1 Then
                    MsgBox("Tidak dapat menyimpan transaksi ini!" & vbCrLf & dgvPurchaseReturn.Item("PRODUCT_NAME", I).Value & " tidak terdapat di dalam " & _
                        "Rummage" & "!", MsgBoxStyle.Critical, "DMI Retail")
                    Check_Warehouse = 0
                    Exit Function
                End If

                If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY") < dgvPurchaseReturn.Item("RETURN_QUANTITY", I).Value Then
                    MsgBox("Barang ini sudah tidak dapat di Retur !" & vbCrLf & _
                       "Jumlah retur melebihi produk yang ada di Gudang Sisa.", MsgBoxStyle.Critical, "DMI Retail")
                    Check_Warehouse = 0
                    Exit Function
                End If

                If SP_PURCHASE_RETURN_DETAIL_LISTBindingSource.Current("QUANTITY") < (dgvPurchaseReturn.Item("RETURNED_QUANTITY", I).Value + dgvPurchaseReturn.Item("RETURN_QUANTITY", I).Value) Then
                    MsgBox("Barang ini sudah tidak dapat di Retur !" & vbCrLf & _
                       "Jumlah retur sudah melebihi produk yang di beli", MsgBoxStyle.Critical, "DMI Retail")
                    Check_Warehouse = 0
                    Exit Function
                End If
            Else
                If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Count < 1 Then
                    MsgBox("Cannot save this transaction!" & vbCrLf & dgvPurchaseReturn.Item("PRODUCT_NAME", I).Value & " has no balance in " & _
                        "Rummage" & "!", MsgBoxStyle.Critical, "DMI Retail")
                    Check_Warehouse = 0
                    Exit Function
                End If

                If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY") < dgvPurchaseReturn.Item("RETURN_QUANTITY", I).Value Then
                    MsgBox("This product can not be Return !" & vbCrLf & _
                       "Total returns have exceeded Stock in Rummage.", MsgBoxStyle.Critical, "DMI Retail")
                    Check_Warehouse = 0
                    Exit Function
                End If

                If SP_PURCHASE_RETURN_DETAIL_LISTBindingSource.Current("QUANTITY") < (dgvPurchaseReturn.Item("RETURNED_QUANTITY", I).Value + dgvPurchaseReturn.Item("RETURN_QUANTITY", I).Value) Then
                    MsgBox("This product can not be Return !" & vbCrLf & _
                      "Total returns have exceeded that in sale products.", MsgBoxStyle.Critical, "DMI Retail")
                    Check_Warehouse = 0
                    Exit Function
                End If
            End If
        Next
        Check_Warehouse = 1
    End Function



    Private Sub dgvPurchaseReturn_CellValueChanged1(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPurchaseReturn.CellValueChanged
        If tmpSavemode = "Update" Then Exit Sub

        If tmpDataGridViewLoaded Then
            If dgvPurchaseReturn.CurrentCell.ColumnIndex = 6 Then
                If Not IsNumeric(dgvPurchaseReturn.CurrentCell.Value) Then
                    If Language = "Indonesian" Then
                        MsgBox("Anda memasukan nilai tak terduga !" & vbCrLf & _
                               "Untuk Kolom : " & _
                                dgvPurchaseReturn.Columns(dgvPurchaseReturn.CurrentCell.ColumnIndex).HeaderText & vbCrLf & _
                                "Baris Nomor : " & _
                                dgvPurchaseReturn.CurrentCell.RowIndex + 1, MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("You entered unexpected value !" & vbCrLf & _
                              "For Column : " & _
                               dgvPurchaseReturn.Columns(dgvPurchaseReturn.CurrentCell.ColumnIndex).HeaderText & vbCrLf & _
                               "Row number : " & _
                               dgvPurchaseReturn.CurrentCell.RowIndex + 1, MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    'give value "0" to empty fields or unexpected fields
                    dgvPurchaseReturn.CurrentCell.Value = 0
                    Exit Sub
                End If
            End If
        End If

        tmpChange = True
    End Sub

    Private Sub dgvPurchaseReturn_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPurchaseReturn.CellContentClick
        If tmpSavemode = "Update" Then Exit Sub

    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Dim tmpCategory As Integer
        Dim getWarehouseId As Integer = 0
        Dim tmpResult As Integer

        tmpPRAccountID = CInt(PURCHASE_RETURNTableAdapter.SP_SELECT_PARAMETER("RETURN PURCHASE ACCOUNT"))
        PURCHASE_RETURNTableAdapter.SP_GET_ACCOUNT_NUMBER(tmpPRAccountID, tmpPRAccount)
        PURCHASE_RETURNTableAdapter.SP_GET_PAYMENT_GROUP(tmpPaymentMethodID, tmpPaymentGroup)
        Try
            PURCHASE_RETURNTableAdapter.SP_GET_BALANCE_PAYABLE_PAYMENT(tmpReceiptId, tmpPRBalance)
        Catch
            tmpPRBalance = 0
        End Try
        tmpPRPrice = 0
        tmpPRAmount = 0

        If WAREHOUSE_NAMEComboBox.Text = "" Then
            getWarehouseId = 0
        Else
            getWarehouseId = WAREHOUSEBindingSource.Current("WAREHOUSE_ID")
        End If

        'getWarehouseId = IIf(WAREHOUSE_NAMEComboBox.Text = "", 0, WAREHOUSEBindingSource.Current("WAREHOUSE_ID"))

        If dgvPurchaseReturn.RowCount = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan masukkan produk yang ingin diretur !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please input any products to returned !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If txtVendor.Text = "" Or txtReceiptNo.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan ini kolom Jumlah !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please Fill Quantity fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If RETURN_TYPEComboBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan ini kolom Jumlah !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please Fill Quantity fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        For p As Integer = 0 To dgvPurchaseReturn.RowCount - 1
            PURCHASE_RETURNTableAdapter.SP_GET_CATEGORY_PRODUCT(dgvPurchaseReturn.Item("PRODUCT_CODE", p).Value, tmpCategory)
            If tmpCategory <> 1 Then
                If Language = "Indonesian" Then
                    MsgBox("Silahkan masukan produk lain !" & vbCrLf & _
                       "Tipe produk ini adalah jasa.", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Please enter other product !" & vbCrLf & _
                            "This type's product is Service.", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If
            If dgvPurchaseReturn.Item("RETURN_QUANTITY", p).Value = 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Silahkan isi kolom Jumlah Retur!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Please enter Return Quantity field!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If
        Next

        If PURCHASE_RETURNTableAdapter.SP_CLOSED_PERIOD_PROCESS("G", dtpReturn.Value) = True Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menyimpan transaksi ini!" & vbCrLf & "Proses Tutup Buku sedang berlangsung", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Save this Transaction!" & vbCrLf & "The Closing Period is Running", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        'If WAREHOUSEBindingSource.Current("WAREHOUSE_ID") = -1 Then
        '    If Language = "Indonesian" Then
        '        MsgBox("Silahkan pilih gudang yang lain !", MsgBoxStyle.Critical, "DMI Retail")
        '    Else
        '        MsgBox("Please enter other warehouse !", MsgBoxStyle.Critical, "DMI Retail")
        '    End If
        '    Exit Sub
        'End If

        ''''Check Return No''''
        PURCHASE_RETURNTableAdapter.SP_RETURN_PURCHASE_CHECK_RETURN_NO(RETURN_NOTextBox.Text, tmpResult)
        If tmpResult > 0 Then
            If Language = "Indonesian" Then
                MsgBox("No Transaksi telah digunakan oleh user lain." & vbCrLf & _
                                           "Nomor transaksi baru akan digunakan untuk transaksi ini.", _
                                           MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Receipt Number is used by another User." & vbCrLf & _
                                                    "New Receipt Number will be assigned to this transaction.", _
                                                    MsgBoxStyle.Critical, "DMI Retail")
            End If
        End If
        ''''if same Get new Return No''''
        While tmpResult > 0
            PURCHASE_RETURNTableAdapter.SP_GENERATE_PURCHASE_RETURN_NO(RETURN_NOTextBox.Text)
            PURCHASE_RETURNTableAdapter.SP_RETURN_PURCHASE_CHECK_RETURN_NO(RETURN_NOTextBox.Text, tmpResult)
        End While


        tmpTotal = dgvPurchaseReturn.RowCount
        ''Re-Useable Condition
        If REUSABLE_STATUSCheckBox.Checked = True Then

            tmpStatus = 1

            ''check product in warehouse normal
            Dim tmpWarehouseID As Integer
            If WAREHOUSE_NAMEComboBox.Text = "" Then
                tmpWarehouseID = 0
            Else
                tmpWarehouseID = WAREHOUSEBindingSource.Current("WAREHOUSE_ID")
            End If
            'tmpWarehouseID = IIf(WAREHOUSE_NAMEComboBox.Text = "", 0, WAREHOUSEBindingSource.Current("WAREHOUSE_ID"))

            '' Check Quantity Product in Warehouse
            For I As Integer = 0 To dgvPurchaseReturn.RowCount - 1
                PURCHASE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvPurchaseReturn.Item("PRODUCT_CODE", I).Value, tmpProductId)
                SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_RETURN_PURCHASE.SP_PRODUCT_WAREHOUSE_DETAIL, _
                                                          tmpProductId)
                SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Filter = "WAREHOUSE_ID = " & tmpWarehouseID

                If Language = "Indonesian" Then
                    If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Count < 1 Then
                        MsgBox("Tidak dapat menyimpan transaksi ini!" & vbCrLf & dgvPurchaseReturn.Item("PRODUCT_NAME", I).Value & " tidak terdapat di dalam " & _
                            WAREHOUSE_NAMEComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
                        Exit Sub
                    End If

                    If SP_PURCHASE_RETURN_DETAIL_LISTBindingSource.Current("QUANTITY") < (dgvPurchaseReturn.Item("RETURNED_QUANTITY", I).Value + dgvPurchaseReturn.Item("RETURN_QUANTITY", I).Value) Then
                        MsgBox("Barang ini sudah tidak dapat di Retur !" & vbCrLf & _
                           "Jumlah retur sudah melebihi produk yang di beli", MsgBoxStyle.Critical, "DMI Retail")
                        Exit Sub
                    End If

                    If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY") < CInt(dgvPurchaseReturn.Item("RETURN_QUANTITY", I).Value) Then
                        MsgBox("Tidak dapat menyimpan transaksi ini!" & vbCrLf & dgvPurchaseReturn.Item("PRODUCT_NAME", I).Value & " tidak terdapat di dalam " & _
                          WAREHOUSE_NAMEComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
                        Exit Sub
                    End If
                Else
                    If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Count < 1 Then
                        MsgBox("Cannot save this transaction!" & vbCrLf & dgvPurchaseReturn.Item("PRODUCT_NAME", I).Value & " has no balance in " & _
                            WAREHOUSE_NAMEComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
                        Exit Sub
                    End If

                    If SP_PURCHASE_RETURN_DETAIL_LISTBindingSource.Current("QUANTITY") < (dgvPurchaseReturn.Item("RETURNED_QUANTITY", I).Value + dgvPurchaseReturn.Item("RETURN_QUANTITY", I).Value) Then
                        MsgBox("This product can not be Return !" & vbCrLf & _
                          "Total returns have exceeded that in sale products", MsgBoxStyle.Critical, "DMI Retail")
                        Exit Sub
                    End If

                    If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY") < CInt(dgvPurchaseReturn.Item("RETURN_QUANTITY", I).Value) Then
                        MsgBox("Cannot save this transaction!" & vbCrLf & dgvPurchaseReturn.Item("PRODUCT_NAME", I).Value & " has no balance in " & _
                        WAREHOUSE_NAMEComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
                        Exit Sub
                    End If
                End If


            Next


            ''Check Account Number for this Transaction in Configuration
            If tmpPRAccount = "" Or tmpPRAccount = "0" Then
                If Language = "Indonesian" Then
                    MsgBox("Tidak ada nomor Akun untuk Transaksi ini." & vbCrLf & _
                           "Silahkan isi nomor Akun untuk transaksi ini di form Konfigurasi !", _
                           vbCritical, "DMI Retail")
                Else
                    MsgBox("There's no Account for this transaction." & vbCrLf & _
                           "Please input the Account for this transaction in Configuration !", _
                           vbCritical, "DMI Retail")
                End If
                Exit Sub
            End If


            ''PAYMENT METHOD CASE
            If tmpPaymentGroup = "CASH" Then

                If RETURN_TYPEComboBox.Text = "Goods" Then
                    ''insert into RETUN ONLY
                    Call save_return()

                ElseIf RETURN_TYPEComboBox.Text = "Non-Goods" Then

                    ''insert into Return
                    Call save_return()

                    ''Update warehouse Normal(-)
                    For x As Integer = 0 To dgvPurchaseReturn.RowCount - 1
                        PURCHASE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvPurchaseReturn.Item("PRODUCT_CODE", x).Value, tmpProductId)
                        If dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value <> 0 Then
                            PURCHASE_RETURNTableAdapter.SP_PRODUCT_WAREHOUSE("I", 0,
                                                                                tmpProductId, _
                                                                                getWarehouseId, _
                                                                                CInt(dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value) * (-1), _
                                                                                USER_ID, _
                                                                                Now, _
                                                                                0, _
                                                                                DateSerial(4000, 12, 31))
                            PURCHASE_RETURNTableAdapter.SP_GET_ATRIBUTE_PURCHASE(tmpReceiptId, _
                                                                                 tmpProductId, _
                                                                                 tmpPRPrice)
                            tmpPRPrice = tmpPRPrice * CInt(dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value)
                            tmpPRAmount = tmpPRAmount + tmpPRPrice
                        End If
                    Next

                    ''insert to Receivable Transaction (ReturPembelian-PurchasePrice)
                    PURCHASE_RETURNTableAdapter.SP_GENERATE_RECEIVABLE_TRANSACTION_NUMBER(tmpReceivableNo)
                    PURCHASE_RETURNTableAdapter.SP_RECEIVABLE_TRANSACTION("I", _
                                                                           0, _
                                                                           tmpReceivableNo, _
                                                                           tmpPRAccount, _
                                                                           dtpReturn.Value, _
                                                                           tmpPRAmount, _
                                                                           "Purchase Return - " & txtReceiptNo.Text, _
                                                                           mdlGeneral.USER_ID, _
                                                                           Now, _
                                                                           0, _
                                                                           DateSerial(4000, 12, 31))


                Else
                    If Language = "Indonesian" Then
                        MsgBox("Tipe Retur = " & RETURN_TYPEComboBox.Text & vbCrLf & _
                               " Tidak dapat ditemukan", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("Return type = " & RETURN_TYPEComboBox.Text & vbCrLf & _
                               " can not found", MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    Exit Sub
                End If

                ''payment method=credit
            Else
                If RETURN_TYPEComboBox.Text = "Goods" Then
                    ''insert into RETURN ONLY
                    Call save_return()

                ElseIf RETURN_TYPEComboBox.Text = "Non-Goods" Then
                    ''insert Into Return
                    Call save_return()

                    ''Update warehouse Normal(-)
                    For x As Integer = 0 To dgvPurchaseReturn.RowCount - 1
                        PURCHASE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvPurchaseReturn.Item("PRODUCT_CODE", x).Value, tmpProductId)
                        If dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value <> 0 Then
                            PURCHASE_RETURNTableAdapter.SP_PRODUCT_WAREHOUSE("I", 0,
                                                                                tmpProductId, _
                                                                                getWarehouseId, _
                                                                                CInt(dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value) * (-1), _
                                                                                USER_ID, _
                                                                                Now, _
                                                                                0, _
                                                                                DateSerial(4000, 12, 31))

                            PURCHASE_RETURNTableAdapter.SP_GET_ATRIBUTE_PURCHASE(tmpReceiptId, tmpProductId, tmpPRPrice)
                            tmpPRPrice = tmpPRPrice * CInt(dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value)
                            tmpPRAmount = tmpPRAmount + tmpPRPrice
                        End If
                    Next

                    PURCHASE_RETURNTableAdapter.SP_GENERATE_RECEIVABLE_TRANSACTION_NUMBER(tmpReceivableNo)
                    PURCHASE_RETURNTableAdapter.SP_PAYABLE_PAYMENT_GENERATE_NUMBER(tmpPayableNo)
                    PURCHASE_RETURNTableAdapter.SP_CHECK_PAYABLE_ID(tmpReceiptId, tmpResult)

                    ''Check if Payable Balance Amount is smaller than the Return, Cash is out, and Payable Payment is settled
                    If tmpPRBalance < tmpPRAmount Then
                        ''insert to Receivable Transaction (ReturPembelian-PurchasePrice)
                        PURCHASE_RETURNTableAdapter.SP_RECEIVABLE_TRANSACTION("I", _
                                                                                     0, _
                                                                                     tmpReceivableNo, _
                                                                                     tmpPRAccount, _
                                                                                     dtpReturn.Value, _
                                                                                     tmpPRAmount - tmpPRBalance,
                                                                                     "Purchase Return - " & txtReceiptNo.Text, _
                                                                                     mdlGeneral.USER_ID, _
                                                                                     Now, _
                                                                                     0, _
                                                                                     DateSerial(4000, 12, 31))
                        ''PAYABLE PAYMENT
                        'Remaining Debit => Payable_Payment is settled
                        PURCHASE_RETURNTableAdapter.SP_PAYABLE_PAYMENT("I", _
                                                                       0, _
                                                                       tmpPayableNo, _
                                                                       tmpReceiptId, _
                                                                       dtpReturn.Value, _
                                                                       tmpPRBalance, _
                                                                       vbNull, _
                                                                       "Purchase Return - " & txtReceiptNo.Text, _
                                                                       0, _
                                                                       mdlGeneral.USER_ID, _
                                                                       Now, _
                                                                       0, _
                                                                       DateSerial(4000, 12, 31))
                    Else
                        ''PAYABLE PAYMENT
                        'Remaining Debit => Payable_Payment 
                        PURCHASE_RETURNTableAdapter.SP_PAYABLE_PAYMENT("I", _
                                                                       0, _
                                                                       tmpPayableNo, _
                                                                       tmpReceiptId, _
                                                                       dtpReturn.Value, _
                                                                       tmpPRAmount, _
                                                                       vbNull, _
                                                                       "Purchase Return - " & txtReceiptNo.Text, _
                                                                       tmpPRBalance - tmpPRAmount, _
                                                                       mdlGeneral.USER_ID, _
                                                                       Now, _
                                                                       0, _
                                                                       DateSerial(4000, 12, 31))
                    End If

                Else
                    If Language = "Indonesian" Then
                        MsgBox("Tipe Retur = " & RETURN_TYPEComboBox.Text & vbCrLf & _
                               " Tidak dapat ditemukan", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("Return type = " & RETURN_TYPEComboBox.Text & vbCrLf & _
                               " can not found", MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    Exit Sub
                End If
            End If

            'Non-Re-Useable Condition
        Else
            tmpStatus = 0

            If Check_Warehouse() = 0 Then
                'GAGAL
                Exit Sub
            End If

            ''Check Account Number for this Transaction in Configuration
            If tmpPRAccount = "" Or tmpPRAccount = "0" Then
                If Language = "Indonesian" Then
                    MsgBox("Tidak ada nomor Akun untuk Transaksi ini." & vbCrLf & _
                           "Silahkan isi nomor Akun untuk transaksi ini di form Konfigurasi !", _
                           vbCritical, "DMI Retail")
                Else
                    MsgBox("There's no Account for this transaction." & vbCrLf & _
                           "Please input the Account for this transaction in Configuration !", _
                           vbCritical, "DMI Retail")
                End If
                Exit Sub
            End If

            If tmpPaymentGroup = "CASH" Then
                If RETURN_TYPEComboBox.Text = "Goods" Then
                    ''insert into return
                    Call save_return()

                    ''REJECT WAREHOUSE (-)
                    For x As Integer = 0 To dgvPurchaseReturn.RowCount - 1
                        PURCHASE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvPurchaseReturn.Item("PRODUCT_CODE", x).Value, tmpProductId)
                        If dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value <> 0 Then
                            PURCHASE_RETURNTableAdapter.SP_PRODUCT_WAREHOUSE("I", 0,
                                                                                tmpProductId, _
                                                                                -1, _
                                                                                CInt(dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value) * (-1), _
                                                                                USER_ID, _
                                                                                Now, _
                                                                                0, _
                                                                                DateSerial(4000, 12, 31))
                        End If
                    Next

                    ''NORMAL WAREHOUSE (+)
                    For x As Integer = 0 To dgvPurchaseReturn.RowCount - 1
                        PURCHASE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvPurchaseReturn.Item("PRODUCT_CODE", x).Value, tmpProductId)
                        If dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value <> 0 Then
                            PURCHASE_RETURNTableAdapter.SP_PRODUCT_WAREHOUSE("I", 0,
                                                                                tmpProductId, _
                                                                                getWarehouseId, _
                                                                                CInt(dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value), _
                                                                                USER_ID, _
                                                                                Now, _
                                                                                0, _
                                                                                DateSerial(4000, 12, 31))
                        End If
                    Next

                ElseIf RETURN_TYPEComboBox.Text = "Non-Goods" Then
                    ''Insert into return
                    Call save_return()

                    ''REJECT WAREHOUSE (-)
                    For x As Integer = 0 To dgvPurchaseReturn.RowCount - 1
                        PURCHASE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvPurchaseReturn.Item("PRODUCT_CODE", x).Value, tmpProductId)
                        If dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value <> 0 Then
                            PURCHASE_RETURNTableAdapter.SP_PRODUCT_WAREHOUSE("I", 0,
                                                                                tmpProductId, _
                                                                               -1, _
                                                                                CInt(dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value) * (-1), _
                                                                                USER_ID, _
                                                                                Now, _
                                                                                0, _
                                                                                DateSerial(4000, 12, 31))
                            ''get purchase price for this product
                            PURCHASE_RETURNTableAdapter.SP_GET_ATRIBUTE_PURCHASE(tmpReceiptId, _
                                                                                 tmpProductId, _
                                                                                 tmpPRPrice)
                            tmpPRPrice = tmpPRPrice * CInt(dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value)
                            tmpPRAmount = tmpPRAmount + tmpPRPrice
                        End If
                    Next

                    ''insert to Receivable Transaction (ReturPembelian-PurchasePrice)
                    PURCHASE_RETURNTableAdapter.SP_GENERATE_RECEIVABLE_TRANSACTION_NUMBER(tmpReceivableNo)
                    PURCHASE_RETURNTableAdapter.SP_RECEIVABLE_TRANSACTION("I", _
                                                                           0, _
                                                                           tmpReceivableNo, _
                                                                           tmpPRAccount, _
                                                                           dtpReturn.Value, _
                                                                           tmpPRAmount, _
                                                                           "Purchase Return - " & txtReceiptNo.Text, _
                                                                           mdlGeneral.USER_ID, _
                                                                           Now, _
                                                                           0, _
                                                                           DateSerial(4000, 12, 31))


                Else
                    If Language = "Indonesian" Then
                        MsgBox("Tipe Retur = " & RETURN_TYPEComboBox.Text & vbCrLf & _
                               " Tidak dapat ditemukan", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("Return type = " & RETURN_TYPEComboBox.Text & vbCrLf & _
                               " can not found", MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    Exit Sub
                End If

                ''payment method=credit
            Else
                If RETURN_TYPEComboBox.Text = "Goods" Then
                    ''Insert Save Return
                    Call save_return()

                    ''REJECT WAREHOUSE (-)
                    For x As Integer = 0 To dgvPurchaseReturn.RowCount - 1
                        PURCHASE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvPurchaseReturn.Item("PRODUCT_CODE", x).Value, tmpProductId)
                        If dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value <> 0 Then
                            PURCHASE_RETURNTableAdapter.SP_PRODUCT_WAREHOUSE("I", 0,
                                                                                tmpProductId, _
                                                                                -1, _
                                                                                CInt(dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value) * (-1), _
                                                                                USER_ID, _
                                                                                Now, _
                                                                                0, _
                                                                                DateSerial(4000, 12, 31))
                        End If
                    Next
                    ''NORMAL WAREHOUSE (+)
                    For x As Integer = 0 To dgvPurchaseReturn.RowCount - 1
                        PURCHASE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvPurchaseReturn.Item("PRODUCT_CODE", x).Value, tmpProductId)
                        If dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value <> 0 Then
                            PURCHASE_RETURNTableAdapter.SP_PRODUCT_WAREHOUSE("I", 0,
                                                                                tmpProductId, _
                                                                                getWarehouseId, _
                                                                                CInt(dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value), _
                                                                                USER_ID, _
                                                                                Now, _
                                                                                0, _
                                                                                DateSerial(4000, 12, 31))
                        End If
                    Next

                ElseIf RETURN_TYPEComboBox.Text = "Non-Goods" Then
                    ''Insert save Return
                    Call save_return()
                    ''REJECT WAREHOUSE (-)
                    For x As Integer = 0 To dgvPurchaseReturn.RowCount - 1
                        PURCHASE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvPurchaseReturn.Item("PRODUCT_CODE", x).Value, tmpProductId)
                        If dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value <> 0 Then
                            PURCHASE_RETURNTableAdapter.SP_PRODUCT_WAREHOUSE("I", 0,
                                                                                tmpProductId, _
                                                                               -1, _
                                                                                CInt(dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value) * (-1), _
                                                                                USER_ID, _
                                                                                Now, _
                                                                                0, _
                                                                                DateSerial(4000, 12, 31))
                            ''Get purchase price for this product
                            PURCHASE_RETURNTableAdapter.SP_GET_ATRIBUTE_PURCHASE(tmpReceiptId, _
                                                                                 tmpProductId, _
                                                                                 tmpPRPrice)
                            tmpPRPrice = tmpPRPrice * CInt(dgvPurchaseReturn.Item("RETURN_QUANTITY", x).Value)
                            tmpPRAmount = tmpPRAmount + tmpPRPrice
                        End If
                    Next

                    PURCHASE_RETURNTableAdapter.SP_GENERATE_RECEIVABLE_TRANSACTION_NUMBER(tmpReceivableNo)
                    PURCHASE_RETURNTableAdapter.SP_PAYABLE_PAYMENT_GENERATE_NUMBER(tmpPayableNo)
                    PURCHASE_RETURNTableAdapter.SP_CHECK_PAYABLE_ID(tmpReceiptId, tmpResult)

                    ''Check if Payable Balance Amount is smaller than the Return, payable payment is finished
                    If tmpPRBalance < tmpPRAmount Then
                        ''insert to Receivable Transaction (ReturPembelian-PurchasePrice)
                        PURCHASE_RETURNTableAdapter.SP_RECEIVABLE_TRANSACTION("I", _
                                                                                     0, _
                                                                                     tmpReceivableNo, _
                                                                                     tmpPRAccount, _
                                                                                     dtpReturn.Value, _
                                                                                     tmpPRAmount - tmpPRBalance,
                                                                                     "Purchase Return - " & txtReceiptNo.Text, _
                                                                                     mdlGeneral.USER_ID, _
                                                                                     Now, _
                                                                                     0, _
                                                                                     DateSerial(4000, 12, 31))
                        ''PAYABLE PAYMENT
                        'Remaining Debit => Payable_Payment 
                        PURCHASE_RETURNTableAdapter.SP_PAYABLE_PAYMENT("I", _
                                                                       0, _
                                                                       tmpPayableNo, _
                                                                       tmpReceiptId, _
                                                                       dtpReturn.Value, _
                                                                       tmpPRBalance, _
                                                                       vbNull, _
                                                                       "Purchase Return - " & txtReceiptNo.Text, _
                                                                       0, _
                                                                       mdlGeneral.USER_ID, _
                                                                       Now, _
                                                                       0, _
                                                                       DateSerial(4000, 12, 31))
                    Else
                        ''PAYABLE PAYMENT
                        'Remaining Debit => Payable_Payment , if the Payable Balance is larger than the Return 
                        PURCHASE_RETURNTableAdapter.SP_PAYABLE_PAYMENT("I", _
                                                                       0, _
                                                                       tmpPayableNo, _
                                                                       tmpReceiptId, _
                                                                       dtpReturn.Value, _
                                                                       tmpPRAmount, _
                                                                       vbNull, _
                                                                       "Purchase Return - " & txtReceiptNo.Text, _
                                                                       tmpPRBalance - tmpPRAmount, _
                                                                       mdlGeneral.USER_ID, _
                                                                       Now, _
                                                                       0, _
                                                                       DateSerial(4000, 12, 31))
                    End If


                Else
                    If Language = "Indonesian" Then
                        MsgBox("Tipe Retur = " & RETURN_TYPEComboBox.Text & vbCrLf & _
                               " Tidak dapat ditemukan", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("Return type = " & RETURN_TYPEComboBox.Text & vbCrLf & _
                               " can not found", MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    Exit Sub
                End If

            End If
        End If


        If Language = "Indonesian" Then
            MsgBox("Data berhasil disimpan", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data Successful saved", MsgBoxStyle.Information, "DMI Retail")
        End If

        txtVendor.Text = ""
        txtReceiptNo.Text = ""
        txtDate.Text = ""
        txtDescription.Text = ""
        txtPaymentMethod.Text = ""

        RETURN_TYPEComboBox.Text = ""
        WAREHOUSE_NAMEComboBox.Text = ""
        REUSABLE_STATUSCheckBox.Checked = False
        dgvPurchaseReturn.Rows.Clear()

        SP_PURCHASE_RETURN_DETAIL_LISTTableAdapter.Fill(Me.DS_RETURN_PURCHASE.SP_PURCHASE_RETURN_DETAIL_LIST, 0)
        PURCHASE_RETURNTableAdapter.SP_GENERATE_PURCHASE_RETURN_NO(RETURN_NOTextBox.Text)

        tmpChange = False
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        Me.Close()
    End Sub
End Class