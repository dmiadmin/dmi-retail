﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Data
Public Class frmShrink
    Dim xConn As New SqlConnection
    Dim xCmd As SqlCommand
    Public Language, ServerName, DatabaseName As String
    Public tmpConfig() As String
    'Dim Path_Name As String = Application.StartupPath
    'Dim Path_Data As String = Replace(Application.StartupPath, Mid(Application.StartupPath, 51, 71), "")
    'Dim PathDb As String = Replace(Application.StartupPath, Mid(Application.StartupPath, 51, 71), "") & "\DMI_RETAIL\bin\Debug\DB_DMI_RETAIL.mdf"

    Sub connection()
        If File.Exists(Application.StartupPath & "\Config.inf") Then
            tmpConfig = File.ReadAllLines(Application.StartupPath & "\Config.inf")
            For x As Integer = 0 To tmpConfig.Count - 1
                If tmpConfig(x).StartsWith("[Language]") Then
                    Language = tmpConfig(x).Split("=")(1)
                ElseIf tmpConfig(x).StartsWith("[Server Name]") Then
                    ServerName = tmpConfig(x).Split("=")(1)
                ElseIf tmpConfig(x).StartsWith("[Database Name]") Then
                    DatabaseName = tmpConfig(x).Split("=")(1)
                End If
            Next
        End If
        xConn.Close()
        xConn.ConnectionString = "Data Source=" & ServerName & ";Initial Catalog=" & DatabaseName & ";Persist Security Info=True;User ID=sa;Password=SA;"
        xConn.Open()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Call connection()
        xCmd = New SqlCommand("USE [master]" & vbCrLf & _
                              "CREATE DATABASE [DB_DMI_RETAIL_2] ON " & vbCrLf & _
                              "( FILENAME = N'" & Application.StartupPath & "\DB_DMI_RETAIL.mdf' ), " & vbCrLf & _
                              "( FILENAME = N'" & Application.StartupPath & "\DB_DMI_RETAIL_log.ldf' )" & vbCrLf & _
                              "FOR ATTACH", xConn)
        Dim x As Integer = xCmd.ExecuteNonQuery

        xCmd = New SqlCommand("DBCC SHRINKDATABASE (N'DB_DMI_RETAIL_2', truncateonly)", xConn)
        x = xCmd.ExecuteNonQuery

        xCmd = New SqlCommand("EXEC master.dbo.sp_detach_db @dbname = N'DB_DMI_RETAIL_2'", xConn)
        x = xCmd.ExecuteNonQuery

        MsgBox("SUCCESS")
        Me.Close()
    End Sub
End Class