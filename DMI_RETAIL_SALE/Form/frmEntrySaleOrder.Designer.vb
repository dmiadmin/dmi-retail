﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEntrySaleOrder
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEntrySaleOrder))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SALE_ORDER_NUMBERTextBox = New System.Windows.Forms.TextBox()
        Me.SALE_ORDER_HEADERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_SALE_ORDER = New DMI_RETAIL_SALE.DS_SALE_ORDER()
        Me.SALE_ORDER_DATEDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.DELIVERY_REQUEST_DATEDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.SHIP_TOTextBox = New System.Windows.Forms.TextBox()
        Me.CUSTOMER_NAMEComboBox = New System.Windows.Forms.ComboBox()
        Me.CUSTOMERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblSaleOrderNo = New System.Windows.Forms.Label()
        Me.lblSaleOrderDate = New System.Windows.Forms.Label()
        Me.lblDelRequestDate = New System.Windows.Forms.Label()
        Me.lblShip = New System.Windows.Forms.Label()
        Me.lblCustomer = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmdSearchName = New System.Windows.Forms.PictureBox()
        Me.dgvSaleOrder = New System.Windows.Forms.DataGridView()
        Me.PRODUCT_CODE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRODUCT_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QUANTITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRICE_PER_UNIT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DISCOUNT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DISCOUNT2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.STATUS = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.DELETE = New System.Windows.Forms.DataGridViewImageColumn()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblNoOfItem = New System.Windows.Forms.Label()
        Me.lblDel = New System.Windows.Forms.Label()
        Me.lblF6 = New System.Windows.Forms.Label()
        Me.lblF5 = New System.Windows.Forms.Label()
        Me.AMOUNT_DISCOUNTTextBox = New System.Windows.Forms.TextBox()
        Me.GRAND_TOTALTextBox = New System.Windows.Forms.TextBox()
        Me.lblDiscount = New System.Windows.Forms.Label()
        Me.lblGrandTotal = New System.Windows.Forms.Label()
        Me.lblSalesman = New System.Windows.Forms.Label()
        Me.TAXPercentageTEXTBOX = New System.Windows.Forms.TextBox()
        Me.TAX_AMOUNTTextBox = New System.Windows.Forms.TextBox()
        Me.lblTax = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.SALESMAN_IDComboBox = New System.Windows.Forms.ComboBox()
        Me.SALESMANBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.cmdPrint = New System.Windows.Forms.Button()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.SALE_ORDER_HEADERTableAdapter = New DMI_RETAIL_SALE.DS_SALE_ORDERTableAdapters.SALE_ORDER_HEADERTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_SALE.DS_SALE_ORDERTableAdapters.TableAdapterManager()
        Me.CUSTOMERTableAdapter = New DMI_RETAIL_SALE.DS_SALE_ORDERTableAdapters.CUSTOMERTableAdapter()
        Me.PRODUCTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PRODUCTTableAdapter = New DMI_RETAIL_SALE.DS_SALE_ORDERTableAdapters.PRODUCTTableAdapter()
        Me.SALESMANTableAdapter = New DMI_RETAIL_SALE.DS_SALE_ORDERTableAdapters.SALESMANTableAdapter()
        Me.SALE_ORDER_DETAILBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SALE_ORDER_DETAILTableAdapter = New DMI_RETAIL_SALE.DS_SALE_ORDERTableAdapters.SALE_ORDER_DETAILTableAdapter()
        Me.SP_DETAIL_SALE_ORDERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_DETAIL_SALE_ORDERTableAdapter = New DMI_RETAIL_SALE.DS_SALE_ORDERTableAdapters.SP_DETAIL_SALE_ORDERTableAdapter()
        CType(Me.SALE_ORDER_HEADERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_SALE_ORDER, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CUSTOMERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSaleOrder, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.SALESMANBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SALE_ORDER_DETAILBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_DETAIL_SALE_ORDERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SALE_ORDER_NUMBERTextBox
        '
        Me.SALE_ORDER_NUMBERTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SALE_ORDER_HEADERBindingSource, "SALE_ORDER_NUMBER", True))
        Me.SALE_ORDER_NUMBERTextBox.Location = New System.Drawing.Point(168, 22)
        Me.SALE_ORDER_NUMBERTextBox.Name = "SALE_ORDER_NUMBERTextBox"
        Me.SALE_ORDER_NUMBERTextBox.ReadOnly = True
        Me.SALE_ORDER_NUMBERTextBox.Size = New System.Drawing.Size(126, 22)
        Me.SALE_ORDER_NUMBERTextBox.TabIndex = 2
        '
        'SALE_ORDER_HEADERBindingSource
        '
        Me.SALE_ORDER_HEADERBindingSource.DataMember = "SALE_ORDER_HEADER"
        Me.SALE_ORDER_HEADERBindingSource.DataSource = Me.DS_SALE_ORDER
        '
        'DS_SALE_ORDER
        '
        Me.DS_SALE_ORDER.DataSetName = "DS_SALE_ORDER"
        Me.DS_SALE_ORDER.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SALE_ORDER_DATEDateTimePicker
        '
        Me.SALE_ORDER_DATEDateTimePicker.CustomFormat = "dd-MMM-yyyy"
        Me.SALE_ORDER_DATEDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.SALE_ORDER_HEADERBindingSource, "SALE_ORDER_DATE", True))
        Me.SALE_ORDER_DATEDateTimePicker.Enabled = False
        Me.SALE_ORDER_DATEDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.SALE_ORDER_DATEDateTimePicker.Location = New System.Drawing.Point(168, 56)
        Me.SALE_ORDER_DATEDateTimePicker.Name = "SALE_ORDER_DATEDateTimePicker"
        Me.SALE_ORDER_DATEDateTimePicker.Size = New System.Drawing.Size(126, 22)
        Me.SALE_ORDER_DATEDateTimePicker.TabIndex = 3
        '
        'DELIVERY_REQUEST_DATEDateTimePicker
        '
        Me.DELIVERY_REQUEST_DATEDateTimePicker.CustomFormat = "dd-MMM-yyyy"
        Me.DELIVERY_REQUEST_DATEDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.SALE_ORDER_HEADERBindingSource, "DELIVERY_REQUEST_DATE", True))
        Me.DELIVERY_REQUEST_DATEDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DELIVERY_REQUEST_DATEDateTimePicker.Location = New System.Drawing.Point(168, 94)
        Me.DELIVERY_REQUEST_DATEDateTimePicker.Name = "DELIVERY_REQUEST_DATEDateTimePicker"
        Me.DELIVERY_REQUEST_DATEDateTimePicker.Size = New System.Drawing.Size(126, 22)
        Me.DELIVERY_REQUEST_DATEDateTimePicker.TabIndex = 5
        '
        'SHIP_TOTextBox
        '
        Me.SHIP_TOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SALE_ORDER_HEADERBindingSource, "SHIP_TO", True))
        Me.SHIP_TOTextBox.Location = New System.Drawing.Point(449, 22)
        Me.SHIP_TOTextBox.Multiline = True
        Me.SHIP_TOTextBox.Name = "SHIP_TOTextBox"
        Me.SHIP_TOTextBox.Size = New System.Drawing.Size(278, 56)
        Me.SHIP_TOTextBox.TabIndex = 7
        '
        'CUSTOMER_NAMEComboBox
        '
        Me.CUSTOMER_NAMEComboBox.DataSource = Me.CUSTOMERBindingSource
        Me.CUSTOMER_NAMEComboBox.DisplayMember = "CUSTOMER_NAME"
        Me.CUSTOMER_NAMEComboBox.FormattingEnabled = True
        Me.CUSTOMER_NAMEComboBox.Location = New System.Drawing.Point(449, 92)
        Me.CUSTOMER_NAMEComboBox.Name = "CUSTOMER_NAMEComboBox"
        Me.CUSTOMER_NAMEComboBox.Size = New System.Drawing.Size(218, 23)
        Me.CUSTOMER_NAMEComboBox.TabIndex = 9
        Me.CUSTOMER_NAMEComboBox.ValueMember = "CUSTOMER_ID"
        '
        'CUSTOMERBindingSource
        '
        Me.CUSTOMERBindingSource.DataMember = "CUSTOMER"
        Me.CUSTOMERBindingSource.DataSource = Me.DS_SALE_ORDER
        '
        'lblSaleOrderNo
        '
        Me.lblSaleOrderNo.AutoSize = True
        Me.lblSaleOrderNo.Location = New System.Drawing.Point(16, 25)
        Me.lblSaleOrderNo.Name = "lblSaleOrderNo"
        Me.lblSaleOrderNo.Size = New System.Drawing.Size(120, 15)
        Me.lblSaleOrderNo.TabIndex = 10
        Me.lblSaleOrderNo.Text = "Sale Order Number"
        '
        'lblSaleOrderDate
        '
        Me.lblSaleOrderDate.AutoSize = True
        Me.lblSaleOrderDate.Location = New System.Drawing.Point(16, 64)
        Me.lblSaleOrderDate.Name = "lblSaleOrderDate"
        Me.lblSaleOrderDate.Size = New System.Drawing.Size(102, 15)
        Me.lblSaleOrderDate.TabIndex = 11
        Me.lblSaleOrderDate.Text = "Sale Order Date"
        '
        'lblDelRequestDate
        '
        Me.lblDelRequestDate.AutoSize = True
        Me.lblDelRequestDate.Location = New System.Drawing.Point(16, 100)
        Me.lblDelRequestDate.Name = "lblDelRequestDate"
        Me.lblDelRequestDate.Size = New System.Drawing.Size(139, 15)
        Me.lblDelRequestDate.TabIndex = 12
        Me.lblDelRequestDate.Text = "Delivery Request Date"
        '
        'lblShip
        '
        Me.lblShip.AutoSize = True
        Me.lblShip.Location = New System.Drawing.Point(361, 25)
        Me.lblShip.Name = "lblShip"
        Me.lblShip.Size = New System.Drawing.Size(52, 15)
        Me.lblShip.TabIndex = 13
        Me.lblShip.Text = "Ship To"
        '
        'lblCustomer
        '
        Me.lblCustomer.AutoSize = True
        Me.lblCustomer.Location = New System.Drawing.Point(361, 97)
        Me.lblCustomer.Name = "lblCustomer"
        Me.lblCustomer.Size = New System.Drawing.Size(66, 15)
        Me.lblCustomer.TabIndex = 14
        Me.lblCustomer.Text = "Customer"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmdSearchName)
        Me.GroupBox1.Controls.Add(Me.lblCustomer)
        Me.GroupBox1.Controls.Add(Me.lblShip)
        Me.GroupBox1.Controls.Add(Me.lblDelRequestDate)
        Me.GroupBox1.Controls.Add(Me.lblSaleOrderDate)
        Me.GroupBox1.Controls.Add(Me.lblSaleOrderNo)
        Me.GroupBox1.Controls.Add(Me.CUSTOMER_NAMEComboBox)
        Me.GroupBox1.Controls.Add(Me.SHIP_TOTextBox)
        Me.GroupBox1.Controls.Add(Me.DELIVERY_REQUEST_DATEDateTimePicker)
        Me.GroupBox1.Controls.Add(Me.SALE_ORDER_DATEDateTimePicker)
        Me.GroupBox1.Controls.Add(Me.SALE_ORDER_NUMBERTextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(750, 136)
        Me.GroupBox1.TabIndex = 15
        Me.GroupBox1.TabStop = False
        '
        'cmdSearchName
        '
        Me.cmdSearchName.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchName.Image = CType(resources.GetObject("cmdSearchName.Image"), System.Drawing.Image)
        Me.cmdSearchName.Location = New System.Drawing.Point(674, 93)
        Me.cmdSearchName.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchName.Name = "cmdSearchName"
        Me.cmdSearchName.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearchName.TabIndex = 19
        Me.cmdSearchName.TabStop = False
        '
        'dgvSaleOrder
        '
        Me.dgvSaleOrder.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSaleOrder.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSaleOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSaleOrder.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PRODUCT_CODE, Me.PRODUCT_NAME, Me.QUANTITY, Me.PRICE_PER_UNIT, Me.DISCOUNT, Me.DISCOUNT2, Me.TOTAL, Me.STATUS, Me.DELETE})
        Me.dgvSaleOrder.Location = New System.Drawing.Point(18, 37)
        Me.dgvSaleOrder.Name = "dgvSaleOrder"
        Me.dgvSaleOrder.Size = New System.Drawing.Size(716, 193)
        Me.dgvSaleOrder.TabIndex = 16
        '
        'PRODUCT_CODE
        '
        Me.PRODUCT_CODE.HeaderText = "Product Code"
        Me.PRODUCT_CODE.Name = "PRODUCT_CODE"
        Me.PRODUCT_CODE.Width = 110
        '
        'PRODUCT_NAME
        '
        Me.PRODUCT_NAME.HeaderText = "Product Name"
        Me.PRODUCT_NAME.Name = "PRODUCT_NAME"
        Me.PRODUCT_NAME.ReadOnly = True
        Me.PRODUCT_NAME.Width = 110
        '
        'QUANTITY
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.QUANTITY.DefaultCellStyle = DataGridViewCellStyle2
        Me.QUANTITY.HeaderText = "Qty"
        Me.QUANTITY.Name = "QUANTITY"
        Me.QUANTITY.Width = 50
        '
        'PRICE_PER_UNIT
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N0"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.PRICE_PER_UNIT.DefaultCellStyle = DataGridViewCellStyle3
        Me.PRICE_PER_UNIT.HeaderText = "Unit Price"
        Me.PRICE_PER_UNIT.Name = "PRICE_PER_UNIT"
        Me.PRICE_PER_UNIT.Width = 80
        '
        'DISCOUNT
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.DISCOUNT.DefaultCellStyle = DataGridViewCellStyle4
        Me.DISCOUNT.HeaderText = "Disc (%)"
        Me.DISCOUNT.Name = "DISCOUNT"
        Me.DISCOUNT.Width = 50
        '
        'DISCOUNT2
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N0"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.DISCOUNT2.DefaultCellStyle = DataGridViewCellStyle5
        Me.DISCOUNT2.HeaderText = "Disc (Rp)"
        Me.DISCOUNT2.Name = "DISCOUNT2"
        Me.DISCOUNT2.Width = 75
        '
        'TOTAL
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N0"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.TOTAL.DefaultCellStyle = DataGridViewCellStyle6
        Me.TOTAL.HeaderText = "Total"
        Me.TOTAL.Name = "TOTAL"
        Me.TOTAL.ReadOnly = True
        Me.TOTAL.Width = 90
        '
        'STATUS
        '
        Me.STATUS.HeaderText = "Status"
        Me.STATUS.Items.AddRange(New Object() {"Open", "Close"})
        Me.STATUS.Name = "STATUS"
        Me.STATUS.Width = 60
        '
        'DELETE
        '
        Me.DELETE.HeaderText = "Del"
        Me.DELETE.Image = Global.DMI_RETAIL_SALE.My.Resources.Resources.BindingNavigatorDeleteItem_Image
        Me.DELETE.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch
        Me.DELETE.Name = "DELETE"
        Me.DELETE.Width = 50
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblNoOfItem)
        Me.GroupBox2.Controls.Add(Me.lblDel)
        Me.GroupBox2.Controls.Add(Me.lblF6)
        Me.GroupBox2.Controls.Add(Me.lblF5)
        Me.GroupBox2.Controls.Add(Me.dgvSaleOrder)
        Me.GroupBox2.Location = New System.Drawing.Point(15, 135)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(748, 260)
        Me.GroupBox2.TabIndex = 17
        Me.GroupBox2.TabStop = False
        '
        'lblNoOfItem
        '
        Me.lblNoOfItem.AutoSize = True
        Me.lblNoOfItem.Location = New System.Drawing.Point(20, 237)
        Me.lblNoOfItem.Name = "lblNoOfItem"
        Me.lblNoOfItem.Size = New System.Drawing.Size(107, 15)
        Me.lblNoOfItem.TabIndex = 40
        Me.lblNoOfItem.Text = "No of Item(s) :  0"
        '
        'lblDel
        '
        Me.lblDel.Location = New System.Drawing.Point(521, 17)
        Me.lblDel.Name = "lblDel"
        Me.lblDel.Size = New System.Drawing.Size(198, 14)
        Me.lblDel.TabIndex = 39
        Me.lblDel.Text = "Del : Remove Product from List"
        Me.lblDel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblF6
        '
        Me.lblF6.Location = New System.Drawing.Point(269, 18)
        Me.lblF6.Name = "lblF6"
        Me.lblF6.Size = New System.Drawing.Size(180, 14)
        Me.lblF6.TabIndex = 38
        Me.lblF6.Text = "F6 : Browse Product by Name"
        Me.lblF6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblF5
        '
        Me.lblF5.Location = New System.Drawing.Point(21, 16)
        Me.lblF5.Name = "lblF5"
        Me.lblF5.Size = New System.Drawing.Size(186, 14)
        Me.lblF5.TabIndex = 37
        Me.lblF5.Text = "F5 : Browse Product by Code"
        Me.lblF5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'AMOUNT_DISCOUNTTextBox
        '
        Me.AMOUNT_DISCOUNTTextBox.Location = New System.Drawing.Point(527, 19)
        Me.AMOUNT_DISCOUNTTextBox.Name = "AMOUNT_DISCOUNTTextBox"
        Me.AMOUNT_DISCOUNTTextBox.ReadOnly = True
        Me.AMOUNT_DISCOUNTTextBox.Size = New System.Drawing.Size(113, 22)
        Me.AMOUNT_DISCOUNTTextBox.TabIndex = 10
        Me.AMOUNT_DISCOUNTTextBox.Text = "0"
        Me.AMOUNT_DISCOUNTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GRAND_TOTALTextBox
        '
        Me.GRAND_TOTALTextBox.Location = New System.Drawing.Point(527, 47)
        Me.GRAND_TOTALTextBox.Name = "GRAND_TOTALTextBox"
        Me.GRAND_TOTALTextBox.Size = New System.Drawing.Size(113, 22)
        Me.GRAND_TOTALTextBox.TabIndex = 17
        Me.GRAND_TOTALTextBox.Text = "0"
        Me.GRAND_TOTALTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDiscount
        '
        Me.lblDiscount.Location = New System.Drawing.Point(447, 22)
        Me.lblDiscount.Name = "lblDiscount"
        Me.lblDiscount.Size = New System.Drawing.Size(75, 14)
        Me.lblDiscount.TabIndex = 38
        Me.lblDiscount.Text = "Discount"
        Me.lblDiscount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblGrandTotal
        '
        Me.lblGrandTotal.Location = New System.Drawing.Point(436, 50)
        Me.lblGrandTotal.Name = "lblGrandTotal"
        Me.lblGrandTotal.Size = New System.Drawing.Size(85, 14)
        Me.lblGrandTotal.TabIndex = 37
        Me.lblGrandTotal.Text = "Grand Total"
        Me.lblGrandTotal.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblSalesman
        '
        Me.lblSalesman.AutoSize = True
        Me.lblSalesman.Location = New System.Drawing.Point(56, 21)
        Me.lblSalesman.Name = "lblSalesman"
        Me.lblSalesman.Size = New System.Drawing.Size(63, 15)
        Me.lblSalesman.TabIndex = 46
        Me.lblSalesman.Text = "Salesman"
        '
        'TAXPercentageTEXTBOX
        '
        Me.TAXPercentageTEXTBOX.Location = New System.Drawing.Point(138, 47)
        Me.TAXPercentageTEXTBOX.MaxLength = 5
        Me.TAXPercentageTEXTBOX.Name = "TAXPercentageTEXTBOX"
        Me.TAXPercentageTEXTBOX.Size = New System.Drawing.Size(44, 22)
        Me.TAXPercentageTEXTBOX.TabIndex = 15
        Me.TAXPercentageTEXTBOX.Text = "0.00"
        Me.TAXPercentageTEXTBOX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TAX_AMOUNTTextBox
        '
        Me.TAX_AMOUNTTextBox.Location = New System.Drawing.Point(188, 47)
        Me.TAX_AMOUNTTextBox.MaxLength = 9
        Me.TAX_AMOUNTTextBox.Name = "TAX_AMOUNTTextBox"
        Me.TAX_AMOUNTTextBox.Size = New System.Drawing.Size(123, 22)
        Me.TAX_AMOUNTTextBox.TabIndex = 16
        Me.TAX_AMOUNTTextBox.Text = "0"
        Me.TAX_AMOUNTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTax
        '
        Me.lblTax.Location = New System.Drawing.Point(73, 50)
        Me.lblTax.Name = "lblTax"
        Me.lblTax.Size = New System.Drawing.Size(59, 14)
        Me.lblTax.TabIndex = 49
        Me.lblTax.Text = "Tax [F2]"
        Me.lblTax.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.SALESMAN_IDComboBox)
        Me.GroupBox3.Controls.Add(Me.lblTax)
        Me.GroupBox3.Controls.Add(Me.TAX_AMOUNTTextBox)
        Me.GroupBox3.Controls.Add(Me.TAXPercentageTEXTBOX)
        Me.GroupBox3.Controls.Add(Me.lblSalesman)
        Me.GroupBox3.Controls.Add(Me.lblGrandTotal)
        Me.GroupBox3.Controls.Add(Me.lblDiscount)
        Me.GroupBox3.Controls.Add(Me.GRAND_TOTALTextBox)
        Me.GroupBox3.Controls.Add(Me.AMOUNT_DISCOUNTTextBox)
        Me.GroupBox3.Location = New System.Drawing.Point(15, 394)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(748, 78)
        Me.GroupBox3.TabIndex = 18
        Me.GroupBox3.TabStop = False
        '
        'SALESMAN_IDComboBox
        '
        Me.SALESMAN_IDComboBox.DataSource = Me.SALESMANBindingSource
        Me.SALESMAN_IDComboBox.DisplayMember = "SALESMAN_NAME"
        Me.SALESMAN_IDComboBox.FormattingEnabled = True
        Me.SALESMAN_IDComboBox.Location = New System.Drawing.Point(139, 18)
        Me.SALESMAN_IDComboBox.Name = "SALESMAN_IDComboBox"
        Me.SALESMAN_IDComboBox.Size = New System.Drawing.Size(172, 23)
        Me.SALESMAN_IDComboBox.TabIndex = 50
        Me.SALESMAN_IDComboBox.ValueMember = "SALESMAN_ID"
        '
        'SALESMANBindingSource
        '
        Me.SALESMANBindingSource.DataMember = "SALESMAN"
        Me.SALESMANBindingSource.DataSource = Me.DS_SALE_ORDER
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.cmdPrint)
        Me.GroupBox4.Controls.Add(Me.cmdUndo)
        Me.GroupBox4.Controls.Add(Me.cmdSave)
        Me.GroupBox4.Controls.Add(Me.cmdEdit)
        Me.GroupBox4.Controls.Add(Me.cmdDelete)
        Me.GroupBox4.Controls.Add(Me.cmdAdd)
        Me.GroupBox4.Location = New System.Drawing.Point(128, 478)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox4.Size = New System.Drawing.Size(636, 55)
        Me.GroupBox4.TabIndex = 19
        Me.GroupBox4.TabStop = False
        '
        'cmdPrint
        '
        Me.cmdPrint.AutoSize = True
        Me.cmdPrint.Font = New System.Drawing.Font("Lucida Bright", 7.0!)
        Me.cmdPrint.Image = Global.DMI_RETAIL_SALE.My.Resources.Resources.Printer
        Me.cmdPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdPrint.Location = New System.Drawing.Point(517, 11)
        Me.cmdPrint.Name = "cmdPrint"
        Me.cmdPrint.Size = New System.Drawing.Size(108, 38)
        Me.cmdPrint.TabIndex = 25
        Me.cmdPrint.Text = "Save && &Print" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F12]"
        Me.cmdPrint.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.cmdPrint.UseVisualStyleBackColor = True
        '
        'cmdUndo
        '
        Me.cmdUndo.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(216, 11)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(83, 38)
        Me.cmdUndo.TabIndex = 21
        Me.cmdUndo.Text = "&Undo" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F9]"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSave.Location = New System.Drawing.Point(317, 11)
        Me.cmdSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(83, 38)
        Me.cmdSave.TabIndex = 22
        Me.cmdSave.Text = "&Save" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F10]"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(115, 11)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(83, 38)
        Me.cmdEdit.TabIndex = 20
        Me.cmdEdit.Text = "&Edit" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F8]"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(418, 11)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(83, 38)
        Me.cmdDelete.TabIndex = 23
        Me.cmdDelete.Text = "&Delete" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F11]"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(14, 11)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(83, 38)
        Me.cmdAdd.TabIndex = 19
        Me.cmdAdd.Text = "&Add" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F7]"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'SALE_ORDER_HEADERTableAdapter
        '
        Me.SALE_ORDER_HEADERTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.SALE_ORDER_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.SALE_ORDER_HEADERTableAdapter = Me.SALE_ORDER_HEADERTableAdapter
        Me.TableAdapterManager.SALESMANTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_SALE.DS_SALE_ORDERTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'CUSTOMERTableAdapter
        '
        Me.CUSTOMERTableAdapter.ClearBeforeFill = True
        '
        'PRODUCTBindingSource
        '
        Me.PRODUCTBindingSource.DataMember = "PRODUCT"
        Me.PRODUCTBindingSource.DataSource = Me.DS_SALE_ORDER
        '
        'PRODUCTTableAdapter
        '
        Me.PRODUCTTableAdapter.ClearBeforeFill = True
        '
        'SALESMANTableAdapter
        '
        Me.SALESMANTableAdapter.ClearBeforeFill = True
        '
        'SALE_ORDER_DETAILBindingSource
        '
        Me.SALE_ORDER_DETAILBindingSource.DataMember = "SALE_ORDER_DETAIL"
        Me.SALE_ORDER_DETAILBindingSource.DataSource = Me.DS_SALE_ORDER
        '
        'SALE_ORDER_DETAILTableAdapter
        '
        Me.SALE_ORDER_DETAILTableAdapter.ClearBeforeFill = True
        '
        'SP_DETAIL_SALE_ORDERBindingSource
        '
        Me.SP_DETAIL_SALE_ORDERBindingSource.DataMember = "SP_DETAIL_SALE_ORDER"
        Me.SP_DETAIL_SALE_ORDERBindingSource.DataSource = Me.DS_SALE_ORDER
        '
        'SP_DETAIL_SALE_ORDERTableAdapter
        '
        Me.SP_DETAIL_SALE_ORDERTableAdapter.ClearBeforeFill = True
        '
        'frmEntrySaleOrder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(784, 538)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmEntrySaleOrder"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Entry Sale Order"
        CType(Me.SALE_ORDER_HEADERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_SALE_ORDER, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CUSTOMERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSaleOrder, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.SALESMANBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SALE_ORDER_DETAILBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_DETAIL_SALE_ORDERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DS_SALE_ORDER As DMI_RETAIL_SALE.DS_SALE_ORDER
    Friend WithEvents SALE_ORDER_HEADERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SALE_ORDER_HEADERTableAdapter As DMI_RETAIL_SALE.DS_SALE_ORDERTableAdapters.SALE_ORDER_HEADERTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_SALE.DS_SALE_ORDERTableAdapters.TableAdapterManager
    Friend WithEvents SALE_ORDER_NUMBERTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALE_ORDER_DATEDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents DELIVERY_REQUEST_DATEDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents SHIP_TOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CUSTOMERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CUSTOMERTableAdapter As DMI_RETAIL_SALE.DS_SALE_ORDERTableAdapters.CUSTOMERTableAdapter
    Friend WithEvents CUSTOMER_NAMEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents lblSaleOrderNo As System.Windows.Forms.Label
    Friend WithEvents lblSaleOrderDate As System.Windows.Forms.Label
    Friend WithEvents lblDelRequestDate As System.Windows.Forms.Label
    Friend WithEvents lblShip As System.Windows.Forms.Label
    Friend WithEvents lblCustomer As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvSaleOrder As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdSearchName As System.Windows.Forms.PictureBox
    Friend WithEvents lblDel As System.Windows.Forms.Label
    Friend WithEvents lblF6 As System.Windows.Forms.Label
    Friend WithEvents lblF5 As System.Windows.Forms.Label
    Friend WithEvents AMOUNT_DISCOUNTTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GRAND_TOTALTextBox As System.Windows.Forms.TextBox
    Friend WithEvents lblDiscount As System.Windows.Forms.Label
    Friend WithEvents lblGrandTotal As System.Windows.Forms.Label
    Friend WithEvents lblSalesman As System.Windows.Forms.Label
    Friend WithEvents TAXPercentageTEXTBOX As System.Windows.Forms.TextBox
    Friend WithEvents TAX_AMOUNTTextBox As System.Windows.Forms.TextBox
    Friend WithEvents lblTax As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents cmdPrint As System.Windows.Forms.Button
    Friend WithEvents PRODUCTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PRODUCTTableAdapter As DMI_RETAIL_SALE.DS_SALE_ORDERTableAdapters.PRODUCTTableAdapter
    Friend WithEvents lblNoOfItem As System.Windows.Forms.Label
    Friend WithEvents SALESMANBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SALESMANTableAdapter As DMI_RETAIL_SALE.DS_SALE_ORDERTableAdapters.SALESMANTableAdapter
    Friend WithEvents SALESMAN_IDComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents SALE_ORDER_DETAILBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SALE_ORDER_DETAILTableAdapter As DMI_RETAIL_SALE.DS_SALE_ORDERTableAdapters.SALE_ORDER_DETAILTableAdapter
    Friend WithEvents SP_DETAIL_SALE_ORDERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_DETAIL_SALE_ORDERTableAdapter As DMI_RETAIL_SALE.DS_SALE_ORDERTableAdapters.SP_DETAIL_SALE_ORDERTableAdapter
    Friend WithEvents PRODUCT_CODE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRODUCT_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QUANTITY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRICE_PER_UNIT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DISCOUNT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DISCOUNT2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STATUS As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents DELETE As System.Windows.Forms.DataGridViewImageColumn
End Class
