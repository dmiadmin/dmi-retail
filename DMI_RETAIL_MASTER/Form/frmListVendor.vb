﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frmListVendor
    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Dim tmpVendorId As Integer

    Private Sub frmListVendor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_VENDOR.SP_LIST_VENDOR' table. You can move, or remove it, as needed.
        'Me.SP_LIST_VENDORTableAdapter.Fill(Me.DS_VENDOR.SP_LIST_VENDOR)
        Dim xDS_Vendor As New DataSet
        With xComm
            .Connection = xConn
            .CommandText = "SELECT VENDOR_ID," & _
            " VENDOR_CODE," & _
            " VENDOR_NAME," & _
            " VENDOR_ADDRESS," & _
            " VENDOR_PHONE," & _
            " VENDOR_FAX," & _
            " VENDOR_CITY," & _
            " VENDOR_CONTACT_PERSON," & _
            " EFFECTIVE_START_DATE," & _
            " EFFECTIVE_END_DATE" & _
            " FROM VENDOR" & _
            " ORDER BY VENDOR_NAME "
            .CommandType = CommandType.Text
        End With
        xAdoAdapter.SelectCommand = xComm
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        Me.xAdoAdapter.Fill(xDS_Vendor, "VENDOR")
        Me.dgvListVendor.DataSource = xDS_Vendor.Tables("VENDOR")

        Label2.Visible = True
        If Language = "Indonesian" Then
            Label1.Text = "Nama Supplier"
            Me.Text = "Daftar Supplier"
            CheckBox1.Text = "Aktif saja"
            dgvListVendor.Columns("VENDOR_ID").Visible = False
            dgvListVendor.Columns("EFFECTIVE_START_DATE").Visible = False
            dgvListVendor.Columns("EFFECTIVE_END_DATE").Visible = False

            dgvListVendor.Columns("VENDOR_ADDRESS").HeaderText = "Alamat"
            dgvListVendor.Columns("VENDOR_CITY").HeaderText = "Kota"
            dgvListVendor.Columns("VENDOR_CODE").HeaderText = "Kode Vendor"
            dgvListVendor.Columns("VENDOR_NAME").HeaderText = "Name Supplier"
            dgvListVendor.Columns("VENDOR_FAX").HeaderText = "Fax"
            dgvListVendor.Columns("VENDOR_PHONE").HeaderText = "Telp."
            dgvListVendor.Columns("VENDOR_CONTACT_PERSON").HeaderText = "Contact Person"
            Label2.Text = "Jumlah data : " & dgvListVendor.RowCount
        Else
            dgvListVendor.Columns("VENDOR_ID").Visible = False
            dgvListVendor.Columns("EFFECTIVE_START_DATE").Visible = False
            dgvListVendor.Columns("EFFECTIVE_END_DATE").Visible = False

            dgvListVendor.Columns("VENDOR_ADDRESS").HeaderText = "Address"
            dgvListVendor.Columns("VENDOR_CITY").HeaderText = "City"
            dgvListVendor.Columns("VENDOR_CODE").HeaderText = "Vendor Code"
            dgvListVendor.Columns("VENDOR_NAME").HeaderText = "Vendor Name"
            dgvListVendor.Columns("VENDOR_FAX").HeaderText = "Fax"
            dgvListVendor.Columns("VENDOR_PHONE").HeaderText = "Phone"
            dgvListVendor.Columns("VENDOR_CONTACT_PERSON").HeaderText = "Contact Person"
            Label2.Text = "No of data : " & dgvListVendor.RowCount
        End If

    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        If CheckBox1.Checked = True Then
            Dim xDS_Vendor As New DataSet
            With xComm
                .Connection = xConn
                .CommandText = "SELECT VENDOR_ID," & _
                " VENDOR_CODE," & _
                " VENDOR_NAME," & _
                " VENDOR_ADDRESS," & _
                " VENDOR_PHONE," & _
                " VENDOR_FAX," & _
                " VENDOR_CITY," & _
                " VENDOR_CONTACT_PERSON," & _
                " EFFECTIVE_START_DATE," & _
                " EFFECTIVE_END_DATE" & _
                " FROM VENDOR" & _
                " WHERE VENDOR_NAME LIKE '%" & TextBox1.Text.ToUpper & "%'" & _
                " AND EFFECTIVE_END_DATE > GETDATE()" & _
                " ORDER BY VENDOR_NAME "
                .CommandType = CommandType.Text
            End With
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            Me.xAdoAdapter.Fill(xDS_Vendor, "VENDOR")
            Me.dgvListVendor.DataSource = xDS_Vendor.Tables("VENDOR")
        Else
            Dim xDS_Vendor As New DataSet
            With xComm
                .Connection = xConn
                .CommandText = "SELECT VENDOR_ID," & _
                " VENDOR_CODE," & _
                " VENDOR_NAME," & _
                " VENDOR_ADDRESS," & _
                " VENDOR_PHONE," & _
                " VENDOR_FAX," & _
                " VENDOR_CITY," & _
                " VENDOR_CONTACT_PERSON," & _
                " EFFECTIVE_START_DATE," & _
                " EFFECTIVE_END_DATE" & _
                " FROM VENDOR" & _
                " WHERE VENDOR_NAME LIKE '%" & TextBox1.Text.ToUpper & "%'" & _
                " ORDER BY VENDOR_NAME "
                .CommandType = CommandType.Text
            End With
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            Me.xAdoAdapter.Fill(xDS_Vendor, "VENDOR")
            Me.dgvListVendor.DataSource = xDS_Vendor.Tables("VENDOR")
        End If

        If Language = "Indonesian" Then
            Label2.Text = "Jumlah data : " & dgvListVendor.RowCount
        Else
            Label2.Text = "No Of data : " & dgvListVendor.RowCount
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            Dim xDS_Vendor As New DataSet
            With xComm
                .Connection = xConn
                .CommandText = "SELECT VENDOR_ID," & _
                " VENDOR_CODE," & _
                " VENDOR_NAME," & _
                " VENDOR_ADDRESS," & _
                " VENDOR_PHONE," & _
                " VENDOR_FAX," & _
                " VENDOR_CITY," & _
                " VENDOR_CONTACT_PERSON," & _
                " EFFECTIVE_START_DATE," & _
                " EFFECTIVE_END_DATE" & _
                " FROM VENDOR" & _
                " WHERE VENDOR_NAME LIKE '%" & TextBox1.Text.ToUpper & "%'" & _
                " AND EFFECTIVE_END_DATE > GETDATE()" & _
                " ORDER BY VENDOR_NAME "
                .CommandType = CommandType.Text
            End With
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            Me.xAdoAdapter.Fill(xDS_Vendor, "VENDOR")
            Me.dgvListVendor.DataSource = xDS_Vendor.Tables("VENDOR")
        Else
            Dim xDS_Vendor As New DataSet
            With xComm
                .Connection = xConn
                .CommandText = "SELECT VENDOR_ID," & _
                " VENDOR_CODE," & _
                " VENDOR_NAME," & _
                " VENDOR_ADDRESS," & _
                " VENDOR_PHONE," & _
                " VENDOR_FAX," & _
                " VENDOR_CITY," & _
                " VENDOR_CONTACT_PERSON," & _
                " EFFECTIVE_START_DATE," & _
                " EFFECTIVE_END_DATE" & _
                " FROM VENDOR" & _
                " WHERE VENDOR_NAME LIKE '%" & TextBox1.Text.ToUpper & "%'" & _
                " ORDER BY VENDOR_NAME "
                .CommandType = CommandType.Text
            End With
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            Me.xAdoAdapter.Fill(xDS_Vendor, "VENDOR")
            Me.dgvListVendor.DataSource = xDS_Vendor.Tables("VENDOR")
        End If

        If Language = "Indonesian" Then
            Label2.Text = "Jumlah data : " & dgvListVendor.RowCount
        Else
            Label2.Text = "No of data : " & dgvListVendor.RowCount
        End If

    End Sub

    Private Sub dgvListVendor_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListVendor.CellDoubleClick
        Try
            tmpVendorId = dgvListVendor.Item("VENDOR_ID", e.RowIndex).Value
            frmEntryVendor.MdiParent = ObjFormMain
            frmEntryVendor.Show()
            frmEntryVendor.BringToFront()
            tmpForm = frmEntryVendor
            tmpRecent = New frmEntryVendor
            frmEntryVendor.VENDORBindingSource.Position = frmEntryVendor.VENDORBindingSource.Find("VENDOR_ID", tmpVendorId)
            Me.Close()
        Catch ex As Exception

        End Try
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        AccessPrivilege(Me.Tag)
    End Sub
End Class