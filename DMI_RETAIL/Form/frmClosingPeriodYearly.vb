﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Data

Public Class frmClosingPeriodYearly
    Dim tmpdate, tmpnow, tmpdate1 As Date
    Dim strFile As String
    Dim xConn As New SqlConnection
    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Dim xReader As  SqlDataReader

    Private Sub connection()
        xConn.Close()
        xConn.ConnectionString = "Data Source=" & ServerName & ";Initial Catalog=" & DatabaseName & ";Persist Security Info=True;User ID=sa;Password=SA;"
        xConn.Open()
    End Sub
    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        tmpnow = DateSerial(DateTimePicker1.Value.Year, DateTimePicker1.Value.Month, 1)
        tmpdate = DateSerial(tmpnow.Year, _
                           tmpnow.Month + 1, _
                           tmpnow.Day - 1)
        tmpdate1 = DateSerial(tmpnow.Year, _
                             1, _
                             1)


        If Language = "Indonesian" Then
            If MsgBox("Anda yakin ingin menutup Periode " & Format(DateTimePicker1.Value, "MMMM yyyy") & " ?" & vbCrLf & _
               "Setelah Tutup Buku, semua transaksi di bawah periode " & _
               Format(DateTimePicker1.Value, "MMMM yyyy") & " tidak dapat dilakukan!", _
               MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub

            'If CLOSED_PERIOD_YEARLYTableAdapter.SP_CLOSED_PERIOD_PROCESS("G", DateTimePicker1.Value) = "TRUE" Then
            '    MsgBox("Tutup Buku sedang dilakukan oleh User lain!" & vbCrLf & _
            '            "Mohon tunggu hingga proses selesai!", MsgBoxStyle.Critical, "DMI Retail")
            '    Exit Sub
            'End If

        Else

            If MsgBox("Are You sure to Close " & Format(DateTimePicker1.Value, "MMMM yyyy") & " Period?" & vbCrLf & _
               "After closing this period, all the changes of period before " & _
               Format(DateTimePicker1.Value, "MMMM yyyy") & " will not allowed!", _
               MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub

            'If CLOSED_PERIOD_YEARLYTableAdapter.SP_CLOSED_PERIOD_PROCESS("G", DateTimePicker1.Value) = "TRUE" Then
            '    MsgBox("Period is being closed by another user!" & vbCrLf & _
            '            "Please wait until this process commited!", MsgBoxStyle.Critical, "DMI Retail")
            '    Exit Sub
            'End If
        End If
        
        Dim tmpCheck As DateTime = DateTimePicker1.Value.Year & "/" & _
                             DateTimePicker1.Value.Month
        'If CLOSED_PERIODTableAdapter.SP_CLOSED_PERIOD_STOP() <> tmpCheck Then

        '    ''INSERT INTO TABLE CLOSED_PERIOD
        '    CLOSED_PERIODTableAdapter.SP_CLOSED_PERIOD("I", _
        '                                               0, _
        '                                               tmpnow, _
        '                                               USER_ID, _
        '                                               Now, _
        '                                               0, _
        '                                               DateSerial(4000, 12, 31))


        '    CLOSED_PERIODTableAdapter.SP_CLOSED_PERIOD_PROCESS("C", DateTimePicker1.Value)

        '    CLOSED_PERIODTableAdapter.SP_ENDING_STOCK(tmpdate, _
        '                                              mdlGeneral.USER_ID, _
        '                                              Now, _
        '                                              0, _
        '                                              DateSerial(4000, 12, 31))
        'End If

        ''INSERT INTO TABLE CLOSED_PEROD_YEARLY
        'CLOSED_PERIOD_YEARLYTableAdapter.SP_CLOSING_PERIOD_YEARLY("I", _
        '                                                             0, _
        '                                                             tmpdate1, _
        '                                                             USER_ID, _
        '                                                             Now, _
        '                                                             0, _
        '                                                             DateSerial(4000, 12, 31))

        'CLOSED_PERIODTableAdapter.SP_CLOSED_PERIOD_PROCESS("C", DateTimePicker1.Value)

        ' '' for call savefiledialog back up
        'SaveFileDialog1.Filter = "Database Backup File|*.bak"
        'SaveFileDialog1.FileName = ""
        'SaveFileDialog1.ShowDialog()
        'strFile = SaveFileDialog1.FileName

        ' '' for back up
        'Dim tmpDBName As String
        'tmpDBName = File.ReadAllText(Application.StartupPath & "\DB_NAME.txt")

        'Me.Cursor = Cursors.WaitCursor

        'Try
        '    Dim sql As String
        '    sql = ("BACKUP DATABASE [" & tmpDBName & "] TO DISK = N'" & strFile & _
        '             "' WITH NOFORMAT, NOINIT,  NAME = N'Full Database Backup', " & _
        '             "SKIP, NOREWIND, NOUNLOAD,  STATS = 10")
        '    xComm = New SqlCommand(sql, xConn)
        '    xComm.ExecuteNonQuery()
        'Catch
        '    If Language = "Indonesian" Then
        '        MsgBox("Backup Data Gagal.", MsgBoxStyle.Information, "DMI Retail")
        '    Else
        '        MsgBox("Backup Data Failed.", MsgBoxStyle.Information, "DMI Retail")
        '    End If
        'End Try

        'Me.Cursor = Cursors.Default

        Timer1.Enabled = True

        ''DELETE ALL TRANSACTION
        'CLOSED_PERIODTableAdapter.SP_DELETE_TRANSACTION()


    End Sub

    Private Sub frmClosingPeriodYearly_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Language = "Indonesian" Then
            Me.Text = "Tutup Buku - Tahun"
            cmdSave.Text = "&Proses..!!"
        End If

    End Sub

    Private Sub CLOSED_PERIODBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.CLOSED_PERIODBindingSource.EndEdit()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        ProgressBar1.Value += 2

        If ProgressBar1.Value <= 50 Then
            If Language = "Indonesian" Then
                Label1.Text = "Dalam Proses ...!"
            Else
                Label1.Text = "Loading...!"
            End If

        Else
            If Language = "Indonesian" Then
                Label1.Text = "Mohon tunggu ...!"
            Else
                Label1.Text = "Please Wait...!"
            End If
        End If


        If ProgressBar1.Value = 100 Then
            Timer1.Enabled = False

            If Language = "Indonesian" Then
                MsgBox("Tutup Buku Tahunan berhasil.", MsgBoxStyle.Information, "DMI Retail")
            Else
                MsgBox("Closing Period Yearly Successful.", MsgBoxStyle.Information, "DMI Retail")
            End If

            Me.Close()
        End If
    End Sub

End Class