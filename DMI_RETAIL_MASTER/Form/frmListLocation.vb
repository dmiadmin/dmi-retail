﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frmListLocation
    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter

    Private Sub LOCATIONBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Me.Validate()
        'Me.LOCATIONBindingSource.EndEdit()
        'Me.TableAdapterManager.UpdateAll(Me.DS_LOCATION)
    End Sub

    Private Sub frmListLocation_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F12 Then
            cmdReport_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub frmListLocation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim xDS_LOCATION As New DataSet
        With xComm
            .Connection = xConn
            .CommandText = "SELECT LOCATION_ID, LOCATION_NAME , [DESCRIPTION] ,EFFECTIVE_END_DATE ,EFFECTIVE_START_DATE  FROM LOCATION"
            .CommandType = CommandType.Text
        End With
        xAdoAdapter.SelectCommand = xComm
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        Me.xAdoAdapter.Fill(xDS_LOCATION, "LOCATION")
        Me.dgvListLocation.DataSource = xDS_LOCATION.Tables("LOCATION")

        Label2.Visible = True
        If Language = "Indonesian" Then
            Me.Text = "Daftar Lokasi"
            Label1.Text = "Nama Lokasi"
            cmdReport.Text = "Cetak"
            CheckBox1.Text = "Tidak Terhapus"
            dgvListLocation.Columns("LOCATION_ID").Visible = False
            dgvListLocation.Columns("EFFECTIVE_START_DATE").Visible = False
            dgvListLocation.Columns("EFFECTIVE_END_DATE").Visible = False
            dgvListLocation.Columns("LOCATION_NAME").HeaderText = "Nama Lokasi"
            dgvListLocation.Columns("LOCATION_NAME").Width = 150
            dgvListLocation.Columns("DESCRIPTION").HeaderText = "Keterangan"
            dgvListLocation.Columns("DESCRIPTION").Width = 150
            Label2.Text = "Jumlah Barang : " & dgvListLocation.RowCount
        Else
            dgvListLocation.Columns("LOCATION_ID").Visible = False
            dgvListLocation.Columns("EFFECTIVE_START_DATE").Visible = False
            dgvListLocation.Columns("EFFECTIVE_END_DATE").Visible = False
            dgvListLocation.Columns("LOCATION_NAME").HeaderText = "Location Name"
            dgvListLocation.Columns("LOCATION_NAME").Width = 150
            dgvListLocation.Columns("DESCRIPTION").HeaderText = "Description"
            dgvListLocation.Columns("DESCRIPTION").Width = 150
            Label2.Text = "No Of Item : " & dgvListLocation.RowCount
        End If

    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        If CheckBox1.Checked = True Then
            Dim xDS_LOCATION As New DataSet
            With xComm
                .Connection = xConn
                .CommandText = "SELECT LOCATION_ID, LOCATION_NAME , [DESCRIPTION] ,EFFECTIVE_END_DATE ,EFFECTIVE_START_DATE  FROM LOCATION WHERE LOCATION_NAME LIKE '%" & TextBox1.Text.ToUpper & "%' AND EFFECTIVE_END_DATE > GETDATE()"
                .CommandType = CommandType.Text
            End With
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            Me.xAdoAdapter.Fill(xDS_LOCATION, "LOCATION")
            Me.dgvListLocation.DataSource = xDS_LOCATION.Tables("LOCATION")
        Else
            Dim xDS_LOCATION As New DataSet
            With xComm
                .Connection = xConn
                .CommandText = "SELECT LOCATION_ID, LOCATION_NAME , [DESCRIPTION] ,EFFECTIVE_END_DATE ,EFFECTIVE_START_DATE  FROM LOCATION WHERE LOCATION_NAME LIKE '%" & TextBox1.Text.ToUpper & "%'"
                .CommandType = CommandType.Text
            End With
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            Me.xAdoAdapter.Fill(xDS_LOCATION, "LOCATION")
            Me.dgvListLocation.DataSource = xDS_LOCATION.Tables("LOCATION")
        End If

        If Language = "Indonesian" Then
            Label2.Text = "Jumlah Barang : " & dgvListLocation.RowCount
        Else
            Label2.Text = "No Of Item : " & dgvListLocation.RowCount
        End If
    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        Dim RPV As New frmRepLocation
        RPV.WindowState = FormWindowState.Maximized
        RPV.Show()
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            Dim xDS_LOCATION As New DataSet
            With xComm
                .Connection = xConn
                .CommandText = "SELECT LOCATION_ID, LOCATION_NAME , [DESCRIPTION] ,EFFECTIVE_END_DATE ,EFFECTIVE_START_DATE  FROM LOCATION WHERE LOCATION_NAME LIKE '%" & TextBox1.Text.ToUpper & "%' AND EFFECTIVE_END_DATE > GETDATE()"
                .CommandType = CommandType.Text
            End With
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            Me.xAdoAdapter.Fill(xDS_LOCATION, "LOCATION")
            Me.dgvListLocation.DataSource = xDS_LOCATION.Tables("LOCATION")
        Else
            Dim xDS_LOCATION As New DataSet
            With xComm
                .Connection = xConn
                .CommandText = "SELECT LOCATION_ID, LOCATION_NAME , [DESCRIPTION] ,EFFECTIVE_END_DATE ,EFFECTIVE_START_DATE  FROM LOCATION WHERE LOCATION_NAME LIKE '%" & TextBox1.Text.ToUpper & "%'"
                .CommandType = CommandType.Text
            End With
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            Me.xAdoAdapter.Fill(xDS_LOCATION, "LOCATION")
            Me.dgvListLocation.DataSource = xDS_LOCATION.Tables("LOCATION")
        End If
        If Language = "Indonesian" Then
            Label2.Text = "Jumlah Lokasi : " & dgvListLocation.RowCount
        Else
            Label2.Text = "No Of Location : " & dgvListLocation.RowCount
        End If

    End Sub

    Private Sub dgvListLocation_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListLocation.CellDoubleClick
        Try
            Dim tmpLocationId As Integer = dgvListLocation.Item("LOCATION_ID", e.RowIndex).Value
            frmEntryLocation.MdiParent = ObjFormMain
            frmEntryLocation.Show()
            frmEntryLocation.BringToFront()
            tmpForm = frmEntryLocation
            tmpRecent = New frmEntryLocation
            frmEntryLocation.LOCATIONBindingSource.Position = frmEntryLocation.LOCATIONBindingSource.Find("LOCATION_ID", tmpLocationId)
            Me.Close()
        Catch ex As Exception

        End Try
       
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        AccessPrivilege(Me.Tag)
    End Sub
End Class