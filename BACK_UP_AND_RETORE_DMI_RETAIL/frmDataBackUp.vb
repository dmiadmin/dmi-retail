﻿Imports System.IO

Public Class frmDataBackUp
    Dim tmpConfig(), language As String
    'Dim Path_Name As String = Application.StartupPath
    'Dim Path_Data As String = Replace(Path_Name, Mid(Path_Name, 51, 71), "")
    'Dim PathDb As String = Path_Data & "\DMI_RETAIL\bin\Debug\DB_DMI_RETAIL.mdf"
    Dim pathConfig As String = Application.StartupPath & "\config.inf"
   
    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        SaveFileDialog1.Filter = "Database Backup File|*.mdf"
        SaveFileDialog1.FileName = ""
        SaveFileDialog1.ShowDialog()
        txtPathBackup.Text = SaveFileDialog1.FileName
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        Me.Dispose()
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
       
        Try
            System.IO.File.Copy(Application.StartupPath & "\DB_DMI_RETAIL.mdf", _
                           txtPathBackup.Text)
            If language = "Indonesian" Then
                MsgBox("Backup Data Berhasil.", MsgBoxStyle.Information, "DMI Retail")
            Else
                MsgBox("Backup Data Successed.", MsgBoxStyle.Information, "DMI Retail")
            End If
            Me.Close()
        Catch ex As Exception
            If language = "Indonesian" Then
                MsgBox("Backup Data Gagal.", MsgBoxStyle.Information, "DMI Retail")
            Else
                MsgBox("Backup Data Failed.", MsgBoxStyle.Information, "DMI Retail")
            End If
        End Try

    End Sub

    Private Sub frmDataBackUp_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If File.Exists(pathConfig) Then
            tmpConfig = File.ReadAllLines(pathConfig)
            For x As Integer = 0 To tmpConfig.Count - 1
                If tmpConfig(x).StartsWith("[Language]") Then
                    language = tmpConfig(x).Split("=")(1)
                End If
            Next
        End If
    End Sub

End Class
