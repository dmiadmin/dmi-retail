﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEntryOpeningBalance
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEntryOpeningBalance))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtOpeningBalanceNo = New System.Windows.Forms.TextBox()
        Me.lblOpeningBalanceNo = New System.Windows.Forms.Label()
        Me.lblWarehouse = New System.Windows.Forms.Label()
        Me.lblPrice = New System.Windows.Forms.Label()
        Me.lblQuantity = New System.Windows.Forms.Label()
        Me.lblProduct = New System.Windows.Forms.Label()
        Me.lblPeriod = New System.Windows.Forms.Label()
        Me.txtDTP = New System.Windows.Forms.TextBox()
        Me.dtpPeriod = New System.Windows.Forms.DateTimePicker()
        Me.PRODUCT_IDTextBox = New System.Windows.Forms.TextBox()
        Me.cmdSearchCode = New System.Windows.Forms.PictureBox()
        Me.PRODUCT_NAMETextBox = New System.Windows.Forms.TextBox()
        Me.QUANTITYTextBox = New System.Windows.Forms.TextBox()
        Me.PRICETextBox = New System.Windows.Forms.TextBox()
        Me.WAREHOUSE_IDComboBox = New System.Windows.Forms.ComboBox()
        Me.OPENING_BALANCEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_OPENING_BALANCE = New DMI_RETAIL_INVENTORY.DS_OPENING_BALANCE()
        Me.WAREHOUSEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PRODUCTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.OPENING_BALANCETableAdapter = New DMI_RETAIL_INVENTORY.DS_OPENING_BALANCETableAdapters.OPENING_BALANCETableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_INVENTORY.DS_OPENING_BALANCETableAdapters.TableAdapterManager()
        Me.PRODUCTTableAdapter = New DMI_RETAIL_INVENTORY.DS_OPENING_BALANCETableAdapters.PRODUCTTableAdapter()
        Me.WAREHOUSETableAdapter = New DMI_RETAIL_INVENTORY.DS_OPENING_BALANCETableAdapters.WAREHOUSETableAdapter()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.DS_PRODUCT_WAREHOUSE = New DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSE()
        Me.PRODUCT_WAREHOUSEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PRODUCT_WAREHOUSETableAdapter = New DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSETableAdapters.PRODUCT_WAREHOUSETableAdapter()
        Me.TableAdapterManager1 = New DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSETableAdapters.TableAdapterManager()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cmdSearchCode, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OPENING_BALANCEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_OPENING_BALANCE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DS_PRODUCT_WAREHOUSE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PRODUCT_WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtOpeningBalanceNo)
        Me.GroupBox1.Controls.Add(Me.lblOpeningBalanceNo)
        Me.GroupBox1.Controls.Add(Me.lblWarehouse)
        Me.GroupBox1.Controls.Add(Me.lblPrice)
        Me.GroupBox1.Controls.Add(Me.lblQuantity)
        Me.GroupBox1.Controls.Add(Me.lblProduct)
        Me.GroupBox1.Controls.Add(Me.lblPeriod)
        Me.GroupBox1.Controls.Add(Me.txtDTP)
        Me.GroupBox1.Controls.Add(Me.dtpPeriod)
        Me.GroupBox1.Controls.Add(Me.PRODUCT_IDTextBox)
        Me.GroupBox1.Controls.Add(Me.cmdSearchCode)
        Me.GroupBox1.Controls.Add(Me.PRODUCT_NAMETextBox)
        Me.GroupBox1.Controls.Add(Me.QUANTITYTextBox)
        Me.GroupBox1.Controls.Add(Me.PRICETextBox)
        Me.GroupBox1.Controls.Add(Me.WAREHOUSE_IDComboBox)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 13)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(516, 199)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'txtOpeningBalanceNo
        '
        Me.txtOpeningBalanceNo.Location = New System.Drawing.Point(145, 17)
        Me.txtOpeningBalanceNo.Name = "txtOpeningBalanceNo"
        Me.txtOpeningBalanceNo.Size = New System.Drawing.Size(121, 22)
        Me.txtOpeningBalanceNo.TabIndex = 1
        '
        'lblOpeningBalanceNo
        '
        Me.lblOpeningBalanceNo.AutoSize = True
        Me.lblOpeningBalanceNo.Location = New System.Drawing.Point(7, 20)
        Me.lblOpeningBalanceNo.Name = "lblOpeningBalanceNo"
        Me.lblOpeningBalanceNo.Size = New System.Drawing.Size(128, 15)
        Me.lblOpeningBalanceNo.TabIndex = 30
        Me.lblOpeningBalanceNo.Text = "Opening Balance. No"
        '
        'lblWarehouse
        '
        Me.lblWarehouse.AutoSize = True
        Me.lblWarehouse.Location = New System.Drawing.Point(7, 162)
        Me.lblWarehouse.Name = "lblWarehouse"
        Me.lblWarehouse.Size = New System.Drawing.Size(73, 15)
        Me.lblWarehouse.TabIndex = 29
        Me.lblWarehouse.Text = "Warehouse"
        '
        'lblPrice
        '
        Me.lblPrice.AutoSize = True
        Me.lblPrice.Location = New System.Drawing.Point(7, 134)
        Me.lblPrice.Name = "lblPrice"
        Me.lblPrice.Size = New System.Drawing.Size(35, 15)
        Me.lblPrice.TabIndex = 28
        Me.lblPrice.Text = "Price"
        '
        'lblQuantity
        '
        Me.lblQuantity.AutoSize = True
        Me.lblQuantity.Location = New System.Drawing.Point(7, 106)
        Me.lblQuantity.Name = "lblQuantity"
        Me.lblQuantity.Size = New System.Drawing.Size(57, 15)
        Me.lblQuantity.TabIndex = 27
        Me.lblQuantity.Text = "Quantity"
        '
        'lblProduct
        '
        Me.lblProduct.AutoSize = True
        Me.lblProduct.Location = New System.Drawing.Point(7, 77)
        Me.lblProduct.Name = "lblProduct"
        Me.lblProduct.Size = New System.Drawing.Size(53, 15)
        Me.lblProduct.TabIndex = 26
        Me.lblProduct.Text = "Product"
        '
        'lblPeriod
        '
        Me.lblPeriod.AutoSize = True
        Me.lblPeriod.Location = New System.Drawing.Point(7, 49)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(44, 15)
        Me.lblPeriod.TabIndex = 25
        Me.lblPeriod.Text = "Period"
        '
        'txtDTP
        '
        Me.txtDTP.Location = New System.Drawing.Point(145, 46)
        Me.txtDTP.Name = "txtDTP"
        Me.txtDTP.Size = New System.Drawing.Size(107, 22)
        Me.txtDTP.TabIndex = 2
        Me.txtDTP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dtpPeriod
        '
        Me.dtpPeriod.CustomFormat = "dd-MMM-yyyy"
        Me.dtpPeriod.Enabled = False
        Me.dtpPeriod.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPeriod.Location = New System.Drawing.Point(145, 46)
        Me.dtpPeriod.Name = "dtpPeriod"
        Me.dtpPeriod.Size = New System.Drawing.Size(121, 22)
        Me.dtpPeriod.TabIndex = 5
        Me.dtpPeriod.Visible = False
        '
        'PRODUCT_IDTextBox
        '
        Me.PRODUCT_IDTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PRODUCT_IDTextBox.Location = New System.Drawing.Point(145, 74)
        Me.PRODUCT_IDTextBox.Name = "PRODUCT_IDTextBox"
        Me.PRODUCT_IDTextBox.Size = New System.Drawing.Size(121, 22)
        Me.PRODUCT_IDTextBox.TabIndex = 3
        Me.PRODUCT_IDTextBox.Tag = "M"
        '
        'cmdSearchCode
        '
        Me.cmdSearchCode.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchCode.Image = CType(resources.GetObject("cmdSearchCode.Image"), System.Drawing.Image)
        Me.cmdSearchCode.Location = New System.Drawing.Point(464, 73)
        Me.cmdSearchCode.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchCode.Name = "cmdSearchCode"
        Me.cmdSearchCode.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearchCode.TabIndex = 18
        Me.cmdSearchCode.TabStop = False
        '
        'PRODUCT_NAMETextBox
        '
        Me.PRODUCT_NAMETextBox.Location = New System.Drawing.Point(272, 74)
        Me.PRODUCT_NAMETextBox.Name = "PRODUCT_NAMETextBox"
        Me.PRODUCT_NAMETextBox.ReadOnly = True
        Me.PRODUCT_NAMETextBox.Size = New System.Drawing.Size(185, 22)
        Me.PRODUCT_NAMETextBox.TabIndex = 4
        Me.PRODUCT_NAMETextBox.TabStop = False
        '
        'QUANTITYTextBox
        '
        Me.QUANTITYTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.QUANTITYTextBox.Location = New System.Drawing.Point(145, 103)
        Me.QUANTITYTextBox.MaxLength = 5
        Me.QUANTITYTextBox.Name = "QUANTITYTextBox"
        Me.QUANTITYTextBox.Size = New System.Drawing.Size(62, 22)
        Me.QUANTITYTextBox.TabIndex = 5
        Me.QUANTITYTextBox.Tag = "M"
        Me.QUANTITYTextBox.Text = "0"
        Me.QUANTITYTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'PRICETextBox
        '
        Me.PRICETextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PRICETextBox.Location = New System.Drawing.Point(145, 131)
        Me.PRICETextBox.MaxLength = 9
        Me.PRICETextBox.Name = "PRICETextBox"
        Me.PRICETextBox.Size = New System.Drawing.Size(121, 22)
        Me.PRICETextBox.TabIndex = 6
        Me.PRICETextBox.Tag = "M"
        Me.PRICETextBox.Text = "0"
        Me.PRICETextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'WAREHOUSE_IDComboBox
        '
        Me.WAREHOUSE_IDComboBox.BackColor = System.Drawing.Color.White
        Me.WAREHOUSE_IDComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.OPENING_BALANCEBindingSource, "WAREHOUSE_ID", True))
        Me.WAREHOUSE_IDComboBox.DataSource = Me.WAREHOUSEBindingSource
        Me.WAREHOUSE_IDComboBox.DisplayMember = "WAREHOUSE_NAME"
        Me.WAREHOUSE_IDComboBox.FormattingEnabled = True
        Me.WAREHOUSE_IDComboBox.Location = New System.Drawing.Point(145, 159)
        Me.WAREHOUSE_IDComboBox.Name = "WAREHOUSE_IDComboBox"
        Me.WAREHOUSE_IDComboBox.Size = New System.Drawing.Size(121, 23)
        Me.WAREHOUSE_IDComboBox.TabIndex = 7
        Me.WAREHOUSE_IDComboBox.Tag = ""
        Me.WAREHOUSE_IDComboBox.ValueMember = "WAREHOUSE_ID"
        '
        'OPENING_BALANCEBindingSource
        '
        Me.OPENING_BALANCEBindingSource.DataMember = "OPENING_BALANCE"
        Me.OPENING_BALANCEBindingSource.DataSource = Me.DS_OPENING_BALANCE
        '
        'DS_OPENING_BALANCE
        '
        Me.DS_OPENING_BALANCE.DataSetName = "DS_OPENING_BALANCE"
        Me.DS_OPENING_BALANCE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'WAREHOUSEBindingSource
        '
        Me.WAREHOUSEBindingSource.DataMember = "WAREHOUSE"
        Me.WAREHOUSEBindingSource.DataSource = Me.DS_OPENING_BALANCE
        '
        'PRODUCTBindingSource
        '
        Me.PRODUCTBindingSource.DataMember = "PRODUCT"
        Me.PRODUCTBindingSource.DataSource = Me.DS_OPENING_BALANCE
        '
        'OPENING_BALANCETableAdapter
        '
        Me.OPENING_BALANCETableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.OPENING_BALANCETableAdapter = Me.OPENING_BALANCETableAdapter
        Me.TableAdapterManager.PRODUCTTableAdapter = Me.PRODUCTTableAdapter
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_INVENTORY.DS_OPENING_BALANCETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.WAREHOUSETableAdapter = Me.WAREHOUSETableAdapter
        '
        'PRODUCTTableAdapter
        '
        Me.PRODUCTTableAdapter.ClearBeforeFill = True
        '
        'WAREHOUSETableAdapter
        '
        Me.WAREHOUSETableAdapter.ClearBeforeFill = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdUndo)
        Me.GroupBox2.Controls.Add(Me.cmdSave)
        Me.GroupBox2.Controls.Add(Me.cmdEdit)
        Me.GroupBox2.Controls.Add(Me.cmdDelete)
        Me.GroupBox2.Controls.Add(Me.cmdAdd)
        Me.GroupBox2.Location = New System.Drawing.Point(13, 217)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Size = New System.Drawing.Size(515, 55)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'cmdUndo
        '
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(221, 17)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(74, 29)
        Me.cmdUndo.TabIndex = 10
        Me.cmdUndo.Text = "&Undo"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSave.Location = New System.Drawing.Point(318, 17)
        Me.cmdSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(86, 29)
        Me.cmdSave.TabIndex = 11
        Me.cmdSave.Text = "&Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(123, 17)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(74, 29)
        Me.cmdEdit.TabIndex = 9
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(425, 17)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(74, 29)
        Me.cmdDelete.TabIndex = 12
        Me.cmdDelete.Text = "&Delete"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(14, 17)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(83, 29)
        Me.cmdAdd.TabIndex = 8
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'DS_PRODUCT_WAREHOUSE
        '
        Me.DS_PRODUCT_WAREHOUSE.DataSetName = "DS_PRODUCT_WAREHOUSE"
        Me.DS_PRODUCT_WAREHOUSE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PRODUCT_WAREHOUSEBindingSource
        '
        Me.PRODUCT_WAREHOUSEBindingSource.DataMember = "PRODUCT_WAREHOUSE"
        Me.PRODUCT_WAREHOUSEBindingSource.DataSource = Me.DS_PRODUCT_WAREHOUSE
        '
        'PRODUCT_WAREHOUSETableAdapter
        '
        Me.PRODUCT_WAREHOUSETableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager1
        '
        Me.TableAdapterManager1.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager1.PRODUCT_WAREHOUSETableAdapter = Me.PRODUCT_WAREHOUSETableAdapter
        Me.TableAdapterManager1.UpdateOrder = DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'frmEntryOpeningBalance
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(541, 281)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmEntryOpeningBalance"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Entry Opening Balance"
        Me.Text = "Entry Opening Balance"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cmdSearchCode, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OPENING_BALANCEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_OPENING_BALANCE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.DS_PRODUCT_WAREHOUSE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PRODUCT_WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents DS_OPENING_BALANCE As DMI_RETAIL_INVENTORY.DS_OPENING_BALANCE
    Friend WithEvents OPENING_BALANCEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents OPENING_BALANCETableAdapter As DMI_RETAIL_INVENTORY.DS_OPENING_BALANCETableAdapters.OPENING_BALANCETableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_INVENTORY.DS_OPENING_BALANCETableAdapters.TableAdapterManager
    Friend WithEvents QUANTITYTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PRICETextBox As System.Windows.Forms.TextBox
    Friend WithEvents WAREHOUSE_IDComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents PRODUCTTableAdapter As DMI_RETAIL_INVENTORY.DS_OPENING_BALANCETableAdapters.PRODUCTTableAdapter
    Friend WithEvents PRODUCTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PRODUCT_NAMETextBox As System.Windows.Forms.TextBox
    Friend WithEvents WAREHOUSETableAdapter As DMI_RETAIL_INVENTORY.DS_OPENING_BALANCETableAdapters.WAREHOUSETableAdapter
    Friend WithEvents WAREHOUSEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents cmdSearchCode As System.Windows.Forms.PictureBox
    Friend WithEvents PRODUCT_IDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents dtpPeriod As System.Windows.Forms.DateTimePicker
    Friend WithEvents DS_PRODUCT_WAREHOUSE As DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSE
    Friend WithEvents PRODUCT_WAREHOUSEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PRODUCT_WAREHOUSETableAdapter As DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSETableAdapters.PRODUCT_WAREHOUSETableAdapter
    Friend WithEvents TableAdapterManager1 As DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSETableAdapters.TableAdapterManager
    Friend WithEvents txtDTP As System.Windows.Forms.TextBox
    Friend WithEvents lblWarehouse As System.Windows.Forms.Label
    Friend WithEvents lblPrice As System.Windows.Forms.Label
    Friend WithEvents lblQuantity As System.Windows.Forms.Label
    Friend WithEvents lblProduct As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lblOpeningBalanceNo As System.Windows.Forms.Label
    Friend WithEvents txtOpeningBalanceNo As System.Windows.Forms.TextBox
End Class
