﻿Public Class frmRepProfitLoss

    Private Sub frmRepProfitLoss_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DB_DMI_RETAILDataSetALL.FS_PROFITLOSS_STATEMENT1' table. You can move, or remove it, as needed.
        Me.FS_PROFITLOSS_STATEMENT1TableAdapter.Fill(Me.DB_DMI_RETAILDataSetALL.FS_PROFITLOSS_STATEMENT1, frmReportProfitLoss.dtpPeriod.Value)
        'TODO: This line of code loads data into the 'DB_DMI_RETAILDataSetALL.FS_PROFITLOSS_STATEMENT2' table. You can move, or remove it, as needed.
        Me.FS_PROFITLOSS_STATEMENT2TableAdapter.Fill(Me.DB_DMI_RETAILDataSetALL.FS_PROFITLOSS_STATEMENT2, frmReportProfitLoss.dtpPeriod.Value)
        'TODO: This line of code loads data into the 'DB_DMI_RETAILDataSetALL.FS_PROFITLOSS_STATEMENT3' table. You can move, or remove it, as needed.
        Me.FS_PROFITLOSS_STATEMENT3TableAdapter.Fill(Me.DB_DMI_RETAILDataSetALL.FS_PROFITLOSS_STATEMENT3, frmReportProfitLoss.dtpPeriod.Value)
        'TODO: This line of code loads data into the 'DB_DMI_RETAILDataSetALL.FS_PROFITLOSS_STATEMENT4' table. You can move, or remove it, as needed.
        Me.FS_PROFITLOSS_STATEMENT4TableAdapter.Fill(Me.DB_DMI_RETAILDataSetALL.FS_PROFITLOSS_STATEMENT4, frmReportProfitLoss.dtpPeriod.Value)
        'TODO: This line of code loads data into the 'DB_DMI_RETAILDataSetALL.COMPANY_INFORMATION' table. You can move, or remove it, as needed.
        Me.COMPANY_INFORMATIONTableAdapter.Fill(Me.DB_DMI_RETAILDataSetALL.COMPANY_INFORMATION)
 
        Me.ReportViewer1.RefreshReport()

        Dim newPageSettings As New System.Drawing.Printing.PageSettings
        newPageSettings.Margins = New System.Drawing.Printing.Margins(50, 50, 35, 35)
        newPageSettings.PaperSize = New System.Drawing.Printing.PaperSize("A4", 827, 1169)
        Me.ReportViewer1.SetPageSettings(newPageSettings)

        If Not tmpReport Is Nothing Then tmpReport.Close()
        tmpReport = Me
    End Sub
End Class