﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frmListOpeningBalance
    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter

    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    
    'Private Sub frmListOpeningBalance_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
    '    If e.KeyCode = Keys.F12 Then
    '        cmdReport_Click(Nothing, Nothing)
    '    End If
    'End Sub

    Private Sub frmListOpeningBalance_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
        Me.Cursor = Cursors.WaitCursor
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "SELECT OPENING_BALANCE_ID, WAREHOUSE_ID ,OPENING_BALANCE_NUMBER,PERIOD ," & _
                                "  (SELECT PRODUCT_CODE  FROM PRODUCT " & _
                                "  WHERE PRODUCT_ID = OPENING_BALANCE .PRODUCT_ID  )PRODUCT_CODE," & _
                                "  (SELECT PRODUCT_NAME  FROM PRODUCT" & _
                                "  WHERE PRODUCT_ID = OPENING_BALANCE .PRODUCT_ID  )PRODUCT_NAME," & _
                                "  (SELECT WAREHOUSE_NAME  FROM WAREHOUSE" & _
                                "  WHERE WAREHOUSE_ID = OPENING_BALANCE .WAREHOUSE_ID)WAREHOUSE_NAME," & _
                                "  QUANTITY ,PRICE,VOID	" & _
                                "  FROM OPENING_BALANCE " & _
                                "  ORDER BY PERIOD"
                       
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.VIEW_OPENING_BALANCEDataGridView.DataSource = dt

        Me.Cursor = Cursors.Default
        If Language = "Indonesian" Then
            Me.Text = "Daftar Saldo Awal"
            cmdReport.Text = "Cetak"
            VIEW_OPENING_BALANCEDataGridView.Columns("OPENING_BALANCE_ID").Visible = False
            VIEW_OPENING_BALANCEDataGridView.Columns("WAREHOUSE_ID").Visible = False
            VIEW_OPENING_BALANCEDataGridView.Columns("PRODUCT_CODE").Visible = False
            VIEW_OPENING_BALANCEDataGridView.Columns("PERIOD").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            VIEW_OPENING_BALANCEDataGridView.Columns("PERIOD").DefaultCellStyle.Format = "dd-MMM-yyyy"
            VIEW_OPENING_BALANCEDataGridView.Columns("PRICE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            VIEW_OPENING_BALANCEDataGridView.Columns("PRICE").DefaultCellStyle.Format = "n0"
            VIEW_OPENING_BALANCEDataGridView.Columns("QUANTITY").Width = 75
            VIEW_OPENING_BALANCEDataGridView.Columns("QUANTITY").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            VIEW_OPENING_BALANCEDataGridView.Columns("VOID").Width = 50
            VIEW_OPENING_BALANCEDataGridView.Columns("PERIOD").HeaderText = "Periode"
            VIEW_OPENING_BALANCEDataGridView.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            VIEW_OPENING_BALANCEDataGridView.Columns("PRICE").HeaderText = "Harga"
            VIEW_OPENING_BALANCEDataGridView.Columns("QUANTITY").HeaderText = "Jumlah"
            VIEW_OPENING_BALANCEDataGridView.Columns("WAREHOUSE_NAME").HeaderText = "Gudang"
            VIEW_OPENING_BALANCEDataGridView.Columns("OPENING_BALANCE_NUMBER").HeaderText = "No Saldo Awal"
            VIEW_OPENING_BALANCEDataGridView.Columns("VOID").HeaderText = "Batal"
        Else
            VIEW_OPENING_BALANCEDataGridView.Columns("OPENING_BALANCE_ID").Visible = False
            VIEW_OPENING_BALANCEDataGridView.Columns("WAREHOUSE_ID").Visible = False
            VIEW_OPENING_BALANCEDataGridView.Columns("PRODUCT_CODE").Visible = False
            VIEW_OPENING_BALANCEDataGridView.Columns("PERIOD").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            VIEW_OPENING_BALANCEDataGridView.Columns("PERIOD").DefaultCellStyle.Format = "dd-MMM-yyyy"
            VIEW_OPENING_BALANCEDataGridView.Columns("PRICE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            VIEW_OPENING_BALANCEDataGridView.Columns("PRICE").DefaultCellStyle.Format = "n0"
            VIEW_OPENING_BALANCEDataGridView.Columns("QUANTITY").Width = 75
            VIEW_OPENING_BALANCEDataGridView.Columns("QUANTITY").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            VIEW_OPENING_BALANCEDataGridView.Columns("VOID").Width = 50
            VIEW_OPENING_BALANCEDataGridView.Columns("PERIOD").HeaderText = "Period"
            VIEW_OPENING_BALANCEDataGridView.Columns("PRODUCT_NAME").HeaderText = "Product Name"
            VIEW_OPENING_BALANCEDataGridView.Columns("PRICE").HeaderText = "Price"
            VIEW_OPENING_BALANCEDataGridView.Columns("QUANTITY").HeaderText = "Quantity"
            VIEW_OPENING_BALANCEDataGridView.Columns("WAREHOUSE_NAME").HeaderText = "Warehouse"
            VIEW_OPENING_BALANCEDataGridView.Columns("OPENING_BALANCE_NUMBER").HeaderText = "Opening Balance No"
            VIEW_OPENING_BALANCEDataGridView.Columns("VOID").HeaderText = "Void"
        End If

    End Sub

    Private Sub VIEW_OPENING_BALANCEDataGridView_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles VIEW_OPENING_BALANCEDataGridView.CellDoubleClick
        Try
            With frmEntryOpeningBalance
                .txtOpeningBalanceNo.Text = VIEW_OPENING_BALANCEDataGridView.Item("OPENING_BALANCE_NUMBER", e.RowIndex).Value
                .PRODUCT_IDTextBox.Text = VIEW_OPENING_BALANCEDataGridView.Item("PRODUCT_CODE", e.RowIndex).Value
                .QUANTITYTextBox.Text = VIEW_OPENING_BALANCEDataGridView.Item("QUANTITY", e.RowIndex).Value
                .PRICETextBox.Text = FormatNumber(VIEW_OPENING_BALANCEDataGridView.Item("PRICE", e.RowIndex).Value, 0)
                .tmpWarehouseID = VIEW_OPENING_BALANCEDataGridView.Item("WAREHOUSE_ID", e.RowIndex).Value
                .cmdSearchCode.Visible = True
                .cmdAdd.Enabled = False
                .cmdEdit.Enabled = False
                .cmdUndo.Enabled = False
                .cmdSave.Enabled = False
                .cmdSearchCode.Enabled = False

                If IsDBNull(VIEW_OPENING_BALANCEDataGridView.Item("VOID", e.RowIndex).Value) Then
                    .cmdDelete.Enabled = True
                Else
                    .cmdDelete.Enabled = False
                End If
                .tmpSaveMode = "Update"

                .ShowDialog(Me)
                .Close()

                .tmpSaveMode = ""

            End With
        Catch

        End Try

        frmListOpeningBalance_Load(Nothing, Nothing)
    End Sub

    'Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
    '    Dim objf As New frmRepOpeningBalance
    '    objf.ReportViewer1.ShowRefreshButton = False
    '    objf.ReportViewer1.ZoomPercent = 100
    '    objf.WindowState = FormWindowState.Maximized
    '    objf.Show()
    'End Sub

    Private Sub txtProductName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtProductName.TextChanged
        Try

            Me.Cursor = Cursors.WaitCursor
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = "SELECT OPENING_BALANCE_ID, WAREHOUSE_ID ,OPENING_BALANCE_NUMBER,PERIOD ," & _
                                    "  (SELECT PRODUCT_CODE  FROM PRODUCT " & _
                                    "  WHERE PRODUCT_ID = OPENING_BALANCE .PRODUCT_ID  )PRODUCT_CODE," & _
                                    "  (SELECT PRODUCT_NAME  FROM PRODUCT" & _
                                    "  WHERE PRODUCT_ID = OPENING_BALANCE .PRODUCT_ID  )PRODUCT_NAME," & _
                                    "  (SELECT WAREHOUSE_NAME  FROM WAREHOUSE" & _
                                    "  WHERE WAREHOUSE_ID = OPENING_BALANCE .WAREHOUSE_ID)WAREHOUSE_NAME," & _
                                    "  QUANTITY ,PRICE, VOID	" & _
                                    "  FROM OPENING_BALANCE " & _
                                    "  WHERE  PRODUCT_ID IN (SELECT PRODUCT_ID FROM PRODUCT WHERE PRODUCT_NAME LIKE '%" & txtProductName.Text.ToUpper & "%')" & _
                                    "  ORDER BY PERIOD"

            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.VIEW_OPENING_BALANCEDataGridView.DataSource = dt
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtProductName.Text = ""
            txtProductName.Focus()
        End Try
    End Sub
End Class