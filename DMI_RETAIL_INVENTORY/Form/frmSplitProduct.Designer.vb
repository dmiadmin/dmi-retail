﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSplitProduct
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSplitProduct))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblParent = New System.Windows.Forms.Label()
        Me.WAREHOUSE_NAMEComboBox = New System.Windows.Forms.ComboBox()
        Me.WAREHOUSEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_GROUPING_PRODUCT = New DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCT()
        Me.lblFrom = New System.Windows.Forms.Label()
        Me.PRODUCT_CODEComboBox = New System.Windows.Forms.ComboBox()
        Me.SP_LIST_PRODUCT_GOODSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PRODUCT_NAMETextBox = New System.Windows.Forms.TextBox()
        Me.cmdSearchParent = New System.Windows.Forms.PictureBox()
        Me.PRODUCTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.txtQuantity = New System.Windows.Forms.TextBox()
        Me.PRODUCTTableAdapter = New DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.PRODUCTTableAdapter()
        Me.dgvSplitProduct = New System.Windows.Forms.DataGridView()
        Me.PRODUCT_CODE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRODUCT_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QUANTITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SP_SPLIT_PRODUCTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.WAREHOUSE_NAMEComboBox1 = New System.Windows.Forms.ComboBox()
        Me.WAREHOUSE1BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblTo = New System.Windows.Forms.Label()
        Me.lblSplited = New System.Windows.Forms.Label()
        Me.TableAdapterManager = New DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.TableAdapterManager()
        Me.SP_SPLIT_PRODUCTTableAdapter = New DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.SP_SPLIT_PRODUCTTableAdapter()
        Me.WAREHOUSETableAdapter = New DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.WAREHOUSETableAdapter()
        Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter = New DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter()
        Me.SP_LIST_PRODUCT_GOODSTableAdapter = New DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.SP_LIST_PRODUCT_GOODSTableAdapter()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_GROUPING_PRODUCT, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_LIST_PRODUCT_GOODSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmdSearchParent, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSplitProduct, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_SPLIT_PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WAREHOUSE1BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblParent)
        Me.GroupBox1.Controls.Add(Me.WAREHOUSE_NAMEComboBox)
        Me.GroupBox1.Controls.Add(Me.lblFrom)
        Me.GroupBox1.Controls.Add(Me.PRODUCT_CODEComboBox)
        Me.GroupBox1.Controls.Add(Me.PRODUCT_NAMETextBox)
        Me.GroupBox1.Controls.Add(Me.cmdSearchParent)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 13)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(517, 127)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        '
        'lblParent
        '
        Me.lblParent.AutoSize = True
        Me.lblParent.Location = New System.Drawing.Point(28, 18)
        Me.lblParent.Name = "lblParent"
        Me.lblParent.Size = New System.Drawing.Size(94, 15)
        Me.lblParent.TabIndex = 14
        Me.lblParent.Text = "Parent Product"
        '
        'WAREHOUSE_NAMEComboBox
        '
        Me.WAREHOUSE_NAMEComboBox.DataSource = Me.WAREHOUSEBindingSource
        Me.WAREHOUSE_NAMEComboBox.DisplayMember = "WAREHOUSE_NAME"
        Me.WAREHOUSE_NAMEComboBox.FormattingEnabled = True
        Me.WAREHOUSE_NAMEComboBox.Location = New System.Drawing.Point(321, 88)
        Me.WAREHOUSE_NAMEComboBox.Name = "WAREHOUSE_NAMEComboBox"
        Me.WAREHOUSE_NAMEComboBox.Size = New System.Drawing.Size(137, 23)
        Me.WAREHOUSE_NAMEComboBox.TabIndex = 3
        Me.WAREHOUSE_NAMEComboBox.ValueMember = "WAREHOUSE_ID"
        '
        'WAREHOUSEBindingSource
        '
        Me.WAREHOUSEBindingSource.DataMember = "WAREHOUSE"
        Me.WAREHOUSEBindingSource.DataSource = Me.DS_GROUPING_PRODUCT
        '
        'DS_GROUPING_PRODUCT
        '
        Me.DS_GROUPING_PRODUCT.DataSetName = "DS_GROUPING_PRODUCT"
        Me.DS_GROUPING_PRODUCT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'lblFrom
        '
        Me.lblFrom.AutoSize = True
        Me.lblFrom.Location = New System.Drawing.Point(277, 91)
        Me.lblFrom.Name = "lblFrom"
        Me.lblFrom.Size = New System.Drawing.Size(38, 15)
        Me.lblFrom.TabIndex = 12
        Me.lblFrom.Text = "From"
        '
        'PRODUCT_CODEComboBox
        '
        Me.PRODUCT_CODEComboBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(202, Byte), Integer))
        Me.PRODUCT_CODEComboBox.DataSource = Me.SP_LIST_PRODUCT_GOODSBindingSource
        Me.PRODUCT_CODEComboBox.DisplayMember = "PRODUCT_CODE"
        Me.PRODUCT_CODEComboBox.FormattingEnabled = True
        Me.PRODUCT_CODEComboBox.Location = New System.Drawing.Point(31, 45)
        Me.PRODUCT_CODEComboBox.Name = "PRODUCT_CODEComboBox"
        Me.PRODUCT_CODEComboBox.Size = New System.Drawing.Size(133, 23)
        Me.PRODUCT_CODEComboBox.TabIndex = 1
        Me.PRODUCT_CODEComboBox.Tag = "M"
        Me.PRODUCT_CODEComboBox.ValueMember = "PRODUCT_ID"
        '
        'SP_LIST_PRODUCT_GOODSBindingSource
        '
        Me.SP_LIST_PRODUCT_GOODSBindingSource.DataMember = "SP_LIST_PRODUCT_GOODS"
        Me.SP_LIST_PRODUCT_GOODSBindingSource.DataSource = Me.DS_GROUPING_PRODUCT
        '
        'PRODUCT_NAMETextBox
        '
        Me.PRODUCT_NAMETextBox.BackColor = System.Drawing.SystemColors.Control
        Me.PRODUCT_NAMETextBox.Location = New System.Drawing.Point(170, 46)
        Me.PRODUCT_NAMETextBox.Name = "PRODUCT_NAMETextBox"
        Me.PRODUCT_NAMETextBox.ReadOnly = True
        Me.PRODUCT_NAMETextBox.Size = New System.Drawing.Size(288, 22)
        Me.PRODUCT_NAMETextBox.TabIndex = 2
        '
        'cmdSearchParent
        '
        Me.cmdSearchParent.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchParent.Image = CType(resources.GetObject("cmdSearchParent.Image"), System.Drawing.Image)
        Me.cmdSearchParent.Location = New System.Drawing.Point(465, 45)
        Me.cmdSearchParent.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchParent.Name = "cmdSearchParent"
        Me.cmdSearchParent.Size = New System.Drawing.Size(33, 25)
        Me.cmdSearchParent.TabIndex = 22
        Me.cmdSearchParent.TabStop = False
        '
        'PRODUCTBindingSource
        '
        Me.PRODUCTBindingSource.DataMember = "PRODUCT"
        Me.PRODUCTBindingSource.DataSource = Me.DS_GROUPING_PRODUCT
        '
        'txtQuantity
        '
        Me.txtQuantity.Location = New System.Drawing.Point(374, 168)
        Me.txtQuantity.Name = "txtQuantity"
        Me.txtQuantity.Size = New System.Drawing.Size(82, 22)
        Me.txtQuantity.TabIndex = 4
        Me.txtQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'PRODUCTTableAdapter
        '
        Me.PRODUCTTableAdapter.ClearBeforeFill = True
        '
        'dgvSplitProduct
        '
        Me.dgvSplitProduct.AllowUserToAddRows = False
        Me.dgvSplitProduct.AllowUserToDeleteRows = False
        Me.dgvSplitProduct.AutoGenerateColumns = False
        Me.dgvSplitProduct.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSplitProduct.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSplitProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSplitProduct.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PRODUCT_CODE, Me.PRODUCT_NAME, Me.QUANTITY})
        Me.dgvSplitProduct.DataSource = Me.SP_SPLIT_PRODUCTBindingSource
        Me.dgvSplitProduct.Location = New System.Drawing.Point(18, 21)
        Me.dgvSplitProduct.Name = "dgvSplitProduct"
        Me.dgvSplitProduct.ReadOnly = True
        Me.dgvSplitProduct.RowHeadersWidth = 24
        Me.dgvSplitProduct.Size = New System.Drawing.Size(478, 131)
        Me.dgvSplitProduct.TabIndex = 11
        '
        'PRODUCT_CODE
        '
        Me.PRODUCT_CODE.DataPropertyName = "PRODUCT_CODE"
        Me.PRODUCT_CODE.HeaderText = "Product Code"
        Me.PRODUCT_CODE.Name = "PRODUCT_CODE"
        Me.PRODUCT_CODE.ReadOnly = True
        Me.PRODUCT_CODE.Width = 120
        '
        'PRODUCT_NAME
        '
        Me.PRODUCT_NAME.DataPropertyName = "PRODUCT_NAME"
        Me.PRODUCT_NAME.HeaderText = "Product Name"
        Me.PRODUCT_NAME.Name = "PRODUCT_NAME"
        Me.PRODUCT_NAME.ReadOnly = True
        Me.PRODUCT_NAME.Width = 210
        '
        'QUANTITY
        '
        Me.QUANTITY.DataPropertyName = "QUANTITY"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.QUANTITY.DefaultCellStyle = DataGridViewCellStyle2
        Me.QUANTITY.HeaderText = "Quantity"
        Me.QUANTITY.Name = "QUANTITY"
        Me.QUANTITY.ReadOnly = True
        '
        'SP_SPLIT_PRODUCTBindingSource
        '
        Me.SP_SPLIT_PRODUCTBindingSource.DataMember = "SP_SPLIT_PRODUCT"
        Me.SP_SPLIT_PRODUCTBindingSource.DataSource = Me.DS_GROUPING_PRODUCT
        '
        'WAREHOUSE_NAMEComboBox1
        '
        Me.WAREHOUSE_NAMEComboBox1.DataSource = Me.WAREHOUSE1BindingSource1
        Me.WAREHOUSE_NAMEComboBox1.DisplayMember = "WAREHOUSE_NAME"
        Me.WAREHOUSE_NAMEComboBox1.FormattingEnabled = True
        Me.WAREHOUSE_NAMEComboBox1.Location = New System.Drawing.Point(333, 207)
        Me.WAREHOUSE_NAMEComboBox1.Name = "WAREHOUSE_NAMEComboBox1"
        Me.WAREHOUSE_NAMEComboBox1.Size = New System.Drawing.Size(123, 23)
        Me.WAREHOUSE_NAMEComboBox1.TabIndex = 5
        Me.WAREHOUSE_NAMEComboBox1.ValueMember = "WAREHOUSE_ID"
        '
        'WAREHOUSE1BindingSource1
        '
        Me.WAREHOUSE1BindingSource1.DataMember = "WAREHOUSE"
        Me.WAREHOUSE1BindingSource1.DataSource = Me.DS_GROUPING_PRODUCT
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblTo)
        Me.GroupBox2.Controls.Add(Me.lblSplited)
        Me.GroupBox2.Controls.Add(Me.WAREHOUSE_NAMEComboBox1)
        Me.GroupBox2.Controls.Add(Me.dgvSplitProduct)
        Me.GroupBox2.Controls.Add(Me.txtQuantity)
        Me.GroupBox2.Location = New System.Drawing.Point(14, 150)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(515, 241)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        '
        'lblTo
        '
        Me.lblTo.AutoSize = True
        Me.lblTo.Location = New System.Drawing.Point(304, 210)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(23, 15)
        Me.lblTo.TabIndex = 15
        Me.lblTo.Text = "To"
        '
        'lblSplited
        '
        Me.lblSplited.Location = New System.Drawing.Point(167, 171)
        Me.lblSplited.Name = "lblSplited"
        Me.lblSplited.Size = New System.Drawing.Size(201, 15)
        Me.lblSplited.TabIndex = 13
        Me.lblSplited.Text = "Number of Product to be Splited"
        Me.lblSplited.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.GROUPING_PRODUCTTableAdapter = Nothing
        Me.TableAdapterManager.PRODUCT_UNITTableAdapter = Nothing
        Me.TableAdapterManager.PRODUCT1TableAdapter = Nothing
        Me.TableAdapterManager.PRODUCTTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'SP_SPLIT_PRODUCTTableAdapter
        '
        Me.SP_SPLIT_PRODUCTTableAdapter.ClearBeforeFill = True
        '
        'WAREHOUSETableAdapter
        '
        Me.WAREHOUSETableAdapter.ClearBeforeFill = True
        '
        'SP_PRODUCT_WAREHOUSE_DETAILBindingSource
        '
        Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource.DataMember = "SP_PRODUCT_WAREHOUSE_DETAIL"
        Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource.DataSource = Me.DS_GROUPING_PRODUCT
        '
        'SP_PRODUCT_WAREHOUSE_DETAILTableAdapter
        '
        Me.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.ClearBeforeFill = True
        '
        'SP_LIST_PRODUCT_GOODSTableAdapter
        '
        Me.SP_LIST_PRODUCT_GOODSTableAdapter.ClearBeforeFill = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.cmdUndo)
        Me.GroupBox4.Controls.Add(Me.cmdSave)
        Me.GroupBox4.Controls.Add(Me.cmdEdit)
        Me.GroupBox4.Controls.Add(Me.cmdDelete)
        Me.GroupBox4.Controls.Add(Me.cmdAdd)
        Me.GroupBox4.Location = New System.Drawing.Point(14, 402)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox4.Size = New System.Drawing.Size(515, 55)
        Me.GroupBox4.TabIndex = 24
        Me.GroupBox4.TabStop = False
        '
        'cmdUndo
        '
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(220, 17)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(74, 29)
        Me.cmdUndo.TabIndex = 16
        Me.cmdUndo.Text = "&Undo"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSave.Location = New System.Drawing.Point(321, 17)
        Me.cmdSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(74, 29)
        Me.cmdSave.TabIndex = 17
        Me.cmdSave.Text = "&Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Enabled = False
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(119, 17)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(74, 29)
        Me.cmdEdit.TabIndex = 15
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Enabled = False
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(422, 17)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(74, 29)
        Me.cmdDelete.TabIndex = 18
        Me.cmdDelete.Text = "&Delete"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Enabled = False
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(18, 17)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(74, 29)
        Me.cmdAdd.TabIndex = 14
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'frmSplitProduct
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(541, 469)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmSplitProduct"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Entry Split Product"
        Me.Text = "Entry Split Product"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_GROUPING_PRODUCT, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_LIST_PRODUCT_GOODSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmdSearchParent, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSplitProduct, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_SPLIT_PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WAREHOUSE1BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents PRODUCT_NAMETextBox As System.Windows.Forms.TextBox
    Friend WithEvents cmdSearchParent As System.Windows.Forms.PictureBox
    Friend WithEvents PRODUCT_CODEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents txtQuantity As System.Windows.Forms.TextBox
    Friend WithEvents DS_GROUPING_PRODUCT As DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCT
    Friend WithEvents PRODUCTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PRODUCTTableAdapter As DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.PRODUCTTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.TableAdapterManager
    Friend WithEvents SP_SPLIT_PRODUCTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_SPLIT_PRODUCTTableAdapter As DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.SP_SPLIT_PRODUCTTableAdapter
    Friend WithEvents dgvSplitProduct As System.Windows.Forms.DataGridView
    Friend WithEvents WAREHOUSEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents WAREHOUSETableAdapter As DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.WAREHOUSETableAdapter
    Friend WithEvents WAREHOUSE_NAMEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents WAREHOUSE_NAMEComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents WAREHOUSE1BindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents SP_PRODUCT_WAREHOUSE_DETAILBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_PRODUCT_WAREHOUSE_DETAILTableAdapter As DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter
    Friend WithEvents SP_LIST_PRODUCT_GOODSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_LIST_PRODUCT_GOODSTableAdapter As DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.SP_LIST_PRODUCT_GOODSTableAdapter
    Friend WithEvents lblParent As System.Windows.Forms.Label
    Friend WithEvents lblFrom As System.Windows.Forms.Label
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents lblSplited As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents PRODUCT_CODE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRODUCT_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QUANTITY As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
