﻿Public Class frmRepDailyInvoice

    Private Sub frmRepDailyInvoice_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DB_DMI_RETAILDataSet1.SP_LIST_DAILY_INVOICE' table. You can move, or remove it, as needed.
        Me.SP_LIST_DAILY_INVOICETableAdapter.Fill(Me.DB_DMI_RETAILDataSet1.SP_LIST_DAILY_INVOICE, frmListDailyInvoice.dtpInvoice.Value)
        'TODO: This line of code loads data into the 'DB_DMI_RETAILDataSet1.COMPANY_INFORMATION' table. You can move, or remove it, as needed.
        Me.COMPANY_INFORMATIONTableAdapter.Fill(Me.DB_DMI_RETAILDataSet1.COMPANY_INFORMATION)

        Me.ReportViewer1.RefreshReport()

        Dim newPageSettings As New System.Drawing.Printing.PageSettings
        newPageSettings.Margins = New System.Drawing.Printing.Margins(50, 50, 35, 35)
        newPageSettings.PaperSize = New System.Drawing.Printing.PaperSize("A4", 827, 1169)
        Me.ReportViewer1.SetPageSettings(newPageSettings)

        If Not tmpReport Is Nothing Then tmpReport.Close()
        tmpReport = Me
    End Sub
End Class