﻿Public Class frmListSalesmanInvoice

    Public tmpSalesmanId, tmpTotal As Integer
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    Private Sub frmListSalesmanInvoice_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F12 Then
            cmdReport_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub frmListSalesmanInvoice_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'TODO: This line of code loads data into the 'DS_SALE.SALESMAN' table. You can move, or remove it, as needed.
        Me.SALESMANTableAdapter.Fill(Me.DS_SALE.SALESMAN)

        If SALESMANBindingSource.Count = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data Sales." & vbCrLf & "Masukkan setidaknya satu Sales !", _
                       MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Salesman." & vbCrLf & "Please input at least one Salesman !", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
        End If

        dtpPeriod1.Value = DateSerial(Today.Year, Today.Month, 1)
        dtpPeriod2.Value = DateSerial(Today.Year, Today.Month + 1, 0)

        For n As Integer = 0 To dgvSalesmanInvoice.RowCount - 1
            tmpTotal = tmpTotal + CInt(dgvSalesmanInvoice.Item("GRAND_TOTAL", n).Value)
        Next

        If Language = "Indonesian" Then
            Me.Text = "Daftar Penjualan Sales per Faktur"
            cmdReport.Text = "Cetak"
            cmdGenerate.Text = "Proses"
            lblSalesman.Text = "Nama Sales"
            lblFrom.Text = "Periode"

            dgvSalesmanInvoice.Columns("SALESMAN_NAME").HeaderText = "Nama Sales"
            dgvSalesmanInvoice.Columns("SALE_DATE").HeaderText = "Tgl Penjualan"
            dgvSalesmanInvoice.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            dgvSalesmanInvoice.Columns("CUSTOMER_NAME").HeaderText = "Nama Pelanggan"
            dgvSalesmanInvoice.Columns("PAYMENT_METHOD_NAME").HeaderText = "Cara Pembayaran"
            dgvSalesmanInvoice.Columns("MATURITY_DATE").HeaderText = "Tgl Jatuh Tempo"
            dgvSalesmanInvoice.Columns("PAYMENT").HeaderText = "Pembayaran"
            dgvSalesmanInvoice.Columns("GRAND_TOTAL").HeaderText = "Total"
            dgvSalesmanInvoice.Columns("REMARK").HeaderText = "Keterangan"
            dgvSalesmanInvoice.Columns("WAREHOUSE_NAME").HeaderText = "Gudang"

            Label1.Text = "Jumlah Faktur : " & dgvSalesmanInvoice.RowCount
            Label2.Text = "Total Penjualan : " & FormatNumber(tmpTotal, 0)
        Else
            Label1.Text = "Total Invoice : " & dgvSalesmanInvoice.RowCount
            Label2.Text = "Total Sales : " & FormatNumber(tmpTotal, 0)
        End If

        cmbSalesmanCode.Text = ""
    End Sub

    Private Sub cmbSalesmanCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSalesmanCode.TextChanged
        SALESMANTableAdapter.SP_GET_SALESMAN_NAME(cmbSalesmanCode.Text, txtSalesmanName.Text)
    End Sub

    Private Sub cmdSearchName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchName.Click
        mdlGeneral.tmpSearchMode = "Salesman Name"
        frmSearchSalesman.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        SALESMANBindingSource.Position = SALESMANBindingSource.Find("SALESMAN_ID", tmpSearchResult)
        cmbSalesmanCode.Text = SALESMANBindingSource.Current("SALESMAN_CODE")
    End Sub

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click

        If SALESMANBindingSource.Count = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data Sales." & vbCrLf & "Form ini akan ditutup !", _
                       MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Salesman." & vbCrLf & "This form will be closed !", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Me.Close()
            Exit Sub
        End If


        
        If Not ValidateAllComboBox(Me) Then
            If Language = "Indonesian" Then
                MsgBox("Anda memasukkan nilai yang tidak terdaftar.", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You input value which is not in the list.", MsgBoxStyle.Critical, "DMI Retail")
            End If
            cmbSalesmanCode.Focus()
            Exit Sub
        End If

        Cursor = Cursors.WaitCursor
        tmpSalesmanId = IIf(cmbSalesmanCode.Text.Length = 0, 0, SALESMANBindingSource.Current("SALESMAN_ID"))

        SP_LIST_SALESMAN_INVOICETableAdapter.Fill(Me.DS_SALE.SP_LIST_SALESMAN_INVOICE, tmpSalesmanId, dtpPeriod1.Value, dtpPeriod2.Value)
        Cursor = Cursors.Default

        tmpTotal = 0

        For n As Integer = 0 To dgvSalesmanInvoice.RowCount - 1
            tmpTotal = tmpTotal + CInt(dgvSalesmanInvoice.Item("GRAND_TOTAL", n).Value)
        Next
        If Language = "Indonesian" Then
            Label1.Text = "Jumlah Faktur : " & dgvSalesmanInvoice.RowCount
            Label2.Text = "Total Penjualan : " & FormatNumber(tmpTotal, 0)
        Else
            Label1.Text = "Total Invoice : " & dgvSalesmanInvoice.RowCount
            Label2.Text = "Total Sales : " & FormatNumber(tmpTotal, 0)
        End If
    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        If SALESMANBindingSource.Count = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data Sales." & vbCrLf & "Form ini akan ditutup !", _
                       MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Salesman." & vbCrLf & "This form will be closed !", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Me.Close()
            Exit Sub
        End If

        Dim RPV As New frmRepSalesmanInvoice
        RPV.date1 = dtpPeriod1.Value
        RPV.date2 = dtpPeriod2.Value
        RPV.tmpSalesmanId = tmpSalesmanId
        RPV.ReportViewer1.ZoomPercent = 100
        RPV.WindowState = FormWindowState.Maximized
        RPV.Show()
    End Sub
End Class