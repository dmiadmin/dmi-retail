﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSearchAccount
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSearchAccount))
        Me.DS_ACCOUNT = New DMI_RETAIL_HUTANG_PIUTANG.DS_ACCOUNT()
        Me.VIEW_PAYABLE_ACCOUNTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VIEW_PAYABLE_ACCOUNTTableAdapter = New DMI_RETAIL_HUTANG_PIUTANG.DS_ACCOUNTTableAdapters.VIEW_PAYABLE_ACCOUNTTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_HUTANG_PIUTANG.DS_ACCOUNTTableAdapters.TableAdapterManager()
        Me.ACCOUNTTableAdapter = New DMI_RETAIL_HUTANG_PIUTANG.DS_ACCOUNTTableAdapters.ACCOUNTTableAdapter()
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.dgvSearchAccount = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACCOUNT_NUMBER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACCOUNT_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACCOUNT_NAMETextBox = New System.Windows.Forms.TextBox()
        Me.VIEW_RECEIVABLE_ACCOUNTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VIEW_RECEIVABLE_ACCOUNTTableAdapter = New DMI_RETAIL_HUTANG_PIUTANG.DS_ACCOUNTTableAdapters.VIEW_RECEIVABLE_ACCOUNTTableAdapter()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ACCOUNTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblAccountName = New System.Windows.Forms.Label()
        CType(Me.DS_ACCOUNT, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VIEW_PAYABLE_ACCOUNTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VIEW_PAYABLE_ACCOUNTBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator.SuspendLayout()
        CType(Me.dgvSearchAccount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VIEW_RECEIVABLE_ACCOUNTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ACCOUNTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DS_ACCOUNT
        '
        Me.DS_ACCOUNT.DataSetName = "DS_ACCOUNT"
        Me.DS_ACCOUNT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'VIEW_PAYABLE_ACCOUNTBindingSource
        '
        Me.VIEW_PAYABLE_ACCOUNTBindingSource.DataMember = "VIEW_PAYABLE_ACCOUNT"
        Me.VIEW_PAYABLE_ACCOUNTBindingSource.DataSource = Me.DS_ACCOUNT
        '
        'VIEW_PAYABLE_ACCOUNTTableAdapter
        '
        Me.VIEW_PAYABLE_ACCOUNTTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ACCOUNTTableAdapter = Me.ACCOUNTTableAdapter
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_HUTANG_PIUTANG.DS_ACCOUNTTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'ACCOUNTTableAdapter
        '
        Me.ACCOUNTTableAdapter.ClearBeforeFill = True
        '
        'VIEW_PAYABLE_ACCOUNTBindingNavigator
        '
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator.AddNewItem = Nothing
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator.BindingSource = Me.VIEW_PAYABLE_ACCOUNTBindingSource
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator.DeleteItem = Nothing
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator.Name = "VIEW_PAYABLE_ACCOUNTBindingNavigator"
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator.Size = New System.Drawing.Size(422, 25)
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator.TabIndex = 0
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'dgvSearchAccount
        '
        Me.dgvSearchAccount.AllowUserToAddRows = False
        Me.dgvSearchAccount.AllowUserToDeleteRows = False
        Me.dgvSearchAccount.AutoGenerateColumns = False
        Me.dgvSearchAccount.BackgroundColor = System.Drawing.Color.White
        Me.dgvSearchAccount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSearchAccount.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.ACCOUNT_NUMBER, Me.ACCOUNT_NAME})
        Me.dgvSearchAccount.DataSource = Me.VIEW_PAYABLE_ACCOUNTBindingSource
        Me.dgvSearchAccount.Location = New System.Drawing.Point(12, 64)
        Me.dgvSearchAccount.Name = "dgvSearchAccount"
        Me.dgvSearchAccount.ReadOnly = True
        Me.dgvSearchAccount.Size = New System.Drawing.Size(398, 280)
        Me.dgvSearchAccount.TabIndex = 5
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "ACCOUNT_ID"
        Me.DataGridViewTextBoxColumn1.HeaderText = "ACCOUNT_ID"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'ACCOUNT_NUMBER
        '
        Me.ACCOUNT_NUMBER.DataPropertyName = "ACCOUNT_NUMBER"
        Me.ACCOUNT_NUMBER.HeaderText = "Account No."
        Me.ACCOUNT_NUMBER.Name = "ACCOUNT_NUMBER"
        Me.ACCOUNT_NUMBER.ReadOnly = True
        Me.ACCOUNT_NUMBER.Width = 120
        '
        'ACCOUNT_NAME
        '
        Me.ACCOUNT_NAME.DataPropertyName = "ACCOUNT_NAME"
        Me.ACCOUNT_NAME.HeaderText = "Account Name"
        Me.ACCOUNT_NAME.Name = "ACCOUNT_NAME"
        Me.ACCOUNT_NAME.ReadOnly = True
        Me.ACCOUNT_NAME.Width = 200
        '
        'ACCOUNT_NAMETextBox
        '
        Me.ACCOUNT_NAMETextBox.Location = New System.Drawing.Point(115, 38)
        Me.ACCOUNT_NAMETextBox.Name = "ACCOUNT_NAMETextBox"
        Me.ACCOUNT_NAMETextBox.Size = New System.Drawing.Size(169, 20)
        Me.ACCOUNT_NAMETextBox.TabIndex = 3
        '
        'VIEW_RECEIVABLE_ACCOUNTBindingSource
        '
        Me.VIEW_RECEIVABLE_ACCOUNTBindingSource.DataMember = "VIEW_RECEIVABLE_ACCOUNT"
        Me.VIEW_RECEIVABLE_ACCOUNTBindingSource.DataSource = Me.DS_ACCOUNT
        '
        'VIEW_RECEIVABLE_ACCOUNTTableAdapter
        '
        Me.VIEW_RECEIVABLE_ACCOUNTTableAdapter.ClearBeforeFill = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(337, 47)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 14)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Label1"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label1.Visible = False
        '
        'ACCOUNTBindingSource
        '
        Me.ACCOUNTBindingSource.DataMember = "ACCOUNT"
        Me.ACCOUNTBindingSource.DataSource = Me.DS_ACCOUNT
        '
        'lblAccountName
        '
        Me.lblAccountName.Location = New System.Drawing.Point(12, 40)
        Me.lblAccountName.Name = "lblAccountName"
        Me.lblAccountName.Size = New System.Drawing.Size(99, 18)
        Me.lblAccountName.TabIndex = 6
        Me.lblAccountName.Text = "Account Name :"
        Me.lblAccountName.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'frmSearchAccount
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(422, 356)
        Me.Controls.Add(Me.lblAccountName)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ACCOUNT_NAMETextBox)
        Me.Controls.Add(Me.dgvSearchAccount)
        Me.Controls.Add(Me.VIEW_PAYABLE_ACCOUNTBindingNavigator)
        Me.Font = New System.Drawing.Font("Lucida Bright", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSearchAccount"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Search Account"
        CType(Me.DS_ACCOUNT, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VIEW_PAYABLE_ACCOUNTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VIEW_PAYABLE_ACCOUNTBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator.ResumeLayout(False)
        Me.VIEW_PAYABLE_ACCOUNTBindingNavigator.PerformLayout()
        CType(Me.dgvSearchAccount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VIEW_RECEIVABLE_ACCOUNTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ACCOUNTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DS_ACCOUNT As DMI_RETAIL_HUTANG_PIUTANG.DS_ACCOUNT
    Friend WithEvents VIEW_PAYABLE_ACCOUNTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VIEW_PAYABLE_ACCOUNTTableAdapter As DMI_RETAIL_HUTANG_PIUTANG.DS_ACCOUNTTableAdapters.VIEW_PAYABLE_ACCOUNTTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_HUTANG_PIUTANG.DS_ACCOUNTTableAdapters.TableAdapterManager
    Friend WithEvents VIEW_PAYABLE_ACCOUNTBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents dgvSearchAccount As System.Windows.Forms.DataGridView
    Friend WithEvents ACCOUNT_NAMETextBox As System.Windows.Forms.TextBox
    Friend WithEvents VIEW_RECEIVABLE_ACCOUNTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VIEW_RECEIVABLE_ACCOUNTTableAdapter As DMI_RETAIL_HUTANG_PIUTANG.DS_ACCOUNTTableAdapters.VIEW_RECEIVABLE_ACCOUNTTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ACCOUNTTableAdapter As DMI_RETAIL_HUTANG_PIUTANG.DS_ACCOUNTTableAdapters.ACCOUNTTableAdapter
    Friend WithEvents ACCOUNTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents lblAccountName As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACCOUNT_NUMBER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACCOUNT_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
