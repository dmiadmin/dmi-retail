﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEntryVendor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim VENDOR_CONTACT_PERSONLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEntryVendor))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkActive = New System.Windows.Forms.CheckBox()
        Me.lblCity = New System.Windows.Forms.Label()
        Me.lblFax = New System.Windows.Forms.Label()
        Me.lblPhone = New System.Windows.Forms.Label()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblCode = New System.Windows.Forms.Label()
        Me.cmdSearchName = New System.Windows.Forms.PictureBox()
        Me.cmdSearchCode = New System.Windows.Forms.PictureBox()
        Me.VENDOR_CODETextBox = New System.Windows.Forms.TextBox()
        Me.VENDOR_NAMETextBox = New System.Windows.Forms.TextBox()
        Me.VENDOR_ADDRESSTextBox = New System.Windows.Forms.TextBox()
        Me.VENDOR_PHONETextBox = New System.Windows.Forms.TextBox()
        Me.VENDOR_FAXTextBox = New System.Windows.Forms.TextBox()
        Me.VENDOR_CITYTextBox = New System.Windows.Forms.TextBox()
        Me.VENDOR_CONTACT_PERSONTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.VENDORBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        VENDOR_CONTACT_PERSONLabel = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmdSearchCode, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.VENDORBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.VENDORBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'VENDOR_CONTACT_PERSONLabel
        '
        VENDOR_CONTACT_PERSONLabel.AutoSize = True
        VENDOR_CONTACT_PERSONLabel.Location = New System.Drawing.Point(41, 226)
        VENDOR_CONTACT_PERSONLabel.Name = "VENDOR_CONTACT_PERSONLabel"
        VENDOR_CONTACT_PERSONLabel.Size = New System.Drawing.Size(122, 17)
        VENDOR_CONTACT_PERSONLabel.TabIndex = 12
        VENDOR_CONTACT_PERSONLabel.Text = "Contact Person"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkActive)
        Me.GroupBox1.Controls.Add(Me.lblCity)
        Me.GroupBox1.Controls.Add(Me.lblFax)
        Me.GroupBox1.Controls.Add(Me.lblPhone)
        Me.GroupBox1.Controls.Add(Me.lblAddress)
        Me.GroupBox1.Controls.Add(Me.lblName)
        Me.GroupBox1.Controls.Add(Me.lblCode)
        Me.GroupBox1.Controls.Add(Me.cmdSearchName)
        Me.GroupBox1.Controls.Add(Me.cmdSearchCode)
        Me.GroupBox1.Controls.Add(Me.VENDOR_CODETextBox)
        Me.GroupBox1.Controls.Add(Me.VENDOR_NAMETextBox)
        Me.GroupBox1.Controls.Add(Me.VENDOR_ADDRESSTextBox)
        Me.GroupBox1.Controls.Add(Me.VENDOR_PHONETextBox)
        Me.GroupBox1.Controls.Add(Me.VENDOR_FAXTextBox)
        Me.GroupBox1.Controls.Add(Me.VENDOR_CITYTextBox)
        Me.GroupBox1.Controls.Add(VENDOR_CONTACT_PERSONLabel)
        Me.GroupBox1.Controls.Add(Me.VENDOR_CONTACT_PERSONTextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 28)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(516, 266)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'chkActive
        '
        Me.chkActive.AutoSize = True
        Me.chkActive.Location = New System.Drawing.Point(433, 226)
        Me.chkActive.Name = "chkActive"
        Me.chkActive.Size = New System.Drawing.Size(76, 21)
        Me.chkActive.TabIndex = 14
        Me.chkActive.Text = "Active"
        Me.chkActive.UseVisualStyleBackColor = True
        '
        'lblCity
        '
        Me.lblCity.AutoSize = True
        Me.lblCity.Location = New System.Drawing.Point(41, 198)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(38, 17)
        Me.lblCity.TabIndex = 10
        Me.lblCity.Text = "City"
        '
        'lblFax
        '
        Me.lblFax.AutoSize = True
        Me.lblFax.Location = New System.Drawing.Point(41, 170)
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Size = New System.Drawing.Size(32, 17)
        Me.lblFax.TabIndex = 8
        Me.lblFax.Text = "Fax"
        '
        'lblPhone
        '
        Me.lblPhone.AutoSize = True
        Me.lblPhone.Location = New System.Drawing.Point(41, 142)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.Size = New System.Drawing.Size(54, 17)
        Me.lblPhone.TabIndex = 6
        Me.lblPhone.Text = "Phone"
        '
        'lblAddress
        '
        Me.lblAddress.AutoSize = True
        Me.lblAddress.Location = New System.Drawing.Point(41, 83)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(70, 17)
        Me.lblAddress.TabIndex = 4
        Me.lblAddress.Text = "Address"
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Location = New System.Drawing.Point(41, 55)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(50, 17)
        Me.lblName.TabIndex = 2
        Me.lblName.Text = "Name"
        '
        'lblCode
        '
        Me.lblCode.AutoSize = True
        Me.lblCode.Location = New System.Drawing.Point(41, 27)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(48, 17)
        Me.lblCode.TabIndex = 0
        Me.lblCode.Text = "Code"
        '
        'cmdSearchName
        '
        Me.cmdSearchName.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchName.Image = CType(resources.GetObject("cmdSearchName.Image"), System.Drawing.Image)
        Me.cmdSearchName.Location = New System.Drawing.Point(359, 52)
        Me.cmdSearchName.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchName.Name = "cmdSearchName"
        Me.cmdSearchName.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearchName.TabIndex = 15
        Me.cmdSearchName.TabStop = False
        '
        'cmdSearchCode
        '
        Me.cmdSearchCode.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchCode.Image = CType(resources.GetObject("cmdSearchCode.Image"), System.Drawing.Image)
        Me.cmdSearchCode.Location = New System.Drawing.Point(246, 24)
        Me.cmdSearchCode.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchCode.Name = "cmdSearchCode"
        Me.cmdSearchCode.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearchCode.TabIndex = 14
        Me.cmdSearchCode.TabStop = False
        '
        'VENDOR_CODETextBox
        '
        Me.VENDOR_CODETextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.VENDOR_CODETextBox.Location = New System.Drawing.Point(150, 24)
        Me.VENDOR_CODETextBox.Name = "VENDOR_CODETextBox"
        Me.VENDOR_CODETextBox.Size = New System.Drawing.Size(89, 25)
        Me.VENDOR_CODETextBox.TabIndex = 1
        Me.VENDOR_CODETextBox.Tag = "M"
        '
        'VENDOR_NAMETextBox
        '
        Me.VENDOR_NAMETextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.VENDOR_NAMETextBox.Location = New System.Drawing.Point(150, 52)
        Me.VENDOR_NAMETextBox.Name = "VENDOR_NAMETextBox"
        Me.VENDOR_NAMETextBox.Size = New System.Drawing.Size(202, 25)
        Me.VENDOR_NAMETextBox.TabIndex = 3
        Me.VENDOR_NAMETextBox.Tag = "M"
        '
        'VENDOR_ADDRESSTextBox
        '
        Me.VENDOR_ADDRESSTextBox.Location = New System.Drawing.Point(150, 80)
        Me.VENDOR_ADDRESSTextBox.Multiline = True
        Me.VENDOR_ADDRESSTextBox.Name = "VENDOR_ADDRESSTextBox"
        Me.VENDOR_ADDRESSTextBox.Size = New System.Drawing.Size(202, 51)
        Me.VENDOR_ADDRESSTextBox.TabIndex = 5
        '
        'VENDOR_PHONETextBox
        '
        Me.VENDOR_PHONETextBox.Location = New System.Drawing.Point(150, 139)
        Me.VENDOR_PHONETextBox.Name = "VENDOR_PHONETextBox"
        Me.VENDOR_PHONETextBox.Size = New System.Drawing.Size(145, 25)
        Me.VENDOR_PHONETextBox.TabIndex = 7
        '
        'VENDOR_FAXTextBox
        '
        Me.VENDOR_FAXTextBox.Location = New System.Drawing.Point(150, 167)
        Me.VENDOR_FAXTextBox.Name = "VENDOR_FAXTextBox"
        Me.VENDOR_FAXTextBox.Size = New System.Drawing.Size(145, 25)
        Me.VENDOR_FAXTextBox.TabIndex = 9
        '
        'VENDOR_CITYTextBox
        '
        Me.VENDOR_CITYTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.VENDOR_CITYTextBox.Location = New System.Drawing.Point(150, 195)
        Me.VENDOR_CITYTextBox.Name = "VENDOR_CITYTextBox"
        Me.VENDOR_CITYTextBox.Size = New System.Drawing.Size(145, 25)
        Me.VENDOR_CITYTextBox.TabIndex = 11
        Me.VENDOR_CITYTextBox.Tag = "M"
        '
        'VENDOR_CONTACT_PERSONTextBox
        '
        Me.VENDOR_CONTACT_PERSONTextBox.Location = New System.Drawing.Point(150, 223)
        Me.VENDOR_CONTACT_PERSONTextBox.Name = "VENDOR_CONTACT_PERSONTextBox"
        Me.VENDOR_CONTACT_PERSONTextBox.Size = New System.Drawing.Size(145, 25)
        Me.VENDOR_CONTACT_PERSONTextBox.TabIndex = 13
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdUndo)
        Me.GroupBox2.Controls.Add(Me.cmdSave)
        Me.GroupBox2.Controls.Add(Me.cmdEdit)
        Me.GroupBox2.Controls.Add(Me.cmdDelete)
        Me.GroupBox2.Controls.Add(Me.cmdAdd)
        Me.GroupBox2.Location = New System.Drawing.Point(13, 300)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Size = New System.Drawing.Size(515, 55)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        '
        'cmdUndo
        '
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(220, 17)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(74, 29)
        Me.cmdUndo.TabIndex = 2
        Me.cmdUndo.Text = "&Undo"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSave.Location = New System.Drawing.Point(321, 17)
        Me.cmdSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(74, 29)
        Me.cmdSave.TabIndex = 3
        Me.cmdSave.Text = "&Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(119, 17)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(74, 29)
        Me.cmdEdit.TabIndex = 1
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(422, 17)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(74, 29)
        Me.cmdDelete.TabIndex = 4
        Me.cmdDelete.Text = "&Delete"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(18, 17)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(74, 29)
        Me.cmdAdd.TabIndex = 0
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'VENDORBindingNavigator
        '
        Me.VENDORBindingNavigator.AddNewItem = Nothing
        Me.VENDORBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.VENDORBindingNavigator.DeleteItem = Nothing
        Me.VENDORBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.VENDORBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.VENDORBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.VENDORBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.VENDORBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.VENDORBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.VENDORBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.VENDORBindingNavigator.Name = "VENDORBindingNavigator"
        Me.VENDORBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.VENDORBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.VENDORBindingNavigator.Size = New System.Drawing.Size(541, 25)
        Me.VENDORBindingNavigator.TabIndex = 0
        Me.VENDORBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(45, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'frmEntryVendor
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(541, 368)
        Me.Controls.Add(Me.VENDORBindingNavigator)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmEntryVendor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Entry Vendor"
        Me.Text = "Entry Vendor"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmdSearchCode, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.VENDORBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.VENDORBindingNavigator.ResumeLayout(False)
        Me.VENDORBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    'Friend WithEvents DS_VENDOR As DMI_RETAIL_MASTER.DS_VENDOR
    'Friend WithEvents VENDORTableAdapter As DMI_RETAIL_MASTER.DS_VENDORTableAdapters.VENDORTableAdapter
    'Friend WithEvents TableAdapterManager As DMI_RETAIL_MASTER.DS_VENDORTableAdapters.TableAdapterManager
    Friend WithEvents VENDORBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents VENDOR_CODETextBox As System.Windows.Forms.TextBox
    Friend WithEvents VENDOR_NAMETextBox As System.Windows.Forms.TextBox
    Friend WithEvents VENDOR_ADDRESSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents VENDOR_PHONETextBox As System.Windows.Forms.TextBox
    Friend WithEvents VENDOR_FAXTextBox As System.Windows.Forms.TextBox
    Friend WithEvents VENDOR_CITYTextBox As System.Windows.Forms.TextBox
    Friend WithEvents VENDOR_CONTACT_PERSONTextBox As System.Windows.Forms.TextBox
    Friend WithEvents cmdSearchName As System.Windows.Forms.PictureBox
    Friend WithEvents cmdSearchCode As System.Windows.Forms.PictureBox
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents lblFax As System.Windows.Forms.Label
    Friend WithEvents lblPhone As System.Windows.Forms.Label
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents chkActive As System.Windows.Forms.CheckBox
End Class
