﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListReceivablePayment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListReceivablePayment))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dtpPeriod = New System.Windows.Forms.DateTimePicker()
        Me.cmdReport = New System.Windows.Forms.Button()
        Me.cmdGenerate = New System.Windows.Forms.Button()
        Me.lblPeriod = New System.Windows.Forms.Label()
        Me.lblTotalAmount = New System.Windows.Forms.Label()
        Me.lblNoOfTrans = New System.Windows.Forms.Label()
        Me.dgvLIST_RECEIVABLE_PAYMENTDataGridView = New System.Windows.Forms.DataGridView()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtCustomer = New System.Windows.Forms.TextBox()
        Me.lblCustomerName = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvLIST_RECEIVABLE_PAYMENTDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dtpPeriod)
        Me.GroupBox1.Controls.Add(Me.cmdReport)
        Me.GroupBox1.Controls.Add(Me.cmdGenerate)
        Me.GroupBox1.Controls.Add(Me.lblPeriod)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 21)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(686, 106)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        '
        'dtpPeriod
        '
        Me.dtpPeriod.CustomFormat = "MMMM - yyyy"
        Me.dtpPeriod.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPeriod.Location = New System.Drawing.Point(275, 21)
        Me.dtpPeriod.Name = "dtpPeriod"
        Me.dtpPeriod.ShowUpDown = True
        Me.dtpPeriod.Size = New System.Drawing.Size(154, 22)
        Me.dtpPeriod.TabIndex = 23
        '
        'cmdReport
        '
        Me.cmdReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdReport.Image = Global.DMI_RETAIL_HUTANG_PIUTANG.My.Resources.Resources.Printer_small
        Me.cmdReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdReport.Location = New System.Drawing.Point(590, 76)
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(90, 24)
        Me.cmdReport.TabIndex = 22
        Me.cmdReport.Text = "Print [F12]"
        Me.cmdReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.cmdReport.UseVisualStyleBackColor = True
        '
        'cmdGenerate
        '
        Me.cmdGenerate.Image = CType(resources.GetObject("cmdGenerate.Image"), System.Drawing.Image)
        Me.cmdGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdGenerate.Location = New System.Drawing.Point(296, 49)
        Me.cmdGenerate.Name = "cmdGenerate"
        Me.cmdGenerate.Size = New System.Drawing.Size(99, 33)
        Me.cmdGenerate.TabIndex = 14
        Me.cmdGenerate.Text = "Generate"
        Me.cmdGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdGenerate.UseVisualStyleBackColor = True
        '
        'lblPeriod
        '
        Me.lblPeriod.AutoSize = True
        Me.lblPeriod.Location = New System.Drawing.Point(182, 24)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(77, 15)
        Me.lblPeriod.TabIndex = 13
        Me.lblPeriod.Text = "Time Period"
        '
        'lblTotalAmount
        '
        Me.lblTotalAmount.AutoSize = True
        Me.lblTotalAmount.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalAmount.Location = New System.Drawing.Point(398, 418)
        Me.lblTotalAmount.Name = "lblTotalAmount"
        Me.lblTotalAmount.Size = New System.Drawing.Size(89, 15)
        Me.lblTotalAmount.TabIndex = 17
        Me.lblTotalAmount.Text = "TotalAmount"
        '
        'lblNoOfTrans
        '
        Me.lblNoOfTrans.AutoSize = True
        Me.lblNoOfTrans.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoOfTrans.Location = New System.Drawing.Point(37, 418)
        Me.lblNoOfTrans.Name = "lblNoOfTrans"
        Me.lblNoOfTrans.Size = New System.Drawing.Size(77, 15)
        Me.lblNoOfTrans.TabIndex = 16
        Me.lblNoOfTrans.Text = "NoOfTrans"
        '
        'dgvLIST_RECEIVABLE_PAYMENTDataGridView
        '
        Me.dgvLIST_RECEIVABLE_PAYMENTDataGridView.AllowUserToAddRows = False
        Me.dgvLIST_RECEIVABLE_PAYMENTDataGridView.AllowUserToDeleteRows = False
        Me.dgvLIST_RECEIVABLE_PAYMENTDataGridView.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvLIST_RECEIVABLE_PAYMENTDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvLIST_RECEIVABLE_PAYMENTDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLIST_RECEIVABLE_PAYMENTDataGridView.Location = New System.Drawing.Point(6, 51)
        Me.dgvLIST_RECEIVABLE_PAYMENTDataGridView.Name = "dgvLIST_RECEIVABLE_PAYMENTDataGridView"
        Me.dgvLIST_RECEIVABLE_PAYMENTDataGridView.ReadOnly = True
        Me.dgvLIST_RECEIVABLE_PAYMENTDataGridView.RowHeadersWidth = 24
        Me.dgvLIST_RECEIVABLE_PAYMENTDataGridView.Size = New System.Drawing.Size(674, 218)
        Me.dgvLIST_RECEIVABLE_PAYMENTDataGridView.TabIndex = 19
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtCustomer)
        Me.GroupBox2.Controls.Add(Me.lblCustomerName)
        Me.GroupBox2.Controls.Add(Me.dgvLIST_RECEIVABLE_PAYMENTDataGridView)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 140)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(686, 275)
        Me.GroupBox2.TabIndex = 20
        Me.GroupBox2.TabStop = False
        '
        'txtCustomer
        '
        Me.txtCustomer.Location = New System.Drawing.Point(136, 19)
        Me.txtCustomer.Name = "txtCustomer"
        Me.txtCustomer.Size = New System.Drawing.Size(160, 22)
        Me.txtCustomer.TabIndex = 21
        '
        'lblCustomerName
        '
        Me.lblCustomerName.Location = New System.Drawing.Point(6, 22)
        Me.lblCustomerName.Name = "lblCustomerName"
        Me.lblCustomerName.Size = New System.Drawing.Size(124, 19)
        Me.lblCustomerName.TabIndex = 20
        Me.lblCustomerName.Text = "Customer Name :"
        Me.lblCustomerName.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'frmListReceivablePayment
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(710, 448)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.lblTotalAmount)
        Me.Controls.Add(Me.lblNoOfTrans)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmListReceivablePayment"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "List Receivable Payment"
        Me.Text = "List Receivable Payment"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvLIST_RECEIVABLE_PAYMENTDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdGenerate As System.Windows.Forms.Button
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lblTotalAmount As System.Windows.Forms.Label
    Friend WithEvents lblNoOfTrans As System.Windows.Forms.Label
    Friend WithEvents dgvLIST_RECEIVABLE_PAYMENTDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtCustomer As System.Windows.Forms.TextBox
    Friend WithEvents lblCustomerName As System.Windows.Forms.Label
    Friend WithEvents cmdReport As System.Windows.Forms.Button
    Friend WithEvents dtpPeriod As System.Windows.Forms.DateTimePicker
End Class
