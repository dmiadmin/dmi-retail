﻿Public Class frmRepActivePayable
    Public date1, date2 As Date
    Private Sub frmRepActivePayable_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DB_DMI_RETAILDataSetALL.COMPANY_INFORMATION' table. You can move, or remove it, as needed.
        Me.COMPANY_INFORMATIONTableAdapter.Fill(Me.DB_DMI_RETAILDataSetALL.COMPANY_INFORMATION)
        'TODO: This line of code loads data into the 'DB_DMI_RETAILDataSetALL.SP_LIST_PAYABLE' table. You can move, or remove it, as needed.
        Me.SP_LIST_PAYABLETableAdapter.Fill(Me.DB_DMI_RETAILDataSetALL.SP_LIST_PAYABLE, date1, date2)

        Me.ReportViewer1.RefreshReport()

        Dim newPageSettings As New System.Drawing.Printing.PageSettings
        newPageSettings.Margins = New System.Drawing.Printing.Margins(50, 50, 35, 35)
        newPageSettings.PaperSize = New System.Drawing.Printing.PaperSize("A4", 827, 1169)
        Me.ReportViewer1.SetPageSettings(newPageSettings)

        If Not tmpReport Is Nothing Then tmpReport.Close()
        tmpReport = Me
        Me.ReportViewer1.RefreshReport()
        Me.ReportViewer1.RefreshReport()
    End Sub
End Class