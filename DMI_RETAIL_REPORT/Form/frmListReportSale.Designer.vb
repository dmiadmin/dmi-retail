﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListReportSale
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListReportSale))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dtpPeriod = New System.Windows.Forms.DateTimePicker()
        Me.lblPeriod = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblTo = New System.Windows.Forms.Label()
        Me.dtpPeriod2 = New System.Windows.Forms.DateTimePicker()
        Me.cmdReport = New System.Windows.Forms.Button()
        Me.cmdGenerate = New System.Windows.Forms.Button()
        Me.SP_SALE_REPORTDataGridView = New System.Windows.Forms.DataGridView()
        Me.SALE_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_OF_ITEM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INVOICE_TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GRAND_TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SP_SALE_REPORT_MONTHLYBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_SALE = New DMI_RETAIL_REPORT.DS_SALE()
        Me.SP_SALE_REPORTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_SALE_REPORTTableAdapter = New DMI_RETAIL_REPORT.DS_SALETableAdapters.SP_SALE_REPORTTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_REPORT.DS_SALETableAdapters.TableAdapterManager()
        Me.lblNoOfTrans = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.SP_SALE_REPORT_DAILYBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_SALE_REPORT_DAILYTableAdapter = New DMI_RETAIL_REPORT.DS_SALETableAdapters.SP_SALE_REPORT_DAILYTableAdapter()
        Me.SP_SALE_REPORT_MONTHLYTableAdapter = New DMI_RETAIL_REPORT.DS_SALETableAdapters.SP_SALE_REPORT_MONTHLYTableAdapter()
        Me.lblItem = New System.Windows.Forms.Label()
        Me.lblInvoice = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.SP_SALE_REPORTDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_SALE_REPORT_MONTHLYBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_SALE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_SALE_REPORTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.SP_SALE_REPORT_DAILYBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dtpPeriod
        '
        Me.dtpPeriod.Location = New System.Drawing.Point(129, 27)
        Me.dtpPeriod.Name = "dtpPeriod"
        Me.dtpPeriod.ShowUpDown = True
        Me.dtpPeriod.Size = New System.Drawing.Size(143, 22)
        Me.dtpPeriod.TabIndex = 3
        '
        'lblPeriod
        '
        Me.lblPeriod.Location = New System.Drawing.Point(7, 30)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(116, 19)
        Me.lblPeriod.TabIndex = 4
        Me.lblPeriod.Text = "Time Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblTo)
        Me.GroupBox1.Controls.Add(Me.dtpPeriod2)
        Me.GroupBox1.Controls.Add(Me.cmdReport)
        Me.GroupBox1.Controls.Add(Me.dtpPeriod)
        Me.GroupBox1.Controls.Add(Me.cmdGenerate)
        Me.GroupBox1.Controls.Add(Me.lblPeriod)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(547, 126)
        Me.GroupBox1.TabIndex = 12
        Me.GroupBox1.TabStop = False
        '
        'lblTo
        '
        Me.lblTo.Location = New System.Drawing.Point(276, 30)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(32, 19)
        Me.lblTo.TabIndex = 15
        Me.lblTo.Text = "To"
        Me.lblTo.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'dtpPeriod2
        '
        Me.dtpPeriod2.Location = New System.Drawing.Point(320, 27)
        Me.dtpPeriod2.Name = "dtpPeriod2"
        Me.dtpPeriod2.ShowUpDown = True
        Me.dtpPeriod2.Size = New System.Drawing.Size(143, 22)
        Me.dtpPeriod2.TabIndex = 14
        '
        'cmdReport
        '
        Me.cmdReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdReport.Image = Global.DMI_RETAIL_REPORT.My.Resources.Resources.Printer_small
        Me.cmdReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdReport.Location = New System.Drawing.Point(451, 91)
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(90, 24)
        Me.cmdReport.TabIndex = 13
        Me.cmdReport.Text = "Print [F12]"
        Me.cmdReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.cmdReport.UseVisualStyleBackColor = True
        '
        'cmdGenerate
        '
        Me.cmdGenerate.Image = CType(resources.GetObject("cmdGenerate.Image"), System.Drawing.Image)
        Me.cmdGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdGenerate.Location = New System.Drawing.Point(228, 65)
        Me.cmdGenerate.Name = "cmdGenerate"
        Me.cmdGenerate.Size = New System.Drawing.Size(99, 39)
        Me.cmdGenerate.TabIndex = 9
        Me.cmdGenerate.Text = "Generate"
        Me.cmdGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdGenerate.UseVisualStyleBackColor = True
        '
        'SP_SALE_REPORTDataGridView
        '
        Me.SP_SALE_REPORTDataGridView.AllowUserToAddRows = False
        Me.SP_SALE_REPORTDataGridView.AllowUserToDeleteRows = False
        Me.SP_SALE_REPORTDataGridView.AutoGenerateColumns = False
        Me.SP_SALE_REPORTDataGridView.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SP_SALE_REPORTDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.SP_SALE_REPORTDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.SP_SALE_REPORTDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SALE_DATE, Me.NO_OF_ITEM, Me.INVOICE_TOTAL, Me.GRAND_TOTAL})
        Me.SP_SALE_REPORTDataGridView.DataSource = Me.SP_SALE_REPORT_MONTHLYBindingSource
        Me.SP_SALE_REPORTDataGridView.Location = New System.Drawing.Point(6, 18)
        Me.SP_SALE_REPORTDataGridView.Name = "SP_SALE_REPORTDataGridView"
        Me.SP_SALE_REPORTDataGridView.ReadOnly = True
        Me.SP_SALE_REPORTDataGridView.Size = New System.Drawing.Size(535, 356)
        Me.SP_SALE_REPORTDataGridView.TabIndex = 14
        '
        'SALE_DATE
        '
        Me.SALE_DATE.DataPropertyName = "SALE_DATE"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "dd-MMM-yyyy"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.SALE_DATE.DefaultCellStyle = DataGridViewCellStyle2
        Me.SALE_DATE.HeaderText = "Sale Date"
        Me.SALE_DATE.Name = "SALE_DATE"
        Me.SALE_DATE.ReadOnly = True
        '
        'NO_OF_ITEM
        '
        Me.NO_OF_ITEM.DataPropertyName = "NO_OF_ITEM"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.NO_OF_ITEM.DefaultCellStyle = DataGridViewCellStyle3
        Me.NO_OF_ITEM.HeaderText = "Total Item"
        Me.NO_OF_ITEM.Name = "NO_OF_ITEM"
        Me.NO_OF_ITEM.ReadOnly = True
        '
        'INVOICE_TOTAL
        '
        Me.INVOICE_TOTAL.DataPropertyName = "INVOICE_TOTAL"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.INVOICE_TOTAL.DefaultCellStyle = DataGridViewCellStyle4
        Me.INVOICE_TOTAL.HeaderText = "Total Invoice"
        Me.INVOICE_TOTAL.Name = "INVOICE_TOTAL"
        Me.INVOICE_TOTAL.ReadOnly = True
        '
        'GRAND_TOTAL
        '
        Me.GRAND_TOTAL.DataPropertyName = "GRAND_TOTAL"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N0"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.GRAND_TOTAL.DefaultCellStyle = DataGridViewCellStyle5
        Me.GRAND_TOTAL.HeaderText = "Grand Total"
        Me.GRAND_TOTAL.Name = "GRAND_TOTAL"
        Me.GRAND_TOTAL.ReadOnly = True
        '
        'SP_SALE_REPORT_MONTHLYBindingSource
        '
        Me.SP_SALE_REPORT_MONTHLYBindingSource.DataMember = "SP_SALE_REPORT_MONTHLY"
        Me.SP_SALE_REPORT_MONTHLYBindingSource.DataSource = Me.DS_SALE
        '
        'DS_SALE
        '
        Me.DS_SALE.DataSetName = "DS_SALE"
        Me.DS_SALE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SP_SALE_REPORTBindingSource
        '
        Me.SP_SALE_REPORTBindingSource.DataMember = "SP_SALE_REPORT"
        Me.SP_SALE_REPORTBindingSource.DataSource = Me.DS_SALE
        '
        'SP_SALE_REPORTTableAdapter
        '
        Me.SP_SALE_REPORTTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.CUSTOMERTableAdapter = Nothing
        Me.TableAdapterManager.PAYMENT_METHODTableAdapter = Nothing
        Me.TableAdapterManager.PRODUCTTableAdapter = Nothing
        Me.TableAdapterManager.RECEIVABLETableAdapter = Nothing
        Me.TableAdapterManager.SALE_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.SALESMANTableAdapter = Nothing
        Me.TableAdapterManager.SALETableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_REPORT.DS_SALETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.WAREHOUSETableAdapter = Nothing
        '
        'lblNoOfTrans
        '
        Me.lblNoOfTrans.AutoSize = True
        Me.lblNoOfTrans.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoOfTrans.Location = New System.Drawing.Point(17, 515)
        Me.lblNoOfTrans.Name = "lblNoOfTrans"
        Me.lblNoOfTrans.Size = New System.Drawing.Size(48, 15)
        Me.lblNoOfTrans.TabIndex = 15
        Me.lblNoOfTrans.Text = "Label1"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(365, 515)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(48, 15)
        Me.lblTotal.TabIndex = 16
        Me.lblTotal.Text = "Label2"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.SP_SALE_REPORTDataGridView)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 130)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(547, 380)
        Me.GroupBox2.TabIndex = 17
        Me.GroupBox2.TabStop = False
        '
        'SP_SALE_REPORT_DAILYBindingSource
        '
        Me.SP_SALE_REPORT_DAILYBindingSource.DataMember = "SP_SALE_REPORT_DAILY"
        Me.SP_SALE_REPORT_DAILYBindingSource.DataSource = Me.DS_SALE
        '
        'SP_SALE_REPORT_DAILYTableAdapter
        '
        Me.SP_SALE_REPORT_DAILYTableAdapter.ClearBeforeFill = True
        '
        'SP_SALE_REPORT_MONTHLYTableAdapter
        '
        Me.SP_SALE_REPORT_MONTHLYTableAdapter.ClearBeforeFill = True
        '
        'lblItem
        '
        Me.lblItem.AutoSize = True
        Me.lblItem.Location = New System.Drawing.Point(17, 546)
        Me.lblItem.Name = "lblItem"
        Me.lblItem.Size = New System.Drawing.Size(45, 15)
        Me.lblItem.TabIndex = 18
        Me.lblItem.Text = "Label1"
        '
        'lblInvoice
        '
        Me.lblInvoice.AutoSize = True
        Me.lblInvoice.Location = New System.Drawing.Point(365, 546)
        Me.lblInvoice.Name = "lblInvoice"
        Me.lblInvoice.Size = New System.Drawing.Size(45, 15)
        Me.lblInvoice.TabIndex = 19
        Me.lblInvoice.Text = "Label1"
        '
        'frmListReportSale
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(571, 564)
        Me.Controls.Add(Me.lblInvoice)
        Me.Controls.Add(Me.lblItem)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.lblNoOfTrans)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmListReportSale"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "List Sale Report"
        Me.Text = "List Sale Report"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.SP_SALE_REPORTDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_SALE_REPORT_MONTHLYBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_SALE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_SALE_REPORTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.SP_SALE_REPORT_DAILYBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dtpPeriod As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cmdGenerate As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents DS_SALE As DMI_RETAIL_REPORT.DS_SALE
    Friend WithEvents SP_SALE_REPORTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_SALE_REPORTTableAdapter As DMI_RETAIL_REPORT.DS_SALETableAdapters.SP_SALE_REPORTTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_REPORT.DS_SALETableAdapters.TableAdapterManager
    Friend WithEvents SP_SALE_REPORTDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents lblNoOfTrans As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents cmdReport As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents dtpPeriod2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents SP_SALE_REPORT_DAILYBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_SALE_REPORT_DAILYTableAdapter As DMI_RETAIL_REPORT.DS_SALETableAdapters.SP_SALE_REPORT_DAILYTableAdapter
    Friend WithEvents SP_SALE_REPORT_MONTHLYBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_SALE_REPORT_MONTHLYTableAdapter As DMI_RETAIL_REPORT.DS_SALETableAdapters.SP_SALE_REPORT_MONTHLYTableAdapter
    Friend WithEvents lblItem As System.Windows.Forms.Label
    Friend WithEvents lblInvoice As System.Windows.Forms.Label
    Friend WithEvents SALE_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_OF_ITEM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INVOICE_TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GRAND_TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
