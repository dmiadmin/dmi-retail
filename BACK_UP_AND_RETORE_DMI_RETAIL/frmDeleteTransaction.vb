﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Data

Public Class frmDeleteTransaction
    Dim xConn As New SqlConnection
    Dim xCmd As SqlCommand
    'Dim Path_Name As String = Application.StartupPath
    'Dim Path_Data As String = Replace(Path_Name, Mid(Path_Name, 51, 71), "")
    'Dim PathDb As String = Path_Data & "\DMI_RETAIL\bin\Debug\DB_DMI_RETAIL.mdf"
    Public Language, ServerName, DatabaseName As String
    Public tmpConfig() As String
    Sub connection()
        If File.Exists(Application.StartupPath & "\Config.inf") Then
            tmpConfig = File.ReadAllLines(Application.StartupPath & "\Config.inf")
            For x As Integer = 0 To tmpConfig.Count - 1
                If tmpConfig(x).StartsWith("[Language]") Then
                    Language = tmpConfig(x).Split("=")(1)
                ElseIf tmpConfig(x).StartsWith("[Server Name]") Then
                    ServerName = tmpConfig(x).Split("=")(1)
                ElseIf tmpConfig(x).StartsWith("[Database Name]") Then
                    DatabaseName = tmpConfig(x).Split("=")(1)
                End If
            Next
        End If
        xConn.Close()
        xConn.ConnectionString = "Data Source=" & ServerName & ";Initial Catalog=" & DatabaseName & ";Persist Security Info=True;User ID=sa;Password=SA;"
        xConn.Open()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If MsgBox("Seluruh data transaksi akan dihapus." & vbCrLf & _
                  "Apakah Anda yakin?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
        Try
            Call connection()
            Dim sql As String = "DECLARE	@return_value int" & vbCrLf & _
                                "EXEC	@return_value = [dbo].[SP_DELETE_TRANSACTION]" & vbCrLf & _
                                "SELECT	'Return Value' = @return_value"
            xCmd = New SqlCommand(sql, xConn)
            xCmd.ExecuteNonQuery()
            MsgBox("Penghapusan data berhasil.", MsgBoxStyle.Information, "DMI Retail")
        Catch ex As Exception
            MsgBox("Penghapusan data gagal.", MsgBoxStyle.Information, "DMI Retail")
        End Try
        xConn.Close()
        Me.Close()
    End Sub
End Class