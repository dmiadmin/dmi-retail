﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSearchDeliveryOrder
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSearchDeliveryOrder))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DS_DELIVERY_SALE_ORDER = New DMI_RETAIL_SALE.DS_DELIVERY_SALE_ORDER()
        Me.SP_SEARCH_DELIVERY_ORDERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_SEARCH_DELIVERY_ORDERTableAdapter = New DMI_RETAIL_SALE.DS_DELIVERY_SALE_ORDERTableAdapters.SP_SEARCH_DELIVERY_ORDERTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_SALE.DS_DELIVERY_SALE_ORDERTableAdapters.TableAdapterManager()
        Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.dgvSearchDO = New System.Windows.Forms.DataGridView()
        Me.txtCustomer = New System.Windows.Forms.TextBox()
        Me.txtInvoiceNo = New System.Windows.Forms.TextBox()
        Me.lblCustomer = New System.Windows.Forms.Label()
        Me.lblInvoice = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.SALE_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RECEIPT_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CUSTOMER_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CUSTOMER_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALE_ORDER_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALE_ORDER_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.REQUEST_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SHIP_TO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_OF_ITEM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AMOUNT_DISCOUNT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TAX_AMOUNT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GRAND_TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PAYMENT_METHOD_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PAYMENT_METHOD_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WAREHOUSE_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WAREHOUSE_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALESMAN_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALESMAN_NAME1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DS_DELIVERY_SALE_ORDER, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_SEARCH_DELIVERY_ORDERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator.SuspendLayout()
        CType(Me.dgvSearchDO, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'DS_DELIVERY_SALE_ORDER
        '
        Me.DS_DELIVERY_SALE_ORDER.DataSetName = "DS_DELIVERY_SALE_ORDER"
        Me.DS_DELIVERY_SALE_ORDER.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SP_SEARCH_DELIVERY_ORDERBindingSource
        '
        Me.SP_SEARCH_DELIVERY_ORDERBindingSource.DataMember = "SP_SEARCH_DELIVERY_ORDER"
        Me.SP_SEARCH_DELIVERY_ORDERBindingSource.DataSource = Me.DS_DELIVERY_SALE_ORDER
        '
        'SP_SEARCH_DELIVERY_ORDERTableAdapter
        '
        Me.SP_SEARCH_DELIVERY_ORDERTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.DELIVERY_SALE_ORDER_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.DELIVERY_SALE_ORDER_HEADERTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_SALE.DS_DELIVERY_SALE_ORDERTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'SP_SEARCH_DELIVERY_ORDERBindingNavigator
        '
        Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator.AddNewItem = Nothing
        Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator.BindingSource = Me.SP_SEARCH_DELIVERY_ORDERBindingSource
        Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator.DeleteItem = Nothing
        Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator.Name = "SP_SEARCH_DELIVERY_ORDERBindingNavigator"
        Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator.Size = New System.Drawing.Size(1051, 25)
        Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator.TabIndex = 0
        Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'dgvSearchDO
        '
        Me.dgvSearchDO.AllowUserToAddRows = False
        Me.dgvSearchDO.AllowUserToDeleteRows = False
        Me.dgvSearchDO.AutoGenerateColumns = False
        Me.dgvSearchDO.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSearchDO.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSearchDO.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSearchDO.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SALE_ID, Me.RECEIPT_NO, Me.CUSTOMER_ID, Me.CUSTOMER_NAME, Me.SALE_ORDER_ID, Me.SALE_ORDER_DATE, Me.REQUEST_DATE, Me.SHIP_TO, Me.NO_OF_ITEM, Me.AMOUNT_DISCOUNT, Me.TAX_AMOUNT, Me.GRAND_TOTAL, Me.PAYMENT_METHOD_ID, Me.PAYMENT_METHOD_NAME, Me.WAREHOUSE_ID, Me.WAREHOUSE_NAME, Me.SALESMAN_ID, Me.SALESMAN_NAME1})
        Me.dgvSearchDO.DataSource = Me.SP_SEARCH_DELIVERY_ORDERBindingSource
        Me.dgvSearchDO.Location = New System.Drawing.Point(11, 121)
        Me.dgvSearchDO.Name = "dgvSearchDO"
        Me.dgvSearchDO.ReadOnly = True
        Me.dgvSearchDO.Size = New System.Drawing.Size(1029, 220)
        Me.dgvSearchDO.TabIndex = 1
        '
        'txtCustomer
        '
        Me.txtCustomer.Location = New System.Drawing.Point(150, 14)
        Me.txtCustomer.Name = "txtCustomer"
        Me.txtCustomer.Size = New System.Drawing.Size(178, 22)
        Me.txtCustomer.TabIndex = 2
        '
        'txtInvoiceNo
        '
        Me.txtInvoiceNo.Location = New System.Drawing.Point(150, 42)
        Me.txtInvoiceNo.Name = "txtInvoiceNo"
        Me.txtInvoiceNo.Size = New System.Drawing.Size(178, 22)
        Me.txtInvoiceNo.TabIndex = 3
        '
        'lblCustomer
        '
        Me.lblCustomer.AutoSize = True
        Me.lblCustomer.Location = New System.Drawing.Point(27, 17)
        Me.lblCustomer.Name = "lblCustomer"
        Me.lblCustomer.Size = New System.Drawing.Size(103, 15)
        Me.lblCustomer.TabIndex = 4
        Me.lblCustomer.Text = "Customer Name"
        '
        'lblInvoice
        '
        Me.lblInvoice.AutoSize = True
        Me.lblInvoice.Location = New System.Drawing.Point(31, 45)
        Me.lblInvoice.Name = "lblInvoice"
        Me.lblInvoice.Size = New System.Drawing.Size(99, 15)
        Me.lblInvoice.TabIndex = 5
        Me.lblInvoice.Text = "Invoice Number"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblInvoice)
        Me.GroupBox1.Controls.Add(Me.lblCustomer)
        Me.GroupBox1.Controls.Add(Me.txtInvoiceNo)
        Me.GroupBox1.Controls.Add(Me.txtCustomer)
        Me.GroupBox1.Location = New System.Drawing.Point(11, 33)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1028, 73)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        '
        'SALE_ID
        '
        Me.SALE_ID.DataPropertyName = "SALE_ID"
        Me.SALE_ID.HeaderText = "SALE_ID"
        Me.SALE_ID.Name = "SALE_ID"
        Me.SALE_ID.ReadOnly = True
        Me.SALE_ID.Visible = False
        '
        'RECEIPT_NO
        '
        Me.RECEIPT_NO.DataPropertyName = "RECEIPT_NO"
        Me.RECEIPT_NO.HeaderText = "Invoice No"
        Me.RECEIPT_NO.Name = "RECEIPT_NO"
        Me.RECEIPT_NO.ReadOnly = True
        Me.RECEIPT_NO.Width = 80
        '
        'CUSTOMER_ID
        '
        Me.CUSTOMER_ID.DataPropertyName = "CUSTOMER_ID"
        Me.CUSTOMER_ID.HeaderText = "CUSTOMER_ID"
        Me.CUSTOMER_ID.Name = "CUSTOMER_ID"
        Me.CUSTOMER_ID.ReadOnly = True
        Me.CUSTOMER_ID.Visible = False
        '
        'CUSTOMER_NAME
        '
        Me.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME"
        Me.CUSTOMER_NAME.HeaderText = "Customer"
        Me.CUSTOMER_NAME.Name = "CUSTOMER_NAME"
        Me.CUSTOMER_NAME.ReadOnly = True
        '
        'SALE_ORDER_ID
        '
        Me.SALE_ORDER_ID.DataPropertyName = "SALE_ORDER_ID"
        Me.SALE_ORDER_ID.HeaderText = "SALE_ORDER_ID"
        Me.SALE_ORDER_ID.Name = "SALE_ORDER_ID"
        Me.SALE_ORDER_ID.ReadOnly = True
        Me.SALE_ORDER_ID.Visible = False
        '
        'SALE_ORDER_DATE
        '
        Me.SALE_ORDER_DATE.DataPropertyName = "SALE_ORDER_DATE"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "dd-MMM-yyyy"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.SALE_ORDER_DATE.DefaultCellStyle = DataGridViewCellStyle2
        Me.SALE_ORDER_DATE.HeaderText = "Order Date"
        Me.SALE_ORDER_DATE.Name = "SALE_ORDER_DATE"
        Me.SALE_ORDER_DATE.ReadOnly = True
        Me.SALE_ORDER_DATE.Width = 80
        '
        'REQUEST_DATE
        '
        Me.REQUEST_DATE.DataPropertyName = "REQUEST_DATE"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "dd-MMM-yyyy"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.REQUEST_DATE.DefaultCellStyle = DataGridViewCellStyle3
        Me.REQUEST_DATE.HeaderText = "Request Date"
        Me.REQUEST_DATE.Name = "REQUEST_DATE"
        Me.REQUEST_DATE.ReadOnly = True
        Me.REQUEST_DATE.Width = 80
        '
        'SHIP_TO
        '
        Me.SHIP_TO.DataPropertyName = "SHIP_TO"
        Me.SHIP_TO.HeaderText = "Ship To"
        Me.SHIP_TO.Name = "SHIP_TO"
        Me.SHIP_TO.ReadOnly = True
        '
        'NO_OF_ITEM
        '
        Me.NO_OF_ITEM.DataPropertyName = "NO_OF_ITEM"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.NO_OF_ITEM.DefaultCellStyle = DataGridViewCellStyle4
        Me.NO_OF_ITEM.HeaderText = "No Of Item"
        Me.NO_OF_ITEM.Name = "NO_OF_ITEM"
        Me.NO_OF_ITEM.ReadOnly = True
        Me.NO_OF_ITEM.Width = 50
        '
        'AMOUNT_DISCOUNT
        '
        Me.AMOUNT_DISCOUNT.DataPropertyName = "AMOUNT_DISCOUNT"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N0"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.AMOUNT_DISCOUNT.DefaultCellStyle = DataGridViewCellStyle5
        Me.AMOUNT_DISCOUNT.HeaderText = "Discount"
        Me.AMOUNT_DISCOUNT.Name = "AMOUNT_DISCOUNT"
        Me.AMOUNT_DISCOUNT.ReadOnly = True
        Me.AMOUNT_DISCOUNT.Width = 75
        '
        'TAX_AMOUNT
        '
        Me.TAX_AMOUNT.DataPropertyName = "TAX_AMOUNT"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N0"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.TAX_AMOUNT.DefaultCellStyle = DataGridViewCellStyle6
        Me.TAX_AMOUNT.HeaderText = "Tax"
        Me.TAX_AMOUNT.Name = "TAX_AMOUNT"
        Me.TAX_AMOUNT.ReadOnly = True
        Me.TAX_AMOUNT.Width = 75
        '
        'GRAND_TOTAL
        '
        Me.GRAND_TOTAL.DataPropertyName = "GRAND_TOTAL"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N0"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.GRAND_TOTAL.DefaultCellStyle = DataGridViewCellStyle7
        Me.GRAND_TOTAL.HeaderText = "Total"
        Me.GRAND_TOTAL.Name = "GRAND_TOTAL"
        Me.GRAND_TOTAL.ReadOnly = True
        '
        'PAYMENT_METHOD_ID
        '
        Me.PAYMENT_METHOD_ID.DataPropertyName = "PAYMENT_METHOD_ID"
        Me.PAYMENT_METHOD_ID.HeaderText = "PAYMENT_METHOD_ID"
        Me.PAYMENT_METHOD_ID.Name = "PAYMENT_METHOD_ID"
        Me.PAYMENT_METHOD_ID.ReadOnly = True
        Me.PAYMENT_METHOD_ID.Visible = False
        '
        'PAYMENT_METHOD_NAME
        '
        Me.PAYMENT_METHOD_NAME.DataPropertyName = "PAYMENT_METHOD_NAME"
        Me.PAYMENT_METHOD_NAME.HeaderText = "Payment Method"
        Me.PAYMENT_METHOD_NAME.Name = "PAYMENT_METHOD_NAME"
        Me.PAYMENT_METHOD_NAME.ReadOnly = True
        Me.PAYMENT_METHOD_NAME.Width = 80
        '
        'WAREHOUSE_ID
        '
        Me.WAREHOUSE_ID.DataPropertyName = "WAREHOUSE_ID"
        Me.WAREHOUSE_ID.HeaderText = "WAREHOUSE_ID"
        Me.WAREHOUSE_ID.Name = "WAREHOUSE_ID"
        Me.WAREHOUSE_ID.ReadOnly = True
        Me.WAREHOUSE_ID.Visible = False
        '
        'WAREHOUSE_NAME
        '
        Me.WAREHOUSE_NAME.DataPropertyName = "WAREHOUSE_NAME"
        Me.WAREHOUSE_NAME.HeaderText = "Warehouse"
        Me.WAREHOUSE_NAME.Name = "WAREHOUSE_NAME"
        Me.WAREHOUSE_NAME.ReadOnly = True
        Me.WAREHOUSE_NAME.Width = 80
        '
        'SALESMAN_ID
        '
        Me.SALESMAN_ID.DataPropertyName = "SALESMAN_ID"
        Me.SALESMAN_ID.HeaderText = "SALESMAN_ID"
        Me.SALESMAN_ID.Name = "SALESMAN_ID"
        Me.SALESMAN_ID.ReadOnly = True
        Me.SALESMAN_ID.Visible = False
        '
        'SALESMAN_NAME1
        '
        Me.SALESMAN_NAME1.DataPropertyName = "SALESMAN_NAME1"
        Me.SALESMAN_NAME1.HeaderText = "Salesman"
        Me.SALESMAN_NAME1.Name = "SALESMAN_NAME1"
        Me.SALESMAN_NAME1.ReadOnly = True
        Me.SALESMAN_NAME1.Width = 80
        '
        'frmSearchDeliveryOrder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1051, 378)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgvSearchDO)
        Me.Controls.Add(Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmSearchDeliveryOrder"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Search Delivery Order"
        CType(Me.DS_DELIVERY_SALE_ORDER, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_SEARCH_DELIVERY_ORDERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator.ResumeLayout(False)
        Me.SP_SEARCH_DELIVERY_ORDERBindingNavigator.PerformLayout()
        CType(Me.dgvSearchDO, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DS_DELIVERY_SALE_ORDER As DMI_RETAIL_SALE.DS_DELIVERY_SALE_ORDER
    Friend WithEvents SP_SEARCH_DELIVERY_ORDERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_SEARCH_DELIVERY_ORDERTableAdapter As DMI_RETAIL_SALE.DS_DELIVERY_SALE_ORDERTableAdapters.SP_SEARCH_DELIVERY_ORDERTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_SALE.DS_DELIVERY_SALE_ORDERTableAdapters.TableAdapterManager
    Friend WithEvents SP_SEARCH_DELIVERY_ORDERBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents dgvSearchDO As System.Windows.Forms.DataGridView
    Friend WithEvents txtCustomer As System.Windows.Forms.TextBox
    Friend WithEvents txtInvoiceNo As System.Windows.Forms.TextBox
    Friend WithEvents lblCustomer As System.Windows.Forms.Label
    Friend WithEvents lblInvoice As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents SALE_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RECEIPT_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CUSTOMER_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CUSTOMER_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALE_ORDER_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALE_ORDER_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents REQUEST_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SHIP_TO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_OF_ITEM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AMOUNT_DISCOUNT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TAX_AMOUNT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GRAND_TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PAYMENT_METHOD_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PAYMENT_METHOD_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WAREHOUSE_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WAREHOUSE_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALESMAN_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALESMAN_NAME1 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
