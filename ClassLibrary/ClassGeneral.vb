﻿Imports Microsoft.Win32
Imports System.IO
Imports System.Windows.Forms

Public Class ClassGeneral
    '----- Configuration Variables -----'
    Dim Language As String
    Dim ServerName As String
    Dim DatabaseName As String
    '----- End of Configuration Variables -----'

    '----- General Variabel -----'
    Private Declare Function GetVolumeInformation Lib "kernel32" Alias "GetVolumeInformationA" (ByVal lpRootPathName As String, ByVal lpVolumeNameBuffer As String, ByVal nVolumeNameSize As Integer, ByRef lpVolumeSerialNumber As Integer, ByVal lpMaximumComponentLength As Integer, ByVal lpFileSystemFlags As Integer, ByVal lpFileSystemNameBuffer As String, ByVal nFileSystemNameSize As Integer) As Integer
    Private Declare Function GetDriveType Lib "kernel32" Alias "GetDriveTypeA" (ByVal nDrive As String) As Long
    Dim tmpConfig() As String
    Dim USER_ID As String
    Dim User_Name As String
    Dim User_Type As String
    '----- General Variabel -----'

    Public Function GetSerialNumber(ByVal pDriveLetter As Char) As String
        Dim serial As Integer

        GetDriveType(pDriveLetter & ":\")
        GetVolumeInformation(pDriveLetter & ":\", "", 255, serial, 0, 0, vbNullString, 255)

        Return serial
    End Function

    Public Function CheckSQLServer() As Boolean
        Dim RegHCU As RegistryKey = Registry.CurrentUser

        Dim RegWord As RegistryKey = RegHCU.OpenSubKey("Microsoft.Microsoft SQL Server")

        If RegWord Is Nothing Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function CheckSQLOLEDB() As Boolean
        Dim RegClasses As RegistryKey = Registry.ClassesRoot
        Dim RegWord As RegistryKey = RegClasses.OpenSubKey("SQLOLEDB")

        If RegWord Is Nothing Then
            Return False
        Else
            Return True
        End If
    End Function

End Class
