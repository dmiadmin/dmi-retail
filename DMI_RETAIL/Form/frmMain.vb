﻿Imports System.Data.SqlClient
Imports System.Data
Imports DMI_RETAIL_MASTER

Public Class frmMain
    Dim xComm As New SqlCommand
    Dim mReader As SqlDataReader
    Dim p As New System.Diagnostics.Process

    Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            p.Kill()
        Catch
        End Try
    End Sub

    Private Sub frmMain_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus
        If Not tmpRecentForm Is Nothing Then RecentToolStripMenuItem.Text = tmpRecentForm.Text

        xComm = New SqlCommand("DECLARE	        @return_value int" & vbCrLf & _
                               "EXEC	        @return_value = [dbo].[SP_SELECT_PARAMETER]" & vbCrLf & _
                               "@DESCRIPTION    = N'SAFE STOCK CHECKING INTERVAL'" & vbCrLf & _
                               "SELECT	'Return Value' = @return_value", xConn)

        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        mReader = xComm.ExecuteReader()
        If mReader.Read = True Then
            If mReader.Item("VALUE").ToString = 0 Then
                TimerSafeStock.Enabled = False
            Else
                TimerSafeStock.Interval = CInt(mReader.Item("VALUE").ToString) * 60000
            End If
        End If

    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ObjDMIRetailMaster.SetConnections(xConn, xConn1, xConn2)
        ObjDMIRetailMaster.SetLanguage(Language)
        ObjDMIRetailMaster.SetUserInformations(UserId, UserName, UserTypeId)
        ObjDMIRetailMaster.SetMdiParentForm(Me)

        xComm = New SqlCommand("DECLARE @return_value int" & vbCrLf & _
                                "EXEC @return_value = [dbo].[SP_NOTIFICATION_SAFE_STOCK]" & vbCrLf & _
                                "SELECT	'Return Value' = @return_value", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xComm.ExecuteNonQuery()

        ToolStripSeparator1.Enabled = False
        ToolStripSeparator2.Enabled = False
        ToolStripSeparator3.Enabled = False
        ToolStripSeparator4.Enabled = False
        ToolStripSeparator5.Enabled = False
        ToolStripSeparator6.Enabled = False
        ToolStripSeparator7.Enabled = False
        ToolStripSeparator8.Enabled = False
        ToolStripSeparator9.Enabled = False
        ToolStripSeparator10.Enabled = False
        ToolStripSeparator11.Enabled = False
        ToolStripSeparator12.Enabled = False
        ToolStripSeparator13.Enabled = False
        ToolStripSeparator14.Enabled = False
        ToolStripSeparator15.Enabled = False

        xComm = New SqlCommand("DECLARE @return_value int" & vbCrLf & _
                               "EXEC @return_value = [dbo].[SP_SELECT_PARAMETER]" & vbCrLf & _
                               "@DESCRIPTION = N'SAFE STOCK CHECKING INTERVAL'" & vbCrLf & _
                               "SELECT 'Return Value' = @return_value", xConn)
        mReader = xComm.ExecuteReader()

        If mReader.Read = True Then
            If mReader.Item("VALUE").ToString = 0 Then
                TimerSafeStock.Enabled = False
            Else
                TimerSafeStock.Interval = CInt(mReader.Item("VALUE").ToString) * 60000
            End If
        End If

        ''Checking Screen Resolution, and then Assign background image
        If Screen.PrimaryScreen.Bounds.Height = 1600 And Screen.PrimaryScreen.Bounds.Width = 900 Or _
           Screen.PrimaryScreen.Bounds.Height = 1920 And Screen.PrimaryScreen.Bounds.Width = 1080 Or _
           Screen.PrimaryScreen.Bounds.Height = 1366 And Screen.PrimaryScreen.Bounds.Width = 768 Or _
           Screen.PrimaryScreen.Bounds.Height = 1280 And Screen.PrimaryScreen.Bounds.Width = 720 Then
            Me.BackgroundImage = Image.FromFile(Application.StartupPath & "/Background/DMI-Retail_1920x1080.jpg")
        ElseIf Screen.PrimaryScreen.Bounds.Height = 1600 And Screen.PrimaryScreen.Bounds.Width = 1200 Or _
               Screen.PrimaryScreen.Bounds.Height = 1280 And Screen.PrimaryScreen.Bounds.Width = 960 Or _
               Screen.PrimaryScreen.Bounds.Height = 1024 And Screen.PrimaryScreen.Bounds.Width = 768 Or _
               Screen.PrimaryScreen.Bounds.Height = 1152 And Screen.PrimaryScreen.Bounds.Width = 864 Or _
               Screen.PrimaryScreen.Bounds.Height = 800 And Screen.PrimaryScreen.Bounds.Width = 600 Then
            Me.BackgroundImage = Image.FromFile(Application.StartupPath & "/Background/DMI-Retail_1600x1200.jpg")
        ElseIf Screen.PrimaryScreen.Bounds.Height = 1280 And Screen.PrimaryScreen.Bounds.Width = 1024 Or _
               Screen.PrimaryScreen.Bounds.Height = 1200 And Screen.PrimaryScreen.Bounds.Width = 960 Then
            Me.BackgroundImage = Image.FromFile(Application.StartupPath & "/Background/DMI-Retail_1280x1024.jpg")
        ElseIf Screen.PrimaryScreen.Bounds.Height = 1280 And Screen.PrimaryScreen.Bounds.Width = 768 Or _
               Screen.PrimaryScreen.Bounds.Height = 1200 And Screen.PrimaryScreen.Bounds.Width = 720 Then
            Me.BackgroundImage = Image.FromFile(Application.StartupPath & "/Background/DMI-Retail_1280x768.jpg")
        ElseIf Screen.PrimaryScreen.Bounds.Height = 1920 And Screen.PrimaryScreen.Bounds.Width = 1200 Or _
               Screen.PrimaryScreen.Bounds.Height = 1280 And Screen.PrimaryScreen.Bounds.Width = 800 Or _
               Screen.PrimaryScreen.Bounds.Height = 1024 And Screen.PrimaryScreen.Bounds.Width = 750 Or _
               Screen.PrimaryScreen.Bounds.Height = 1440 And Screen.PrimaryScreen.Bounds.Width = 900 Or _
               Screen.PrimaryScreen.Bounds.Height = 1680 And Screen.PrimaryScreen.Bounds.Width = 1050 Then
            Me.BackgroundImage = Image.FromFile(Application.StartupPath & "/Background/DMI-Retail_1920x1200.jpg")
        ElseIf Screen.PrimaryScreen.Bounds.Height = 1280 And Screen.PrimaryScreen.Bounds.Width = 854 Then
            Me.BackgroundImage = Image.FromFile(Application.StartupPath & "/Background/DMI-Retail_1280x854.jpg")
        ElseIf Screen.PrimaryScreen.Bounds.Height = 1280 And Screen.PrimaryScreen.Bounds.Width = 600 Then
            Me.BackgroundImage = Image.FromFile(Application.StartupPath & "/Background/DMI-Retail_1280x600.jpg")
        Else
            Me.BackgroundImage = Image.FromFile(Application.StartupPath & "/Background/DMI-Retail_1920x1080.jpg")
        End If

        'frmNotification.get_notification()

        If Language = "Indonesian" Then

            StatusLabelUser.Text = "Nama User : " & UserName

            StatusLabelNotif.Text = frmNotification.Label1.Text

            RecentFormToolStripMenuItem.Text = "Form Terakhir Dibuka"

            ReferenceDataToolStripMenuItem.Text = "Data Referensi"
            PaymentMethodToolStripMenuItem.Text = "Metode Pembayaran"
            ListPaymentMethodToolStripMenuItem.Text = "Daftar"
            EntryPaymentMethodToolStripMenuItem.Text = "Input"
            CompanyInformationToolStripMenuItem.Text = "Info Perusahaan"
            UserToolStripMenuItem1.Text = "User"
            ListUserToolStripMenuItem.Text = "Daftar"
            EntryUserToolStripMenuItem1.Text = "Input"
            ChangePasswordToolStripMenuItem.Text = "Ganti Password"
            EntryUserToolStripMenuItem1.Text = "Tambah User"
            AccessPrivilegeToolStripMenuItem.Text = "Input Akses"

            menuMasterData.Text = "Data Master"
            CustomerToolStripMenuItem.Text = "Pelanggan"
            ListCustomerToolStripMenuItem.Text = "Daftar Pelanggan"
            EntryCustomerToolStripMenuItem.Text = "Input Pelanggan"
            VendorToolStripMenuItem.Text = "Supplier"
            ListVendorToolStripMenuItem.Text = "Daftar Supplier"
            EntryVendorToolStripMenuItem.Text = "Input Supplier"
            ProductToolStripMenuItem.Text = "Produk"
            ListToolStripMenuItem10.Text = "Daftar Produk"
            EntryProductToolStripMenuItem.Text = "Input Produk"
            WarehouseToolStripMenuItem.Text = "Gudang"
            ListWarehouseToolStripMenuItem.Text = "Daftar Gudang"
            EntryWarehouseToolStripMenuItem.Text = "Input Gudang"
            LocationToolStripMenuItem.Text = "Lokasi Penyimpanan"
            ListLocationToolStripMenuItem.Text = "Daftar Lokasi"
            EntryLocationToolStripMenuItem.Text = "Input Lokasi"
            AccountToolStripMenuItem.Text = "Akun"
            ListAccountToolStripMenuItem.Text = "Daftar Akun"
            EntryAccountToolStripMenuItem.Text = "Input Akun"
            ListSalesmanToolStripMenuItem.Text = "Daftar Salesman"
            EntrySalesmanToolStripMenuItem.Text = "Input Salesman"

            TransactionToolStripMenuItem.Text = "Transaksi"
            OpeningBalanceToolStripMenuItem.Text = "Saldo Awal"
            PurchaseToolStripMenuItem.Text = "Pembelian"
            SaleToolStripMenuItem.Text = "Penjualan"
            AdjustmentToolStripMenuItem.Text = "Penyesuaian"
            PayableToolStripMenuItem.Text = "Hutang"
            ReceivableToolStripMenuItem.Text = "Piutang"
            ReturnPurchaseToolStripMenuItem.Text = "Retur Pembelian"
            ReturnSaleToolStripMenuItem.Text = "Retur Penjualan"

            StockToolStripMenuItem.Text = "Stok"
            ProductStockToolStripMenuItem.Text = "Stok Barang"
            ''''new edit Report''''
            StockPerProductToolStripMenuItem.Text = "Stock per Barang"
            ProductWarehouseToolStripMenuItem.Text = "Stok per Gudang"
            ''''new edit Report''''
            StockPerWarehouseToolStripMenuItem.Text = "Stock per Gudang"
            ProductDistributionToolStripMenuItem.Text = "Distribusi Produk"
            EndingStockToolStripMenuItem.Text = "Stok Akhir"
            ''''new edit Report''''
            StockPerPriceToolStripMenuItem.Text = "Stock per Harga"
            StockCardToolStripMenuItem.Text = "Kartu Stok"
            ''''new edit Report''''
            StockCardToolStripMenuItem1.Text = "Kartu Stok"
            GroupingProductToolStripMenuItem.Text = "Pengelompokkan Produk"
            SplitProductToolStripMenuItem.Text = "Pemecahan Produk"

            ListToolStripMenuItem3.Text = "Laporan"
            SoldProductToolStripMenuItem.Text = "Produk terjual"
            ''''New Edit Report''''
            SoldProductToolStripMenuItem1.Text = "Produk terjual"
            SalesReportToolStripMenuItem.Text = "Laporan Penjualan"
            DailyInvoiceToolStripMenuItem.Text = "Faktur Harian"
            ByDailySoldProductToolStripMenuItem.Text = "Produk terjual per Hari"
            ActivePayableToolStripMenuItem1.Text = "Hutang Aktif"
            ActiveReceivableToolStripMenuItem1.Text = "Piutang Aktif"
            StockBalanceToolStripMenuItem.Text = "Saldo Stok"
            ByPurchasingPriceToolStripMenuItem.Text = "Harga Pembelian"
            ProductMovementToolStripMenuItem.Text = "Pergerakan Produk"
            ''''New edit report''''
            ProductMovementToolStripMenuItem1.Text = "Pergerakan Produk"
            SalesPerInvoiceToolStripMenuItem.Text = "Penjualan per Faktur"
            ''''New Edit report''''
            PerInvoiceToolStripMenuItem.Text = "Penjualan per Faktur"
            SalesPerProductToolStripMenuItem.Text = "Penjualan per Produk"
            PerProductToolStripMenuItem.Text = "Penjualan per Produk"
            ProductSoldPerInvoiceToolStripMenuItem.Text = "Penjualan oleh Sales"

            ''''New Report''''
            TransactionToolStripMenuItem1.Text = "Transaksi"
            StockToolStripMenuItem1.Text = "Stok"
            PurchaseToolStripMenuItem1.Text = "Pembelian"
            ByPaymentMethodToolStripMenuItem1.Text = "per Cara Pembayaran"
            ByVendorToolStripMenuItem.Text = "per Supplier"
            ByInvoiceToolStripMenuItem1.Text = "per Faktur"
            ByStatusToolStripMenuItem1.Text = "Pembatalan"
            GeneralToolStripMenuItem1.Text = "Umum"
            DailyPurchaseToolStripMenuItem.Text = "Pembelian per hari"
            MonthlyPurchaseToolStripMenuItem.Text = "Pembelian per bulan"

            SaleToolStripMenuItem2.Text = "Penjualan"
            GeneralToolStripMenuItem.Text = "Umum"
            DailySalesToolStripMenuItem1.Text = "Penjualan per hari"
            MonthlySalesToolStripMenuItem.Text = "Penjualan per bulan"
            ByInvoiceToolStripMenuItem.Text = "per Faktur"
            ByPaymentMethodToolStripMenuItem.Text = "per Cara Pembayaran"
            ByCustomerToolStripMenuItem.Text = "per Customer"
            ByStatusToolStripMenuItem.Text = "Pembatalan"
            BySalesmanToolStripMenuItem.Text = "oleh Sales"
            PerProductToolStripMenuItem.Text = "Sales per Produk"
            PerInvoiceToolStripMenuItem.Text = "Sales per Faktur"
            FinancialToolStripMenuItem.Text = "Keuangan"
            BalanceSheetToolStripMenuItem1.Text = "Neraca"
            ProfitLossToolStripMenuItem.Text = "Laba-Rugi"

            ProductToolStripMenuItem1.Text = "Produk"
            ReportToolStripMenuItem.Text = "Laporan"

            CashRegisterToolStripMenuItem1.Text = "Kasir"
            PayableTransactionToolStripMenuItem1.Text = "Pengeluaran"
            ReceivableTransactionToolStripMenuItem1.Text = "Pemasukan"

            PurchaseOrderToolStripMenuItem.Text = "Pemesanan Pembelian"
            SaleOrderToolStripMenuItem.Text = "Pemesanan Penjualan"
            DeliveryOrderToolStripMenuItem.Text = "Pengiriman"

            DataManagerToolStripMenuItem.Text = "Kelola Data"
            BackupDatabaseToolStripMenuItem.Text = "Organisasi Data"
            RestoreDatabaseToolStripMenuItem.Text = "Restore Data"
            ClosingPeriodToolStripMenuItem.Text = "Tutup Buku"
            MonthlyToolStripMenuItem.Text = "per Bulan"
            YearlyToolStripMenuItem.Text = "per Tahun"

            ConfigurationToolStripMenuItem1.Text = "Konfigurasi"

            EntryToolStripMenuItem.Text = "Input"
            EntryToolStripMenuItem1.Text = "Input"
            EntryToolStripMenuItem2.Text = "Input"
            EntryToolStripMenuItem3.Text = "Input"
            EntryToolStripMenuItem4.Text = "Input"
            EntryToolStripMenuItem5.Text = "Input"
            EntryToolStripMenuItem6.Text = "Input"
            EntryToolStripMenuItem8.Text = "Input"
            EntryToolStripMenuItem9.Text = "Input"
            EntryToolStripMenuItem10.Text = "Input"
            ProductDistributionEntryToolStripMenuItem.Text = "Input"

            EntryToolStripMenuItem11.Text = "Input"
            EntryToolStripMenuItem12.Text = "Input"
            EntryToolStripMenuItem13.Text = "Input"

            EntryToolStripMenuItem7.Text = "Pembayaran"
            ReceivableEntryToolStripMenu.Text = "Pembayaran"

            ListToolStripMenuItem.Text = "Daftar"
            ListToolStripMenuItem1.Text = "Daftar"
            ListToolStripMenuItem2.Text = "Daftar"
            ByInvoiceToolStripMenuItem.Text = "per Faktur"
            ListToolStripMenuItem3.Text = "Daftar"
            ListToolStripMenuItem4.Text = "Daftar"
            ListToolStripMenuItem5.Text = "Daftar"
            ListToolStripMenuItem6.Text = "Daftar"
            ListToolStripMenuItem7.Text = "Daftar"
            ListToolStripMenuItem8.Text = "Daftar"
            ListToolStripMenuItem9.Text = "Daftar"
            ListToolStripMenuItem11.Text = "Daftar"
            ListToolStripMenuItem12.Text = "Daftar"
            ListToolStripMenuItem13.Text = "Daftar"

            ListToolStripMenuItem14.Text = "Daftar"
            ListToolStripMenuItem15.Text = "Daftar"
            ListToolStripMenuItem16.Text = "Daftar"

            ProductDistributionListToolStripMenuItem.Text = "Daftar"
            ''''New Edit Report''''
            ProductDistributionToolStripMenuItem1.Text = "Distribusi Produk"

            SaleButton1.Text = "Penjualan"
            SaleButton1.ToolTipText = "Input Penjualan Baru"

            PurchaseButton1.Text = "Pembelian"
            PurchaseButton1.ToolTipText = "Input Pembelian Baru"

            InvoiceButton1.Text = "Faktur"
            InvoiceButton1.ToolTipText = "Daftar Faktur Harian"

            StockButton1.Text = "Stok"
            StockButton1.ToolTipText = "Daftar Stok Produk"

            ConfigButton1.Text = "P'aturan"
            ConfigButton1.ToolTipText = "Pengaturan Aplikasi"

            StockCardButton1.Text = "Kartu Stok"
            StockCardButton1.ToolTipText = "Daftar Kartu Stok"

            WarehouseButton1.Text = "Stok Gudang"
            WarehouseButton1.ToolTipText = "Daftar Stok per Gudang"

        Else

            StatusLabelUser.Text = "User Name : " & UserName

            StatusLabelNotif.Text = frmNotification.Label1.Text
        End If
    End Sub

    Private Sub ListToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmListOpeningBalance
        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openListOpeningBalance(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmListOpeningBalance
        End If
    End Sub

    Private Sub EntryToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryToolStripMenuItem.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmEntryOpeningBalance
        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openOpeningBalance(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmEntryOpeningBalance
        End If
    End Sub

    Private Sub ListToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem1.Click
        Dim tmptmp As New DMI_RETAIL_PURCHASE.frmListPurchase
        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openListPurchase(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_PURCHASE.frmListPurchase
        End If
    End Sub

    Private Sub EntryToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryToolStripMenuItem1.Click
        Dim tmptmp As New DMI_RETAIL_PURCHASE.frmEntryPurchase

        AccessPrivilege(tmptmp.Tag)
        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openEntryPurchase(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_PURCHASE.frmEntryPurchase
        End If
    End Sub

    Private Sub ListToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem2.Click
        Dim tmptmp As New DMI_RETAIL_SALE.frmListSale

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openListSale(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_SALE.frmListSale
        End If
    End Sub

    Private Sub EntryToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryToolStripMenuItem2.Click
        Dim tmptmp As New DMI_RETAIL_SALE.frmEntrySale

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openEntrySale(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_SALE.frmEntrySale
        End If

    End Sub

    Private Sub ProductStockToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProductStockToolStripMenuItem.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmListProductStock

        AccessPrivilege(tmptmp.Tag)
        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openProductStock(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmListProductStock
        End If
    End Sub

    Private Sub ProductWarehouseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProductWarehouseToolStripMenuItem.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmListProductWarehouse

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openProductWarehouse(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmListProductWarehouse
        End If
    End Sub

    Private Sub EndingStockToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EndingStockToolStripMenuItem.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmListEndingStock

        AccessPrivilege(tmptmp.Tag)
        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openEndingStok(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmListEndingStock
        End If
    End Sub

    Private Sub StockCardToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StockCardToolStripMenuItem.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmListStockCard

        AccessPrivilege(tmptmp.Tag)
        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openStockCard(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmListStockCard
        End If
    End Sub

    Private Sub SoldProductToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SoldProductToolStripMenuItem.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmListMovingProduct
        'accFormName = tmptmp.Tag

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptProductMovement(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmListMovingProduct
        End If
    End Sub

    Private Sub SalesReportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalesReportToolStripMenuItem.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmListReportSale
        'accFormName = tmptmp.Tag
        If AllowOpen = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            tmpType = "M"
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptDailySales(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language, mdlGeneral.tmpType)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmListReportSale
        End If
    End Sub

    Private Sub DailyInvoiceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DailyInvoiceToolStripMenuItem.Click
        AccessPrivilege(frmListDailyInvoice.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            frmListDailyInvoice.MdiParent = Me
            frmListDailyInvoice.Show()
            frmListDailyInvoice.BringToFront()
            tmpRecentForm = New frmListDailyInvoice
        End If
    End Sub

    Private Sub BackupDatabaseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BackupDatabaseToolStripMenuItem.Click
        'accFormName = frmDataBackup.Tag
        'AccessPrivilege(frmDataBackup.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        Me.Close()
        'Dim Path_Name As String = Application.StartupPath
        'Dim Path_Data As String = Replace(Path_Name, Mid(Path_Name, 51, 90), "")
        Dim PathDb As String = Application.StartupPath & "\BACK_UP_AND_RETORE_DMI_RETAIL.exe"
        p.StartInfo.FileName = PathDb
        p.Start()

        'If Not tmpForm Is Nothing Then tmpForm.Close()
        'If tmpvar = 2 Then
        '    Exit Sub
        'Else
        '    frmBackUpData.MdiParent = Me
        '    frmBackUpData.Show()
        '    frmBackUpData.BringToFront()
        '    tmpForm = frmBackUpData
        '    tmpRecentForm = New frmBackUpData
        'End If
    End Sub

    Private Sub RestoreDatabaseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RestoreDatabaseToolStripMenuItem.Click
        AccessPrivilege(frmDataRestore.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        Me.Close()
        Dim Path_Name As String = Application.StartupPath
        Dim Path_Data As String = Replace(Path_Name, Mid(Path_Name, 51, 90), "")
        Dim PathDb As String = Path_Data & "\BACK_UP_AND_RETORE_DMI_RETAIL\bin\Debug\BACK_UP_AND_RETORE_DMI_RETAIL"
        p.StartInfo.FileName = PathDb
        p.Start()

        'If Not tmpForm Is Nothing Then tmpForm.Close()
        'If tmpvar = 2 Then
        '    Exit Sub
        'Else
        '    frmDataRestore.MdiParent = Me
        '    frmDataRestore.Show()
        '    frmDataRestore.BringToFront()
        '    tmpForm = frmDataRestore
        '    tmpRecentForm = New frmDataRestore
        'End If
    End Sub

    Private Sub MonthlyToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MonthlyToolStripMenuItem.Click
        AccessPrivilege(frmClosingPeriod.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            frmClosingPeriod.MdiParent = Me
            frmClosingPeriod.Show()
            frmClosingPeriod.BringToFront()
            tmpRecentForm = New frmClosingPeriod
        End If
    End Sub

    Private Sub YearlyToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles YearlyToolStripMenuItem.Click
        AccessPrivilege(frmClosingPeriodYearly.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            frmClosingPeriodYearly.MdiParent = Me
            frmClosingPeriodYearly.Show()
            frmClosingPeriodYearly.BringToFront()
            tmpRecentForm = New frmClosingPeriodYearly
        End If
    End Sub

    Private Sub SaleToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim tmptmp As New DMI_RETAIL_SALE.frmEntrySale

        AccessPrivilege(tmptmp.Tag)
        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openEntrySale(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_SALE.frmEntrySale
        End If

    End Sub

    Private Sub ProductStockToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmListProductStock

        Call AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openProductStock(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmListProductStock
        End If
    End Sub

    Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        StatusLabelDatetime.Text = "  " & Format(Now(), "dd MMMM yyyy   HH:mm") & "  "
    End Sub


    'Private Sub ListToolStripMenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem4.Click
    '    If Not tmpForm Is Nothing Then tmpForm.Close()
    '    If tmpvar = 2 Then
    '        Exit Sub
    '    Else
    '        frmListGroupingProduct.MdiParent = Me
    '        frmListGroupingProduct.Show()
    '        frmListGroupingProduct.BringToFront()
    '        tmpForm = frmListGroupingProduct
    '    End If
    'End Sub

    Private Sub EntryToolStripMenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryToolStripMenuItem3.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmEntryGroupingProduct

        Call AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openEntryGroupingProduct(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmEntryGroupingProduct
        End If
    End Sub

    Private Sub StatusLabelNotif_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StatusLabelNotif.Click
        AccessPrivilege(frmNotification.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            frmNotification.MdiParent = Me
            frmNotification.Show()
            frmNotification.BringToFront()
            tmpRecentForm = New frmNotification
        End If
    End Sub

    Private Sub ProductDistributionListToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProductDistributionListToolStripMenuItem.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmListProductDistribution

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openListProductDistribution(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmListProductDistribution
        End If
    End Sub

    Private Sub ProductDistributionEntryToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProductDistributionEntryToolStripMenuItem.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmEntryProductDistribution

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openProductDistribution(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmEntryProductDistribution
        End If
    End Sub

    Private Sub ReceivableEntryToolStripMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReceivableEntryToolStripMenu.Click
        Dim tmptmp As New DMI_RETAIL_HUTANG_PIUTANG.frmReceivablePayment

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openReceivablePayment(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_HUTANG_PIUTANG.frmReceivablePayment
        End If
    End Sub

    Private Sub ListToolStripMenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem11.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmListSplitProduct

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openListSplitProduct(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmListSplitProduct
        End If
    End Sub

    Private Sub EntryToolStripMenuItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryToolStripMenuItem5.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmSplitProduct

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openEntrySplitProduct(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmSplitProduct
        End If
    End Sub

    Private Sub EntryToolStripMenuItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryToolStripMenuItem6.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmEntryAdjustment

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openAdjustment(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmEntryAdjustment
        End If
    End Sub

    Private Sub ListToolStripMenuItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem6.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmListAdjustment

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openListAdjustment(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmListAdjustment
        End If
    End Sub

    Private Sub EntryToolStripMenuItem7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryToolStripMenuItem7.Click
        Dim tmptmp As New DMI_RETAIL_HUTANG_PIUTANG.frmPayablePayment

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openPayablePayment(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_HUTANG_PIUTANG.frmPayablePayment
        End If
    End Sub

    Private Sub ListToolStripMenuItem8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem8.Click
        Dim tmptmp As New DMI_RETAIL_PURCHASE.frmListPurchaseReturn

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openListPurchaseReturn(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_PURCHASE.frmListPurchaseReturn
        End If
    End Sub

    Private Sub EntryToolStripMenuItem8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryToolStripMenuItem8.Click
        Dim tmptmp As New DMI_RETAIL_PURCHASE.frmListPurchaseReturn

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openEntryPurchaseReturn(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_PURCHASE.frmEntryReturnPurchase
        End If
    End Sub

    Private Sub EntryToolStripMenuItem9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryToolStripMenuItem9.Click
        Dim tmptmp As New DMI_RETAIL_SALE.frmEntryReturnSale

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openEntryReturnSale(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_SALE.frmEntryReturnSale
        End If
    End Sub

    Private Sub ListToolStripMenuItem9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem9.Click
        Dim tmptmp As New DMI_RETAIL_SALE.frmListSaleReturn

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openListReturnSale(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_SALE.frmListSaleReturn
        End If
    End Sub

    Private Sub ListToolStripMenuItem7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem7.Click
        Dim tmptmp As New DMI_RETAIL_HUTANG_PIUTANG.frmListPayablePayment

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openListPayablePayment(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_HUTANG_PIUTANG.frmListPayablePayment
        End If
    End Sub

    Private Sub ListToolStripMenuItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem5.Click
        Dim tmptmp As New DMI_RETAIL_HUTANG_PIUTANG.frmListReceivablePayment

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openListReceivablePayment(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_HUTANG_PIUTANG.frmListReceivablePayment
        End If
    End Sub

    Private Sub CompanyInformationToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CompanyInformationToolStripMenuItem.Click
        AccessPrivilege(frmCompanyInfo.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            frmCompanyInfo.MdiParent = Me
            frmCompanyInfo.Show()
            frmCompanyInfo.BringToFront()
            tmpRecentForm = New frmCompanyInfo
        End If
    End Sub

    Private Sub EntryUserToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryUserToolStripMenuItem1.Click
        AccessPrivilege(frmEntryUser.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            frmEntryUser.MdiParent = Me
            frmEntryUser.Show()
            frmEntryUser.BringToFront()
            tmpRecentForm = New frmEntryUser
        End If
    End Sub

    Private Sub LogOutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LogOutToolStripMenuItem.Click
        HideAllForms()

        If tmpvar = 2 Then
            Exit Sub
        Else
            Me.Hide()
            ObjFormLogin = New frmLogin
            ObjFormLogin.ShowDialog()
        End If
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        If tmpvar = 2 Then
            Exit Sub
        Else
            Me.Close()
        End If
    End Sub

    Private Sub ConfigurationToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConfigurationToolStripMenuItem1.Click
        AccessPrivilege(frmConfig.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            frmConfig.MdiParent = Me
            frmConfig.Show()
            frmConfig.BringToFront()
            tmpRecentForm = New frmConfig
        End If
    End Sub

    Private Sub PurchaseButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PurchaseButton1.Click
        Dim tmptmp As New DMI_RETAIL_PURCHASE.frmEntryPurchase

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openListPurchase(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_PURCHASE.frmEntryPurchase
        End If
    End Sub

    Private Sub SaleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaleButton1.Click
        Dim tmptmp As New DMI_RETAIL_SALE.frmEntrySale

        AccessPrivilege(tmptmp.Tag)
        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openEntrySale(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_SALE.frmEntrySale
        End If

    End Sub

    Private Sub NotepadToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NotepadToolStripMenuItem.Click

        p.StartInfo.FileName = "Notepad.exe"

        p.Start()

    End Sub

    Private Sub CalculatorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CalculatorToolStripMenuItem.Click

        p.StartInfo.FileName = "Calc.exe"

        p.Start()

    End Sub

    Private Sub StockButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StockButton1.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmListProductStock

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openProductStock(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmListProductStock
        End If
    End Sub

    Private Sub InvoiceButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InvoiceButton1.Click
        AccessPrivilege(frmListDailyInvoice.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            frmListDailyInvoice.MdiParent = Me
            frmListDailyInvoice.Show()
            frmListDailyInvoice.BringToFront()
            tmpRecentForm = New frmListDailyInvoice
        End If
    End Sub

    Private Sub ConfigButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConfigButton1.Click
        AccessPrivilege(frmConfig.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            frmConfig.MdiParent = Me
            frmConfig.Show()
            frmConfig.BringToFront()
            tmpRecentForm = New frmConfig
        End If
    End Sub

    Private Sub StockCardButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StockCardButton1.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmListStockCard

        Call AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openStockCard(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmListStockCard
        End If
    End Sub

    Private Sub WarehouseButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles WarehouseButton1.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmListProductWarehouse

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openProductWarehouse(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmListProductWarehouse
        End If
    End Sub

    Private Sub AccessPrivilegeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AccessPrivilegeToolStripMenuItem.Click
        Call AccessPrivilege(frmAccessPrivilege.Tag)


        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            frmAccessPrivilege.MdiParent = Me
            frmAccessPrivilege.Show()
            frmAccessPrivilege.BringToFront()
            tmpRecentForm = New frmAccessPrivilege
        End If
    End Sub

    Private Sub ListToolStripMenuItem10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem10.Click
        Dim ObjFormListProduct As New DMI_RETAIL_MASTER.frmListProduct

        AccessPrivilege("Entry Product")

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            ObjFormListProduct.MdiParent = Me
            ObjFormListProduct.Show()
            tmpRecentForm = New DMI_RETAIL_MASTER.frmListProduct
        End If
    End Sub

    Private Sub EntryProductToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryProductToolStripMenuItem.Click
        Dim ObjFormEntryProduct As New DMI_RETAIL_MASTER.frmEntryProduct

        AccessPrivilege(ObjFormEntryProduct.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            ObjFormEntryProduct.MdiParent = Me
            ObjFormEntryProduct.Show()
            tmpRecentForm = New DMI_RETAIL_MASTER.frmEntryProduct
        End If
    End Sub

    Private Sub ListCustomerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListCustomerToolStripMenuItem.Click
        Dim ObjFormListCustomer As New DMI_RETAIL_MASTER.frmListCustomer

        AccessPrivilege("Entry Customer")

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            ObjFormListCustomer.MdiParent = Me
            ObjFormListCustomer.Show()
            tmpRecentForm = ObjFormListCustomer
        End If
    End Sub

    Private Sub EntryCustomerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryCustomerToolStripMenuItem.Click
        Dim ObjFormEntryCustomer As New DMI_RETAIL_MASTER.frmEntryCustomer

        AccessPrivilege(ObjFormEntryCustomer.Tag)
        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            ObjFormEntryCustomer.MdiParent = ObjFormMain
            ObjFormEntryCustomer.Show()
            tmpRecentForm = ObjFormEntryCustomer
        End If
    End Sub

    Private Sub EntryVendorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryVendorToolStripMenuItem.Click
        Dim ObjFormEntryVendor As New DMI_RETAIL_MASTER.frmEntryVendor

        AccessPrivilege(ObjFormEntryVendor.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            ObjFormEntryVendor.MdiParent = Me
            ObjFormEntryVendor.Show()
            tmpRecentForm = New DMI_RETAIL_MASTER.frmEntryVendor
        End If
    End Sub

    Private Sub ListVendorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListVendorToolStripMenuItem.Click
        Dim ObjFormListVendor As New DMI_RETAIL_MASTER.frmListVendor

        AccessPrivilege("Entry Vendor")

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            ObjFormListVendor.MdiParent = Me
            ObjFormListVendor.Show()
            tmpRecentForm = ObjFormListVendor
        End If
    End Sub

    Private Sub EntryWarehouseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryWarehouseToolStripMenuItem.Click
        Dim ObjFormEntryWarehouse As New DMI_RETAIL_MASTER.frmEntryWarehouse

        Call AccessPrivilege(ObjFormEntryWarehouse.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        HideAllForms()
        If tmpvar = 2 Then
            Exit Sub
        Else
            ObjFormEntryWarehouse.MdiParent = Me
            ObjFormEntryWarehouse.Show()
            tmpRecentForm = ObjFormEntryWarehouse
        End If
    End Sub

    Private Sub ListWarehouseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListWarehouseToolStripMenuItem.Click
        Dim ObjFormListWarehouse As New DMI_RETAIL_MASTER.frmListWarehouse

        AccessPrivilege("Entry Warehouse")

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            ObjFormListWarehouse.MdiParent = ObjFormMain
            ObjFormListWarehouse.Show()
            tmpRecentForm = ObjFormListWarehouse
        End If
    End Sub

    Private Sub EntryAccountToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryAccountToolStripMenuItem.Click
        Dim ObjFormEntryAccount As New DMI_RETAIL_MASTER.frmEntryAccount

        AccessPrivilege(ObjFormEntryAccount.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengakses fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            ObjFormEntryAccount.MdiParent = Me
            ObjFormEntryAccount.Show()
            tmpRecentForm = ObjFormEntryAccount
        End If
    End Sub

    Private Sub ListAccountToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListAccountToolStripMenuItem.Click
        Dim ObjFormListAccount As New DMI_RETAIL_MASTER.frmListAccount

        AccessPrivilege("Entry Account")

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengakses fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            ObjFormListAccount.MdiParent = Me
            ObjFormListAccount.Show()
            tmpRecentForm = ObjFormListAccount
        End If
    End Sub

    Private Sub EntryLocationToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryLocationToolStripMenuItem.Click
        Dim ObjFormEntryLocation As New DMI_RETAIL_MASTER.frmEntryLocation

        AccessPrivilege(ObjFormEntryLocation.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            ObjFormEntryLocation.MdiParent = Me
            ObjFormEntryLocation.Show()
            tmpRecentForm = New DMI_RETAIL_MASTER.frmEntryLocation
        End If
    End Sub

    Private Sub ListLocationToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListLocationToolStripMenuItem.Click
        Dim ObjFormListLocation As New DMI_RETAIL_MASTER.frmListLocation

        AccessPrivilege("Entry Location")

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            ObjFormListLocation.MdiParent = Me
            ObjFormListLocation.Show()
            tmpRecentForm = New DMI_RETAIL_MASTER.frmListLocation
        End If
    End Sub

    Private Sub ListSalesmanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListSalesmanToolStripMenuItem.Click
        Dim ObjFormListSalesman As New DMI_RETAIL_MASTER.frmListSalesman

        AccessPrivilege("Entry Salesman")

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            ObjFormListSalesman.MdiParent = Me
            ObjFormListSalesman.Show()
            tmpRecentForm = New DMI_RETAIL_MASTER.frmListSalesman
        End If
    End Sub

    Private Sub EntrySalesmanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntrySalesmanToolStripMenuItem.Click
        Dim ObjFormEntrySalesman As New DMI_RETAIL_MASTER.frmEntrySalesman

        AccessPrivilege(ObjFormEntrySalesman.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            ObjFormEntrySalesman.MdiParent = Me
            ObjFormEntrySalesman.Show()
            tmpRecentForm = New DMI_RETAIL_MASTER.frmEntrySalesman
        End If
    End Sub

    Private Sub ListToolStripMenuItem4_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem4.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmListGroupingProduct

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openListGroupingProduct(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmListGroupingProduct
        End If
    End Sub

    Private Sub ProductMovementToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProductMovementToolStripMenuItem.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmListProductMovement
        'accFormName = tmptmp.Tag
        'AccessPrivilege()

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            'tmpForm = New DMI_RETAIL_REPORT.frmListProductMovement
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptProductMovement(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmListProductMovement
        End If
    End Sub

    Private Sub RecentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RecentToolStripMenuItem.Click
        Try
            If tmpvar = 2 Then
                Exit Sub
            Else
                tmpRecentForm.MdiParent = Me
                tmpRecentForm.Show()
                tmpRecentForm.BringToFront()
            End If
        Catch

        End Try
    End Sub

    Private Sub EntryToolStripMenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryToolStripMenuItem4.Click
        Dim tmptmp As New DMI_RETAIL_HUTANG_PIUTANG.frmPayableTransaction

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openPayableTransaction(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_HUTANG_PIUTANG.frmPayableTransaction
        End If
    End Sub

    Private Sub ListToolStripMenuItem12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem12.Click
        Dim tmptmp As New DMI_RETAIL_HUTANG_PIUTANG.frmPayableTransaction

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openListPayableTransaction(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_HUTANG_PIUTANG.frmListPayableTransaction
        End If
    End Sub

    Private Sub EntryToolStripMenuItem10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryToolStripMenuItem10.Click
        Dim tmptmp As New DMI_RETAIL_HUTANG_PIUTANG.frmReceivableTransaction

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openReceivableTransaction(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_HUTANG_PIUTANG.frmReceivableTransaction
        End If
    End Sub

    Private Sub ListToolStripMenuItem13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem13.Click
        Dim tmptmp As New DMI_RETAIL_HUTANG_PIUTANG.frmReceivableTransaction

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openListReceivableTransaction(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_HUTANG_PIUTANG.frmListReceivableTransaction
        End If
    End Sub

    Private Sub SalesPerInvoiceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalesPerInvoiceToolStripMenuItem.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmListSalesmanInvoice
        'accFormName = tmptmp.Tag
        'AccessPrivilege()

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptSaleBySalesmanInvoice(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmListSalesmanInvoice
        End If
    End Sub

    Private Sub SalesPerProductToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalesPerProductToolStripMenuItem.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmListSalesmanProduct
        'accFormName = tmptmp.Tag
        'AccessPrivilege()

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            'tmpForm = New DMI_RETAIL_REPORT.frmListSalesmanProduct
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptSaleBySalesmanProduct(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmListSalesmanProduct
        End If
    End Sub

    Private Sub EntryPaymentMethodToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryPaymentMethodToolStripMenuItem.Click
        AccessPrivilege(frmEntryPaymentMethod.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            frmEntryPaymentMethod.MdiParent = Me
            frmEntryPaymentMethod.Show()
            frmEntryPaymentMethod.BringToFront()
            tmpRecentForm = New frmEntryPaymentMethod
        End If
    End Sub

    Private Sub ListPaymentMethodToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListPaymentMethodToolStripMenuItem.Click
        AccessPrivilege(frmEntryPaymentMethod.Tag)


        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            frmListPaymentMethod.MdiParent = Me
            frmListPaymentMethod.Show()
            frmListPaymentMethod.BringToFront()
            tmpRecentForm = New frmListPaymentMethod
        End If
    End Sub

    Private Sub ChangePasswordToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChangePasswordToolStripMenuItem.Click
        AccessPrivilege(frmChangePassword.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            frmChangePassword.MdiParent = Me
            frmChangePassword.Show()
            frmChangePassword.BringToFront()
            tmpRecentForm = New frmChangePassword
        End If
    End Sub

    Private Sub ListUserToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListUserToolStripMenuItem.Click
        AccessPrivilege(frmEntryUser.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            frmListUser.MdiParent = Me
            frmListUser.Show()
            frmListUser.BringToFront()
            tmpRecentForm = New frmListUser
        End If
    End Sub

    ''new report''
    Private Sub StockPerProductToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StockPerProductToolStripMenuItem.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmListProductStock

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openProductStock(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmListProductStock
        End If
    End Sub

    Private Sub StockPerWarehouseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StockPerWarehouseToolStripMenuItem.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmListProductWarehouse

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openProductWarehouse(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmListProductWarehouse
        End If
    End Sub

    Private Sub StockPerPriceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StockPerPriceToolStripMenuItem.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmListEndingStock

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openEndingStok(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmListEndingStock
        End If
    End Sub

    Private Sub StockCardToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StockCardToolStripMenuItem1.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmListStockCard

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openStockCard(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmListStockCard
        End If
    End Sub

    Private Sub ProductMovementToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProductMovementToolStripMenuItem1.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmListProductMovement
        'accFormName = tmptmp.Tag
        'AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptProductMovement(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmListProductMovement
        End If
    End Sub

    Private Sub SoldProductToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SoldProductToolStripMenuItem1.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmListMovingProduct
        'accFormName = tmptmp.Tag
        'AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'tmpForm = New DMI_RETAIL_REPORT.frmListMovingProduct
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptProductMovement(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmListMovingProduct
        End If
    End Sub

    Private Sub ProductDistributionToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProductDistributionToolStripMenuItem1.Click
        Dim tmptmp As New DMI_RETAIL_INVENTORY.frmListProductDistribution

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openListProductDistribution(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_INVENTORY.frmListProductDistribution
        End If
    End Sub

    Private Sub PerProductToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PerProductToolStripMenuItem.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmListSalesmanProduct
        'accFormName = tmptmp.Tag
        'AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptSaleBySalesmanProduct(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmListSalesmanProduct
        End If
    End Sub

    Private Sub PerInvoiceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PerInvoiceToolStripMenuItem.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmListSalesmanInvoice
        'accFormName = tmptmp.Tag
        'AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptSaleBySalesmanInvoice(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmListSalesmanInvoice
        End If
    End Sub

    Private Sub ByInvoiceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ByInvoiceToolStripMenuItem.Click
        Dim tmptmp As New DMI_RETAIL_SALE.frmListSale

        AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openListSale(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_SALE.frmListSale
        End If
    End Sub

    Private Sub ByCustomerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ByCustomerToolStripMenuItem.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmReportSaleByCustomer
        'accFormName = tmptmp.Tag
        'AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptSaleByCustomer(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmReportSaleByCustomer
        End If
    End Sub

    Private Sub ByPaymentMethodToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ByPaymentMethodToolStripMenuItem.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmReportSaleByPaymentMethod
        'accFormName = tmptmp.Tag
        'AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptSaleByPaymentMethod(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmReportSaleByPaymentMethod
        End If
    End Sub

    Private Sub ByStatusToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ByStatusToolStripMenuItem.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmReportSaleByStatus
        'accFormName = tmptmp.Tag
        'AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptSaleByStatus(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmReportSaleByStatus
        End If
    End Sub

    Private Sub ByInvoiceToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ByInvoiceToolStripMenuItem1.Click
        Dim tmptmp As New DMI_RETAIL_PURCHASE.frmListPurchase

        'AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openListPurchase(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_PURCHASE.frmListPurchase
        End If
    End Sub

    Private Sub ByVendorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ByVendorToolStripMenuItem.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmReportPurchaseByVendor
        'accFormName = tmptmp.Tag
        'AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptPurchaseByVendor(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmReportPurchaseByVendor
        End If
    End Sub

    Private Sub ByPaymentMethodToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ByPaymentMethodToolStripMenuItem1.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmReportPurchaseByPaymentMethod
        'accFormName = tmptmp.Tag
        'AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptPurchaseByPaymentMethod(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmReportPurchaseByPaymentMethod
        End If
    End Sub

    Private Sub ByStatusToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ByStatusToolStripMenuItem1.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmReportPurchaseByStatus
        'accFormName = tmptmp.Tag
        'AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptPurchaseByStatus(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmReportPurchaseByStatus
        End If
    End Sub

    Private Sub ProfitLossToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProfitLossToolStripMenuItem.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmReportProfitLoss
        'accFormName = tmptmp.Tag
        'AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptPurchaseByStatus(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmReportProfitLoss
        End If
    End Sub

    Private Sub BalanceSheetToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BalanceSheetToolStripMenuItem1.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmBalanceSheetFinance
        'accFormName = tmptmp.Tag
        'AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptFinancialBalanceSheet(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmBalanceSheetFinance
        End If
    End Sub

    Private Sub TimerSafeStock_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerSafeStock.Tick
        Dim tmpSafe As Boolean
        'tmpSafe = SP_NOTIFICATION_SAFE_STOCKTableAdapter.Fill(Me.DS_USER.SP_NOTIFICATION_SAFE_STOCK)

        If tmpSafe = True Then
            If Language = "Indonesian" Then
                If MsgBox("Produk dengan jumlah kurang dari Stok Aman !" & vbCrLf & "Apakah anda ingin mengecek Produk?", _
                          MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then
                    Exit Sub
                Else
                    If tmpvar = 2 Then
                        Exit Sub
                    Else
                        HideAllForms()
                        frmListSafeStock.MdiParent = Me
                        frmListSafeStock.Show()
                        frmListSafeStock.BringToFront()
                    End If
                End If
            Else
                If MsgBox("Product Quantity less than Safe Stock !" & vbCrLf & "Do you want to check the product?", _
                          MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then
                    Exit Sub
                Else
                    If tmpvar = 2 Then
                        Exit Sub
                    Else
                        HideAllForms()
                        frmListSafeStock.MdiParent = Me
                        frmListSafeStock.Show()
                        frmListSafeStock.BringToFront()
                    End If
                End If
            End If
        End If

    End Sub

    Private Sub ActivePayableToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActivePayableToolStripMenuItem1.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmListPayable
        'accFormName = tmptmp.Tag
        'AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptActivePayable(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmListPayable
        End If
    End Sub

    Private Sub ActiveReceivableToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActiveReceivableToolStripMenuItem1.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmListReceivable
        'accFormName = tmptmp.Tag
        'AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptActiveReceivable(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmListReceivable
        End If
    End Sub

    Private Sub ByPurchasingPriceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ByPurchasingPriceToolStripMenuItem.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmListPurchasePrice
        'accFormName = tmptmp.Tag
        'AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptPurchaseByPrice(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmListPurchasePrice
        End If
    End Sub

    Private Sub StockBalanceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StockBalanceToolStripMenuItem.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmBalanceSheet
        'accFormName = tmptmp.Tag
        'AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptBalanceSheet(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmBalanceSheet
        End If
    End Sub

    Private Sub ByDailySoldProductToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ByDailySoldProductToolStripMenuItem.Click
        'Dim tmptmp As New DMI_RETAIL_REPORT.frmListDailySoldProduct
        'accFormName = tmptmp.Tag
        'AccessPrivilege(tmptmp.Tag)

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If


        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptDailySoldProduct(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmListDailySoldProduct
        End If
    End Sub

    Private Sub DailySalesToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DailySalesToolStripMenuItem1.Click
        AccessPrivilege("List Sale Report")

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            tmpType = "M"
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptDailySales(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language, mdlGeneral.tmpType)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmListReportSale
        End If
    End Sub

    Private Sub MonthlySalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MonthlySalesToolStripMenuItem.Click
        AccessPrivilege("List Sales Report")

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            tmpType = "Y"
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptDailySales(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language, mdlGeneral.tmpType)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmListReportSale
        End If
    End Sub

    Private Sub DailyPurchaseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DailyPurchaseToolStripMenuItem.Click
        AccessPrivilege("Report Purchase General")

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            tmpType = "M"
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptDailyPurchase(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language, mdlGeneral.tmpType)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmReportPurchaseGeneral
        End If
    End Sub

    Private Sub MonthlyPurchaseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MonthlyPurchaseToolStripMenuItem.Click
        AccessPrivilege("Report Purchase General")

        If AllowOpen = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpvar = 2 Then
            Exit Sub
        Else
            tmpType = "Y"
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openRptDailyPurchase(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language, mdlGeneral.tmpType)
            'tmpRecentForm = New DMI_RETAIL_REPORT.frmReportPurchaseGeneral
        End If
    End Sub

    Private Sub EntryToolStripMenuItem11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryToolStripMenuItem11.Click
        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openEntrySaleOrder(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_SALE.frmEntrySaleOrder
        End If
    End Sub

    Private Sub ListToolStripMenuItem14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem14.Click
        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openListSaleOrder(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_SALE.frmListSaleOrderHeader
        End If
    End Sub

    Private Sub EntryToolStripMenuItem12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryToolStripMenuItem12.Click
        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openEntryDeliveryOrder(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_SALE.frmDeliveryOrder
        End If
    End Sub

    Private Sub ListToolStripMenuItem15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem15.Click
        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openListDeliveryOrder(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_SALE.frmListDeliveryOrder
        End If
    End Sub

    Private Sub ListToolStripMenuItem16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListToolStripMenuItem16.Click
        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openListPurchaseOrder(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            'tmpRecentForm = New DMI_RETAIL_PURCHASE.frmListPurchaseOrder
        End If
    End Sub

    Private Sub EntryToolStripMenuItem13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntryToolStripMenuItem13.Click
        If tmpvar = 2 Then
            Exit Sub
        Else
            HideAllForms()
            'Dim clstmptmp As New ClassLibrary.clsGeneral
            'clstmptmp.openEntryPurchaseOrder(Me, mdlGeneral.User_Type, mdlGeneral.USER_ID, tmpForm, mdlGeneral.Language)
            tmpRecentForm = New DMI_RETAIL_PURCHASE.frmEntryPurchaseOrder
        End If
    End Sub

End Class
