﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAccessPrivilege
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAccessPrivilege))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmdSearch = New System.Windows.Forms.PictureBox()
        Me.ACCESS_PRIVILEGE_NAMETextBox = New System.Windows.Forms.TextBox()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdCheckAll = New System.Windows.Forms.Button()
        Me.dgvPrivilege = New System.Windows.Forms.DataGridView()
        Me.FORM_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ALLOW_ADD = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ALLOW_EDIT = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ALLOW_DELETE = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ALLOW_OPEN = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ALLOW_PRINT = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ACCESS_PRIVILEGEBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.GroupBox1.SuspendLayout()
        CType(Me.cmdSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvPrivilege, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.ACCESS_PRIVILEGEBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ACCESS_PRIVILEGEBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(4, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(176, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Access Privilege Name"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmdSearch)
        Me.GroupBox1.Controls.Add(Me.ACCESS_PRIVILEGE_NAMETextBox)
        Me.GroupBox1.Controls.Add(Me.lblStatus)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 30)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(496, 70)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'cmdSearch
        '
        Me.cmdSearch.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearch.Image = CType(resources.GetObject("cmdSearch.Image"), System.Drawing.Image)
        Me.cmdSearch.Location = New System.Drawing.Point(297, 27)
        Me.cmdSearch.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearch.Name = "cmdSearch"
        Me.cmdSearch.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearch.TabIndex = 19
        Me.cmdSearch.TabStop = False
        '
        'ACCESS_PRIVILEGE_NAMETextBox
        '
        Me.ACCESS_PRIVILEGE_NAMETextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ACCESS_PRIVILEGE_NAMETextBox.Location = New System.Drawing.Point(143, 27)
        Me.ACCESS_PRIVILEGE_NAMETextBox.Name = "ACCESS_PRIVILEGE_NAMETextBox"
        Me.ACCESS_PRIVILEGE_NAMETextBox.Size = New System.Drawing.Size(150, 25)
        Me.ACCESS_PRIVILEGE_NAMETextBox.TabIndex = 26
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Lucida Bright", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(340, 21)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(151, 28)
        Me.lblStatus.TabIndex = 25
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblStatus.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdCheckAll)
        Me.GroupBox2.Controls.Add(Me.dgvPrivilege)
        Me.GroupBox2.Location = New System.Drawing.Point(13, 106)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(496, 310)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        '
        'cmdCheckAll
        '
        Me.cmdCheckAll.Location = New System.Drawing.Point(383, 281)
        Me.cmdCheckAll.Name = "cmdCheckAll"
        Me.cmdCheckAll.Size = New System.Drawing.Size(107, 23)
        Me.cmdCheckAll.TabIndex = 1
        Me.cmdCheckAll.Text = "Check All"
        Me.cmdCheckAll.UseVisualStyleBackColor = True
        '
        'dgvPrivilege
        '
        Me.dgvPrivilege.AllowUserToAddRows = False
        Me.dgvPrivilege.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPrivilege.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPrivilege.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPrivilege.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.FORM_NAME, Me.ALLOW_ADD, Me.ALLOW_EDIT, Me.ALLOW_DELETE, Me.ALLOW_OPEN, Me.ALLOW_PRINT})
        Me.dgvPrivilege.Location = New System.Drawing.Point(6, 21)
        Me.dgvPrivilege.Name = "dgvPrivilege"
        Me.dgvPrivilege.RowHeadersWidth = 32
        Me.dgvPrivilege.Size = New System.Drawing.Size(484, 254)
        Me.dgvPrivilege.TabIndex = 0
        '
        'FORM_NAME
        '
        Me.FORM_NAME.HeaderText = "Form Name"
        Me.FORM_NAME.Name = "FORM_NAME"
        Me.FORM_NAME.Width = 175
        '
        'ALLOW_ADD
        '
        Me.ALLOW_ADD.HeaderText = "Add"
        Me.ALLOW_ADD.Name = "ALLOW_ADD"
        Me.ALLOW_ADD.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ALLOW_ADD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.ALLOW_ADD.Width = 50
        '
        'ALLOW_EDIT
        '
        Me.ALLOW_EDIT.HeaderText = "Edit"
        Me.ALLOW_EDIT.Name = "ALLOW_EDIT"
        Me.ALLOW_EDIT.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ALLOW_EDIT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.ALLOW_EDIT.Width = 50
        '
        'ALLOW_DELETE
        '
        Me.ALLOW_DELETE.HeaderText = "Del"
        Me.ALLOW_DELETE.Name = "ALLOW_DELETE"
        Me.ALLOW_DELETE.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ALLOW_DELETE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.ALLOW_DELETE.Width = 50
        '
        'ALLOW_OPEN
        '
        Me.ALLOW_OPEN.HeaderText = "Open"
        Me.ALLOW_OPEN.Name = "ALLOW_OPEN"
        Me.ALLOW_OPEN.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ALLOW_OPEN.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.ALLOW_OPEN.Width = 50
        '
        'ALLOW_PRINT
        '
        Me.ALLOW_PRINT.HeaderText = "Print"
        Me.ALLOW_PRINT.Name = "ALLOW_PRINT"
        Me.ALLOW_PRINT.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ALLOW_PRINT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.ALLOW_PRINT.Width = 50
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cmdUndo)
        Me.GroupBox3.Controls.Add(Me.cmdSave)
        Me.GroupBox3.Controls.Add(Me.cmdEdit)
        Me.GroupBox3.Controls.Add(Me.cmdDelete)
        Me.GroupBox3.Controls.Add(Me.cmdAdd)
        Me.GroupBox3.Location = New System.Drawing.Point(13, 422)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox3.Size = New System.Drawing.Size(497, 55)
        Me.GroupBox3.TabIndex = 6
        Me.GroupBox3.TabStop = False
        '
        'cmdUndo
        '
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(210, 17)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(74, 29)
        Me.cmdUndo.TabIndex = 6
        Me.cmdUndo.Text = "&Undo"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSave.Location = New System.Drawing.Point(308, 17)
        Me.cmdSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(74, 29)
        Me.cmdSave.TabIndex = 7
        Me.cmdSave.Text = "&Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(114, 17)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(74, 29)
        Me.cmdEdit.TabIndex = 5
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(404, 17)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(74, 29)
        Me.cmdDelete.TabIndex = 8
        Me.cmdDelete.Text = "&Delete"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(18, 17)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(74, 29)
        Me.cmdAdd.TabIndex = 4
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(45, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'ACCESS_PRIVILEGEBindingNavigator
        '
        Me.ACCESS_PRIVILEGEBindingNavigator.AddNewItem = Nothing
        Me.ACCESS_PRIVILEGEBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.ACCESS_PRIVILEGEBindingNavigator.DeleteItem = Nothing
        Me.ACCESS_PRIVILEGEBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ACCESS_PRIVILEGEBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.ACCESS_PRIVILEGEBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ACCESS_PRIVILEGEBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.ACCESS_PRIVILEGEBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.ACCESS_PRIVILEGEBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.ACCESS_PRIVILEGEBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.ACCESS_PRIVILEGEBindingNavigator.Name = "ACCESS_PRIVILEGEBindingNavigator"
        Me.ACCESS_PRIVILEGEBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.ACCESS_PRIVILEGEBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.ACCESS_PRIVILEGEBindingNavigator.Size = New System.Drawing.Size(523, 25)
        Me.ACCESS_PRIVILEGEBindingNavigator.TabIndex = 7
        Me.ACCESS_PRIVILEGEBindingNavigator.Text = "BindingNavigator1"
        '
        'frmAccessPrivilege
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(523, 500)
        Me.Controls.Add(Me.ACCESS_PRIVILEGEBindingNavigator)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmAccessPrivilege"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Access Privilege"
        Me.Text = "Access Privilege"
        Me.TransparencyKey = System.Drawing.Color.White
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cmdSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgvPrivilege, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.ACCESS_PRIVILEGEBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ACCESS_PRIVILEGEBindingNavigator.ResumeLayout(False)
        Me.ACCESS_PRIVILEGEBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents dgvPrivilege As System.Windows.Forms.DataGridView
    Friend WithEvents cmdCheckAll As System.Windows.Forms.Button
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents ACCESS_PRIVILEGE_NAMETextBox As System.Windows.Forms.TextBox
    Friend WithEvents cmdSearch As System.Windows.Forms.PictureBox
    Friend WithEvents FORM_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ALLOW_ADD As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents ALLOW_EDIT As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents ALLOW_DELETE As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents ALLOW_OPEN As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents ALLOW_PRINT As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ACCESS_PRIVILEGEBindingNavigator As System.Windows.Forms.BindingNavigator
End Class
