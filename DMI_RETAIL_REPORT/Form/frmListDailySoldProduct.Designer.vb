﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListDailySoldProduct
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListDailySoldProduct))
        Me.DS_SALE = New DMI_RETAIL_REPORT.DS_SALE()
        Me.SP_LIST_DAILY_PRODUCT_SOLDBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_LIST_DAILY_PRODUCT_SOLDTableAdapter = New DMI_RETAIL_REPORT.DS_SALETableAdapters.SP_LIST_DAILY_PRODUCT_SOLDTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_REPORT.DS_SALETableAdapters.TableAdapterManager()
        Me.SP_LIST_DAILY_PRODUCT_SOLDDataGridView = New System.Windows.Forms.DataGridView()
        Me.PRODUCT_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRODUCT_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SELLING_PRICE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DISCOUNT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QUANTITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmdReport = New System.Windows.Forms.Button()
        Me.dtpSoldProduct = New System.Windows.Forms.DateTimePicker()
        Me.lblPeriod = New System.Windows.Forms.Label()
        Me.cmdGenerate = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtProductName = New System.Windows.Forms.TextBox()
        Me.lblProductName = New System.Windows.Forms.Label()
        Me.lblTotalPrice = New System.Windows.Forms.Label()
        Me.lblTotalQty = New System.Windows.Forms.Label()
        CType(Me.DS_SALE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_LIST_DAILY_PRODUCT_SOLDBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_LIST_DAILY_PRODUCT_SOLDDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'DS_SALE
        '
        Me.DS_SALE.DataSetName = "DS_SALE"
        Me.DS_SALE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SP_LIST_DAILY_PRODUCT_SOLDBindingSource
        '
        Me.SP_LIST_DAILY_PRODUCT_SOLDBindingSource.DataMember = "SP_LIST_DAILY_PRODUCT_SOLD"
        Me.SP_LIST_DAILY_PRODUCT_SOLDBindingSource.DataSource = Me.DS_SALE
        '
        'SP_LIST_DAILY_PRODUCT_SOLDTableAdapter
        '
        Me.SP_LIST_DAILY_PRODUCT_SOLDTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.CUSTOMERTableAdapter = Nothing
        Me.TableAdapterManager.PAYMENT_METHODTableAdapter = Nothing
        Me.TableAdapterManager.PRODUCTTableAdapter = Nothing
        Me.TableAdapterManager.RECEIVABLETableAdapter = Nothing
        Me.TableAdapterManager.SALE_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.SALESMANTableAdapter = Nothing
        Me.TableAdapterManager.SALETableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_REPORT.DS_SALETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.WAREHOUSETableAdapter = Nothing
        '
        'SP_LIST_DAILY_PRODUCT_SOLDDataGridView
        '
        Me.SP_LIST_DAILY_PRODUCT_SOLDDataGridView.AllowUserToAddRows = False
        Me.SP_LIST_DAILY_PRODUCT_SOLDDataGridView.AllowUserToDeleteRows = False
        Me.SP_LIST_DAILY_PRODUCT_SOLDDataGridView.AutoGenerateColumns = False
        Me.SP_LIST_DAILY_PRODUCT_SOLDDataGridView.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SP_LIST_DAILY_PRODUCT_SOLDDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.SP_LIST_DAILY_PRODUCT_SOLDDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.SP_LIST_DAILY_PRODUCT_SOLDDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PRODUCT_ID, Me.PRODUCT_NAME, Me.SELLING_PRICE, Me.DISCOUNT, Me.TOTAL, Me.QUANTITY})
        Me.SP_LIST_DAILY_PRODUCT_SOLDDataGridView.DataSource = Me.SP_LIST_DAILY_PRODUCT_SOLDBindingSource
        Me.SP_LIST_DAILY_PRODUCT_SOLDDataGridView.Location = New System.Drawing.Point(6, 56)
        Me.SP_LIST_DAILY_PRODUCT_SOLDDataGridView.Name = "SP_LIST_DAILY_PRODUCT_SOLDDataGridView"
        Me.SP_LIST_DAILY_PRODUCT_SOLDDataGridView.ReadOnly = True
        Me.SP_LIST_DAILY_PRODUCT_SOLDDataGridView.RowHeadersWidth = 24
        Me.SP_LIST_DAILY_PRODUCT_SOLDDataGridView.Size = New System.Drawing.Size(554, 360)
        Me.SP_LIST_DAILY_PRODUCT_SOLDDataGridView.TabIndex = 2
        '
        'PRODUCT_ID
        '
        Me.PRODUCT_ID.DataPropertyName = "PRODUCT_ID"
        Me.PRODUCT_ID.HeaderText = "PRODUCT_ID"
        Me.PRODUCT_ID.Name = "PRODUCT_ID"
        Me.PRODUCT_ID.ReadOnly = True
        Me.PRODUCT_ID.Visible = False
        '
        'PRODUCT_NAME
        '
        Me.PRODUCT_NAME.DataPropertyName = "PRODUCT_NAME"
        Me.PRODUCT_NAME.HeaderText = "Product Name"
        Me.PRODUCT_NAME.Name = "PRODUCT_NAME"
        Me.PRODUCT_NAME.ReadOnly = True
        Me.PRODUCT_NAME.Width = 140
        '
        'SELLING_PRICE
        '
        Me.SELLING_PRICE.DataPropertyName = "SELLING_PRICE"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.SELLING_PRICE.DefaultCellStyle = DataGridViewCellStyle2
        Me.SELLING_PRICE.HeaderText = "Selling Price"
        Me.SELLING_PRICE.Name = "SELLING_PRICE"
        Me.SELLING_PRICE.ReadOnly = True
        '
        'DISCOUNT
        '
        Me.DISCOUNT.DataPropertyName = "DISCOUNT"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N0"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.DISCOUNT.DefaultCellStyle = DataGridViewCellStyle3
        Me.DISCOUNT.HeaderText = "Total Disc."
        Me.DISCOUNT.Name = "DISCOUNT"
        Me.DISCOUNT.ReadOnly = True
        Me.DISCOUNT.Width = 85
        '
        'TOTAL
        '
        Me.TOTAL.DataPropertyName = "TOTAL"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.TOTAL.DefaultCellStyle = DataGridViewCellStyle4
        Me.TOTAL.HeaderText = "Sold Price"
        Me.TOTAL.Name = "TOTAL"
        Me.TOTAL.ReadOnly = True
        '
        'QUANTITY
        '
        Me.QUANTITY.DataPropertyName = "QUANTITY"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.QUANTITY.DefaultCellStyle = DataGridViewCellStyle5
        Me.QUANTITY.HeaderText = "Qty."
        Me.QUANTITY.Name = "QUANTITY"
        Me.QUANTITY.ReadOnly = True
        Me.QUANTITY.Width = 55
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmdReport)
        Me.GroupBox1.Controls.Add(Me.dtpSoldProduct)
        Me.GroupBox1.Controls.Add(Me.lblPeriod)
        Me.GroupBox1.Controls.Add(Me.cmdGenerate)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(566, 99)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        '
        'cmdReport
        '
        Me.cmdReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdReport.Image = Global.DMI_RETAIL_REPORT.My.Resources.Resources.Printer_small
        Me.cmdReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdReport.Location = New System.Drawing.Point(470, 69)
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(90, 24)
        Me.cmdReport.TabIndex = 12
        Me.cmdReport.Text = "Print [F12]"
        Me.cmdReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.cmdReport.UseVisualStyleBackColor = True
        '
        'dtpSoldProduct
        '
        Me.dtpSoldProduct.CustomFormat = "dd - MMM - yyyy"
        Me.dtpSoldProduct.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpSoldProduct.Location = New System.Drawing.Point(244, 19)
        Me.dtpSoldProduct.Name = "dtpSoldProduct"
        Me.dtpSoldProduct.Size = New System.Drawing.Size(149, 22)
        Me.dtpSoldProduct.TabIndex = 1
        '
        'lblPeriod
        '
        Me.lblPeriod.AutoSize = True
        Me.lblPeriod.Location = New System.Drawing.Point(163, 22)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(77, 15)
        Me.lblPeriod.TabIndex = 11
        Me.lblPeriod.Text = "Time Period"
        '
        'cmdGenerate
        '
        Me.cmdGenerate.Image = CType(resources.GetObject("cmdGenerate.Image"), System.Drawing.Image)
        Me.cmdGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdGenerate.Location = New System.Drawing.Point(233, 56)
        Me.cmdGenerate.Name = "cmdGenerate"
        Me.cmdGenerate.Size = New System.Drawing.Size(99, 33)
        Me.cmdGenerate.TabIndex = 2
        Me.cmdGenerate.Text = "Generate"
        Me.cmdGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdGenerate.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtProductName)
        Me.GroupBox2.Controls.Add(Me.lblProductName)
        Me.GroupBox2.Controls.Add(Me.SP_LIST_DAILY_PRODUCT_SOLDDataGridView)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 117)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(566, 422)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        '
        'txtProductName
        '
        Me.txtProductName.Location = New System.Drawing.Point(102, 19)
        Me.txtProductName.Name = "txtProductName"
        Me.txtProductName.Size = New System.Drawing.Size(165, 22)
        Me.txtProductName.TabIndex = 3
        '
        'lblProductName
        '
        Me.lblProductName.AutoSize = True
        Me.lblProductName.Location = New System.Drawing.Point(6, 22)
        Me.lblProductName.Name = "lblProductName"
        Me.lblProductName.Size = New System.Drawing.Size(90, 15)
        Me.lblProductName.TabIndex = 3
        Me.lblProductName.Text = "Product Name"
        '
        'lblTotalPrice
        '
        Me.lblTotalPrice.AutoSize = True
        Me.lblTotalPrice.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalPrice.Location = New System.Drawing.Point(20, 550)
        Me.lblTotalPrice.Name = "lblTotalPrice"
        Me.lblTotalPrice.Size = New System.Drawing.Size(48, 15)
        Me.lblTotalPrice.TabIndex = 5
        Me.lblTotalPrice.Text = "Label1"
        '
        'lblTotalQty
        '
        Me.lblTotalQty.AutoSize = True
        Me.lblTotalQty.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalQty.Location = New System.Drawing.Point(410, 550)
        Me.lblTotalQty.Name = "lblTotalQty"
        Me.lblTotalQty.Size = New System.Drawing.Size(48, 15)
        Me.lblTotalQty.TabIndex = 6
        Me.lblTotalQty.Text = "Label1"
        '
        'frmListDailySoldProduct
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(590, 583)
        Me.Controls.Add(Me.lblTotalQty)
        Me.Controls.Add(Me.lblTotalPrice)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmListDailySoldProduct"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Daily Sales"
        Me.Text = "Daily Sales"
        CType(Me.DS_SALE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_LIST_DAILY_PRODUCT_SOLDBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_LIST_DAILY_PRODUCT_SOLDDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DS_SALE As DMI_RETAIL_REPORT.DS_SALE
    Friend WithEvents SP_LIST_DAILY_PRODUCT_SOLDBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_LIST_DAILY_PRODUCT_SOLDTableAdapter As DMI_RETAIL_REPORT.DS_SALETableAdapters.SP_LIST_DAILY_PRODUCT_SOLDTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_REPORT.DS_SALETableAdapters.TableAdapterManager
    Friend WithEvents SP_LIST_DAILY_PRODUCT_SOLDDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dtpSoldProduct As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cmdGenerate As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblProductName As System.Windows.Forms.Label
    Friend WithEvents txtProductName As System.Windows.Forms.TextBox
    Friend WithEvents lblTotalPrice As System.Windows.Forms.Label
    Friend WithEvents lblTotalQty As System.Windows.Forms.Label
    Friend WithEvents PRODUCT_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRODUCT_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SELLING_PRICE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DISCOUNT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QUANTITY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdReport As System.Windows.Forms.Button
End Class
