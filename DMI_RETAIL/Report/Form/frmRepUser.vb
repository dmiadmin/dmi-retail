﻿Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Public Class frmRepUser

    Private Sub frmRepUser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dt = New DataTable
        sql = "SELECT (SELECT COMPANY_NAME FROM COMPANY_INFORMATION)COMPANY_NAME," & _
                                        "(SELECT ADDRESS_LINE_1 FROM COMPANY_INFORMATION)ADDRESS_LINE_1," & _
                                        "(SELECT ADDRESS_LINE_3 FROM COMPANY_INFORMATION)ADDRESS_LINE_3," & _
                                        "(SELECT REGENCY FROM COMPANY_INFORMATION)REGENCY," & _
                                        "(SELECT PHONE_1 FROM COMPANY_INFORMATION)PHONE_1," & _
                                        "(SELECT FAX FROM COMPANY_INFORMATION)FAX," & _
                                        "[USER_ID]," & _
       "[USER_NAME], " & _
   "(SELECT ACCESS_PRIVILEDGE_NAME " & _
            "FROM ACCESS_PRIVILEDGE " & _
     "WHERE ACCESS_PRIVILEDGE_ID = [USER].ACCESS_PRIVILEDGE)ACCESS_PRIVILEGE_NAME," & _
        "NUMBER_OF_WRONG_PASSWORD, " & _
        "ACTIVE, " & _
        "LAST_LOGIN, " & _
       " EFFECTIVE_START_DATE, " & _
       " EFFECTIVE_END_DATE, " & _
        "PASSWORD_HINT " & _
         "   FROM [USER]"
        sqladapter = New SqlDataAdapter(sql, xConn)
        sqladapter.Fill(dt)
        Dim rpt As New rptUser
        rpt.SetDataSource(dt)
        CrystalReportViewer1.ReportSource = rpt
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        CrystalReportViewer1.Refresh()
    End Sub
End Class