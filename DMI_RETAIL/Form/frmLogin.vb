﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Data

Public Class frmLogin
    Dim DMIUpdate As String
    Dim tmpUpdate As String()
    Dim xDreader As SqlDataReader
    Dim Cmd As SqlCommand
    Dim sql, sql1, sql2, sql3 As String
    Dim pathName As String = Application.StartupPath
    Dim pathSQL As String = pathName & "\SQLServer2008SP1_32Bit.exe"
    Dim p As New System.Diagnostics.Process

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click

        'If GetSerialNumber(Mid(Application.StartupPath, 1, 1)) <> "-196907119" Then
        '    If Language = "Indonesian" Then
        '        MsgBox("Aplikasi DMI Retail yang Anda gunakan tidak sesuai dengan lisensi." & vbCrLf & _
        '               "Silahkan hubungi DMI-Soft untuk pemakaian Aplikasi DMI Retail lebih lanjut.", _
        '               MsgBoxStyle.Critical, "DMI Retail")
        '    ElseIf Language = "English" Then
        '        MsgBox("DMI Retail you are using is not licensed." & vbCrLf & _
        '               "Please contact DMI-Soft to continue using this Software", _
        '               MsgBoxStyle.Critical, "DMI Retail")
        '    End If
        '    Me.Close()
        'End If

        If txtUserName.Text = "" Or txtPassword.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Isi semua kolom yang diperlukan!", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields!", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        sql1 = "SELECT PASSWORD, [ACTIVE], NUMBER_OF_WRONG_PASSWORD, EFFECTIVE_END_DATE " & _
               "FROM [USER] WHERE [USER_NAME] = '" & txtUserName.Text & "'"
        Cmd = New SqlCommand(sql1, xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xDreader = Cmd.ExecuteReader()
        If xDreader.Read Then
            If xDreader.Item("ACTIVE").ToString = "NO" Then
                If Language = "Indonesian" Then
                    MsgBox("Nama User anda telah dikunci." & vbCrLf & "Silahkan hubungi Administrator Sistem anda.", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Your User Name is locked." & vbCrLf & "Please contact your System Administrator.", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If

            If xDreader.Item("EFFECTIVE_END_DATE") < Now Then
                If Language = "Indonesian" Then
                    MsgBox("Nama User anda telah dihapus." & vbCrLf & _
                           "Silahkan hubungi Administrator untuk membuat account baru.", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Your User Name is deleted." & vbCrLf & _
                           "Please contact Administrator to create new User.", MsgBoxStyle.Critical, "DMI Retail")
                End If
                txtUserName.Text = ""
                txtPassword.Text = ""
                Exit Sub
            End If

            If xDreader.Item("PASSWORD").ToString <> txtPassword.Text Then
                If Language = "Indonesian" Then
                    MsgBox("Nama User atau Kata Sandi salah.", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Wrong User Name or Password.", MsgBoxStyle.Critical, "DMI Retail")
                End If

                Dim tmpNoOfWrongPass As Integer
                tmpNoOfWrongPass = xDreader.Item("NUMBER_OF_WRONG_PASSWORD").ToString

                xDreader.Close()
                sql2 = "UPDATE [USER] SET NUMBER_OF_WRONG_PASSWORD = NUMBER_OF_WRONG_PASSWORD + 1 WHERE [USER_NAME] = '" & txtUserName.Text & "'"
                Cmd = New SqlCommand(sql2, xConn)
                Cmd.ExecuteNonQuery()

                Dim xParameter As SqlDataReader
                sql3 = "SELECT [VALUE] FROM [PARAMETER] WHERE [DESCRIPTION] = 'MAX WRONG PASSWORD'"
                Cmd = New SqlCommand(sql3, xConn)
                xParameter = Cmd.ExecuteReader()
                If xParameter.Read = True Then
                    If xParameter.Item("VALUE").ToString <= tmpNoOfWrongPass + 1 Then
                        Cmd = New SqlCommand("UPDATE [USER] SET [ACTIVE] = 'NO' WHERE [USER_NAME] = '" & txtUserName.Text & "'", xConn)
                        Cmd.ExecuteNonQuery()

                        If Language = "Indonesian" Then
                            MsgBox("Nama User anda telah dikunci." & vbCrLf & "Silahkan hubungi Administrator Sistem anda.", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Your User Name is locked." & vbCrLf & "Please contact your System Administrator.", _
                                   MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        Exit Sub
                    End If
                End If
                Exit Sub
            Else
                UserName = txtUserName.Text
                xDreader.Close()
                Dim myCmd As New SqlCommand("UPDATE [USER] SET NUMBER_OF_WRONG_PASSWORD = 0, [LAST_LOGIN] = '" & Now & _
                                            "' WHERE [USER_NAME] = '" & txtUserName.Text & "'", xConn)
                myCmd.ExecuteNonQuery()
            End If
        Else
            If Language = "Indonesian" Then
                MsgBox("Nama User atau Kata Sandi salah.", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong User Name or Password.", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        Cmd = New SqlCommand("SELECT ACCESS_PRIVILEGE FROM [USER] WHERE [USER_NAME] = '" & txtUserName.Text & "'", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xDreader = Cmd.ExecuteReader()
        If xDreader.Read = True Then UserTypeId = CInt(xDreader.Item("ACCESS_PRIVILEGE").ToString)

        ''<<This block of code is used to get multiple update from SQL>>
        tmpUpdate = Directory.GetFiles(Application.StartupPath)
        For x As Integer = 0 To tmpUpdate.Count - 1
            If Mid(tmpUpdate(x), tmpUpdate(x).Length - 3, 4) = ".sql" Then
                DMIUpdate = File.ReadAllText(tmpUpdate(x))
                xDreader.Close()
                sql = (DMIUpdate)
                Cmd = New SqlCommand(sql, xConn)
                Try
                    Cmd.ExecuteNonQuery()
                    File.Delete(tmpUpdate(x))
                Catch
                    If Language = "Indonesian" Then
                        MsgBox("Update gagal.", MsgBoxStyle.Information, "DMI Retail")
                    Else
                        MsgBox("Update Failed.", MsgBoxStyle.Information, "DMI Retail")
                    End If
                End Try

            End If
        Next
        ''End of multiple update from SQL>> 

        xDreader.Close()
        Cmd = New SqlCommand("DECLARE @return_value int" & vbCrLf & _
                             "EXEC @return_value = [dbo].[SP_GET_USER_ID]" & vbCrLf & _
                             "@USER_NAME = N'" & txtUserName.Text & "'" & vbCrLf & _
                             "SELECT 'Return Value' = @return_value", xConn)
        xDreader = Cmd.ExecuteReader()
        If xDreader.Read = True Then UserId = CInt(xDreader.Item("USER_ID").ToString)

        Me.Hide() : Me.Close()
        ObjFormMain = New frmMain
        ObjFormMain.ShowDialog()
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.Close()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Label3.Text = Format(Now(), "dd MMMM yyyy  HH:mm")
    End Sub

    Private Sub frmLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'If mdlGeneral.CheckSQLOLEDB() Then
        'Else
        '    If Language = "Indonesian" Then
        '        If MsgBox("Database tidak ditemukan !" & vbCrLf & _
        '                  "Apakah anda mau menginstall SQL Services?", MsgBoxStyle.YesNo, _
        '                  "DMI Retail") = MsgBoxResult.No Then
        '            End
        '        Else
        '            p.StartInfo.FileName = pathSQL
        '            p.Start()
        '        End If
        '    Else
        '        If MsgBox("Database not found !" & vbCrLf & _
        '                 "Do you want to install SQL Services?", MsgBoxStyle.YesNo, _
        '                 "DMI Retail") = MsgBoxResult.No Then
        '            End
        '        Else
        '            p.StartInfo.FileName = pathSQL
        '            p.Start()
        '        End If
        '    End If
        'End If

        If Language = "Indonesian" Then
            lblPassword.Text = "Kata Sandi"
            lblUserName.Text = "Nama User"
            cmdCancel.Text = "&Batal"
            LinkForgotPassword.Text = "Lupa Sandi ?"
        End If

        Try
            'Call clsClassLibrary.connection2()
        Catch
            If Mid(Err.Description, 1, 64) = "SQL Network Interfaces: Error Locating Server/Instance Specified" Or _
                Mid(Err.Description, 1, 64) = "[DBNETLIB][ConnectionOpen (Connect()).]SQL Server does not exist" Then
                If Language = "Indonesian" Then
                    MsgBox("Server tidak ditemukan !" & vbCrLf & "Periksa kembali Koneksi anda atau Konfigurasi Nama Server !", _
                      MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Server does not exist !" & vbCrLf & "Please check the Connection or Server Name configuration !", _
                        MsgBoxStyle.Critical, "DMI Retail")
                End If
            ElseIf Mid(Err.Description, 1, 20) = "Cannot open database" Then
                If Language = "Indonesian" Then
                    MsgBox("Database tidak ditemukan !" & vbCrLf & "Periksa kembali Server anda atau Konfigurasi Nama Database !", _
                          MsgBoxStyle.Critical, "DMI Retail")
                    'If MsgBox("Apakah anda ingin membuat database baru ?", MsgBoxStyle.YesNo, "DMI Retail") = _
                    '    MsgBoxResult.Yes Then

                    'End If
                Else
                    MsgBox("Database does not exist !" & vbCrLf & "Please check the Server or Database Name configuration !", _
                        MsgBoxStyle.Critical, "DMI Retail")
                    'If MsgBox("Do you want to create new database ?", MsgBoxStyle.YesNo, "DMI Retail") = _
                    '    MsgBoxResult.Yes Then

                    'End If
                End If
            Else
                MsgBox(Err.Description, MsgBoxStyle.Critical, "Error " & Err.Number)
            End If

            If Language = "Indonesian" Then
                If MsgBox("Apakah anda ingin konfigurasi ulang Server dan Database ?", _
                          MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then
                    Me.Close()
                    Exit Sub
                Else
                    frmTerminalConfig.Show()
                    frmTerminalConfig.BringToFront()
                End If
                Me.Close()
            Else
                If MsgBox("Do you want to re-setup the Server and Database ?", MsgBoxStyle.YesNo, _
                          "DMI Retail") = MsgBoxResult.No Then
                    Me.Close()
                    Exit Sub
                Else
                    frmTerminalConfig.Show()
                    frmTerminalConfig.BringToFront()
                End If
                Me.Close()
            End If
        End Try

        If System.Globalization.CultureInfo.CurrentCulture.ToString <> "en-US" Then
            If Language = "Indonesian" Then
                MsgBox("Regional Settings anda bukan 'English-United States' !" & vbCrLf & _
                   "Set ulang Regional Settings anda lalu jalankan kembali aplikasi ini !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Your Regional Settings is not English-United States !" & vbCrLf & _
                   "Please reset the Regional Settings then restart this application !", MsgBoxStyle.Critical, "DMI Retail")
            End If

            Me.Close()
        End If
    End Sub

    Private Sub LinkForgotPassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkForgotPassword.Click
        If txtUserName.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Mohon isi Nama User untuk identifikasi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please fill the User Name to identify !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        Dim Rs As ADODB.Recordset
        Rs = New ADODB.Recordset

        Rs.Open("SELECT PASSWORD_HINT FROM [USER] WHERE [USER_NAME] = '" & txtUserName.Text & "'", _
                Conn, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)
        If Rs.EOF Then
            If Language = "Indonesian" Then
                MsgBox("Nama User salah.", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong User Name.", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        With frmForgotPassword
            .tmpUser = txtUserName.Text
            If Rs.Fields(0).Value Is DBNull.Value Then
                .tmpHint = ""
            Else
                .tmpHint = Rs.Fields(0).Value
            End If
        End With

        frmForgotPassword.ShowDialog()

    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        Call cmdOK_Click(Nothing, Nothing)
    End Sub

End Class