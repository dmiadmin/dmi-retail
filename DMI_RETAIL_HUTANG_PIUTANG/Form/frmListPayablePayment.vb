﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frmListPayablePayment
    Dim tmpAmount As Integer
    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter

    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub

    Private Sub frmListPayablePayment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dtpPeriod.Value = DateSerial(Today.Year, Today.Month, 1)

        For x As Integer = 0 To dgvLIST_PAYABLE_PAYMENTDataGridView.RowCount - 1
            tmpAmount = tmpAmount + dgvLIST_PAYABLE_PAYMENTDataGridView.Item("PAYMENT_AMOUNT", x).Value
            tmpAmount = tmpAmount
        Next

        If Language = "Indonesian" Then
            Me.Text = "Daftar Pembayaran Hutang"
            lblPeriod.Text = "Periode"
            cmdGenerate.Text = "Proses"
            cmdReport.Text = "Cetak"
            lblVendor.Text = "Nama Supplier :"

            lblNoOfTrans.Text = "Jumlah Transaksi : " & dgvLIST_PAYABLE_PAYMENTDataGridView.RowCount
            lblTotalAmount.Text = "Jumlah Nominal : " & FormatNumber(tmpAmount, 0)
        Else
            lblNoOfTrans.Text = "No Of Transaction : " & dgvLIST_PAYABLE_PAYMENTDataGridView.RowCount
            lblTotalAmount.Text = "Jumlah Nominal : " & FormatNumber(tmpAmount, 0)
        End If

        tmpAmount = 0
    End Sub


    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click

        Me.Cursor = Cursors.WaitCursor

        Dim dt As New DataTable
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC	    @return_value = [dbo].[SP_LIST_PAYABLE_PAYMENT]" & vbCrLf & _
              "		    @PERIOD = N'" & dtpPeriod.Value & "'," & vbCrLf & _
              "		    @VENDOR_NAME = N'" & txtVendor.Text & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xAdoAdapter.Fill(dt)
        Me.dgvLIST_PAYABLE_PAYMENTDataGridView.DataSource = dt
        Me.Cursor = Cursors.Default

        For x As Integer = 0 To dgvLIST_PAYABLE_PAYMENTDataGridView.RowCount - 1
            tmpAmount = tmpAmount + dgvLIST_PAYABLE_PAYMENTDataGridView.Item("PAYMENT_AMOUNT", x).Value
            tmpAmount = tmpAmount
        Next

        If Language = "Indonesian" Then
            lblNoOfTrans.Text = "Jumlah Transaksi : " & dgvLIST_PAYABLE_PAYMENTDataGridView.RowCount
            lblTotalAmount.Text = "Jumlah Nominal : " & FormatNumber(tmpAmount, 0)
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("VENDOR_NAME").HeaderText = "Nama Supplier"
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("TOTAL_PAYABLE").HeaderText = "Total Hutang"
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYMENT_DATE").HeaderText = "Tgl Pembayaran"
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYMENT_AMOUNT").HeaderText = "Jumlah Pembayaran"
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYMENT_METHOD").HeaderText = "Cara Pembayaran"
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYABLE_PAYMENT_NUMBER").HeaderText = "No.Pembayaran"
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYABLE_ID").Visible = False
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("REP_PERIOD").Visible = False
            'dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("").Visible = False
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYABLE_PAYMENT_NUMBER").Width = 75
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("RECEIPT_NO").Width = 75
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("VENDOR_NAME").Width = 110
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("TOTAL_PAYABLE").Width = 90
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYMENT_AMOUNT").Width = 85
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYMENT_METHOD").Width = 90
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYMENT_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYMENT_AMOUNT").DefaultCellStyle.Format = "n0"
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("TOTAL_PAYABLE").DefaultCellStyle.Format = "n0"
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYMENT_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYMENT_AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("TOTAL_PAYABLE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        Else
            lblNoOfTrans.Text = "No Of Transaction : " & dgvLIST_PAYABLE_PAYMENTDataGridView.RowCount
            lblTotalAmount.Text = "Jumlah Nominal : " & FormatNumber(tmpAmount, 0)
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("RECEIPT_NO").HeaderText = "Receipt No"
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("VENDOR_NAME").HeaderText = "Vendor Name"
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("TOTAL_PAYABLE").HeaderText = "Total Payable"
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYMENT_DATE").HeaderText = "Payment Date"
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYMENT_AMOUNT").HeaderText = "Payment Amount"
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYMENT_METHOD").HeaderText = "Payment Method"
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYABLE_PAYMENT_NUMBER").HeaderText = "Payment Number"
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYABLE_ID").Visible = False
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("REP_PERIOD").Visible = False
            'dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("").Visible = False
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYABLE_PAYMENT_NUMBER").Width = 75
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("RECEIPT_NO").Width = 75
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("VENDOR_NAME").Width = 110
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("TOTAL_PAYABLE").Width = 90
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYMENT_AMOUNT").Width = 85
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYMENT_METHOD").Width = 90
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYMENT_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYMENT_AMOUNT").DefaultCellStyle.Format = "n0"
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("TOTAL_PAYABLE").DefaultCellStyle.Format = "n0"
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYMENT_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("PAYMENT_AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_PAYABLE_PAYMENTDataGridView.Columns("TOTAL_PAYABLE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End If

        tmpAmount = 0
    End Sub

    Private Sub txtVendor_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtVendor.TextChanged
        Try
            'SP_LIST_PAYABLE_PAYMENTBindingSource.Filter = "VENDOR_NAME LIKE '%" & txtVendor.Text.ToUpper & "%'"
            Dim dt As New DataTable
            Dim sql As String
            sql = "DECLARE	@return_value int" & vbCrLf & _
                  "EXEC	    @return_value = [dbo].[SP_LIST_PAYABLE_PAYMENT]" & vbCrLf & _
                  "		    @PERIOD = N'" & dtpPeriod.Value & "'," & vbCrLf & _
                  "		    @VENDOR_NAME = N'" & txtVendor.Text & "'" & vbCrLf & _
                  "SELECT	'Return Value' = @return_value"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xAdoAdapter.Fill(dt)
            Me.dgvLIST_PAYABLE_PAYMENTDataGridView.DataSource = dt
        Catch ex As Exception
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtVendor.Text = ""
            txtVendor.Focus()
        End Try

        For x As Integer = 0 To dgvLIST_PAYABLE_PAYMENTDataGridView.RowCount - 1
            tmpAmount = tmpAmount + dgvLIST_PAYABLE_PAYMENTDataGridView.Item("PAYMENT_AMOUNT", x).Value
            tmpAmount = tmpAmount
        Next

        If Language = "Indonesian" Then
            lblNoOfTrans.Text = "Jumlah Transaksi : " & dgvLIST_PAYABLE_PAYMENTDataGridView.RowCount
            lblTotalAmount.Text = "Jumlah Nominal : " & FormatNumber(tmpAmount, 0)
        Else
            lblNoOfTrans.Text = "No Of Transaction : " & dgvLIST_PAYABLE_PAYMENTDataGridView.RowCount
            lblTotalAmount.Text = "Jumlah Nominal : " & FormatNumber(tmpAmount, 0)
        End If

        tmpAmount = 0
    End Sub

    'Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
    '    Dim objf As New frmRepPayablePayment
    '    objf.period = dtpPeriod.Value
    '    objf.vendorname = txtVendor.Text
    '    objf.ReportViewer1.ShowRefreshButton = False
    '    objf.ReportViewer1.ZoomPercent = 100
    '    objf.WindowState = FormWindowState.Maximized
    '    objf.Show()
    'End Sub
End Class