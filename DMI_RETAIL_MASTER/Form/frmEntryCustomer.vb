﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frmEntryCustomer
    Dim xComm As New SqlCommand
    Dim cReader As SqlDataReader
    Dim tmpSaveMode As String
    Dim tmpCode As String
    Dim tmpCustomerCode, tmpCustomerName, tmpEditCode As String
    Dim tmpNoOfCustomerNo, tmpNoOfCustomerName, tmpUsedCustomer, tmpResult As Integer
    Dim tmpChange As Boolean
    Dim tmpEndDate As DateTime
    Public CUSTOMERBindingSource As New BindingSource

    Private Sub frmEntryCustomer_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If tmpChange = True And CUSTOMER_NOTextBox.Enabled = True Then
            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah Anda ingin menyimpan perubahan " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If
            If tmpvar = 6 Then
                Call cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub GetData()
        sql = "SELECT CUSTOMER_ID, CUSTOMER_NO, CUSTOMER_NAME, CUSTOMER_ADDRESS, " & _
              "CUSTOMER_PHONE, CUSTOMER_FAX, CUSTOMER_CITY, CUSTOMER_CONTACT_PERSON, " & _
              "USER_ID_INPUT, INPUT_DATE, USER_ID_UPDATE, UPDATE_DATE, MAX_AR, MAX_DAY, " & _
              "DISCOUNT, EFFECTIVE_END_DATE, EFFECTIVE_START_DATE FROM CUSTOMER ORDER BY CUSTOMER_NAME"
        dt = New DataTable
        sqladapter = New SqlDataAdapter(sql, xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        CUSTOMERBindingSource.DataSource = dt
        CUSTOMERBindingNavigator.BindingSource = CUSTOMERBindingSource
    End Sub

    Private Sub frmEntryCustomer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_CUSTOMER.CUSTOMER' table. You can move, or remove it, as needed.
        'Me.CUSTOMERTableAdapter.Fill(Me.DS_CUSTOMER.CUSTOMER)

        accFormName = Me.Text
        GetData()
        CUSTOMER_NOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", CUSTOMERBindingSource, "CUSTOMER_NO", True))
        CUSTOMER_NAMETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", CUSTOMERBindingSource, "CUSTOMER_NAME", True))
        CUSTOMER_ADDRESSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", CUSTOMERBindingSource, "CUSTOMER_ADDRESS", True))
        CUSTOMER_PHONETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", CUSTOMERBindingSource, "CUSTOMER_PHONE", True))
        CUSTOMER_FAXTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", CUSTOMERBindingSource, "CUSTOMER_FAX", True))
        CUSTOMER_CITYTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", CUSTOMERBindingSource, "CUSTOMER_CITY", True))
        CUSTOMER_CONTACT_PERSONTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", CUSTOMERBindingSource, "CUSTOMER_CONTACT_PERSON", True))
        DISCOUNTTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", CUSTOMERBindingSource, "DISCOUNT", True))
        DisableInputBox(Me)

        cmdSearchCode.Visible = True
        cmdSearchName.Visible = True
        CUSTOMERBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        tmpChange = False
        chkActive.Enabled = False


        If Language = "Indonesian" Then
            lblCity.Text = "Kota"
            lblAddress.Text = "Alamat"
            lblCode.Text = "Kode"
            lblMaxDay.Text = "Max Hari"
            lblName.Text = "Nama"
            cmdAdd.Text = "Tambah"
            cmdDelete.Text = "Hapus"
            cmdEdit.Text = "Ubah"
            cmdSave.Text = "Simpan"
            cmdUndo.Text = "Batal"
            Me.Text = "Input Pelanggan"
        End If
        If CUSTOMER_NAMETextBox.Text <> "" Then
            If CUSTOMERBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        End If
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If AllowAdd = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Insert"

        EnableInputBox(Me)
        CUSTOMERBindingSource.AddNew()

        xComm = New SqlCommand("SP_GENERATE_MASTER_CODE", xConn)
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@DOCUMENT_TYPE", "CUSTOMER")
        xComm.Parameters.Add("@RECEIPT_NO", SqlDbType.VarChar, 50)
        xComm.Parameters.Item("@RECEIPT_NO").Direction = ParameterDirection.Output

        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xComm.ExecuteNonQuery()
        tmpCustomerCode = xComm.Parameters.Item("@RECEIPT_NO").Value
        CUSTOMER_NOTextBox.Text = tmpCustomerCode

        cmdSearchCode.Visible = False
        cmdSearchName.Visible = False
        CUSTOMERBindingNavigator.Enabled = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        tmpChange = False
        chkActive.Checked = True
        chkActive.Enabled = True
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        If CUSTOMER_NAMETextBox.Text = "" Then Exit Sub
        If AllowEdit = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Update"

        EnableInputBox(Me)


        cmdSearchCode.Visible = False
        cmdSearchName.Visible = False
        CUSTOMERBindingNavigator.Enabled = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        chkActive.Enabled = True
        tmpEditCode = CUSTOMER_NOTextBox.Text

        tmpCustomerCode = CUSTOMER_NOTextBox.Text
        tmpCustomerName = CUSTOMER_NAMETextBox.Text

        tmpChange = False

        If CUSTOMERBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
            chkActive.Checked = False
        Else
            chkActive.Checked = True
        End If

    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        tmpSaveMode = ""

        DisableInputBox(Me)
        CUSTOMERBindingSource.CancelEdit()

        cmdSearchCode.Visible = True
        cmdSearchName.Visible = True
        CUSTOMERBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        chkActive.Enabled = False
        tmpChange = False
        CUSTOMERBindingSource_CurrentChanged(Nothing, Nothing)
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        If DISCOUNTTextBox.Text <> "" Then
            If CInt(DISCOUNTTextBox.Text) > 100 Then
                If Language = "Indonesian" Then
                    MsgBox("Discount seharusnya tidak lebih besar dari 100%" & vbCrLf & _
                  "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Discount should not greater than 100%" & vbCrLf & _
                  "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                DISCOUNTTextBox.Text = "0"
                Exit Sub
            End If
        Else
            DISCOUNTTextBox.Text = "0"
        End If

        If CUSTOMER_NAMETextBox.Text = "" Or CUSTOMER_NOTextBox.Text = "" Or CUSTOMER_CITYTextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please fill all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpCode = CUSTOMER_NOTextBox.Text

        If tmpSaveMode = "Insert" Then
            xComm = New SqlCommand("SP_CHECK_CUSTOMER_NO", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@CUSTOMER_NO", CUSTOMER_NOTextBox.Text)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            tmpNoOfCustomerNo = xComm.ExecuteScalar

            If tmpNoOfCustomerNo > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Nomor Pelanggan sudah ada di database !" & vbCrLf & _
                               "Tolong isi Kode yang berbeda!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Code already exist in Database." & vbCrLf & _
                           "Please enter another Code!", MsgBoxStyle.Critical, "DMI Retail")
                End If

                While tmpNoOfCustomerNo > 0
                    xComm = New SqlCommand("SP_GENERATE_MASTER_CODE", xConn)
                    xComm.CommandType = CommandType.StoredProcedure
                    xComm.Parameters.AddWithValue("@DOCUMENT_TYPE", "CUSTOMER")
                    If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                    xdreader = xComm.ExecuteReader
                    xdreader.Read()
                    If xdreader.HasRows Then
                        CUSTOMER_NOTextBox.Text = xdreader.Item(0)
                    End If

                    xComm = New SqlCommand("SP_CHECK_CUSTOMER_NO", xConn)
                    xComm.CommandType = CommandType.StoredProcedure
                    xComm.Parameters.AddWithValue("@CUSTOMER_NO", CUSTOMER_NOTextBox.Text)
                    xdreader = xComm.ExecuteReader
                    xdreader.Read()
                    tmpNoOfCustomerNo = xdreader.Item(0)
                End While
                Exit Sub
            End If

            'CUSTOMERTableAdapter.SP_CHECK_MASTER_CODE("CUSTOMER", CUSTOMER_NOTextBox.Text, tmpResult)
            'If tmpResult > 0 Then
            '    If Language = "Indonesian" Then
            '        MsgBox("No Transaksi telah digunakan oleh user lain." & vbCrLf & _
            '                                   "Nomor transaksi baru akan digunakan untuk transaksi ini.", _
            '                                   MsgBoxStyle.Critical, "DMI Retail")
            '    Else
            '        MsgBox("Receipt Number is used by another User." & vbCrLf & _
            '                                            "New Receipt Number will be assigned to this transaction.", _
            '                                            MsgBoxStyle.Critical, "DMI Retail")
            '    End If
            '    While tmpResult > 0
            '        CUSTOMERTableAdapter.SP_GENERATE_MASTER_CODE("CUSTOMER", CUSTOMER_NOTextBox.Text)
            '        CUSTOMERTableAdapter.SP_CHECK_MASTER_CODE("CUSTOMER", CUSTOMER_NOTextBox.Text, tmpResult)
            '    End While
            '    Exit Sub
            'End If


            Dim tmp As Integer = IIf(MAX_ARTextBox.Text = "", 0, MAX_ARTextBox.Text)
            Dim tmp2 As Integer = IIf(MAX_DAYTextBox.Text = "", 0, MAX_DAYTextBox.Text)
            Dim dsc As Integer = IIf(DISCOUNTTextBox.Text = "", 0, DISCOUNTTextBox.Text)

            xComm = New SqlCommand("SP_CUSTOMER", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "I")
            xComm.Parameters.AddWithValue("@CUSTOMER_ID", 0)
            xComm.Parameters.AddWithValue("@CUSTOMER_NO", CUSTOMER_NOTextBox.Text)
            xComm.Parameters.AddWithValue("@CUSTOMER_NAME", CUSTOMER_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@CUSTOMER_ADDRESS", CUSTOMER_ADDRESSTextBox.Text)
            xComm.Parameters.AddWithValue("@CUSTOMER_PHONE", CUSTOMER_PHONETextBox.Text)
            xComm.Parameters.AddWithValue("@CUSTOMER_FAX", CUSTOMER_FAXTextBox.Text)
            xComm.Parameters.AddWithValue("@CUSTOMER_CITY", CUSTOMER_CITYTextBox.Text)
            xComm.Parameters.AddWithValue("@CUSTOMER_CONTACT_PERSON", CUSTOMER_CONTACT_PERSONTextBox.Text)
            xComm.Parameters.AddWithValue("@MAX_AR", tmp)
            xComm.Parameters.AddWithValue("@MAX_DAY", tmp2)
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", UserId)
            xComm.Parameters.AddWithValue("@INPUT_DATE", Now)
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", 0)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", DateSerial(4000, 12, 31))
            xComm.Parameters.AddWithValue("@DISCOUNT", dsc)
            xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", Now)
            xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", IIf(chkActive.Checked, DateSerial(4000, 12, 31), Now))
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()
            'CUSTOMERTableAdapter.SP_CUSTOMER("I", _
            '                             0, _
            '                             CUSTOMER_NOTextBox.Text, _
            '                             CUSTOMER_NAMETextBox.Text, _
            '                             CUSTOMER_ADDRESSTextBox.Text, _
            '                             CUSTOMER_PHONETextBox.Text, _
            '                             CUSTOMER_FAXTextBox.Text, _
            '                             CUSTOMER_CITYTextBox.Text, _
            '                             CUSTOMER_CONTACT_PERSONTextBox.Text, _
            '                             tmp, _
            '                             tmp2, _
            '                             mdlGeneral.USER_ID, _
            '                             Now, _
            '                             0, _
            '                             DateSerial(4000, 12, 31), _
            '                             dsc, _
            '                             Now, _
            '                             DateSerial(4000, 12, 31))

        ElseIf tmpSaveMode = "Update" Then

            If tmpEditCode <> CUSTOMER_NOTextBox.Text Then
                xComm = New SqlCommand("SP_CHECK_CUSTOMER_NO", xConn)
                xComm.CommandType = CommandType.StoredProcedure
                xComm.Parameters.AddWithValue("@CUSTOMER_NO", CUSTOMER_NOTextBox.Text)
                If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                xdreader = xComm.ExecuteReader
                xdreader.Read()
                tmpNoOfCustomerNo = xdreader.Item(0)
                'CUSTOMERTableAdapter.SP_CHECK_CUSTOMER_NO(CUSTOMER_NOTextBox.Text, tmpNoOfCustomerNo)
                If tmpNoOfCustomerNo > 0 Then
                    If Language = "Indonesian" Then
                        MsgBox("Kode Pelanggan sudah ada di database !" & vbCrLf & _
                                   "Tolong isi Code yang berbeda !", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("Code already exist in Database." & vbCrLf & _
                               "Please enter another Code!", MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    CUSTOMER_NOTextBox.Focus()
                    Exit Sub
                End If
            End If

            If chkActive.Checked = True Then
                tmpEndDate = DateSerial(4000, 12, 31)
            Else
                tmpEndDate = Now
            End If

            If tmpCustomerCode = CUSTOMER_NOTextBox.Text Then
                tmpNoOfCustomerNo = 0
            Else
                xComm = New SqlCommand("SP_CHECK_CUSTOMER_NO", xConn)
                xComm.CommandType = CommandType.StoredProcedure
                xComm.Parameters.AddWithValue("@CUSTOMER_NO", CUSTOMER_NOTextBox.Text)
                If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                xdreader = xComm.ExecuteReader
                xdreader.Read()
                tmpNoOfCustomerNo = xdreader.Item(0)
                'CUSTOMERTableAdapter.SP_CHECK_CUSTOMER_NO(CUSTOMER_NOTextBox.Text, tmpNoOfCustomerNo)
            End If

            'Waktu edit, gak usah cek apakah ada nama customer di DB. Karena Nama cust sangat mungkin duplicate
            'If tmpCustomerName = CUSTOMER_NAMETextBox.Text Then
            '    tmpNoOfCustomerName = 0
            'Else
            '    connection()
            '    xComm = New SqlCommand("SP_CHECK_CUSTOMER_NAME", xConn)
            '    xComm.CommandType = CommandType.StoredProcedure
            '    xComm.Parameters.AddWithValue("@CUSTOMER_NAME", CUSTOMER_NAMETextBox.Text)
            '    xdreader = xComm.ExecuteReader
            '    xdreader.Read()
            '    tmpNoOfCustomerName = xdreader.Item(0)
            '    ' CUSTOMERTableAdapter.SP_CHECK_CUSTOMER_NAME(CUSTOMER_NAMETextBox.Text, tmpNoOfCustomerName)
            'End If

            'If tmpNoOfCustomerNo > 0 Or tmpNoOfCustomerName > 0 Then

            If tmpNoOfCustomerNo > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Kode Pelanggan sudah ada di database !" & vbCrLf & _
                           "Tolong isi Code yang berbeda !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Code already exist in Database." & vbCrLf & _
                           "Please enter another Code!", MsgBoxStyle.Critical, "DMI Retail")
                    Exit Sub
                End If
                Exit Sub
            End If
            xComm = New SqlCommand("SP_CUSTOMER", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "U")
            xComm.Parameters.AddWithValue("@CUSTOMER_ID", CUSTOMERBindingSource.Current("CUSTOMER_ID"))
            xComm.Parameters.AddWithValue("@CUSTOMER_NO", CUSTOMERBindingSource.Current("CUSTOMER_NO"))
            xComm.Parameters.AddWithValue("@CUSTOMER_NAME", CUSTOMER_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@CUSTOMER_ADDRESS", CUSTOMER_ADDRESSTextBox.Text)
            xComm.Parameters.AddWithValue("@CUSTOMER_PHONE", CUSTOMER_PHONETextBox.Text)
            xComm.Parameters.AddWithValue("@CUSTOMER_FAX", CUSTOMER_FAXTextBox.Text)
            xComm.Parameters.AddWithValue("@CUSTOMER_CITY", CUSTOMER_CITYTextBox.Text)
            xComm.Parameters.AddWithValue("@CUSTOMER_CONTACT_PERSON", CUSTOMER_CONTACT_PERSONTextBox.Text)
            xComm.Parameters.AddWithValue("@MAX_AR", 0)
            xComm.Parameters.AddWithValue("@MAX_DAY", 0)
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", CUSTOMERBindingSource.Current("USER_ID_INPUT"))
            xComm.Parameters.AddWithValue("@INPUT_DATE", CUSTOMERBindingSource.Current("INPUT_DATE"))
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", UserId)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", Now)
            xComm.Parameters.AddWithValue("@DISCOUNT", DISCOUNTTextBox.Text)
            xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", CUSTOMERBindingSource.Current("EFFECTIVE_START_DATE"))
            xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", tmpEndDate)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()
        End If

        tmpSaveMode = ""

        DisableInputBox(Me)
        CUSTOMERBindingSource.CancelEdit()

        cmdSearchCode.Visible = True
        cmdSearchName.Visible = True
        CUSTOMERBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        chkActive.Enabled = False
        GetData()
        'Me.CUSTOMERTableAdapter.Fill(Me.DS_CUSTOMER.CUSTOMER)
        CUSTOMERBindingSource.Position = CUSTOMERBindingSource.Find("CUSTOMER_NO", tmpCode)


        If Language = "Indonesian" Then
            MsgBox("Data berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
        End If

        tmpChange = False

        If CUSTOMER_NAMETextBox.Text <> "" Then
            If CUSTOMERBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        End If

    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If CUSTOMER_NAMETextBox.Text = "" Then Exit Sub
        If AllowDelete = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If Language = "Indonesian" Then
            If MsgBox("Hapus Pelanggan ?" & vbCrLf & CUSTOMER_NAMETextBox.Text, _
                 MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
            xComm = New SqlCommand("SP_CHECK_CUSTOMER_USED", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@CUSTOMER_NO", CUSTOMER_NOTextBox.Text)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            tmpUsedCustomer = xComm.ExecuteScalar

            If tmpUsedCustomer > 0 Then
                MsgBox("Pelanggan ini telah digunakan di transaksi yang lain !" & vbCrLf & _
                             "Anda tidak dapat menghapus data ini !", _
                             MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If
        Else
            If MsgBox("Delete CUSTOMER?" & vbCrLf & CUSTOMER_NAMETextBox.Text, _
                      MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
            xComm = New SqlCommand("SP_CHECK_CUSTOMER_USED", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@CUSTOMER_NO", CUSTOMER_NOTextBox.Text)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            tmpUsedCustomer = xComm.ExecuteScalar

            If tmpUsedCustomer > 0 Then
                MsgBox("This Customer is used by another transaction !" & vbCrLf & _
                         "You can not delete this data !", _
                         MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If
        End If
        xComm = New SqlCommand("SP_CUSTOMER", xConn)
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@METHOD", "D")
        xComm.Parameters.AddWithValue("@CUSTOMER_ID", CUSTOMERBindingSource.Current("CUSTOMER_ID"))
        xComm.Parameters.AddWithValue("@CUSTOMER_NO", CUSTOMERBindingSource.Current("CUSTOMER_NO"))
        xComm.Parameters.AddWithValue("@CUSTOMER_NAME", CUSTOMERBindingSource.Current("CUSTOMER_NAME"))
        xComm.Parameters.AddWithValue("@CUSTOMER_ADDRESS", CUSTOMERBindingSource.Current("CUSTOMER_ADDRESS"))
        xComm.Parameters.AddWithValue("@CUSTOMER_PHONE", CUSTOMERBindingSource.Current("CUSTOMER_PHONE"))
        xComm.Parameters.AddWithValue("@CUSTOMER_FAX", CUSTOMERBindingSource.Current("CUSTOMER_FAX"))
        xComm.Parameters.AddWithValue("@CUSTOMER_CITY", CUSTOMERBindingSource.Current("CUSTOMER_CITY"))
        xComm.Parameters.AddWithValue("@CUSTOMER_CONTACT_PERSON", CUSTOMERBindingSource.Current("CUSTOMER_CONTACT_PERSON"))
        xComm.Parameters.AddWithValue("@MAX_AR", CUSTOMERBindingSource.Current("MAX_AR"))
        xComm.Parameters.AddWithValue("@MAX_DAY", CUSTOMERBindingSource.Current("MAX_DAY"))
        xComm.Parameters.AddWithValue("@USER_ID_INPUT", CUSTOMERBindingSource.Current("USER_ID_INPUT"))
        xComm.Parameters.AddWithValue("@INPUT_DATE", CUSTOMERBindingSource.Current("INPUT_DATE"))
        xComm.Parameters.AddWithValue("@USER_ID_UPDATE", CUSTOMERBindingSource.Current("USER_ID_UPDATE"))
        xComm.Parameters.AddWithValue("@UPDATE_DATE", CUSTOMERBindingSource.Current("UPDATE_DATE"))
        xComm.Parameters.AddWithValue("@DISCOUNT", CUSTOMERBindingSource.Current("DISCOUNT"))
        xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", CUSTOMERBindingSource.Current("EFFECTIVE_START_DATE"))
        xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", Now)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xComm.ExecuteNonQuery()

        CUSTOMERBindingSource.RemoveCurrent()
        CUSTOMERBindingSource.Position = 0
        GetData()

        Try
            If CUSTOMERBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        Catch
        End Try

    End Sub

    Private Sub MAX_ARTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MAX_ARTextBox.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Asc(e.KeyChar) < Asc("0") Or Asc(e.KeyChar) > Asc("9") Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub MAX_DAYTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MAX_DAYTextBox.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Asc(e.KeyChar) < Asc("0") Or Asc(e.KeyChar) > Asc("9") Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub DISCOUNTTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DISCOUNTTextBox.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Asc(e.KeyChar) < Asc("0") Or Asc(e.KeyChar) > Asc("9") Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub cmdSearchName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearchName.Click
        mdlGeneral.tmpSearchMode = "Customer - Customer Name"
        frmSearchCustomer.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        CUSTOMERBindingSource.Position = CUSTOMERBindingSource.Find("CUSTOMER_ID", tmpSearchResult)
        tmpEditCode = CUSTOMER_NOTextBox.Text
    End Sub

    Private Sub cmdSearchCode_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearchCode.Click
        mdlGeneral.tmpSearchMode = "Customer - Customer Code"
        frmSearchCustomer.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        CUSTOMERBindingSource.Position = CUSTOMERBindingSource.Find("CUSTOMER_ID", tmpSearchResult)
        tmpEditCode = CUSTOMER_NOTextBox.Text
    End Sub

    Private Sub CUSTOMER_NOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CUSTOMER_NOTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub CUSTOMER_NAMETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CUSTOMER_NAMETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub CUSTOMER_ADDRESSTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CUSTOMER_ADDRESSTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub CUSTOMER_PHONETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CUSTOMER_PHONETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub CUSTOMER_FAXTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CUSTOMER_FAXTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub CUSTOMER_CITYTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CUSTOMER_CITYTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub CUSTOMER_CONTACT_PERSONTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CUSTOMER_CONTACT_PERSONTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub MAX_ARTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MAX_ARTextBox.TextChanged
        tmpChange = True
        If MAX_ARTextBox.Text = "" Then
            MAX_ARTextBox.Text = "0"
        End If
    End Sub

    Private Sub MAX_DAYTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MAX_DAYTextBox.TextChanged
        tmpChange = True
        If MAX_DAYTextBox.Text = "" Then
            MAX_DAYTextBox.Text = "0"
        End If
    End Sub

    'Private Sub DISCOUNTTextBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles DISCOUNTTextBox.LostFocus
    '    If CInt(DISCOUNTTextBox.Text) > 100 Then
    '        DISCOUNTTextBox.Text = "0"
    '    End If
    'End Sub

    Private Sub CUSTOMERBindingSource_CurrentChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If CUSTOMERBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        Catch
        End Try
    End Sub

    Private Sub DISCOUNTTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DISCOUNTTextBox.TextChanged
        tmpChange = True
        If DISCOUNTTextBox.Text = "" Then
            DISCOUNTTextBox.Text = "0"
        End If
    End Sub

    Private Sub BindingNavigatorPositionItem_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BindingNavigatorPositionItem.TextChanged
        Try
            If CUSTOMERBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        Catch
        End Try
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        AccessPrivilege(Me.Tag)
    End Sub

End Class
