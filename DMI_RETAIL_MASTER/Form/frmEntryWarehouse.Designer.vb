﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEntryWarehouse
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim FAXLabel As System.Windows.Forms.Label
        Dim EMAILLabel As System.Windows.Forms.Label
        Dim WAREHOUSE_CODELabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEntryWarehouse))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkActive = New System.Windows.Forms.CheckBox()
        Me.WAREHOUSE_CODETextBox = New System.Windows.Forms.TextBox()
        Me.lblPhone = New System.Windows.Forms.Label()
        Me.lblPic = New System.Windows.Forms.Label()
        Me.lblCountry = New System.Windows.Forms.Label()
        Me.lblCity = New System.Windows.Forms.Label()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.lblDesc = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.chkRummage = New System.Windows.Forms.CheckBox()
        Me.cmdSearch = New System.Windows.Forms.PictureBox()
        Me.WAREHOUSE_NAMETextBox = New System.Windows.Forms.TextBox()
        Me.DESCRIPTIONTextBox = New System.Windows.Forms.TextBox()
        Me.WAREHOUSE_ADDRESSTextBox = New System.Windows.Forms.TextBox()
        Me.WAREHOUSE_CITYTextBox = New System.Windows.Forms.TextBox()
        Me.WAREHOUSE_COUNTRYTextBox = New System.Windows.Forms.TextBox()
        Me.PICTextBox = New System.Windows.Forms.TextBox()
        Me.PHONETextBox = New System.Windows.Forms.TextBox()
        Me.FAXTextBox = New System.Windows.Forms.TextBox()
        Me.EMAILTextBox = New System.Windows.Forms.TextBox()
        Me.WAREHOUSEBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        FAXLabel = New System.Windows.Forms.Label()
        EMAILLabel = New System.Windows.Forms.Label()
        WAREHOUSE_CODELabel = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cmdSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WAREHOUSEBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.WAREHOUSEBindingNavigator.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'FAXLabel
        '
        FAXLabel.AutoSize = True
        FAXLabel.Location = New System.Drawing.Point(63, 308)
        FAXLabel.Name = "FAXLabel"
        FAXLabel.Size = New System.Drawing.Size(28, 15)
        FAXLabel.TabIndex = 14
        FAXLabel.Text = "Fax"
        '
        'EMAILLabel
        '
        EMAILLabel.AutoSize = True
        EMAILLabel.Location = New System.Drawing.Point(63, 336)
        EMAILLabel.Name = "EMAILLabel"
        EMAILLabel.Size = New System.Drawing.Size(38, 15)
        EMAILLabel.TabIndex = 16
        EMAILLabel.Text = "Email"
        '
        'WAREHOUSE_CODELabel
        '
        WAREHOUSE_CODELabel.AutoSize = True
        WAREHOUSE_CODELabel.Location = New System.Drawing.Point(63, 26)
        WAREHOUSE_CODELabel.Name = "WAREHOUSE_CODELabel"
        WAREHOUSE_CODELabel.Size = New System.Drawing.Size(39, 15)
        WAREHOUSE_CODELabel.TabIndex = 28
        WAREHOUSE_CODELabel.Text = "Code"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkActive)
        Me.GroupBox1.Controls.Add(WAREHOUSE_CODELabel)
        Me.GroupBox1.Controls.Add(Me.WAREHOUSE_CODETextBox)
        Me.GroupBox1.Controls.Add(Me.lblPhone)
        Me.GroupBox1.Controls.Add(Me.lblPic)
        Me.GroupBox1.Controls.Add(Me.lblCountry)
        Me.GroupBox1.Controls.Add(Me.lblCity)
        Me.GroupBox1.Controls.Add(Me.lblAddress)
        Me.GroupBox1.Controls.Add(Me.lblDesc)
        Me.GroupBox1.Controls.Add(Me.lblName)
        Me.GroupBox1.Controls.Add(Me.chkRummage)
        Me.GroupBox1.Controls.Add(Me.cmdSearch)
        Me.GroupBox1.Controls.Add(Me.WAREHOUSE_NAMETextBox)
        Me.GroupBox1.Controls.Add(Me.DESCRIPTIONTextBox)
        Me.GroupBox1.Controls.Add(Me.WAREHOUSE_ADDRESSTextBox)
        Me.GroupBox1.Controls.Add(Me.WAREHOUSE_CITYTextBox)
        Me.GroupBox1.Controls.Add(Me.WAREHOUSE_COUNTRYTextBox)
        Me.GroupBox1.Controls.Add(Me.PICTextBox)
        Me.GroupBox1.Controls.Add(Me.PHONETextBox)
        Me.GroupBox1.Controls.Add(FAXLabel)
        Me.GroupBox1.Controls.Add(Me.FAXTextBox)
        Me.GroupBox1.Controls.Add(EMAILLabel)
        Me.GroupBox1.Controls.Add(Me.EMAILTextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 28)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(515, 387)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'chkActive
        '
        Me.chkActive.AutoSize = True
        Me.chkActive.Location = New System.Drawing.Point(422, 335)
        Me.chkActive.Name = "chkActive"
        Me.chkActive.Size = New System.Drawing.Size(56, 17)
        Me.chkActive.TabIndex = 46
        Me.chkActive.Text = "Active"
        Me.chkActive.UseVisualStyleBackColor = True
        '
        'WAREHOUSE_CODETextBox
        '
        Me.WAREHOUSE_CODETextBox.Location = New System.Drawing.Point(152, 23)
        Me.WAREHOUSE_CODETextBox.Name = "WAREHOUSE_CODETextBox"
        Me.WAREHOUSE_CODETextBox.Size = New System.Drawing.Size(100, 22)
        Me.WAREHOUSE_CODETextBox.TabIndex = 1
        Me.WAREHOUSE_CODETextBox.Tag = "M"
        '
        'lblPhone
        '
        Me.lblPhone.AutoSize = True
        Me.lblPhone.Location = New System.Drawing.Point(63, 280)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.Size = New System.Drawing.Size(42, 15)
        Me.lblPhone.TabIndex = 25
        Me.lblPhone.Text = "Phone"
        '
        'lblPic
        '
        Me.lblPic.AutoSize = True
        Me.lblPic.Location = New System.Drawing.Point(63, 252)
        Me.lblPic.Name = "lblPic"
        Me.lblPic.Size = New System.Drawing.Size(25, 15)
        Me.lblPic.TabIndex = 24
        Me.lblPic.Text = "PIC"
        '
        'lblCountry
        '
        Me.lblCountry.AutoSize = True
        Me.lblCountry.Location = New System.Drawing.Point(63, 224)
        Me.lblCountry.Name = "lblCountry"
        Me.lblCountry.Size = New System.Drawing.Size(54, 15)
        Me.lblCountry.TabIndex = 23
        Me.lblCountry.Text = "Country"
        '
        'lblCity
        '
        Me.lblCity.AutoSize = True
        Me.lblCity.Location = New System.Drawing.Point(63, 196)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(30, 15)
        Me.lblCity.TabIndex = 22
        Me.lblCity.Text = "City"
        '
        'lblAddress
        '
        Me.lblAddress.AutoSize = True
        Me.lblAddress.Location = New System.Drawing.Point(63, 139)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(58, 15)
        Me.lblAddress.TabIndex = 21
        Me.lblAddress.Text = "Address"
        '
        'lblDesc
        '
        Me.lblDesc.AutoSize = True
        Me.lblDesc.Location = New System.Drawing.Point(63, 82)
        Me.lblDesc.Name = "lblDesc"
        Me.lblDesc.Size = New System.Drawing.Size(76, 15)
        Me.lblDesc.TabIndex = 20
        Me.lblDesc.Text = "Description"
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Location = New System.Drawing.Point(63, 54)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(40, 15)
        Me.lblName.TabIndex = 19
        Me.lblName.Text = "Name"
        '
        'chkRummage
        '
        Me.chkRummage.AutoSize = True
        Me.chkRummage.Location = New System.Drawing.Point(152, 361)
        Me.chkRummage.Name = "chkRummage"
        Me.chkRummage.Size = New System.Drawing.Size(74, 17)
        Me.chkRummage.TabIndex = 18
        Me.chkRummage.Text = "Rummage"
        Me.chkRummage.UseVisualStyleBackColor = True
        '
        'cmdSearch
        '
        Me.cmdSearch.Image = CType(resources.GetObject("cmdSearch.Image"), System.Drawing.Image)
        Me.cmdSearch.Location = New System.Drawing.Point(301, 51)
        Me.cmdSearch.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearch.Name = "cmdSearch"
        Me.cmdSearch.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearch.TabIndex = 18
        Me.cmdSearch.TabStop = False
        '
        'WAREHOUSE_NAMETextBox
        '
        Me.WAREHOUSE_NAMETextBox.Location = New System.Drawing.Point(152, 51)
        Me.WAREHOUSE_NAMETextBox.Name = "WAREHOUSE_NAMETextBox"
        Me.WAREHOUSE_NAMETextBox.Size = New System.Drawing.Size(142, 22)
        Me.WAREHOUSE_NAMETextBox.TabIndex = 2
        Me.WAREHOUSE_NAMETextBox.Tag = "M"
        '
        'DESCRIPTIONTextBox
        '
        Me.DESCRIPTIONTextBox.Location = New System.Drawing.Point(152, 79)
        Me.DESCRIPTIONTextBox.Multiline = True
        Me.DESCRIPTIONTextBox.Name = "DESCRIPTIONTextBox"
        Me.DESCRIPTIONTextBox.Size = New System.Drawing.Size(206, 51)
        Me.DESCRIPTIONTextBox.TabIndex = 3
        '
        'WAREHOUSE_ADDRESSTextBox
        '
        Me.WAREHOUSE_ADDRESSTextBox.Location = New System.Drawing.Point(152, 136)
        Me.WAREHOUSE_ADDRESSTextBox.Multiline = True
        Me.WAREHOUSE_ADDRESSTextBox.Name = "WAREHOUSE_ADDRESSTextBox"
        Me.WAREHOUSE_ADDRESSTextBox.Size = New System.Drawing.Size(206, 51)
        Me.WAREHOUSE_ADDRESSTextBox.TabIndex = 5
        '
        'WAREHOUSE_CITYTextBox
        '
        Me.WAREHOUSE_CITYTextBox.Location = New System.Drawing.Point(152, 193)
        Me.WAREHOUSE_CITYTextBox.Name = "WAREHOUSE_CITYTextBox"
        Me.WAREHOUSE_CITYTextBox.Size = New System.Drawing.Size(142, 22)
        Me.WAREHOUSE_CITYTextBox.TabIndex = 7
        Me.WAREHOUSE_CITYTextBox.Tag = "M"
        '
        'WAREHOUSE_COUNTRYTextBox
        '
        Me.WAREHOUSE_COUNTRYTextBox.Location = New System.Drawing.Point(152, 221)
        Me.WAREHOUSE_COUNTRYTextBox.Name = "WAREHOUSE_COUNTRYTextBox"
        Me.WAREHOUSE_COUNTRYTextBox.Size = New System.Drawing.Size(142, 22)
        Me.WAREHOUSE_COUNTRYTextBox.TabIndex = 9
        '
        'PICTextBox
        '
        Me.PICTextBox.Location = New System.Drawing.Point(152, 249)
        Me.PICTextBox.Name = "PICTextBox"
        Me.PICTextBox.Size = New System.Drawing.Size(142, 22)
        Me.PICTextBox.TabIndex = 11
        Me.PICTextBox.Tag = "M"
        '
        'PHONETextBox
        '
        Me.PHONETextBox.Location = New System.Drawing.Point(152, 277)
        Me.PHONETextBox.Name = "PHONETextBox"
        Me.PHONETextBox.Size = New System.Drawing.Size(142, 22)
        Me.PHONETextBox.TabIndex = 13
        '
        'FAXTextBox
        '
        Me.FAXTextBox.Location = New System.Drawing.Point(152, 305)
        Me.FAXTextBox.Name = "FAXTextBox"
        Me.FAXTextBox.Size = New System.Drawing.Size(142, 22)
        Me.FAXTextBox.TabIndex = 15
        '
        'EMAILTextBox
        '
        Me.EMAILTextBox.Location = New System.Drawing.Point(152, 333)
        Me.EMAILTextBox.Name = "EMAILTextBox"
        Me.EMAILTextBox.Size = New System.Drawing.Size(142, 22)
        Me.EMAILTextBox.TabIndex = 17
        '
        'WAREHOUSEBindingNavigator
        '
        Me.WAREHOUSEBindingNavigator.AddNewItem = Nothing
        Me.WAREHOUSEBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.WAREHOUSEBindingNavigator.DeleteItem = Nothing
        Me.WAREHOUSEBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.WAREHOUSEBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.WAREHOUSEBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.WAREHOUSEBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.WAREHOUSEBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.WAREHOUSEBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.WAREHOUSEBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.WAREHOUSEBindingNavigator.Name = "WAREHOUSEBindingNavigator"
        Me.WAREHOUSEBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.WAREHOUSEBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.WAREHOUSEBindingNavigator.Size = New System.Drawing.Size(543, 25)
        Me.WAREHOUSEBindingNavigator.TabIndex = 1
        Me.WAREHOUSEBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdUndo)
        Me.GroupBox2.Controls.Add(Me.cmdSave)
        Me.GroupBox2.Controls.Add(Me.cmdEdit)
        Me.GroupBox2.Controls.Add(Me.cmdDelete)
        Me.GroupBox2.Controls.Add(Me.cmdAdd)
        Me.GroupBox2.Location = New System.Drawing.Point(13, 421)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Size = New System.Drawing.Size(515, 55)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        '
        'cmdUndo
        '
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(220, 17)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(74, 29)
        Me.cmdUndo.TabIndex = 21
        Me.cmdUndo.Text = "&Undo"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSave.Location = New System.Drawing.Point(321, 17)
        Me.cmdSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(74, 29)
        Me.cmdSave.TabIndex = 22
        Me.cmdSave.Text = "&Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(119, 17)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(74, 29)
        Me.cmdEdit.TabIndex = 20
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(422, 17)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(74, 29)
        Me.cmdDelete.TabIndex = 23
        Me.cmdDelete.Text = "&Delete"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(18, 17)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(74, 29)
        Me.cmdAdd.TabIndex = 19
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'frmEntryWarehouse
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(543, 488)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.WAREHOUSEBindingNavigator)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmEntryWarehouse"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Entry Warehouse"
        Me.Text = "Entry Warehouse"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cmdSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WAREHOUSEBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.WAREHOUSEBindingNavigator.ResumeLayout(False)
        Me.WAREHOUSEBindingNavigator.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents WAREHOUSEBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents WAREHOUSE_NAMETextBox As System.Windows.Forms.TextBox
    Friend WithEvents DESCRIPTIONTextBox As System.Windows.Forms.TextBox
    Friend WithEvents WAREHOUSE_ADDRESSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents WAREHOUSE_CITYTextBox As System.Windows.Forms.TextBox
    Friend WithEvents WAREHOUSE_COUNTRYTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PICTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PHONETextBox As System.Windows.Forms.TextBox
    Friend WithEvents FAXTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EMAILTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents cmdSearch As System.Windows.Forms.PictureBox
    Friend WithEvents chkRummage As System.Windows.Forms.CheckBox
    Friend WithEvents lblPhone As System.Windows.Forms.Label
    Friend WithEvents lblPic As System.Windows.Forms.Label
    Friend WithEvents lblCountry As System.Windows.Forms.Label
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents lblDesc As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents WAREHOUSE_CODETextBox As System.Windows.Forms.TextBox
    Friend WithEvents chkActive As System.Windows.Forms.CheckBox
End Class
