﻿Imports System.Data.SqlClient

Public Class frmRepLocation
    Private Sub frmRepLocation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dt = New DataTable
        sql = "SELECT (SELECT COMPANY_NAME FROM COMPANY_INFORMATION)COMPANY_NAME," & _
                                        "(SELECT ADDRESS_LINE_1 FROM COMPANY_INFORMATION)ADDRESS_LINE_1," & _
                                        "(SELECT ADDRESS_LINE_3 FROM COMPANY_INFORMATION)ADDRESS_LINE_3," & _
                                        "(SELECT REGENCY FROM COMPANY_INFORMATION)REGENCY," & _
                                        "(SELECT PHONE_1 FROM COMPANY_INFORMATION)PHONE_1," & _
                                        "(SELECT FAX FROM COMPANY_INFORMATION)FAX," & _
                                        "LOCATION_ID ,LOCATION_NAME ,DESCRIPTION,EFFECTIVE_START_DATE ,EFFECTIVE_END_DATE  " & _
                                        " " & _
                                        "FROM dbo.LOCATION ORDER BY LOCATION_NAME"
        sqladapter = New SqlDataAdapter(sql, xConn)
        sqladapter.Fill(dt)
        Dim rpt As New rptLocation
        rpt.SetDataSource(dt)
        CrystalReportViewer1.ReportSource = rpt
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        CrystalReportViewer1.Refresh()
    End Sub
End Class