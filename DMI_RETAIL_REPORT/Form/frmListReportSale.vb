﻿Public Class frmListReportSale


    Dim tmpdate1, tmpdate2 As DateTime
    Public tmpDatePassing, tmpPeriod1, tmpPeriod2 As DateTime
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String, ByVal ptmpType As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.tmpType = ptmpType
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    Private Sub frmListReportSale_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F12 Then
            cmdReport_Click(Nothing, Nothing)
        End If
    End Sub


    Private Sub frmListReportSale_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'lblPeriod.Visible = False
        'lblTo.Visible = False
        'dtpPeriod.Visible = False
        'dtpPeriod2.Visible = False



        If tmpType = "M" Then
            dtpPeriod.Value = DateSerial(Today.Year, Today.Month, 1)
            dtpPeriod2.Value = DateSerial(Today.Year, Today.Month + 1, 0)
            dtpPeriod.Format = DateTimePickerFormat.Custom
            dtpPeriod.CustomFormat = "dd-MMM- yyyy"
            dtpPeriod2.Format = DateTimePickerFormat.Custom
            dtpPeriod2.CustomFormat = "dd-MMM-yyyy"
        ElseIf tmpType = "Y" Then
            dtpPeriod.Format = DateTimePickerFormat.Custom
            dtpPeriod.CustomFormat = "MMMM-yyyy"
            dtpPeriod2.Format = DateTimePickerFormat.Custom
            dtpPeriod2.CustomFormat = "MMMM-yyyy"
        End If

        Dim tmpTotal As Integer
        Dim tmpInvoice As Integer
        Dim tmpItem As Integer
        For n As Integer = 0 To SP_SALE_REPORTDataGridView.RowCount - 1
            tmpTotal = tmpTotal + SP_SALE_REPORTDataGridView.Item("GRAND_TOTAL", n).Value
            tmpTotal = tmpTotal
            tmpInvoice = tmpInvoice + SP_SALE_REPORTDataGridView.Item("INVOICE_TOTAL", n).Value
            tmpInvoice = tmpInvoice
            tmpItem = tmpItem + SP_SALE_REPORTDataGridView.Item("NO_OF_ITEM", n).Value
            tmpItem = tmpItem
        Next

        If Language = "Indonesian" Then
            Me.Text = "Daftar Penjualan"

            cmdGenerate.Text = "Proses"
            cmdReport.Text = "Cetak"

            SP_SALE_REPORTDataGridView.Columns("SALE_DATE").HeaderText = "Tgl Penjualan"
            SP_SALE_REPORTDataGridView.Columns("NO_OF_ITEM").HeaderText = "Jumlah Item"
            SP_SALE_REPORTDataGridView.Columns("INVOICE_TOTAL").HeaderText = "Jumlah Faktur"

            lblPeriod.Text = "Periode"
            lblTo.Text = "Ke"
            lblTotal.Text = "Jumlah Nominal : " & FormatNumber(tmpTotal, 0)
            lblNoOfTrans.Text = "Jumlah Transaksi : " & SP_SALE_REPORTDataGridView.RowCount
            lblInvoice.Text = "Jumlah Faktur : " & tmpInvoice
            lblItem.Text = "Jumlah Item : " & tmpItem
        Else
            lblTotal.Text = "Total amount : " & FormatNumber(tmpTotal, 0)
            lblNoOfTrans.Text = "No of transaction(s) : " & SP_SALE_REPORTDataGridView.RowCount
            lblInvoice.Text = "Total Invoice : " & tmpInvoice
            lblItem.Text = "Total Item : " & tmpItem
        End If

    End Sub


    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        If tmpType = "" Then
            Exit Sub
        End If


        tmpdate1 = DateSerial(Year(dtpPeriod.Value), _
                             Month(dtpPeriod.Value), 1)
        tmpdate2 = DateSerial(Year(dtpPeriod2.Value), _
                              Month(dtpPeriod2.Value) + 1, 0)

        'SP_SALE_REPORTDataGridView.Columns(0).ValueType =

        Cursor = Cursors.WaitCursor
        'SP_SALE_REPORTTableAdapter.Fill(Me.DS_SALE.SP_SALE_REPORT, tmpType, dtpPeriod.Value)
        If tmpType = "M" Then
            SP_SALE_REPORT_DAILYTableAdapter.Fill(Me.DS_SALE.SP_SALE_REPORT_DAILY, dtpPeriod.Value, dtpPeriod2.Value)
            SP_SALE_REPORTDataGridView.DataSource = SP_SALE_REPORT_DAILYBindingSource

        ElseIf tmpType = "Y" Then

            SP_SALE_REPORT_MONTHLYTableAdapter.Fill(Me.DS_SALE.SP_SALE_REPORT_MONTHLY, tmpdate1, tmpdate2)
            SP_SALE_REPORTDataGridView.DataSource = SP_SALE_REPORT_MONTHLYBindingSource

        End If
        Cursor = Cursors.Default

        Dim tmpTotal As Integer = 0
        Dim tmpInvoice As Integer = 0
        Dim tmpItem As Integer = 0
        For n As Integer = 0 To SP_SALE_REPORTDataGridView.RowCount - 1
            tmpTotal = tmpTotal + SP_SALE_REPORTDataGridView.Item("GRAND_TOTAL", n).Value
            tmpTotal = tmpTotal
            tmpInvoice = tmpInvoice + SP_SALE_REPORTDataGridView.Item("INVOICE_TOTAL", n).Value
            tmpInvoice = tmpInvoice
            tmpItem = tmpItem + SP_SALE_REPORTDataGridView.Item("NO_OF_ITEM", n).Value
            tmpItem = tmpItem
        Next

        If Language = "Indonesian" Then
            lblTotal.Text = "Jumlah Nominal : " & FormatNumber(tmpTotal, 0)
            lblNoOfTrans.Text = "Jumlah Transaksi : " & SP_SALE_REPORTDataGridView.RowCount
            lblInvoice.Text = "Jumlah Invoice : " & tmpInvoice
            lblItem.Text = "Jumlah Item : " & tmpItem
        Else
            lblTotal.Text = "Total amount : " & FormatNumber(tmpTotal, 0)
            lblNoOfTrans.Text = "No of transaction(s) : " & SP_SALE_REPORTDataGridView.RowCount
            lblInvoice.Text = "Total Invoice : " & tmpInvoice
            lblItem.Text = "Total Item : " & tmpItem
        End If

    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        If tmpType = "M" Then
            Dim RPV As New frmRepSaleReport
            RPV.date1 = dtpPeriod.Value
            RPV.date2 = dtpPeriod2.Value
            RPV.ReportViewer1.ZoomPercent = 100
            RPV.WindowState = FormWindowState.Maximized
            RPV.Show()
        ElseIf tmpType = "Y" Then
            tmpPeriod1 = DateSerial(dtpPeriod.Value.Year, dtpPeriod.Value.Month, 1)
            tmpPeriod2 = DateSerial(dtpPeriod2.Value.Year, dtpPeriod2.Value.Month + 1, 0)
            Dim RPV As New frmRepSaleReportMonthly
            RPV.tmpPeriod1 = tmpPeriod1
            RPV.tmpPeriod2 = tmpPeriod2
            RPV.ReportViewer1.ZoomPercent = 100
            RPV.WindowState = FormWindowState.Maximized
            RPV.Show()
        End If
    End Sub


    Private Sub SP_SALE_REPORTDataGridView_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles SP_SALE_REPORTDataGridView.DoubleClick
        Try
            If tmpType = "M" Then
                tmpDatePassing = SP_SALE_REPORT_DAILYBindingSource.Current("SALE_DATE")
            ElseIf tmpType = "Y" Then
                tmpDatePassing = SP_SALE_REPORT_MONTHLYBindingSource.Current("SALE_DATE")
            End If
            frmDetailSale.tmpDatePassing = tmpDatePassing
            frmDetailSale.ShowDialog()
        Catch ex As Exception

        End Try

    End Sub
End Class
