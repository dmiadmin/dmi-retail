﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frmListProduct
    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter

    Private Sub frmListProduct_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim xDS_Produk As New DataSet
        With xComm
            .Connection = xConn
            .CommandText = "	SELECT PRODUCT_ID ," & _
            " PRODUCT_CODE," & _
            " PRODUCT_NAME," & _
            " [DESCRIPTION]," & _
            " SELLING_PRICE ," & _
            " (SELECT LOCATION_NAME FROM LOCATION WHERE LOCATION_ID = PRODUCT .LOCATION_ID ) LOCATION_NAME," & _
            " SAFE_STOCK ," & _
            " CATEGORY = CASE CATEGORY" & _
            " WHEN 0 THEN 'SERVICES'" & _
            " WHEN 1 THEN 'PRODUCT'" & _
            " WHEN 2 THEN 'NON STOCK'" & _
            " END," & _
            " EFFECTIVE_START_DATE," & _
            " EFFECTIVE_END_DATE" & _
            " FROM PRODUCT" & _
            " ORDER BY PRODUCT_NAME "
            .CommandType = CommandType.Text
        End With
        xAdoAdapter.SelectCommand = xComm
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        Me.xAdoAdapter.Fill(xDS_Produk, "PRODUCT")
        Me.dgvListProduct.DataSource = xDS_Produk.Tables("PRODUCT")

        Label2.Visible = True
        If Language = "Indonesian" Then
            Me.Text = "Daftar Produk"
            Label1.Text = "Nama Produk"
            CheckBox1.Text = "Tidak Terhapus"
            dgvListProduct.Columns("PRODUCT_ID").Visible = False
            dgvListProduct.Columns("EFFECTIVE_START_DATE").Visible = False
            dgvListProduct.Columns("EFFECTIVE_END_DATE").Visible = False

            dgvListProduct.Columns("PRODUCT_CODE").HeaderText = "Kode Produk"
            dgvListProduct.Columns("PRODUCT_CODE").Width = 75
            dgvListProduct.Columns("DESCRIPTION").HeaderText = "Keterangan"
            dgvListProduct.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            dgvListProduct.Columns("LOCATION_NAME").HeaderText = "Lokasi"
            dgvListProduct.Columns("SELLING_PRICE").HeaderText = "Harga Jual"
            dgvListProduct.Columns("SELLING_PRICE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvListProduct.Columns("SELLING_PRICE").DefaultCellStyle.Format = "n0"
            dgvListProduct.Columns("SAFE_STOCK").HeaderText = "Stock Aman"
            dgvListProduct.Columns("SAFE_STOCK").Width = 50
            dgvListProduct.Columns("SAFE_STOCK").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvListProduct.Columns("CATEGORY").HeaderText = "Kategori"
            Label2.Text = "Jumlah Barang : " & dgvListProduct.RowCount
        Else
            dgvListProduct.Columns("PRODUCT_ID").Visible = False
            dgvListProduct.Columns("EFFECTIVE_START_DATE").Visible = False
            dgvListProduct.Columns("EFFECTIVE_END_DATE").Visible = False
            dgvListProduct.Columns("PRODUCT_CODE").HeaderText = "Product Code"
            dgvListProduct.Columns("PRODUCT_CODE").Width = 75
            dgvListProduct.Columns("DESCRIPTION").HeaderText = "Description"
            dgvListProduct.Columns("PRODUCT_NAME").HeaderText = "Product Name"
            dgvListProduct.Columns("LOCATION_NAME").HeaderText = "Location"
            dgvListProduct.Columns("SELLING_PRICE").HeaderText = "Selling Price"
            dgvListProduct.Columns("SELLING_PRICE").DefaultCellStyle.Format = "n0"
            dgvListProduct.Columns("SELLING_PRICE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvListProduct.Columns("SAFE_STOCK").HeaderText = "Safe Stock"
            dgvListProduct.Columns("SAFE_STOCK").Width = 50
            dgvListProduct.Columns("SAFE_STOCK").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvListProduct.Columns("CATEGORY").HeaderText = "Category"
            Label2.Text = "No Of Item : " & dgvListProduct.RowCount
        End If

    End Sub

    Private Sub txtProductName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProductName.TextChanged
        Try
            If CheckBox1.Checked = True Then
                Dim xDS_Produk As New DataSet
                With xComm
                    .Connection = xConn
                    .CommandText = "	SELECT PRODUCT_ID ," & _
                    " PRODUCT_CODE," & _
                    " PRODUCT_NAME," & _
                    " [DESCRIPTION]," & _
                    " SELLING_PRICE ," & _
                    " (SELECT LOCATION_NAME FROM LOCATION WHERE LOCATION_ID = PRODUCT .LOCATION_ID ) LOCATION_NAME," & _
                    " SAFE_STOCK ," & _
                    " CATEGORY = CASE CATEGORY" & _
                    " WHEN 0 THEN 'SERVICES'" & _
                    " WHEN 1 THEN 'PRODUCT'" & _
                    " WHEN 2 THEN 'NON STOCK'" & _
                    " END," & _
                    " EFFECTIVE_START_DATE," & _
                    " EFFECTIVE_END_DATE" & _
                    " FROM PRODUCT" & _
                    " WHERE PRODUCT_NAME LIKE '%" & txtProductName.Text.ToUpper & "%'" & _
                    " AND EFFECTIVE_END_DATE > GETDATE()" & _
                    " ORDER BY PRODUCT_NAME "
                    .CommandType = CommandType.Text
                End With
                xAdoAdapter.SelectCommand = xComm
                If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                Me.xAdoAdapter.Fill(xDS_Produk, "PRODUCT")
                Me.dgvListProduct.DataSource = xDS_Produk.Tables("PRODUCT")
            Else
                Dim xDS_Produk As New DataSet
                With xComm
                    .Connection = xConn
                    .CommandText = "	SELECT PRODUCT_ID ," & _
                    " PRODUCT_CODE," & _
                    " PRODUCT_NAME," & _
                    " [DESCRIPTION]," & _
                    " SELLING_PRICE ," & _
                    " (SELECT LOCATION_NAME FROM LOCATION WHERE LOCATION_ID = PRODUCT .LOCATION_ID ) LOCATION_NAME," & _
                    " SAFE_STOCK ," & _
                    " CATEGORY = CASE CATEGORY" & _
                    " WHEN 0 THEN 'SERVICES'" & _
                    " WHEN 1 THEN 'PRODUCT'" & _
                    " WHEN 2 THEN 'NON STOCK'" & _
                    " END," & _
                    " EFFECTIVE_START_DATE," & _
                    " EFFECTIVE_END_DATE" & _
                    " FROM PRODUCT" & _
                    " WHERE PRODUCT_NAME LIKE '%" & txtProductName.Text.ToUpper & "%'" & _
                    " ORDER BY PRODUCT_NAME "
                    .CommandType = CommandType.Text
                End With
                xAdoAdapter.SelectCommand = xComm
                If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                Me.xAdoAdapter.Fill(xDS_Produk, "PRODUCT")
                Me.dgvListProduct.DataSource = xDS_Produk.Tables("PRODUCT")
            End If
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtProductName.Text = ""
            txtProductName.Focus()
        End Try
        If Language = "Indonesian" Then
            Label2.Text = "Jumlah Barang : " & dgvListProduct.RowCount
        Else
            Label2.Text = "No Of Item : " & dgvListProduct.RowCount
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            Dim xDS_Produk As New DataSet
            With xComm
                .Connection = xConn
                .CommandText = "	SELECT PRODUCT_ID ," & _
                " PRODUCT_CODE," & _
                " PRODUCT_NAME," & _
                " [DESCRIPTION]," & _
                " SELLING_PRICE ," & _
                " (SELECT LOCATION_NAME FROM LOCATION WHERE LOCATION_ID = PRODUCT .LOCATION_ID ) LOCATION_NAME," & _
                " SAFE_STOCK ," & _
                " CATEGORY = CASE CATEGORY" & _
                " WHEN 0 THEN 'SERVICES'" & _
                " WHEN 1 THEN 'PRODUCT'" & _
                " WHEN 2 THEN 'NON STOCK'" & _
                " END," & _
                " EFFECTIVE_START_DATE," & _
                " EFFECTIVE_END_DATE" & _
                " FROM PRODUCT" & _
                " WHERE PRODUCT_NAME LIKE '%" & txtProductName.Text.ToUpper & "%'" & _
                " AND EFFECTIVE_END_DATE > GETDATE()" & _
                " ORDER BY PRODUCT_NAME "
                .CommandType = CommandType.Text
            End With
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            Me.xAdoAdapter.Fill(xDS_Produk, "PRODUCT")
            Me.dgvListProduct.DataSource = xDS_Produk.Tables("PRODUCT")
        Else
            Dim xDS_Produk As New DataSet
            With xComm
                .Connection = xConn
                .CommandText = "	SELECT PRODUCT_ID ," & _
                " PRODUCT_CODE," & _
                " PRODUCT_NAME," & _
                " [DESCRIPTION]," & _
                " SELLING_PRICE ," & _
                " (SELECT LOCATION_NAME FROM LOCATION WHERE LOCATION_ID = PRODUCT .LOCATION_ID ) LOCATION_NAME," & _
                " SAFE_STOCK ," & _
                " CATEGORY = CASE CATEGORY" & _
                " WHEN 0 THEN 'SERVICES'" & _
                " WHEN 1 THEN 'PRODUCT'" & _
                " WHEN 2 THEN 'NON STOCK'" & _
                " END," & _
                " EFFECTIVE_START_DATE," & _
                " EFFECTIVE_END_DATE" & _
                " FROM PRODUCT" & _
                " WHERE PRODUCT_NAME LIKE '%" & txtProductName.Text.ToUpper & "%'" & _
                " ORDER BY PRODUCT_NAME "
                .CommandType = CommandType.Text
            End With
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            Me.xAdoAdapter.Fill(xDS_Produk, "PRODUCT")
            Me.dgvListProduct.DataSource = xDS_Produk.Tables("PRODUCT")
        End If
        If Language = "Indonesian" Then
            Label2.Text = "Jumlah Barang : " & dgvListProduct.RowCount
        Else
            Label2.Text = "No Of Item : " & dgvListProduct.RowCount
        End If

    End Sub

    Private Sub dgvListProduct_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListProduct.CellDoubleClick
        Try
            Dim tmpProductId As Integer = dgvListProduct.Item("PRODUCT_ID", e.RowIndex).Value
            frmEntryProduct.MdiParent = ObjFormMain
            frmEntryProduct.Show()
            frmEntryProduct.BringToFront()
            tmpForm = frmEntryProduct
            tmpRecent = New frmEntryProduct
            frmEntryProduct.PRODUCTBindingSource.Position = frmEntryProduct.PRODUCTBindingSource.Find("PRODUCT_ID", tmpProductId)
            Me.Close()
        Catch ex As Exception

        End Try
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        AccessPrivilege(Me.Tag)
    End Sub
End Class