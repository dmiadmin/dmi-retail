﻿Imports System.Data.SqlClient

Public Class ClassMaster
    Public Sub SetConnections(ByRef pXConn As SqlConnection,
                              ByRef pXConn1 As SqlConnection,
                              ByRef pXConn2 As SqlConnection)
        xConn = pXConn
        xConn1 = pXConn1
        Xconn2 = pXConn2
    End Sub

    Public Sub SetUserInformations(ByRef pUserID As Integer,
                                   ByRef pUserName As String,
                                   ByRef pUserTypeId As Integer)
        UserId = pUserID
        UserName = pUserName
        UserTypeId = pUserTypeId
    End Sub

    Public Sub SetMdiParentForm(ByRef pMdiParentForm As Form)
        ObjFormMain = pMdiParentForm
    End Sub

    Public Sub SetLanguage(ByVal pLanguage As String)
        Language = pLanguage
    End Sub

End Class
