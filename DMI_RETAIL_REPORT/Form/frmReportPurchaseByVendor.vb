﻿Imports System.Data.SqlClient
Imports System.Data
Public Class frmReportPurchaseByVendor

    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
   

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        'Me.SP_PURCHASE_REPORT_BY_VENDORTableAdapter.Fill(Me.DS_PURCHASE.SP_PURCHASE_REPORT_BY_VENDOR, dtpPeriod1.Value, dtpPeriod2.Value)
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC 	@return_value = [dbo].[SP_PURCHASE_REPORT_BY_VENDOR]" & vbCrLf & _
              "         @PERIOD1 = N'" & dtpPeriod1.Value & "'," & vbCrLf & _
              "    		@PERIOD2 = N'" & dtpPeriod2.Value & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.dgvRptPurchaseByVendor.DataSource = dt
        If Language = "Indonesian" Then
            dgvRptPurchaseByVendor.Columns("VENDOR_NAME").HeaderText = "Nama Supplier"
            dgvRptPurchaseByVendor.Columns("NO_OF_ITEM").HeaderText = "Jumlah Item"
            dgvRptPurchaseByVendor.Columns("NO_OF_INVOICE").HeaderText = "Jumlah Faktur"
            dgvRptPurchaseByVendor.Columns("TOTAL_AMOUNT").HeaderText = "Jumlah Nominal"
            dgvRptPurchaseByVendor.Columns("REP_PERIOD1").Visible = False
            dgvRptPurchaseByVendor.Columns("REP_PERIOD2").Visible = False
            dgvRptPurchaseByVendor.Columns("VENDOR_NAME").Width = 150
            dgvRptPurchaseByVendor.Columns("NO_OF_ITEM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptPurchaseByVendor.Columns("NO_OF_INVOICE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptPurchaseByVendor.Columns("TOTAL_AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptPurchaseByVendor.Columns("TOTAL_AMOUNT").DefaultCellStyle.Format = "n0"
        Else
            dgvRptPurchaseByVendor.Columns("VENDOR_NAME").HeaderText = "Vendor Name"
            dgvRptPurchaseByVendor.Columns("NO_OF_ITEM").HeaderText = "No Of Item"
            dgvRptPurchaseByVendor.Columns("NO_OF_INVOICE").HeaderText = "No Of Invoice"
            dgvRptPurchaseByVendor.Columns("TOTAL_AMOUNT").HeaderText = "Total Amount"
            dgvRptPurchaseByVendor.Columns("REP_PERIOD1").Visible = False
            dgvRptPurchaseByVendor.Columns("REP_PERIOD2").Visible = False
            dgvRptPurchaseByVendor.Columns("VENDOR_NAME").Width = 150
            dgvRptPurchaseByVendor.Columns("NO_OF_ITEM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptPurchaseByVendor.Columns("NO_OF_INVOICE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptPurchaseByVendor.Columns("TOTAL_AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptPurchaseByVendor.Columns("TOTAL_AMOUNT").DefaultCellStyle.Format = "n0"
        End If
    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        Dim objf As New frmRepPurchaseByVendor
        objf.date1 = dtpPeriod1.Value
        objf.date2 = dtpPeriod2.Value
        objf.ReportViewer1.ShowRefreshButton = False
        objf.ReportViewer1.ZoomPercent = 100
        objf.WindowState = FormWindowState.Maximized
        objf.Show()
    End Sub

    Private Sub frmReportPurchaseByVendor_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dtpPeriod1.Value = DateSerial(Today.Year, Today.Month, 1)
        dtpPeriod2.Value = DateSerial(Today.Year, Today.Month + 1, 0)

        If Language = "Indonesian" Then
            lblFrom.Text = "Periode"
            cmdGenerate.Text = "Proses"
            cmdReport.Text = "Cetak"

            'dgvRptPurchaseByVendor.Columns("VENDOR_NAME").HeaderText = "Nama Supplier"
            'dgvRptPurchaseByVendor.Columns("NO_OF_ITEM").HeaderText = "Jumlah Item"
            'dgvRptPurchaseByVendor.Columns("NO_OF_INVOICE").HeaderText = "Jumlah Faktur"
            'dgvRptPurchaseByVendor.Columns("TOTAL_AMOUNT").HeaderText = "Jumlah Nominal"

        End If
    End Sub
End Class