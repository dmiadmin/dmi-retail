﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListSafeStock
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListSafeStock))
        Me.SP_NOTIFICATION_PRODUCTDataGridView = New System.Windows.Forms.DataGridView()
        Me.PRODUCT_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QUANTITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAFE_STOCK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VENDOR_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VENDOR_CANTACT_PERSON = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VENDOR_PHONE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SP_NOTIFICATION_PRODUCTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmdGenerate = New System.Windows.Forms.Button()
        Me.cmdSearchName = New System.Windows.Forms.PictureBox()
        Me.PRODUCT_CODEComboBox = New System.Windows.Forms.ComboBox()
        Me.SP_LIST_PRODUCT_GOODSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PRODUCT_NAMETextBox = New System.Windows.Forms.TextBox()
        Me.PRODUCTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DSPRODUCTSTOCKBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.SP_NOTIFICATION_PRODUCTDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_NOTIFICATION_PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_LIST_PRODUCT_GOODSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DSPRODUCTSTOCKBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SP_NOTIFICATION_PRODUCTDataGridView
        '
        Me.SP_NOTIFICATION_PRODUCTDataGridView.AllowUserToAddRows = False
        Me.SP_NOTIFICATION_PRODUCTDataGridView.AllowUserToDeleteRows = False
        Me.SP_NOTIFICATION_PRODUCTDataGridView.AutoGenerateColumns = False
        Me.SP_NOTIFICATION_PRODUCTDataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SP_NOTIFICATION_PRODUCTDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.SP_NOTIFICATION_PRODUCTDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.SP_NOTIFICATION_PRODUCTDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PRODUCT_NAME, Me.QUANTITY, Me.SAFE_STOCK, Me.VENDOR_NAME, Me.VENDOR_CANTACT_PERSON, Me.VENDOR_PHONE})
        Me.SP_NOTIFICATION_PRODUCTDataGridView.DataSource = Me.SP_NOTIFICATION_PRODUCTBindingSource
        Me.SP_NOTIFICATION_PRODUCTDataGridView.Location = New System.Drawing.Point(25, 137)
        Me.SP_NOTIFICATION_PRODUCTDataGridView.Name = "SP_NOTIFICATION_PRODUCTDataGridView"
        Me.SP_NOTIFICATION_PRODUCTDataGridView.ReadOnly = True
        Me.SP_NOTIFICATION_PRODUCTDataGridView.RowHeadersWidth = 28
        Me.SP_NOTIFICATION_PRODUCTDataGridView.Size = New System.Drawing.Size(675, 171)
        Me.SP_NOTIFICATION_PRODUCTDataGridView.TabIndex = 0
        '
        'PRODUCT_NAME
        '
        Me.PRODUCT_NAME.DataPropertyName = "PRODUCT_NAME"
        Me.PRODUCT_NAME.HeaderText = "Product Name"
        Me.PRODUCT_NAME.Name = "PRODUCT_NAME"
        Me.PRODUCT_NAME.ReadOnly = True
        Me.PRODUCT_NAME.Width = 130
        '
        'QUANTITY
        '
        Me.QUANTITY.DataPropertyName = "QUANTITY"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.QUANTITY.DefaultCellStyle = DataGridViewCellStyle2
        Me.QUANTITY.HeaderText = "Qty."
        Me.QUANTITY.Name = "QUANTITY"
        Me.QUANTITY.ReadOnly = True
        Me.QUANTITY.Width = 70
        '
        'SAFE_STOCK
        '
        Me.SAFE_STOCK.DataPropertyName = "SAFE_STOCK"
        Me.SAFE_STOCK.HeaderText = "Safe Stock"
        Me.SAFE_STOCK.Name = "SAFE_STOCK"
        Me.SAFE_STOCK.ReadOnly = True
        '
        'VENDOR_NAME
        '
        Me.VENDOR_NAME.DataPropertyName = "VENDOR_NAME"
        Me.VENDOR_NAME.HeaderText = "Vendor Name"
        Me.VENDOR_NAME.Name = "VENDOR_NAME"
        Me.VENDOR_NAME.ReadOnly = True
        Me.VENDOR_NAME.Width = 120
        '
        'VENDOR_CANTACT_PERSON
        '
        Me.VENDOR_CANTACT_PERSON.DataPropertyName = "VENDOR_CONTACT_PERSON"
        Me.VENDOR_CANTACT_PERSON.HeaderText = "Vendor Contact-Person"
        Me.VENDOR_CANTACT_PERSON.Name = "VENDOR_CANTACT_PERSON"
        Me.VENDOR_CANTACT_PERSON.ReadOnly = True
        Me.VENDOR_CANTACT_PERSON.Width = 110
        '
        'VENDOR_PHONE
        '
        Me.VENDOR_PHONE.DataPropertyName = "VENDOR_PHONE"
        Me.VENDOR_PHONE.HeaderText = "Vendor Phone"
        Me.VENDOR_PHONE.Name = "VENDOR_PHONE"
        Me.VENDOR_PHONE.ReadOnly = True
        Me.VENDOR_PHONE.Width = 90
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cmdGenerate)
        Me.GroupBox1.Controls.Add(Me.cmdSearchName)
        Me.GroupBox1.Controls.Add(Me.PRODUCT_CODEComboBox)
        Me.GroupBox1.Controls.Add(Me.PRODUCT_NAMETextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(25, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(675, 109)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(24, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 15)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Product"
        '
        'cmdGenerate
        '
        Me.cmdGenerate.Image = CType(resources.GetObject("cmdGenerate.Image"), System.Drawing.Image)
        Me.cmdGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdGenerate.Location = New System.Drawing.Point(287, 64)
        Me.cmdGenerate.Name = "cmdGenerate"
        Me.cmdGenerate.Size = New System.Drawing.Size(99, 39)
        Me.cmdGenerate.TabIndex = 3
        Me.cmdGenerate.Text = "Generate"
        Me.cmdGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdGenerate.UseVisualStyleBackColor = True
        '
        'cmdSearchName
        '
        Me.cmdSearchName.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchName.Image = CType(resources.GetObject("cmdSearchName.Image"), System.Drawing.Image)
        Me.cmdSearchName.Location = New System.Drawing.Point(564, 29)
        Me.cmdSearchName.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchName.Name = "cmdSearchName"
        Me.cmdSearchName.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearchName.TabIndex = 19
        Me.cmdSearchName.TabStop = False
        '
        'PRODUCT_CODEComboBox
        '
        Me.PRODUCT_CODEComboBox.DataSource = Me.SP_LIST_PRODUCT_GOODSBindingSource
        Me.PRODUCT_CODEComboBox.DisplayMember = "PRODUCT_CODE"
        Me.PRODUCT_CODEComboBox.FormattingEnabled = True
        Me.PRODUCT_CODEComboBox.Location = New System.Drawing.Point(83, 29)
        Me.PRODUCT_CODEComboBox.Name = "PRODUCT_CODEComboBox"
        Me.PRODUCT_CODEComboBox.Size = New System.Drawing.Size(152, 23)
        Me.PRODUCT_CODEComboBox.TabIndex = 1
        Me.PRODUCT_CODEComboBox.ValueMember = "PRODUCT_ID"
        '
        'PRODUCT_NAMETextBox
        '
        Me.PRODUCT_NAMETextBox.Location = New System.Drawing.Point(241, 29)
        Me.PRODUCT_NAMETextBox.Name = "PRODUCT_NAMETextBox"
        Me.PRODUCT_NAMETextBox.ReadOnly = True
        Me.PRODUCT_NAMETextBox.Size = New System.Drawing.Size(316, 22)
        Me.PRODUCT_NAMETextBox.TabIndex = 2
        '
        'PRODUCTBindingSource
        '
        Me.PRODUCTBindingSource.DataMember = "PRODUCT"
        Me.PRODUCTBindingSource.DataSource = Me.DSPRODUCTSTOCKBindingSource
        '
        'frmListSafeStock
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(721, 336)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.SP_NOTIFICATION_PRODUCTDataGridView)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmListSafeStock"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Notification Safe Stock"
        CType(Me.SP_NOTIFICATION_PRODUCTDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_NOTIFICATION_PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_LIST_PRODUCT_GOODSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DSPRODUCTSTOCKBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SP_NOTIFICATION_PRODUCTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_NOTIFICATION_PRODUCTDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdGenerate As System.Windows.Forms.Button
    Friend WithEvents cmdSearchName As System.Windows.Forms.PictureBox
    Friend WithEvents PRODUCT_CODEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents PRODUCT_NAMETextBox As System.Windows.Forms.TextBox
    Friend WithEvents DSPRODUCTSTOCKBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PRODUCTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PRODUCT_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QUANTITY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAFE_STOCK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VENDOR_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VENDOR_CANTACT_PERSON As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VENDOR_PHONE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SP_LIST_PRODUCT_GOODSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
