﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frmListSaleReturn
    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter

    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub

    'Private Sub frmListSaleReturn_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
    '    If e.KeyCode = Keys.F12 Then
    '        cmdReport_Click(Nothing, Nothing)
    '    End If
    'End Sub

    Private Sub frmListSaleReturn_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        dtpPeriod.Value = DateSerial(Today.Year, Today.Month, 1)
        dtpPeriod2.Value = DateSerial(Today.Year, Today.Month + 1, 0)

        If Language = "Indonesian" Then
            Me.Text = "Daftar Retur Penjualan"
            lblInvoiceNo.Text = "No Retur"
            cmdReport.Text = "Cetak"

            'dgvListSaleReturn.Columns("RETURN_NO").HeaderText = "No Retur"
            'dgvListSaleReturn.Columns("RETURN_DATE").HeaderText = "Tanggal Retur"
            'dgvListSaleReturn.Columns("RETURN_TYPE").HeaderText = "Tipe Retur"
            'dgvListSaleReturn.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            'dgvListSaleReturn.Columns("WAREHOUSE_NAME").HeaderText = "Gudang"
            'dgvListSaleReturn.Columns("NO_OF_ITEM").HeaderText = "Jumlah Barang"
            'dgvListSaleReturn.Columns("DESCRIPTION").HeaderText = "Keterangan"
            'dgvListSaleReturn.Columns("QUANTITY").HeaderText = "Jumlah"
            lblNoTrans.Text = "Jumlah Transaksi : " & dgvListSaleReturn.RowCount
        Else
            lblNoTrans.Text = "No of Transaction(s) : " & dgvListSaleReturn.RowCount
        End If

        txtInvoice.Focus()
    End Sub

    Private Sub txtInvoice_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtInvoice.KeyUp
        If e.KeyCode = Keys.Enter Then
            dgvListSaleReturn.Focus()
        End If
    End Sub

    Private Sub txtInvoice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInvoice.TextChanged
        Try
            'SP_LIST_SALE_RETURNBindingSource.Filter = "RECEIPT_NO LIKE '%" & txtInvoice.Text.ToUpper & "%'"
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = " DECLARE	@return_value int" & _
                                "  EXEC	@return_value = [dbo].[SP_LIST_SALE_RETURN]" & _
                                "  @PERIOD1 = '" & dtpPeriod.Value & "'," & _
                                "  @PERIOD2 = '" & dtpPeriod2.Value & "'," & _
                                "  @RECEIPT_NO = N'" & txtInvoice.Text & "'" & _
                                "  SELECT	'Return Value' = @return_value"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.dgvListSaleReturn.DataSource = dt

            If Language = "Indonesian" Then
                lblNoTrans.Text = "Jumlah Transaksi : " & dgvListSaleReturn.RowCount
            Else
                lblNoTrans.Text = "No of Transaction(s) : " & dgvListSaleReturn.RowCount
            End If
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtInvoice.Text = ""
            txtInvoice.Focus()
        End Try
    End Sub

    Private Sub dgvListSaleReturn_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListSaleReturn.CellDoubleClick
        Try

            With frmEntryReturnSale
                .txtReturnNo.Text = dgvListSaleReturn.Item("RETURN_NO", e.RowIndex).Value
                .dtpReturnDate.Text = Format(dgvListSaleReturn.Item("RETURN_DATE", e.RowIndex).Value, "dd-MMM-yyy")
                If IsDBNull(dgvListSaleReturn.Item("CUSTOMER_NAME", e.RowIndex).Value) Then
                    .txtCustomer.Text = ""
                Else
                    .txtCustomer.Text = dgvListSaleReturn.Item("CUSTOMER_NAME", e.RowIndex).Value
                End If
                .txtReceipNo.Text = dgvListSaleReturn.Item("RECEIPT_NO", e.RowIndex).Value
                .txtPayment.Text = dgvListSaleReturn.Item("PAYMENT_METHOD", e.RowIndex).Value
                .txtSaleDate.Text = Format(dgvListSaleReturn.Item("SALE_DATE", e.RowIndex).Value, "dd-MMM-yyyy")
                .tmpWarehouseName = dgvListSaleReturn.Item("WAREHOUSE_NAME", e.RowIndex).Value
                .cmbReturnType.Text = dgvListSaleReturn.Item("RETURN_TYPE", e.RowIndex).Value
                .getSaleId = dgvListSaleReturn.Item("RECEIPT_ID", e.RowIndex).Value
                .tmpSaveMode = "Update"
                .getQuantity = dgvListSaleReturn.Item("QUANTITY", e.RowIndex).Value
                .getDescription = dgvListSaleReturn.Item("DESCRIPTION", e.RowIndex).Value
                .tmpReusable = dgvListSaleReturn.Item("REUSABLE_STATUS", e.RowIndex).Value
                .tmpNoOfItem = dgvListSaleReturn.Item("NO_OF_ITEM", e.RowIndex).Value
                .tmpSaleReturnID = dgvListSaleReturn.Item("SALE_RETURN_ID", e.RowIndex).Value
            End With
            frmEntryReturnSale.ShowDialog(Me)
            frmEntryReturnSale.Close()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub cmdGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click

        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = " DECLARE	@return_value int" & _
                                "  EXEC	@return_value = [dbo].[SP_LIST_SALE_RETURN]" & _
                                "  @PERIOD1 = '" & dtpPeriod.Value & "'," & _
                                "  @PERIOD2 = '" & dtpPeriod2.Value & "'," & _
                                "  @RECEIPT_NO = N'" & txtInvoice.Text & "'" & _
                                "  SELECT	'Return Value' = @return_value"

        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.dgvListSaleReturn.DataSource = dt

        If Language = "Indonesian" Then
            dgvListSaleReturn.Columns("RETURN_NO").HeaderText = "No Retur"
            dgvListSaleReturn.Columns("RETURN_DATE").HeaderText = "Tanggal Retur"
            dgvListSaleReturn.Columns("RETURN_TYPE").HeaderText = "Tipe Retur"
            dgvListSaleReturn.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            dgvListSaleReturn.Columns("WAREHOUSE_NAME").HeaderText = "Gudang"
            dgvListSaleReturn.Columns("NO_OF_ITEM").HeaderText = "Jumlah Barang"
            dgvListSaleReturn.Columns("DESCRIPTION").HeaderText = "Keterangan"
            dgvListSaleReturn.Columns("QUANTITY").HeaderText = "Jumlah"
            dgvListSaleReturn.Columns("TOTAL").HeaderText = "Total"
            dgvListSaleReturn.Columns("SALE_RETURN_ID").Visible = False
            dgvListSaleReturn.Columns("RECEIPT_ID").Visible = False
            dgvListSaleReturn.Columns("SALE_DATE").Visible = False
            dgvListSaleReturn.Columns("PAYMENT_METHOD").Visible = False
            dgvListSaleReturn.Columns("CUSTOMER_NAME").Visible = False
            dgvListSaleReturn.Columns("WAREHOUSE_ID").Visible = False
            dgvListSaleReturn.Columns("REUSABLE_STATUS").Visible = False
            dgvListSaleReturn.Columns("RETURN_NO").Width = 50
            dgvListSaleReturn.Columns("RETURN_DATE").Width = 80
            dgvListSaleReturn.Columns("RETURN_TYPE").Width = 90
            dgvListSaleReturn.Columns("RECEIPT_NO").Width = 75
            dgvListSaleReturn.Columns("WAREHOUSE_NAME").Width = 110
            dgvListSaleReturn.Columns("NO_OF_ITEM").Width = 65
            dgvListSaleReturn.Columns("DESCRIPTION").Width = 125
            dgvListSaleReturn.Columns("QUANTITY").Width = 50
            dgvListSaleReturn.Columns("TOTAL").Width = 75
            dgvListSaleReturn.Columns("RETURN_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvListSaleReturn.Columns("QUANTITY").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvListSaleReturn.Columns("TOTAL").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvListSaleReturn.Columns("RETURN_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            lblNoTrans.Text = "Jumlah Transaksi : " & dgvListSaleReturn.RowCount
        Else
            dgvListSaleReturn.Columns("RETURN_NO").HeaderText = "Return No"
            dgvListSaleReturn.Columns("RETURN_DATE").HeaderText = "Return Date"
            dgvListSaleReturn.Columns("RETURN_TYPE").HeaderText = "Return Type"
            dgvListSaleReturn.Columns("RECEIPT_NO").HeaderText = "Receipt No"
            dgvListSaleReturn.Columns("WAREHOUSE_NAME").HeaderText = "Warehouse"
            dgvListSaleReturn.Columns("NO_OF_ITEM").HeaderText = "No Of Item"
            dgvListSaleReturn.Columns("DESCRIPTION").HeaderText = "Description"
            dgvListSaleReturn.Columns("QUANTITY").HeaderText = "Qty"
            dgvListSaleReturn.Columns("TOTAL").HeaderText = "Total"
            dgvListSaleReturn.Columns("SALE_RETURN_ID").Visible = False
            dgvListSaleReturn.Columns("RECEIPT_ID").Visible = False
            dgvListSaleReturn.Columns("SALE_DATE").Visible = False
            dgvListSaleReturn.Columns("PAYMENT_METHOD").Visible = False
            dgvListSaleReturn.Columns("CUSTOMER_NAME").Visible = False
            dgvListSaleReturn.Columns("WAREHOUSE_ID").Visible = False
            dgvListSaleReturn.Columns("REUSABLE_STATUS").Visible = False
            dgvListSaleReturn.Columns("RETURN_NO").Width = 50
            dgvListSaleReturn.Columns("RETURN_DATE").Width = 80
            dgvListSaleReturn.Columns("RETURN_TYPE").Width = 90
            dgvListSaleReturn.Columns("RECEIPT_NO").Width = 75
            dgvListSaleReturn.Columns("WAREHOUSE_NAME").Width = 110
            dgvListSaleReturn.Columns("NO_OF_ITEM").Width = 65
            dgvListSaleReturn.Columns("DESCRIPTION").Width = 125
            dgvListSaleReturn.Columns("QUANTITY").Width = 50
            dgvListSaleReturn.Columns("TOTAL").Width = 75
            dgvListSaleReturn.Columns("RETURN_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvListSaleReturn.Columns("QUANTITY").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvListSaleReturn.Columns("TOTAL").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvListSaleReturn.Columns("RETURN_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            lblNoTrans.Text = "No of Transaction(s) : " & dgvListSaleReturn.RowCount
        End If
    End Sub

    'Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
    '    Dim objf As New frmRepReturnSale
    '    objf.date1 = dtpPeriod.Value
    '    objf.date2 = dtpPeriod2.Value
    '    objf.inv = txtInvoice.Text
    '    objf.ReportViewer1.ShowRefreshButton = False
    '    objf.ReportViewer1.ZoomPercent = 100
    '    objf.WindowState = FormWindowState.Maximized
    '    objf.Show()
    'End Sub
End Class