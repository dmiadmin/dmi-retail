﻿Imports System.Data.SqlClient
Imports System.Data
Public Class frmReportSaleByPaymentMethod

    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        ' Me.SP_SALE_REPORT_BY_PAYMENT_METHODTableAdapter.Fill(DS_SALE.SP_SALE_REPORT_BY_PAYMENT_METHOD, dtpPeriod1.Value, dtpPeriod2.Value)
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC	    @return_value = [dbo].[SP_SALE_REPORT_BY_PAYMENT_METHOD]" & vbCrLf & _
              "         @PERIOD1 = N'" & dtpPeriod1.Value & "'," & vbCrLf & _
              "    		@PERIOD2 = N'" & dtpPeriod2.Value & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.dgvRptSaleByPaymentMethod.DataSource = dt
        If Language = "Indonesian" Then
            dgvRptSaleByPaymentMethod.Columns("PAYMENT_METHOD").HeaderText = "Cara Pembayaran"
            dgvRptSaleByPaymentMethod.Columns("NO_OF_INVOICE").HeaderText = "Jumlah Faktur"
            dgvRptSaleByPaymentMethod.Columns("NO_OF_ITEM").HeaderText = "Jumlah Item"
            dgvRptSaleByPaymentMethod.Columns("TOTAL_AMOUNT").HeaderText = "Jumlah Nominal"
            dgvRptSaleByPaymentMethod.Columns("DATE_FROM").Visible = False
            dgvRptSaleByPaymentMethod.Columns("DATE_TO").Visible = False
            dgvRptSaleByPaymentMethod.Columns("NO_OF_INVOICE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptSaleByPaymentMethod.Columns("NO_OF_ITEM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptSaleByPaymentMethod.Columns("TOTAL_AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptSaleByPaymentMethod.Columns("TOTAL_AMOUNT").DefaultCellStyle.Format = "n0"
        Else
            dgvRptSaleByPaymentMethod.Columns("PAYMENT_METHOD").HeaderText = "Payment Method"
            dgvRptSaleByPaymentMethod.Columns("NO_OF_INVOICE").HeaderText = "No Of Invoice"
            dgvRptSaleByPaymentMethod.Columns("NO_OF_ITEM").HeaderText = "No Of Item"
            dgvRptSaleByPaymentMethod.Columns("TOTAL_AMOUNT").HeaderText = "Total Amount"
            dgvRptSaleByPaymentMethod.Columns("DATE_FROM").Visible = False
            dgvRptSaleByPaymentMethod.Columns("DATE_TO").Visible = False
            dgvRptSaleByPaymentMethod.Columns("NO_OF_INVOICE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptSaleByPaymentMethod.Columns("NO_OF_ITEM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptSaleByPaymentMethod.Columns("TOTAL_AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptSaleByPaymentMethod.Columns("TOTAL_AMOUNT").DefaultCellStyle.Format = "n0"
        End If
    End Sub


    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        Dim objf As New frmRepSaleByPaymentMethodvb
        objf.date1 = dtpPeriod1.Value
        objf.date2 = dtpPeriod2.Value
        objf.ReportViewer1.ShowRefreshButton = False
        objf.ReportViewer1.ZoomPercent = 100
        objf.WindowState = FormWindowState.Maximized
        objf.Show()
    End Sub

    Private Sub frmReportSaleByPaymentMethod_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dtpPeriod1.Value = DateSerial(Today.Year, Today.Month, 1)
        dtpPeriod2.Value = DateSerial(Today.Year, Today.Month + 1, 0)

        If Language = "Indonesian" Then
            cmdGenerate.Text = "Proses"
            cmdReport.Text = "Cetak"
            lblFrom.Text = "Periode"

            'dgvRptSaleByPaymentMethod.Columns("PAYMENT_METHOD").HeaderText = "Cara Pembayaran"
            'dgvRptSaleByPaymentMethod.Columns("NO_OF_INVOICE").HeaderText = "Jumlah Faktur"
            'dgvRptSaleByPaymentMethod.Columns("NO_OF_ITEM").HeaderText = "Jumlah Item"
            'dgvRptSaleByPaymentMethod.Columns("TOTAL_AMOUNT").HeaderText = "Jumlah Nominal"
        End If
    End Sub
End Class