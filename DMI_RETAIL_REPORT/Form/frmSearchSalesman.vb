﻿Public Class frmSearchSalesman

    Private Sub SALESMANBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.SALESMANBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.DS_SALE)

    End Sub

    Private Sub frmSearchSalesman_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_SALE.SALESMAN' table. You can move, or remove it, as needed.
        Me.SALESMANTableAdapter.Fill(Me.DS_SALE.SALESMAN)
        tmpSearchResult = ""
        txtSalesman.Text = ""
        txtSalesman.Focus()

        If tmpSearchMode = "Salesman Name" Then
            If Language = "Indonesian" Then
                lblSalesman.Text = "Nama Salesman"
            Else
                lblSalesman.Text = "Salesman Name"
            End If
        ElseIf tmpSearchMode = "Salesman Code" Then
            If Language = "Indonesian" Then
                lblSalesman.Text = "Kode Salesman"
            Else
                lblSalesman.Text = "Salesman Code"
            End If
        End If

        If Language = "Indonesian" Then
            Me.Text = "Pencarian Salesman"
            dgvSearchSalesman.Columns("SALESMAN_CODE").HeaderText = "Kode Salesman"
            dgvSearchSalesman.Columns("SALESMAN_NAME").HeaderText = "Nama Salesman"
            dgvSearchSalesman.Columns("SALESMAN_ADDRESS").HeaderText = "Alamat"
            dgvSearchSalesman.Columns("SALESMAN_ADDRESS_CITY").HeaderText = "Kota"
            dgvSearchSalesman.Columns("SALESMAN_ADDRESS_STATE").HeaderText = "Provinsi"
            dgvSearchSalesman.Columns("SALESMAN_PHONE1").HeaderText = "Telp. 1"
            dgvSearchSalesman.Columns("SALESMAN_PHONE2").HeaderText = "Telp. 2"
            dgvSearchSalesman.Columns("SALESMAN_PHONE3").HeaderText = "Telp. 3"
        End If
    End Sub

    Private Sub txtSalesman_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSalesman.TextChanged
        Try
            If tmpSearchMode = "Salesman Name" Then
                SALESMANBindingSource.Filter = "SALESMAN_NAME LIKE '%" & txtSalesman.Text.ToUpper & "%'"
            ElseIf tmpSearchMode = "Salesman Code" Then
                SALESMANBindingSource.Filter = "SALESMAN_CODE LIKE '%" & txtSalesman.Text.ToUpper & "%'"
            End If
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtSalesman.Text = ""
            txtSalesman.Focus()
        End Try
        txtSalesman.Focus()
    End Sub

    Private Sub dgvSearchSalesman_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSearchSalesman.DoubleClick
        Try
            tmpSearchResult = SALESMANBindingSource.Current("SALESMAN_ID")
        Catch
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data Salesman !" & vbCrLf & _
                       "Silahkan input setidaknya satu data Salesman !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Salesman data !" & vbCrLf & _
                       "Please input at least one Salesman data !", MsgBoxStyle.Critical, "DMI Retail")
            End If
        End Try
        Me.Close()
    End Sub

    Private Sub dgvSearchSalesman_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvSearchSalesman.KeyDown

        If e.KeyCode = Keys.Enter Then
            Try
                tmpSearchResult = SALESMANBindingSource.Current("SALESMAN_ID")
            Catch
                If Language = "Indonesian" Then
                    MsgBox("Tidak ada data Salesman !" & vbCrLf & _
                           "Silahkan input setidaknya satu data Salesman !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("There is no Salesman data !" & vbCrLf & _
                           "Please input at least one Salesman data !", MsgBoxStyle.Critical, "DMI Retail")
                End If
            End Try
            Me.Close()

        ElseIf e.KeyCode = Keys.Escape Then
            tmpSearchResult = ""
            Me.Close()
        End If
        
    End Sub
End Class