﻿Public Class frmListPurchasePrice

    Public TMPProdIDList As Integer
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        If SP_LIST_PRODUCT_GOODSBindingSource.Count = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data Produk." & vbCrLf & "Form ini akan ditutup !", _
                       MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Product." & vbCrLf & "This form will be closed !", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Me.Close()
            Exit Sub
        End If


        If PRODUCT_CODEComboBox.Text = "" Or PRODUCT_NAMETextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Masukkan Produk !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please Input the Product !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        Me.SP_LIST_PURCHASE_PRICETableAdapter.Fill(Me.DS_PURCHASE.SP_LIST_PURCHASE_PRICE, _
                                                   SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID"), _
                                                   dtpPeriod1.Value, dtpPeriod2.Value)

        If Language = "Indonesian" Then
            Label1.Text = "Jumlah Transaksi = " & dgvPurchasePrice.RowCount
        Else
            Label1.Text = "No of Transaction(s) = " & dgvPurchasePrice.RowCount
        End If
    End Sub

    Private Sub frmListPurchasePrice_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F12 Then
            cmdReport_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub frmListPurchasePrice_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SP_LIST_PRODUCT_GOODSTableAdapter.Fill(Me.DS_PURCHASE.SP_LIST_PRODUCT_GOODS, Now)
        PRODUCT_CODEComboBox.Text = ""

        If SP_LIST_PRODUCT_GOODSBindingSource.Count = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Tidak ada Data Produk." & vbCrLf & "Masukkan setidaknya satu data Produk !", MsgBoxStyle.Critical, _
                       "DMI Retail")
            Else
                MsgBox("There is no Product." & vbCrLf & "Please input at least one Product !", MsgBoxStyle.Critical, _
                       "DMI Retail")
            End If
        Else
            SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID") = 0
        End If


        dtpPeriod1.Value = DateSerial(Today.Year, Today.Month, 1)
        dtpPeriod2.Value = DateSerial(Today.Year, Today.Month + 1, 0)

        If Language = "Indonesian" Then
            Me.Text = "Daftar Harga Pembelian"

            lblProduct.Text = "Produk"
            lblFrom.Text = "Periode"
            Label1.Text = "Jumlah Transaksi = " & dgvPurchasePrice.RowCount
            cmdGenerate.Text = "Proses"
            cmdReport.Text = "Cetak"

            dgvPurchasePrice.Columns("TRANSACTION_DATE").HeaderText = "Tgl. Transaksi"
            dgvPurchasePrice.Columns("TRANSACTION_NO").HeaderText = "No Transaksi"
            dgvPurchasePrice.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            dgvPurchasePrice.Columns("PRICE_PER_UNIT").HeaderText = "Harga Satuan"
            dgvPurchasePrice.Columns("WAREHOUSE_NAME").HeaderText = "Gudang"
            dgvPurchasePrice.Columns("TRANSACTION_NAME").HeaderText = "Nama Transaksi"

        Else
            Label1.Text = "No of Transaction(s) = " & dgvPurchasePrice.RowCount
        End If
    End Sub

    Private Sub cmdSearchName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchName.Click
        mdlGeneral.tmpSearchMode = "PURCHASE - Product Name"
        frmSearchProduct.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        SP_LIST_PRODUCT_GOODSBindingSource.Position = SP_LIST_PRODUCT_GOODSBindingSource.Find("PRODUCT_ID", tmpSearchResult)
        PRODUCT_CODEComboBox.Text = SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_CODE")
    End Sub

    Private Sub PRODUCT_CODEComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PRODUCT_CODEComboBox.TextChanged
        SP_LIST_PRODUCT_GOODSTableAdapter.SP_GET_PRODUCT_NAME(PRODUCT_CODEComboBox.Text, PRODUCT_NAMETextBox.Text)

    End Sub


    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        If SP_LIST_PRODUCT_GOODSBindingSource.Count = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data Produk." & vbCrLf & "Form ini akan ditutup !", _
                       MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Product." & vbCrLf & "This form will be closed !", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Me.Close()
            Exit Sub
        End If

        Dim RPV As New frmRepPurchasingPrice
        RPV.date1 = dtpPeriod1.Value
        RPV.date2 = dtpPeriod2.Value
        RPV.TMPProdIDList = TMPProdIDList
        RPV.ReportViewer1.ZoomPercent = 100
        RPV.WindowState = FormWindowState.Maximized
        RPV.Show()
    End Sub

    
    Private Sub dtpPeriod1_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpPeriod1.ValueChanged
        Me.SP_LIST_PURCHASE_PRICETableAdapter.Fill(Me.DS_PURCHASE.SP_LIST_PURCHASE_PRICE, _
                                                   0, dtpPeriod1.Value, dtpPeriod2.Value)
    End Sub

    Private Sub dtpPeriod2_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpPeriod2.ValueChanged
        Me.SP_LIST_PURCHASE_PRICETableAdapter.Fill(Me.DS_PURCHASE.SP_LIST_PURCHASE_PRICE, _
                                                    0, dtpPeriod1.Value, dtpPeriod2.Value)
    End Sub

    Private Sub SP_LIST_PRODUCT_GOODSBindingSource_CurrentChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SP_LIST_PRODUCT_GOODSBindingSource.CurrentChanged
        TMPProdIDList = SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID")
    End Sub

   
End Class