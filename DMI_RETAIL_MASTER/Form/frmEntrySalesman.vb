﻿Imports System.Data.SqlClient

Public Class frmEntrySalesman
    Dim tmpChange, tmpNoData As Boolean
    Dim tmpSaveMode, tmpSalesmanCode, tmpSalesmanName, tmpCode, tmpEditCode As String
    Dim tmpUsedSalesman, tmpUsedCode, tmpBindPosition As Integer
    Dim tmpEndDate As DateTime
    Public SALESMANBindingSource As New BindingSource

    Private Sub GetData()
        sql = "SELECT SALESMAN_ID, SALESMAN_NAME, SALESMAN_ADDRESS, SALESMAN_ADDRESS_CITY, " & _
              "SALESMAN_ADDRESS_STATE, SALESMAN_PHONE1, SALESMAN_PHONE2, " & _
              "SALESMAN_PHONE3, SALESMAN_CODE, EFFECTIVE_END_DATE, EFFECTIVE_START_DATE, " & _
              "INPUT_DATE, UPDATE_DATE, USER_ID_INPUT, USER_ID_UPDATE FROM SALESMAN " & _
              "ORDER BY SALESMAN_NAME"
        dt = New DataTable
        sqladapter = New SqlDataAdapter(sql, xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        SALESMANBindingSource.DataSource = dt
        SALESMANBindingNavigator.BindingSource = SALESMANBindingSource

    End Sub

    Private Sub frmEntrySalesman_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If tmpChange = True And SALESMAN_NAMETextBox.Enabled = True Then
            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah Anda ingin menyimpan perubahan " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If
            If tmpvar = 6 Then
                cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        End If

    End Sub

    Private Sub frmEntrySalesman_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_SALE.SALESMAN' table. You can move, or remove it, as needed.
        'Me.SALESMANTableAdapter.Fill(Me.DS_SALE.SALESMAN)

        accFormName = Me.Text
        GetData()
        SALESMAN_CODETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", SALESMANBindingSource, "SALESMAN_CODE", True))
        SALESMAN_NAMETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", SALESMANBindingSource, "SALESMAN_NAME", True))
        SALESMAN_ADDRESSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", SALESMANBindingSource, "SALESMAN_ADDRESS", True))
        SALESMAN_ADDRESS_CITYTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", SALESMANBindingSource, "SALESMAN_ADDRESS_CITY", True))
        SALESMAN_ADDRESS_STATETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", SALESMANBindingSource, "SALESMAN_ADDRESS_STATE", True))
        SALESMAN_PHONE1TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", SALESMANBindingSource, "SALESMAN_PHONE1", True))
        SALESMAN_PHONE2TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", SALESMANBindingSource, "SALESMAN_PHONE2", True))
        SALESMAN_PHONE3TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", SALESMANBindingSource, "SALESMAN_PHONE3", True))

        DisableInputBox(Me)

        If SALESMAN_NAMETextBox.Text <> "" Then
            If SALESMANBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        End If

        cmdSearchCode.Visible = True
        cmdSearchName.Visible = True
        SALESMANBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        'cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        'cmdDelete.Enabled = True
        tmpChange = False
        chkActive.Enabled = False

        If Language = "Indonesian" Then
            Me.Text = "Input Sales"

            cmdAdd.Text = "Tambah"
            cmdEdit.Text = "Ubah"
            cmdDelete.Text = "Hapus"
            cmdSave.Text = "Simpan"
            cmdUndo.Text = "Batal"

            lblSalesmanCode.Text = "Kode Sales"
            lblSalesmanName.Text = "Nama Sales"
            lblSalesmanAddress.Text = "Alamat"
            lblSalesmanAddressCity.Text = "Kota"
            lblSalesmanAddressState.Text = "Propinsi"
            lblSalesmanPhone1.Text = "No Telp. 1"
            lblSalesmanPhone2.Text = "No Telp. 2"
            lblSalesmanPhone3.Text = "No Telp. 3"
        End If
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If AllowAdd = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Insert"
        tmpBindPosition = SALESMANBindingSource.Position
        EnableInputBox(Me)
        SALESMANBindingSource.AddNew()
        xComm = New SqlCommand("SP_GENERATE_MASTER_CODE", xConn)
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@DOCUMENT_TYPE", "SALESMAN")
        xComm.Parameters.Add("@RECEIPT_NO", SqlDbType.VarChar, 15).Direction = ParameterDirection.Output
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xComm.ExecuteNonQuery()
        SALESMAN_CODETextBox.Text = xComm.Parameters.Item("@RECEIPT_NO").Value
        'SALESMANTableAdapter.SP_GENERATE_MASTER_CODE("SALESMAN", SALESMAN_CODETextBox.Text)

        tmpEditCode = SALESMAN_CODETextBox.Text
        chkActive.Enabled = True
        chkActive.Checked = True
        cmdSearchCode.Visible = False
        cmdSearchName.Visible = False
        SALESMANBindingNavigator.Enabled = False

        cmdAdd.Enabled = False
        cmdDelete.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True

        tmpChange = False

    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        If SALESMAN_CODETextBox.Text = "" Then Exit Sub
        If AllowEdit = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If SALESMANBindingSource.Current("SALESMAN_ID") <> 0 Then
            SALESMANBindingSource.Position = _
                SALESMANBindingSource.Find("SALESMAN_ID", SALESMANBindingSource.Current("SALESMAN_ID"))
        End If

        tmpSaveMode = "Update"

        EnableInputBox(Me)

        chkActive.Enabled = True
        cmdSearchCode.Visible = False
        cmdSearchName.Visible = False
        SALESMANBindingNavigator.Enabled = False

        cmdAdd.Enabled = False
        cmdDelete.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True

        tmpSalesmanCode = SALESMAN_CODETextBox.Text
        tmpSalesmanName = SALESMAN_NAMETextBox.Text

        tmpChange = False
        tmpBindPosition = SALESMANBindingSource.Position

        If SALESMANBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
            chkActive.Checked = False
        Else
            chkActive.Checked = True
        End If
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        tmpSaveMode = ""

        DisableInputBox(Me)
        SALESMANBindingSource.CancelEdit()

        Try
            If SALESMANBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        Catch
        End Try

        cmdSearchCode.Visible = True
        cmdSearchName.Visible = True
        SALESMANBindingNavigator.Enabled = True

        cmdAdd.Enabled = True
        cmdSave.Enabled = False
        cmdUndo.Enabled = False
        chkActive.Enabled = False

        If SALESMAN_CODETextBox.Text = "" Then
            cmdEdit.Enabled = False
            cmdDelete.Enabled = False
        Else
            cmdEdit.Enabled = True
            cmdDelete.Enabled = True
        End If

        tmpChange = False

        'SALESMANBindingSource.Position = tmpBindPosition
        SALESMANBindingSource_CurrentChanged(Nothing, Nothing)
    End Sub

    Private Sub SALESMANBindingSource_CurrentChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If SALESMAN_CODETextBox.Text <> "" Then
            If SALESMANBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        End If
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If SALESMAN_CODETextBox.Text = "" Then Exit Sub

        If AllowDelete = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If Language = "Indonesian" Then
            If MsgBox("Hapus Sales ?" & vbCrLf & SALESMAN_NAMETextBox.Text, _
              MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
        Else
            If MsgBox("Delete Salesman ?" & vbCrLf & SALESMAN_NAMETextBox.Text, _
                      MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
        End If

        xComm = New SqlCommand("SP_CHECK_SALESMAN_USED", xConn)
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@SALESMAN_NAME", SALESMAN_NAMETextBox.Text)
        xComm.Parameters.Add("@RESULT", SqlDbType.Int).Direction = ParameterDirection.Output
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xComm.ExecuteNonQuery()
        tmpUsedSalesman = xComm.Parameters("@RESULT").Value

        If Language = "Indonesian" Then
            If tmpUsedSalesman > 0 Then
                MsgBox("Tidak dapat menghapus Sales ini!" & vbCrLf & "Sales telah melakukan transaksi!" & vbCrLf & _
                       "Anda tidak dapat menghapus data ini!", _
                      MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If
        Else
            If tmpUsedSalesman > 0 Then
                MsgBox("Cannot delete this Salesman!" & vbCrLf & _
                       "Salesman has doing the transaction(s)!" & vbCrLf & _
                       "You can not delete this data!", MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If
        End If

        xComm = New SqlCommand("SP_SALESMAN", xConn)
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@METHOD", "D")
        xComm.Parameters.AddWithValue("@SALESMAN_ID", SALESMANBindingSource.Current("SALESMAN_ID"))
        xComm.Parameters.AddWithValue("@SALESMAN_CODE", SALESMANBindingSource.Current("SALESMAN_CODE"))
        xComm.Parameters.AddWithValue("@SALESMAN_NAME", SALESMANBindingSource.Current("SALESMAN_NAME"))
        xComm.Parameters.AddWithValue("@SALESMAN_ADDRESS", SALESMANBindingSource.Current("SALESMAN_ADDRESS"))
        xComm.Parameters.AddWithValue("@SALESMAN_ADDRESS_CITY", SALESMANBindingSource.Current("SALESMAN_ADDRESS_CITY"))
        xComm.Parameters.AddWithValue("@SALESMAN_ADDRESS_STATE", SALESMANBindingSource.Current("SALESMAN_ADDRESS_STATE"))
        xComm.Parameters.AddWithValue("@SALESMAN_PHONE1", SALESMANBindingSource.Current("SALESMAN_PHONE1"))
        xComm.Parameters.AddWithValue("@SALESMAN_PHONE2", SALESMANBindingSource.Current("SALESMAN_PHONE2"))
        xComm.Parameters.AddWithValue("@SALESMAN_PHONE3", SALESMANBindingSource.Current("SALESMAN_PHONE3"))
        xComm.Parameters.AddWithValue("EFFECTIVE_START_DATE", SALESMANBindingSource.Current("EFFECTIVE_START_DATE"))
        xComm.Parameters.AddWithValue("EFFECTIVE_END_DATE", Now)
        xComm.Parameters.AddWithValue("USER_ID_INPUT", SALESMANBindingSource.Current("USER_ID_INPUT"))
        xComm.Parameters.AddWithValue("INPUT_DATE", SALESMANBindingSource.Current("INPUT_DATE"))
        xComm.Parameters.AddWithValue("USER_ID_UPDATE", SALESMANBindingSource.Current("USER_ID_UPDATE"))
        xComm.Parameters.AddWithValue("UPDATE_DATE", SALESMANBindingSource.Current("UPDATE_DATE"))
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xComm.ExecuteNonQuery()

        GetData()
        'Me.SALESMANTableAdapter.Fill(Me.DS_SALE.SALESMAN)

        Try
            If SALESMANBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        Catch
        End Try

        chkActive.Enabled = False
        tmpChange = False

    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        If SALESMAN_NAMETextBox.Text = "" Or SALESMAN_CODETextBox.Text = "" Or SALESMAN_ADDRESSTextBox.Text = "" _
            Or SALESMAN_PHONE1TextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please fill all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpCode = SALESMAN_CODETextBox.Text

        If tmpSaveMode = "Insert" Then
            xComm = New SqlCommand("SP_CHECK_SALESMAN_CODE", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@SALESMAN_CODE", SALESMAN_CODETextBox.Text)
            xComm.Parameters.Add("@RESULT", SqlDbType.Int).Direction = ParameterDirection.Output
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()
            tmpUsedCode = xComm.Parameters("@RESULT").Value

            If tmpUsedCode > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Kode Sales sudah ada di database." & vbCrLf & _
                           "Tolong isi Kode yang berbeda !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Code already exist in Database." & vbCrLf & _
                           "Please enter another Code!", MsgBoxStyle.Critical, "DMI Retail")
                    Exit Sub
                End If
            End If
            xComm = New SqlCommand("SP_SALESMAN", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "I")
            xComm.Parameters.AddWithValue("@SALESMAN_ID", 0)
            xComm.Parameters.AddWithValue("@SALESMAN_CODE", SALESMAN_CODETextBox.Text)
            xComm.Parameters.AddWithValue("@SALESMAN_NAME", SALESMAN_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@SALESMAN_ADDRESS", SALESMAN_ADDRESSTextBox.Text)
            xComm.Parameters.AddWithValue("@SALESMAN_ADDRESS_CITY", SALESMAN_ADDRESS_CITYTextBox.Text)
            xComm.Parameters.AddWithValue("@SALESMAN_ADDRESS_STATE", SALESMAN_ADDRESS_STATETextBox.Text)
            xComm.Parameters.AddWithValue("@SALESMAN_PHONE1", SALESMAN_PHONE1TextBox.Text)
            xComm.Parameters.AddWithValue("@SALESMAN_PHONE2", SALESMAN_PHONE2TextBox.Text)
            xComm.Parameters.AddWithValue("@SALESMAN_PHONE3", SALESMAN_PHONE3TextBox.Text)
            xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", Now)
            xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", IIf(chkActive.Checked, DateSerial(4000, 12, 31), Now))
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", UserId)
            xComm.Parameters.AddWithValue("@INPUT_DATE", Now)
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", 0)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", DateSerial(4000, 12, 31))
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()

        ElseIf tmpSaveMode = "Update" Then
            If SALESMAN_CODETextBox.Text <> tmpSalesmanCode Then
                xComm = New SqlCommand("SP_CHECK_SALESMAN_CODE", xConn)
                xComm.CommandType = CommandType.StoredProcedure
                xComm.Parameters.AddWithValue("@SALESMAN_CODE", SALESMAN_CODETextBox.Text)
                xComm.Parameters.Add("@RESULT", SqlDbType.Int).Direction = ParameterDirection.Output
                If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                xComm.ExecuteNonQuery()
                tmpUsedCode = xComm.Parameters("@RESULT").Value
                ' SALESMANTableAdapter.SP_CHECK_SALESMAN_CODE(SALESMAN_CODETextBox.Text, tmpUsedCode)
                If tmpUsedCode > 0 Then
                    If Language = "Indonesian" Then
                        MsgBox("Kode Sales atau Nama Sales sudah ada di database." & vbCrLf & _
                                "Tolong isi Kode atau Nama yang berbeda !", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("Either Code or Name already exist in Database." & vbCrLf & _
                               "Please enter another Code or Name!", MsgBoxStyle.Critical, "DMI Retail")
                        Exit Sub
                    End If
                End If
            End If

            If SALESMAN_CODETextBox.Text.ToUpper <> tmpSalesmanCode.ToUpper Then
                xComm = New SqlCommand("SP_CHECK_SALESMAN_CODE", xConn)
                xComm.CommandType = CommandType.StoredProcedure
                xComm.Parameters.AddWithValue("@SALESMAN_CODE", SALESMAN_CODETextBox.Text)
                xComm.Parameters.Add("@RESULT", SqlDbType.Int).Direction = ParameterDirection.Output
                If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                xComm.ExecuteNonQuery()
                tmpUsedCode = xComm.Parameters("@RESULT").Value
            Else
                tmpUsedCode = 0
            End If

            If tmpUsedCode > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Kode Sales sudah ada di database !" & vbCrLf & _
                           "Tolong isi Kode yang berbeda !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Code already exist in Database." & vbCrLf & _
                           "Please enter another Code!", MsgBoxStyle.Critical, "DMI Retail")
                    Exit Sub
                End If
            End If

            If chkActive.Checked = True Then
                tmpEndDate = DateSerial(4000, 12, 31)
            Else
                tmpEndDate = Now
            End If
            xComm = New SqlCommand("SP_SALESMAN", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "U")
            xComm.Parameters.AddWithValue("@SALESMAN_ID", SALESMANBindingSource.Current("SALESMAN_ID"))
            xComm.Parameters.AddWithValue("@SALESMAN_CODE", SALESMAN_CODETextBox.Text)
            xComm.Parameters.AddWithValue("@SALESMAN_NAME", SALESMAN_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@SALESMAN_ADDRESS", SALESMAN_ADDRESSTextBox.Text)
            xComm.Parameters.AddWithValue("@SALESMAN_ADDRESS_CITY", SALESMAN_ADDRESS_CITYTextBox.Text)
            xComm.Parameters.AddWithValue("@SALESMAN_ADDRESS_STATE", SALESMAN_ADDRESS_STATETextBox.Text)
            xComm.Parameters.AddWithValue("@SALESMAN_PHONE1", SALESMAN_PHONE1TextBox.Text)
            xComm.Parameters.AddWithValue("@SALESMAN_PHONE2", SALESMAN_PHONE2TextBox.Text)
            xComm.Parameters.AddWithValue("@SALESMAN_PHONE3", SALESMAN_PHONE3TextBox.Text)
            xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", Now)
            xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", tmpEndDate)
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", SALESMANBindingSource.Current("USER_ID_INPUT"))
            xComm.Parameters.AddWithValue("@INPUT_DATE", SALESMANBindingSource.Current("INPUT_DATE"))
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", UserId)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", Now)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()
        End If
        getdata()
        'Me.SALESMANTableAdapter.Fill(Me.DS_SALE.SALESMAN)
        SALESMANBindingSource.Position = SALESMANBindingSource.Find("SALESMAN_CODE", tmpCode)

        tmpSaveMode = ""

        DisableInputBox(Me)
        SALESMANBindingSource.CancelEdit()

        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdDelete.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        chkActive.Enabled = False

        cmdSearchCode.Visible = True
        cmdSearchName.Visible = True
        SALESMANBindingNavigator.Enabled = True
        SALESMANBindingSource_CurrentChanged(Nothing, Nothing)

        If Language = "Indonesian" Then
            MsgBox("Data berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
        End If

    End Sub

    Private Sub SALESMAN_CODETextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles SALESMAN_CODETextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            SALESMAN_NAMETextBox.Focus()
        End If
    End Sub

    Private Sub SALESMAN_NAMETextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles SALESMAN_NAMETextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            SALESMAN_ADDRESSTextBox.Focus()
        End If
    End Sub

    Private Sub frmEntrySalesman_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
        If tmpNoData Then Me.Close()
    End Sub

    Private Sub SALESMAN_ADDRESS_CITYTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SALESMAN_ADDRESS_CITYTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub SALESMAN_ADDRESS_STATETextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SALESMAN_ADDRESS_STATETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub SALESMAN_ADDRESSTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SALESMAN_ADDRESSTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub SALESMAN_CODETextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SALESMAN_CODETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub SALESMAN_NAMETextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SALESMAN_NAMETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub SALESMAN_PHONE1TextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SALESMAN_PHONE1TextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub SALESMAN_PHONE2TextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SALESMAN_PHONE2TextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub SALESMAN_PHONE3TextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SALESMAN_PHONE3TextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub cmdSearchCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchCode.Click
        If SALESMANBindingSource.Count = 0 Then Exit Sub
        tmpSearchMode = "Salesman Code"
        frmSearchSalesman.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        SALESMANBindingSource.Position = SALESMANBindingSource.Find("SALESMAN_ID", tmpSearchResult)
        tmpEditCode = SALESMAN_CODETextBox.Text
    End Sub

    Private Sub cmdSearchName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchName.Click
        If SALESMANBindingSource.Count = 0 Then Exit Sub
        tmpSearchMode = "Salesman Name"
        frmSearchSalesman.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        SALESMANBindingSource.Position = SALESMANBindingSource.Find("SALESMAN_ID", tmpSearchResult)
        tmpEditCode = SALESMAN_CODETextBox.Text
    End Sub

    Private Sub BindingNavigatorPositionItem_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BindingNavigatorPositionItem.TextChanged
        Try
            If SALESMAN_CODETextBox.Text <> "" Then
                If SALESMANBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                    chkActive.Checked = False
                Else
                    chkActive.Checked = True
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        AccessPrivilege(Me.Tag)
    End Sub
End Class