﻿Imports System.Data.SqlClient
Imports System.Data
Public Class frmReportSaleByStatus

    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
   

    Private Sub frmReportSaleByStatus_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dtpPeriod1.Value = DateSerial(Today.Year, Today.Month, 1)
        dtpPeriod2.Value = DateSerial(Today.Year, Today.Month + 1, 0)

        If Language = "Indonesian" Then
            cmdGenerate.Text = "Proses"
            cmdReport.Text = "Cetak"
            lblFrom.Text = "Periode"

            'dgvRptSaleByStatus.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            'dgvRptSaleByStatus.Columns("SALE_DATE").HeaderText = "Tgl. Penjualan"
            'dgvRptSaleByStatus.Columns("CUSTOMER").HeaderText = "Pelanggan"
            'dgvRptSaleByStatus.Columns("PAYMENT_METHOD").HeaderText = "Cara Pembayaran"
            'dgvRptSaleByStatus.Columns("WAREHOUSE").HeaderText = "Gudang"
            'dgvRptSaleByStatus.Columns("SALESMAN").HeaderText = "Nama Sales"
            'dgvRptSaleByStatus.Columns("NUMBER_OF_ITEM").HeaderText = "Jumlah Item"
            'dgvRptSaleByStatus.Columns("DOWN_PAYMENT").HeaderText = "Uang Muka"

        End If
    End Sub


    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        'Me.SP_SALE_REPORT_BY_STATUSTableAdapter.Fill(Me.DS_SALE.SP_SALE_REPORT_BY_STATUS, dtpPeriod1.Value, dtpPeriod2.Value)
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC	    @return_value = [dbo].[SP_SALE_REPORT_BY_STATUS]" & vbCrLf & _
              "         @PERIOD1 = N'" & dtpPeriod1.Value & "'," & vbCrLf & _
              "    		@PERIOD2 = N'" & dtpPeriod2.Value & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.dgvRptSaleByStatus.DataSource = dt
        If Language = "Indonesian" Then
            dgvRptSaleByStatus.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            dgvRptSaleByStatus.Columns("SALE_DATE").HeaderText = "Tgl. Penjualan"
            dgvRptSaleByStatus.Columns("CUSTOMER").HeaderText = "Pelanggan"
            dgvRptSaleByStatus.Columns("PAYMENT_METHOD").HeaderText = "Cara Pembayaran"
            dgvRptSaleByStatus.Columns("WAREHOUSE").HeaderText = "Gudang"
            dgvRptSaleByStatus.Columns("SALESMAN").HeaderText = "Nama Sales"
            dgvRptSaleByStatus.Columns("NUMBER_OF_ITEM").HeaderText = "Jumlah Item"
            dgvRptSaleByStatus.Columns("DOWN_PAYMENT").HeaderText = "Uang Muka"
            dgvRptSaleByStatus.Columns("USER_UPDATE").HeaderText = "User Update"
            dgvRptSaleByStatus.Columns("GRAND_TOTAL").HeaderText = "Total"
            dgvRptSaleByStatus.Columns("RECEIPT_NO").HeaderText = "Receipt No"
            dgvRptSaleByStatus.Columns("SALE_DATE").HeaderText = "Sale Date"
            dgvRptSaleByStatus.Columns("CUSTOMER").HeaderText = "Customer"
            dgvRptSaleByStatus.Columns("PAYMENT_METHOD").HeaderText = "Payment Method"
            dgvRptSaleByStatus.Columns("WAREHOUSE").HeaderText = "Warehouse"
            dgvRptSaleByStatus.Columns("SALESMAN").HeaderText = "Saleman"
            dgvRptSaleByStatus.Columns("NUMBER_OF_ITEM").HeaderText = "No Of Item"
            dgvRptSaleByStatus.Columns("DOWN_PAYMENT").HeaderText = "Dp"
            dgvRptSaleByStatus.Columns("USER_UPDATE").HeaderText = "User Update"
            dgvRptSaleByStatus.Columns("GRAND_TOTAL").HeaderText = "Total"
            dgvRptSaleByStatus.Columns("RECEIPT_NO").Width = 75
            dgvRptSaleByStatus.Columns("SALE_DATE").Width = 80
            dgvRptSaleByStatus.Columns("CUSTOMER").Width = 90
            dgvRptSaleByStatus.Columns("PAYMENT_METHOD").Width = 80
            dgvRptSaleByStatus.Columns("WAREHOUSE").Width = 80
            dgvRptSaleByStatus.Columns("SALESMAN").Width = 75
            dgvRptSaleByStatus.Columns("NUMBER_OF_ITEM").Width = 50
            dgvRptSaleByStatus.Columns("DOWN_PAYMENT").Width = 75
            dgvRptSaleByStatus.Columns("USER_UPDATE").Width = 75
            dgvRptSaleByStatus.Columns("SALE_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptSaleByStatus.Columns("NUMBER_OF_ITEM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptSaleByStatus.Columns("DOWN_PAYMENT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptSaleByStatus.Columns("GRAND_TOTAL").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptSaleByStatus.Columns("SALE_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvRptSaleByStatus.Columns("GRAND_TOTAL").DefaultCellStyle.Format = "n0"
            dgvRptSaleByStatus.Columns("DATE_FROM").Visible = False
            dgvRptSaleByStatus.Columns("DATE_TO").Visible = False
        Else
            dgvRptSaleByStatus.Columns("RECEIPT_NO").HeaderText = "Receipt No"
            dgvRptSaleByStatus.Columns("SALE_DATE").HeaderText = "Sale Date"
            dgvRptSaleByStatus.Columns("CUSTOMER").HeaderText = "Customer"
            dgvRptSaleByStatus.Columns("PAYMENT_METHOD").HeaderText = "Payment Method"
            dgvRptSaleByStatus.Columns("WAREHOUSE").HeaderText = "Warehouse"
            dgvRptSaleByStatus.Columns("SALESMAN").HeaderText = "Saleman"
            dgvRptSaleByStatus.Columns("NUMBER_OF_ITEM").HeaderText = "No Of Item"
            dgvRptSaleByStatus.Columns("DOWN_PAYMENT").HeaderText = "Dp"
            dgvRptSaleByStatus.Columns("USER_UPDATE").HeaderText = "User Update"
            dgvRptSaleByStatus.Columns("GRAND_TOTAL").HeaderText = "Total"
            dgvRptSaleByStatus.Columns("RECEIPT_NO").Width = 75
            dgvRptSaleByStatus.Columns("SALE_DATE").Width = 80
            dgvRptSaleByStatus.Columns("CUSTOMER").Width = 90
            dgvRptSaleByStatus.Columns("PAYMENT_METHOD").Width = 80
            dgvRptSaleByStatus.Columns("WAREHOUSE").Width = 80
            dgvRptSaleByStatus.Columns("SALESMAN").Width = 75
            dgvRptSaleByStatus.Columns("NUMBER_OF_ITEM").Width = 50
            dgvRptSaleByStatus.Columns("DOWN_PAYMENT").Width = 75
            dgvRptSaleByStatus.Columns("USER_UPDATE").Width = 75
            dgvRptSaleByStatus.Columns("SALE_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptSaleByStatus.Columns("NUMBER_OF_ITEM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptSaleByStatus.Columns("DOWN_PAYMENT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptSaleByStatus.Columns("GRAND_TOTAL").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptSaleByStatus.Columns("SALE_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvRptSaleByStatus.Columns("GRAND_TOTAL").DefaultCellStyle.Format = "n0"
            dgvRptSaleByStatus.Columns("DATE_FROM").Visible = False
            dgvRptSaleByStatus.Columns("DATE_TO").Visible = False
        End If
    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        Dim objf As New frmRepSaleByStatus
        objf.date1 = dtpPeriod1.Value
        objf.date2 = dtpPeriod2.Value
        objf.ReportViewer1.ShowRefreshButton = False
        objf.ReportViewer1.ZoomPercent = 100
        objf.WindowState = FormWindowState.Maximized
        objf.Show()
    End Sub
End Class