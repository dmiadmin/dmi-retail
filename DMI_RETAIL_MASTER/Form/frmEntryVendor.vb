﻿Imports System.Data.SqlClient

Public Class frmEntryVendor
    Dim tmpSaveMode, tmpVendorCode, tmpVendorName, tmpCode, tmpEditNo As String
    Dim tmpNoOfCode, tmpNoOfName, tmpUsedVendor, tmpBindPosition As Integer
    Dim tmpChange As Boolean
    Dim tmpEndDate As DateTime
    Public VENDORBindingSource As New BindingSource

    Private Sub frmEntryVendor_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If tmpChange = True And VENDOR_CODETextBox.Enabled = True Then
            tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            If tmpvar = 6 Then
                cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub GetData()
        sql = "SELECT VENDOR_ID, VENDOR_CODE, VENDOR_NAME, VENDOR_ADDRESS, VENDOR_PHONE, VENDOR_FAX, VENDOR_CITY, " & _
              "VENDOR_CONTACT_PERSON, USER_ID_INPUT, INPUT_DATE, USER_ID_UPDATE, UPDATE_DATE, " & _
              "EFFECTIVE_END_DATE, EFFECTIVE_START_DATE FROM VENDOR ORDER BY VENDOR_NAME"
        dt = New DataTable
        sqladapter = New SqlDataAdapter(sql, xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        VENDORBindingSource.DataSource = dt
        VENDORBindingNavigator.BindingSource = VENDORBindingSource

    End Sub

    Private Sub frmEntryVendor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_VENDOR.VENDOR' table. You can move, or remove it, as needed.
        'Me.VENDORTableAdapter.Fill(Me.DS_VENDOR.VENDOR)
        GetData()
        VENDOR_CODETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", VENDORBindingSource, "VENDOR_CODE", True))
        VENDOR_NAMETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", VENDORBindingSource, "VENDOR_NAME", True))
        VENDOR_ADDRESSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", VENDORBindingSource, "VENDOR_ADDRESS", True))
        VENDOR_PHONETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", VENDORBindingSource, "VENDOR_PHONE", True))
        VENDOR_FAXTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", VENDORBindingSource, "VENDOR_FAX", True))
        VENDOR_CITYTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", VENDORBindingSource, "VENDOR_CITY", True))
        VENDOR_CONTACT_PERSONTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", VENDORBindingSource, "VENDOR_CONTACT_PERSON", True))
        accFormName = Me.Text
        DisableInputBox(Me)

        cmdSearchCode.Visible = True
        cmdSearchName.Visible = True
        VENDORBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        tmpChange = False
        chkActive.Enabled = False

        If Language = "Indonesian" Then
            lblAddress.Text = "Alamat"
            lblCity.Text = "Kota"
            lblCode.Text = "Kode"
            lblName.Text = "Nama"
            lblPhone.Text = "Telpon"
            cmdAdd.Text = "Tambah"
            cmdEdit.Text = "Ubah"
            cmdSave.Text = "Simpan"
            cmdUndo.Text = "Batal"
            cmdDelete.Text = "Hapus"
            Me.Text = "Input Supplier"
        End If

        Try
            If VENDORBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        Catch
        End Try

    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If AllowAdd = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Insert"
        tmpBindPosition = VENDORBindingSource.Position

        EnableInputBox(Me)
        VENDORBindingSource.AddNew()
        xComm = New SqlCommand("SP_GENERATE_MASTER_CODE", xConn)
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@DOCUMENT_TYPE", "VENDOR")
        xComm.Parameters.Add("@RECEIPT_NO", SqlDbType.VarChar, 50)
        xComm.Parameters.Item("@RECEIPT_NO").Direction = ParameterDirection.Output

        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xComm.ExecuteNonQuery()
        tmpVendorCode = xComm.Parameters.Item("@RECEIPT_NO").Value
        VENDOR_CODETextBox.Text = tmpVendorCode

        cmdSearchCode.Visible = False
        cmdSearchName.Visible = False
        VENDORBindingNavigator.Enabled = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        tmpChange = False
        chkActive.Enabled = True
        chkActive.Checked = True

    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        If VENDOR_NAMETextBox.Text = "" Then Exit Sub

        If AllowEdit = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If


        tmpSaveMode = "Update"

        EnableInputBox(Me)

        cmdSearchCode.Visible = False
        cmdSearchName.Visible = False
        VENDORBindingNavigator.Enabled = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        chkActive.Enabled = True
        cmdDelete.Enabled = False
        tmpEditNo = VENDOR_CODETextBox.Text
        tmpVendorCode = VENDOR_CODETextBox.Text
        tmpVendorName = VENDOR_NAMETextBox.Text
        tmpChange = False

        tmpBindPosition = VENDORBindingSource.Position

        If VENDORBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
            chkActive.Checked = False
        Else
            chkActive.Checked = True
        End If
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        tmpSaveMode = ""

        DisableInputBox(Me)
        VENDORBindingSource.CancelEdit()

        cmdSearchCode.Visible = True
        cmdSearchName.Visible = True
        VENDORBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        tmpChange = False
        chkActive.Enabled = False

        VENDORBindingSource.Position = tmpBindPosition
        VENDORBindingSource_CurrentChanged(Nothing, Nothing)
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Me.Validate()

        If VENDOR_NAMETextBox.Text = "" Or VENDOR_CODETextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpSaveMode = "Insert" Then
            xComm = New SqlCommand("SP_VENDOR_CHECK_CODE", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@VENDOR_CODE", VENDOR_CODETextBox.Text)
            xComm.Parameters.Add("@OUTPUT", SqlDbType.Int)
            xComm.Parameters.Item("@OUTPUT").Direction = ParameterDirection.Output
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()

            tmpNoOfCode = xComm.Parameters.Item("@OUTPUT").Value

            If tmpNoOfCode > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Kode sudah ada di dalam database." & vbCrLf & _
                           "Silahkan isi kode yang lain!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Code already exist in Database." & vbCrLf & _
                           "Please enter another Code!", MsgBoxStyle.Critical, "DMI Retail")
                End If

                While tmpNoOfCode > 0
                    xComm = New SqlCommand("SP_GENERATE_MASTER_CODE", xConn)
                    xComm.CommandType = CommandType.StoredProcedure
                    xComm.Parameters.AddWithValue("@DOCUMENT_TYPE", "VENDOR")
                    If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                    xdreader = xComm.ExecuteReader
                    xdreader.Read()
                    If xdreader.HasRows Then
                        VENDOR_CODETextBox.Text = xdreader.Item(0)
                    End If
                    'VENDORTableAdapter.SP_GENERATE_MASTER_CODE("VENDOR", VENDOR_CODETextBox.Text)
                    xComm = New SqlCommand("SP_VENDOR_CHECK_CODE", xConn)
                    xComm.CommandType = CommandType.StoredProcedure
                    xComm.Parameters.AddWithValue("@VENDOR_CODE", VENDOR_CODETextBox.Text)
                    If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                    xdreader = xComm.ExecuteReader
                    xdreader.Read()
                    tmpNoOfCode = xdreader.Item(0)
                    'VENDORTableAdapter.SP_VENDOR_CHECK_CODE(VENDOR_CODETextBox.Text, tmpNoOfCode)
                End While
                Exit Sub
            End If
            xComm = New SqlCommand("SP_VENDOR", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "I")
            xComm.Parameters.AddWithValue("@VENDOR_ID", 0)
            xComm.Parameters.AddWithValue("@VENDOR_CODE", VENDOR_CODETextBox.Text)
            xComm.Parameters.AddWithValue("@VENDOR_NAME", VENDOR_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@VENDOR_ADDRESS", VENDOR_ADDRESSTextBox.Text)
            xComm.Parameters.AddWithValue("@VENDOR_PHONE", VENDOR_PHONETextBox.Text)
            xComm.Parameters.AddWithValue("@VENDOR_FAX", VENDOR_FAXTextBox.Text)
            xComm.Parameters.AddWithValue("@VENDOR_CITY", VENDOR_CITYTextBox.Text)
            xComm.Parameters.AddWithValue("@VENDOR_CONTACT_PERSON", VENDOR_CONTACT_PERSONTextBox.Text)
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", UserId)
            xComm.Parameters.AddWithValue("@INPUT_DATE", Now)
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", 0)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", DateSerial(4000, 12, 31))
            xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", Now)
            xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", IIf(chkActive.Checked, DateSerial(4000, 12, 31), Now))
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()
        ElseIf tmpSaveMode = "Update" Then
            If tmpEditNo <> VENDOR_CODETextBox.Text Then
                xComm = New SqlCommand("SP_VENDOR_CHECK_CODE", xConn)
                xComm.CommandType = CommandType.StoredProcedure
                xComm.Parameters.AddWithValue("@VENDOR_CODE", VENDOR_CODETextBox.Text)
                xComm.Parameters.Add("@OUTPUT", SqlDbType.Int)
                xComm.Parameters.Item("@OUTPUT").Direction = ParameterDirection.Output
                If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                xComm.ExecuteNonQuery()

                tmpNoOfCode = xComm.Parameters.Item("@OUTPUT").Value
                'VENDORTableAdapter.SP_VENDOR_CHECK_CODE(VENDOR_CODETextBox.Text, tmpNoOfCode)

                If tmpNoOfCode > 0 Then
                    If Language = "Indonesian" Then
                        MsgBox("Kode sudah ada di dalam database." & vbCrLf & _
                           "Silahkan isi nama kode yang lain!", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("Either Code  already exist in Database." & vbCrLf & _
                               "Please enter another Code !", MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    Exit Sub
                End If
            End If

            If chkActive.Checked = True Then
                tmpEndDate = DateSerial(4000, 12, 31)
            Else
                tmpEndDate = Now
            End If

            If VENDOR_CODETextBox.Text <> tmpVendorCode Then
                xComm = New SqlCommand("SP_VENDOR_CHECK_CODE", xConn)
                xComm.CommandType = CommandType.StoredProcedure
                xComm.Parameters.AddWithValue("@VENDOR_CODE", VENDOR_CODETextBox.Text)
                xComm.Parameters.Add("@OUTPUT", SqlDbType.Int).Direction = ParameterDirection.Output
                If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                xComm.ExecuteNonQuery()
                tmpNoOfCode = xComm.Parameters.Item("@OUTPUT").Value
                'VENDORTableAdapter.SP_VENDOR_CHECK_CODE(VENDOR_CODETextBox.Text, tmpNoOfCode)
            Else
                tmpNoOfCode = 0
            End If

            If tmpNoOfCode > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Kode sudah ada di dalam database." & vbCrLf & _
                           "Silahkan isi kode yang lain!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Code already exist in Database." & vbCrLf & _
                           "Please enter another Code!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If

            xComm = New SqlCommand("SP_VENDOR", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "U")
            xComm.Parameters.AddWithValue("@VENDOR_ID", VENDORBindingSource.Current("VENDOR_ID"))
            xComm.Parameters.AddWithValue("@VENDOR_CODE", VENDORBindingSource.Current("VENDOR_CODE"))
            xComm.Parameters.AddWithValue("@VENDOR_NAME", VENDOR_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@VENDOR_ADDRESS", VENDOR_ADDRESSTextBox.Text)
            xComm.Parameters.AddWithValue("@VENDOR_PHONE", VENDOR_PHONETextBox.Text)
            xComm.Parameters.AddWithValue("@VENDOR_FAX", VENDOR_FAXTextBox.Text)
            xComm.Parameters.AddWithValue("@VENDOR_CITY", VENDOR_CITYTextBox.Text)
            xComm.Parameters.AddWithValue("@VENDOR_CONTACT_PERSON", VENDOR_CONTACT_PERSONTextBox.Text)
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", VENDORBindingSource.Current("USER_ID_INPUT"))
            xComm.Parameters.AddWithValue("@INPUT_DATE", VENDORBindingSource.Current("INPUT_DATE"))
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", UserId)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", Now)
            xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", VENDORBindingSource.Current("EFFECTIVE_START_DATE"))
            xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", tmpEndDate)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()
        End If

        tmpCode = VENDOR_CODETextBox.Text

        tmpSaveMode = ""

        DisableInputBox(Me)
        VENDORBindingSource.CancelEdit()

        cmdSearchCode.Visible = True
        cmdSearchName.Visible = True
        VENDORBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        chkActive.Enabled = False

        VENDORBindingSource_CurrentChanged(Nothing, Nothing)
        GetData()
        'Me.VENDORTableAdapter.Fill(Me.DS_VENDOR.VENDOR)
        If Language = "Indonesian" Then
            MsgBox("Data berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
        End If

        tmpChange = False

        VENDORBindingSource.Position = VENDORBindingSource.Find("VENDOR_CODE", tmpCode)
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If VENDOR_NAMETextBox.Text = "" Then Exit Sub

        If AllowDelete = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If Language = "Indonesian" Then
            If MsgBox("Hapus supplier?" & vbCrLf & VENDOR_NAMETextBox.Text, _
                      MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
            xComm = New SqlCommand("SP_CHECK_VENDOR_USED", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@VENDOR_NAME", VENDOR_NAMETextBox.Text)
            xComm.Parameters.Add("@RESULT", SqlDbType.Int).Direction = ParameterDirection.Output
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()
            tmpUsedVendor = xComm.Parameters.Item("@RESULT").Value
            'VENDORTableAdapter.SP_CHECK_VENDOR_USED(VENDOR_NAMETextBox.Text, tmpUsedVendor)

            If tmpUsedVendor > 0 Then
                MsgBox("Supplier ini telah digunakan di transaksi yang lain !" & vbCrLf & _
                           "Anda tidak dapat menghapus data ini !", _
                           MsgBoxStyle.Critical)
                Exit Sub
            End If
        Else
            If MsgBox("Delete VENDOR?" & vbCrLf & VENDOR_NAMETextBox.Text, _
                 MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
            xComm = New SqlCommand("SP_CHECK_VENDOR_USED", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@VENDOR_NAME", VENDOR_NAMETextBox.Text)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xdreader = xComm.ExecuteReader
            xdreader.Read()
            tmpUsedVendor = xdreader.Item(0)
            'VENDORTableAdapter.SP_CHECK_VENDOR_USED(VENDOR_NAMETextBox.Text, tmpUsedVendor)

            If tmpUsedVendor > 0 Then
                MsgBox("This Vendor is used by another transaction !" & vbCrLf & _
                      "You can not delete this data !", _
                      MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If
        End If
        xComm = New SqlCommand("SP_VENDOR", xConn)
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@METHOD", "D")
        xComm.Parameters.AddWithValue("@VENDOR_ID", VENDORBindingSource.Current("VENDOR_ID"))
        xComm.Parameters.AddWithValue("@VENDOR_CODE", VENDORBindingSource.Current("VENDOR_CODE"))
        xComm.Parameters.AddWithValue("@VENDOR_NAME", VENDORBindingSource.Current("VENDOR_NAME"))
        xComm.Parameters.AddWithValue("@VENDOR_ADDRESS", VENDORBindingSource.Current("VENDOR_ADDRESS"))
        xComm.Parameters.AddWithValue("@VENDOR_PHONE", VENDORBindingSource.Current("VENDOR_PHONE"))
        xComm.Parameters.AddWithValue("@VENDOR_FAX", VENDORBindingSource.Current("VENDOR_FAX"))
        xComm.Parameters.AddWithValue("@VENDOR_CITY", VENDORBindingSource.Current("VENDOR_CITY"))
        xComm.Parameters.AddWithValue("@VENDOR_CONTACT_PERSON", VENDORBindingSource.Current("VENDOR_CONTACT_PERSON"))
        xComm.Parameters.AddWithValue("@USER_ID_INPUT", VENDORBindingSource.Current("USER_ID_INPUT"))
        xComm.Parameters.AddWithValue("@INPUT_DATE", VENDORBindingSource.Current("INPUT_DATE"))
        xComm.Parameters.AddWithValue("@USER_ID_UPDATE", VENDORBindingSource.Current("USER_ID_UPDATE"))
        xComm.Parameters.AddWithValue("@UPDATE_DATE", VENDORBindingSource.Current("UPDATE_DATE"))
        xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", VENDORBindingSource.Current("EFFECTIVE_START_DATE"))
        xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", Now)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xComm.ExecuteNonQuery()

        VENDORBindingSource.RemoveCurrent()
        VENDORBindingSource.Position = 0
        GetData()
        'VENDORTableAdapter.Fill(Me.DS_VENDOR.VENDOR)

        Try
            If VENDORBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        Catch
        End Try

        tmpChange = False
        chkActive.Enabled = False

    End Sub

    Private Sub VENDOR_CODETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VENDOR_CODETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub VENDOR_NAMETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VENDOR_NAMETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub VENDOR_ADDRESSTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VENDOR_ADDRESSTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub VENDOR_PHONETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VENDOR_PHONETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub VENDOR_FAXTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VENDOR_FAXTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub VENDOR_CITYTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VENDOR_CITYTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub VENDOR_CONTACT_PERSONTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VENDOR_CONTACT_PERSONTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub VENDORBindingSource_CurrentChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If VENDORBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        Catch
        End Try

    End Sub

    Private Sub cmdSearchCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchCode.Click
        tmpSearchMode = "Vendor Code"
        frmSearchVendor.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        VENDORBindingSource.Position = VENDORBindingSource.Find("VENDOR_ID", tmpSearchResult)
        tmpEditNo = VENDOR_CODETextBox.Text
    End Sub

    Private Sub cmdSearchName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchName.Click
        tmpSearchMode = "Vendor Name"
        frmSearchVendor.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        VENDORBindingSource.Position = VENDORBindingSource.Find("VENDOR_ID", tmpSearchResult)
        tmpEditNo = VENDOR_CODETextBox.Text
    End Sub

    Private Sub BindingNavigatorPositionItem_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BindingNavigatorPositionItem.TextChanged
        Try
            If VENDORBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        Catch
        End Try

    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        AccessPrivilege(Me.Tag)
    End Sub
End Class