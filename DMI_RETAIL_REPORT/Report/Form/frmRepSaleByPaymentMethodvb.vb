﻿Public Class frmRepSaleByPaymentMethodvb
    Public date1, date2 As Date
    Private Sub frmRepSaleByPaymentMethodvb_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DB_DMI_RETAILDataSetALL.COMPANY_INFORMATION' table. You can move, or remove it, as needed.
        Me.COMPANY_INFORMATIONTableAdapter.Fill(Me.DB_DMI_RETAILDataSetALL.COMPANY_INFORMATION)
        'TODO: This line of code loads data into the 'DB_DMI_RETAILDataSetALL.SP_SALE_REPORT_BY_PAYMENT_METHOD' table. You can move, or remove it, as needed.
        Me.SP_SALE_REPORT_BY_PAYMENT_METHODTableAdapter.Fill(Me.DB_DMI_RETAILDataSetALL.SP_SALE_REPORT_BY_PAYMENT_METHOD, date1, date2)

        Me.ReportViewer1.RefreshReport()

        Dim newPageSettings As New System.Drawing.Printing.PageSettings
        newPageSettings.Margins = New System.Drawing.Printing.Margins(30, 30, 35, 35)
        newPageSettings.PaperSize = New System.Drawing.Printing.PaperSize("A4", 827, 1169)
        newPageSettings.Landscape = True
        Me.ReportViewer1.SetPageSettings(newPageSettings)

        If Not tmpReport Is Nothing Then tmpReport.Close()
        tmpReport = Me
    End Sub
End Class