﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEntryReturnPurchase
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEntryReturnPurchase))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblVendor = New System.Windows.Forms.Label()
        Me.lblReceipNo = New System.Windows.Forms.Label()
        Me.lblPurchaseDate = New System.Windows.Forms.Label()
        Me.lblPayment = New System.Windows.Forms.Label()
        Me.txtVendor = New System.Windows.Forms.TextBox()
        Me.txtPaymentMethod = New System.Windows.Forms.TextBox()
        Me.txtReceiptNo = New System.Windows.Forms.TextBox()
        Me.cmdSearch = New System.Windows.Forms.PictureBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtDate = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.lblReturnDate = New System.Windows.Forms.Label()
        Me.lblReturnNo = New System.Windows.Forms.Label()
        Me.txtReturnDate = New System.Windows.Forms.TextBox()
        Me.dtpReturn = New System.Windows.Forms.DateTimePicker()
        Me.RETURN_NOTextBox = New System.Windows.Forms.TextBox()
        Me.PURCHASE_RETURNBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_RETURN_PURCHASE = New DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASE()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dgvPurchaseReturn = New System.Windows.Forms.DataGridView()
        Me.PRODUCT_CODE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRODUCT_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QUANTITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RETURNED_QUANTITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RETURN_QUANTITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DESCRIPTION = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblWarehouse = New System.Windows.Forms.Label()
        Me.lblReturnType = New System.Windows.Forms.Label()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.REUSABLE_STATUSCheckBox = New System.Windows.Forms.CheckBox()
        Me.PURCHASE_RETURN_DETAILBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RETURN_TYPEComboBox = New System.Windows.Forms.ComboBox()
        Me.WAREHOUSE_NAMEComboBox = New System.Windows.Forms.ComboBox()
        Me.WAREHOUSEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_PURCHASE_RETURN_DETAIL_LISTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TableAdapterManager = New DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASETableAdapters.TableAdapterManager()
        Me.PURCHASE_RETURNTableAdapter = New DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASETableAdapters.PURCHASE_RETURNTableAdapter()
        Me.WAREHOUSETableAdapter = New DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASETableAdapters.WAREHOUSETableAdapter()
        Me.PURCHASE_RETURN_DETAILTableAdapter = New DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASETableAdapters.PURCHASE_RETURN_DETAILTableAdapter()
        Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter = New DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASETableAdapters.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter()
        Me.SP_PURCHASE_RETURN_DETAIL_LISTTableAdapter = New DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASETableAdapters.SP_PURCHASE_RETURN_DETAIL_LISTTableAdapter()
        Me.SP_SHOW_LIST_RETURN_PURCHASEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_SHOW_LIST_RETURN_PURCHASETableAdapter = New DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASETableAdapters.SP_SHOW_LIST_RETURN_PURCHASETableAdapter()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        CType(Me.cmdSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.PURCHASE_RETURNBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_RETURN_PURCHASE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvPurchaseReturn, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PURCHASE_RETURN_DETAILBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_PURCHASE_RETURN_DETAIL_LISTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_SHOW_LIST_RETURN_PURCHASEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblVendor
        '
        Me.lblVendor.AutoSize = True
        Me.lblVendor.Location = New System.Drawing.Point(353, 19)
        Me.lblVendor.Name = "lblVendor"
        Me.lblVendor.Size = New System.Drawing.Size(50, 15)
        Me.lblVendor.TabIndex = 0
        Me.lblVendor.Text = "Vendor"
        '
        'lblReceipNo
        '
        Me.lblReceipNo.AutoSize = True
        Me.lblReceipNo.Location = New System.Drawing.Point(353, 46)
        Me.lblReceipNo.Name = "lblReceipNo"
        Me.lblReceipNo.Size = New System.Drawing.Size(72, 15)
        Me.lblReceipNo.TabIndex = 1
        Me.lblReceipNo.Text = "Receipt No"
        '
        'lblPurchaseDate
        '
        Me.lblPurchaseDate.AutoSize = True
        Me.lblPurchaseDate.Location = New System.Drawing.Point(353, 77)
        Me.lblPurchaseDate.Name = "lblPurchaseDate"
        Me.lblPurchaseDate.Size = New System.Drawing.Size(35, 15)
        Me.lblPurchaseDate.TabIndex = 2
        Me.lblPurchaseDate.Text = "Date"
        '
        'lblPayment
        '
        Me.lblPayment.AutoSize = True
        Me.lblPayment.Location = New System.Drawing.Point(353, 105)
        Me.lblPayment.Name = "lblPayment"
        Me.lblPayment.Size = New System.Drawing.Size(105, 15)
        Me.lblPayment.TabIndex = 3
        Me.lblPayment.Text = "Payment Method"
        '
        'txtVendor
        '
        Me.txtVendor.Location = New System.Drawing.Point(469, 16)
        Me.txtVendor.Name = "txtVendor"
        Me.txtVendor.ReadOnly = True
        Me.txtVendor.Size = New System.Drawing.Size(158, 22)
        Me.txtVendor.TabIndex = 3
        '
        'txtPaymentMethod
        '
        Me.txtPaymentMethod.Location = New System.Drawing.Point(469, 102)
        Me.txtPaymentMethod.Name = "txtPaymentMethod"
        Me.txtPaymentMethod.ReadOnly = True
        Me.txtPaymentMethod.Size = New System.Drawing.Size(158, 22)
        Me.txtPaymentMethod.TabIndex = 6
        '
        'txtReceiptNo
        '
        Me.txtReceiptNo.Location = New System.Drawing.Point(469, 43)
        Me.txtReceiptNo.Name = "txtReceiptNo"
        Me.txtReceiptNo.ReadOnly = True
        Me.txtReceiptNo.Size = New System.Drawing.Size(158, 22)
        Me.txtReceiptNo.TabIndex = 4
        '
        'cmdSearch
        '
        Me.cmdSearch.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearch.Image = CType(resources.GetObject("cmdSearch.Image"), System.Drawing.Image)
        Me.cmdSearch.Location = New System.Drawing.Point(635, 16)
        Me.cmdSearch.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearch.Name = "cmdSearch"
        Me.cmdSearch.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearch.TabIndex = 19
        Me.cmdSearch.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtDate)
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.cmdSearch)
        Me.GroupBox1.Controls.Add(Me.txtReceiptNo)
        Me.GroupBox1.Controls.Add(Me.txtPaymentMethod)
        Me.GroupBox1.Controls.Add(Me.txtVendor)
        Me.GroupBox1.Controls.Add(Me.lblPayment)
        Me.GroupBox1.Controls.Add(Me.lblPurchaseDate)
        Me.GroupBox1.Controls.Add(Me.lblReceipNo)
        Me.GroupBox1.Controls.Add(Me.lblVendor)
        Me.GroupBox1.Location = New System.Drawing.Point(24, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(700, 144)
        Me.GroupBox1.TabIndex = 21
        Me.GroupBox1.TabStop = False
        '
        'txtDate
        '
        Me.txtDate.Location = New System.Drawing.Point(469, 74)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.ReadOnly = True
        Me.txtDate.Size = New System.Drawing.Size(157, 22)
        Me.txtDate.TabIndex = 5
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lblReturnDate)
        Me.GroupBox3.Controls.Add(Me.lblReturnNo)
        Me.GroupBox3.Controls.Add(Me.txtReturnDate)
        Me.GroupBox3.Controls.Add(Me.dtpReturn)
        Me.GroupBox3.Controls.Add(Me.RETURN_NOTextBox)
        Me.GroupBox3.Location = New System.Drawing.Point(24, 25)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(286, 96)
        Me.GroupBox3.TabIndex = 23
        Me.GroupBox3.TabStop = False
        '
        'lblReturnDate
        '
        Me.lblReturnDate.AutoSize = True
        Me.lblReturnDate.Location = New System.Drawing.Point(32, 58)
        Me.lblReturnDate.Name = "lblReturnDate"
        Me.lblReturnDate.Size = New System.Drawing.Size(78, 15)
        Me.lblReturnDate.TabIndex = 27
        Me.lblReturnDate.Text = "Return Date"
        '
        'lblReturnNo
        '
        Me.lblReturnNo.AutoSize = True
        Me.lblReturnNo.Location = New System.Drawing.Point(32, 29)
        Me.lblReturnNo.Name = "lblReturnNo"
        Me.lblReturnNo.Size = New System.Drawing.Size(66, 15)
        Me.lblReturnNo.TabIndex = 26
        Me.lblReturnNo.Text = "Return No"
        '
        'txtReturnDate
        '
        Me.txtReturnDate.Location = New System.Drawing.Point(132, 55)
        Me.txtReturnDate.Name = "txtReturnDate"
        Me.txtReturnDate.ReadOnly = True
        Me.txtReturnDate.Size = New System.Drawing.Size(113, 22)
        Me.txtReturnDate.TabIndex = 2
        '
        'dtpReturn
        '
        Me.dtpReturn.CustomFormat = "dd-MMM-yyyy"
        Me.dtpReturn.Enabled = False
        Me.dtpReturn.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpReturn.Location = New System.Drawing.Point(132, 55)
        Me.dtpReturn.Name = "dtpReturn"
        Me.dtpReturn.Size = New System.Drawing.Size(113, 22)
        Me.dtpReturn.TabIndex = 22
        Me.dtpReturn.Visible = False
        '
        'RETURN_NOTextBox
        '
        Me.RETURN_NOTextBox.Location = New System.Drawing.Point(132, 26)
        Me.RETURN_NOTextBox.Name = "RETURN_NOTextBox"
        Me.RETURN_NOTextBox.ReadOnly = True
        Me.RETURN_NOTextBox.Size = New System.Drawing.Size(113, 22)
        Me.RETURN_NOTextBox.TabIndex = 1
        '
        'PURCHASE_RETURNBindingSource
        '
        Me.PURCHASE_RETURNBindingSource.DataMember = "PURCHASE_RETURN"
        Me.PURCHASE_RETURNBindingSource.DataSource = Me.DS_RETURN_PURCHASE
        '
        'DS_RETURN_PURCHASE
        '
        Me.DS_RETURN_PURCHASE.DataSetName = "DS_RETURN_PURCHASE"
        Me.DS_RETURN_PURCHASE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dgvPurchaseReturn)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.lblWarehouse)
        Me.GroupBox2.Controls.Add(Me.lblReturnType)
        Me.GroupBox2.Controls.Add(Me.txtDescription)
        Me.GroupBox2.Controls.Add(Me.REUSABLE_STATUSCheckBox)
        Me.GroupBox2.Controls.Add(Me.RETURN_TYPEComboBox)
        Me.GroupBox2.Controls.Add(Me.WAREHOUSE_NAMEComboBox)
        Me.GroupBox2.Location = New System.Drawing.Point(25, 162)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(699, 259)
        Me.GroupBox2.TabIndex = 22
        Me.GroupBox2.TabStop = False
        '
        'dgvPurchaseReturn
        '
        Me.dgvPurchaseReturn.AllowUserToAddRows = False
        Me.dgvPurchaseReturn.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPurchaseReturn.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPurchaseReturn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPurchaseReturn.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PRODUCT_CODE, Me.PRODUCT_NAME, Me.QUANTITY, Me.RETURNED_QUANTITY, Me.RETURN_QUANTITY, Me.DESCRIPTION})
        Me.dgvPurchaseReturn.Location = New System.Drawing.Point(6, 21)
        Me.dgvPurchaseReturn.Name = "dgvPurchaseReturn"
        Me.dgvPurchaseReturn.RowHeadersWidth = 28
        Me.dgvPurchaseReturn.Size = New System.Drawing.Size(687, 150)
        Me.dgvPurchaseReturn.TabIndex = 28
        '
        'PRODUCT_CODE
        '
        Me.PRODUCT_CODE.HeaderText = "Product Code"
        Me.PRODUCT_CODE.Name = "PRODUCT_CODE"
        Me.PRODUCT_CODE.ReadOnly = True
        '
        'PRODUCT_NAME
        '
        Me.PRODUCT_NAME.HeaderText = "Produk Name"
        Me.PRODUCT_NAME.Name = "PRODUCT_NAME"
        Me.PRODUCT_NAME.ReadOnly = True
        Me.PRODUCT_NAME.Width = 150
        '
        'QUANTITY
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = "0"
        Me.QUANTITY.DefaultCellStyle = DataGridViewCellStyle2
        Me.QUANTITY.HeaderText = "Qty"
        Me.QUANTITY.Name = "QUANTITY"
        Me.QUANTITY.ReadOnly = True
        Me.QUANTITY.Width = 50
        '
        'RETURNED_QUANTITY
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N0"
        DataGridViewCellStyle3.NullValue = "0"
        Me.RETURNED_QUANTITY.DefaultCellStyle = DataGridViewCellStyle3
        Me.RETURNED_QUANTITY.HeaderText = "Returned Quantity"
        Me.RETURNED_QUANTITY.Name = "RETURNED_QUANTITY"
        Me.RETURNED_QUANTITY.ReadOnly = True
        '
        'RETURN_QUANTITY
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = "0"
        Me.RETURN_QUANTITY.DefaultCellStyle = DataGridViewCellStyle4
        Me.RETURN_QUANTITY.HeaderText = "Return Quantity"
        Me.RETURN_QUANTITY.Name = "RETURN_QUANTITY"
        Me.RETURN_QUANTITY.Width = 82
        '
        'DESCRIPTION
        '
        Me.DESCRIPTION.HeaderText = "Description"
        Me.DESCRIPTION.Name = "DESCRIPTION"
        Me.DESCRIPTION.Width = 160
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(387, 194)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(76, 15)
        Me.Label1.TabIndex = 28
        Me.Label1.Text = "Description"
        '
        'lblWarehouse
        '
        Me.lblWarehouse.AutoSize = True
        Me.lblWarehouse.Location = New System.Drawing.Point(26, 223)
        Me.lblWarehouse.Name = "lblWarehouse"
        Me.lblWarehouse.Size = New System.Drawing.Size(73, 15)
        Me.lblWarehouse.TabIndex = 10
        Me.lblWarehouse.Text = "Warehouse"
        '
        'lblReturnType
        '
        Me.lblReturnType.AutoSize = True
        Me.lblReturnType.Location = New System.Drawing.Point(26, 195)
        Me.lblReturnType.Name = "lblReturnType"
        Me.lblReturnType.Size = New System.Drawing.Size(79, 15)
        Me.lblReturnType.TabIndex = 9
        Me.lblReturnType.Text = "Return Type"
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(469, 192)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(207, 51)
        Me.txtDescription.TabIndex = 10
        '
        'REUSABLE_STATUSCheckBox
        '
        Me.REUSABLE_STATUSCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.PURCHASE_RETURN_DETAILBindingSource, "REUSABLE_STATUS", True))
        Me.REUSABLE_STATUSCheckBox.Location = New System.Drawing.Point(249, 190)
        Me.REUSABLE_STATUSCheckBox.Name = "REUSABLE_STATUSCheckBox"
        Me.REUSABLE_STATUSCheckBox.Size = New System.Drawing.Size(132, 24)
        Me.REUSABLE_STATUSCheckBox.TabIndex = 8
        Me.REUSABLE_STATUSCheckBox.Text = "Reusable"
        Me.REUSABLE_STATUSCheckBox.UseVisualStyleBackColor = True
        '
        'PURCHASE_RETURN_DETAILBindingSource
        '
        Me.PURCHASE_RETURN_DETAILBindingSource.DataMember = "PURCHASE_RETURN_DETAIL"
        Me.PURCHASE_RETURN_DETAILBindingSource.DataSource = Me.DS_RETURN_PURCHASE
        '
        'RETURN_TYPEComboBox
        '
        Me.RETURN_TYPEComboBox.FormattingEnabled = True
        Me.RETURN_TYPEComboBox.Items.AddRange(New Object() {"Goods", "Non-Goods"})
        Me.RETURN_TYPEComboBox.Location = New System.Drawing.Point(122, 191)
        Me.RETURN_TYPEComboBox.Name = "RETURN_TYPEComboBox"
        Me.RETURN_TYPEComboBox.Size = New System.Drawing.Size(121, 23)
        Me.RETURN_TYPEComboBox.TabIndex = 7
        '
        'WAREHOUSE_NAMEComboBox
        '
        Me.WAREHOUSE_NAMEComboBox.DataSource = Me.WAREHOUSEBindingSource
        Me.WAREHOUSE_NAMEComboBox.DisplayMember = "WAREHOUSE_NAME"
        Me.WAREHOUSE_NAMEComboBox.Enabled = False
        Me.WAREHOUSE_NAMEComboBox.FormattingEnabled = True
        Me.WAREHOUSE_NAMEComboBox.Location = New System.Drawing.Point(122, 220)
        Me.WAREHOUSE_NAMEComboBox.Name = "WAREHOUSE_NAMEComboBox"
        Me.WAREHOUSE_NAMEComboBox.Size = New System.Drawing.Size(121, 23)
        Me.WAREHOUSE_NAMEComboBox.TabIndex = 9
        Me.WAREHOUSE_NAMEComboBox.ValueMember = "WAREHOUSE_ID"
        '
        'WAREHOUSEBindingSource
        '
        Me.WAREHOUSEBindingSource.DataMember = "WAREHOUSE"
        Me.WAREHOUSEBindingSource.DataSource = Me.DS_RETURN_PURCHASE
        '
        'SP_PURCHASE_RETURN_DETAIL_LISTBindingSource
        '
        Me.SP_PURCHASE_RETURN_DETAIL_LISTBindingSource.DataMember = "SP_PURCHASE_RETURN_DETAIL_LIST"
        Me.SP_PURCHASE_RETURN_DETAIL_LISTBindingSource.DataSource = Me.DS_RETURN_PURCHASE
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.PURCHASE_RETURN_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.PURCHASE_RETURNTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'PURCHASE_RETURNTableAdapter
        '
        Me.PURCHASE_RETURNTableAdapter.ClearBeforeFill = True
        '
        'WAREHOUSETableAdapter
        '
        Me.WAREHOUSETableAdapter.ClearBeforeFill = True
        '
        'PURCHASE_RETURN_DETAILTableAdapter
        '
        Me.PURCHASE_RETURN_DETAILTableAdapter.ClearBeforeFill = True
        '
        'SP_PRODUCT_WAREHOUSE_DETAILBindingSource
        '
        Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource.DataMember = "SP_PRODUCT_WAREHOUSE_DETAIL"
        Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource.DataSource = Me.DS_RETURN_PURCHASE
        '
        'SP_PRODUCT_WAREHOUSE_DETAILTableAdapter
        '
        Me.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.ClearBeforeFill = True
        '
        'SP_PURCHASE_RETURN_DETAIL_LISTTableAdapter
        '
        Me.SP_PURCHASE_RETURN_DETAIL_LISTTableAdapter.ClearBeforeFill = True
        '
        'SP_SHOW_LIST_RETURN_PURCHASEBindingSource
        '
        Me.SP_SHOW_LIST_RETURN_PURCHASEBindingSource.DataMember = "SP_SHOW_LIST_RETURN_PURCHASE"
        Me.SP_SHOW_LIST_RETURN_PURCHASEBindingSource.DataSource = Me.DS_RETURN_PURCHASE
        '
        'SP_SHOW_LIST_RETURN_PURCHASETableAdapter
        '
        Me.SP_SHOW_LIST_RETURN_PURCHASETableAdapter.ClearBeforeFill = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.cmdUndo)
        Me.GroupBox4.Controls.Add(Me.cmdSave)
        Me.GroupBox4.Controls.Add(Me.cmdEdit)
        Me.GroupBox4.Controls.Add(Me.cmdDelete)
        Me.GroupBox4.Controls.Add(Me.cmdAdd)
        Me.GroupBox4.Location = New System.Drawing.Point(116, 427)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox4.Size = New System.Drawing.Size(515, 55)
        Me.GroupBox4.TabIndex = 23
        Me.GroupBox4.TabStop = False
        '
        'cmdUndo
        '
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(220, 17)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(74, 29)
        Me.cmdUndo.TabIndex = 16
        Me.cmdUndo.Text = "&Undo"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSave.Location = New System.Drawing.Point(321, 17)
        Me.cmdSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(74, 29)
        Me.cmdSave.TabIndex = 17
        Me.cmdSave.Text = "&Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Enabled = False
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(119, 17)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(74, 29)
        Me.cmdEdit.TabIndex = 15
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Enabled = False
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(422, 17)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(74, 29)
        Me.cmdDelete.TabIndex = 18
        Me.cmdDelete.Text = "&Delete"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Enabled = False
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(18, 17)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(74, 29)
        Me.cmdAdd.TabIndex = 14
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'frmEntryReturnPurchase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(745, 498)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmEntryReturnPurchase"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Entry Return Purchase"
        Me.Text = "Entry Return Purchase"
        CType(Me.cmdSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.PURCHASE_RETURNBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_RETURN_PURCHASE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvPurchaseReturn, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PURCHASE_RETURN_DETAILBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_PURCHASE_RETURN_DETAIL_LISTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_SHOW_LIST_RETURN_PURCHASEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblVendor As System.Windows.Forms.Label
    Friend WithEvents lblReceipNo As System.Windows.Forms.Label
    Friend WithEvents lblPurchaseDate As System.Windows.Forms.Label
    Friend WithEvents lblPayment As System.Windows.Forms.Label
    Friend WithEvents txtVendor As System.Windows.Forms.TextBox
    Friend WithEvents txtPaymentMethod As System.Windows.Forms.TextBox
    Friend WithEvents txtReceiptNo As System.Windows.Forms.TextBox
    Friend WithEvents cmdSearch As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents DS_RETURN_PURCHASE As DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASE
    Friend WithEvents TableAdapterManager As DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASETableAdapters.TableAdapterManager
    Friend WithEvents PURCHASE_RETURNBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PURCHASE_RETURNTableAdapter As DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASETableAdapters.PURCHASE_RETURNTableAdapter
    Friend WithEvents RETURN_NOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents dtpReturn As System.Windows.Forms.DateTimePicker
    Friend WithEvents WAREHOUSEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents WAREHOUSETableAdapter As DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASETableAdapters.WAREHOUSETableAdapter
    Friend WithEvents RETURN_TYPEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents WAREHOUSE_NAMEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents PURCHASE_RETURN_DETAILBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PURCHASE_RETURN_DETAILTableAdapter As DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASETableAdapters.PURCHASE_RETURN_DETAILTableAdapter
    Friend WithEvents REUSABLE_STATUSCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents SP_PRODUCT_WAREHOUSE_DETAILBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_PRODUCT_WAREHOUSE_DETAILTableAdapter As DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASETableAdapters.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter
    Friend WithEvents txtDate As System.Windows.Forms.TextBox
    Friend WithEvents txtReturnDate As System.Windows.Forms.TextBox
    Friend WithEvents lblReturnDate As System.Windows.Forms.Label
    Friend WithEvents lblReturnNo As System.Windows.Forms.Label
    Friend WithEvents lblWarehouse As System.Windows.Forms.Label
    Friend WithEvents lblReturnType As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SP_PURCHASE_RETURN_DETAIL_LISTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_PURCHASE_RETURN_DETAIL_LISTTableAdapter As DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASETableAdapters.SP_PURCHASE_RETURN_DETAIL_LISTTableAdapter
    Friend WithEvents dgvPurchaseReturn As System.Windows.Forms.DataGridView
    Friend WithEvents SP_SHOW_LIST_RETURN_PURCHASEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_SHOW_LIST_RETURN_PURCHASETableAdapter As DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASETableAdapters.SP_SHOW_LIST_RETURN_PURCHASETableAdapter
    Friend WithEvents PRODUCT_CODE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRODUCT_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QUANTITY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RETURNED_QUANTITY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RETURN_QUANTITY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DESCRIPTION As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
End Class
