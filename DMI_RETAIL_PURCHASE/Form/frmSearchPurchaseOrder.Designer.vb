﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSearchPurchaseOrder
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSearchPurchaseOrder))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DS_PURCHASE_ORDER = New DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDER()
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_SELECT_PURCHASE_ORDER_HEADERTableAdapter = New DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.SP_SELECT_PURCHASE_ORDER_HEADERTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.TableAdapterManager()
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.dgvSearchPO = New System.Windows.Forms.DataGridView()
        Me.PURCHASE_ORDER_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PURCHASE_ORDER_NUMBER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PURCHASE_ORDER_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DELIVERY_REQUEST_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VENDOR_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VENDOR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SHIP_TO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_OF_ITEM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GRAND_TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtVendor = New System.Windows.Forms.TextBox()
        Me.txtPurchaseOrderNo = New System.Windows.Forms.TextBox()
        CType(Me.DS_PURCHASE_ORDER, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator.SuspendLayout()
        CType(Me.dgvSearchPO, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'DS_PURCHASE_ORDER
        '
        Me.DS_PURCHASE_ORDER.DataSetName = "DS_PURCHASE_ORDER"
        Me.DS_PURCHASE_ORDER.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SP_SELECT_PURCHASE_ORDER_HEADERBindingSource
        '
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingSource.DataMember = "SP_SELECT_PURCHASE_ORDER_HEADER"
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingSource.DataSource = Me.DS_PURCHASE_ORDER
        '
        'SP_SELECT_PURCHASE_ORDER_HEADERTableAdapter
        '
        Me.SP_SELECT_PURCHASE_ORDER_HEADERTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.DELIVERY_PURCHASE_ORDER_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.DELIVERY_PURCHASE_ORDER_HEADERTableAdapter = Nothing
        Me.TableAdapterManager.PRODUCTTableAdapter = Nothing
        Me.TableAdapterManager.PURCHASE_ORDER_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.PURCHASE_ORDER_HEADERTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.VENDORTableAdapter = Nothing
        '
        'SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator
        '
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator.AddNewItem = Nothing
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator.BindingSource = Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingSource
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator.DeleteItem = Nothing
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator.Name = "SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator"
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator.Size = New System.Drawing.Size(845, 25)
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator.TabIndex = 0
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(58, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'dgvSearchPO
        '
        Me.dgvSearchPO.AllowUserToAddRows = False
        Me.dgvSearchPO.AllowUserToDeleteRows = False
        Me.dgvSearchPO.AutoGenerateColumns = False
        Me.dgvSearchPO.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSearchPO.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSearchPO.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSearchPO.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PURCHASE_ORDER_ID, Me.PURCHASE_ORDER_NUMBER, Me.PURCHASE_ORDER_DATE, Me.DELIVERY_REQUEST_DATE, Me.VENDOR_ID, Me.VENDOR, Me.SHIP_TO, Me.NO_OF_ITEM, Me.GRAND_TOTAL})
        Me.dgvSearchPO.DataSource = Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingSource
        Me.dgvSearchPO.Location = New System.Drawing.Point(22, 152)
        Me.dgvSearchPO.Name = "dgvSearchPO"
        Me.dgvSearchPO.ReadOnly = True
        Me.dgvSearchPO.Size = New System.Drawing.Size(802, 254)
        Me.dgvSearchPO.TabIndex = 1
        '
        'PURCHASE_ORDER_ID
        '
        Me.PURCHASE_ORDER_ID.DataPropertyName = "PURCHASE_ORDER_ID"
        Me.PURCHASE_ORDER_ID.HeaderText = "PURCHASE_ORDER_ID"
        Me.PURCHASE_ORDER_ID.Name = "PURCHASE_ORDER_ID"
        Me.PURCHASE_ORDER_ID.ReadOnly = True
        Me.PURCHASE_ORDER_ID.Visible = False
        '
        'PURCHASE_ORDER_NUMBER
        '
        Me.PURCHASE_ORDER_NUMBER.DataPropertyName = "PURCHASE_ORDER_NUMBER"
        Me.PURCHASE_ORDER_NUMBER.HeaderText = "Purchase Order No"
        Me.PURCHASE_ORDER_NUMBER.Name = "PURCHASE_ORDER_NUMBER"
        Me.PURCHASE_ORDER_NUMBER.ReadOnly = True
        '
        'PURCHASE_ORDER_DATE
        '
        Me.PURCHASE_ORDER_DATE.DataPropertyName = "PURCHASE_ORDER_DATE"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "dd-MMM-yyyy"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.PURCHASE_ORDER_DATE.DefaultCellStyle = DataGridViewCellStyle2
        Me.PURCHASE_ORDER_DATE.HeaderText = "Order Date"
        Me.PURCHASE_ORDER_DATE.Name = "PURCHASE_ORDER_DATE"
        Me.PURCHASE_ORDER_DATE.ReadOnly = True
        '
        'DELIVERY_REQUEST_DATE
        '
        Me.DELIVERY_REQUEST_DATE.DataPropertyName = "DELIVERY_REQUEST_DATE"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "dd-MMM-yyyy"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.DELIVERY_REQUEST_DATE.DefaultCellStyle = DataGridViewCellStyle3
        Me.DELIVERY_REQUEST_DATE.HeaderText = "Delivery Request Date"
        Me.DELIVERY_REQUEST_DATE.Name = "DELIVERY_REQUEST_DATE"
        Me.DELIVERY_REQUEST_DATE.ReadOnly = True
        '
        'VENDOR_ID
        '
        Me.VENDOR_ID.DataPropertyName = "VENDOR_ID"
        Me.VENDOR_ID.HeaderText = "VENDOR_ID"
        Me.VENDOR_ID.Name = "VENDOR_ID"
        Me.VENDOR_ID.ReadOnly = True
        Me.VENDOR_ID.Visible = False
        '
        'VENDOR
        '
        Me.VENDOR.DataPropertyName = "VENDOR"
        Me.VENDOR.HeaderText = "Vendor"
        Me.VENDOR.Name = "VENDOR"
        Me.VENDOR.ReadOnly = True
        '
        'SHIP_TO
        '
        Me.SHIP_TO.DataPropertyName = "SHIP_TO"
        Me.SHIP_TO.HeaderText = "Ship To"
        Me.SHIP_TO.Name = "SHIP_TO"
        Me.SHIP_TO.ReadOnly = True
        Me.SHIP_TO.Width = 175
        '
        'NO_OF_ITEM
        '
        Me.NO_OF_ITEM.DataPropertyName = "NO_OF_ITEM"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.NO_OF_ITEM.DefaultCellStyle = DataGridViewCellStyle4
        Me.NO_OF_ITEM.HeaderText = "No Of Item"
        Me.NO_OF_ITEM.Name = "NO_OF_ITEM"
        Me.NO_OF_ITEM.ReadOnly = True
        Me.NO_OF_ITEM.Width = 60
        '
        'GRAND_TOTAL
        '
        Me.GRAND_TOTAL.DataPropertyName = "GRAND_TOTAL"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N0"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.GRAND_TOTAL.DefaultCellStyle = DataGridViewCellStyle5
        Me.GRAND_TOTAL.HeaderText = "Grand Total"
        Me.GRAND_TOTAL.Name = "GRAND_TOTAL"
        Me.GRAND_TOTAL.ReadOnly = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtVendor)
        Me.GroupBox1.Controls.Add(Me.txtPurchaseOrderNo)
        Me.GroupBox1.Location = New System.Drawing.Point(22, 31)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(801, 109)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(19, 66)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(87, 15)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Vendor Name"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(19, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(149, 15)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Purchase Order Number"
        '
        'txtVendor
        '
        Me.txtVendor.Location = New System.Drawing.Point(205, 60)
        Me.txtVendor.Name = "txtVendor"
        Me.txtVendor.Size = New System.Drawing.Size(175, 22)
        Me.txtVendor.TabIndex = 7
        '
        'txtPurchaseOrderNo
        '
        Me.txtPurchaseOrderNo.Location = New System.Drawing.Point(205, 33)
        Me.txtPurchaseOrderNo.Name = "txtPurchaseOrderNo"
        Me.txtPurchaseOrderNo.Size = New System.Drawing.Size(175, 22)
        Me.txtPurchaseOrderNo.TabIndex = 6
        '
        'frmSearchPurchaseOrder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(845, 421)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgvSearchPO)
        Me.Controls.Add(Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmSearchPurchaseOrder"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Search Purchase Order"
        CType(Me.DS_PURCHASE_ORDER, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator.ResumeLayout(False)
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator.PerformLayout()
        CType(Me.dgvSearchPO, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DS_PURCHASE_ORDER As DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDER
    Friend WithEvents SP_SELECT_PURCHASE_ORDER_HEADERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_SELECT_PURCHASE_ORDER_HEADERTableAdapter As DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.SP_SELECT_PURCHASE_ORDER_HEADERTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.TableAdapterManager
    Friend WithEvents SP_SELECT_PURCHASE_ORDER_HEADERBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents dgvSearchPO As System.Windows.Forms.DataGridView
    Friend WithEvents PURCHASE_ORDER_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PURCHASE_ORDER_NUMBER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PURCHASE_ORDER_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DELIVERY_REQUEST_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VENDOR_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VENDOR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SHIP_TO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_OF_ITEM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GRAND_TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtVendor As System.Windows.Forms.TextBox
    Friend WithEvents txtPurchaseOrderNo As System.Windows.Forms.TextBox
End Class
