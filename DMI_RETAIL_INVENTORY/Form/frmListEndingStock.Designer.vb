﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListEndingStock
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListEndingStock))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dtpPeriod = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmdGenerate = New System.Windows.Forms.Button()
        Me.lblList = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.dvgListEndingStock = New System.Windows.Forms.DataGridView()
        Me.PRODUCT_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TRANSACTION_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QUANTITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRICE_PER_UNIT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SPLISTENDINGSTOCKBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DSLISTENDINGSTOCKBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_LIST_ENDING_STOCK = New DMI_RETAIL_INVENTORY.DS_LIST_ENDING_STOCK()
        Me.SP_LIST_ENDING_STOCKTableAdapter = New DMI_RETAIL_INVENTORY.DS_LIST_ENDING_STOCKTableAdapters.SP_LIST_ENDING_STOCKTableAdapter()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtFilter = New System.Windows.Forms.TextBox()
        Me.cmdReport = New System.Windows.Forms.Button()
        CType(Me.dvgListEndingStock, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SPLISTENDINGSTOCKBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DSLISTENDINGSTOCKBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_LIST_ENDING_STOCK, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dtpPeriod
        '
        Me.dtpPeriod.CustomFormat = "MMMM - yyyy"
        Me.dtpPeriod.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPeriod.Location = New System.Drawing.Point(230, 16)
        Me.dtpPeriod.Name = "dtpPeriod"
        Me.dtpPeriod.Size = New System.Drawing.Size(160, 22)
        Me.dtpPeriod.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(148, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Time Period"
        '
        'cmdGenerate
        '
        Me.cmdGenerate.Image = CType(resources.GetObject("cmdGenerate.Image"), System.Drawing.Image)
        Me.cmdGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdGenerate.Location = New System.Drawing.Point(230, 45)
        Me.cmdGenerate.Name = "cmdGenerate"
        Me.cmdGenerate.Size = New System.Drawing.Size(99, 33)
        Me.cmdGenerate.TabIndex = 13
        Me.cmdGenerate.Text = "Generate"
        Me.cmdGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdGenerate.UseVisualStyleBackColor = True
        '
        'lblList
        '
        Me.lblList.AutoSize = True
        Me.lblList.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblList.Location = New System.Drawing.Point(18, 546)
        Me.lblList.Name = "lblList"
        Me.lblList.Size = New System.Drawing.Size(48, 15)
        Me.lblList.TabIndex = 14
        Me.lblList.Text = "Label2"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(367, 546)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(48, 15)
        Me.lblTotal.TabIndex = 15
        Me.lblTotal.Text = "Label3"
        '
        'dvgListEndingStock
        '
        Me.dvgListEndingStock.AllowUserToAddRows = False
        Me.dvgListEndingStock.AllowUserToDeleteRows = False
        Me.dvgListEndingStock.AutoGenerateColumns = False
        Me.dvgListEndingStock.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dvgListEndingStock.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dvgListEndingStock.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dvgListEndingStock.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PRODUCT_NAME, Me.TRANSACTION_DATE, Me.QUANTITY, Me.PRICE_PER_UNIT})
        Me.dvgListEndingStock.DataSource = Me.SPLISTENDINGSTOCKBindingSource
        Me.dvgListEndingStock.Location = New System.Drawing.Point(12, 132)
        Me.dvgListEndingStock.Name = "dvgListEndingStock"
        Me.dvgListEndingStock.ReadOnly = True
        Me.dvgListEndingStock.RowHeadersWidth = 32
        Me.dvgListEndingStock.Size = New System.Drawing.Size(546, 400)
        Me.dvgListEndingStock.TabIndex = 16
        '
        'PRODUCT_NAME
        '
        Me.PRODUCT_NAME.DataPropertyName = "PRODUCT_NAME"
        Me.PRODUCT_NAME.HeaderText = "Product Name"
        Me.PRODUCT_NAME.Name = "PRODUCT_NAME"
        Me.PRODUCT_NAME.ReadOnly = True
        Me.PRODUCT_NAME.Width = 150
        '
        'TRANSACTION_DATE
        '
        Me.TRANSACTION_DATE.DataPropertyName = "TRANSACTION_DATE"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "dd - MMM - yyyy"
        Me.TRANSACTION_DATE.DefaultCellStyle = DataGridViewCellStyle2
        Me.TRANSACTION_DATE.HeaderText = "Transaction Date"
        Me.TRANSACTION_DATE.Name = "TRANSACTION_DATE"
        Me.TRANSACTION_DATE.ReadOnly = True
        Me.TRANSACTION_DATE.Width = 125
        '
        'QUANTITY
        '
        Me.QUANTITY.DataPropertyName = "QUANTITY"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N0"
        DataGridViewCellStyle3.NullValue = "0"
        Me.QUANTITY.DefaultCellStyle = DataGridViewCellStyle3
        Me.QUANTITY.HeaderText = "Qty."
        Me.QUANTITY.Name = "QUANTITY"
        Me.QUANTITY.ReadOnly = True
        Me.QUANTITY.Width = 90
        '
        'PRICE_PER_UNIT
        '
        Me.PRICE_PER_UNIT.DataPropertyName = "PRICE_PER_UNIT"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.PRICE_PER_UNIT.DefaultCellStyle = DataGridViewCellStyle4
        Me.PRICE_PER_UNIT.HeaderText = "Price per Unit"
        Me.PRICE_PER_UNIT.Name = "PRICE_PER_UNIT"
        Me.PRICE_PER_UNIT.ReadOnly = True
        Me.PRICE_PER_UNIT.Width = 120
        '
        'SPLISTENDINGSTOCKBindingSource
        '
        Me.SPLISTENDINGSTOCKBindingSource.DataMember = "SP_LIST_ENDING_STOCK"
        Me.SPLISTENDINGSTOCKBindingSource.DataSource = Me.DSLISTENDINGSTOCKBindingSource
        '
        'DSLISTENDINGSTOCKBindingSource
        '
        Me.DSLISTENDINGSTOCKBindingSource.DataSource = Me.DS_LIST_ENDING_STOCK
        Me.DSLISTENDINGSTOCKBindingSource.Position = 0
        '
        'DS_LIST_ENDING_STOCK
        '
        Me.DS_LIST_ENDING_STOCK.DataSetName = "DS_LIST_ENDING_STOCK"
        Me.DS_LIST_ENDING_STOCK.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SP_LIST_ENDING_STOCKTableAdapter
        '
        Me.SP_LIST_ENDING_STOCKTableAdapter.ClearBeforeFill = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtFilter)
        Me.GroupBox1.Controls.Add(Me.cmdReport)
        Me.GroupBox1.Controls.Add(Me.dtpPeriod)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cmdGenerate)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(546, 114)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 89)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 15)
        Me.Label2.TabIndex = 27
        Me.Label2.Text = "Product Name"
        '
        'txtFilter
        '
        Me.txtFilter.Location = New System.Drawing.Point(108, 86)
        Me.txtFilter.Name = "txtFilter"
        Me.txtFilter.Size = New System.Drawing.Size(151, 22)
        Me.txtFilter.TabIndex = 26
        '
        'cmdReport
        '
        Me.cmdReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdReport.Image = Global.DMI_RETAIL_INVENTORY.My.Resources.Resources.Printer_small
        Me.cmdReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdReport.Location = New System.Drawing.Point(450, 84)
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(90, 24)
        Me.cmdReport.TabIndex = 22
        Me.cmdReport.Text = "Print [F12]"
        Me.cmdReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.cmdReport.UseVisualStyleBackColor = True
        '
        'frmListEndingStock
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(570, 583)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dvgListEndingStock)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.lblList)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmListEndingStock"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Ending Stock"
        Me.Text = "Ending Stock"
        CType(Me.dvgListEndingStock, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SPLISTENDINGSTOCKBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DSLISTENDINGSTOCKBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_LIST_ENDING_STOCK, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dtpPeriod As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmdGenerate As System.Windows.Forms.Button
    Friend WithEvents lblList As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents dvgListEndingStock As System.Windows.Forms.DataGridView
    Friend WithEvents SPLISTENDINGSTOCKBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DSLISTENDINGSTOCKBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DS_LIST_ENDING_STOCK As DMI_RETAIL_INVENTORY.DS_LIST_ENDING_STOCK
    Friend WithEvents SP_LIST_ENDING_STOCKTableAdapter As DMI_RETAIL_INVENTORY.DS_LIST_ENDING_STOCKTableAdapters.SP_LIST_ENDING_STOCKTableAdapter
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdReport As System.Windows.Forms.Button
    Friend WithEvents PRODUCT_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TRANSACTION_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QUANTITY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRICE_PER_UNIT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtFilter As System.Windows.Forms.TextBox
End Class
