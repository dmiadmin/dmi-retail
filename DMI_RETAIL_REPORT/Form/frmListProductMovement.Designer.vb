﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListProductMovement
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListProductMovement))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.SP_LIST_PRODUCT_MOVEMENTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_PRODUCT_STOCK = New DMI_RETAIL_REPORT.DS_PRODUCT_STOCK()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.dgvPRODUCT_MOVEMENT = New System.Windows.Forms.DataGridView()
        Me.TRANSACTION_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TRANSACTION_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRODUCT_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TRANSACTION_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QUANTITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRICE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WAREHOUSE_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdSearchName = New System.Windows.Forms.PictureBox()
        Me.PRODUCT_CODEComboBox = New System.Windows.Forms.ComboBox()
        Me.SP_LIST_PRODUCT_GOODSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PRODUCT_NAMETextBox = New System.Windows.Forms.TextBox()
        Me.lblProduct = New System.Windows.Forms.Label()
        Me.cmdReport = New System.Windows.Forms.Button()
        Me.lblTo = New System.Windows.Forms.Label()
        Me.lblFrom = New System.Windows.Forms.Label()
        Me.cmdGenerate = New System.Windows.Forms.Button()
        Me.dtpPeriod2 = New System.Windows.Forms.DateTimePicker()
        Me.dtpPeriod1 = New System.Windows.Forms.DateTimePicker()
        Me.SP_LIST_PRODUCT_MOVEMENTTableAdapter = New DMI_RETAIL_REPORT.DS_PRODUCT_STOCKTableAdapters.SP_LIST_PRODUCT_MOVEMENTTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_REPORT.DS_PRODUCT_STOCKTableAdapters.TableAdapterManager()
        Me.SP_LIST_PRODUCT_GOODSTableAdapter = New DMI_RETAIL_REPORT.DS_PRODUCT_STOCKTableAdapters.SP_LIST_PRODUCT_GOODSTableAdapter()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator.SuspendLayout()
        CType(Me.SP_LIST_PRODUCT_MOVEMENTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_PRODUCT_STOCK, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPRODUCT_MOVEMENT, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_LIST_PRODUCT_GOODSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SP_LIST_PRODUCT_MOVEMENTBindingNavigator
        '
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator.AddNewItem = Nothing
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator.BindingSource = Me.SP_LIST_PRODUCT_MOVEMENTBindingSource
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator.DeleteItem = Nothing
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator.Name = "SP_LIST_PRODUCT_MOVEMENTBindingNavigator"
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator.Size = New System.Drawing.Size(790, 25)
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator.TabIndex = 0
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator.Text = "BindingNavigator1"
        '
        'SP_LIST_PRODUCT_MOVEMENTBindingSource
        '
        Me.SP_LIST_PRODUCT_MOVEMENTBindingSource.DataMember = "SP_LIST_PRODUCT_MOVEMENT"
        Me.SP_LIST_PRODUCT_MOVEMENTBindingSource.DataSource = Me.DS_PRODUCT_STOCK
        '
        'DS_PRODUCT_STOCK
        '
        Me.DS_PRODUCT_STOCK.DataSetName = "DS_PRODUCT_STOCK"
        Me.DS_PRODUCT_STOCK.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'dgvPRODUCT_MOVEMENT
        '
        Me.dgvPRODUCT_MOVEMENT.AllowUserToAddRows = False
        Me.dgvPRODUCT_MOVEMENT.AllowUserToDeleteRows = False
        Me.dgvPRODUCT_MOVEMENT.AutoGenerateColumns = False
        Me.dgvPRODUCT_MOVEMENT.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPRODUCT_MOVEMENT.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPRODUCT_MOVEMENT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPRODUCT_MOVEMENT.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.TRANSACTION_NAME, Me.TRANSACTION_NO, Me.PRODUCT_NAME, Me.TRANSACTION_DATE, Me.QUANTITY, Me.PRICE, Me.WAREHOUSE_NAME})
        Me.dgvPRODUCT_MOVEMENT.DataSource = Me.SP_LIST_PRODUCT_MOVEMENTBindingSource
        Me.dgvPRODUCT_MOVEMENT.Location = New System.Drawing.Point(12, 175)
        Me.dgvPRODUCT_MOVEMENT.Name = "dgvPRODUCT_MOVEMENT"
        Me.dgvPRODUCT_MOVEMENT.ReadOnly = True
        Me.dgvPRODUCT_MOVEMENT.RowHeadersWidth = 32
        Me.dgvPRODUCT_MOVEMENT.Size = New System.Drawing.Size(766, 256)
        Me.dgvPRODUCT_MOVEMENT.TabIndex = 2
        '
        'TRANSACTION_NAME
        '
        Me.TRANSACTION_NAME.DataPropertyName = "TRANSACTION_NAME"
        Me.TRANSACTION_NAME.HeaderText = "Transaction Name"
        Me.TRANSACTION_NAME.Name = "TRANSACTION_NAME"
        Me.TRANSACTION_NAME.ReadOnly = True
        Me.TRANSACTION_NAME.Width = 110
        '
        'TRANSACTION_NO
        '
        Me.TRANSACTION_NO.DataPropertyName = "TRANSACTION_NO"
        Me.TRANSACTION_NO.HeaderText = "Transaction No"
        Me.TRANSACTION_NO.Name = "TRANSACTION_NO"
        Me.TRANSACTION_NO.ReadOnly = True
        Me.TRANSACTION_NO.Width = 90
        '
        'PRODUCT_NAME
        '
        Me.PRODUCT_NAME.DataPropertyName = "PRODUCT_NAME"
        Me.PRODUCT_NAME.HeaderText = "Product Name"
        Me.PRODUCT_NAME.Name = "PRODUCT_NAME"
        Me.PRODUCT_NAME.ReadOnly = True
        Me.PRODUCT_NAME.Width = 150
        '
        'TRANSACTION_DATE
        '
        Me.TRANSACTION_DATE.DataPropertyName = "TRANSACTION_DATE"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "dd-MMM-yyyy"
        Me.TRANSACTION_DATE.DefaultCellStyle = DataGridViewCellStyle2
        Me.TRANSACTION_DATE.HeaderText = "Transaction Date"
        Me.TRANSACTION_DATE.Name = "TRANSACTION_DATE"
        Me.TRANSACTION_DATE.ReadOnly = True
        '
        'QUANTITY
        '
        Me.QUANTITY.DataPropertyName = "QUANTITY"
        DataGridViewCellStyle3.Format = "N0"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.QUANTITY.DefaultCellStyle = DataGridViewCellStyle3
        Me.QUANTITY.HeaderText = "Qty."
        Me.QUANTITY.Name = "QUANTITY"
        Me.QUANTITY.ReadOnly = True
        Me.QUANTITY.Width = 50
        '
        'PRICE
        '
        Me.PRICE.DataPropertyName = "PRICE"
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.PRICE.DefaultCellStyle = DataGridViewCellStyle4
        Me.PRICE.HeaderText = "Price"
        Me.PRICE.Name = "PRICE"
        Me.PRICE.ReadOnly = True
        Me.PRICE.Width = 90
        '
        'WAREHOUSE_NAME
        '
        Me.WAREHOUSE_NAME.DataPropertyName = "WAREHOUSE_NAME"
        Me.WAREHOUSE_NAME.HeaderText = "Warehouse"
        Me.WAREHOUSE_NAME.Name = "WAREHOUSE_NAME"
        Me.WAREHOUSE_NAME.ReadOnly = True
        Me.WAREHOUSE_NAME.Width = 120
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdSearchName)
        Me.GroupBox2.Controls.Add(Me.PRODUCT_CODEComboBox)
        Me.GroupBox2.Controls.Add(Me.PRODUCT_NAMETextBox)
        Me.GroupBox2.Controls.Add(Me.lblProduct)
        Me.GroupBox2.Controls.Add(Me.cmdReport)
        Me.GroupBox2.Controls.Add(Me.lblTo)
        Me.GroupBox2.Controls.Add(Me.lblFrom)
        Me.GroupBox2.Controls.Add(Me.cmdGenerate)
        Me.GroupBox2.Controls.Add(Me.dtpPeriod2)
        Me.GroupBox2.Controls.Add(Me.dtpPeriod1)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 28)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(766, 141)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        '
        'cmdSearchName
        '
        Me.cmdSearchName.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchName.Image = CType(resources.GetObject("cmdSearchName.Image"), System.Drawing.Image)
        Me.cmdSearchName.Location = New System.Drawing.Point(558, 20)
        Me.cmdSearchName.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchName.Name = "cmdSearchName"
        Me.cmdSearchName.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearchName.TabIndex = 22
        Me.cmdSearchName.TabStop = False
        '
        'PRODUCT_CODEComboBox
        '
        Me.PRODUCT_CODEComboBox.DataSource = Me.SP_LIST_PRODUCT_GOODSBindingSource
        Me.PRODUCT_CODEComboBox.DisplayMember = "PRODUCT_CODE"
        Me.PRODUCT_CODEComboBox.FormattingEnabled = True
        Me.PRODUCT_CODEComboBox.Location = New System.Drawing.Point(241, 21)
        Me.PRODUCT_CODEComboBox.Name = "PRODUCT_CODEComboBox"
        Me.PRODUCT_CODEComboBox.Size = New System.Drawing.Size(109, 23)
        Me.PRODUCT_CODEComboBox.TabIndex = 20
        Me.PRODUCT_CODEComboBox.Tag = "M"
        Me.PRODUCT_CODEComboBox.ValueMember = "PRODUCT_ID"
        '
        'SP_LIST_PRODUCT_GOODSBindingSource
        '
        Me.SP_LIST_PRODUCT_GOODSBindingSource.DataMember = "SP_LIST_PRODUCT_GOODS"
        Me.SP_LIST_PRODUCT_GOODSBindingSource.DataSource = Me.DS_PRODUCT_STOCK
        '
        'PRODUCT_NAMETextBox
        '
        Me.PRODUCT_NAMETextBox.Location = New System.Drawing.Point(356, 21)
        Me.PRODUCT_NAMETextBox.Name = "PRODUCT_NAMETextBox"
        Me.PRODUCT_NAMETextBox.ReadOnly = True
        Me.PRODUCT_NAMETextBox.Size = New System.Drawing.Size(195, 22)
        Me.PRODUCT_NAMETextBox.TabIndex = 21
        '
        'lblProduct
        '
        Me.lblProduct.Location = New System.Drawing.Point(147, 24)
        Me.lblProduct.Name = "lblProduct"
        Me.lblProduct.Size = New System.Drawing.Size(83, 21)
        Me.lblProduct.TabIndex = 17
        Me.lblProduct.Text = "Product"
        Me.lblProduct.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cmdReport
        '
        Me.cmdReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdReport.Image = Global.DMI_RETAIL_REPORT.My.Resources.Resources.Printer_small
        Me.cmdReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdReport.Location = New System.Drawing.Point(670, 111)
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(90, 24)
        Me.cmdReport.TabIndex = 16
        Me.cmdReport.Text = "Print [F12]"
        Me.cmdReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.cmdReport.UseVisualStyleBackColor = True
        '
        'lblTo
        '
        Me.lblTo.AutoSize = True
        Me.lblTo.Location = New System.Drawing.Point(392, 59)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(23, 15)
        Me.lblTo.TabIndex = 8
        Me.lblTo.Text = "To"
        '
        'lblFrom
        '
        Me.lblFrom.AutoSize = True
        Me.lblFrom.Location = New System.Drawing.Point(193, 59)
        Me.lblFrom.Name = "lblFrom"
        Me.lblFrom.Size = New System.Drawing.Size(44, 15)
        Me.lblFrom.TabIndex = 7
        Me.lblFrom.Text = "Period"
        '
        'cmdGenerate
        '
        Me.cmdGenerate.Image = CType(resources.GetObject("cmdGenerate.Image"), System.Drawing.Image)
        Me.cmdGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdGenerate.Location = New System.Drawing.Point(338, 90)
        Me.cmdGenerate.Name = "cmdGenerate"
        Me.cmdGenerate.Size = New System.Drawing.Size(99, 39)
        Me.cmdGenerate.TabIndex = 5
        Me.cmdGenerate.Text = "Generate"
        Me.cmdGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdGenerate.UseVisualStyleBackColor = True
        '
        'dtpPeriod2
        '
        Me.dtpPeriod2.CustomFormat = "dd-MMM-yyyy"
        Me.dtpPeriod2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPeriod2.Location = New System.Drawing.Point(423, 56)
        Me.dtpPeriod2.Name = "dtpPeriod2"
        Me.dtpPeriod2.Size = New System.Drawing.Size(128, 22)
        Me.dtpPeriod2.TabIndex = 4
        '
        'dtpPeriod1
        '
        Me.dtpPeriod1.CustomFormat = "dd-MMM-yyyy"
        Me.dtpPeriod1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPeriod1.Location = New System.Drawing.Point(241, 56)
        Me.dtpPeriod1.Name = "dtpPeriod1"
        Me.dtpPeriod1.Size = New System.Drawing.Size(128, 22)
        Me.dtpPeriod1.TabIndex = 3
        '
        'SP_LIST_PRODUCT_MOVEMENTTableAdapter
        '
        Me.SP_LIST_PRODUCT_MOVEMENTTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.PRODUCTTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_REPORT.DS_PRODUCT_STOCKTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'SP_LIST_PRODUCT_GOODSTableAdapter
        '
        Me.SP_LIST_PRODUCT_GOODSTableAdapter.ClearBeforeFill = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(38, 438)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 15)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Label1"
        '
        'frmListProductMovement
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(790, 468)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.dgvPRODUCT_MOVEMENT)
        Me.Controls.Add(Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmListProductMovement"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "List Product Movement"
        Me.Text = "List Product Movement"
        CType(Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator.ResumeLayout(False)
        Me.SP_LIST_PRODUCT_MOVEMENTBindingNavigator.PerformLayout()
        CType(Me.SP_LIST_PRODUCT_MOVEMENTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_PRODUCT_STOCK, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPRODUCT_MOVEMENT, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_LIST_PRODUCT_GOODSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DS_PRODUCT_STOCK As DMI_RETAIL_REPORT.DS_PRODUCT_STOCK
    Friend WithEvents SP_LIST_PRODUCT_MOVEMENTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_LIST_PRODUCT_MOVEMENTTableAdapter As DMI_RETAIL_REPORT.DS_PRODUCT_STOCKTableAdapters.SP_LIST_PRODUCT_MOVEMENTTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_REPORT.DS_PRODUCT_STOCKTableAdapters.TableAdapterManager
    Friend WithEvents SP_LIST_PRODUCT_MOVEMENTBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents dgvPRODUCT_MOVEMENT As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdSearchName As System.Windows.Forms.PictureBox
    Friend WithEvents PRODUCT_CODEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents PRODUCT_NAMETextBox As System.Windows.Forms.TextBox
    Friend WithEvents lblProduct As System.Windows.Forms.Label
    Friend WithEvents cmdReport As System.Windows.Forms.Button
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents lblFrom As System.Windows.Forms.Label
    Friend WithEvents cmdGenerate As System.Windows.Forms.Button
    Friend WithEvents dtpPeriod2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpPeriod1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents SP_LIST_PRODUCT_GOODSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_LIST_PRODUCT_GOODSTableAdapter As DMI_RETAIL_REPORT.DS_PRODUCT_STOCKTableAdapters.SP_LIST_PRODUCT_GOODSTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TRANSACTION_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TRANSACTION_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRODUCT_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TRANSACTION_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QUANTITY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRICE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WAREHOUSE_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
