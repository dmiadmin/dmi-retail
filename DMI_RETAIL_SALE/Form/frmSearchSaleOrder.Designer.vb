﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSearchSaleOrder
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSearchSaleOrder))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.SP_SELECT_SALE_ORDER_HEADERDataGridView = New System.Windows.Forms.DataGridView()
        Me.NO_OF_ITEM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtSaleOrderNo = New System.Windows.Forms.TextBox()
        Me.txtCustomer = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.SALE_ORDER_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALE_ORDER_NUMBER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALE_ORDER_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CUSTOMER_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CUSTOMER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SHIP_TO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DELIVERY_REQUEST_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALESMAN_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALESMAN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GRAND_TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SP_SELECT_SALE_ORDER_HEADERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_SALE_ORDER = New DMI_RETAIL_SALE.DS_SALE_ORDER()
        Me.SP_SELECT_SALE_ORDER_HEADERTableAdapter = New DMI_RETAIL_SALE.DS_SALE_ORDERTableAdapters.SP_SELECT_SALE_ORDER_HEADERTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_SALE.DS_SALE_ORDERTableAdapters.TableAdapterManager()
        CType(Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator.SuspendLayout()
        CType(Me.SP_SELECT_SALE_ORDER_HEADERDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.SP_SELECT_SALE_ORDER_HEADERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_SALE_ORDER, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SP_SELECT_SALE_ORDER_HEADERBindingNavigator
        '
        Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator.AddNewItem = Nothing
        Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator.BindingSource = Me.SP_SELECT_SALE_ORDER_HEADERBindingSource
        Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator.DeleteItem = Nothing
        Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator.Name = "SP_SELECT_SALE_ORDER_HEADERBindingNavigator"
        Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator.Size = New System.Drawing.Size(885, 25)
        Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator.TabIndex = 0
        Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'SP_SELECT_SALE_ORDER_HEADERDataGridView
        '
        Me.SP_SELECT_SALE_ORDER_HEADERDataGridView.AllowUserToAddRows = False
        Me.SP_SELECT_SALE_ORDER_HEADERDataGridView.AllowUserToDeleteRows = False
        Me.SP_SELECT_SALE_ORDER_HEADERDataGridView.AutoGenerateColumns = False
        Me.SP_SELECT_SALE_ORDER_HEADERDataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SP_SELECT_SALE_ORDER_HEADERDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.SP_SELECT_SALE_ORDER_HEADERDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.SP_SELECT_SALE_ORDER_HEADERDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SALE_ORDER_ID, Me.SALE_ORDER_NUMBER, Me.SALE_ORDER_DATE, Me.CUSTOMER_ID, Me.CUSTOMER, Me.NO_OF_ITEM, Me.SHIP_TO, Me.DELIVERY_REQUEST_DATE, Me.SALESMAN_ID, Me.SALESMAN, Me.GRAND_TOTAL})
        Me.SP_SELECT_SALE_ORDER_HEADERDataGridView.DataSource = Me.SP_SELECT_SALE_ORDER_HEADERBindingSource
        Me.SP_SELECT_SALE_ORDER_HEADERDataGridView.Location = New System.Drawing.Point(13, 15)
        Me.SP_SELECT_SALE_ORDER_HEADERDataGridView.Name = "SP_SELECT_SALE_ORDER_HEADERDataGridView"
        Me.SP_SELECT_SALE_ORDER_HEADERDataGridView.ReadOnly = True
        Me.SP_SELECT_SALE_ORDER_HEADERDataGridView.Size = New System.Drawing.Size(851, 220)
        Me.SP_SELECT_SALE_ORDER_HEADERDataGridView.TabIndex = 1
        '
        'NO_OF_ITEM
        '
        Me.NO_OF_ITEM.DataPropertyName = "NO_OF_ITEM"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.NO_OF_ITEM.DefaultCellStyle = DataGridViewCellStyle3
        Me.NO_OF_ITEM.HeaderText = "No Of Item"
        Me.NO_OF_ITEM.Name = "NO_OF_ITEM"
        Me.NO_OF_ITEM.ReadOnly = True
        Me.NO_OF_ITEM.Width = 50
        '
        'txtSaleOrderNo
        '
        Me.txtSaleOrderNo.Location = New System.Drawing.Point(161, 21)
        Me.txtSaleOrderNo.Name = "txtSaleOrderNo"
        Me.txtSaleOrderNo.Size = New System.Drawing.Size(175, 22)
        Me.txtSaleOrderNo.TabIndex = 2
        '
        'txtCustomer
        '
        Me.txtCustomer.Location = New System.Drawing.Point(161, 51)
        Me.txtCustomer.Name = "txtCustomer"
        Me.txtCustomer.Size = New System.Drawing.Size(175, 22)
        Me.txtCustomer.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(18, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 15)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Sale Order Number"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(18, 54)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(103, 15)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Customer Name"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.SP_SELECT_SALE_ORDER_HEADERDataGridView)
        Me.GroupBox1.Location = New System.Drawing.Point(5, 110)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(876, 244)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.txtCustomer)
        Me.GroupBox2.Controls.Add(Me.txtSaleOrderNo)
        Me.GroupBox2.Location = New System.Drawing.Point(5, 28)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(875, 82)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.TabStop = False
        '
        'SALE_ORDER_ID
        '
        Me.SALE_ORDER_ID.DataPropertyName = "SALE_ORDER_ID"
        Me.SALE_ORDER_ID.HeaderText = "SALE_ORDER_ID"
        Me.SALE_ORDER_ID.Name = "SALE_ORDER_ID"
        Me.SALE_ORDER_ID.ReadOnly = True
        Me.SALE_ORDER_ID.Visible = False
        '
        'SALE_ORDER_NUMBER
        '
        Me.SALE_ORDER_NUMBER.DataPropertyName = "SALE_ORDER_NUMBER"
        Me.SALE_ORDER_NUMBER.HeaderText = "Sale Order Number"
        Me.SALE_ORDER_NUMBER.Name = "SALE_ORDER_NUMBER"
        Me.SALE_ORDER_NUMBER.ReadOnly = True
        '
        'SALE_ORDER_DATE
        '
        Me.SALE_ORDER_DATE.DataPropertyName = "SALE_ORDER_DATE"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "dd-MMM-yyyy"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.SALE_ORDER_DATE.DefaultCellStyle = DataGridViewCellStyle2
        Me.SALE_ORDER_DATE.HeaderText = "Sale Order Date"
        Me.SALE_ORDER_DATE.Name = "SALE_ORDER_DATE"
        Me.SALE_ORDER_DATE.ReadOnly = True
        '
        'CUSTOMER_ID
        '
        Me.CUSTOMER_ID.DataPropertyName = "CUSTOMER_ID"
        Me.CUSTOMER_ID.HeaderText = "CUSTOMER_ID"
        Me.CUSTOMER_ID.Name = "CUSTOMER_ID"
        Me.CUSTOMER_ID.ReadOnly = True
        Me.CUSTOMER_ID.Visible = False
        '
        'CUSTOMER
        '
        Me.CUSTOMER.DataPropertyName = "CUSTOMER"
        Me.CUSTOMER.HeaderText = "Customer"
        Me.CUSTOMER.Name = "CUSTOMER"
        Me.CUSTOMER.ReadOnly = True
        '
        'SHIP_TO
        '
        Me.SHIP_TO.DataPropertyName = "SHIP_TO"
        Me.SHIP_TO.HeaderText = "Ship To"
        Me.SHIP_TO.Name = "SHIP_TO"
        Me.SHIP_TO.ReadOnly = True
        Me.SHIP_TO.Width = 175
        '
        'DELIVERY_REQUEST_DATE
        '
        Me.DELIVERY_REQUEST_DATE.DataPropertyName = "DELIVERY_REQUEST_DATE"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "dd-MMM-yyyy"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.DELIVERY_REQUEST_DATE.DefaultCellStyle = DataGridViewCellStyle4
        Me.DELIVERY_REQUEST_DATE.HeaderText = "Delivery Request Date"
        Me.DELIVERY_REQUEST_DATE.Name = "DELIVERY_REQUEST_DATE"
        Me.DELIVERY_REQUEST_DATE.ReadOnly = True
        '
        'SALESMAN_ID
        '
        Me.SALESMAN_ID.DataPropertyName = "SALESMAN_ID"
        Me.SALESMAN_ID.HeaderText = "SALESMAN_ID"
        Me.SALESMAN_ID.Name = "SALESMAN_ID"
        Me.SALESMAN_ID.ReadOnly = True
        Me.SALESMAN_ID.Visible = False
        '
        'SALESMAN
        '
        Me.SALESMAN.DataPropertyName = "SALESMAN"
        Me.SALESMAN.HeaderText = "Salesman"
        Me.SALESMAN.Name = "SALESMAN"
        Me.SALESMAN.ReadOnly = True
        Me.SALESMAN.Width = 80
        '
        'GRAND_TOTAL
        '
        Me.GRAND_TOTAL.DataPropertyName = "GRAND_TOTAL"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N0"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.GRAND_TOTAL.DefaultCellStyle = DataGridViewCellStyle5
        Me.GRAND_TOTAL.HeaderText = "Grand Total"
        Me.GRAND_TOTAL.Name = "GRAND_TOTAL"
        Me.GRAND_TOTAL.ReadOnly = True
        '
        'SP_SELECT_SALE_ORDER_HEADERBindingSource
        '
        Me.SP_SELECT_SALE_ORDER_HEADERBindingSource.DataMember = "SP_SELECT_SALE_ORDER_HEADER"
        Me.SP_SELECT_SALE_ORDER_HEADERBindingSource.DataSource = Me.DS_SALE_ORDER
        '
        'DS_SALE_ORDER
        '
        Me.DS_SALE_ORDER.DataSetName = "DS_SALE_ORDER"
        Me.DS_SALE_ORDER.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SP_SELECT_SALE_ORDER_HEADERTableAdapter
        '
        Me.SP_SELECT_SALE_ORDER_HEADERTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.SALE_ORDER_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.SALE_ORDER_HEADERTableAdapter = Nothing
        Me.TableAdapterManager.SALESMANTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_SALE.DS_SALE_ORDERTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'frmSearchSaleOrder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(885, 375)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmSearchSaleOrder"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Search Sale Order"
        CType(Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator.ResumeLayout(False)
        Me.SP_SELECT_SALE_ORDER_HEADERBindingNavigator.PerformLayout()
        CType(Me.SP_SELECT_SALE_ORDER_HEADERDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.SP_SELECT_SALE_ORDER_HEADERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_SALE_ORDER, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DS_SALE_ORDER As DMI_RETAIL_SALE.DS_SALE_ORDER
    Friend WithEvents SP_SELECT_SALE_ORDER_HEADERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_SELECT_SALE_ORDER_HEADERTableAdapter As DMI_RETAIL_SALE.DS_SALE_ORDERTableAdapters.SP_SELECT_SALE_ORDER_HEADERTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_SALE.DS_SALE_ORDERTableAdapters.TableAdapterManager
    Friend WithEvents SP_SELECT_SALE_ORDER_HEADERBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SP_SELECT_SALE_ORDER_HEADERDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents txtSaleOrderNo As System.Windows.Forms.TextBox
    Friend WithEvents txtCustomer As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents SALE_ORDER_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALE_ORDER_NUMBER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALE_ORDER_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CUSTOMER_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CUSTOMER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_OF_ITEM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SHIP_TO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DELIVERY_REQUEST_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALESMAN_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALESMAN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GRAND_TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
