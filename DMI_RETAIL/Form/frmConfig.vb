﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Data

Public Class frmConfig
    Dim tmpFormLoaded, tmpChange As Boolean

    Private Sub frmConfig_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If tmpChange Then
            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah anda ingin menyimpan perubahan yang dilakukan ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do you want to save the changes ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If
            If tmpvar = 6 Then
                Button1_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub frmConfig_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ''''' Tab General '''''
        cmbLanguage.Text = Language
        'txtMaxPass.Text = PARAMETERTableAdapter.SP_SELECT_PARAMETER("MAX WRONG PASSWORD")
        'txtSafeStockInterval.Text = PARAMETERTableAdapter.SP_SELECT_PARAMETER("SAFE STOCK CHECKING INTERVAL")

        ''''' Tab Database '''''
        'txtServer.Text = ServerName
        'txtDatabaseName.Text = DatabaseName
        'txtPathBack.Text = PARAMETERTableAdapter.SP_SELECT_PARAMETER("PATH BACK UP")

        ''''' Tab Purchase '''''
        'If PARAMETERTableAdapter.SP_SELECT_PARAMETER("PURCHASE PAYMENT METHOD") = "0" Then
        '    PURCHASE_PAYMENT_METHOD_NAMEComboBox.SelectedIndex = -1
        'Else
        '    PURCHASE_PAYMENT_METHODBindingSource.Position = _
        '        PURCHASE_PAYMENT_METHODBindingSource.Find("PAYMENT_METHOD_ID", _
        '                                                  PARAMETERTableAdapter.SP_SELECT_PARAMETER("PURCHASE PAYMENT METHOD"))
        'End If
        'If PARAMETERTableAdapter.SP_SELECT_PARAMETER("PURCHASE WAREHOUSE") = "0" Then
        '    PURCHASE_WAREHOUSE_NAMEComboBox.SelectedIndex = -1
        'Else
        '    PURCHASE_WAREHOUSEBindingSource.Position = _
        '        PURCHASE_WAREHOUSEBindingSource.Find("WAREHOUSE_ID", _
        '                                             PARAMETERTableAdapter.SP_SELECT_PARAMETER("PURCHASE WAREHOUSE"))
        'End If
        'If PARAMETERTableAdapter.SP_SELECT_PARAMETER("PURCHASE VENDOR") = "0" Then
        '    VENDOR_NAMEComboBox.SelectedIndex = -1
        'Else
        '    VENDORBindingSource.Position = _
        '        VENDORBindingSource.Find("VENDOR_ID", PARAMETERTableAdapter.SP_SELECT_PARAMETER("PURCHASE VENDOR"))
        'End If

        ''''' Tab Sale '''''
        'If PARAMETERTableAdapter.SP_SELECT_PARAMETER("SALE PAYMENT METHOD") = "0" Then
        '    SALE_PAYMENT_METHOD_NAMEComboBox.SelectedIndex = -1
        'Else
        '    SALE_PAYMENT_METHODBindingSource.Position = _
        '        SALE_PAYMENT_METHODBindingSource.Find("PAYMENT_METHOD_ID", _
        '                                              PARAMETERTableAdapter.SP_SELECT_PARAMETER("SALE PAYMENT METHOD"))
        'End If
        'If PARAMETERTableAdapter.SP_SELECT_PARAMETER("SALE WAREHOUSE") = "0" Then
        '    SALE_WAREHOUSE_NAMEComboBox.SelectedIndex = -1
        'Else
        '    SALE_WAREHOUSEBindingSource.Position = _
        '        SALE_WAREHOUSEBindingSource.Find("WAREHOUSE_ID", _
        '                                         PARAMETERTableAdapter.SP_SELECT_PARAMETER("SALE WAREHOUSE"))
        'End If
        'If PARAMETERTableAdapter.SP_SELECT_PARAMETER("SALE CUSTOMER") = "0" Then
        '    CUSTOMER_NAMEComboBox.SelectedIndex = -1
        'Else
        '    CUSTOMERBindingSource.Position = _
        '        CUSTOMERBindingSource.Find("CUSTOMER_ID", PARAMETERTableAdapter.SP_SELECT_PARAMETER("SALE CUSTOMER"))
        'End If

        'If PARAMETERTableAdapter.SP_SELECT_PARAMETER("SALESMAN") = "0" Then
        '    SALESMAN_NAMEComboBox.SelectedIndex = -1
        'Else
        '    SALESMANBindingSource.Position = _
        '        SALESMANBindingSource.Find("SALESMAN_ID", PARAMETERTableAdapter.SP_SELECT_PARAMETER("SALESMAN"))
        'End If

        'If PARAMETERTableAdapter.SP_SELECT_PARAMETER("SALE_COLUMN_PRICE") = "True" Then
        '    chkLockPrice.Checked = True
        'Else
        '    chkLockPrice.Checked = False
        'End If

        ' ''''' Tab Inventory '''''
        'chkAutoProduct.Checked = PARAMETERTableAdapter.SP_SELECT_PARAMETER("AUTO PRODUCT MOVEMENT")
        'chkAutoSplit.Checked = PARAMETERTableAdapter.SP_SELECT_PARAMETER("AUTO SPLIT PRODUCT")

        ' ''''' Tab Product Distribution '''''
        'If PARAMETERTableAdapter.SP_SELECT_PARAMETER("PD WAREHOUSE SOURCE") = "0" Then
        '    PD_SOURCE_WAREHOUSE_NAMEComboBox.SelectedIndex = -1
        'Else
        '    NEW_PD_SOURCE_WAREHOUSEBindingSource.Position = _
        '       NEW_PD_SOURCE_WAREHOUSEBindingSource.Find("WAREHOUSE_ID", _
        '                                              PARAMETERTableAdapter.SP_SELECT_PARAMETER("PD WAREHOUSE SOURCE"))
        'End If
        'If PARAMETERTableAdapter.SP_SELECT_PARAMETER("PD WAREHOUSE DESTINATION") = "0" Then
        '    PD_DESTINATION_WAREHOUSE_NAMEComboBox.SelectedIndex = -1
        'Else
        '    PD_DESTINATION_WAREHOUSEBindingSource.Position = _
        '        PD_DESTINATION_WAREHOUSEBindingSource.Find("WAREHOUSE_ID", _
        '                                              PARAMETERTableAdapter.SP_SELECT_PARAMETER("PD WAREHOUSE DESTINATION"))
        'End If

        'If PARAMETERTableAdapter.SP_SELECT_PARAMETER("RETURN SALE ACCOUNT") = "0" Then
        '    ACCOUNT_NAMELabel2.Text = ""
        '    cmbAcountNumberSR.SelectedValue = -1
        '    ACCOUNT_TYPELabel2.Text = ""
        'Else
        '    ACCOUNTBindingSource1.Position = ACCOUNTBindingSource1.Find("ACCOUNT_ID", _
        '                                                                    PARAMETERTableAdapter.SP_SELECT_PARAMETER("RETURN SALE ACCOUNT"))
        'End If

        'If PARAMETERTableAdapter.SP_SELECT_PARAMETER("RETURN PURCHASE ACCOUNT") = "0" Then
        '    ACCOUNT_NAMELabel1.Text = ""
        '    cmbAccountNumberPR.SelectedValue = -1
        '    ACCOUNT_TYPELabel1.Text = ""
        'Else
        '    ACCOUNTBindingSource.Position = ACCOUNTBindingSource.Find("ACCOUNT_ID", _
        '                                                                PARAMETERTableAdapter.SP_SELECT_PARAMETER("RETURN PURCHASE ACCOUNT"))
        'End If

        '''''Tab Document Code''''''
        dgvDocCode.Rows.Add(9)
        dgvDocCode.Item(0, 0).Value = "Purchase Invoice"
        dgvDocCode.Item(0, 1).Value = "Sale Invoice"
        dgvDocCode.Item(0, 2).Value = "Purchase Return"
        dgvDocCode.Item(0, 3).Value = "Sale Return"
        dgvDocCode.Item(0, 4).Value = "Adustment"
        dgvDocCode.Item(0, 5).Value = "Product Distribution"
        dgvDocCode.Item(0, 6).Value = "Opening Balance"
        dgvDocCode.Item(0, 7).Value = "Payable Payment"
        dgvDocCode.Item(0, 8).Value = "Receivable Payment"

        xComm = New SqlCommand("SELECT PREFIX FROM DOCUMENT_CODE WHERE DOCUMENT_TYPE = 'PURCHASE_INVOICE'", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdReader = xComm.ExecuteReader()
        xdReader.Read()
        dgvDocCode.Item(1, 0).Value = xdReader.Item(0).ToString
        xdReader.Close()

        xComm = New SqlCommand("SELECT PREFIX FROM DOCUMENT_CODE WHERE DOCUMENT_TYPE = 'SALE_INVOICE'", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdReader = xComm.ExecuteReader()
        xdReader.Read()
        dgvDocCode.Item(1, 1).Value = xdReader.Item(0).ToString
        xdReader.Close()

        xComm = New SqlCommand("SELECT PREFIX FROM DOCUMENT_CODE WHERE DOCUMENT_TYPE = 'PURCHASE_RETURN'", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdReader = xComm.ExecuteReader()
        xdReader.Read()
        dgvDocCode.Item(1, 2).Value = xdReader.Item(0).ToString
        xdReader.Close()

        xComm = New SqlCommand("SELECT PREFIX FROM DOCUMENT_CODE WHERE DOCUMENT_TYPE = 'SALE_RETURN'", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdReader = xComm.ExecuteReader()
        xdReader.Read()
        dgvDocCode.Item(1, 3).Value = xdReader.Item(0).ToString
        xdReader.Close()

        xComm = New SqlCommand("SELECT PREFIX FROM DOCUMENT_CODE WHERE DOCUMENT_TYPE = 'ADJUSTMENT'", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdReader = xComm.ExecuteReader()
        xdReader.Read()
        dgvDocCode.Item(1, 4).Value = xdReader.Item(0).ToString
        xdReader.Close()

        xComm = New SqlCommand("SELECT PREFIX FROM DOCUMENT_CODE WHERE DOCUMENT_TYPE = 'PRODUCT_DISTRIBUTION'", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdReader = xComm.ExecuteReader()
        xdReader.Read()
        dgvDocCode.Item(1, 5).Value = xdReader.Item(0).ToString
        xdReader.Close()

        xComm = New SqlCommand("SELECT PREFIX FROM DOCUMENT_CODE WHERE DOCUMENT_TYPE = 'OPENING_BALANCE'", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdReader = xComm.ExecuteReader()
        xdReader.Read()
        dgvDocCode.Item(1, 6).Value = xdReader.Item(0).ToString
        xdReader.Close()

        xComm = New SqlCommand("SELECT PREFIX FROM DOCUMENT_CODE WHERE DOCUMENT_TYPE = 'PAYABLE_PAYMENT'", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdReader = xComm.ExecuteReader()
        xdReader.Read()
        dgvDocCode.Item(1, 7).Value = xdReader.Item(0).ToString
        xdReader.Close()

        xComm = New SqlCommand("SELECT PREFIX FROM DOCUMENT_CODE WHERE DOCUMENT_TYPE = 'RECEIVABLE_PAYMENT'", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdReader = xComm.ExecuteReader()
        xdReader.Read()
        dgvDocCode.Item(1, 8).Value = xdReader.Item(0).ToString
        xdReader.Close()

        ''''Tab Master Data Code''''
        dgvMasterDataCode.Rows.Add(6)
        dgvMasterDataCode.Item(0, 0).Value = "Customer"
        dgvMasterDataCode.Item(0, 1).Value = "Vendor"
        dgvMasterDataCode.Item(0, 2).Value = "Salesman"
        dgvMasterDataCode.Item(0, 3).Value = "Location"
        dgvMasterDataCode.Item(0, 4).Value = "Warehouse"
        dgvMasterDataCode.Item(0, 5).Value = "Product"

        xComm = New SqlCommand("SELECT PREFIX FROM DOCUMENT_CODE WHERE DOCUMENT_TYPE = 'CUSTOMER'", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdReader = xComm.ExecuteReader()
        xdReader.Read()
        dgvMasterDataCode.Item(1, 0).Value = xdReader.Item(0).ToString
        xdReader.Close()

        xComm = New SqlCommand("SELECT PREFIX FROM DOCUMENT_CODE WHERE DOCUMENT_TYPE = 'VENDOR'", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdReader = xComm.ExecuteReader()
        xdReader.Read()
        dgvMasterDataCode.Item(1, 1).Value = xdReader.Item(0).ToString
        xdReader.Close()

        xComm = New SqlCommand("SELECT PREFIX FROM DOCUMENT_CODE WHERE DOCUMENT_TYPE = 'SALESMAN'", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdReader = xComm.ExecuteReader()
        xdReader.Read()
        dgvMasterDataCode.Item(1, 2).Value = xdReader.Item(0).ToString
        xdReader.Close()

        xComm = New SqlCommand("SELECT PREFIX FROM DOCUMENT_CODE WHERE DOCUMENT_TYPE = 'LOCATION'", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdReader = xComm.ExecuteReader()
        xdReader.Read()
        dgvMasterDataCode.Item(1, 3).Value = xdReader.Item(0).ToString
        xdReader.Close()

        xComm = New SqlCommand("SELECT PREFIX FROM DOCUMENT_CODE WHERE DOCUMENT_TYPE = 'WAREHOUSE'", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdReader = xComm.ExecuteReader()
        xdReader.Read()
        dgvMasterDataCode.Item(1, 4).Value = xdReader.Item(0).ToString
        xdReader.Close()

        xComm = New SqlCommand("SELECT PREFIX FROM DOCUMENT_CODE WHERE DOCUMENT_TYPE = 'PRODUCT'", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdReader = xComm.ExecuteReader()
        xdReader.Read()
        dgvMasterDataCode.Item(1, 5).Value = xdReader.Item(0).ToString
        xdReader.Close()


        ''''Tab Product Category''''
        'If PARAMETERTableAdapter.SP_SELECT_PARAMETER("PRODUCT CATEGORY") = "0" Then
        '    cmbProductCategory.Text = ""
        'Else
        '    cmbProductCategory.Text = PARAMETERTableAdapter.SP_SELECT_PARAMETER("PRODUCT CATEGORY")
        'End If

        If Language = "Indonesian" Then
            Me.Text = "Konfigurasi"
            chkLockPrice.Text = "Kunci Harga Jual"
            TabControl1.TabPages(0).Text = "Umum"
            TabControl1.TabPages(1).Text = "Pembelian"
            TabControl1.TabPages(2).Text = "Penjualan"
            TabControl1.TabPages(3).Text = "Persediaan"
            TabControl1.TabPages(4).Text = "Distribusi Produk"
            TabControl1.TabPages(5).Text = "Kode Dokumen"
            TabControl1.TabPages(6).Text = "Kategori Produk"
            TabControl1.TabPages(7).Text = "Kode Data Master"
            'TabControl1.TabPages(8).Text = "Kode Data Master"

            ''''' Tab General '''''
            Label1.Text = "Bahasa"
            Label11.Text = "Maks. Kesalahan Password"
            lblSafeStockInterval.Text = "Waktu Pengecekan Stok Aman"
            lblSafeStockMinute.Text = "Menit"

            ''''' Tab Database '''''
            'Label2.Text = "Nama Server"
            'Label9.Text = "Nama Database"
            'Label3.Text = "Lokasi Folder Backup"

            ''''' Tab Purchase '''''
            Label4.Text = "Metode Pembayaran"
            Label5.Text = "Gudang"
            Label6.Text = "Supplier"

            ''''' Tab Sale '''''
            Label7.Text = "Metode Pembayaran"
            WAREHOUSE_NAMELabel.Text = "Gudang"
            Label8.Text = "Pelanggan"
            Label13.Text = "Sales"

            ''''' Tab Inventory '''''
            chkAutoProduct.Text = "Perpindahan Produk Otomatis"
            chkAutoSplit.Text = "Pemecahan Produk Otomatis"

            ''''' Tab Product Distribution '''''
            Label10.Text = "Gudang Asal"
            Label12.Text = "Gudang Tujuan"

            '''''Tab Document Code'''''
            dgvDocCode.Columns("DOCUMENT").HeaderText = "Dokumen"
            dgvDocCode.Columns("CODE").HeaderText = "Kode"

            cmdUndo.Text = "Batal"
            cmdAdd.Text = "Tambah"
            cmdEdit.Text = "Ubah"
            Button1.Text = "Simpan"
            cmdDelete.Text = "Hapus"

            ''''Tab Product Category'''''
            Label14.Text = "Kategori Produk"

            ''''Tab Return''''
            lblNameAccountRP.Text = "Nama Akun"
            lblNoAccountRP.Text = "Nomor Akun"
            lblTypeAccountRP.Text = "Tipe Akun"
            lblNameAccountRS.Text = "Nama Akun"
            lblNoAccountRS.Text = "Nomor Akun"
            lblTypeAccountRS.Text = "Tipe Akun"
            GroupBox11.Text = "Retur Pembelian"
            GroupBox12.Text = "Retur Penjualan"
        End If


        tmpFormLoaded = True
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
         For I As Integer = 0 To dgvDocCode.RowCount - 1
            If dgvDocCode.Item(1, I).Value = "" Then
                If Language = "Indonesian" Then
                    MsgBox("Isi semua kolom yang diperlukan !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Please enter all required field(s)!", MsgBoxStyle.Critical, "DMI Retail")
                    Exit Sub
                End If
                Exit Sub
            End If

            If dgvDocCode.Item(1, I).Value.ToString.Length > 3 Then
                If Language = "Indonesian" Then
                    MsgBox("Panjang Kode melebihi panjang yang diperkenankan.", _
                   MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Maximum Length of Code is 3 characters.", _
                   MsgBoxStyle.Critical, "DMI Retail")
                End If
                dgvDocCode.Item(1, dgvDocCode.CurrentCell.RowIndex).Value = ""
                Exit Sub
            End If

            'If dgvDocConvig.CurrentCell.ColumnIndex = 1 Then
            If IsNumeric(dgvDocCode.Item(1, I).Value) Then
                If Language = "Indonesian" Then
                    MsgBox("Anda memasukan nilai yang salah !" & vbCrLf & _
                      "Untuk Kolom : " & _
                       dgvDocCode.Columns(dgvDocCode.CurrentCell.ColumnIndex).HeaderText & vbCrLf & _
                       "Baris Nomor : " & _
                       dgvDocCode.CurrentCell.RowIndex + 1, MsgBoxStyle.Critical, "DMI Retail")
                    'give value "0" to empty fields or unexpected fields
                    dgvDocCode.CurrentCell.Value = ""
                Else
                    MsgBox("You entered unexpected value !" & vbCrLf & _
                      "For Column : " & _
                       dgvDocCode.Columns(dgvDocCode.CurrentCell.ColumnIndex).HeaderText & vbCrLf & _
                       "Row number : " & _
                       dgvDocCode.CurrentCell.RowIndex + 1, MsgBoxStyle.Critical, "DMI Retail")
                    'give value "0" to empty fields or unexpected fields
                    dgvDocCode.CurrentCell.Value = ""
                End If
                Exit Sub
            End If
            'End If
        Next

        For d As Integer = 0 To dgvMasterDataCode.RowCount - 1
            If dgvMasterDataCode.Item(1, d).Value = "" Then
                If Language = "Indonesian" Then
                    MsgBox("Isi semua kolom yang diperlukan !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Please enter all required field(s)!", MsgBoxStyle.Critical, "DMI Retail")
                    Exit Sub
                End If
                Exit Sub
            End If

            If dgvMasterDataCode.Item(1, d).Value.ToString.Length > 3 Then
                If Language = "Indonesian" Then
                    MsgBox("Panjang Kode melebihi panjang yang diperkenankan.", _
                   MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Maximum Length of Code is 3 characters.", _
                   MsgBoxStyle.Critical, "DMI Retail")
                End If
                dgvMasterDataCode.Item(1, dgvMasterDataCode.CurrentCell.RowIndex).Value = ""
                Exit Sub
            End If

            'If dgvDocConvig.CurrentCell.ColumnIndex = 1 Then
            If IsNumeric(dgvMasterDataCode.Item(1, d).Value) Then
                If Language = "Indonesian" Then
                    MsgBox("Anda memasukan nilai yang salah !" & vbCrLf & _
                      "Untuk Kolom : " & _
                       dgvMasterDataCode.Columns(dgvMasterDataCode.CurrentCell.ColumnIndex).HeaderText & vbCrLf & _
                       "Baris Nomor : " & _
                       dgvMasterDataCode.CurrentCell.RowIndex + 1, MsgBoxStyle.Critical, "DMI Retail")
                    'give value "0" to empty fields or unexpected fields
                    dgvMasterDataCode.CurrentCell.Value = ""
                Else
                    MsgBox("You entered unexpected value !" & vbCrLf & _
                      "For Column : " & _
                       dgvMasterDataCode.Columns(dgvMasterDataCode.CurrentCell.ColumnIndex).HeaderText & vbCrLf & _
                       "Row number : " & _
                       dgvMasterDataCode.CurrentCell.RowIndex + 1, MsgBoxStyle.Critical, "DMI Retail")
                    'give value "0" to empty fields or unexpected fields
                    dgvMasterDataCode.CurrentCell.Value = ""
                End If
                Exit Sub
            End If
        Next


        If txtMaxPass.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Isi semua kolom yang diperlukan !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required field(s)!", MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If
            txtMaxPass.Focus()
        End If

        'If txtSafeStockInterval.Text = "" Then
        '    If Language = "Indonesian" Then
        '        MsgBox("Isi semua kolom yang diperlukan !", MsgBoxStyle.Critical, "DMI Retail")
        '    Else
        '        MsgBox("Please enter all required field(s)!", MsgBoxStyle.Critical, "DMI Retail")
        '        Exit Sub
        '    End If
        '    txtSafeStockInterval.Focus()
        'End If

        If mdlGeneral.ValidateAllComboBox(Me) = False Then
            If Language = "Indonesian" Then
                MsgBox("Anda memasukkan data yang salah." & vbCrLf & "Silahkan periksa kembali hasil input Anda!", _
                       vbCritical, "DMI Retail")
                Exit Sub
            Else
                MsgBox("You input unexpected value." & vbCrLf & "Please check all of the fields!", _
                       vbCritical, "DMI Retail")
                Exit Sub
            End If
        End If

        Dim tmpCustomerId, tmpSalePaymentMethodId, tmpVendorId, tmpPurchaseWarehouse, tmpSaleWarehouse, tmpPDSource, _
            tmpPDDestination, tmpPaymentPurchase, tmpSaleSalesman, tmpReturnPurchaseAccount, tmpReturnSaleAccount, tmpInternalSafeStock As Integer
        Dim tmpProductCategory As String
        Dim tmpCheckPrice As Boolean

        Language = cmbLanguage.Text

        If PURCHASE_PAYMENT_METHODBindingSource.Count = 0 Then
            tmpPaymentPurchase = 0
        Else
            tmpPaymentPurchase = IIf(PURCHASE_PAYMENT_METHOD_NAMEComboBox.Text = "", _
                                0, _
                                PURCHASE_PAYMENT_METHODBindingSource.Current("PAYMENT_METHOD_ID"))
        End If


        If PURCHASE_WAREHOUSEBindingSource.Count = 0 Then
            tmpPurchaseWarehouse = 0
        Else
            tmpPurchaseWarehouse = IIf(PURCHASE_WAREHOUSE_NAMEComboBox.Text = "", _
                                   0, _
                                   PURCHASE_WAREHOUSEBindingSource.Current("WAREHOUSE_ID"))
        End If

        If VENDORBindingSource.Count = 0 Then
            tmpVendorId = 0
        Else
            tmpVendorId = IIf(VENDOR_NAMEComboBox.Text = "", _
                          0, _
                          VENDORBindingSource.Current("VENDOR_ID"))
        End If
        If chkLockPrice.Checked = True Then
            tmpCheckPrice = True
        Else
            tmpCheckPrice = False
        End If
        If SALE_PAYMENT_METHODBindingSource.Count = 0 Then
            tmpSalePaymentMethodId = 0
        Else
            tmpSalePaymentMethodId = IIf(SALE_PAYMENT_METHOD_NAMEComboBox.Text = "", _
                                     0, _
                                     SALE_PAYMENT_METHODBindingSource.Current("PAYMENT_METHOD_ID"))
        End If

        If SALE_WAREHOUSEBindingSource.Count = 0 Then
            tmpSaleWarehouse = 0
        Else
            tmpSaleWarehouse = IIf(SALE_WAREHOUSE_NAMEComboBox.Text = "", _
                               0, _
                               SALE_WAREHOUSEBindingSource.Current("WAREHOUSE_ID"))
        End If

        If SALESMANBindingSource.Count = 0 Then
            tmpSaleSalesman = 0
        Else
            tmpSaleSalesman = IIf(SALESMAN_NAMEComboBox.Text = "", _
                                   0, _
                                   SALESMANBindingSource.Current("SALESMAN_ID"))
        End If

        If CUSTOMERBindingSource.Count = 0 Then
            tmpCustomerId = 0
        Else
            tmpCustomerId = IIf(CUSTOMER_NAMEComboBox.Text = "", _
                            0, _
                            CUSTOMERBindingSource.Current("CUSTOMER_ID"))
        End If

        If NEW_PD_SOURCE_WAREHOUSEBindingSource.Count = 0 Then
            tmpPDSource = 0
        Else
            tmpPDSource = IIf(PD_SOURCE_WAREHOUSE_NAMEComboBox.Text = "", _
                          0, _
                         NEW_PD_SOURCE_WAREHOUSEBindingSource.Current("WAREHOUSE_ID"))
        End If

        If PD_DESTINATION_WAREHOUSEBindingSource.Count = 0 Then
            tmpPDDestination = 0
        Else
            tmpPDDestination = IIf(PD_DESTINATION_WAREHOUSE_NAMEComboBox.Text = "", _
                               0, _
                               PD_DESTINATION_WAREHOUSEBindingSource.Current("WAREHOUSE_ID"))
        End If

        If Not ValidateComboBox(cmbProductCategory) Then
            tmpProductCategory = ""
        Else
            tmpProductCategory = cmbProductCategory.Text
        End If

        If ACCOUNTBindingSource.Count = 0 Then
            tmpReturnPurchaseAccount = 0
        Else
            tmpReturnPurchaseAccount = IIf(ACCOUNT_NAMELabel1.Text = "", _
                                    0, _
                                    ACCOUNTBindingSource.Current("ACCOUNT_ID"))
        End If

        If ACCOUNTBindingSource1.Count = 0 Then
            tmpReturnSaleAccount = 0
        Else
            tmpReturnSaleAccount = IIf(ACCOUNT_NAMELabel2.Text = "", _
                                   0, _
                                   ACCOUNTBindingSource1.Current("ACCOUNT_ID"))
        End If

        tmpInternalSafeStock = IIf(txtSafeStockInterval.Text = "", _
                                  0, _
                                  Val(txtSafeStockInterval.Text))

        Language = "[Language]=" & cmbLanguage.Text

        File.WriteAllLines(Application.StartupPath & "\Config.inf", tmpConfig)

        'PARAMETERTableAdapter.SP_CONFIGURATION(tmpPaymentPurchase, _
        '                                       tmpPurchaseWarehouse, _
        '                                       tmpVendorId, _
        '                                       tmpSalePaymentMethodId, _
        '                                       tmpCustomerId, _
        '                                       cmbLanguage.Text, _
        '                                       "Stand Alone", _
        '                                       chkAutoProduct.Checked.ToString, _
        '                                       "", _
        '                                       "", _
        '                                       txtMaxPass.Text, _
        '                                       tmpSaleWarehouse, _
        '                                       tmpPDSource, _
        '                                       tmpPDDestination, _
        '                                       chkAutoSplit.Checked.ToString, _
        '                                       tmpSaleSalesman, _
        '                                       tmpProductCategory, _
        '                                       tmpReturnSaleAccount, _
        '                                       tmpReturnPurchaseAccount, _
        '                                       tmpInternalSafeStock
        '                                     )

        xComm = New SqlCommand("UPDATE PARAMETER SET [VALUE] = '" & tmpCheckPrice & "' " & _
                               "WHERE DESCRIPTION = 'SALE_COLUMN_PRICE'", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xComm.ExecuteNonQuery()

        ''''SAVE DOCUMENT CODE''''
        'PARAMETERTableAdapter.SP_DOCUMENT_CODE("PURCHASE_INVOICE", dgvDocCode.Item(1, 0).Value)
        'PARAMETERTableAdapter.SP_DOCUMENT_CODE("SALE_INVOICE", dgvDocCode.Item(1, 1).Value)
        'PARAMETERTableAdapter.SP_DOCUMENT_CODE("PURCHASE_RETURN", dgvDocCode.Item(1, 2).Value)
        'PARAMETERTableAdapter.SP_DOCUMENT_CODE("SALE_RETURN", dgvDocCode.Item(1, 3).Value)
        'PARAMETERTableAdapter.SP_DOCUMENT_CODE("ADJUSTMENT", dgvDocCode.Item(1, 4).Value)
        'PARAMETERTableAdapter.SP_DOCUMENT_CODE("PRODUCT_DISTRIBUTION", dgvDocCode.Item(1, 5).Value)
        'PARAMETERTableAdapter.SP_DOCUMENT_CODE("OPENING_BALANCE", dgvDocCode.Item(1, 6).Value)
        'PARAMETERTableAdapter.SP_DOCUMENT_CODE("PAYABLE_PAYMENT", dgvDocCode.Item(1, 7).Value)
        'PARAMETERTableAdapter.SP_DOCUMENT_CODE("RECEIVABLE_PAYMENT", dgvDocCode.Item(1, 8).Value)

        ''''SAVE MASTERDATA CODE''''
        'PARAMETERTableAdapter.SP_DOCUMENT_CODE("CUSTOMER", dgvMasterDataCode.Item(1, 0).Value)
        'PARAMETERTableAdapter.SP_DOCUMENT_CODE("VENDOR", dgvMasterDataCode.Item(1, 1).Value)
        'PARAMETERTableAdapter.SP_DOCUMENT_CODE("SALESMAN", dgvMasterDataCode.Item(1, 2).Value)
        'PARAMETERTableAdapter.SP_DOCUMENT_CODE("LOCATION", dgvMasterDataCode.Item(1, 3).Value)
        'PARAMETERTableAdapter.SP_DOCUMENT_CODE("WAREHOUSE", dgvMasterDataCode.Item(1, 4).Value)
        'PARAMETERTableAdapter.SP_DOCUMENT_CODE("PRODUCT", dgvMasterDataCode.Item(1, 5).Value)


        If cmbLanguage.Text = "Indonesian" Then
            MsgBox("Konfigurasi Data berhasil disimpan." & vbCrLf & _
                   "Silahkan restart aplikasi untuk melihat hasilnya.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Configure Data is Success." & vbCrLf & _
                   "Please restart the application to take effect.", MsgBoxStyle.Information, "DMI Retail")
        End If

        tmpChange = False
        Me.Close()
    End Sub

    Private Sub txtServer_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = ""
    End Sub

    Private Sub txtDatabaseName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = ""
    End Sub

    Private Sub txtMaxPass_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMaxPass.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""
    End Sub

    Private Sub txtMaxPass_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMaxPass.TextChanged
        If tmpFormLoaded Then tmpChange = True
    End Sub

    Private Sub cmbLanguage_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLanguage.SelectedIndexChanged
        If tmpFormLoaded Then tmpChange = True
    End Sub

    Private Sub txtPathBack_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If tmpFormLoaded Then tmpChange = True
    End Sub

    Private Sub PURCHASE_PAYMENT_METHOD_NAMEComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PURCHASE_PAYMENT_METHOD_NAMEComboBox.TextChanged
        If tmpFormLoaded Then tmpChange = True
    End Sub

    Private Sub PURCHASE_WAREHOUSE_NAMEComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PURCHASE_WAREHOUSE_NAMEComboBox.TextChanged
        If tmpFormLoaded Then tmpChange = True
    End Sub

    Private Sub VENDOR_NAMEComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles VENDOR_NAMEComboBox.TextChanged
        If tmpFormLoaded Then tmpChange = True
    End Sub

    Private Sub SALE_PAYMENT_METHOD_NAMEComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SALE_PAYMENT_METHOD_NAMEComboBox.TextChanged
        If tmpFormLoaded Then tmpChange = True
    End Sub

    Private Sub SALE_WAREHOUSE_NAMEComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SALE_WAREHOUSE_NAMEComboBox.TextChanged
        If tmpFormLoaded Then tmpChange = True
    End Sub

    Private Sub CUSTOMER_NAMEComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CUSTOMER_NAMEComboBox.TextChanged
        If tmpFormLoaded Then tmpChange = True
    End Sub

    Private Sub chkAutoProduct_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutoProduct.CheckedChanged
        If tmpFormLoaded Then tmpChange = True
    End Sub

    Private Sub chkAutoSplit_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutoSplit.CheckedChanged
        If tmpFormLoaded Then tmpChange = True
    End Sub

    Private Sub PD_SOURCE_WAREHOUSE_NAMEComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PD_SOURCE_WAREHOUSE_NAMEComboBox.TextChanged
        If tmpFormLoaded Then tmpChange = True
    End Sub

    Private Sub PD_DESTINATION_WAREHOUSE_NAMEComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PD_DESTINATION_WAREHOUSE_NAMEComboBox.TextChanged
        If tmpFormLoaded Then tmpChange = True
    End Sub

    Private Sub cmdBackUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'FolderBrowserDialog1.SelectedPath = txtPathBack.Text
        'FolderBrowserDialog1.ShowDialog()
        'txtPathBack.Text = FolderBrowserDialog1.SelectedPath
    End Sub

    Private Sub cmdUndo_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        Me.Close()
    End Sub

    Private Sub txtSafeStockInterval_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSafeStockInterval.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""
    End Sub

End Class
