﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frmChangePassword
    Dim xCmd As SqlCommand
    Dim xReader As SqlDataReader

    Private Sub frmChangePassword_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Language = "Indonesian" Then
            Me.Text = "Ganti Password"
            lblConfirmPass.Text = "Konfirmasi"
            lblOldPass.Text = "Sandi Lama"
            lblNewPass.Text = "Sandi Baru"
            cmdCancel.Text = "&Batal"
        End If

        TxtOldPassword.Text = ""
        TxtNewPassword.Text = ""
        TxtConfirmPassword.Text = ""
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If Language = "Indonesian" Then
            If TxtOldPassword.Text = "" Or TxtNewPassword.Text = "" Or TxtConfirmPassword.Text = "" Then
                MsgBox("Isi semua kolom yang diperlukan !", MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If

            If TxtNewPassword.Text <> TxtConfirmPassword.Text Then
                MsgBox("Anda memasukkan Konfirmasi Sandi yang salah !" & vbCrLf & _
                       "Mohon periksa ulang Konfirmasi anda !", MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If

            If TxtNewPassword.Text = TxtOldPassword.Text Then
                MsgBox("Kata Sandi Baru sama dengan Kata Sandi Lama !" & vbCrLf & _
                       "Silahkan ganti dengan Sandi yang berbeda !", MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If

        Else

            If TxtOldPassword.Text = "" Or TxtNewPassword.Text = "" Or TxtConfirmPassword.Text = "" Then
                MsgBox("Please fill all fields !", MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If

            If TxtNewPassword.Text <> TxtConfirmPassword.Text Then
                MsgBox("You have entered the wrong Password Confirmation !" & vbCrLf & _
                       "Please check the confirmed Password !", MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If

            If TxtNewPassword.Text = TxtOldPassword.Text Then
                MsgBox("The New Password is the same with Old Password !" & vbCrLf & _
                       "Please change with the new one !", MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If
        End If

        Dim sql As String = "SELECT PASSWORD FROM [USER] WHERE USER_NAME = '" & UserName & "'"
        xCmd = New SqlCommand(sql, xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xReader = xCmd.ExecuteReader()
        xReader.Read()
        If Language = "Indonesian" Then
            If xReader.Item("PASSWORD").ToString <> TxtOldPassword.Text Then
                MsgBox("Anda salah memasukkan Kata Sandi Lama !" & vbCrLf & _
                       "Periksa ulang Kata Sandi Lama !", MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If

        Else

            If xReader.Item("PASSWORD").ToString <> TxtOldPassword.Text Then
                MsgBox("You have entered the wrong Old Password !" & vbCrLf & _
                       "Please check the Old Password!", MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If
        End If

        xReader.Close()
        Dim sql1 As String = "UPDATE [USER] SET PASSWORD = '" & TxtNewPassword.Text & "' WHERE USER_NAME = '" & UserName & "'"
        xCmd = New SqlCommand(sql1, xConn)
        Dim x As Integer = xCmd.ExecuteNonQuery()
        If x = 1 Then
            If Language = "Indonesian" Then
                MsgBox("Sandi berhasil diubah.", MsgBoxStyle.Information, "DMI Retail")
            Else
                MsgBox("Password changed successfully.", MsgBoxStyle.Information, "DMI Retail")
            End If
        Else
            If Language = "Indonesian" Then
                MsgBox("Sandi gagal diubah.", MsgBoxStyle.Information, "DMI Retail")
            Else
                MsgBox("Password changed failed.", MsgBoxStyle.Information, "DMI Retail")
            End If
        End If
        Me.Close()
    End Sub

End Class