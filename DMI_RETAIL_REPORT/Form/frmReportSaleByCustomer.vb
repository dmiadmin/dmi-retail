﻿Imports System.Data.SqlClient
Imports System.Data
Public Class frmReportSaleByCustomer

    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        'Me.SP_SALE_REPORT_BY_CUSTOMERTableAdapter.Fill(Me.DS_SALE.SP_SALE_REPORT_BY_CUSTOMER, dtpPeriod1.Value, dtpPeriod2.Value)
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC	    @return_value = [dbo].[SP_SALE_REPORT_BY_CUSTOMER]" & vbCrLf & _
              "         @PERIOD1 = N'" & dtpPeriod1.Value & "'," & vbCrLf & _
              "    		@PERIOD2 = N'" & dtpPeriod2.Value & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.dgvRptSaleByCustomer.DataSource = dt
        If Language = "Indonesian" Then
            dgvRptSaleByCustomer.Columns("CUSTOMER_NAME").HeaderText = "Nama Pelanggan"
            dgvRptSaleByCustomer.Columns("NO_OF_INVOICE").HeaderText = "Jumlah Faktur"
            dgvRptSaleByCustomer.Columns("NO_OF_ITEM").HeaderText = "Jumlah Item"
            dgvRptSaleByCustomer.Columns("TOTAL_AMOUNT").HeaderText = "Jumlah Nominal"
            dgvRptSaleByCustomer.Columns("REP_PERIOD1").Visible = False
            dgvRptSaleByCustomer.Columns("REP_PERIOD2").Visible = False
            dgvRptSaleByCustomer.Columns("NO_OF_INVOICE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptSaleByCustomer.Columns("NO_OF_ITEM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptSaleByCustomer.Columns("TOTAL_AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptSaleByCustomer.Columns("TOTAL_AMOUNT").DefaultCellStyle.Format = "n0"
        Else
            dgvRptSaleByCustomer.Columns("CUSTOMER_NAME").HeaderText = "Customer Name"
            dgvRptSaleByCustomer.Columns("NO_OF_INVOICE").HeaderText = "No Of Invoice"
            dgvRptSaleByCustomer.Columns("NO_OF_ITEM").HeaderText = "No Of Item"
            dgvRptSaleByCustomer.Columns("TOTAL_AMOUNT").HeaderText = "Total Amount"
            dgvRptSaleByCustomer.Columns("REP_PERIOD1").Visible = False
            dgvRptSaleByCustomer.Columns("REP_PERIOD2").Visible = False
            dgvRptSaleByCustomer.Columns("NO_OF_INVOICE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptSaleByCustomer.Columns("NO_OF_ITEM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptSaleByCustomer.Columns("TOTAL_AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptSaleByCustomer.Columns("TOTAL_AMOUNT").DefaultCellStyle.Format = "n0"
        End If

    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        Dim objf As New frmRepSaleByCustomer
        objf.date1 = dtpPeriod1.Value
        objf.date2 = dtpPeriod2.Value
        objf.ReportViewer1.ShowRefreshButton = False
        objf.ReportViewer1.ZoomPercent = 100
        objf.WindowState = FormWindowState.Maximized
        objf.Show()
    End Sub

    Private Sub frmReportSaleByCustomer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dtpPeriod1.Value = DateSerial(Today.Year, Today.Month, 1)
        dtpPeriod2.Value = DateSerial(Today.Year, Today.Month + 1, 0)

        If Language = "Indonesian" Then
            'dgvRptSaleByCustomer.Columns("CUSTOMER_NAME").HeaderText = "Nama Pelanggan"
            'dgvRptSaleByCustomer.Columns("NO_OF_INVOICE").HeaderText = "Jumlah Faktur"
            'dgvRptSaleByCustomer.Columns("NO_OF_ITEM").HeaderText = "Jumlah Item"
            'dgvRptSaleByCustomer.Columns("TOTAL_AMOUNT").HeaderText = "Jumlah Nominal"

            cmdReport.Text = "Cetak"
            cmdGenerate.Text = "Proses"
            lblFrom.Text = "Periode"
        End If
    End Sub
End Class