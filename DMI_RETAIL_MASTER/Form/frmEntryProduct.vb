﻿Imports System.Data.SqlClient

Public Class frmEntryProduct
    Dim tmpSaveMode As String
    Dim tmpCode, tmpEditCode As String
    Dim tmpNoOfCode, tmpNoOfName, tmpUsedProduct As Integer
    Dim tmpProductCode, tmpProductName As String
    Dim tmpProdID, tmpCategory As Integer
    Dim tmpChange, tmpNoData As Boolean
    Dim tmpCheckCategory As String
    Public PRODUCTBindingSource As New BindingSource
    Public LOCATIONBindingSource As New BindingSource

    Private Sub frmEntryProduct_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If tmpChange = True And PRODUCT_CODETextBox.Enabled = True Then
            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah Anda ingin menyimpan perubahan " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If
            If tmpvar = 6 Then
                cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub GetLocation()
        sql = "SELECT LOCATION_ID, LOCATION_NAME, DESCRIPTION, EFFECTIVE_START_DATE, " & _
              "EFFECTIVE_END_DATE, USER_ID_INPUT, INPUT_DATE, USER_ID_UPDATE, " & _
              "UPDATE_DATE, LOCATION_CODE FROM LOCATION"
        dt = New DataTable
        sqladapter = New SqlDataAdapter(sql, xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        LOCATIONBindingSource.DataSource = dt
        LOCATION_NAMEComboBox.DataSource = LOCATIONBindingSource
        LOCATION_NAMEComboBox.ValueMember = "LOCATION_ID"
        LOCATION_NAMEComboBox.DisplayMember = "LOCATION_NAME"

    End Sub

    Private Sub GetData()
        sql = "SELECT PRODUCT_ID, PRODUCT_CODE, PRODUCT_NAME, DESCRIPTION, USER_ID_INPUT, INPUT_DATE, USER_ID_UPDATE, " & _
              "UPDATE_DATE, SAFE_STOCK, LOCATION_ID, SELLING_PRICE, CATEGORY, EFFECTIVE_END_DATE, EFFECTIVE_START_DATE, " & _
              "(SELECT LOCATION_NAME FROM LOCATION WHERE LOCATION_ID = PRODUCT.LOCATION_ID) LOCATION_NAME FROM PRODUCT " & _
              "ORDER BY PRODUCT_NAME"
        dt = New DataTable
        sqladapter = New SqlDataAdapter(sql, xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        PRODUCTBindingSource.DataSource = dt
        PRODUCTBindingNavigator.BindingSource = PRODUCTBindingSource

    End Sub

    Private Sub frmEntryProduct_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        accFormName = Me.Text
        GetData()
        GetLocation()
        PRODUCT_CODETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", PRODUCTBindingSource, "PRODUCT_CODE", True))
        PRODUCT_NAMETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", PRODUCTBindingSource, "PRODUCT_NAME", True))
        DESCRIPTIONTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", PRODUCTBindingSource, "DESCRIPTION", True))
        SELLING_PRICETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", PRODUCTBindingSource, "SELLING_PRICE", True))
        SAFE_STOCKTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", PRODUCTBindingSource, "SAFE_STOCK", True))
        LOCATION_NAMEComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", PRODUCTBindingSource, "LOCATION_NAME", True))
        'CATEGORYComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", PRODUCTBindingSource, "CATEGORY", True))

        DisableInputBox(Me)

        If PRODUCT_NAMETextBox.Text <> "" And CATEGORYComboBox.Enabled = False Then
            tmpCheckCategory = PRODUCTBindingSource.Current("CATEGORY")
            If tmpCheckCategory = "0" Then
                CATEGORYComboBox.Text = "Services"
            ElseIf tmpCheckCategory = "1" Then
                CATEGORYComboBox.Text = "Product"
            ElseIf tmpCheckCategory = "2" Then
                CATEGORYComboBox.Text = "Non Stock"
            End If

            If PRODUCTBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        End If

        cmdSearchCode.Visible = True
        cmdSearchName.Visible = True
        PRODUCTBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        'cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        'cmdDelete.Enabled = True
        tmpChange = False
        chkActive.Enabled = False

        If Language = "Indonesian" Then
            lblProductCode.Text = "Kode Produk"
            lblProductName.Text = "Nama Produk"
            lblDesc.Text = "Keterangan"
            lblSafeStock.Text = "Stock Aman"
            lblSellingPrice.Text = "Harga Jual"
            lblLocation.Text = "Lokasi"
            lblCategory.Text = "Kategori"
            cmdAdd.Text = "Tambah"
            cmdEdit.Text = "Ubah"
            cmdUndo.Text = "Batal"
            cmdDelete.Text = "Hapus"
            cmdSave.Text = "Simpan"
            Me.Text = "Input Produk"
        End If

    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        LOCATION_NAMEComboBox.DataSource = LOCATIONBindingSource
        If AllowAdd = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Insert"

        EnableInputBox(Me)
        PRODUCTBindingSource.AddNew()

        chkActive.Enabled = True
        chkActive.Checked = True

        cmdSearchCode.Visible = False
        cmdSearchName.Visible = False
        PRODUCTBindingNavigator.Enabled = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        LOCATION_NAMEComboBox.Text = ""
        xComm = New SqlCommand("SP_SELECT_PARAMETER", xConn)
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@DESCRIPTION", "PRODUCT CATEGORY")
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdreader = xComm.ExecuteReader
        xdreader.Read()
        If xdreader.HasRows Then
            If xdreader.Item(0) = "0" Then
                CATEGORYComboBox.Text = ""
            Else
                CATEGORYComboBox.Text = xdreader.Item(0)
            End If
        End If

        tmpChange = False
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        If PRODUCT_NAMETextBox.Text = "" Then Exit Sub

        If AllowEdit = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        LOCATION_NAMEComboBox.DataSource = LOCATIONBindingSource
        'If PRODUCTBindingSource.Current("LOCATION_ID") = 0 Then
        '    LOCATION_NAMEComboBox.Text = ""
        'Else
        '    LOCATIONBindingSource.Position = _
        '        LOCATIONBindingSource.Find("LOCATION_ID", PRODUCTBindingSource.Current("LOCATION_ID"))
        'End If

        tmpSaveMode = "Update"

        EnableInputBox(Me)

        cmdSearchCode.Visible = False
        cmdSearchName.Visible = False
        PRODUCTBindingNavigator.Enabled = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        chkActive.Enabled = True

        tmpProductCode = PRODUCT_CODETextBox.Text
        tmpProductName = PRODUCT_NAMETextBox.Text
        tmpChange = False

        If PRODUCTBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
            chkActive.Checked = False
        Else
            chkActive.Checked = True
        End If


    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        LOCATION_NAMEComboBox.DataSource = LOCATIONBindingSource

        tmpSaveMode = ""

        DisableInputBox(Me)
        PRODUCTBindingSource.CancelEdit()

        Try
            If PRODUCTBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        Catch
        End Try

        cmdSearchCode.Visible = True
        cmdSearchName.Visible = True
        PRODUCTBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        tmpChange = False
        chkActive.Enabled = False

        PRODUCTBindingSource_CurrentChanged(Nothing, Nothing)

    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        If PRODUCT_NAMETextBox.Text = "" Or PRODUCT_CODETextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please fill all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If CATEGORYComboBox.Text = "Services" Then
            tmpCategory = 0
        ElseIf CATEGORYComboBox.Text = "Product" Then
            tmpCategory = 1
        ElseIf CATEGORYComboBox.Text = "Non Stock" Then
            tmpCategory = 2
        Else
            If Language = "Indonesian" Then
                MsgBox("Silahkan isi colom category!", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter Category's field!", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        Dim tmpSafeStock, tmpLocationID, tmpSellingPrice As Integer
        tmpSafeStock = IIf(SAFE_STOCKTextBox.Text = "", 0, SAFE_STOCKTextBox.Text)

        If LOCATIONBindingSource Is Nothing Or LOCATIONBindingSource.Count < 1 Then
            tmpLocationID = 0
        Else
            If Not ValidateComboBox(LOCATION_NAMEComboBox) Then
                tmpLocationID = 0
            Else
                tmpLocationID = IIf(LOCATION_NAMEComboBox.Text = "", 0, LOCATIONBindingSource.Current("LOCATION_ID"))
            End If
        End If

        tmpSellingPrice = IIf(SELLING_PRICETextBox.Text = "", 0, SELLING_PRICETextBox.Text)
        tmpCode = PRODUCT_CODETextBox.Text

        If tmpSaveMode = "Insert" Then
            xComm = New SqlCommand("SP_CHECK_PRODUCT_CODE", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@PRODUCT_CODE", PRODUCT_CODETextBox.Text)
            xComm.Parameters.Add("@RESULT", SqlDbType.Int).Direction = ParameterDirection.Output
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()
            tmpNoOfCode = xComm.Parameters("@RESULT").Value

            xComm = New SqlCommand("SP_CHECK_PRODUCT_NAME", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@PRODUCT_NAME", PRODUCT_NAMETextBox.Text)
            xComm.Parameters.Add("@RESULT", SqlDbType.Int).Direction = ParameterDirection.Output
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()
            tmpNoOfName = xComm.Parameters("@RESULT").Value

            If tmpNoOfCode > 0 Or tmpNoOfName > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Kode atau Nama Produk sudah ada di database!" & vbCrLf & _
                               "Tolong isi Kode atau Nama yang lain!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Either Code or Name already exist in Database." & vbCrLf & _
                           "Please enter another Code or Name!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If
            xComm = New SqlCommand("SP_PRODUCT", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "I")
            xComm.Parameters.AddWithValue("@PRODUCT_ID", 0)
            xComm.Parameters.AddWithValue("@PRODUCT_CODE", PRODUCT_CODETextBox.Text)
            xComm.Parameters.AddWithValue("@PRODUCT_NAME", PRODUCT_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@DESCRIPTION", DESCRIPTIONTextBox.Text)
            xComm.Parameters.AddWithValue("@SELLING_PRICE", tmpSellingPrice)
            xComm.Parameters.AddWithValue("@LOCATION_ID", tmpLocationID)
            xComm.Parameters.AddWithValue("@SAFE_STOCK", tmpSafeStock)
            xComm.Parameters.AddWithValue("@CATEGORY", tmpCategory)
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", UserId)
            xComm.Parameters.AddWithValue("@INPUT_DATE", Now)
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", 0)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", DateSerial(4000, 12, 31))
            xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", Now)
            xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", IIf(chkActive.Checked, DateSerial(4000, 12, 31), Now))
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()

        ElseIf tmpSaveMode = "Update" Then
            If PRODUCT_CODETextBox.Text <> tmpProductCode Then
                xComm = New SqlCommand("SP_CHECK_PRODUCT_CODE", xConn)
                xComm.CommandType = CommandType.StoredProcedure
                xComm.Parameters.AddWithValue("@PRODUCT_CODE", PRODUCT_CODETextBox.Text)
                xComm.Parameters.Add("@RESULT", SqlDbType.Int).Direction = ParameterDirection.Output
                If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                xComm.ExecuteNonQuery()
                tmpNoOfCode = xComm.Parameters("@RESULT").Value

                If tmpNoOfCode > 0 Then
                    If Language = "Indonesian" Then
                        MsgBox("Kode atau Nama Produk sudah ada di database!" & vbCrLf & _
                               "Tolong isi Kode atau Nama yang berbeda!", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("Either Code or Name already exist in Database." & vbCrLf & _
                               "Please enter another Code or Name!", MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    PRODUCT_CODETextBox.Focus()
                    Exit Sub
                End If
            End If

            If PRODUCT_NAMETextBox.Text.ToUpper <> tmpProductName.ToUpper Then
                xComm = New SqlCommand("SP_CHECK_PRODUCT_NAME", xConn)
                xComm.CommandType = CommandType.StoredProcedure
                xComm.Parameters.AddWithValue("@PRODUCT_NAME", PRODUCT_NAMETextBox.Text)
                xComm.Parameters.Add("@RESULT", SqlDbType.Int).Direction = ParameterDirection.Output
                If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                xComm.ExecuteNonQuery()
                tmpNoOfName = xComm.Parameters("@RESULT").Value
            Else
                tmpNoOfName = 0
            End If

            If tmpNoOfName > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Kode atau Nama sudah ada di database!" & vbCrLf & _
                           "Tolong isi Kode atau Nama yang berbeda!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Either Code or Name already exist in Database." & vbCrLf & _
                           "Please enter another Code or Name!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If

            xComm = New SqlCommand("SP_PRODUCT", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "U")
            xComm.Parameters.AddWithValue("@PRODUCT_ID", PRODUCTBindingSource.Current("PRODUCT_ID"))
            xComm.Parameters.AddWithValue("@PRODUCT_CODE", PRODUCT_CODETextBox.Text)
            xComm.Parameters.AddWithValue("@PRODUCT_NAME", PRODUCT_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@DESCRIPTION", DESCRIPTIONTextBox.Text)
            xComm.Parameters.AddWithValue("@SELLING_PRICE", tmpSellingPrice)
            xComm.Parameters.AddWithValue("@LOCATION_ID", tmpLocationID)
            xComm.Parameters.AddWithValue("@SAFE_STOCK", tmpSafeStock)
            xComm.Parameters.AddWithValue("@CATEGORY", tmpCategory)
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", PRODUCTBindingSource.Current("USER_ID_INPUT"))
            xComm.Parameters.AddWithValue("@INPUT_DATE", PRODUCTBindingSource.Current("INPUT_DATE"))
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", UserId)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", Now)
            xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", PRODUCTBindingSource.Current("EFFECTIVE_START_DATE"))
            xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", IIf(chkActive.Checked, DateSerial(4000, 12, 31), Now))
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()
        End If

        LOCATION_NAMEComboBox.DataSource = LOCATIONBindingSource
        GetData()
        ' Me.PRODUCTTableAdapter.Fill(Me.DS_PRODUCT.PRODUCT)
        PRODUCTBindingSource.Position = PRODUCTBindingSource.Find("PRODUCT_CODE", tmpCode)

        tmpSaveMode = ""

        DisableInputBox(Me)
        PRODUCTBindingSource.CancelEdit()

        cmdSearchCode.Visible = True
        cmdSearchName.Visible = True
        PRODUCTBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        chkActive.Enabled = False

        If Language = "Indonesian" Then
            MsgBox("Data berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
        End If
        tmpLocationID = False

        PRODUCTBindingSource_CurrentChanged(Nothing, Nothing)

    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If PRODUCT_NAMETextBox.Text = "" Then Exit Sub

        If AllowDelete = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If Language = "Indonesian" Then
            If MsgBox("Hapus Produk?" & vbCrLf & PRODUCT_NAMETextBox.Text, _
                MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
            xComm = New SqlCommand("SP_CHECK_PRODUCT_USED", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@PRODUCT_NAME", PRODUCT_NAMETextBox.Text)
            xComm.Parameters.Add("@RESULT", SqlDbType.Int).Direction = ParameterDirection.Output
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()
            tmpUsedProduct = xComm.Parameters("@RESULT").Value

            If tmpUsedProduct > 0 Then
                MsgBox("Produk ini telah digunakan di transaksi!" & vbCrLf & _
                       "Anda tidak dapat menghapus data ini!", _
                       MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If
        Else
            If MsgBox("Delete Product?" & vbCrLf & PRODUCT_NAMETextBox.Text, _
                      MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
            xComm = New SqlCommand("SP_CHECK_PRODUCT_USED", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@PRODUCT_NAME", PRODUCT_NAMETextBox.Text)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xdreader = xComm.ExecuteReader
            xdreader.Read()
            tmpUsedProduct = xdreader.Item(0)

            If tmpUsedProduct > 0 Then
                MsgBox("Product is used by another transaction !" & vbCrLf & _
                      "You can not delete this data !", MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If
        End If
        xComm = New SqlCommand("SP_PRODUCT", xConn)
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@METHOD", "D")
        xComm.Parameters.AddWithValue("@PRODUCT_ID", PRODUCTBindingSource.Current("PRODUCT_ID"))
        xComm.Parameters.AddWithValue("@PRODUCT_CODE", PRODUCTBindingSource.Current("PRODUCT_CODE"))
        xComm.Parameters.AddWithValue("@PRODUCT_NAME", PRODUCTBindingSource.Current("PRODUCT_NAME"))
        xComm.Parameters.AddWithValue("@DESCRIPTION", PRODUCTBindingSource.Current("DESCRIPTION"))
        xComm.Parameters.AddWithValue("@SELLING_PRICE", PRODUCTBindingSource.Current("SELLING_PRICE"))
        xComm.Parameters.AddWithValue("@LOCATION_ID", PRODUCTBindingSource.Current("LOCATION_ID"))
        xComm.Parameters.AddWithValue("@SAFE_STOCK", PRODUCTBindingSource.Current("SAFE_STOCK"))
        xComm.Parameters.AddWithValue("@CATEGORY", PRODUCTBindingSource.Current("CATEGORY"))
        xComm.Parameters.AddWithValue("@USER_ID_INPUT", PRODUCTBindingSource.Current("USER_ID_INPUT"))
        xComm.Parameters.AddWithValue("@INPUT_DATE", PRODUCTBindingSource.Current("INPUT_DATE"))
        xComm.Parameters.AddWithValue("@USER_ID_UPDATE", PRODUCTBindingSource.Current("PRODUCT_ID"))
        xComm.Parameters.AddWithValue("@UPDATE_DATE", PRODUCTBindingSource.Current("PRODUCT_ID"))
        xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", PRODUCTBindingSource.Current("EFFECTIVE_START_DATE"))
        xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", Now)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xComm.ExecuteNonQuery()

        PRODUCTBindingSource.RemoveCurrent()
        PRODUCTBindingSource.Position = 0

        GetData()
        Try
            If PRODUCTBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        Catch
        End Try

        chkActive.Enabled = False
        tmpChange = False

    End Sub

    Private Sub PRODUCT_CODETextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PRODUCT_CODETextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            PRODUCT_NAMETextBox.Focus()
        End If
    End Sub

    Private Sub cmdSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchCode.Click
        If PRODUCT_CODETextBox.Text = "" Then Exit Sub
        mdlGeneral.tmpSearchMode = "Entry Product - Product Code"
        frmSearchProduct.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        PRODUCTBindingSource.Position = PRODUCTBindingSource.Find("PRODUCT_ID", tmpSearchResult)
    End Sub

    Private Sub cmdSearchName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchName.Click
        If PRODUCT_CODETextBox.Text = "" Then Exit Sub
        mdlGeneral.tmpSearchMode = "Entry Product - Product Name"
        frmSearchProduct.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        PRODUCTBindingSource.Position = PRODUCTBindingSource.Find("PRODUCT_ID", tmpSearchResult)
    End Sub

    Private Sub SAFE_STOCKTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles SAFE_STOCKTextBox.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Asc(e.KeyChar) < Asc("0") Or Asc(e.KeyChar) > Asc("9") Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub SELLING_PRICETextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles SELLING_PRICETextBox.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Asc(e.KeyChar) < Asc("0") Or Asc(e.KeyChar) > Asc("9") Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub frmEntryProduct_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
        If tmpNoData Then Me.Close()
    End Sub

    Private Sub PRODUCT_CODETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PRODUCT_CODETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub PRODUCT_NAMETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PRODUCT_NAMETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub DESCRIPTIONTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DESCRIPTIONTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub SELLING_PRICETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SELLING_PRICETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub SAFE_STOCKTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SAFE_STOCKTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub LOCATION_NAMEComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LOCATION_NAMEComboBox.SelectedIndexChanged
        tmpChange = True
    End Sub

    Private Sub PRODUCTBindingSource_CurrentChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If PRODUCT_NAMETextBox.Text <> "" And CATEGORYComboBox.Enabled = False Then
            tmpCheckCategory = PRODUCTBindingSource.Current("CATEGORY")
            If tmpCheckCategory = "0" Then
                CATEGORYComboBox.Text = "Services"
            ElseIf tmpCheckCategory = "1" Then
                CATEGORYComboBox.Text = "Product"
            ElseIf tmpCheckCategory = "2" Then
                CATEGORYComboBox.Text = "Non Stock"
            End If

            If PRODUCTBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        End If

    End Sub

    Private Sub SpecialQtyTextbox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Asc(e.KeyChar) < Asc("0") Or Asc(e.KeyChar) > Asc("9") Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub SpecialQtyTextbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tmpChange = True
    End Sub

    Private Sub BindingNavigatorPositionItem_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BindingNavigatorPositionItem.TextChanged
        Try
            If PRODUCT_NAMETextBox.Text <> "" And CATEGORYComboBox.Enabled = False Then
                tmpCheckCategory = PRODUCTBindingSource.Current("CATEGORY")
                If tmpCheckCategory = "0" Then
                    CATEGORYComboBox.Text = "Services"
                ElseIf tmpCheckCategory = "1" Then
                    CATEGORYComboBox.Text = "Product"
                ElseIf tmpCheckCategory = "2" Then
                    CATEGORYComboBox.Text = "Non Stock"
                End If

                If PRODUCTBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                    chkActive.Checked = False
                Else
                    chkActive.Checked = True
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        AccessPrivilege(Me.Tag)
    End Sub

End Class