﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEntrySalesman
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEntrySalesman))
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.SALESMANBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.SALESMAN_NAMETextBox = New System.Windows.Forms.TextBox()
        Me.SALESMAN_ADDRESSTextBox = New System.Windows.Forms.TextBox()
        Me.SALESMAN_ADDRESS_CITYTextBox = New System.Windows.Forms.TextBox()
        Me.SALESMAN_ADDRESS_STATETextBox = New System.Windows.Forms.TextBox()
        Me.SALESMAN_PHONE1TextBox = New System.Windows.Forms.TextBox()
        Me.SALESMAN_PHONE2TextBox = New System.Windows.Forms.TextBox()
        Me.SALESMAN_PHONE3TextBox = New System.Windows.Forms.TextBox()
        Me.SALESMAN_CODETextBox = New System.Windows.Forms.TextBox()
        Me.cmdSearchCode = New System.Windows.Forms.PictureBox()
        Me.cmdSearchName = New System.Windows.Forms.PictureBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkActive = New System.Windows.Forms.CheckBox()
        Me.lblSalesmanPhone3 = New System.Windows.Forms.Label()
        Me.lblSalesmanPhone2 = New System.Windows.Forms.Label()
        Me.lblSalesmanPhone1 = New System.Windows.Forms.Label()
        Me.lblSalesmanAddressState = New System.Windows.Forms.Label()
        Me.lblSalesmanAddressCity = New System.Windows.Forms.Label()
        Me.lblSalesmanAddress = New System.Windows.Forms.Label()
        Me.lblSalesmanName = New System.Windows.Forms.Label()
        Me.lblSalesmanCode = New System.Windows.Forms.Label()
        Me.GroupBox2.SuspendLayout()
        CType(Me.SALESMANBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SALESMANBindingNavigator.SuspendLayout()
        CType(Me.cmdSearchCode, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdUndo)
        Me.GroupBox2.Controls.Add(Me.cmdSave)
        Me.GroupBox2.Controls.Add(Me.cmdEdit)
        Me.GroupBox2.Controls.Add(Me.cmdDelete)
        Me.GroupBox2.Controls.Add(Me.cmdAdd)
        Me.GroupBox2.Location = New System.Drawing.Point(13, 293)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Size = New System.Drawing.Size(515, 55)
        Me.GroupBox2.TabIndex = 14
        Me.GroupBox2.TabStop = False
        '
        'cmdUndo
        '
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(224, 17)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(74, 29)
        Me.cmdUndo.TabIndex = 11
        Me.cmdUndo.Text = "&Undo"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSave.Location = New System.Drawing.Point(321, 17)
        Me.cmdSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(80, 29)
        Me.cmdSave.TabIndex = 12
        Me.cmdSave.Text = "&Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(123, 17)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(74, 29)
        Me.cmdEdit.TabIndex = 10
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(422, 17)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(74, 29)
        Me.cmdDelete.TabIndex = 13
        Me.cmdDelete.Text = "&Delete"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(18, 17)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(78, 29)
        Me.cmdAdd.TabIndex = 9
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'SALESMANBindingNavigator
        '
        Me.SALESMANBindingNavigator.AddNewItem = Nothing
        Me.SALESMANBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.SALESMANBindingNavigator.DeleteItem = Nothing
        Me.SALESMANBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.SALESMANBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.SALESMANBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.SALESMANBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.SALESMANBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.SALESMANBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.SALESMANBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.SALESMANBindingNavigator.Name = "SALESMANBindingNavigator"
        Me.SALESMANBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.SALESMANBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.SALESMANBindingNavigator.Size = New System.Drawing.Size(541, 25)
        Me.SALESMANBindingNavigator.TabIndex = 15
        Me.SALESMANBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'SALESMAN_NAMETextBox
        '
        Me.SALESMAN_NAMETextBox.Location = New System.Drawing.Point(147, 50)
        Me.SALESMAN_NAMETextBox.Name = "SALESMAN_NAMETextBox"
        Me.SALESMAN_NAMETextBox.Size = New System.Drawing.Size(216, 22)
        Me.SALESMAN_NAMETextBox.TabIndex = 2
        Me.SALESMAN_NAMETextBox.Tag = "M"
        '
        'SALESMAN_ADDRESSTextBox
        '
        Me.SALESMAN_ADDRESSTextBox.Location = New System.Drawing.Point(147, 79)
        Me.SALESMAN_ADDRESSTextBox.Name = "SALESMAN_ADDRESSTextBox"
        Me.SALESMAN_ADDRESSTextBox.Size = New System.Drawing.Size(268, 22)
        Me.SALESMAN_ADDRESSTextBox.TabIndex = 3
        Me.SALESMAN_ADDRESSTextBox.Tag = "M"
        '
        'SALESMAN_ADDRESS_CITYTextBox
        '
        Me.SALESMAN_ADDRESS_CITYTextBox.Location = New System.Drawing.Point(147, 108)
        Me.SALESMAN_ADDRESS_CITYTextBox.Name = "SALESMAN_ADDRESS_CITYTextBox"
        Me.SALESMAN_ADDRESS_CITYTextBox.Size = New System.Drawing.Size(179, 22)
        Me.SALESMAN_ADDRESS_CITYTextBox.TabIndex = 4
        '
        'SALESMAN_ADDRESS_STATETextBox
        '
        Me.SALESMAN_ADDRESS_STATETextBox.Location = New System.Drawing.Point(147, 137)
        Me.SALESMAN_ADDRESS_STATETextBox.Name = "SALESMAN_ADDRESS_STATETextBox"
        Me.SALESMAN_ADDRESS_STATETextBox.Size = New System.Drawing.Size(179, 22)
        Me.SALESMAN_ADDRESS_STATETextBox.TabIndex = 5
        '
        'SALESMAN_PHONE1TextBox
        '
        Me.SALESMAN_PHONE1TextBox.Location = New System.Drawing.Point(147, 166)
        Me.SALESMAN_PHONE1TextBox.Name = "SALESMAN_PHONE1TextBox"
        Me.SALESMAN_PHONE1TextBox.Size = New System.Drawing.Size(134, 22)
        Me.SALESMAN_PHONE1TextBox.TabIndex = 6
        Me.SALESMAN_PHONE1TextBox.Tag = "M"
        '
        'SALESMAN_PHONE2TextBox
        '
        Me.SALESMAN_PHONE2TextBox.Location = New System.Drawing.Point(147, 195)
        Me.SALESMAN_PHONE2TextBox.Name = "SALESMAN_PHONE2TextBox"
        Me.SALESMAN_PHONE2TextBox.Size = New System.Drawing.Size(134, 22)
        Me.SALESMAN_PHONE2TextBox.TabIndex = 7
        '
        'SALESMAN_PHONE3TextBox
        '
        Me.SALESMAN_PHONE3TextBox.Location = New System.Drawing.Point(147, 224)
        Me.SALESMAN_PHONE3TextBox.Name = "SALESMAN_PHONE3TextBox"
        Me.SALESMAN_PHONE3TextBox.Size = New System.Drawing.Size(134, 22)
        Me.SALESMAN_PHONE3TextBox.TabIndex = 8
        '
        'SALESMAN_CODETextBox
        '
        Me.SALESMAN_CODETextBox.Location = New System.Drawing.Point(147, 21)
        Me.SALESMAN_CODETextBox.Name = "SALESMAN_CODETextBox"
        Me.SALESMAN_CODETextBox.Size = New System.Drawing.Size(134, 22)
        Me.SALESMAN_CODETextBox.TabIndex = 1
        Me.SALESMAN_CODETextBox.Tag = "M"
        '
        'cmdSearchCode
        '
        Me.cmdSearchCode.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchCode.Image = CType(resources.GetObject("cmdSearchCode.Image"), System.Drawing.Image)
        Me.cmdSearchCode.Location = New System.Drawing.Point(288, 21)
        Me.cmdSearchCode.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchCode.Name = "cmdSearchCode"
        Me.cmdSearchCode.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearchCode.TabIndex = 33
        Me.cmdSearchCode.TabStop = False
        '
        'cmdSearchName
        '
        Me.cmdSearchName.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchName.Image = CType(resources.GetObject("cmdSearchName.Image"), System.Drawing.Image)
        Me.cmdSearchName.Location = New System.Drawing.Point(371, 50)
        Me.cmdSearchName.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchName.Name = "cmdSearchName"
        Me.cmdSearchName.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearchName.TabIndex = 34
        Me.cmdSearchName.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkActive)
        Me.GroupBox1.Controls.Add(Me.lblSalesmanPhone3)
        Me.GroupBox1.Controls.Add(Me.lblSalesmanPhone2)
        Me.GroupBox1.Controls.Add(Me.lblSalesmanPhone1)
        Me.GroupBox1.Controls.Add(Me.lblSalesmanAddressState)
        Me.GroupBox1.Controls.Add(Me.lblSalesmanAddressCity)
        Me.GroupBox1.Controls.Add(Me.lblSalesmanAddress)
        Me.GroupBox1.Controls.Add(Me.lblSalesmanName)
        Me.GroupBox1.Controls.Add(Me.lblSalesmanCode)
        Me.GroupBox1.Controls.Add(Me.cmdSearchName)
        Me.GroupBox1.Controls.Add(Me.SALESMAN_CODETextBox)
        Me.GroupBox1.Controls.Add(Me.cmdSearchCode)
        Me.GroupBox1.Controls.Add(Me.SALESMAN_PHONE3TextBox)
        Me.GroupBox1.Controls.Add(Me.SALESMAN_NAMETextBox)
        Me.GroupBox1.Controls.Add(Me.SALESMAN_PHONE2TextBox)
        Me.GroupBox1.Controls.Add(Me.SALESMAN_ADDRESSTextBox)
        Me.GroupBox1.Controls.Add(Me.SALESMAN_PHONE1TextBox)
        Me.GroupBox1.Controls.Add(Me.SALESMAN_ADDRESS_CITYTextBox)
        Me.GroupBox1.Controls.Add(Me.SALESMAN_ADDRESS_STATETextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 28)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(517, 259)
        Me.GroupBox1.TabIndex = 35
        Me.GroupBox1.TabStop = False
        '
        'chkActive
        '
        Me.chkActive.AutoSize = True
        Me.chkActive.Location = New System.Drawing.Point(433, 227)
        Me.chkActive.Name = "chkActive"
        Me.chkActive.Size = New System.Drawing.Size(56, 17)
        Me.chkActive.TabIndex = 44
        Me.chkActive.Text = "Active"
        Me.chkActive.UseVisualStyleBackColor = True
        '
        'lblSalesmanPhone3
        '
        Me.lblSalesmanPhone3.Location = New System.Drawing.Point(25, 227)
        Me.lblSalesmanPhone3.Name = "lblSalesmanPhone3"
        Me.lblSalesmanPhone3.Size = New System.Drawing.Size(116, 19)
        Me.lblSalesmanPhone3.TabIndex = 43
        Me.lblSalesmanPhone3.Text = "Phone 3"
        Me.lblSalesmanPhone3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblSalesmanPhone2
        '
        Me.lblSalesmanPhone2.Location = New System.Drawing.Point(22, 198)
        Me.lblSalesmanPhone2.Name = "lblSalesmanPhone2"
        Me.lblSalesmanPhone2.Size = New System.Drawing.Size(119, 19)
        Me.lblSalesmanPhone2.TabIndex = 42
        Me.lblSalesmanPhone2.Text = "Phone 2"
        Me.lblSalesmanPhone2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblSalesmanPhone1
        '
        Me.lblSalesmanPhone1.Location = New System.Drawing.Point(25, 169)
        Me.lblSalesmanPhone1.Name = "lblSalesmanPhone1"
        Me.lblSalesmanPhone1.Size = New System.Drawing.Size(116, 19)
        Me.lblSalesmanPhone1.TabIndex = 41
        Me.lblSalesmanPhone1.Text = "Phone 1"
        Me.lblSalesmanPhone1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblSalesmanAddressState
        '
        Me.lblSalesmanAddressState.Location = New System.Drawing.Point(22, 140)
        Me.lblSalesmanAddressState.Name = "lblSalesmanAddressState"
        Me.lblSalesmanAddressState.Size = New System.Drawing.Size(119, 19)
        Me.lblSalesmanAddressState.TabIndex = 40
        Me.lblSalesmanAddressState.Text = "State"
        Me.lblSalesmanAddressState.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblSalesmanAddressCity
        '
        Me.lblSalesmanAddressCity.Location = New System.Drawing.Point(22, 111)
        Me.lblSalesmanAddressCity.Name = "lblSalesmanAddressCity"
        Me.lblSalesmanAddressCity.Size = New System.Drawing.Size(119, 19)
        Me.lblSalesmanAddressCity.TabIndex = 39
        Me.lblSalesmanAddressCity.Text = "City"
        Me.lblSalesmanAddressCity.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblSalesmanAddress
        '
        Me.lblSalesmanAddress.Location = New System.Drawing.Point(19, 82)
        Me.lblSalesmanAddress.Name = "lblSalesmanAddress"
        Me.lblSalesmanAddress.Size = New System.Drawing.Size(122, 19)
        Me.lblSalesmanAddress.TabIndex = 38
        Me.lblSalesmanAddress.Text = "Address"
        Me.lblSalesmanAddress.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblSalesmanName
        '
        Me.lblSalesmanName.Location = New System.Drawing.Point(19, 53)
        Me.lblSalesmanName.Name = "lblSalesmanName"
        Me.lblSalesmanName.Size = New System.Drawing.Size(122, 19)
        Me.lblSalesmanName.TabIndex = 37
        Me.lblSalesmanName.Text = "Salesman Name"
        Me.lblSalesmanName.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblSalesmanCode
        '
        Me.lblSalesmanCode.Location = New System.Drawing.Point(19, 25)
        Me.lblSalesmanCode.Name = "lblSalesmanCode"
        Me.lblSalesmanCode.Size = New System.Drawing.Size(122, 17)
        Me.lblSalesmanCode.TabIndex = 36
        Me.lblSalesmanCode.Text = "Salesman Code"
        Me.lblSalesmanCode.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'frmEntrySalesman
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(541, 364)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.SALESMANBindingNavigator)
        Me.Controls.Add(Me.GroupBox2)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmEntrySalesman"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Entry Salesman"
        Me.Text = "Entry Salesman"
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.SALESMANBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SALESMANBindingNavigator.ResumeLayout(False)
        Me.SALESMANBindingNavigator.PerformLayout()
        CType(Me.cmdSearchCode, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents SALESMANBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SALESMAN_NAMETextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALESMAN_ADDRESSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALESMAN_ADDRESS_CITYTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALESMAN_ADDRESS_STATETextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALESMAN_PHONE1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALESMAN_PHONE2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALESMAN_PHONE3TextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALESMAN_CODETextBox As System.Windows.Forms.TextBox
    Friend WithEvents cmdSearchCode As System.Windows.Forms.PictureBox
    Friend WithEvents cmdSearchName As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblSalesmanPhone3 As System.Windows.Forms.Label
    Friend WithEvents lblSalesmanPhone2 As System.Windows.Forms.Label
    Friend WithEvents lblSalesmanPhone1 As System.Windows.Forms.Label
    Friend WithEvents lblSalesmanAddressState As System.Windows.Forms.Label
    Friend WithEvents lblSalesmanAddressCity As System.Windows.Forms.Label
    Friend WithEvents lblSalesmanAddress As System.Windows.Forms.Label
    Friend WithEvents lblSalesmanName As System.Windows.Forms.Label
    Friend WithEvents lblSalesmanCode As System.Windows.Forms.Label
    Friend WithEvents chkActive As System.Windows.Forms.CheckBox
End Class
