﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRepSaleByCustomer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource2 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRepSaleByCustomer))
        Me.COMPANY_INFORMATIONBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DB_DMI_RETAILDataSetALL = New DMI_RETAIL_REPORT.DB_DMI_RETAILDataSetALL()
        Me.SP_SALE_REPORT_BY_CUSTOMERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.COMPANY_INFORMATIONTableAdapter = New DMI_RETAIL_REPORT.DB_DMI_RETAILDataSetALLTableAdapters.COMPANY_INFORMATIONTableAdapter()
        Me.SP_SALE_REPORT_BY_CUSTOMERTableAdapter = New DMI_RETAIL_REPORT.DB_DMI_RETAILDataSetALLTableAdapters.SP_SALE_REPORT_BY_CUSTOMERTableAdapter()
        CType(Me.COMPANY_INFORMATIONBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DB_DMI_RETAILDataSetALL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_SALE_REPORT_BY_CUSTOMERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'COMPANY_INFORMATIONBindingSource
        '
        Me.COMPANY_INFORMATIONBindingSource.DataMember = "COMPANY_INFORMATION"
        Me.COMPANY_INFORMATIONBindingSource.DataSource = Me.DB_DMI_RETAILDataSetALL
        '
        'DB_DMI_RETAILDataSetALL
        '
        Me.DB_DMI_RETAILDataSetALL.DataSetName = "DB_DMI_RETAILDataSetALL"
        Me.DB_DMI_RETAILDataSetALL.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SP_SALE_REPORT_BY_CUSTOMERBindingSource
        '
        Me.SP_SALE_REPORT_BY_CUSTOMERBindingSource.DataMember = "SP_SALE_REPORT_BY_CUSTOMER"
        Me.SP_SALE_REPORT_BY_CUSTOMERBindingSource.DataSource = Me.DB_DMI_RETAILDataSetALL
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "DS_RPT_COMPANY_INFO"
        ReportDataSource1.Value = Me.COMPANY_INFORMATIONBindingSource
        ReportDataSource2.Name = "DS_RPT_SALE_BY_CUSTOMER"
        ReportDataSource2.Value = Me.SP_SALE_REPORT_BY_CUSTOMERBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource2)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "DMI_RETAIL_REPORT.repSaleByCustomer.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(331, 302)
        Me.ReportViewer1.TabIndex = 0
        '
        'COMPANY_INFORMATIONTableAdapter
        '
        Me.COMPANY_INFORMATIONTableAdapter.ClearBeforeFill = True
        '
        'SP_SALE_REPORT_BY_CUSTOMERTableAdapter
        '
        Me.SP_SALE_REPORT_BY_CUSTOMERTableAdapter.ClearBeforeFill = True
        '
        'frmRepSaleByCustomer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(331, 302)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmRepSaleByCustomer"
        Me.Text = "Report Sale By Customer"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.COMPANY_INFORMATIONBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DB_DMI_RETAILDataSetALL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_SALE_REPORT_BY_CUSTOMERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents COMPANY_INFORMATIONBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DB_DMI_RETAILDataSetALL As DMI_RETAIL_REPORT.DB_DMI_RETAILDataSetALL
    Friend WithEvents SP_SALE_REPORT_BY_CUSTOMERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents COMPANY_INFORMATIONTableAdapter As DMI_RETAIL_REPORT.DB_DMI_RETAILDataSetALLTableAdapters.COMPANY_INFORMATIONTableAdapter
    Friend WithEvents SP_SALE_REPORT_BY_CUSTOMERTableAdapter As DMI_RETAIL_REPORT.DB_DMI_RETAILDataSetALLTableAdapters.SP_SALE_REPORT_BY_CUSTOMERTableAdapter
End Class
