﻿Public Class frmEntryUnit
    Dim tmpSaveMode As String
    Dim tmpUsedUnit As Integer
    Dim tmpChange As Boolean

    Private Sub frmEntryUnit_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If tmpChange = True And UNIT_NAMETextBox.Enabled = True Then
            tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            If tmpvar = 6 Then
                cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        End If
    End Sub


    Private Sub frmEntryUnit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        DisableInputBox(Me)

        cmdSearch.Visible = True
        UNITBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        tmpChange = False

    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        tmpSaveMode = "Insert"

        EnableInputBox(Me)
        UNITBindingSource.AddNew()

        cmdSearch.Visible = False
        UNITBindingNavigator.Enabled = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        tmpChange = False
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        If UNIT_NAMETextBox.Text = "" Then Exit Sub

        tmpSaveMode = "Update"

        EnableInputBox(Me)

        cmdSearch.Visible = False
        UNITBindingNavigator.Enabled = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        tmpChange = False
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        tmpSaveMode = ""

        DisableInputBox(Me)
        UNITBindingSource.CancelEdit()

        cmdSearch.Visible = True
        UNITBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        tmpChange = False
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Me.Validate()

        If UNIT_NAMETextBox.Text = "" Then
            MsgBox("Please enter all required field(s) !", MsgBoxStyle.Critical, "DMI Retail")
            Exit Sub
        End If

        If tmpSaveMode = "Insert" Then
            'UNITTableAdapter.SP_UNIT("I", _
            '                         0, _
            '                         UNIT_NAMETextBox.Text, _
            '                         DateSerial(Today.Year, Today.Month, Today.Day), _
            '                         DateSerial(4000, 12, 31), _
            '                         mdlGeneral.USER_ID, _
            '                         Now, _
            '                         0, _
            '                         DateSerial(4000, 12, 31))
        ElseIf tmpSaveMode = "Update" Then
            'UNITTableAdapter.SP_UNIT("U", _
            '                         UNITBindingSource.Current("UNIT_ID"), _
            '                         UNIT_NAMETextBox.Text, _
            '                         UNITBindingSource.Current("EFFECTIVE_START_DATE"), _
            '                         UNITBindingSource.Current("EFFECTIVE_END_DATE"), _
            '                         UNITBindingSource.Current("USER_ID_INPUT"), _
            '                         UNITBindingSource.Current("INPUT_DATE"), _
            '                         mdlGeneral.USER_ID, _
            '                         Now)
        End If

        tmpSaveMode = ""

        DisableInputBox(Me)
        UNITBindingSource.CancelEdit()

        cmdSearch.Visible = True
        UNITBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True

        'Me.UNITTableAdapter.Fill(Me.DS_UNIT.UNIT)
        MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If UNIT_NAMETextBox.Text = "" Then Exit Sub

        If MsgBox("Delete Unit?" & vbCrLf & UNIT_NAMETextBox.Text, _
                  MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub

        'UNITTableAdapter.SP_CHECK_UNIT_USED(UNIT_NAMETextBox.Text, tmpUsedUnit)
        If tmpUsedUnit > 0 Then
            MsgBox("Cannot delete this unit !" & vbCrLf & "This Unit is used by another transaction !", _
                   MsgBoxStyle.Critical, "DMI Retail")
            Exit Sub
        End If

        'UNITTableAdapter.SP_UNIT("D", _
        '                         UNITBindingSource.Current("UNIT_ID"), _
        '                         UNITBindingSource.Current("UNIT_NAME"), _
        '                         UNITBindingSource.Current("EFFECTIVE_START_DATE"), _
        '                         UNITBindingSource.Current("EFFECTIVE_END_DATE"), _
        '                         UNITBindingSource.Current("USER_ID_INPUT"), _
        '                         UNITBindingSource.Current("INPUT_DATE"), _
        '                         UNITBindingSource.Current("USER_ID_UPDATE"), _
        '                         UNITBindingSource.Current("UPDATE_DATE"))

        UNITBindingSource.RemoveCurrent()
        UNITBindingSource.Position = 0
    End Sub

    Private Sub UNIT_NAMETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UNIT_NAMETextBox.TextChanged
        tmpChange = True
    End Sub
End Class
