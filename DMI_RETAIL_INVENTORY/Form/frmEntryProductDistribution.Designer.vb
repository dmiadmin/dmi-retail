﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEntryProductDistribution
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEntryProductDistribution))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblQuantity = New System.Windows.Forms.Label()
        Me.lblQuantity1 = New System.Windows.Forms.Label()
        Me.lblDestination = New System.Windows.Forms.Label()
        Me.lblSource = New System.Windows.Forms.Label()
        Me.lblProduct = New System.Windows.Forms.Label()
        Me.lblNumber = New System.Windows.Forms.Label()
        Me.lblDate = New System.Windows.Forms.Label()
        Me.txtDate = New System.Windows.Forms.TextBox()
        Me.PRODUCT_DISTRIBUTIONBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_PRODUCT_DISTRIBUTION = New DMI_RETAIL_INVENTORY.DS_PRODUCT_DISTRIBUTION()
        Me.PRODUCT_DISTRIBUTION_NUMBERTextBox = New System.Windows.Forms.TextBox()
        Me.WAREHOUSE_NAMEComboBox1 = New System.Windows.Forms.ComboBox()
        Me.WAREHOUSE1BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_WAREHOUSE = New DMI_RETAIL_INVENTORY.DS_WAREHOUSE()
        Me.WAREHOUSE_NAMEComboBox = New System.Windows.Forms.ComboBox()
        Me.WAREHOUSEBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.txtSourceQty = New System.Windows.Forms.TextBox()
        Me.txtQuantity = New System.Windows.Forms.TextBox()
        Me.cmdSearchName = New System.Windows.Forms.PictureBox()
        Me.PRODUCT_CODEComboBox = New System.Windows.Forms.ComboBox()
        Me.SP_LIST_PRODUCT_GOODSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_PRODUCT = New DMI_RETAIL_INVENTORY.DS_PRODUCT()
        Me.PRODUCT_NAMETextBox = New System.Windows.Forms.TextBox()
        Me.WAREHOUSE1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.WAREHOUSEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PRODUCTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.cmdMode = New System.Windows.Forms.Button()
        Me.WAREHOUSETableAdapter = New DMI_RETAIL_INVENTORY.DS_WAREHOUSETableAdapters.WAREHOUSETableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_INVENTORY.DS_WAREHOUSETableAdapters.TableAdapterManager()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.PRODUCTTableAdapter = New DMI_RETAIL_INVENTORY.DS_PRODUCTTableAdapters.PRODUCTTableAdapter()
        Me.TableAdapterManager1 = New DMI_RETAIL_INVENTORY.DS_PRODUCTTableAdapters.TableAdapterManager()
        Me.DS_PRODUCT_WAREHOUSE = New DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSE()
        Me.PRODUCT_WAREHOUSEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PRODUCT_WAREHOUSETableAdapter = New DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSETableAdapters.PRODUCT_WAREHOUSETableAdapter()
        Me.TableAdapterManager2 = New DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSETableAdapters.TableAdapterManager()
        Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter = New DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSETableAdapters.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter()
        Me.WAREHOUSE1TableAdapter = New DMI_RETAIL_INVENTORY.DS_WAREHOUSETableAdapters.WAREHOUSE1TableAdapter()
        Me.PRODUCT_DISTRIBUTIONTableAdapter = New DMI_RETAIL_INVENTORY.DS_PRODUCT_DISTRIBUTIONTableAdapters.PRODUCT_DISTRIBUTIONTableAdapter()
        Me.TableAdapterManager3 = New DMI_RETAIL_INVENTORY.DS_PRODUCT_DISTRIBUTIONTableAdapters.TableAdapterManager()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.SP_LIST_PRODUCT_GOODSTableAdapter = New DMI_RETAIL_INVENTORY.DS_PRODUCTTableAdapters.SP_LIST_PRODUCT_GOODSTableAdapter()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PRODUCT_DISTRIBUTIONBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_PRODUCT_DISTRIBUTION, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WAREHOUSE1BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_WAREHOUSE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WAREHOUSEBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_LIST_PRODUCT_GOODSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_PRODUCT, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WAREHOUSE1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_PRODUCT_WAREHOUSE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PRODUCT_WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblQuantity)
        Me.GroupBox1.Controls.Add(Me.lblQuantity1)
        Me.GroupBox1.Controls.Add(Me.lblDestination)
        Me.GroupBox1.Controls.Add(Me.lblSource)
        Me.GroupBox1.Controls.Add(Me.lblProduct)
        Me.GroupBox1.Controls.Add(Me.lblNumber)
        Me.GroupBox1.Controls.Add(Me.lblDate)
        Me.GroupBox1.Controls.Add(Me.txtDate)
        Me.GroupBox1.Controls.Add(Me.PRODUCT_DISTRIBUTION_NUMBERTextBox)
        Me.GroupBox1.Controls.Add(Me.WAREHOUSE_NAMEComboBox1)
        Me.GroupBox1.Controls.Add(Me.WAREHOUSE_NAMEComboBox)
        Me.GroupBox1.Controls.Add(Me.txtSourceQty)
        Me.GroupBox1.Controls.Add(Me.txtQuantity)
        Me.GroupBox1.Controls.Add(Me.cmdSearchName)
        Me.GroupBox1.Controls.Add(Me.PRODUCT_CODEComboBox)
        Me.GroupBox1.Controls.Add(Me.PRODUCT_NAMETextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(587, 203)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        '
        'lblQuantity
        '
        Me.lblQuantity.Location = New System.Drawing.Point(29, 162)
        Me.lblQuantity.Name = "lblQuantity"
        Me.lblQuantity.Size = New System.Drawing.Size(73, 15)
        Me.lblQuantity.TabIndex = 35
        Me.lblQuantity.Text = "Quantity"
        Me.lblQuantity.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblQuantity1
        '
        Me.lblQuantity1.Location = New System.Drawing.Point(235, 108)
        Me.lblQuantity1.Name = "lblQuantity1"
        Me.lblQuantity1.Size = New System.Drawing.Size(72, 15)
        Me.lblQuantity1.TabIndex = 34
        Me.lblQuantity1.Text = "Quantity"
        Me.lblQuantity1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblDestination
        '
        Me.lblDestination.Location = New System.Drawing.Point(17, 136)
        Me.lblDestination.Name = "lblDestination"
        Me.lblDestination.Size = New System.Drawing.Size(85, 15)
        Me.lblDestination.TabIndex = 33
        Me.lblDestination.Text = "Destination"
        Me.lblDestination.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblSource
        '
        Me.lblSource.Location = New System.Drawing.Point(26, 107)
        Me.lblSource.Name = "lblSource"
        Me.lblSource.Size = New System.Drawing.Size(76, 15)
        Me.lblSource.TabIndex = 32
        Me.lblSource.Text = "Source"
        Me.lblSource.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblProduct
        '
        Me.lblProduct.Location = New System.Drawing.Point(26, 78)
        Me.lblProduct.Name = "lblProduct"
        Me.lblProduct.Size = New System.Drawing.Size(76, 15)
        Me.lblProduct.TabIndex = 31
        Me.lblProduct.Text = "Product"
        Me.lblProduct.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblNumber
        '
        Me.lblNumber.Location = New System.Drawing.Point(26, 22)
        Me.lblNumber.Name = "lblNumber"
        Me.lblNumber.Size = New System.Drawing.Size(76, 15)
        Me.lblNumber.TabIndex = 30
        Me.lblNumber.Text = "Number"
        Me.lblNumber.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblDate
        '
        Me.lblDate.Location = New System.Drawing.Point(48, 50)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(54, 13)
        Me.lblDate.TabIndex = 29
        Me.lblDate.Text = "Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtDate
        '
        Me.txtDate.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtDate.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PRODUCT_DISTRIBUTIONBindingSource, "PRODUCT_DISTRIBUTION_NUMBER", True))
        Me.txtDate.Location = New System.Drawing.Point(108, 47)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.Size = New System.Drawing.Size(121, 22)
        Me.txtDate.TabIndex = 2
        Me.txtDate.Tag = "M"
        Me.txtDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'PRODUCT_DISTRIBUTIONBindingSource
        '
        Me.PRODUCT_DISTRIBUTIONBindingSource.DataMember = "PRODUCT_DISTRIBUTION"
        Me.PRODUCT_DISTRIBUTIONBindingSource.DataSource = Me.DS_PRODUCT_DISTRIBUTION
        '
        'DS_PRODUCT_DISTRIBUTION
        '
        Me.DS_PRODUCT_DISTRIBUTION.DataSetName = "DS_PRODUCT_DISTRIBUTION"
        Me.DS_PRODUCT_DISTRIBUTION.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PRODUCT_DISTRIBUTION_NUMBERTextBox
        '
        Me.PRODUCT_DISTRIBUTION_NUMBERTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PRODUCT_DISTRIBUTION_NUMBERTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PRODUCT_DISTRIBUTIONBindingSource, "PRODUCT_DISTRIBUTION_NUMBER", True))
        Me.PRODUCT_DISTRIBUTION_NUMBERTextBox.Location = New System.Drawing.Point(108, 19)
        Me.PRODUCT_DISTRIBUTION_NUMBERTextBox.Name = "PRODUCT_DISTRIBUTION_NUMBERTextBox"
        Me.PRODUCT_DISTRIBUTION_NUMBERTextBox.Size = New System.Drawing.Size(121, 22)
        Me.PRODUCT_DISTRIBUTION_NUMBERTextBox.TabIndex = 1
        Me.PRODUCT_DISTRIBUTION_NUMBERTextBox.Tag = "M"
        '
        'WAREHOUSE_NAMEComboBox1
        '
        Me.WAREHOUSE_NAMEComboBox1.DataSource = Me.WAREHOUSE1BindingSource1
        Me.WAREHOUSE_NAMEComboBox1.DisplayMember = "WAREHOUSE_NAME"
        Me.WAREHOUSE_NAMEComboBox1.FormattingEnabled = True
        Me.WAREHOUSE_NAMEComboBox1.Location = New System.Drawing.Point(108, 133)
        Me.WAREHOUSE_NAMEComboBox1.Name = "WAREHOUSE_NAMEComboBox1"
        Me.WAREHOUSE_NAMEComboBox1.Size = New System.Drawing.Size(121, 23)
        Me.WAREHOUSE_NAMEComboBox1.TabIndex = 7
        Me.WAREHOUSE_NAMEComboBox1.ValueMember = "WAREHOUSE_ID"
        '
        'WAREHOUSE1BindingSource1
        '
        Me.WAREHOUSE1BindingSource1.DataMember = "WAREHOUSE1"
        Me.WAREHOUSE1BindingSource1.DataSource = Me.DS_WAREHOUSE
        '
        'DS_WAREHOUSE
        '
        Me.DS_WAREHOUSE.DataSetName = "DS_WAREHOUSE"
        Me.DS_WAREHOUSE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'WAREHOUSE_NAMEComboBox
        '
        Me.WAREHOUSE_NAMEComboBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.WAREHOUSE_NAMEComboBox.DataSource = Me.WAREHOUSEBindingSource1
        Me.WAREHOUSE_NAMEComboBox.DisplayMember = "WAREHOUSE_NAME"
        Me.WAREHOUSE_NAMEComboBox.FormattingEnabled = True
        Me.WAREHOUSE_NAMEComboBox.Location = New System.Drawing.Point(108, 104)
        Me.WAREHOUSE_NAMEComboBox.Name = "WAREHOUSE_NAMEComboBox"
        Me.WAREHOUSE_NAMEComboBox.Size = New System.Drawing.Size(121, 23)
        Me.WAREHOUSE_NAMEComboBox.TabIndex = 5
        Me.WAREHOUSE_NAMEComboBox.ValueMember = "WAREHOUSE_ID"
        '
        'WAREHOUSEBindingSource1
        '
        Me.WAREHOUSEBindingSource1.DataMember = "WAREHOUSE"
        Me.WAREHOUSEBindingSource1.DataSource = Me.DS_WAREHOUSE
        '
        'txtSourceQty
        '
        Me.txtSourceQty.Enabled = False
        Me.txtSourceQty.Location = New System.Drawing.Point(313, 105)
        Me.txtSourceQty.Name = "txtSourceQty"
        Me.txtSourceQty.Size = New System.Drawing.Size(97, 22)
        Me.txtSourceQty.TabIndex = 6
        Me.txtSourceQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtQuantity
        '
        Me.txtQuantity.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtQuantity.Location = New System.Drawing.Point(108, 162)
        Me.txtQuantity.Name = "txtQuantity"
        Me.txtQuantity.Size = New System.Drawing.Size(121, 22)
        Me.txtQuantity.TabIndex = 8
        Me.txtQuantity.Tag = "M"
        Me.txtQuantity.Text = "0"
        Me.txtQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cmdSearchName
        '
        Me.cmdSearchName.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchName.Image = CType(resources.GetObject("cmdSearchName.Image"), System.Drawing.Image)
        Me.cmdSearchName.Location = New System.Drawing.Point(522, 74)
        Me.cmdSearchName.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchName.Name = "cmdSearchName"
        Me.cmdSearchName.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearchName.TabIndex = 19
        Me.cmdSearchName.TabStop = False
        '
        'PRODUCT_CODEComboBox
        '
        Me.PRODUCT_CODEComboBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PRODUCT_CODEComboBox.DataSource = Me.SP_LIST_PRODUCT_GOODSBindingSource
        Me.PRODUCT_CODEComboBox.DisplayMember = "PRODUCT_CODE"
        Me.PRODUCT_CODEComboBox.FormattingEnabled = True
        Me.PRODUCT_CODEComboBox.Location = New System.Drawing.Point(108, 75)
        Me.PRODUCT_CODEComboBox.Name = "PRODUCT_CODEComboBox"
        Me.PRODUCT_CODEComboBox.Size = New System.Drawing.Size(121, 23)
        Me.PRODUCT_CODEComboBox.TabIndex = 3
        Me.PRODUCT_CODEComboBox.Tag = "M"
        Me.PRODUCT_CODEComboBox.ValueMember = "PRODUCT_ID"
        '
        'SP_LIST_PRODUCT_GOODSBindingSource
        '
        Me.SP_LIST_PRODUCT_GOODSBindingSource.DataMember = "SP_LIST_PRODUCT_GOODS"
        Me.SP_LIST_PRODUCT_GOODSBindingSource.DataSource = Me.DS_PRODUCT
        '
        'DS_PRODUCT
        '
        Me.DS_PRODUCT.DataSetName = "DS_PRODUCT"
        Me.DS_PRODUCT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PRODUCT_NAMETextBox
        '
        Me.PRODUCT_NAMETextBox.Location = New System.Drawing.Point(235, 75)
        Me.PRODUCT_NAMETextBox.Name = "PRODUCT_NAMETextBox"
        Me.PRODUCT_NAMETextBox.ReadOnly = True
        Me.PRODUCT_NAMETextBox.Size = New System.Drawing.Size(280, 22)
        Me.PRODUCT_NAMETextBox.TabIndex = 4
        Me.PRODUCT_NAMETextBox.TabStop = False
        '
        'WAREHOUSE1BindingSource
        '
        Me.WAREHOUSE1BindingSource.DataMember = "WAREHOUSE1"
        Me.WAREHOUSE1BindingSource.DataSource = Me.DS_WAREHOUSE
        '
        'WAREHOUSEBindingSource
        '
        Me.WAREHOUSEBindingSource.DataMember = "WAREHOUSE"
        Me.WAREHOUSEBindingSource.DataSource = Me.DS_WAREHOUSE
        '
        'PRODUCTBindingSource
        '
        Me.PRODUCTBindingSource.DataMember = "PRODUCT"
        Me.PRODUCTBindingSource.DataSource = Me.DS_PRODUCT
        '
        'cmdMode
        '
        Me.cmdMode.Image = CType(resources.GetObject("cmdMode.Image"), System.Drawing.Image)
        Me.cmdMode.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdMode.Location = New System.Drawing.Point(360, 16)
        Me.cmdMode.Name = "cmdMode"
        Me.cmdMode.Size = New System.Drawing.Size(97, 37)
        Me.cmdMode.TabIndex = 10
        Me.cmdMode.Text = "&Save"
        Me.cmdMode.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdMode.UseVisualStyleBackColor = True
        '
        'WAREHOUSETableAdapter
        '
        Me.WAREHOUSETableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_INVENTORY.DS_WAREHOUSETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.WAREHOUSE1TableAdapter = Nothing
        Me.TableAdapterManager.WAREHOUSE2TableAdapter = Nothing
        Me.TableAdapterManager.WAREHOUSE3TableAdapter = Nothing
        Me.TableAdapterManager.WAREHOUSETableAdapter = Me.WAREHOUSETableAdapter
        '
        'cmdUndo
        '
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(246, 16)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(84, 37)
        Me.cmdUndo.TabIndex = 9
        Me.cmdUndo.Text = "&Undo"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'PRODUCTTableAdapter
        '
        Me.PRODUCTTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager1
        '
        Me.TableAdapterManager1.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager1.LOCATIONTableAdapter = Nothing
        Me.TableAdapterManager1.PRODUCTTableAdapter = Me.PRODUCTTableAdapter
        Me.TableAdapterManager1.UpdateOrder = DMI_RETAIL_INVENTORY.DS_PRODUCTTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'DS_PRODUCT_WAREHOUSE
        '
        Me.DS_PRODUCT_WAREHOUSE.DataSetName = "DS_PRODUCT_WAREHOUSE"
        Me.DS_PRODUCT_WAREHOUSE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PRODUCT_WAREHOUSEBindingSource
        '
        Me.PRODUCT_WAREHOUSEBindingSource.DataMember = "PRODUCT_WAREHOUSE"
        Me.PRODUCT_WAREHOUSEBindingSource.DataSource = Me.DS_PRODUCT_WAREHOUSE
        '
        'PRODUCT_WAREHOUSETableAdapter
        '
        Me.PRODUCT_WAREHOUSETableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager2
        '
        Me.TableAdapterManager2.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager2.PRODUCT_WAREHOUSETableAdapter = Me.PRODUCT_WAREHOUSETableAdapter
        Me.TableAdapterManager2.UpdateOrder = DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'SP_PRODUCT_WAREHOUSE_DETAILBindingSource
        '
        Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource.DataMember = "SP_PRODUCT_WAREHOUSE_DETAIL"
        Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource.DataSource = Me.DS_PRODUCT_WAREHOUSE
        '
        'SP_PRODUCT_WAREHOUSE_DETAILTableAdapter
        '
        Me.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.ClearBeforeFill = True
        '
        'WAREHOUSE1TableAdapter
        '
        Me.WAREHOUSE1TableAdapter.ClearBeforeFill = True
        '
        'PRODUCT_DISTRIBUTIONTableAdapter
        '
        Me.PRODUCT_DISTRIBUTIONTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager3
        '
        Me.TableAdapterManager3.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager3.PRODUCT_DISTRIBUTIONTableAdapter = Me.PRODUCT_DISTRIBUTIONTableAdapter
        Me.TableAdapterManager3.UpdateOrder = DMI_RETAIL_INVENTORY.DS_PRODUCT_DISTRIBUTIONTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdEdit)
        Me.GroupBox2.Controls.Add(Me.cmdDelete)
        Me.GroupBox2.Controls.Add(Me.cmdAdd)
        Me.GroupBox2.Controls.Add(Me.cmdUndo)
        Me.GroupBox2.Controls.Add(Me.cmdMode)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 221)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(587, 67)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        '
        'cmdEdit
        '
        Me.cmdEdit.Enabled = False
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(131, 16)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(79, 37)
        Me.cmdEdit.TabIndex = 24
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Enabled = False
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(487, 16)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(74, 37)
        Me.cmdDelete.TabIndex = 25
        Me.cmdDelete.Text = "&Delete"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Enabled = False
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(20, 16)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(82, 37)
        Me.cmdAdd.TabIndex = 23
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'SP_LIST_PRODUCT_GOODSTableAdapter
        '
        Me.SP_LIST_PRODUCT_GOODSTableAdapter.ClearBeforeFill = True
        '
        'frmEntryProductDistribution
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(613, 301)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmEntryProductDistribution"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Entry Product Distribution"
        Me.Text = "Entry Product Distribution"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PRODUCT_DISTRIBUTIONBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_PRODUCT_DISTRIBUTION, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WAREHOUSE1BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_WAREHOUSE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WAREHOUSEBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_LIST_PRODUCT_GOODSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_PRODUCT, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WAREHOUSE1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_PRODUCT_WAREHOUSE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PRODUCT_WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdMode As System.Windows.Forms.Button
    Friend WithEvents cmdSearchName As System.Windows.Forms.PictureBox
    Friend WithEvents PRODUCT_CODEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents PRODUCT_NAMETextBox As System.Windows.Forms.TextBox
    Friend WithEvents DS_WAREHOUSE As DMI_RETAIL_INVENTORY.DS_WAREHOUSE
    Friend WithEvents WAREHOUSEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents WAREHOUSETableAdapter As DMI_RETAIL_INVENTORY.DS_WAREHOUSETableAdapters.WAREHOUSETableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_INVENTORY.DS_WAREHOUSETableAdapters.TableAdapterManager
    Friend WithEvents txtQuantity As System.Windows.Forms.TextBox
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents DS_PRODUCT As DMI_RETAIL_INVENTORY.DS_PRODUCT
    Friend WithEvents PRODUCTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PRODUCTTableAdapter As DMI_RETAIL_INVENTORY.DS_PRODUCTTableAdapters.PRODUCTTableAdapter
    Friend WithEvents TableAdapterManager1 As DMI_RETAIL_INVENTORY.DS_PRODUCTTableAdapters.TableAdapterManager
    Friend WithEvents DS_PRODUCT_WAREHOUSE As DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSE
    Friend WithEvents PRODUCT_WAREHOUSEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PRODUCT_WAREHOUSETableAdapter As DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSETableAdapters.PRODUCT_WAREHOUSETableAdapter
    Friend WithEvents TableAdapterManager2 As DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSETableAdapters.TableAdapterManager
    Friend WithEvents SP_PRODUCT_WAREHOUSE_DETAILBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_PRODUCT_WAREHOUSE_DETAILTableAdapter As DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSETableAdapters.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter
    Friend WithEvents txtSourceQty As System.Windows.Forms.TextBox
    Friend WithEvents WAREHOUSE_NAMEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents WAREHOUSE1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents WAREHOUSE1TableAdapter As DMI_RETAIL_INVENTORY.DS_WAREHOUSETableAdapters.WAREHOUSE1TableAdapter
    Friend WithEvents WAREHOUSE_NAMEComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents WAREHOUSE1BindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents WAREHOUSEBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents DS_PRODUCT_DISTRIBUTION As DMI_RETAIL_INVENTORY.DS_PRODUCT_DISTRIBUTION
    Friend WithEvents PRODUCT_DISTRIBUTIONBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PRODUCT_DISTRIBUTIONTableAdapter As DMI_RETAIL_INVENTORY.DS_PRODUCT_DISTRIBUTIONTableAdapters.PRODUCT_DISTRIBUTIONTableAdapter
    Friend WithEvents TableAdapterManager3 As DMI_RETAIL_INVENTORY.DS_PRODUCT_DISTRIBUTIONTableAdapters.TableAdapterManager
    Friend WithEvents PRODUCT_DISTRIBUTION_NUMBERTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDate As System.Windows.Forms.TextBox
    Friend WithEvents SP_LIST_PRODUCT_GOODSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_LIST_PRODUCT_GOODSTableAdapter As DMI_RETAIL_INVENTORY.DS_PRODUCTTableAdapters.SP_LIST_PRODUCT_GOODSTableAdapter
    Friend WithEvents lblQuantity As System.Windows.Forms.Label
    Friend WithEvents lblQuantity1 As System.Windows.Forms.Label
    Friend WithEvents lblDestination As System.Windows.Forms.Label
    Friend WithEvents lblSource As System.Windows.Forms.Label
    Friend WithEvents lblProduct As System.Windows.Forms.Label
    Friend WithEvents lblNumber As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
End Class
