﻿Imports System.Data.SqlClient

Public Class frmListAccessPrivilege
    Public ACCESS_PRIVILEGEBindingSource As New BindingSource

    Private Sub getdata()
        dt = New DataTable
        sqladapter = New SqlDataAdapter("SELECT ACCESS_PRIVILEGE_ID, ACCESS_PRIVILEGE_NAME, " & _
                                        "EFFECTIVE_START_DATE, EFFECTIFE_END_DATE, USER_ID_INPUT, " & _
                                        "INPUT_DATE, USER_ID_UPDATE, UPDATE_DATE FROM ACCESS_PRIVILEGE " & _
                                        "ORDER BY ACCESS_PRIVILEGE_NAME", xConn)
        sqladapter.Fill(dt)
        ACCESS_PRIVILEGEBindingSource.DataSource = dt
        ACCESS_PRIVILEGEBindingNavigator.BindingSource = ACCESS_PRIVILEGEBindingSource
        dgvAccesPrivilege.DataSource = ACCESS_PRIVILEGEBindingSource
        dgvAccesPrivilege.Columns("ACCESS_PRIVILEGE_ID").Visible = False
        dgvAccesPrivilege.Columns("EFFECTIVE_START_DATE").Visible = False
        dgvAccesPrivilege.Columns("EFFECTIFE_END_DATE").Visible = False
        dgvAccesPrivilege.Columns("USER_ID_INPUT").Visible = False
        dgvAccesPrivilege.Columns("INPUT_DATE").Visible = False
        dgvAccesPrivilege.Columns("USER_ID_UPDATE").Visible = False
        dgvAccesPrivilege.Columns("UPDATE_DATE").Visible = False
    End Sub

    Private Sub frmListAccessPrivilege_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_ACCESS_PRIVILEGE.ACCESS_PRIVILEGE' table. You can move, or remove it, as needed.
        'Me.ACCESS_PRIVILEGETableAdapter.Fill(Me.DS_ACCESS_PRIVILEGE.ACCESS_PRIVILEGE)
        getdata()
        If Language = "Indonesian" Then
            Me.Text = "Daftar Akses"
            dgvAccesPrivilege.Columns("ACCESS_PRIVILEGE_NAME").HeaderText = "Akses"
            chkDeleted.Text = "Tidak terhapus"
        Else
            dgvAccesPrivilege.Columns("ACCESS_PRIVILEGE_NAME").HeaderText = "Access"
        End If
        chkDeleted.Checked = False
    End Sub

    Private Sub dgvAccesPrivilege_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvAccesPrivilege.DoubleClick
        frmAccessPrivilege.MdiParent = frmMain
        frmAccessPrivilege.Show()
        frmAccessPrivilege.BringToFront()
        tmpRecentForm = New frmAccessPrivilege
        frmAccessPrivilege.ACCESS_PRIVILEGEBindingSource.Position = frmAccessPrivilege.ACCESS_PRIVILEGEBindingSource.Find("ACCESS_PRIVILEGE_ID", ACCESS_PRIVILEGEBindingSource.Current("ACCESS_PRIVILEGE_ID"))
        Me.Close()
    End Sub

    Private Sub dgvAccesPrivilege_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvAccesPrivilege.KeyDown
        If e.KeyCode = Keys.Enter Then
            frmAccessPrivilege.MdiParent = frmMain
            frmAccessPrivilege.Show()
            frmAccessPrivilege.BringToFront()
            tmpRecentForm = New frmAccessPrivilege
            frmAccessPrivilege.ACCESS_PRIVILEGEBindingSource.Position = frmAccessPrivilege.ACCESS_PRIVILEGEBindingSource.Find("ACCESS_PRIVILEGE_ID", ACCESS_PRIVILEGEBindingSource.Current("ACCESS_PRIVILEGE_ID"))
            Me.Close()
        ElseIf e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        Try
            ACCESS_PRIVILEGEBindingSource.Filter = "ACCESS_PRIVILEGE_NAME LIKE '%" & TextBox1.Text.ToUpper & "%'"
        Catch
            TextBox1.Text = ""
            TextBox1.Focus()
        End Try
    End Sub


    Private Sub chkDeleted_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDeleted.CheckedChanged
        If chkDeleted.Checked = True Then
            For x As Integer = 0 To dgvAccesPrivilege.RowCount - 1
                ACCESS_PRIVILEGEBindingSource.Position = x
                If ACCESS_PRIVILEGEBindingSource.Current("EFFECTIFE_END_DATE") < Now Then
                    dgvAccesPrivilege.Rows.RemoveAt(dgvAccesPrivilege.CurrentCell.RowIndex)
                End If
            Next
        Else
            getdata()
            'Me.ACCESS_PRIVILEGETableAdapter.Fill(Me.DS_ACCESS_PRIVILEGE.ACCESS_PRIVILEGE)
        End If

        ACCESS_PRIVILEGEBindingSource.Position = 0
    End Sub
End Class