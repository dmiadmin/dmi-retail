﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frmListPurchase
    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Dim xDreader As SqlDataReader

    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub

    'Private Sub frmListPurchase_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
    '    If e.KeyCode = Keys.F12 Then
    '        cmdReport_Click(Nothing, Nothing)
    '    End If
    'End Sub

    Private Sub frmListPurchase_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_PURCHASE.VIEW_LIST_PURCHASE_DETAIL' table. You can move, or remove it, as needed.
        'Me.VIEW_LIST_PURCHASE_DETAILTableAdapter.Fill(Me.DS_PURCHASE.VIEW_LIST_PURCHASE_DETAIL)
        'TODO: This line of code loads data into the 'DS_PURCHASE.VIEW_LIST_PURCHASE' table. You can move, or remove it, as needed.
        'Me.VIEW_LIST_PURCHASETableAdapter.Fill(Me.DS_PURCHASE.VIEW_LIST_PURCHASE)
        If Language = "Indonesian" Then
            Me.Text = "Daftar Pembelian"
            lblFrom.Text = "Dari Periode"
            lblTo.Text = "s/d"
            cmdGenerate.Text = "PROSES"
            cmdReport.Text = "Cetak"
            lblNoOfTrans.Text = "Jumlah Item(s) : " & SP_LIST_PURCHASEDataGridView.RowCount
        Else
            lblNoOfTrans.Text = "No of transaction(s) : " & SP_LIST_PURCHASEDataGridView.RowCount
        End If

        DateTimePicker1.Value = DateSerial(Today.Year, Today.Month, 1)
        DateTimePicker2.Value = DateSerial(Today.Year, Today.Month + 1, 0)
        Call connection()
    End Sub

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        'Try

        ' Me.SP_LIST_PURCHASETableAdapter.Fill(Me.DS_PURCHASE.SP_LIST_PURCHASE, DateTimePicker1.Value, DateTimePicker2.Value)
        Dim xDS_PURCHASE As New DataSet
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        Dim tmpDate1 As Date = DateTimePicker1.Value
        Dim tmpdate2 As Date = DateTimePicker2.Value

        sql = " DECLARE	@return_value int" & _
                            " EXEC	@return_value = [dbo].[SP_LIST_PURCHASE]" & _
                            " @PERIOD1 = '" & tmpDate1 & "'," & _
                            " @PERIOD2 = '" & tmpdate2 & "'" & _
                            " SELECT	'Return Value' = @return_value"
        Try
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.SP_LIST_PURCHASEDataGridView.DataSource = dt
            If Language = "Indonesian" Then
                SP_LIST_PURCHASEDataGridView.Columns("RECEIPT_NO").HeaderText = "No Faktur"
                SP_LIST_PURCHASEDataGridView.Columns("PURCHASE_DATE").HeaderText = "Tanggal Pembelian"
                SP_LIST_PURCHASEDataGridView.Columns("VENDOR_NAME").HeaderText = "Supplier"
                SP_LIST_PURCHASEDataGridView.Columns("PAYMENT_METHOD_NAME").HeaderText = "Cara Pembayaran"
                SP_LIST_PURCHASEDataGridView.Columns("MATURITY_DATE").HeaderText = "Jatuh Tempo"
                SP_LIST_PURCHASEDataGridView.Columns("NUMBER_OF_ITEM").HeaderText = "Jumlah Barang"
                SP_LIST_PURCHASEDataGridView.Columns("GRAND_TOTAL").HeaderText = "Total"
                SP_LIST_PURCHASEDataGridView.Columns("WAREHOUSE_NAME").HeaderText = "Gudang"
                SP_LIST_PURCHASEDataGridView.Columns("VOID").HeaderText = "Batal"
                SP_LIST_PURCHASEDataGridView.Columns("TAX_AMOUNT").HeaderText = "Pajak"
                SP_LIST_PURCHASEDataGridView.Columns("AMOUNT_DISCOUNT").HeaderText = "Disc"
                SP_LIST_PURCHASEDataGridView.Columns("DOWN_PAYMENT").HeaderText = "DP"
                ''''Setting dataGridView''''
                SP_LIST_PURCHASEDataGridView.Columns("RECEIPT_NO").Width = 70
                SP_LIST_PURCHASEDataGridView.Columns("PURCHASE_DATE").Width = 80
                SP_LIST_PURCHASEDataGridView.Columns("MATURITY_DATE").Width = 80
                SP_LIST_PURCHASEDataGridView.Columns("PAYMENT_METHOD_NAME").Width = 60
                SP_LIST_PURCHASEDataGridView.Columns("NUMBER_OF_ITEM").Width = 50
                SP_LIST_PURCHASEDataGridView.Columns("VENDOR_NAME").Width = 70
                SP_LIST_PURCHASEDataGridView.Columns("WAREHOUSE_NAME").Width = 75
                SP_LIST_PURCHASEDataGridView.Columns("VOID").Width = 50
                SP_LIST_PURCHASEDataGridView.Columns("TAX_AMOUNT").Width = 50
                SP_LIST_PURCHASEDataGridView.Columns("AMOUNT_DISCOUNT").Width = 50
                SP_LIST_PURCHASEDataGridView.Columns("DOWN_PAYMENT").Width = 50
                SP_LIST_PURCHASEDataGridView.Columns("GRAND_TOTAL").Width = 80
                SP_LIST_PURCHASEDataGridView.Columns("PURCHASE_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_PURCHASEDataGridView.Columns("MATURITY_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_PURCHASEDataGridView.Columns("NUMBER_OF_ITEM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_PURCHASEDataGridView.Columns("GRAND_TOTAL").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_PURCHASEDataGridView.Columns("TAX_AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_PURCHASEDataGridView.Columns("AMOUNT_DISCOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_PURCHASEDataGridView.Columns("DOWN_PAYMENT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_PURCHASEDataGridView.Columns("TAX_AMOUNT").DefaultCellStyle.Format = "n0"
                SP_LIST_PURCHASEDataGridView.Columns("AMOUNT_DISCOUNT").DefaultCellStyle.Format = "n0"
                SP_LIST_PURCHASEDataGridView.Columns("DOWN_PAYMENT").DefaultCellStyle.Format = "n0"
                SP_LIST_PURCHASEDataGridView.Columns("GRAND_TOTAL").DefaultCellStyle.Format = "n0"
                SP_LIST_PURCHASEDataGridView.Columns("MATURITY_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
                SP_LIST_PURCHASEDataGridView.Columns("PURCHASE_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
                SP_LIST_PURCHASEDataGridView.Columns("PURCHASE_ID").Visible = False
                SP_LIST_PURCHASEDataGridView.Columns("VENDOR_ID").Visible = False
                SP_LIST_PURCHASEDataGridView.Columns("WAREHOUSE_ID").Visible = False
                SP_LIST_PURCHASEDataGridView.Columns("REMARK").Visible = False
                SP_LIST_PURCHASEDataGridView.Columns("PAYMENT_METHOD_ID").Visible = False
                SP_LIST_PURCHASEDataGridView.Columns("REP_PERIOD1").Visible = False
                SP_LIST_PURCHASEDataGridView.Columns("REP_PERIOD2").Visible = False
                SP_LIST_PURCHASEDataGridView.Columns("PURCHASE_ORDER_ID").Visible = False
                SP_LIST_PURCHASEDataGridView.Columns("PO_NO").Visible = False
                SP_LIST_PURCHASEDataGridView.Columns("PO_DATE").Visible = False
            Else
                SP_LIST_PURCHASEDataGridView.Columns("RECEIPT_NO").HeaderText = "Receipt No"
                SP_LIST_PURCHASEDataGridView.Columns("PURCHASE_DATE").HeaderText = "Purchase date"
                SP_LIST_PURCHASEDataGridView.Columns("VENDOR_NAME").HeaderText = "Vendor"
                SP_LIST_PURCHASEDataGridView.Columns("PAYMENT_METHOD_NAME").HeaderText = "Payment Method"
                SP_LIST_PURCHASEDataGridView.Columns("MATURITY_DATE").HeaderText = "Maturity Date"
                SP_LIST_PURCHASEDataGridView.Columns("NUMBER_OF_ITEM").HeaderText = "No Of Item"
                SP_LIST_PURCHASEDataGridView.Columns("GRAND_TOTAL").HeaderText = "Total"
                SP_LIST_PURCHASEDataGridView.Columns("WAREHOUSE_NAME").HeaderText = "Warehouse"
                SP_LIST_PURCHASEDataGridView.Columns("VOID").HeaderText = "Void"
                SP_LIST_PURCHASEDataGridView.Columns("TAX_AMOUNT").HeaderText = "Tax"
                SP_LIST_PURCHASEDataGridView.Columns("AMOUNT_DISCOUNT").HeaderText = "Disc"
                SP_LIST_PURCHASEDataGridView.Columns("DOWN_PAYMENT").HeaderText = "DP"
                ''''Setting dataGridView''''
                SP_LIST_PURCHASEDataGridView.Columns("RECEIPT_NO").Width = 70
                SP_LIST_PURCHASEDataGridView.Columns("PURCHASE_DATE").Width = 80
                SP_LIST_PURCHASEDataGridView.Columns("MATURITY_DATE").Width = 80
                SP_LIST_PURCHASEDataGridView.Columns("PAYMENT_METHOD_NAME").Width = 60
                SP_LIST_PURCHASEDataGridView.Columns("NUMBER_OF_ITEM").Width = 50
                SP_LIST_PURCHASEDataGridView.Columns("VENDOR_NAME").Width = 70
                SP_LIST_PURCHASEDataGridView.Columns("WAREHOUSE_NAME").Width = 75
                SP_LIST_PURCHASEDataGridView.Columns("VOID").Width = 50
                SP_LIST_PURCHASEDataGridView.Columns("TAX_AMOUNT").Width = 50
                SP_LIST_PURCHASEDataGridView.Columns("AMOUNT_DISCOUNT").Width = 50
                SP_LIST_PURCHASEDataGridView.Columns("DOWN_PAYMENT").Width = 50
                SP_LIST_PURCHASEDataGridView.Columns("GRAND_TOTAL").Width = 80
                SP_LIST_PURCHASEDataGridView.Columns("PURCHASE_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_PURCHASEDataGridView.Columns("MATURITY_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_PURCHASEDataGridView.Columns("NUMBER_OF_ITEM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_PURCHASEDataGridView.Columns("GRAND_TOTAL").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_PURCHASEDataGridView.Columns("TAX_AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_PURCHASEDataGridView.Columns("AMOUNT_DISCOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_PURCHASEDataGridView.Columns("DOWN_PAYMENT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_PURCHASEDataGridView.Columns("TAX_AMOUNT").DefaultCellStyle.Format = "n0"
                SP_LIST_PURCHASEDataGridView.Columns("AMOUNT_DISCOUNT").DefaultCellStyle.Format = "n0"
                SP_LIST_PURCHASEDataGridView.Columns("DOWN_PAYMENT").DefaultCellStyle.Format = "n0"
                SP_LIST_PURCHASEDataGridView.Columns("GRAND_TOTAL").DefaultCellStyle.Format = "n0"
                SP_LIST_PURCHASEDataGridView.Columns("MATURITY_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
                SP_LIST_PURCHASEDataGridView.Columns("PURCHASE_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
                SP_LIST_PURCHASEDataGridView.Columns("PURCHASE_ID").Visible = False
                SP_LIST_PURCHASEDataGridView.Columns("VENDOR_ID").Visible = False
                SP_LIST_PURCHASEDataGridView.Columns("WAREHOUSE_ID").Visible = False
                SP_LIST_PURCHASEDataGridView.Columns("REMARK").Visible = False
                SP_LIST_PURCHASEDataGridView.Columns("REP_PERIOD1").Visible = False
                SP_LIST_PURCHASEDataGridView.Columns("REP_PERIOD2").Visible = False
                SP_LIST_PURCHASEDataGridView.Columns("PAYMENT_METHOD_ID").Visible = False
                SP_LIST_PURCHASEDataGridView.Columns("PURCHASE_ORDER_ID").Visible = False
                SP_LIST_PURCHASEDataGridView.Columns("PO_NO").Visible = False
                SP_LIST_PURCHASEDataGridView.Columns("PO_DATE").Visible = False
            End If
        Catch
        End Try

        If Language = "Indonesian" Then
            lblNoOfTrans.Text = "Jumlah Item(s) : " & SP_LIST_PURCHASEDataGridView.RowCount
        Else
            lblNoOfTrans.Text = "No of transaction(s) : " & SP_LIST_PURCHASEDataGridView.RowCount
        End If

        xConn.Close()
    End Sub

    'Private Sub SP_LIST_PURCHASEDataGridView_CellDoubleClick1(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles SP_LIST_PURCHASEDataGridView.CellDoubleClick
    '    Try

    '        With frmEntryPurchase
    '            .tmpPurchaseId = SP_LIST_PURCHASEBindingSource.Current("PURCHASE_ID")
    '            .tmpReceipt = SP_LIST_PURCHASEBindingSource.Current("RECEIPT_NO")
    '            .PURCHASE_DATEDateTimePicker.Value = SP_LIST_PURCHASEBindingSource.Current("PURCHASE_DATE")
    '            .tmpVendor = SP_LIST_PURCHASEBindingSource.Current("VENDOR_ID")
    '            .tmpPaymentMethod = SP_LIST_PURCHASEBindingSource.Current("PAYMENT_METHOD_ID")
    '            .MATURITY_DATEDateTimePicker.Value = SP_LIST_PURCHASEBindingSource.Current("MATURITY_DATE")
    '            .DOWN_PAYMENTTextBox.Text = FormatNumber(SP_LIST_PURCHASEBindingSource.Current("DOWN_PAYMENT"), 0)
    '            .TAX_AMOUNTTextBox.Text = FormatNumber(SP_LIST_PURCHASEBindingSource.Current("TAX_AMOUNT"), 0)
    '            .AMOUNT_DISCOUNTTextBox.Text = FormatNumber(SP_LIST_PURCHASEBindingSource.Current("AMOUNT_DISCOUNT"), 0)
    '            .GRAND_TOTALTextBox.Text = FormatNumber(SP_LIST_PURCHASEBindingSource.Current("GRAND_TOTAL"), 0)
    '            .REMARKTextBox.Text = SP_LIST_PURCHASEBindingSource.Current("REMARK")
    '            If SP_LIST_PURCHASEDataGridView.Item("WAREHOUSE_ID", SP_LIST_PURCHASEDataGridView.CurrentCell.RowIndex).Value Is DBNull.Value Then
    '                .tmpWarehouseID = 0
    '            Else
    '                .tmpWarehouseID = SP_LIST_PURCHASEBindingSource.Current("WAREHOUSE_ID")
    '            End If
    '            If SP_LIST_PURCHASEDataGridView.Item("WAREHOUSE_NAME", SP_LIST_PURCHASEDataGridView.CurrentCell.RowIndex).Value Is DBNull.Value Then
    '                .tmpWareHouseName = " "
    '            Else
    '                .tmpWareHouseName = SP_LIST_PURCHASEBindingSource.Current("WAREHOUSE_NAME")
    '            End If

    '            .cmdAdd.Enabled = False
    '            .cmdEdit.Enabled = False
    '            .cmdUndo.Enabled = True
    '            .cmdSave.Enabled = False
    '            If SP_LIST_PURCHASEDataGridView.Item("VOID", SP_LIST_PURCHASEDataGridView.CurrentCell.RowIndex).Value Is DBNull.Value Then
    '                .cmdDelete.Enabled = True
    '            Else
    '                If Not SP_LIST_PURCHASEDataGridView.Item("VOID", SP_LIST_PURCHASEDataGridView.CurrentCell.RowIndex).Value = "TRUE" Then
    '                    .cmdDelete.Enabled = True
    '                End If
    '            End If
    '            .tmpSaveMode = "Update"
    '        End With

    '        frmEntryPurchase.ShowDialog(Me)
    '        frmEntryPurchase.Close()

    '        Me.VIEW_LIST_PURCHASE_DETAILTableAdapter.Fill(Me.DS_PURCHASE.VIEW_LIST_PURCHASE_DETAIL)
    '        Me.SP_LIST_PURCHASETableAdapter.Fill(Me.DS_PURCHASE.SP_LIST_PURCHASE, DateTimePicker1.Value, DateTimePicker2.Value)

    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click

    '    Dim objf As New frmRepPurchase
    '    objf.date1 = DateTimePicker1.Value
    '    objf.date2 = DateTimePicker2.Value
    '    objf.ReportViewer1.ShowRefreshButton = False
    '    objf.ReportViewer1.ZoomPercent = 100
    '    objf.WindowState = FormWindowState.Maximized
    '    objf.Show()
    'End Sub

    Private Sub SP_LIST_PURCHASEDataGridView_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles SP_LIST_PURCHASEDataGridView.CellDoubleClick
        Try

            With frmEntryPurchase
                .tmpPurchaseId = SP_LIST_PURCHASEDataGridView.Item("PURCHASE_ID", e.RowIndex).Value
                .tmpReceipt = SP_LIST_PURCHASEDataGridView.Item("RECEIPT_NO", e.RowIndex).Value
                .PURCHASE_DATEDateTimePicker.Value = SP_LIST_PURCHASEDataGridView.Item("PURCHASE_DATE", e.RowIndex).Value
                .tmpVendor = SP_LIST_PURCHASEDataGridView.Item("VENDOR_ID", e.RowIndex).Value
                .tmpPaymentMethod = SP_LIST_PURCHASEDataGridView.Item("PAYMENT_METHOD_ID", e.RowIndex).Value
                .MATURITY_DATEDateTimePicker.Value = SP_LIST_PURCHASEDataGridView.Item("MATURITY_DATE", e.RowIndex).Value
                .DOWN_PAYMENTTextBox.Text = FormatNumber(SP_LIST_PURCHASEDataGridView.Item("DOWN_PAYMENT", e.RowIndex).Value, 0)
                .TAX_AMOUNTTextBox.Text = FormatNumber(SP_LIST_PURCHASEDataGridView.Item("TAX_AMOUNT", e.RowIndex).Value, 0)
                .AMOUNT_DISCOUNTTextBox.Text = FormatNumber(SP_LIST_PURCHASEDataGridView.Item("AMOUNT_DISCOUNT", e.RowIndex).Value, 0)
                .GRAND_TOTALTextBox.Text = FormatNumber(SP_LIST_PURCHASEDataGridView.Item("GRAND_TOTAL", e.RowIndex).Value, 0)
                .REMARKTextBox.Text = SP_LIST_PURCHASEDataGridView.Item("REMARK", e.RowIndex).Value.ToString
                If Not IsDBNull(SP_LIST_PURCHASEDataGridView.Item("PURCHASE_ORDER_ID", e.RowIndex).Value.ToString) Then
                    .tmpPurchaseOrderId = SP_LIST_PURCHASEDataGridView.Item("PURCHASE_ORDER_ID", e.RowIndex).Value
                Else
                    .tmpPurchaseOrderId = 0
                End If

                If Not IsDBNull(SP_LIST_PURCHASEDataGridView.Item("PO_NO", e.RowIndex).Value) Then
                    .tmpPoNo = SP_LIST_PURCHASEDataGridView.Item("PO_NO", e.RowIndex).Value
                Else
                    .tmpPoNo = ""
                End If

                If Not IsDBNull(SP_LIST_PURCHASEDataGridView.Item("PO_DATE", e.RowIndex).Value) Then
                    .tmpPurchaseOrderDate = SP_LIST_PURCHASEDataGridView.Item("PO_DATE", e.RowIndex).Value
                Else
                    .tmpPurchaseOrderDate = Now
                End If

                If SP_LIST_PURCHASEDataGridView.Item("WAREHOUSE_ID", e.RowIndex).Value Is DBNull.Value Then
                    .tmpWarehouseID = 0
                Else
                    If SP_LIST_PURCHASEDataGridView.Item("WAREHOUSE_ID", e.RowIndex).Value = 0 Then
                        .tmpWarehouseID = 0
                    Else
                        .tmpWarehouseID = SP_LIST_PURCHASEDataGridView.Item("WAREHOUSE_ID", e.RowIndex).Value
                    End If
                End If

                'If SP_LIST_PURCHASEDataGridView.Item("WAREHOUSE_NAME", SP_LIST_PURCHASEDataGridView.CurrentCell.RowIndex).Value Is DBNull.Value Then
                '    .tmpWareHouseName = " "
                'Else
                '    .tmpWareHouseName = SP_LIST_PURCHASEBindingSource.Current("WAREHOUSE_NAME")
                'End If

                .cmdAdd.Enabled = False
                .cmdEdit.Enabled = False
                .cmdUndo.Enabled = True
                .cmdSave.Enabled = False
                If SP_LIST_PURCHASEDataGridView.Item("VOID", e.RowIndex).Value.ToString = "" Then
                    .cmdDelete.Enabled = True
                Else
                    If SP_LIST_PURCHASEDataGridView.Item("VOID", e.RowIndex).Value.ToString = "TRUE" Then
                        .cmdDelete.Enabled = False
                    End If
                End If
                .tmpSaveMode = "Update"
            End With

            frmEntryPurchase.ShowDialog(Me)
            frmEntryPurchase.Close()

        Catch ex As Exception

        End Try
        cmdGenerate_Click(Nothing, Nothing)
    End Sub
End Class