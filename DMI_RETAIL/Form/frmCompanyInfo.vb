﻿Imports System.Data.SqlClient

Public Class frmCompanyInfo
    Dim tmpSaveMode As String
    Dim tmpChange As Boolean
    Dim CompanyBindingSource As New BindingSource

    Private Sub frmCompanyInfo_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If tmpChange = True And COMPANY_NAMETextBox.Enabled = True Then
            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah Anda ingin menyimpan perubahan " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If
            If tmpvar = 6 Then
                cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        Else
            Me.Dispose()
        End If
    End Sub

    Private Sub GetData()
        sql = "SELECT COMPANY_NAME, ADDRESS_LINE_1, ADDRESS_LINE_2, ADDRESS_LINE_3, REGENCY, " & _
              "CITY, STATE, POSTAL_CODE, PHONE_1, PHONE_2, FAX, OWNER_NAME, TAX_NUMBER, " & _
              "USER_ID_INPUT, INPUT_DATE, USER_ID_UPDATE, UPDATE_DATE FROM dbo.COMPANY_INFORMATION"
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter = New SqlDataAdapter(sql, xConn)
        dt = New DataTable
        sqladapter.Fill(dt)
        CompanyBindingSource.DataSource = dt

        COMPANY_NAMETextBox.Text = CompanyBindingSource.Current("COMPANY_NAME")
        ADDRESS_LINE_1TextBox.Text = CompanyBindingSource.Current("ADDRESS_LINE_1")
        ADDRESS_LINE_2TextBox.Text = CompanyBindingSource.Current("ADDRESS_LINE_2")
        ADDRESS_LINE_3TextBox.Text = CompanyBindingSource.Current("ADDRESS_LINE_3")
        REGENCYTextBox.Text = CompanyBindingSource.Current("REGENCY")
        CITYTextBox.Text = CompanyBindingSource.Current("CITY")
        STATETextBox.Text = CompanyBindingSource.Current("STATE")
        POSTAL_CODETextBox.Text = CompanyBindingSource.Current("POSTAL_CODE")
        PHONE_1TextBox.Text = CompanyBindingSource.Current("PHONE_1")
        PHONE_2TextBox.Text = CompanyBindingSource.Current("PHONE_2")
        FAXTextBox.Text = CompanyBindingSource.Current("FAX")
        OWNER_NAMETextBox.Text = CompanyBindingSource.Current("OWNER_NAME")
        TAX_NUMBERTextBox.Text = CompanyBindingSource.Current("TAX_NUMBER")

    End Sub

    Private Sub frmCompanyInfo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetData()
        AccessPrivilege(Me.Text)

        If Language = "Indonesian" Then
            lblCompany.Text = "Nama Perusahaan"
            lblAddress.Text = "Alamat"
            lblRegency.Text = "Kabupaten"
            lblCity.Text = "Kota"
            lblState.Text = "Provinsi"
            lblPos.Text = "Kode Pos"
            lblOwner.Text = "Nama Pemilik"
            lblTax.Text = "Nomor Pajak"
            cmdAdd.Text = "Tambah"
            cmdEdit.Text = "Ubah"
            cmdUndo.Text = "Batal"
            cmdSave.Text = "Simpan"
            cmdDelete.Text = "Hapus"
            Me.Text = "Informasi Perusahaan"
        End If
        DisableInputBox(Me)

        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = False
        tmpChange = False

    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If AllowAdd = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Insert"

        If COMPANY_NAMETextBox.Text <> "" Then
            If Language = "Indonesian" Then
                MsgBox("Perusahaan sudah ada !" & vbCrLf & "Menambahkan perusahaan yang baru tidak diperbolehkan !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is a Company exists !" & vbCrLf & "Adding new Company does not allowed !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        EnableInputBox(Me)
        CompanyBindingSource.AddNew()

        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        tmpChange = False
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        If COMPANY_NAMETextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak ada perusahaan untuk di ubah !", MsgBoxStyle.Information, "DMI Retail")
            Else
                MsgBox("There is no Company to edit !", MsgBoxStyle.Information, "DMI Retail")
            End If
            Exit Sub
        End If

        If AllowEdit = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Update"

        EnableInputBox(Me)

        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        tmpChange = False
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        tmpSaveMode = ""

        DisableInputBox(Me)
        CompanyBindingSource.CancelEdit()

        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = False
        tmpChange = False
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Me.Validate()

        If COMPANY_NAMETextBox.Text = "" Or ADDRESS_LINE_1TextBox.Text = "" Or CITYTextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpSaveMode = "Insert" Then
            xComm = New SqlCommand("SP_COMPANY_INFORMATION", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "I")
            xComm.Parameters.AddWithValue("@COMPANY_NAME", COMPANY_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@ADDRESS_LINE_1", ADDRESS_LINE_1TextBox.Text)
            xComm.Parameters.AddWithValue("@ADDRESS_LINE_2", ADDRESS_LINE_2TextBox.Text)
            xComm.Parameters.AddWithValue("@ADDRESS_LINE_3", ADDRESS_LINE_3TextBox.Text)
            xComm.Parameters.AddWithValue("@REGENCY", REGENCYTextBox.Text)
            xComm.Parameters.AddWithValue("@CITY", CITYTextBox.Text)
            xComm.Parameters.AddWithValue("@STATE", STATETextBox.Text)
            xComm.Parameters.AddWithValue("@POSTAL_CODE", POSTAL_CODETextBox.Text)
            xComm.Parameters.AddWithValue("@PHONE_1", PHONE_1TextBox.Text)
            xComm.Parameters.AddWithValue("@PHONE_2", PHONE_2TextBox.Text)
            xComm.Parameters.AddWithValue("@FAX", FAXTextBox.Text)
            xComm.Parameters.AddWithValue("@OWNER_NAME", OWNER_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@TAX_NUMBER", TAX_NUMBERTextBox.Text)
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", UserId)
            xComm.Parameters.AddWithValue("@INPUT_DATE", Now)
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", 0)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", DateSerial(4000, 12, 31))
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Close()
            xComm.ExecuteNonQuery()
        ElseIf tmpSaveMode = "Update" Then
            xComm = New SqlCommand("SP_COMPANY_INFORMATION", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "U")
            xComm.Parameters.AddWithValue("@COMPANY_NAME", COMPANY_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@ADDRESS_LINE_1", ADDRESS_LINE_1TextBox.Text)
            xComm.Parameters.AddWithValue("@ADDRESS_LINE_2", ADDRESS_LINE_2TextBox.Text)
            xComm.Parameters.AddWithValue("@ADDRESS_LINE_3", ADDRESS_LINE_3TextBox.Text)
            xComm.Parameters.AddWithValue("@REGENCY", REGENCYTextBox.Text)
            xComm.Parameters.AddWithValue("@CITY", CITYTextBox.Text)
            xComm.Parameters.AddWithValue("@STATE", STATETextBox.Text)
            xComm.Parameters.AddWithValue("@POSTAL_CODE", POSTAL_CODETextBox.Text)
            xComm.Parameters.AddWithValue("@PHONE_1", PHONE_1TextBox.Text)
            xComm.Parameters.AddWithValue("@PHONE_2", PHONE_2TextBox.Text)
            xComm.Parameters.AddWithValue("@FAX", FAXTextBox.Text)
            xComm.Parameters.AddWithValue("@OWNER_NAME", OWNER_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@TAX_NUMBER", TAX_NUMBERTextBox.Text)
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", CompanyBindingSource.Current("USER_ID_INPUT"))
            xComm.Parameters.AddWithValue("@INPUT_DATE", CompanyBindingSource.Current("INPUT_DATE"))
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", UserId)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", Now)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()
        End If

        tmpSaveMode = ""

        DisableInputBox(Me)
        CompanyBindingSource.CancelEdit()

        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = False

        GetData()
        If Language = "Indonesian" Then
            MsgBox("Data Berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
        End If
    End Sub

    Private Sub POSTAL_CODETextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles POSTAL_CODETextBox.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""
    End Sub

    Private Sub COMPANY_NAMETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles COMPANY_NAMETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub ADDRESS_LINE_1TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ADDRESS_LINE_1TextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub ADDRESS_LINE_2TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ADDRESS_LINE_2TextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub ADDRESS_LINE_3TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ADDRESS_LINE_3TextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub REGENCYTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles REGENCYTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub CITYTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CITYTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub STATETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles STATETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub POSTAL_CODETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles POSTAL_CODETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub PHONE_1TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PHONE_1TextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub PHONE_2TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PHONE_2TextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub FAXTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FAXTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub OWNER_NAMETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OWNER_NAMETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub TAX_NUMBERTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TAX_NUMBERTextBox.TextChanged
        tmpChange = True
    End Sub

End Class