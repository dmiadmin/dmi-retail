﻿Public Class frmSearchPayableInvoice

    Dim tmpFilterEdit As Boolean

    Private Sub frmSearchPayableInvoice_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_PAYMENT_TRANSACTION.SP_PAYABLE_INVOICE' table. You can move, or remove it, as needed.
        Me.SP_PAYABLE_INVOICETableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_PAYABLE_INVOICE)

        txtVendorName.Text = ""
        txtInvoiceNo.Text = ""

        tmpSearchResult = ""

        If mdlGeneral.tmpSearchMode = "Vendor - Vendor Name" Then
            txtVendorName.Focus()
        ElseIf mdlGeneral.tmpSearchMode = "Vendor - Receipt No" Then
            txtInvoiceNo.Focus()
        End If

        If Language = "Indonesian" Then
            Me.Text = "Daftar Faktur Hutang"

            lblCustomer.Text = "Nama Supplier"
            lblInvoice.Text = "No Faktur"

            dgvPAYABLE_INVOICE.Columns("VENDOR_NAME").HeaderText = "Nama Supplier"
            dgvPAYABLE_INVOICE.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            dgvPAYABLE_INVOICE.Columns("TOTAL_PAYABLE").HeaderText = "Jumlah Hutang"
            dgvPAYABLE_INVOICE.Columns("MATURITY_DATE").HeaderText = "Tgl Jatuh Tempo"

            lblNoOfInvoice.Text = "Jumlah Faktur : " & dgvPAYABLE_INVOICE.RowCount
        Else
            lblNoOfInvoice.Text = "No of Invoice(s) : " & dgvPAYABLE_INVOICE.RowCount
        End If

    End Sub


    Private Sub dgvPAYABLE_INVOICE_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvPAYABLE_INVOICE.DoubleClick
        Try
            tmpSearchResult = SP_PAYABLE_INVOICEBindingSource.Current("RECEIPT_ID")
        Catch
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data Faktur Hutang !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Payable Invoice !", MsgBoxStyle.Critical, "DMI Retail")
            End If
        End Try
        Me.Close()
    End Sub


    Private Sub dgvPAYABLE_INVOICE_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvPAYABLE_INVOICE.KeyDown
        If e.KeyCode = Keys.Enter Then
            Try
                tmpSearchResult = SP_PAYABLE_INVOICEBindingSource.Current("RECEIPT_ID")
            Catch
                If Language = "Indonesian" Then
                    MsgBox("Tidak ada data Faktur Hutang !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("There is no Payable Invoice !", MsgBoxStyle.Critical, "DMI Retail")
                End If
            End Try
            Me.Close()

        ElseIf e.KeyCode = Keys.Escape Then
            tmpSearchResult = ""
            Me.Close()
        End If
    End Sub


    Private Sub frmSearchPayableInvoice_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
        txtVendorName.Focus()
    End Sub

    Private Sub txtVendorName_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtVendorName.KeyUp
        If e.KeyCode = Keys.Enter Then
            dgvPAYABLE_INVOICE.Focus()
            Exit Sub
        ElseIf e.KeyCode = Keys.Escape Then
            tmpSearchResult = ""
            Me.Close()
        End If
    End Sub

    Private Sub txtInvoiceNo_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtInvoiceNo.KeyUp
        If e.KeyCode = Keys.Enter Then
            dgvPAYABLE_INVOICE.Focus()
            Exit Sub
        ElseIf e.KeyCode = Keys.Escape Then
            tmpSearchResult = ""
            Me.Close()
        End If
    End Sub

    Private Sub txtVendorName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtVendorName.TextChanged
        Try
            txtInvoiceNo.Text = ""
            SP_PAYABLE_INVOICEBindingSource.Filter = "VENDOR_NAME LIKE '%" & txtVendorName.Text.ToUpper & "%'"
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtVendorName.Text = ""
            txtVendorName.Focus()
        End Try
    End Sub

    Private Sub txtInvoiceNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInvoiceNo.TextChanged
        Try
            txtVendorName.Text = ""
            SP_PAYABLE_INVOICEBindingSource.Filter = "RECEIPT_NO LIKE '%" & txtInvoiceNo.Text & "%'"
        Catch ex As Exception
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtInvoiceNo.Text = ""
            txtInvoiceNo.Focus()
        End Try
    End Sub
End Class