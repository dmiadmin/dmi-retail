﻿Public Class frmRepSaleReport
    Public date1 As Date
    Public date2 As Date
    Private Sub frmRepSaleReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DB_DMI_RETAILDataSetALL.COMPANY_INFORMATION' table. You can move, or remove it, as needed.
        Me.COMPANY_INFORMATIONTableAdapter.Fill(Me.DB_DMI_RETAILDataSetALL.COMPANY_INFORMATION)
        'TODO: This line of code loads data into the 'DB_DMI_RETAILDataSetALL.SP_SALE_REPORT_DAILY' table. You can move, or remove it, as needed.
        Me.SP_SALE_REPORT_DAILYTableAdapter.Fill(Me.DB_DMI_RETAILDataSetALL.SP_SALE_REPORT_DAILY, date1, date2)
        'TODO: This line of code loads data into the 'DB_DMI_RETAILDataSet1.COMPANY_INFORMATION' table. You can move, or remove it, as needed.
        'Me.COMPANY_INFORMATIONTableAdapter.Fill(Me.DB_DMI_RETAILDataSet1.COMPANY_INFORMATION)
        ''TODO: This line of code loads data into the 'DB_DMI_RETAILDataSet1.SP_SALE_REPORT' table. You can move, or remove it, as needed.
        'Me.SP_SALE_REPORTTableAdapter.Fill(Me.DB_DMI_RETAILDataSet1.SP_SALE_REPORT, frmListReportSale.tmpType, frmListReportSale.dtpPeriod.Value)

        Me.ReportViewer1.RefreshReport()

        Dim newPageSettings As New System.Drawing.Printing.PageSettings
        newPageSettings.Margins = New System.Drawing.Printing.Margins(50, 50, 35, 35)
        newPageSettings.PaperSize = New System.Drawing.Printing.PaperSize("A4", 827, 1169)
        Me.ReportViewer1.SetPageSettings(newPageSettings)

        If Not tmpReport Is Nothing Then tmpReport.Close()
        tmpReport = Me
        Me.ReportViewer1.RefreshReport()
    End Sub
End Class