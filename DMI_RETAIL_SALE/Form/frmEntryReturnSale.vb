﻿Public Class frmEntryReturnSale
    Dim tmpProductId, tmpstatus As Integer
    Dim tmpTotal, tmpReceiptID As Integer
    Dim tmpDataGridViewLoaded, tmpChange As Boolean
    Dim tmpDescription As String
    Public tmpSaveMode, getDescription, tmpWarehouseName As String
    Public getSaleId, getQuantity, tmpReusable, tmpBalance, tmpResult, tmpNoOfItem, tmpSaleReturnID As Integer
    Dim tmpPrice, tmpPaymentMethodID, tmpGrandTotal, tmpDP, tmpBalanceSale, tmpAccountID As Integer
    Dim tmpPayableNo, tmpReceivableNo, tmpAccountNO As String
    Dim tmpAmount As Integer = 0
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    Private Sub cmdSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        Dim popSearchReturn As New frmSearchReturnSale
        dgvSaleReturn.Rows.Clear()
        popSearchReturn.ShowDialog()

        txtCustomer.Text = popSearchReturn.tmpCustomuer
        txtReceipNo.Text = popSearchReturn.tmpReceiptNo
        txtSaleDate.Text = Format(popSearchReturn.tmpDate, "dd-MMM-yyyy")
        txtPayment.Text = popSearchReturn.tmpPayment
        tmpReceiptID = popSearchReturn.tmpSaleId
        If popSearchReturn.dgvSearchReturnSale.RowCount <> 0 Then
            tmpPaymentMethodID = popSearchReturn.SP_SEARCH_SALE_RETURNBindingSource.Current("PAYMENT_METHOD_ID")
            tmpGrandTotal = popSearchReturn.SP_SEARCH_SALE_RETURNBindingSource.Current("GRAND_TOTAL")
            tmpDP = popSearchReturn.SP_SEARCH_SALE_RETURNBindingSource.Current("DOWN_PAYMENT")
        End If

        'SALE_RETURNTableAdapter.SP_GENERATE_DESCRIPTION_SALE_RETURN(popSearchReturn.tmpSaleId, tmpDescription)
        For x As Integer = 0 To popSearchReturn.tmpTotal - 1
            Me.SP_SALE_RETURN_DETAIL_LISTTableAdapter.Fill(DS_RETURN_SALE.SP_SALE_RETURN_DETAIL_LIST, popSearchReturn.tmpSaleId)
            dgvSaleReturn.Rows.Add()
            SP_SALE_RETURN_DETAIL_LISTBindingSource.Position = x
            dgvSaleReturn.Item("PRODUCT_CODE", x).Value = SP_SALE_RETURN_DETAIL_LISTBindingSource.Current("PRODUCT_CODE")
            dgvSaleReturn.Item("PRODUCT_NAME", x).Value = SP_SALE_RETURN_DETAIL_LISTBindingSource.Current("PRODUCT_NAME")
            dgvSaleReturn.Item("QUANTITY", x).Value = SP_SALE_RETURN_DETAIL_LISTBindingSource.Current("QUANTITY")
            dgvSaleReturn.Item("RETURNED_QUANTITY", x).Value = SP_SALE_RETURN_DETAIL_LISTBindingSource.Current("RETURNED_QUANTITY")
        Next

        tmpChange = False
    End Sub

    Sub Clear()
        Me.SP_SALE_RETURN_DETAIL_LISTTableAdapter.Fill(DS_RETURN_SALE.SP_SALE_RETURN_DETAIL_LIST, 0)
        txtCustomer.Text = ""
        txtReceipNo.Text = ""
        txtSaleDate.Text = ""
        txtPayment.Text = ""
        dgvSaleReturn.Rows.Clear()
        txtDescription.Text = ""
        cmbReturnType.Text = ""
        SALE_RETURNTableAdapter.SP_GENERATE_SALE_RETURN_NO(txtReturnNo.Text)
        cmdSearch.Focus()
        WAREHOUSE_NAMEComboBox.Text = ""
    End Sub

    Private Sub dgvSaleReturn_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvSaleReturn.CellValueChanged
        If tmpDataGridViewLoaded Then
            If dgvSaleReturn.CurrentCell.ColumnIndex = 6 Then
                If Not IsNumeric(dgvSaleReturn.CurrentCell.Value) Then
                    If Language = "Indonesian" Then
                        MsgBox("Anda memasukan nilai tak terduga !" & vbCrLf & _
                           "Untuk Kolom : " & _
                            dgvSaleReturn.Columns(dgvSaleReturn.CurrentCell.ColumnIndex).HeaderText & vbCrLf & _
                            "Baris Nomor : " & _
                            dgvSaleReturn.CurrentCell.RowIndex + 1, MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("You entered unexpected value !" & vbCrLf & _
                           "For Column : " & _
                            dgvSaleReturn.Columns(dgvSaleReturn.CurrentCell.ColumnIndex).HeaderText & vbCrLf & _
                            "Row number : " & _
                            dgvSaleReturn.CurrentCell.RowIndex + 1, MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    'give value "0" to empty fields or unexpected fields
                    dgvSaleReturn.CurrentCell.Value = 0
                    Exit Sub
                End If
            End If
        End If

        tmpChange = True
    End Sub

    Private Sub frmEntryReturnSale_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If tmpChange = True Then
            tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            If tmpvar = 6 Then
                Call cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub frmEntryReturnSale_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_RETURN_SALE.WAREHOUSE' table. You can move, or remove it, as needed.
        Me.WAREHOUSETableAdapter.Fill(Me.DS_RETURN_SALE.WAREHOUSE)
        'TODO: This line of code loads data into the 'DS_RETURN_SALE.SALE_RETURN' table. You can move, or remove it, as needed.
        Me.SALE_RETURNTableAdapter.Fill(Me.DS_RETURN_SALE.SALE_RETURN)

        If Language = "Indonesian" Then

            Me.Text = "Input Retur Penjualan"

            lblReturnNo.Text = "No Retur"
            lblReturnDate.Text = "Tanggal Retur"
            lblCustomer.Text = "Pelanggan"
            lblReceiptNo.Text = "No Faktur"
            lblSaleDate.Text = "Tanggal Jual"
            lblPayment.Text = "Cara Bayar"
            lblDescription.Text = "Keterangan"
            lblWarehouse.Text = "Gudang"
            lblReturnType.Text = "Tipe Retur"
            ckbReusable.Text = "Dapat Dipakai"

            cmdAdd.Text = "Tambah"
            cmdEdit.Text = "Ubah"
            cmdSave.Text = "Simpan"
            cmdUndo.Text = "Batal"
            cmdDelete.Text = "Hapus"

            dgvSaleReturn.Columns("PRODUCT_CODE").HeaderText = "Kode Produk"
            dgvSaleReturn.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            dgvSaleReturn.Columns("QUANTITY").HeaderText = "Jumlah"
            dgvSaleReturn.Columns("RETURNED_QUANTITY").HeaderText = "Jumlah yg telah di retur"
            dgvSaleReturn.Columns("Return_Quantity").HeaderText = "Jumlah Retur"
            dgvSaleReturn.Columns("RETURNED_DESCRIPTION").HeaderText = "Keterangan"
        End If
        If tmpSaveMode = "Update" Then
            If tmpReusable = 1 Then
                ckbReusable.Checked = True
            Else
                ckbReusable.Checked = False
            End If

            WAREHOUSEBindingSource.Position = WAREHOUSEBindingSource.Find("WAREHOUSE_NAME", tmpWarehouseName)
            dgvSaleReturn.ReadOnly = True
            cmbReturnType.Enabled = False
            WAREHOUSE_NAMEComboBox.Enabled = False
            ckbReusable.Enabled = False
            txtDescription.Enabled = False
            cmdSearch.Enabled = False
            cmdUndo.Enabled = False
            cmdSave.Enabled = False
            txtReturnNo.Focus()

            For z As Integer = 0 To tmpNoOfItem - 1
                Me.SP_SALE_RETURN_DETAIL_LISTTableAdapter.Fill(DS_RETURN_SALE.SP_SALE_RETURN_DETAIL_LIST, getSaleId)
                Me.SP_SHOW_LIST_RETURN_SALETableAdapter.Fill(DS_RETURN_SALE.SP_SHOW_LIST_RETURN_SALE, tmpSaleReturnID)
                dgvSaleReturn.Rows.Add()
                SP_SALE_RETURN_DETAIL_LISTBindingSource.Position = z
                SP_SHOW_LIST_RETURN_SALEBindingSource.Position = z
                dgvSaleReturn.Item("PRODUCT_CODE", z).Value = SP_SHOW_LIST_RETURN_SALEBindingSource.Current("PRODUCT_CODE")
                dgvSaleReturn.Item("PRODUCT_NAME", z).Value = SP_SHOW_LIST_RETURN_SALEBindingSource.Current("PRODUCT_NAME")
                dgvSaleReturn.Item("QUANTITY", z).Value = SP_SALE_RETURN_DETAIL_LISTBindingSource.Current("QUANTITY")
                dgvSaleReturn.Item("RETURN_QUANTITY", z).Value = SP_SHOW_LIST_RETURN_SALEBindingSource.Current("QUANTITY")
                dgvSaleReturn.Item("RETURNED_QUANTITY", z).Value = SP_SALE_RETURN_DETAIL_LISTBindingSource.Current("RETURNED_QUANTITY")
                dgvSaleReturn.Item("RETURNED_DESCRIPTION", z).Value = getDescription
            Next
        Else
            SALE_RETURNTableAdapter.SP_GENERATE_SALE_RETURN_NO(txtReturnNo.Text)
            dtpReturnDate.Text = Format(Now, "dd-MMM-yyyy")
            Call Clear()
        End If
        tmpDataGridViewLoaded = True
        tmpChange = False

    End Sub

    Sub Save()
        Dim tmpwarehouseId As Integer

        If WAREHOUSE_NAMEComboBox.Text = "" Then
            tmpwarehouseId = 0
        Else
            tmpwarehouseId = WAREHOUSEBindingSource.Current("WAREHOUSE_ID")
        End If

        'tmpwarehouseId = IIf(WAREHOUSE_NAMEComboBox.Text = "", 0, WAREHOUSEBindingSource.Current("WAREHOUSE_ID"))
        tmpTotal = dgvSaleReturn.RowCount
        SALE_RETURNTableAdapter.SP_SALE_RETURN("I", 0, _
                                                 txtReturnNo.Text, _
                                                 Now, _
                                                 tmpReceiptID, _
                                                 tmpTotal, _
                                                 tmpwarehouseId, _
                                                 cmbReturnType.Text, _
                                                 txtDescription.Text, _
                                                 USER_ID, _
                                                 Now, _
                                                 0, _
                                                 DateSerial(4000, 12, 31))

        For Y As Integer = 0 To dgvSaleReturn.RowCount - 1
            SALE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvSaleReturn.Item("PRODUCT_CODE", Y).Value, tmpProductId)
            If dgvSaleReturn.Item("RETURN_QUANTITY", Y).Value <> 0 Then
                SALE_RETURNTableAdapter.SP_SALE_RETURN_DETAIL("I", 0, _
                                                                  0, _
                                                                  tmpProductId, _
                                                                  CInt(dgvSaleReturn.Item("RETURN_QUANTITY", Y).Value), _
                                                                  tmpstatus, _
                                                                  dgvSaleReturn.Item("RETURNED_DESCRIPTION", Y).Value, _
                                                                  USER_ID, _
                                                                  Now, _
                                                                  0, _
                                                                  DateSerial(4000, 12, 31))
            End If
        Next


    End Sub

    Function Check_Warehouse() As Byte
        '' Check Quantity Product in Warehouse
        Dim tmpwarehouseId As Integer
        tmpwarehouseId = IIf(WAREHOUSE_NAMEComboBox.Text = "", 0, WAREHOUSEBindingSource.Current("WAREHOUSE_ID"))

        For I As Integer = 0 To dgvSaleReturn.RowCount - 1
            SALE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvSaleReturn.Item("PRODUCT_CODE", I).Value, tmpProductId)
            SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_RETURN_SALE.SP_PRODUCT_WAREHOUSE_DETAIL, _
                                                      tmpProductId)
            SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Filter = "WAREHOUSE_ID = " & tmpwarehouseId

            If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Count < 1 Then
                If Language = "Indonesian" Then
                    MsgBox("Tidak dapat menyimpan transaksi ini!" & vbCrLf & dgvSaleReturn.Item("PRODUCT_NAME", I).Value & " tidak terdapat di dalam " & _
                        WAREHOUSE_NAMEComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Cannot save this transaction!" & vbCrLf & dgvSaleReturn.Item("PRODUCT_NAME", I).Value & " has no balance in " & _
                  WAREHOUSE_NAMEComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Check_Warehouse = 0
                Exit Function
            End If

            If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY") < CInt(dgvSaleReturn.Item("RETURN_QUANTITY", I).Value) Then
                If Language = "Indonesian" Then
                    MsgBox("Tidak dapat menyimpan transaksi ini!" & vbCrLf & dgvSaleReturn.Item("PRODUCT_NAME", I).Value & " tidak terdapat di dalam " & _
                        WAREHOUSE_NAMEComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Cannot save this transaction!" & vbCrLf & dgvSaleReturn.Item("PRODUCT_NAME", I).Value & " has no balance in " & _
                        WAREHOUSE_NAMEComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Check_Warehouse = 0
                Exit Function
            End If
        Next
        Check_Warehouse = 1
    End Function



    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Dim tmpCategory As Integer
        Dim getWarehouseId As Integer
        Dim tmpResult As Integer
        Dim tmpPaymentGroup As String = ""

        SALE_RETURNTableAdapter.SP_GET_PAYMENT_GROUP(tmpPaymentMethodID, tmpPaymentGroup)
        tmpAccountID = SALE_RETURNTableAdapter.SP_SELECT_PARAMETER("RETURN SALE ACCOUNT")
        SALE_RETURNTableAdapter.SP_GET_ACCOUNT_NUMBER(tmpAccountID, tmpAccountNO)

        'This block set to comment by Yohan, and replaced with a line after
        'If WAREHOUSE_NAMEComboBox.Text = "" Then
        '    getWarehouseId = 0
        'Else
        '    getWarehouseId = WAREHOUSEBindingSource.Current("WAREHOUSE_ID")
        'End If
        If WAREHOUSE_NAMEComboBox.Text = "" Then
            getWarehouseId = 0
        Else
            getWarehouseId = WAREHOUSEBindingSource.Current("WAREHOUSE_ID")
        End If

        'getWarehouseId = IIf(WAREHOUSE_NAMEComboBox.Text = "", 0, WAREHOUSEBindingSource.Current("WAREHOUSE_ID")) 'Edited by Yohan

        ''''Check Input in datagrid''''
        If dgvSaleReturn.RowCount = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan masukkan produk yang ingin diretur !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please input any products to returned !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        ''''Check Field in form''''
        If txtCustomer.Text = "" And txtReceipNo.Text = "" And txtPayment.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan ini kolom Jumlah !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please Fill Quantity fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        ''''Check Category Product & columns Return Quantity in datagrid view''''
        For y As Integer = 0 To dgvSaleReturn.RowCount - 1

            SALE_RETURNTableAdapter.SP_GET_CATEGORY_PRODUCT(dgvSaleReturn.Item("PRODUCT_CODE", y).Value, tmpCategory)
            If tmpCategory <> 1 Then
                If Language = "Indonesian" Then
                    MsgBox("Silahkan masukan produk lain !" & vbCrLf & _
                       "Tipe produk ini adalah jasa.", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Please enter other product !" & vbCrLf & _
                            "This type's product is Service.", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If

            If dgvSaleReturn.Item("RETURN_QUANTITY", y).Value = 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Silahkan isi kolom Jumlah Retur!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Please enter Return Quantity field!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If

        Next

        ''''Check Proses Period is Running''''
        If SALE_RETURNTableAdapter.SP_CLOSED_PERIOD_PROCESS("G", Now) = True Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menyimpan transaksi ini!" & vbCrLf & "Proses Tutup Buku sedang berlangsung", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Save this Transaction!" & vbCrLf & "The Closing Period is Running", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        For c As Integer = 0 To dgvSaleReturn.RowCount - 1
            ''''Chcek Quantity Returns have exceedeed that in sale Product''''
            Dim tmpReturned As Integer
            If IsDBNull(dgvSaleReturn.Item("RETURNED_QUANTITY", c).Value) Then
                tmpReturned = 0
            Else
                tmpReturned = dgvSaleReturn.Item("RETURNED_QUANTITY", c).Value
            End If
            If SP_SALE_RETURN_DETAIL_LISTBindingSource.Current("QUANTITY") < (tmpReturned + dgvSaleReturn.Item("RETURN_QUANTITY", c).Value) Then
                If Language = "Indonesian" Then
                    MsgBox("Barang ini sudah tidak dapat di Retur !" & vbCrLf & _
                           "Jumlah retur sudah melebihi produk yang di beli", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("This product can not be Return !" & vbCrLf & _
                           "Total returns have exceeded that in sale products", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Call Clear()
                Exit Sub

            End If
        Next

        ''''Check Rummage''''
        'If WAREHOUSEBindingSource.Current("WAREHOUSE_ID") = -1 Then
        '    If Language = "Indonesian" Then
        '        MsgBox("Silahkan pilih gudang yang lain !", MsgBoxStyle.Critical, "DMI Retail")
        '    Else
        '        MsgBox("Please enter other warehouse !", MsgBoxStyle.Critical, "DMI Retail")
        '    End If
        '    Exit Sub
        'End If

        ''''Check Return No''''
        SALE_RETURNTableAdapter.SP_RETURN_SALE_CHECK_RETURN_NO(txtReturnNo.Text, tmpResult)
        If tmpResult > 0 Then
            If Language = "Indonesian" Then
                MsgBox("No Transaksi telah digunakan oleh user lain." & vbCrLf & _
                                           "Nomor transaksi baru akan digunakan untuk transaksi ini.", _
                                           MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Receipt Number is used by another User." & vbCrLf & _
                                                    "New Receipt Number will be assigned to this transaction.", _
                                                    MsgBoxStyle.Critical, "DMI Retail")
            End If
        End If
        ''''If return No same....get new Return No''''
        While tmpResult > 0
            SALE_RETURNTableAdapter.SP_GENERATE_SALE_RETURN_NO(txtReturnNo.Text)
            SALE_RETURNTableAdapter.SP_RETURN_SALE_CHECK_RETURN_NO(txtReturnNo.Text, tmpResult)
        End While

        If Check_Warehouse() = 0 Then
            'batal
            Exit Sub
        End If


        ''Reusable Condition
        If ckbReusable.Checked = True Then
            tmpstatus = 1


            'Payment Method Cash
            If tmpPaymentGroup = "Cash" Then
                'Return Type
                If cmbReturnType.Text = "Goods" Then
                    'Insert table Return Sale & Return Sale Detail
                    Call Save()

                ElseIf cmbReturnType.Text = "Non Goods" Then
                    'Insert table Return Sale & Return Sale Detail
                    Call Save()

                    'Warehouse Normal (+) => Purchase Price & Purchase Date
                    tmpAmount = 0

                    For i As Integer = 0 To dgvSaleReturn.RowCount - 1

                        SALE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvSaleReturn.Item("PRODUCT_CODE", i).Value, tmpProductId)
                        SALE_RETURNTableAdapter.SP_GET_ATRIBUTE_SALE(tmpReceiptID, tmpProductId, tmpPrice)

                        If dgvSaleReturn.Item("RETURN_QUANTITY", i).Value <> 0 Then
                            SALE_RETURNTableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                                           0, _
                                                                           tmpProductId, _
                                                                           getWarehouseId, _
                                                                           CInt(dgvSaleReturn.Item("RETURN_QUANTITY", i).Value), _
                                                                           USER_ID, _
                                                                           Now, _
                                                                           0, _
                                                                           DateSerial(4000, 12, 31))

                        End If

                        tmpAmount = tmpAmount + (tmpPrice * CInt(dgvSaleReturn.Item("RETURN_QUANTITY", i).Value))

                    Next

                    If tmpAmount <> 0 Then
                        SALE_RETURNTableAdapter.SP_GENERATE_PAYABLE_TRANSACTION_NUMBER(tmpPayableNo)
                        'Insert Payable Transaction (Retur Penjualan, Selling Price)
                        SALE_RETURNTableAdapter.SP_PAYABLE_TRANSACTION("I", _
                                                                         0, _
                                                                         tmpPayableNo, _
                                                                         tmpAccountNO, _
                                                                         Now, _
                                                                         tmpAmount, _
                                                                         "Return Sale", _
                                                                         USER_ID, _
                                                                         Now, _
                                                                         0, _
                                                                         DateSerial(4000, 12, 31))

                    End If
                Else

                    If Language = "Indonesian" Then
                        MsgBox("Tipe Retur = " & cmbReturnType.Text & vbCrLf & _
                               "Tidak dapat ditemukan", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("Return type = " & cmbReturnType.Text & vbCrLf & _
                               "can not found", MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    Exit Sub

                End If

                'Payment Method Credit
            ElseIf tmpPaymentGroup = "Credit" Then
                'Return Type
                If cmbReturnType.Text = "Goods" Then
                    'Insert table Return Sale & Return Sale Detail
                    Call Save()

                ElseIf cmbReturnType.Text = "Non Goods" Then
                    'Insert table Return Sale & Return Sale Detail
                    Call Save()

                    'Warehouse Normal (+) => Purchase Price & Purchase Date
                    tmpAmount = 0

                    For i As Integer = 0 To dgvSaleReturn.RowCount - 1

                        SALE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvSaleReturn.Item("PRODUCT_CODE", i).Value, tmpProductId)
                        SALE_RETURNTableAdapter.SP_GET_ATRIBUTE_SALE(tmpReceiptID, tmpProductId, tmpPrice)

                        If dgvSaleReturn.Item("RETURN_QUANTITY", i).Value <> 0 Then
                            SALE_RETURNTableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                                           0, _
                                                                           tmpProductId, _
                                                                           getWarehouseId, _
                                                                           CInt(dgvSaleReturn.Item("RETURN_QUANTITY", i).Value), _
                                                                           USER_ID, _
                                                                           Now, _
                                                                           0, _
                                                                           DateSerial(4000, 12, 31))



                        End If

                        tmpAmount = tmpAmount + (tmpPrice * CInt(dgvSaleReturn.Item("RETURN_QUANTITY", i).Value))

                    Next

                    If tmpAmount <> 0 Then
                        SALE_RETURNTableAdapter.SP_GENERATE_PAYABLE_TRANSACTION_NUMBER(tmpPayableNo)
                        SALE_RETURNTableAdapter.SP_RECEIVABLE_PAYMENT_GENERATE_NUMBER(tmpReceivableNo)
                        SALE_RETURNTableAdapter.SP_CHECK_RECEIVABLE_ID(tmpReceiptID, tmpResult)

                        If tmpResult > 0 Then
                            SALE_RETURNTableAdapter.SP_GET_BALANCE_RECEIVABLE_PAYMENT(tmpReceiptID, tmpBalance)
                            If tmpAmount > tmpBalance Then
                                'Remaining Debit => Receivable_Payment 
                                SALE_RETURNTableAdapter.SP_RECEIVABLE_PAYMENT("I", _
                                                                                0, _
                                                                                tmpReceivableNo, _
                                                                                tmpReceiptID, _
                                                                                Now, _
                                                                                tmpBalance, _
                                                                                1, _
                                                                                "SALE RETURN", _
                                                                                tmpBalance - tmpBalance, _
                                                                                USER_ID, _
                                                                                Now, _
                                                                                0, _
                                                                                DateSerial(4000, 12, 31))

                                'Insert Payable Transaction (Retur Penjualan, Selling Price)
                                SALE_RETURNTableAdapter.SP_PAYABLE_TRANSACTION("I", _
                                                                               0, _
                                                                               tmpPayableNo, _
                                                                               tmpAccountNO, _
                                                                               Now, _
                                                                               tmpAmount - tmpBalance, _
                                                                               "Return Sale", _
                                                                               USER_ID, _
                                                                               Now, _
                                                                               0, _
                                                                               DateSerial(4000, 12, 31))
                            Else

                                'Remaining Debit => Receivable_Payment 
                                SALE_RETURNTableAdapter.SP_RECEIVABLE_PAYMENT("I", _
                                                                                0, _
                                                                                tmpReceivableNo, _
                                                                                tmpReceiptID, _
                                                                                Now, _
                                                                                tmpAmount, _
                                                                                1, _
                                                                                "SALE RETURN", _
                                                                                tmpBalance - tmpAmount, _
                                                                                USER_ID, _
                                                                                Now, _
                                                                                0, _
                                                                                DateSerial(4000, 12, 31))

                            End If
                        Else
                            tmpBalanceSale = tmpGrandTotal - tmpDP
                            If tmpAmount > tmpBalanceSale Then
                                'Remaining Debit => Receivable_Payment 
                                SALE_RETURNTableAdapter.SP_RECEIVABLE_PAYMENT("I", _
                                                                                0, _
                                                                                tmpReceivableNo, _
                                                                                tmpReceiptID, _
                                                                                Now, _
                                                                                tmpBalance, _
                                                                                1, _
                                                                                "SALE RETURN", _
                                                                                tmpBalanceSale, _
                                                                                USER_ID, _
                                                                                Now, _
                                                                                0, _
                                                                                DateSerial(4000, 12, 31))

                                'Insert Payable Transaction (Retur Penjualan, Selling Price)
                                SALE_RETURNTableAdapter.SP_PAYABLE_TRANSACTION("I", _
                                                                               0, _
                                                                               tmpPayableNo, _
                                                                               tmpAccountNO, _
                                                                               Now, _
                                                                               tmpAmount - tmpBalanceSale, _
                                                                               "Return Sale", _
                                                                               USER_ID, _
                                                                               Now, _
                                                                               0, _
                                                                               DateSerial(4000, 12, 31))
                            Else

                                'Remaining Debit => Receivable_Payment 
                                SALE_RETURNTableAdapter.SP_RECEIVABLE_PAYMENT("I", _
                                                                                0, _
                                                                                tmpReceivableNo, _
                                                                                tmpReceiptID, _
                                                                                Now, _
                                                                                tmpAmount, _
                                                                                1, _
                                                                                "SALE RETURN", _
                                                                                tmpBalanceSale - tmpAmount, _
                                                                                USER_ID, _
                                                                                Now, _
                                                                                0, _
                                                                                DateSerial(4000, 12, 31))

                            End If
                        End If
                    End If

                Else

                    If Language = "Indonesian" Then
                        MsgBox("Tipe Retur = " & cmbReturnType.Text & vbCrLf & _
                               "Tidak dapat ditemukan", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("Return type = " & cmbReturnType.Text & vbCrLf & _
                               "can not found", MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    Exit Sub

                End If
            End If

        Else 'Condition Non Reusable
            tmpstatus = 0

            'payment method = 'cash'
            If tmpPaymentGroup = "Cash" Then
                'Return Type
                If cmbReturnType.Text = "Goods" Then
                    'Insert table Return Sale & Return Sale Detail
                    Call Save()

                    'Warehouse Reject (+)
                    For i As Integer = 0 To dgvSaleReturn.RowCount - 1

                        SALE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvSaleReturn.Item("PRODUCT_CODE", i).Value, tmpProductId)

                        If dgvSaleReturn.Item("RETURN_QUANTITY", i).Value <> 0 Then
                            SALE_RETURNTableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                                           0, _
                                                                           tmpProductId, _
                                                                           -1, _
                                                                           CInt(dgvSaleReturn.Item("RETURN_QUANTITY", i).Value), _
                                                                           USER_ID, _
                                                                           Now, _
                                                                           0, _
                                                                           DateSerial(4000, 12, 31))
                        End If

                    Next

                    'Warehouse normal (-)
                    For i As Integer = 0 To dgvSaleReturn.RowCount - 1
                        Call Check_Warehouse()

                        SALE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvSaleReturn.Item("PRODUCT_CODE", i).Value, tmpProductId)

                        If dgvSaleReturn.Item("RETURN_QUANTITY", i).Value <> 0 Then
                            SALE_RETURNTableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                                           0, _
                                                                           tmpProductId, _
                                                                           getWarehouseId, _
                                                                           CInt(dgvSaleReturn.Item("RETURN_QUANTITY", i).Value) * (-1), _
                                                                           USER_ID, _
                                                                           Now, _
                                                                           0, _
                                                                           DateSerial(4000, 12, 31))
                        End If

                    Next

                ElseIf cmbReturnType.Text = "Non Goods" Then

                    'Insert table Return Sale & Return Sale Detail
                    Call Save()

                    'Warehouse Reject (+)

                    tmpAmount = 0

                    For i As Integer = 0 To dgvSaleReturn.RowCount - 1

                        SALE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvSaleReturn.Item("PRODUCT_CODE", i).Value, tmpProductId)
                        SALE_RETURNTableAdapter.SP_GET_ATRIBUTE_SALE(tmpReceiptID, tmpProductId, tmpPrice)

                        If dgvSaleReturn.Item("RETURN_QUANTITY", i).Value <> 0 Then
                            SALE_RETURNTableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                                           0, _
                                                                           tmpProductId, _
                                                                          -1, _
                                                                           CInt(dgvSaleReturn.Item("RETURN_QUANTITY", i).Value), _
                                                                           USER_ID, _
                                                                           Now, _
                                                                           0, _
                                                                           DateSerial(4000, 12, 31))

                        End If

                        tmpAmount = tmpAmount + (tmpPrice * CInt(dgvSaleReturn.Item("RETURN_QUANTITY", i).Value))

                    Next

                    If tmpAmount <> 0 Then
                        SALE_RETURNTableAdapter.SP_GENERATE_PAYABLE_TRANSACTION_NUMBER(tmpPayableNo)
                        'Insert Payable Transaction (Retur Penjualan, Selling Price)
                        SALE_RETURNTableAdapter.SP_PAYABLE_TRANSACTION("I", _
                                                                      0, _
                                                                      tmpPayableNo, _
                                                                      tmpAccountNO, _
                                                                      Now, _
                                                                      tmpAmount, _
                                                                      "Return Sale", _
                                                                      USER_ID, _
                                                                      Now, _
                                                                      0, _
                                                                      DateSerial(4000, 12, 31))

                    End If

                Else

                    If Language = "Indonesian" Then
                        MsgBox("Tipe Retur = " & cmbReturnType.Text & vbCrLf & _
                               "Tidak dapat ditemukan", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("Return type = " & cmbReturnType.Text & vbCrLf & _
                               "can not found", MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    Exit Sub

                End If
            ElseIf tmpPaymentGroup = "Credit" Then
                'Return Type
                If cmbReturnType.Text = "Goods" Then
                    'Insert table Return Sale & Return Sale Detail
                    Call Save()
                    'Warehouse Reject (+)
                    For i As Integer = 0 To dgvSaleReturn.RowCount - 1
                        SALE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvSaleReturn.Item("PRODUCT_CODE", i).Value, tmpProductId)
                        If dgvSaleReturn.Item("RETURN_QUANTITY", i).Value <> 0 Then
                            SALE_RETURNTableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                                           0, _
                                                                           tmpProductId, _
                                                                           -1, _
                                                                           CInt(dgvSaleReturn.Item("RETURN_QUANTITY", i).Value), _
                                                                           USER_ID, _
                                                                           Now, _
                                                                           0, _
                                                                           DateSerial(4000, 12, 31))
                        End If

                    Next

                    ''Warehouse normal (-)
                    For i As Integer = 0 To dgvSaleReturn.RowCount - 1
                        Call Check_Warehouse()
                        SALE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvSaleReturn.Item("PRODUCT_CODE", i).Value, tmpProductId)
                        If dgvSaleReturn.Item("RETURN_QUANTITY", i).Value <> 0 Then
                            SALE_RETURNTableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                                           0, _
                                                                           tmpProductId, _
                                                                           getWarehouseId, _
                                                                           CInt(dgvSaleReturn.Item("RETURN_QUANTITY", i).Value) * (-1), _
                                                                           USER_ID, _
                                                                           Now, _
                                                                           0, _
                                                                           DateSerial(4000, 12, 31))
                        End If

                    Next

                ElseIf cmbReturnType.Text = "Non Goods" Then
                    'Inser Into Return Sale and Return Sale_detail
                    Call Save()
                    'Reject warehouse (+)

                    tmpAmount = 0

                    For i As Integer = 0 To dgvSaleReturn.RowCount - 1

                        SALE_RETURNTableAdapter.SP_GET_PRODUCT_ID(dgvSaleReturn.Item("PRODUCT_CODE", i).Value, tmpProductId)
                        SALE_RETURNTableAdapter.SP_GET_ATRIBUTE_SALE(tmpReceiptID, tmpProductId, tmpPrice)

                        If dgvSaleReturn.Item("RETURN_QUANTITY", i).Value <> 0 Then
                            SALE_RETURNTableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                                           0, _
                                                                           tmpProductId, _
                                                                          -1, _
                                                                           CInt(dgvSaleReturn.Item("RETURN_QUANTITY", i).Value), _
                                                                           USER_ID, _
                                                                           Now, _
                                                                           0, _
                                                                           DateSerial(4000, 12, 31))
                        End If

                        tmpAmount = tmpAmount + (tmpPrice * CInt(dgvSaleReturn.Item("RETURN_QUANTITY", i).Value))

                    Next

                    If tmpAmount <> 0 Then
                        SALE_RETURNTableAdapter.SP_GENERATE_PAYABLE_TRANSACTION_NUMBER(tmpPayableNo)
                        SALE_RETURNTableAdapter.SP_RECEIVABLE_PAYMENT_GENERATE_NUMBER(tmpReceivableNo)
                        SALE_RETURNTableAdapter.SP_CHECK_RECEIVABLE_ID(tmpReceiptID, tmpResult)

                        If tmpResult > 0 Then
                            SALE_RETURNTableAdapter.SP_GET_BALANCE_RECEIVABLE_PAYMENT(tmpReceiptID, tmpBalance)
                            If tmpAmount > tmpBalance Then
                                'Remaining Debit => Receivable_Payment 
                                SALE_RETURNTableAdapter.SP_RECEIVABLE_PAYMENT("I", _
                                                                                0, _
                                                                                tmpReceivableNo, _
                                                                                tmpReceiptID, _
                                                                                Now, _
                                                                                tmpBalance, _
                                                                                1, _
                                                                                "SALE RETURN", _
                                                                                tmpBalance - tmpBalance, _
                                                                                USER_ID, _
                                                                                Now, _
                                                                                0, _
                                                                                DateSerial(4000, 12, 31))

                                'Insert Payable Transaction (Retur Penjualan, Selling Price)
                                SALE_RETURNTableAdapter.SP_PAYABLE_TRANSACTION("I", _
                                                                               0, _
                                                                               tmpPayableNo, _
                                                                               tmpAccountNO, _
                                                                               Now, _
                                                                               tmpAmount - tmpBalance, _
                                                                               "Return Sale", _
                                                                               USER_ID, _
                                                                               Now, _
                                                                               0, _
                                                                               DateSerial(4000, 12, 31))
                            Else

                                'Remaining Debit => Receivable_Payment 
                                SALE_RETURNTableAdapter.SP_RECEIVABLE_PAYMENT("I", _
                                                                                0, _
                                                                                tmpReceivableNo, _
                                                                                tmpReceiptID, _
                                                                                Now, _
                                                                                tmpAmount, _
                                                                                1, _
                                                                                "SALE RETURN", _
                                                                                tmpBalance - tmpAmount, _
                                                                                USER_ID, _
                                                                                Now, _
                                                                                0, _
                                                                                DateSerial(4000, 12, 31))

                            End If
                        Else
                            tmpBalanceSale = tmpGrandTotal - tmpDP
                            If tmpAmount > tmpBalanceSale Then
                                'Remaining Debit => Receivable_Payment 
                                SALE_RETURNTableAdapter.SP_RECEIVABLE_PAYMENT("I", _
                                                                                0, _
                                                                                tmpReceivableNo, _
                                                                                tmpReceiptID, _
                                                                                Now, _
                                                                                tmpBalance, _
                                                                                1, _
                                                                                "SALE RETURN", _
                                                                                tmpBalanceSale, _
                                                                                USER_ID, _
                                                                                Now, _
                                                                                0, _
                                                                                DateSerial(4000, 12, 31))

                                'Insert Payable Transaction (Retur Penjualan, Selling Price)
                                SALE_RETURNTableAdapter.SP_PAYABLE_TRANSACTION("I", _
                                                                               0, _
                                                                               tmpPayableNo, _
                                                                               tmpAccountNO, _
                                                                               Now, _
                                                                               tmpAmount - tmpBalanceSale, _
                                                                               "Return Sale", _
                                                                               USER_ID, _
                                                                               Now, _
                                                                               0, _
                                                                               DateSerial(4000, 12, 31))
                            Else

                                'Remaining Debit => Receivable_Payment 
                                SALE_RETURNTableAdapter.SP_RECEIVABLE_PAYMENT("I", _
                                                                                0, _
                                                                                tmpReceivableNo, _
                                                                                tmpReceiptID, _
                                                                                Now, _
                                                                                tmpAmount, _
                                                                                1, _
                                                                                "SALE RETURN", _
                                                                                tmpBalanceSale - tmpAmount, _
                                                                                USER_ID, _
                                                                                Now, _
                                                                                0, _
                                                                                DateSerial(4000, 12, 31))

                            End If
                        End If
                    End If

                Else
                    If Language = "Indonesian" Then
                        MsgBox("Tipe Retur = " & cmbReturnType.Text & vbCrLf & _
                               "Tidak dapat ditemukan", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("Return type = " & cmbReturnType.Text & vbCrLf & _
                               "can not found", MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    Exit Sub
                End If

            End If
        End If
        If Language = "Indonesian" Then
            MsgBox("Data berhasil disimpan", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data Successful saved", MsgBoxStyle.Information, "DMI Retail")
        End If
        Call Clear()
        tmpChange = False
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        Me.Close()
    End Sub
End Class