﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Data
Public Class frmDataRestore
    Dim tmpchange As Boolean
    Dim xconn As SqlConnection
    Dim xComm As SqlCommand
    Dim xReader As SqlDataReader

    Private Sub connection()
        xconn.Close()
        xconn.ConnectionString = "Data Source=" & ServerName & ";Initial Catalog=" & DatabaseName & ";Persist Security Info=True;User ID=sa;Password=SA;"
        xconn.Open()
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        Me.Close()
    End Sub

    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        OpenFileDialog1.Filter = "Database Backup File|*.bak"
        OpenFileDialog1.FileName = ""
        'OpenFileDialog1.InitialDirectory = PARAMETERTableAdapter.SP_SELECT_PARAMETER("PATH BACK UP")
        OpenFileDialog1.ShowDialog()
        txtPathRestore.Text = OpenFileDialog1.FileName
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        tmpchange = False
        If txtPathRestore.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Masukkan lokasi folder untuk Backup !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please Fill the Path Location of Backup File !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If Language = "Indonesian" Then
            If MsgBox("Restore Database akan menimpa database yang ada dengan data pada saat file backup dibuat." _
                              & vbCrLf & "Database : " & DatabaseName & vbCrLf & _
                              "Apakah anda ingin melanjutkan ?", MsgBoxStyle.Question & MsgBoxStyle.YesNo, _
                              "DMI Retail") = MsgBoxResult.No Then Exit Sub
        Else
            If MsgBox("Restore Database will set the database to data when the backup file was created." _
                              & vbCrLf & "Database : " & DatabaseName & vbCrLf & _
                              "Are you sure to continue ?", MsgBoxStyle.Question & MsgBoxStyle.YesNo, _
                              "DMI Retail") = MsgBoxResult.No Then Exit Sub
        End If


        Me.Cursor = Cursors.WaitCursor
        Me.Height = 305

        Try
            Timer1.Enabled = True
            Dim sql As String
            sql = ("Use Master" & vbCrLf & _
                             "Alter Database [" & DatabaseName & "] SET SINGLE_USER With ROLLBACK IMMEDIATE" & vbCrLf & _
                             "Use Master" & vbCrLf & _
                             "RESTORE DATABASE [" & DatabaseName & "] FROM  DISK = N'" & txtPathRestore.Text & _
                             "' WITH  FILE = 1,  NOUNLOAD,  REPLACE,  STATS = 10" & vbCrLf & _
                             "Use Master" & vbCrLf & _
                             "Alter Database [" & DatabaseName & "] " & _
                             "SET MULTI_USER")
            xComm = New SqlCommand(sql, xconn)
            xComm.ExecuteNonQuery()
        Catch
            If Language = "Indonesian" Then
                MsgBox("Restore Database gagal.", MsgBoxStyle.Information, "DMI Retail")
            Else
                MsgBox("Restore Database failed.", MsgBoxStyle.Information, "DMI Retail")
            End If
            Exit Sub
        End Try

        System.Data.SqlClient.SqlConnection.ClearAllPools()
        Conn.Close()
        Conn.Open()
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub frmDataRestore_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If tmpchange = True Then
            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah anda ingin melanjutkan proses Restore Database?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do you want to process Restoring Database?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If

            If tmpvar = 6 Then
                cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub frmDataRestore_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Label1.Text = ""
        tmpchange = False

        Me.Height = 241

        If Language = "Indonesian" Then
            cmdBrowse.Text = "&Cari"
            cmdSave.Text = "&Simpan"
            cmdUndo.Text = "&Batal"
        End If
    End Sub

    Private Sub txtPathRestore_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPathRestore.TextChanged
        tmpchange = True
    End Sub

    Private Sub PARAMETERBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.PARAMETERBindingSource.EndEdit()
        'Me.TableAdapterManager.UpdateAll(Me.DS_PARAMETER)
    End Sub

    Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        ProgressBar1.Value += 2
        If ProgressBar1.Value <= 50 Then
            If Language = "Indonesian" Then
                Label1.Text = "Dalam Proses ..."
            Else
                Label1.Text = "Loading ..."
            End If
        ElseIf ProgressBar1.Value <= 70 Then
            If Language = "Indonesian" Then
                Label1.Text = "Mohon Tunggu ..."
            Else
                Label1.Text = "Please Wait ..."
            End If
        End If

        If ProgressBar1.Value = 100 Then
            Timer1.Enabled = False
            Me.Hide()

            If Language = "Indonesian" Then
                MsgBox("Restore Database Berhasil.", MsgBoxStyle.Information, "DMI Retail")
            Else
                MsgBox("Restore Database Successful.", MsgBoxStyle.Information, "DMI Retail")
            End If

        End If
    End Sub

End Class
