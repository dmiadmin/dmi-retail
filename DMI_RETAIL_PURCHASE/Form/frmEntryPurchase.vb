﻿Public Class frmEntryPurchase

    Dim tmpDataGridViewLoaded As Boolean
    Dim tmpProductID, tmpNoOfReceiptNo As Integer
    Dim tmpInput As String
    Public tmpSaveMode, tmpVendor, tmpPaymentMethod, tmpReceipt, tmpWareHouseName, tmpPoNo As String
    Public tmpPurchaseId, tmpCategory, tmpWarehouseID, tmpPurchaseOrderId, tmpTotItem As Integer
    Public tmpPurchaseOrderDate As DateTime
    Dim tmpChange, tmpNoData As Boolean
    Dim TmpPurchaseWarehouse, TmpPaymentPurchaseID, TmpBalance, tmpNewPurchaseId As Integer
    Dim tmpPaymentGroup, tmpPaymentNo As String
    Dim tmpNewPurchaseDate, tmpNewPaymentDate As Date
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub

    Private Sub frmEntryPurchase_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing


        If tmpChange = True And REMARKTextBox.Enabled = True Then
            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah anda ingin menyimpan perubahan " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do you want to save the changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If
            If tmpvar = 6 Then
                cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        Else
            Me.Dispose()
        End If
    End Sub


    Private Sub frmEntryPurchase_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If tmpSaveMode = "Update" Then
            If e.KeyCode = Keys.F1 Then Exit Sub
            If e.KeyCode = Keys.F2 Then Exit Sub
            If e.KeyCode = Keys.F3 Then Exit Sub
            If e.KeyCode = Keys.F5 Then Exit Sub
            If e.KeyCode = Keys.F6 Then Exit Sub
            If e.KeyCode = Keys.F7 Then Exit Sub
            If e.KeyCode = Keys.F9 Then Exit Sub
            If e.KeyCode = Keys.F10 Then Exit Sub
        Else
            If e.KeyCode = Keys.F1 Then
                REMARKTextBox.Focus()
            ElseIf e.KeyCode = Keys.F2 Then
                TAXPercentageTEXTBOX.Focus()
            ElseIf e.KeyCode = Keys.F3 Then
                DOWN_PAYMENTTextBox.Focus()
            ElseIf e.KeyCode = Keys.F5 Then
                tmpInput = "Browse"
                dgvPurchaseDetail.CancelEdit()
                mdlGeneral.tmpSearchMode = "PURCHASE - Product Code"

                frmSearchProduct.ShowDialog(Me)
                If tmpSearchResult = "" Then
                    tmpInput = ""
                    Exit Sub
                End If

                If txtPoNo.Text <> "" Then
                    dgvPurchaseDetail.Rows.Clear()
                    txtPoNo.Text = ""
                    dtpPoDate.Value = Now
                End If
                PRODUCTBindingSource.Filter = "PRODUCT_ID = " & tmpSearchResult
                dgvPurchaseDetail.Rows.Insert(dgvPurchaseDetail.RowCount - 1)
                dgvPurchaseDetail.CurrentCell = dgvPurchaseDetail.Rows(dgvPurchaseDetail.RowCount - 1).Cells(0)
                dgvPurchaseDetail.Item(0, dgvPurchaseDetail.RowCount - 1).Value = _
                    PRODUCTBindingSource.Current("PRODUCT_CODE")
                dgvPurchaseDetail.Item(0, dgvPurchaseDetail.RowCount - 1).Value = ""

            ElseIf e.KeyCode = Keys.F6 Then
                tmpInput = "Browse"
                dgvPurchaseDetail.CancelEdit()
                mdlGeneral.tmpSearchMode = "PURCHASE - Product Name"

                frmSearchProduct.ShowDialog(Me)
                If tmpSearchResult = "" Then
                    tmpInput = ""
                    Exit Sub
                End If

                If txtPoNo.Text <> "" Then
                    dgvPurchaseDetail.Rows.Clear()
                    txtPoNo.Text = ""
                    dtpPoDate.Value = Now
                End If

                PRODUCTBindingSource.Filter = "PRODUCT_ID = " & tmpSearchResult
                dgvPurchaseDetail.Rows.Insert(dgvPurchaseDetail.RowCount - 1)
                dgvPurchaseDetail.CurrentCell = dgvPurchaseDetail.Rows(dgvPurchaseDetail.RowCount - 1).Cells(0)
                dgvPurchaseDetail.Item(0, dgvPurchaseDetail.RowCount - 1).Value = _
                    PRODUCTBindingSource.Current("PRODUCT_CODE")
                dgvPurchaseDetail.Item(0, dgvPurchaseDetail.RowCount - 1).Value = ""

            ElseIf e.KeyCode = Keys.F7 Then
                If cmdAdd.Enabled = False Then Exit Sub
                cmdAdd_Click(Nothing, Nothing)
            ElseIf e.KeyCode = Keys.F9 Then
                If cmdUndo.Enabled = False Then Exit Sub
                cmdUndo_Click(Nothing, Nothing)
            ElseIf e.KeyCode = Keys.F10 Then
                If cmdSave.Enabled = False Then Exit Sub
                cmdSave_Click(Nothing, Nothing)
            End If
        End If
    End Sub


    Private Sub frmEntryPurchase_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_PURCHASE_ORDER.SP_SELECT_PURCHASE_ORDER_HEADER' table. You can move, or remove it, as needed.
        Me.SP_SELECT_PURCHASE_ORDER_HEADERTableAdapter.Fill(Me.DS_PURCHASE_ORDER.SP_SELECT_PURCHASE_ORDER_HEADER)
        'TODO: This line of code loads data into the 'DS_PURCHASE.WAREHOUSE' table. You can move, or remove it, as needed.
        Me.WAREHOUSETableAdapter.Fill(Me.DS_PURCHASE.WAREHOUSE)
        'TODO: This line of code loads data into the 'DS_PRODUCT_WAREHOUSE.PRODUCT_WAREHOUSE' table. You can move, or remove it, as needed.
        Me.PRODUCT_WAREHOUSETableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.PRODUCT_WAREHOUSE)
        'TODO: This line of code loads data into the 'DS_PURCHASE.VIEW_LIST_PURCHASE_DETAIL' table. You can move, or remove it, as needed.
        Me.VIEW_LIST_PURCHASE_DETAILTableAdapter.Fill(Me.DS_PURCHASE.VIEW_LIST_PURCHASE_DETAIL)
        'TODO: This line of code loads data into the 'DS_PURCHASE.VIEW_LIST_PURCHASE' table. You can move, or remove it, as needed.
        Me.VIEW_LIST_PURCHASETableAdapter.Fill(Me.DS_PURCHASE.VIEW_LIST_PURCHASE)
        ''TODO: This line of code loads data into the 'DS_PURCHASE.PURCHASE_DETAIL' table. You can move, or remove it, as needed.
        'Me.PURCHASE_DETAILTableAdapter.Fill(Me.DS_PURCHASE.PURCHASE_DETAIL)
        'TODO: This line of code loads data into the 'DS_PURCHASE.PRODUCT' table. You can move, or remove it, as needed.
        Me.PRODUCTTableAdapter.Fill(Me.DS_PURCHASE.PRODUCT)
        'TODO: This line of code loads data into the 'DS_PURCHASE.PAYMENT_METHOD' table. You can move, or remove it, as needed.
        Me.PAYMENT_METHODTableAdapter.Fill(Me.DS_PURCHASE.PAYMENT_METHOD)
        'TODO: This line of code loads data into the 'DS_PURCHASE.VENDOR' table. You can move, or remove it, as needed.
        Me.VENDORTableAdapter.Fill(Me.DS_PURCHASE.VENDOR)
        ''TODO: This line of code loads data into the 'DS_PURCHASE.PURCHASE' table. You can move, or remove it, as needed.
        'Me.PURCHASETableAdapter.Fill(Me.DS_PURCHASE.PURCHASE)

        accFormName = Me.Text
        Call AccessPrivilege()

        If Language = "Indonesian" Then
            lblMaturity.Text = "Jatuh Tempo"
            lblPayment.Text = "Cara Pembayaran"
            lblPurchaseDate.Text = "Tgl Pembelian"
            lblVendor.Text = "Supplier"
            lblReceiptNo.Text = "No Faktur"
            lblWarehouse.Text = "Nama Gudang"
            lblF5.Text = "F5 : Cari produk berdasarkan kode"
            lblF6.Text = "F6 : Cari produk berdasarkan nama"
            lblDel.Text = "Del : Hapus produk dari daftar"
            cmdAdd.Text = "Tambah" & vbCrLf & "[F7]"
            cmdEdit.Text = "Ubah" & vbCrLf & "[F8]"
            cmdUndo.Text = "Batal" & vbCrLf & "[F9]"
            cmdSave.Text = "Simpan" & vbCrLf & "[F10]"
            cmdDelete.Text = "Hapus" & vbCrLf & "[F11]"
            lblNoOfItem.Text = "No dari item :"
            lblRemark.Text = "Keterangan" & vbCrLf & "[F1]"
            lblTax.Text = "Pajak [F2]"
            lblDp.Text = "Uang Muka [F3]"
            lblNoOfItem.Text = "Jumlah Item :"
            dgvPurchaseDetail.Columns("PRODUCT_CODE").HeaderText = "Kode Produk"
            dgvPurchaseDetail.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            dgvPurchaseDetail.Columns("QUANTITY").HeaderText = "Jumlah"
            dgvPurchaseDetail.Columns("PRICE_PER_UNIT").HeaderText = "Harga per Unit"
            dgvPurchaseDetail.Columns("DELETE").HeaderText = "Hapus"
            Me.Text = "Input Pembelian"
        End If

        If tmpSaveMode = "Update" Then
            dgvPurchaseDetail.Rows.Clear()
            cmdSearchSaleOrder.Enabled = False
            dgvPurchaseDetail.RowCount = 1
            VIEW_LIST_PURCHASE_DETAILBindingSource.Filter = "PURCHASE_ID = " & tmpPurchaseId
            If tmpVendor = "" Then
                VENDOR_IDComboBox.Text = ""
            Else
                VENDORBindingSource.Position = VENDORBindingSource.Find("VENDOR_ID", tmpVendor)
            End If
            PAYMENT_METHODBindingSource.Position = PAYMENT_METHODBindingSource.Find("PAYMENT_METHOD_ID", tmpPaymentMethod)
            If tmpWarehouseID = 0 Then
                WAREHOUSE_NAMEComboBox.SelectedValue = -1
            Else
                WAREHOUSEBindingSource.Position = WAREHOUSEBindingSource.Find("WAREHOUSE_ID", tmpWarehouseID)
            End If

            'WAREHOUSE_NAMEComboBox.Text = tmpWareHouseName
            RECEIPT_NOTextBox.Text = tmpReceipt

            If Not IsDBNull(tmpPurchaseOrderId) Then
                txtPoNo.Text = tmpPoNo
                dtpPoDate.Value = tmpPurchaseOrderDate
            End If

            dgvPurchaseDetail.Rows.Add(VIEW_LIST_PURCHASE_DETAILBindingSource.Count)

            For X As Integer = 0 To (VIEW_LIST_PURCHASE_DETAILBindingSource.Count) - 1
                VIEW_LIST_PURCHASE_DETAILBindingSource.Position = X
                dgvPurchaseDetail.Item(0, X).Value = VIEW_LIST_PURCHASE_DETAILBindingSource.Current("PRODUCT_CODE")
                dgvPurchaseDetail.Item(1, X).Value = VIEW_LIST_PURCHASE_DETAILBindingSource.Current("PRODUCT_NAME")
                dgvPurchaseDetail.Item(2, X).Value = VIEW_LIST_PURCHASE_DETAILBindingSource.Current("QUANTITY")
                dgvPurchaseDetail.Item(3, X).Value = VIEW_LIST_PURCHASE_DETAILBindingSource.Current("PRICE_PER_UNIT")
                dgvPurchaseDetail.Item(5, X).Value = VIEW_LIST_PURCHASE_DETAILBindingSource.Current("DISCOUNT")
                dgvPurchaseDetail.Item(6, X).Value = VIEW_LIST_PURCHASE_DETAILBindingSource.Current("TOTAL")
                dgvPurchaseDetail.Rows(X).ReadOnly = True
                dgvPurchaseDetail.Columns(7).Visible = False

                If dgvPurchaseDetail.Item(3, X).Value = 0 Then
                    dgvPurchaseDetail.Item(4, X).Value = 0
                Else
                    dgvPurchaseDetail.Item(4, X).Value = _
                                FormatNumber(CInt(dgvPurchaseDetail.Item(5, X).Value) / _
                                (CInt(dgvPurchaseDetail.Item(2, X).Value) * _
                                CInt(dgvPurchaseDetail.Item(3, X).Value)) * 100, 2)
                End If

            Next

            dgvPurchaseDetail.AllowUserToAddRows = False
            dgvPurchaseDetail.AllowUserToDeleteRows = False
            dgvPurchaseDetail.AllowUserToOrderColumns = False
            lblNoOfItem.Text = "No of Item(s) : " & dgvPurchaseDetail.RowCount
            If TAX_AMOUNTTextBox.Text = "0" Then
                TAXPercentageTEXTBOX.Text = "0.00"
            Else
                TAXPercentageTEXTBOX.Text = FormatNumber((CDbl(TAX_AMOUNTTextBox.Text) * 100) / (CDbl(GRAND_TOTALTextBox.Text) - CDbl(TAX_AMOUNTTextBox.Text)))
            End If


            DisableInputBox(Me)
        Else
            cmdAdd_Click(Nothing, Nothing)
            PURCHASETableAdapter.SP_GENERATE_PURCHASE_RECEIPT_NO(RECEIPT_NOTextBox.Text)


            tmpDataGridViewLoaded = True

            cmdAdd.Enabled = False
            cmdEdit.Enabled = False
            cmdUndo.Enabled = True
            cmdSave.Enabled = True
            cmdDelete.Enabled = False

            If PRODUCTBindingSource Is Nothing Or PRODUCTBindingSource.Count < 1 Then
                If Language = "Indonesian" Then
                    MsgBox("Tidak ada data produk." & vbCrLf & "Tolong masukan salah satu data produk!", _
                           MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("There is no Product data." & vbCrLf & "Please enter at least one Product data!", _
                           MsgBoxStyle.Critical, "DMI Retail")
                End If
                tmpNoData = True
            End If

        End If

        txtDTP.Text = Format(PURCHASE_DATEDateTimePicker.Value, "dd-MMM-yyyy")

        tmpChange = False

    End Sub

    Private Sub dgvPurchaseDetail_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPurchaseDetail.CellValueChanged
        If tmpSaveMode = "Update" Then Exit Sub

        If tmpDataGridViewLoaded Then

            If dgvPurchaseDetail.CurrentCell.ColumnIndex = 2 Or dgvPurchaseDetail.CurrentCell.ColumnIndex = 3 Or dgvPurchaseDetail.CurrentCell.ColumnIndex = 4 Or dgvPurchaseDetail.CurrentCell.ColumnIndex = 5 Then
                If Not IsNumeric(dgvPurchaseDetail.CurrentCell.Value) Then
                    If Language = "Indonesian" Then
                        MsgBox("Anda memasukan nilai tak terduga !" & vbCrLf & _
                          "Untuk Kolom : " & _
                           dgvPurchaseDetail.Columns(dgvPurchaseDetail.CurrentCell.ColumnIndex).HeaderText & vbCrLf & _
                           "Baris Nomor : " & _
                           dgvPurchaseDetail.CurrentCell.RowIndex + 1, MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("You entered unexpected value !" & vbCrLf & _
                               "For Column : " & _
                                dgvPurchaseDetail.Columns(dgvPurchaseDetail.CurrentCell.ColumnIndex).HeaderText & vbCrLf & _
                                "Row number : " & _
                                dgvPurchaseDetail.CurrentCell.RowIndex + 1, MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    dgvPurchaseDetail.CurrentCell.Value = 0
                    Exit Sub
                Else
                    dgvPurchaseDetail.CurrentCell.Value = FormatNumber((dgvPurchaseDetail.CurrentCell.Value), 0)
                End If
            End If

            If tmpFillingDGV = False Then
                If dgvPurchaseDetail.CurrentCell.ColumnIndex = 0 Then
                    Dim tmpProductName As String = ""
                    'Dim tmpUnitPrice As Integer = 0

                    PRODUCTTableAdapter.SP_GET_PRODUCT_NAME(dgvPurchaseDetail.CurrentCell.Value, tmpProductName)
                    'PRODUCTTableAdapter.SP_GET_SELLING_PRICE(dgvPurchaseDetail.CurrentCell.Value, tmpUnitPrice)
                    ' PRODUCTTableAdapter.SP_GET_CATEGORY_PRODUCT(dgvPurchaseDetail.CurrentCell.Value, tmpCategory)


                    If tmpProductName = "" Then
                        If dgvPurchaseDetail.CurrentCell.Value <> "" Then
                            If Language = "Indonesian" Then
                                MsgBox("Produk tidak ditemukan", MsgBoxStyle.Critical, "DMI Retail")
                                dgvPurchaseDetail.Rows.RemoveAt(dgvPurchaseDetail.CurrentCell.RowIndex)
                            Else
                                MsgBox("Product not found!", MsgBoxStyle.Critical, "DMI Retail")
                                dgvPurchaseDetail.Rows.RemoveAt(dgvPurchaseDetail.CurrentCell.RowIndex)
                            End If
                        End If
                        Exit Sub
                    End If

                    'If tmpCategory <> 1 Then
                    '    If dgvPurchaseDetail.CurrentCell.Value <> "" Then
                    '        If Language = "Indonesian" Then
                    '            MsgBox("Produk yang anda input bukan barang !" & vbCrLf & _
                    '               "Silahkan gunakan Form Pencarian untuk menemukan produk yang benar !", MsgBoxStyle.Critical, "DMI Retail")
                    '        Else
                    '            MsgBox("You input Product which is not Goods !" & vbCrLf & _
                    '                   "Please use the Search Form to find the right Products !", MsgBoxStyle.Critical, "DMI Retail")
                    '        End If
                    '        Exit Sub
                    '        dgvPurchaseDetail.Rows.RemoveAt(dgvPurchaseDetail.CurrentCell.RowIndex)
                    '    End If

                    'End If


                    tmpFillingDGV = True
                    AddDataToGrid(dgvPurchaseDetail, _
                                  dgvPurchaseDetail.CurrentCell.Value, _
                                  0, _
                                  tmpProductName, _
                                  1, _
                                  2, _
                                  0, _
                                  3, _
                                  4, _
                                  5, _
                                  6)
                    CalculationAll(dgvPurchaseDetail, _
                                     5, _
                                     6, _
                                     AMOUNT_DISCOUNTTextBox, _
                                     TAXPercentageTEXTBOX, _
                                     TAX_AMOUNTTextBox, _
                                     DOWN_PAYMENTTextBox, _
                                     GRAND_TOTALTextBox)
                    lblNoOfItem.Text = "No of Item(s) : " & dgvPurchaseDetail.RowCount - 1

                    ''Calculate Tax Amount when adding Product
                    Dim tmpGrandTotal As Integer = 0
                    For x As Integer = 0 To dgvPurchaseDetail.RowCount - 1
                        tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvPurchaseDetail.Item(6, x).Value)
                    Next
                    TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
                    TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)


                ElseIf dgvPurchaseDetail.CurrentCell.ColumnIndex = 4 Then

                    If dgvPurchaseDetail.Item(0, dgvPurchaseDetail.CurrentCell.RowIndex).Value = "" Then
                        dgvPurchaseDetail.Rows.RemoveAt(dgvPurchaseDetail.CurrentCell.RowIndex)
                        Exit Sub
                    End If

                    dgvPurchaseDetail.Item(5, dgvPurchaseDetail.CurrentCell.RowIndex).Value = _
                        CInt(dgvPurchaseDetail.Item(2, dgvPurchaseDetail.CurrentCell.RowIndex).Value) * _
                        CInt(dgvPurchaseDetail.Item(3, dgvPurchaseDetail.CurrentCell.RowIndex).Value) * _
                        CInt(dgvPurchaseDetail.Item(4, dgvPurchaseDetail.CurrentCell.RowIndex).Value) / 100

                    Calculation(dgvPurchaseDetail.Item(2, dgvPurchaseDetail.CurrentCell.RowIndex).Value, _
                                dgvPurchaseDetail.Item(3, dgvPurchaseDetail.CurrentCell.RowIndex).Value, _
                                dgvPurchaseDetail.Item(5, dgvPurchaseDetail.CurrentCell.RowIndex).Value, _
                                dgvPurchaseDetail.Item(6, dgvPurchaseDetail.CurrentCell.RowIndex).Value)
                    CalculationAll(dgvPurchaseDetail, _
                                     5, _
                                     6, _
                                     AMOUNT_DISCOUNTTextBox, _
                                     TAXPercentageTEXTBOX, _
                                     TAX_AMOUNTTextBox, _
                                     DOWN_PAYMENTTextBox, _
                                     GRAND_TOTALTextBox)
                    lblNoOfItem.Text = "No of Item(s) : " & dgvPurchaseDetail.RowCount - 1

                    ''Calculate Tax Amount when adding Product
                    Dim tmpGrandTotal As Integer = 0
                    For x As Integer = 0 To dgvPurchaseDetail.RowCount - 1
                        tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvPurchaseDetail.Item(6, x).Value)
                    Next
                    TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
                    TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)

                    If CInt(dgvPurchaseDetail.Item(4, dgvPurchaseDetail.CurrentCell.RowIndex).Value) > 100 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih besar dari 100%" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not greater than 100%" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvPurchaseDetail.Item(4, dgvPurchaseDetail.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                    If CInt(dgvPurchaseDetail.Item(4, dgvPurchaseDetail.CurrentCell.RowIndex).Value) < 0 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih kecil dari 0" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not smaller than 0" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvPurchaseDetail.Item(4, dgvPurchaseDetail.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                ElseIf dgvPurchaseDetail.CurrentCell.ColumnIndex = 5 Then

                    If dgvPurchaseDetail.Item(0, dgvPurchaseDetail.CurrentCell.RowIndex).Value = "" Then
                        dgvPurchaseDetail.Rows.Remove(dgvPurchaseDetail.CurrentRow)
                        Exit Sub
                    End If

                    dgvPurchaseDetail.Item(4, dgvPurchaseDetail.CurrentCell.RowIndex).Value = _
                        FormatNumber(CInt(dgvPurchaseDetail.Item(5, dgvPurchaseDetail.CurrentCell.RowIndex).Value) / _
                        (CInt(dgvPurchaseDetail.Item(2, dgvPurchaseDetail.CurrentCell.RowIndex).Value) * _
                        CInt(dgvPurchaseDetail.Item(3, dgvPurchaseDetail.CurrentCell.RowIndex).Value)) * 100, 2)

                    Calculation(dgvPurchaseDetail.Item(2, dgvPurchaseDetail.CurrentCell.RowIndex).Value, _
                                dgvPurchaseDetail.Item(3, dgvPurchaseDetail.CurrentCell.RowIndex).Value, _
                                dgvPurchaseDetail.Item(5, dgvPurchaseDetail.CurrentCell.RowIndex).Value, _
                                dgvPurchaseDetail.Item(6, dgvPurchaseDetail.CurrentCell.RowIndex).Value)
                    CalculationAll(dgvPurchaseDetail, _
                                     5, _
                                     6, _
                                     AMOUNT_DISCOUNTTextBox, _
                                     TAXPercentageTEXTBOX, _
                                     TAX_AMOUNTTextBox, _
                                     DOWN_PAYMENTTextBox, _
                                     GRAND_TOTALTextBox)
                    lblNoOfItem.Text = "No of Item(s) : " & dgvPurchaseDetail.RowCount - 1

                    ''Calculate Tax Amount when adding Product
                    Dim tmpGrandTotal As Integer = 0
                    For x As Integer = 0 To dgvPurchaseDetail.RowCount - 1
                        tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvPurchaseDetail.Item(6, x).Value)
                    Next
                    TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
                    TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)

                    'If CInt(dgvPurchaseDetail.Item("DISCOUNT2", dgvPurchaseDetail.CurrentCell.RowIndex).Value) > CInt(dgvPurchaseDetail.Item("PRICE_PER_UNIT", dgvPurchaseDetail.CurrentCell.RowIndex).Value) Then
                    '    If Language = "Indonesian" Then
                    '        MsgBox("Discount seharusnya tidak lebih besar dari 100%" & vbCrLf & _
                    '      "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                    '    Else
                    '        MsgBox("Discount should not greater than 100%" & vbCrLf & _
                    '      "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                    '    End If
                    '    dgvPurchaseDetail.Item("DISCOUNT", dgvPurchaseDetail.CurrentCell.RowIndex).Value = 0
                    '    dgvPurchaseDetail.Item("DISCOUNT2", dgvPurchaseDetail.CurrentCell.RowIndex).Value = 0
                    '    Exit Sub
                    'End If

                    If CInt(dgvPurchaseDetail.Item("TOTAL", dgvPurchaseDetail.CurrentCell.RowIndex).Value) < 0 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih besar dari 100%" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not greater than 100%" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvPurchaseDetail.Item("DISCOUNT2", dgvPurchaseDetail.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If
                   

                    If CInt(dgvPurchaseDetail.Item(5, dgvPurchaseDetail.CurrentCell.RowIndex).Value) < 0 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih kecil dari 0" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not smaller than 0" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvPurchaseDetail.Item(5, dgvPurchaseDetail.CurrentCell.RowIndex).Value = 0
                        dgvPurchaseDetail.Item(4, dgvPurchaseDetail.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                ElseIf dgvPurchaseDetail.CurrentCell.ColumnIndex = 2 Or _
                    dgvPurchaseDetail.CurrentCell.ColumnIndex = 3 Or _
                    dgvPurchaseDetail.CurrentCell.ColumnIndex = 4 Or _
                    dgvPurchaseDetail.CurrentCell.ColumnIndex = 5 Or _
                    dgvPurchaseDetail.CurrentCell.ColumnIndex = 6 Then

                    If dgvPurchaseDetail.Item(0, dgvPurchaseDetail.CurrentCell.RowIndex).Value = "" Then
                        dgvPurchaseDetail.Rows.Remove(dgvPurchaseDetail.CurrentRow)
                        Exit Sub
                    End If


                    dgvPurchaseDetail.Item(5, dgvPurchaseDetail.CurrentCell.RowIndex).Value = _
                        CInt(dgvPurchaseDetail.Item(2, dgvPurchaseDetail.CurrentCell.RowIndex).Value) * _
                        CInt(dgvPurchaseDetail.Item(3, dgvPurchaseDetail.CurrentCell.RowIndex).Value) * _
                        CInt(dgvPurchaseDetail.Item(4, dgvPurchaseDetail.CurrentCell.RowIndex).Value) / 100

                    Calculation(dgvPurchaseDetail.Item(2, dgvPurchaseDetail.CurrentCell.RowIndex).Value, _
                                dgvPurchaseDetail.Item(3, dgvPurchaseDetail.CurrentCell.RowIndex).Value, _
                                dgvPurchaseDetail.Item(5, dgvPurchaseDetail.CurrentCell.RowIndex).Value, _
                                dgvPurchaseDetail.Item(6, dgvPurchaseDetail.CurrentCell.RowIndex).Value)
                    CalculationAll(dgvPurchaseDetail, _
                                     5, _
                                     6, _
                                     AMOUNT_DISCOUNTTextBox, _
                                     TAXPercentageTEXTBOX, _
                                     TAX_AMOUNTTextBox, _
                                     DOWN_PAYMENTTextBox, _
                                     GRAND_TOTALTextBox)
                    lblNoOfItem.Text = "No of Item(s) : " & dgvPurchaseDetail.RowCount - 1
                    'dgvPurchaseDetail.CurrentCell = _
                    '    dgvPurchaseDetail.Rows(dgvPurchaseDetail.CurrentCell.RowIndex).Cells(0)


                    ''Calculate Tax Amount when adding Product
                    Dim tmpGrandTotal As Integer = 0
                    For x As Integer = 0 To dgvPurchaseDetail.RowCount - 1
                        tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvPurchaseDetail.Item(6, x).Value)
                    Next
                    TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
                    TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)

                    If CInt(dgvPurchaseDetail.Item(4, dgvPurchaseDetail.CurrentCell.RowIndex).Value) > 100 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih besar dari 100%" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not greater than 100%" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvPurchaseDetail.Item(4, dgvPurchaseDetail.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                    If CInt(dgvPurchaseDetail.Item("TOTAL", dgvPurchaseDetail.CurrentCell.RowIndex).Value) < 0 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih besar dari 100%" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not greater than 100%" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvPurchaseDetail.Item("DISCOUNT", dgvPurchaseDetail.CurrentCell.RowIndex).Value = 0
                        dgvPurchaseDetail.Item("DISCOUNT2", dgvPurchaseDetail.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                    If CInt(dgvPurchaseDetail.Item(4, dgvPurchaseDetail.CurrentCell.RowIndex).Value) < 0 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih kecil dari 0" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not smaller than 0" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvPurchaseDetail.Item(4, dgvPurchaseDetail.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                End If
            End If
        End If

        tmpChange = True
    End Sub

    Private Sub dgvPurchaseDetail_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvPurchaseDetail.KeyUp
        If tmpSaveMode = "Update" Then
            If e.KeyData = 13 Then Exit Sub
            If e.KeyCode = Keys.Delete Then Exit Sub
        Else
            If e.KeyData = 13 Then
                If dgvPurchaseDetail.RowCount = 1 Then Exit Sub
                dgvPurchaseDetail.CurrentCell = _
                    dgvPurchaseDetail.Rows(dgvPurchaseDetail.RowCount - 1).Cells(0)
            ElseIf e.KeyCode = Keys.Delete Then
                If dgvPurchaseDetail.CurrentRow.Index = dgvPurchaseDetail.RowCount - 1 Then Exit Sub
                dgvPurchaseDetail.Rows.RemoveAt(dgvPurchaseDetail.CurrentCell.RowIndex)
                CalculationAll(dgvPurchaseDetail, _
                             5, _
                             6, _
                             AMOUNT_DISCOUNTTextBox, _
                             TAXPercentageTEXTBOX, _
                             TAX_AMOUNTTextBox, _
                             DOWN_PAYMENTTextBox, _
                             GRAND_TOTALTextBox)
                lblNoOfItem.Text = "No of Item(s) : " & dgvPurchaseDetail.RowCount - 1

                If dgvPurchaseDetail.Item("PRODUCT_CODE", 0).Value = "" Then
                    TAX_AMOUNTTextBox.Text = "0"
                    TAXPercentageTEXTBOX.Text = "0"
                End If
            End If
        End If
    End Sub

    Private Sub dgvPurchaseDetail_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPurchaseDetail.CellContentClick
        If dgvPurchaseDetail.Item(0, dgvPurchaseDetail.CurrentCell.RowIndex).Value = "" Then Exit Sub
        If dgvPurchaseDetail.CurrentCell.ColumnIndex = 7 Then
            dgvPurchaseDetail.Rows.RemoveAt(dgvPurchaseDetail.CurrentCell.RowIndex)
            CalculationAll(dgvPurchaseDetail, _
                                     5, _
                                     6, _
                                     AMOUNT_DISCOUNTTextBox, _
                                     TAXPercentageTEXTBOX, _
                                     TAX_AMOUNTTextBox, _
                                     DOWN_PAYMENTTextBox, _
                                     GRAND_TOTALTextBox)
            lblNoOfItem.Text = "No of Item(s) : " & dgvPurchaseDetail.RowCount - 1
            If TAX_AMOUNTTextBox.Text = "0" Then
                TAXPercentageTEXTBOX.Text = "0.00"
            Else
                TAXPercentageTEXTBOX.Text = FormatNumber((CDbl(TAX_AMOUNTTextBox.Text) * 100) / (CDbl(GRAND_TOTALTextBox.Text) - CDbl(TAX_AMOUNTTextBox.Text)))
            End If
        End If

        If dgvPurchaseDetail.Item("PRODUCT_CODE", 0).Value = "" Then
            TAX_AMOUNTTextBox.Text = "0"
            TAXPercentageTEXTBOX.Text = "0"
        End If
    End Sub

    Private Sub TAX_AMOUNTTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TAX_AMOUNTTextBox.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""
        If GRAND_TOTALTextBox.Text = "0" Or GRAND_TOTALTextBox.Text = "" Then e.KeyChar = ""
    End Sub

    Private Sub GRAND_TOTALTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GRAND_TOTALTextBox.KeyPress
        e.KeyChar = ""
    End Sub

    Private Sub TAX_AMOUNTTextBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TAX_AMOUNTTextBox.LostFocus
        TAX_AMOUNTTextBox.Text = IIf(TAX_AMOUNTTextBox.Text = "", "0", TAX_AMOUNTTextBox.Text)
        Dim tmpGrandTotal As Integer = 0
        For x As Integer = 0 To dgvPurchaseDetail.RowCount - 1
            tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvPurchaseDetail.Item(6, x).Value)
        Next
        If TAX_AMOUNTTextBox.Text = "0" Then
            TAXPercentageTEXTBOX.Text = "0"
        Else
            TAXPercentageTEXTBOX.Text = FormatNumber((TAX_AMOUNTTextBox.Text / tmpGrandTotal) * 100)
        End If

        If Val(TAXPercentageTEXTBOX.Text) > 100 Then
            TAXPercentageTEXTBOX.Text = "0"
            TAX_AMOUNTTextBox.Text = "0"
        End If
    End Sub

    Private Sub TAX_AMOUNTTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TAX_AMOUNTTextBox.TextChanged
        TAX_AMOUNTTextBox.Text = IIf(TAX_AMOUNTTextBox.Text = "", "0", TAX_AMOUNTTextBox.Text)
        Dim tmpGrandTotal As Integer = 0
        For x As Integer = 0 To dgvPurchaseDetail.RowCount - 1
            tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvPurchaseDetail.Item(6, x).Value)
        Next
        If TAX_AMOUNTTextBox.Text = "0" Then
            TAXPercentageTEXTBOX.Text = "0"
        Else
            TAXPercentageTEXTBOX.Text = FormatNumber((TAX_AMOUNTTextBox.Text / tmpGrandTotal) * 100)
        End If

        CalculationAll(dgvPurchaseDetail, _
                         5, _
                         6, _
                         AMOUNT_DISCOUNTTextBox, _
                         TAXPercentageTEXTBOX, _
                         TAX_AMOUNTTextBox, _
                         DOWN_PAYMENTTextBox, _
                         GRAND_TOTALTextBox)
        lblNoOfItem.Text = "No of Item(s) : " & dgvPurchaseDetail.RowCount - 1

        If Val(TAXPercentageTEXTBOX.Text) > 100 Then
            TAXPercentageTEXTBOX.Text = "0"
            TAX_AMOUNTTextBox.Text = "0"
        End If
        tmpChange = True
    End Sub

    Private Sub AMOUNT_DISCOUNTTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles AMOUNT_DISCOUNTTextBox.KeyPress
        e.KeyChar = ""
    End Sub

    Private Sub TAXPercentageTEXTBOX_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TAXPercentageTEXTBOX.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""
    End Sub

    Private Sub TAXPercentageTEXTBOX_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TAXPercentageTEXTBOX.LostFocus
        Dim tmpGrandTotal As Integer = 0
        For x As Integer = 0 To dgvPurchaseDetail.RowCount - 1
            tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvPurchaseDetail.Item(6, x).Value)
        Next
        TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
        TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)
    End Sub


    Private Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click

        If PURCHASEBindingSource.Count > 100 Then
            If Language = "Indonesian" Then
                MsgBox("Aplikasi yang anda gunakan sudah melebihi batas trial !" & vbCrLf & _
                       "Silahkan menghubungi DMI-Soft untuk pemakaian Aplikasi Retail lebih lanjut", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("DMI Retail you are using is not licensed." & vbCrLf & _
                       "Please contact DMI-Soft to continue using this Software", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Me.Close()
            Exit Sub
        End If

        Dim tmpDate As DateTime
        Dim tmpPrice As Integer
        Dim tmpCheck As DateTime = PURCHASE_DATEDateTimePicker.Value.Year & "/" & _
                     PURCHASE_DATEDateTimePicker.Value.Month

        If Val(TAXPercentageTEXTBOX.Text) < 0 Or Val(TAXPercentageTEXTBOX.Text) > 100 Then
            If Language = "Indonesian" Then
                MsgBox("Nilai pajak yang anda masukan salah !" & vbCrLf & _
               "Silahkan masukan pajak yang benar !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You entered wrong tax's value !" & vbCrLf & _
                "Please enter the correct Tax !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            TAXPercentageTEXTBOX.Text = "0.00"
            TAX_AMOUNTTextBox.Text = "0"
            Exit Sub
        End If


        If PURCHASETableAdapter.SP_CLOSED_PERIOD_STOP() = tmpCheck Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menyimpan transaksi ini!" & vbCrLf & "Transaksi ini sudah masuk dalam Tutup Buku", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Save this Transaction!" & vbCrLf & "This Transaction has been in Closed Period", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If WAREHOUSE_NAMEComboBox.Text = "" Then
            TmpPurchaseWarehouse = 0
        Else
            TmpPurchaseWarehouse = WAREHOUSEBindingSource.Current("WAREHOUSE_ID")
        End If

        For i As Integer = 0 To dgvPurchaseDetail.RowCount - 2
            If Language = "Indonesian" Then
                If dgvPurchaseDetail.Item("PRICE_PER_UNIT", i).Value = 0 Then
                    tmpPrice = MsgBox("Harga Pembelian = 0 ." & vbCrLf & "Apakah anda yakin ingin melanjutkannya ?", _
                        MsgBoxStyle.YesNo, "DMI Retail")
                    If tmpPrice = 7 Then
                        Exit Sub
                    End If
                End If
            Else
                If dgvPurchaseDetail.Item("PRICE_PER_UNIT", i).Value = 0 Then
                    tmpPrice = MsgBox("Purchase price = 0 ." & vbCrLf & "Are you sure want to continue ?", _
                        MsgBoxStyle.YesNo, "DMI Retail")
                    If tmpPrice = 7 Then
                        Exit Sub
                    End If
                End If
            End If
            PURCHASETableAdapter.SP_GET_CATEGORY_PRODUCT(dgvPurchaseDetail.Item("PRODUCT_CODE", i).Value, tmpCategory)
            If tmpCategory <> 1 Then
                If Language = "Indonesian" Then
                    MsgBox("Produk yang anda input bukan barang !" & vbCrLf & _
                       "Silahkan gunakan Form Pencarian untuk menemukan produk yang benar !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("You input Product which is not Goods !" & vbCrLf & _
                           "Please use the Search Form to find the right Products !", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub

            End If
        Next

        tmpDate = PURCHASE_DATEDateTimePicker.Value.Year & "/" & _
                 PURCHASE_DATEDateTimePicker.Value.Month & "/" & _
                 PURCHASE_DATEDateTimePicker.Value.Day & " " & _
                 Now.Hour & ":" & Now.Minute & ":" & Now.Second

        If DOWN_PAYMENTTextBox.Text = "" Then DOWN_PAYMENTTextBox.Text = "0"

        For N = 0 To dgvPurchaseDetail.RowCount - 2
            If dgvPurchaseDetail.Item("QUANTITY", N).Value = "0" Then
                If Language = "Indonesian" Then
                    MsgBox("Silahkan isi kolom Jumlah !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Please Fill Quantity fields !", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
                'ElseIf dgvPurchaseDetail.Item("PRICE_PER_UNIT", N).Value = "" Then
                '  MsgBox("Please Fill Unit Price fields !", MsgBoxStyle.Critical, "DMI Retail")
                '  Exit Sub
            End If
        Next

        If RECEIPT_NOTextBox.Text = "" Or PAYMENT_METHOD_IDComboBox.Text = "" Or _
            GRAND_TOTALTextBox.Text = "" Or dgvPurchaseDetail.RowCount = 1 Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If Not ValidateAllComboBox(Me) Then
            If Language = "Indonesian" Then
                MsgBox("Nilai yang anda masukan tidak terdapat di daftar.", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You input value which is not in the list.", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If CInt(DOWN_PAYMENTTextBox.Text) > CInt(GRAND_TOTALTextBox.Text) Then
            If Language = "Indonesian" Then
                MsgBox("Uang muka seharusnya tidak lebih besar dari Total Pembayaran!", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Down Payment should not greater than Grand Total!", MsgBoxStyle.Critical, "DMI Retail")
            End If
            DOWN_PAYMENTTextBox.Focus()
            Exit Sub
        End If

        If MATURITY_DATEDateTimePicker.Value < PURCHASE_DATEDateTimePicker.Value Then
            If Language = "Indonesian" Then
                MsgBox("Tanggal jatuh tempo seharusnya lebih besar dari tanggal pembelian!", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Maturity Date should greater than Purchase Date!", MsgBoxStyle.Critical, "DMI Retail")
            End If
            MATURITY_DATEDateTimePicker.Value = Now
            Exit Sub
        End If

        PURCHASETableAdapter.SP_PURCHASE_CHECK_RECEIPT_NO(RECEIPT_NOTextBox.Text, tmpNoOfReceiptNo)
        If tmpNoOfReceiptNo > 0 Then
            If Language = "Indonesian" Then
                MsgBox("No Transaksi telah digunakan oleh user lain." & vbCrLf & _
                                            "Nomor transaksi baru akan digunakan untuk transaksi ini.", _
                                            MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Receipt Number is used by another User." & vbCrLf & _
                                            "New Receipt Number will be assigned to this transaction.", _
                                            MsgBoxStyle.Critical, "DMI Retail")
            End If
        End If
        While tmpNoOfReceiptNo > 0
            PURCHASETableAdapter.SP_GENERATE_PURCHASE_RECEIPT_NO(RECEIPT_NOTextBox.Text)
            PURCHASETableAdapter.SP_PURCHASE_CHECK_RECEIPT_NO(RECEIPT_NOTextBox.Text, tmpNoOfReceiptNo)
        End While

        If VENDOR_IDComboBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        Dim tmpVendorID As Integer
        If VENDORBindingSource.Count = 0 Then
            tmpVendorID = 0
        Else
            tmpVendorID = IIf(VENDOR_IDComboBox.Text.Length = 0, 0, VENDORBindingSource.Current("VENDOR_ID"))
        End If

        If PURCHASETableAdapter.SP_CLOSED_PERIOD_PROCESS("G", PURCHASE_DATEDateTimePicker.Value) = "TRUE" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menyimpan transaksi ini!" & vbCrLf & "Proses Tutup Buku sedang berlangsung", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Save this Transaction!" & vbCrLf & "The Closing Period is Running", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If txtPoNo.Text = "" Then
            tmpPurchaseOrderId = 0
        End If
        DOWN_PAYMENTTextBox.Text = IIf(DOWN_PAYMENTTextBox.Text.Length = 0, "0", DOWN_PAYMENTTextBox.Text)
        PURCHASETableAdapter.SP_PURCHASE("I", _
                                         0, _
                                         RECEIPT_NOTextBox.Text, _
                                         tmpDate, _
                                         tmpVendorID, _
                                         DOWN_PAYMENTTextBox.Text, _
                                         AMOUNT_DISCOUNTTextBox.Text, _
                                         TAX_AMOUNTTextBox.Text, _
                                         GRAND_TOTALTextBox.Text, _
                                         REMARKTextBox.Text, _
                                         PAYMENT_METHODBindingSource.Current("PAYMENT_METHOD_ID"), _
                                         DateSerial(MATURITY_DATEDateTimePicker.Value.Year, _
                                            MATURITY_DATEDateTimePicker.Value.Month, _
                                            MATURITY_DATEDateTimePicker.Value.Day), _
                                         mdlGeneral.USER_ID, _
                                         Now, _
                                         0, _
                                         DateSerial(4000, 12, 31), _
                                         TmpPurchaseWarehouse, _
                                         tmpPurchaseOrderId)

        For x As Integer = 0 To dgvPurchaseDetail.RowCount - 2
            PRODUCTTableAdapter.SP_GET_PRODUCT_ID(dgvPurchaseDetail.Item(0, x).Value, tmpProductID)
            PURCHASE_DETAILTableAdapter.SP_PURCHASE_DETAIL("I", _
                                                           0, _
                                                           0, _
                                                           RECEIPT_NOTextBox.Text, _
                                                           tmpProductID, _
                                                           CInt(dgvPurchaseDetail.Item(3, x).Value), _
                                                           CInt(dgvPurchaseDetail.Item(2, x).Value), _
                                                           CInt(dgvPurchaseDetail.Item(5, x).Value), _
                                                           CInt(dgvPurchaseDetail.Item(6, x).Value), _
                                                           mdlGeneral.USER_ID, _
                                                           Now, _
                                                           0, _
                                                           DateSerial(4000, 12, 31))

            If txtPoNo.Text <> "" Then
                Dim tmpQuantityPurchase, tmpQtyOrder As Integer
                PURCHASETableAdapter.SP_GET_PARAMETER_QUANTITY_PURCHASE_ORDER(tmpPurchaseOrderId, tmpProductID, tmpQuantityPurchase, tmpQtyOrder)
                If tmpQuantityPurchase >= tmpQtyOrder Then
                    SP_SELECT_PURCHASE_ORDER_DETAILTableAdapter.SP_UPDATE_STATUS_PURCHASE_ORDER("Close", _
                                                                                                  "Close", _
                                                                                                  "Close", _
                                                                                                  tmpPurchaseOrderId, _
                                                                                                  tmpProductID)
                End If
            End If

            PRODUCT_WAREHOUSETableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                               0, _
                                                               tmpProductID, _
                                                               TmpPurchaseWarehouse, _
                                                               CInt(dgvPurchaseDetail.Item(2, x).Value), _
                                                               USER_ID, _
                                                               Now, _
                                                               0, _
                                                               DateSerial(4000, 12, 31))
        Next

        PURCHASETableAdapter.SP_PAYABLE("I", _
                                        0, _
                                        0, _
                                        CInt(GRAND_TOTALTextBox.Text), _
                                        mdlGeneral.USER_ID, _
                                        PURCHASE_DATEDateTimePicker.Value, _
                                        0, _
                                        DateSerial(4000, 12, 31))


        ''If PAYMENT_GROUP not in "Cash"
        PAYMENT_METHODTableAdapter.SP_GET_PAYMENT_GROUP(PAYMENT_METHODBindingSource.Current("PAYMENT_METHOD_ID"), _
                                                                        tmpPaymentGroup)

        Try
            If tmpPaymentGroup <> "Cash" And CInt(DOWN_PAYMENTTextBox.Text) > 0 Then


                PURCHASETableAdapter.SP_PAYABLE_PAYMENT_GENERATE_NUMBER(tmpPaymentNo)
                PURCHASETableAdapter.SP_GET_PURCHASE_ID(RECEIPT_NOTextBox.Text, TmpPaymentPurchaseID)
                TmpBalance = CInt(GRAND_TOTALTextBox.Text) - CInt(DOWN_PAYMENTTextBox.Text)

                PURCHASETableAdapter.SP_PAYABLE_PAYMENT("I", _
                                                        0, _
                                                        tmpPaymentNo, _
                                                        TmpPaymentPurchaseID, _
                                                        tmpDate, _
                                                        CInt(DOWN_PAYMENTTextBox.Text), _
                                                        PAYMENT_METHODBindingSource.Current("PAYMENT_METHOD_ID"), _
                                                        REMARKTextBox.Text, _
                                                        TmpBalance, _
                                                        mdlGeneral.USER_ID, _
                                                        Now, _
                                                        0, _
                                                        DateSerial(4000, 12, 31))

            End If
        Catch
            If Language = "Indonesian" Then
                MsgBox("Pembayaran tidak tersimpan !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Can't save the Payment !", MsgBoxStyle.Critical, "DMI Retail")
            End If
        End Try



        If Language = "Indonesian" Then
            MsgBox("Data Berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
        End If
        WAREHOUSE_NAMEComboBox.Text = ""
        cmdAdd_Click(Nothing, Nothing)
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If RECEIPT_NOTextBox.Text = "" Then Exit Sub

        If Trim(RsAccessPrivilege.Item("ALLOW_DELETE").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        Dim tmpCheck As DateTime = PURCHASE_DATEDateTimePicker.Value.Year & "/" & _
                     PURCHASE_DATEDateTimePicker.Value.Month

        If PURCHASETableAdapter.SP_CLOSED_PERIOD_STOP() = tmpCheck Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menghapus transaksi in!" & vbCrLf & "Transaksi ini telah masuk dalam proses Tutup Buku", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Delete this Transaction!" & vbCrLf & "This Transaction has been in Closed Period", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If
        If Language = "Indonesian" Then
            If MsgBox("Batalkan transaksi ini?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = _
                MsgBoxResult.No Then Exit Sub
        Else
            If MsgBox("Void this transaction?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = _
               MsgBoxResult.No Then Exit Sub
        End If

        ''get payment group in this transaction
        Dim tmpPaymentGroup As String = ""
        PURCHASETableAdapter.SP_GET_PAYMENT_GROUP(PAYMENT_METHODBindingSource.Current("PAYMENT_METHOD_ID"), tmpPaymentGroup)

        tmpNewPurchaseDate = PURCHASE_DATEDateTimePicker.Value
        tmpNewPurchaseId = tmpPurchaseId

        If tmpPaymentGroup = "Credit" Then
            ''''get payment date''''
            tmpNewPaymentDate = PURCHASETableAdapter.SP_CHECK_TRANSACTION_PAYABLE(tmpNewPurchaseId)
            ''''check sale date & payment date''''
            If tmpNewPaymentDate <> tmpNewPurchaseDate Then
                If Language = "Indonesian" Then
                    MsgBox("Tidak dapat menghapus transaksi ini !" & vbCrLf & "Sudah ada pembayaran hutang lain", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Cannot delete this transaction !" & vbCrLf & "There have been other accounts payable payments", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If

            PURCHASETableAdapter.SP_VOID_PAYABLE_PAYMENT(tmpNewPurchaseId, tmpNewPaymentDate)
        End If


        PURCHASETableAdapter.SP_PURCHASE("V", _
                                 0, _
                                 RECEIPT_NOTextBox.Text, _
                                 PURCHASE_DATEDateTimePicker.Value, _
                                 0, _
                                 0, _
                                 0, _
                                 0, _
                                 0, _
                                 "", _
                                 0, _
                                 DateSerial(4000, 12, 31), _
                                 0, _
                                 DateSerial(4000, 12, 31), _
                                 USER_ID, _
                                 Now, _
                                 tmpWarehouseID,
                                 0)

        For x As Integer = 0 To dgvPurchaseDetail.RowCount - 1
            PRODUCTTableAdapter.SP_GET_PRODUCT_ID(dgvPurchaseDetail.Item(0, x).Value, tmpProductID)
            PRODUCT_WAREHOUSETableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                               0, _
                                                               tmpProductID, _
                                                               tmpWarehouseID, _
                                                               CInt(dgvPurchaseDetail.Item(2, x).Value) * (-1), _
                                                               USER_ID, _
                                                               Now, _
                                                               0, _
                                                               DateSerial(4000, 12, 31))

            '''''VOID PURCHASE ORDER'''''
            If Not IsDBNull(frmListPurchase.SP_LIST_PURCHASEBindingSource.Current("PURCHASE_ORDER_ID")) Then
                SP_SELECT_PURCHASE_ORDER_DETAILTableAdapter.SP_UPDATE_STATUS_PURCHASE_ORDER("Open", _
                                                                                            "Open", _
                                                                                            "Open", _
                                                                                            frmListPurchase.SP_LIST_PURCHASEBindingSource.Current("PURCHASE_ORDER_ID"), _
                                                                                            tmpProductID)
            End If
        Next

        If Language = "Indonesian" Then
            MsgBox("Transaksi ini telah di batalkan", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("This Transaction has been Voided", MsgBoxStyle.Information, "DMI Retail")
        End If
        Me.Close()
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        PURCHASETableAdapter.SP_GENERATE_PURCHASE_RECEIPT_NO(RECEIPT_NOTextBox.Text)
        dgvPurchaseDetail.Rows.Clear()
        lblNoOfItem.Text = "No of Item(s) : 0"
        REMARKTextBox.Text = ""
        AMOUNT_DISCOUNTTextBox.Text = "0"
        TAXPercentageTEXTBOX.Text = "0.00"
        TAX_AMOUNTTextBox.Text = "0"
        DOWN_PAYMENTTextBox.Text = "0"
        GRAND_TOTALTextBox.Text = "0"
        txtPoNo.Text = ""
        dtpPoDate.Value = Now
        dgvPurchaseDetail.Focus()

        ''Default Parameter Vendor
        If PURCHASETableAdapter.SP_SELECT_PARAMETER("PURCHASE VENDOR") = "0" Then
            VENDOR_IDComboBox.SelectedIndex = -1
        Else
            VENDORBindingSource.Position = _
            VENDORBindingSource.Find("VENDOR_ID", _
                                     PURCHASETableAdapter.SP_SELECT_PARAMETER("PURCHASE VENDOR"))
        End If

        If PAYMENT_METHODBindingSource Is Nothing Or PAYMENT_METHODBindingSource.Count < 1 Then
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data cara bayar." & vbCrLf & "Tolong input salah satu cara pembayaran!", _
                       MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Payment Method data." & vbCrLf & "Please enter at least one Payment Method data!", _
                   MsgBoxStyle.Critical, "DMI Retail")
            End If
            tmpNoData = True
        Else
            ''Default Parameter Payment Method
            If PURCHASETableAdapter.SP_SELECT_PARAMETER("PURCHASE PAYMENT METHOD") = "0" Then
                PAYMENT_METHOD_IDComboBox.SelectedIndex = -1
            Else
                PAYMENT_METHODBindingSource.Position = _
                    PAYMENT_METHODBindingSource.Find("PAYMENT_METHOD_ID", _
                                                     PURCHASETableAdapter.SP_SELECT_PARAMETER("PURCHASE PAYMENT METHOD"))
            End If
        End If


        If WAREHOUSEBindingSource Is Nothing Or WAREHOUSEBindingSource.Count < 1 Then
            'If Language = "Indonesian" Then
            '    MsgBox("Tidak ada data gudang." & vbCrLf & "Tolong input salah satu gudang!", _
            '           MsgBoxStyle.Critical, "DMI Retail")
            'Else
            '    MsgBox("There is no Warehouse data." & vbCrLf & "Please enter at least one Warehouse data!", _
            '       MsgBoxStyle.Critical, "DMI Retail")
            'End If
            'tmpNoData = True
            WAREHOUSE_NAMEComboBox.SelectedIndex = -1
        Else
            ''Default Parameter Warehouse
            If PURCHASETableAdapter.SP_SELECT_PARAMETER("PURCHASE WAREHOUSE") = "0" Then
                WAREHOUSE_NAMEComboBox.SelectedIndex = -1
            Else
                WAREHOUSEBindingSource.Position = _
                WAREHOUSEBindingSource.Find("WAREHOUSE_ID", _
                                            PURCHASETableAdapter.SP_SELECT_PARAMETER("PURCHASE WAREHOUSE"))
            End If
        End If

        tmpChange = False
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        tmpSaveMode = ""
        Me.Close()
        tmpChange = False
    End Sub

    Private Sub DOWN_PAYMENTTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""
    End Sub

    Private Sub AMOUNT_DISCOUNTTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AMOUNT_DISCOUNTTextBox.TextChanged
        If AMOUNT_DISCOUNTTextBox.Text = "" Then
            Dim tmpDisc As Integer

            For x As Integer = 0 To dgvPurchaseDetail.RowCount - 1
                tmpDisc = tmpDisc + dgvPurchaseDetail.Item("DISCOUNT2", x).Value

            Next

            AMOUNT_DISCOUNTTextBox.Text = FormatNumber(CInt(tmpDisc), 0)
        End If
        tmpChange = True
    End Sub

    Private Sub GRAND_TOTALTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GRAND_TOTALTextBox.TextChanged
        If GRAND_TOTALTextBox.Text = "" Then
            Dim tmpTotal As Integer

            For x As Integer = 0 To dgvPurchaseDetail.RowCount - 1
                tmpTotal = tmpTotal + dgvPurchaseDetail.Item("TOTAL", x).Value
            Next

            GRAND_TOTALTextBox.Text = FormatNumber(CInt(tmpTotal) + CInt(TAX_AMOUNTTextBox.Text), 0)
        End If
        tmpChange = True
    End Sub


    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        tmpChange = False
    End Sub

    Private Sub RECEIPT_NOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RECEIPT_NOTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub PURCHASE_DATEDateTimePicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PURCHASE_DATEDateTimePicker.ValueChanged
        tmpChange = True
    End Sub

    Private Sub VENDOR_IDComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VENDOR_IDComboBox.SelectedIndexChanged
        tmpChange = True
    End Sub

    Private Sub PAYMENT_METHOD_IDComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PAYMENT_METHOD_IDComboBox.SelectedIndexChanged
        tmpChange = True
    End Sub

    Private Sub MATURITY_DATEDateTimePicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MATURITY_DATEDateTimePicker.ValueChanged
        tmpChange = True
    End Sub

    Private Sub REMARKTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles REMARKTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub TAXPercentageTEXTBOX_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TAXPercentageTEXTBOX.TextChanged
        If Val(TAXPercentageTEXTBOX.Text) > 100 Then
            TAXPercentageTEXTBOX.Text = "0.00"
            TAX_AMOUNTTextBox.Text = "0"
        End If
        tmpChange = True
    End Sub

    Private Sub DOWN_PAYMENTTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tmpChange = True
    End Sub

    Private Sub WAREHOUSE_NAMEComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WAREHOUSE_NAMEComboBox.SelectedIndexChanged
        tmpChange = True
    End Sub

    Private Sub txtDTP_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDTP.KeyDown

        If e.KeyCode = Keys.Delete Then
            e.Handled = True
            Exit Sub
        End If
      
    End Sub

    Private Sub txtDTP_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDTP.KeyPress
        e.KeyChar = ""
    End Sub

    Private Sub frmEntryPurchase_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
        'If tmpNoData Then Me.Close()
    End Sub

    Private Sub WAREHOUSE_NAMEComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles WAREHOUSE_NAMEComboBox.TextChanged

    End Sub

    Private Sub WAREHOUSEBindingSource_CurrentChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles WAREHOUSEBindingSource.CurrentChanged
        'If WAREHOUSE_NAMEComboBox.Text <> "" Then
        '    If WAREHOUSEBindingSource.Current("WAREHOUSE_ID") = -1 Then
        '        If Language = "Indonesian" Then
        '            MsgBox("Silahkan pilih gudang lain ." & vbCrLf & _
        '           "Ini adalah Gudang barang rusak !", MsgBoxStyle.Critical, "DMI Retail")

        '        Else
        '            MsgBox("Please enter other warehouse ." & vbCrLf & _
        '                "This Warehouse is Rummage !", MsgBoxStyle.Critical, "DMI Retail")
        '        End If
        '        WAREHOUSEBindingSource.Position = 0
        '        Exit Sub
        '    End If
        'End If
    End Sub

    Private Sub txtDTP_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDTP.TextChanged

    End Sub

    Private Sub DOWN_PAYMENTTextBox_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DOWN_PAYMENTTextBox.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Asc(e.KeyChar) < Asc("0") Or Asc(e.KeyChar) > Asc("9") Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub DOWN_PAYMENTTextBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles DOWN_PAYMENTTextBox.LostFocus
        Try
            DOWN_PAYMENTTextBox.Text = FormatNumber(DOWN_PAYMENTTextBox.Text, 0)
        Catch
        End Try
    End Sub

    Private Sub cmdSearchSaleOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchSaleOrder.Click
        Dim popPurchaseOrder As New frmSearchPurchaseOrder
        dgvPurchaseDetail.Rows.Clear()
        popPurchaseOrder.ShowDialog(Me)
        Try
            txtPoNo.Text = popPurchaseOrder.tmpPurchaseOrderNo
            dtpPoDate.Value = popPurchaseOrder.tmpPurchaseOrderDate
            tmpPurchaseOrderId = popPurchaseOrder.tmpPurchaseOrderId
            tmpTotItem = popPurchaseOrder.tmpTotItem
            VENDORBindingSource.Position = VENDORBindingSource.Find("VENDOR_ID", popPurchaseOrder.tmpVendorId)
            If Not IsDBNull(popPurchaseOrder.SP_SELECT_PURCHASE_ORDER_HEADERBindingSource.Current("TAX_AMOUNT")) Then
                TAX_AMOUNTTextBox.Text = popPurchaseOrder.SP_SELECT_PURCHASE_ORDER_HEADERBindingSource.Current("TAX_AMOUNT")
                TAXPercentageTEXTBOX.Text = popPurchaseOrder.SP_SELECT_PURCHASE_ORDER_HEADERBindingSource.Current("TAX_PERCENTAGE")
            End If
        Catch ex As Exception
            dgvPurchaseDetail.Rows.Clear()
            txtPoNo.Text = ""
            dtpPoDate.Value = Now
            AMOUNT_DISCOUNTTextBox.Text = "0"
            TAX_AMOUNTTextBox.Text = "0"
            TAXPercentageTEXTBOX.Text = "0.00"
            Exit Sub
        End Try

        For X As Integer = 0 To tmpTotItem - 1
            tmpFillingDGV = True
            Me.SP_SELECT_PURCHASE_ORDER_DETAILTableAdapter.Fill(Me.DS_PURCHASE_ORDER.SP_SELECT_PURCHASE_ORDER_DETAIL, tmpPurchaseOrderId)
            SP_SELECT_PURCHASE_ORDER_DETAILBindingSource.Position = X

            dgvPurchaseDetail.Rows.Add()
            AddDataToGridOrder(dgvPurchaseDetail, _
                               SP_SELECT_PURCHASE_ORDER_DETAILBindingSource.Current("PRODUCT_CODE"), _
                               0, _
                               SP_SELECT_PURCHASE_ORDER_DETAILBindingSource.Current("PRODUCT_NAME"), _
                               1, _
                               SP_SELECT_PURCHASE_ORDER_DETAILBindingSource.Current("QUANTITY"), _
                               2, _
                               SP_SELECT_PURCHASE_ORDER_DETAILBindingSource.Current("PRICE_PER_UNIT"), _
                               3, _
                               SP_SELECT_PURCHASE_ORDER_DETAILBindingSource.Current("DISC_PERCENTAGE"), _
                               4, _
                               SP_SELECT_PURCHASE_ORDER_DETAILBindingSource.Current("DISC_AMOUNT"), _
                               5, _
                               6)
            CalculationAll(dgvPurchaseDetail, _
                                   5, _
                                   6, _
                                   AMOUNT_DISCOUNTTextBox, _
                                   TAXPercentageTEXTBOX, _
                                   TAX_AMOUNTTextBox, _
                                   DOWN_PAYMENTTextBox, _
                                   GRAND_TOTALTextBox)

        Next

    End Sub

    Private Sub AddDataToGridOrder(ByRef pDataGridView As DataGridView, _
                              ByVal pCode As String, ByVal pCodeCol As Integer, _
                              ByVal pName As String, ByVal pNameCol As Integer, _
                              ByVal pQuantity As String, ByVal pQuantityCol As Integer, _
                              ByVal pUnitPrice As String, ByVal pUnitPriceCol As Integer, _
                              ByVal pDiscPer As Integer, ByVal pDiscPerCol As Integer, _
                              ByVal pDiscAmount As Integer, ByVal pDiscAmountCol As Integer, _
                              ByVal pTotalCol As Integer)

        If pDataGridView.RowCount > 2 Then
            For x As Integer = 0 To pDataGridView.RowCount - 3
                If pDataGridView.Item(pCodeCol, x).Value.ToString.ToUpper = pCode.ToUpper Then
                    tmpFillingDGV = True
                    If tmpQuantity = pDataGridView.Item(pQuantityCol, x).Value Then
                        MsgBox("Selling a Product with quantity exceed the balance is not allowed." & vbCrLf & _
                               "Adding this Product is cancelled!", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        pDataGridView.Item(pQuantityCol, x).Value = _
                            CInt(pDataGridView.Item(pQuantityCol, x).Value) + 1
                    End If
                    pDataGridView.Rows.RemoveAt(pDataGridView.RowCount - 2)
                    Calculation(pDataGridView.Item(pQuantityCol, x).Value, _
                                pDataGridView.Item(pUnitPriceCol, x).Value, _
                                pDataGridView.Item(pDiscAmountCol, x).Value, _
                                pDataGridView.Item(pTotalCol, x).Value)
                    tmpFillingDGV = False
                    pDataGridView.CurrentCell = _
                        pDataGridView.Rows(pDataGridView.RowCount - 1).Cells(0)
                    Exit Sub
                End If
            Next
            For y As Integer = 0 To pDataGridView.RowCount - 2
                pDataGridView.Item(0, y).ReadOnly = True
            Next
        End If

        If pDataGridView.RowCount = 1 Then pDataGridView.Rows.Add(1)
        pDataGridView.Item(pCodeCol, pDataGridView.RowCount - 2).Value = pCode
        pDataGridView.Item(pNameCol, pDataGridView.RowCount - 2).Value = pName
        pDataGridView.Item(pQuantityCol, pDataGridView.RowCount - 2).Value = CInt(pQuantity)
        pDataGridView.Item(pUnitPriceCol, pDataGridView.RowCount - 2).Value = CInt(pUnitPrice)
        pDataGridView.Item(pDiscPerCol, pDataGridView.RowCount - 2).Value = CInt(pDiscPer)
        pDataGridView.Item(pDiscAmountCol, pDataGridView.RowCount - 2).Value = CInt(pDiscAmount)
        pDataGridView.Item(pTotalCol, pDataGridView.RowCount - 2).Value = _
            (CInt(pDataGridView.Item(pQuantityCol, pDataGridView.RowCount - 2).Value) * _
            CInt(pDataGridView.Item(pUnitPriceCol, pDataGridView.RowCount - 2).Value)) - _
            CInt(pDataGridView.Item(pDiscAmountCol, pDataGridView.RowCount - 2).Value)
        tmpFillingDGV = False

        pDataGridView.CurrentCell = pDataGridView.Rows(pDataGridView.RowCount - 1).Cells(0)

    End Sub

End Class
