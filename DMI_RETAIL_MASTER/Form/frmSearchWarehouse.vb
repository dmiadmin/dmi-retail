﻿Imports System.Data.SqlClient

Public Class frmSearchWarehouse
    Public WAREHOUSEBindingSource As New BindingSource

    Private Sub GetData()
        sql = "SELECT WAREHOUSE_ID, WAREHOUSE_NAME, DESCRIPTION, WAREHOUSE_ADDRESS, WAREHOUSE_CITY, " & _
              "WAREHOUSE_COUNTRY, PIC, PHONE FROM dbo.WAREHOUSE"
        sqladapter = New SqlDataAdapter(sql, xConn)
        dt = New DataTable
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        WAREHOUSEBindingSource.DataSource = dt
        WAREHOUSEBindingNavigator.BindingSource = WAREHOUSEBindingSource
        dgvSearchWarehouse.DataSource = WAREHOUSEBindingSource
        dgvSearchWarehouse.Columns("WAREHOUSE_ID").Visible = False
    End Sub

    Private Sub frmSearchWarehouse_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_WAREHOUSE.WAREHOUSE' table. You can move, or remove it, as needed.
        'Me.WAREHOUSETableAdapter.Fill(Me.DS_WAREHOUSE.WAREHOUSE)
        GetData()
        tmpSearchResult = ""
        txtWarehouse.Text = ""
        txtWarehouse.Focus()

        If Language = "Indonesian" Then
            lblWarehouse.Text = "Gudang"
            Me.Text = "Pencarian Gudang"
            dgvSearchWarehouse.Columns("WAREHOUSE_NAME").HeaderText = "Nama Gudang"
            dgvSearchWarehouse.Columns("WAREHOUSE_ADDRESS").HeaderText = "Alamat"
            dgvSearchWarehouse.Columns("WAREHOUSE_CITY").HeaderText = "Kota"
            dgvSearchWarehouse.Columns("WAREHOUSE_COUNTRY").HeaderText = "Negara"
            dgvSearchWarehouse.Columns("PIC").HeaderText = "POS"
            dgvSearchWarehouse.Columns("PHONE").HeaderText = "Telp."
            dgvSearchWarehouse.Columns("DESCRIPTION").HeaderText = "Keterangan"
        Else
            dgvSearchWarehouse.Columns("WAREHOUSE_NAME").HeaderText = "Warehouse Name"
            dgvSearchWarehouse.Columns("WAREHOUSE_ADDRESS").HeaderText = "Address"
            dgvSearchWarehouse.Columns("WAREHOUSE_CITY").HeaderText = "City"
            dgvSearchWarehouse.Columns("WAREHOUSE_COUNTRY").HeaderText = "Country"
            dgvSearchWarehouse.Columns("PIC").HeaderText = "PIC"
            dgvSearchWarehouse.Columns("PHONE").HeaderText = "Telp."
            dgvSearchWarehouse.Columns("DESCRIPTION").HeaderText = "Description"
        End If

    End Sub

    Private Sub frmSearchWarehouse_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
        txtWarehouse.Focus()
    End Sub

    Private Sub txtWarehouse_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtWarehouse.TextChanged
        Try
            WAREHOUSEBindingSource.Filter = "WAREHOUSE_NAME LIKE '%" & txtWarehouse.Text.ToUpper & "%'"
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtWarehouse.Text = ""
            txtWarehouse.Focus()
        End Try

    End Sub

    Private Sub dgvSearchWarehouse_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSearchWarehouse.DoubleClick
        Try
            tmpSearchResult = WAREHOUSEBindingSource.Current("WAREHOUSE_ID")
        Catch
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data gudang !" & vbCrLf & _
                       "Silahkan input setidaknya satu data gudang !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Warehouse data !" & vbCrLf & _
                       "Please input at least one Warehouse data !", MsgBoxStyle.Critical, "DMI Retail")
            End If
        End Try
        Me.Close()
    End Sub

    Private Sub dgvSearchWarehouse_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvSearchWarehouse.KeyDown
        If e.KeyCode = Keys.Enter Then
            Try
                tmpSearchResult = WAREHOUSEBindingSource.Current("WAREHOUSE_ID")
            Catch
                If Language = "Indonesian" Then
                    MsgBox("Tidak ada data gudang !" & vbCrLf & _
                           "Silahkan input setidaknya satu data gudang !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("There is no Warehouse data !" & vbCrLf & _
                           "Please input at least one Warehouse data !", MsgBoxStyle.Critical, "DMI Retail")
                End If
            End Try
            Me.Close()
        ElseIf e.KeyCode = Keys.Escape Then
            tmpSearchResult = ""
            Me.Close()
        End If
    End Sub
End Class