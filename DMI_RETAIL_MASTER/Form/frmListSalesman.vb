﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frmListSalesman
    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter

    Private Sub frmListSalesman_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_SALE.SALESMAN' table. You can move, or remove it, as needed.
        'Me.SALESMANTableAdapter.Fill(Me.DS_SALE.SALESMAN)

        Dim xDS_Salesman As New DataSet
        With xComm
            .Connection = xConn
            .CommandText = "SELECT SALESMAN_ID ," & _
            " SALESMAN_CODE ," & _
            " SALESMAN_NAME ," & _
            " SALESMAN_ADDRESS ," & _
            " SALESMAN_ADDRESS_CITY ," & _
            " SALESMAN_ADDRESS_STATE ," & _
            " SALESMAN_PHONE1 ," & _
            " SALESMAN_PHONE2 ," & _
            " SALESMAN_PHONE3 ," & _
            " EFFECTIVE_START_DATE," & _
            " EFFECTIVE_END_DATE" & _
            " FROM SALESMAN " & _
            " ORDER BY SALESMAN_NAME"
            .CommandType = CommandType.Text
        End With
        xAdoAdapter.SelectCommand = xComm
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        Me.xAdoAdapter.Fill(xDS_Salesman, "SALESMAN")
        Me.dgvSALESMAN.DataSource = xDS_Salesman.Tables("SALESMAN")

        Label2.Visible = True
        If Language = "Indonesian" Then
            Me.Text = "Daftar Salesman"
            lblSalesmanName.Text = "Nama Salesman"
            CheckBox1.Text = "Aktif saja"
            dgvSALESMAN.Columns("SALESMAN_ID").Visible = False
            dgvSALESMAN.Columns("EFFECTIVE_END_DATE").Visible = False
            dgvSALESMAN.Columns("EFFECTIVE_START_DATE").Visible = False
            dgvSALESMAN.Columns("SALESMAN_CODE").HeaderText = "Kode Salesman"
            dgvSALESMAN.Columns("SALESMAN_NAME").HeaderText = "Nama Salesman"
            dgvSALESMAN.Columns("SALESMAN_ADDRESS").HeaderText = "Alamat"
            dgvSALESMAN.Columns("SALESMAN_ADDRESS_CITY").HeaderText = "Kota"
            dgvSALESMAN.Columns("SALESMAN_ADDRESS_STATE").HeaderText = "Propinsi"
            dgvSALESMAN.Columns("SALESMAN_PHONE1").HeaderText = "Telp.1"
            dgvSALESMAN.Columns("SALESMAN_PHONE2").HeaderText = "Telp.2"
            dgvSALESMAN.Columns("SALESMAN_PHONE3").HeaderText = "Telp.3"
            dgvSALESMAN.Columns("SALESMAN_ADDRESS").Width = 75
            dgvSALESMAN.Columns("SALESMAN_ADDRESS_CITY").Width = 75
            dgvSALESMAN.Columns("SALESMAN_ADDRESS_STATE").Width = 75
            dgvSALESMAN.Columns("SALESMAN_PHONE1").Width = 65
            dgvSALESMAN.Columns("SALESMAN_PHONE2").Width = 65
            dgvSALESMAN.Columns("SALESMAN_PHONE3").Width = 65
            Label2.Text = "Jumlah Data : " & dgvSALESMAN.RowCount
        Else
            dgvSALESMAN.Columns("SALESMAN_ID").Visible = False
            dgvSALESMAN.Columns("EFFECTIVE_END_DATE").Visible = False
            dgvSALESMAN.Columns("EFFECTIVE_START_DATE").Visible = False
            dgvSALESMAN.Columns("SALESMAN_CODE").HeaderText = "Salesman Code"
            dgvSALESMAN.Columns("SALESMAN_NAME").HeaderText = "Salesman Name"
            dgvSALESMAN.Columns("SALESMAN_ADDRESS").HeaderText = "Address"
            dgvSALESMAN.Columns("SALESMAN_ADDRESS_CITY").HeaderText = "City"
            dgvSALESMAN.Columns("SALESMAN_ADDRESS_STATE").HeaderText = "State"
            dgvSALESMAN.Columns("SALESMAN_PHONE1").HeaderText = "Telp.1"
            dgvSALESMAN.Columns("SALESMAN_PHONE2").HeaderText = "Telp.2"
            dgvSALESMAN.Columns("SALESMAN_PHONE3").HeaderText = "Telp.3"
            dgvSALESMAN.Columns("SALESMAN_ADDRESS").Width = 75
            dgvSALESMAN.Columns("SALESMAN_ADDRESS_CITY").Width = 75
            dgvSALESMAN.Columns("SALESMAN_ADDRESS_STATE").Width = 75
            dgvSALESMAN.Columns("SALESMAN_PHONE1").Width = 65
            dgvSALESMAN.Columns("SALESMAN_PHONE2").Width = 65
            dgvSALESMAN.Columns("SALESMAN_PHONE3").Width = 65
            Label2.Text = "No Of Salesman : " & dgvSALESMAN.RowCount
        End If

    End Sub

    Private Sub txtSalesmanName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSalesmanName.TextChanged
        If CheckBox1.Checked = True Then
            Dim xDS_Salesman As New DataSet
            With xComm
                .Connection = xConn
                .CommandText = "SELECT SALESMAN_ID ," & _
                " SALESMAN_CODE ," & _
                " SALESMAN_NAME ," & _
                " SALESMAN_ADDRESS ," & _
                " SALESMAN_ADDRESS_CITY ," & _
                " SALESMAN_ADDRESS_STATE ," & _
                " SALESMAN_PHONE1 ," & _
                " SALESMAN_PHONE2 ," & _
                " SALESMAN_PHONE3 ," & _
                " EFFECTIVE_START_DATE," & _
                " EFFECTIVE_END_DATE" & _
                " FROM SALESMAN " & _
                " WHERE SALESMAN_NAME LIKE '%" & txtSalesmanName.Text.ToUpper & "%'" & _
                " AND EFFECTIVE_END_DATE > GETDATE()" & _
                " ORDER BY SALESMAN_NAME"
                .CommandType = CommandType.Text
            End With
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            Me.xAdoAdapter.Fill(xDS_Salesman, "SALESMAN")
            Me.dgvSALESMAN.DataSource = xDS_Salesman.Tables("SALESMAN")

        Else
            Dim xDS_Salesman As New DataSet
            With xComm
                .Connection = xConn
                .CommandText = "SELECT SALESMAN_ID ," & _
                " SALESMAN_CODE ," & _
                " SALESMAN_NAME ," & _
                " SALESMAN_ADDRESS ," & _
                " SALESMAN_ADDRESS_CITY ," & _
                " SALESMAN_ADDRESS_STATE ," & _
                " SALESMAN_PHONE1 ," & _
                " SALESMAN_PHONE2 ," & _
                " SALESMAN_PHONE3 ," & _
                " EFFECTIVE_START_DATE," & _
                " EFFECTIVE_END_DATE" & _
                " FROM SALESMAN " & _
                " WHERE SALESMAN_NAME LIKE '%" & txtSalesmanName.Text.ToUpper & "%'" & _
                " ORDER BY SALESMAN_NAME"
                .CommandType = CommandType.Text
            End With
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            Me.xAdoAdapter.Fill(xDS_Salesman, "SALESMAN")
            Me.dgvSALESMAN.DataSource = xDS_Salesman.Tables("SALESMAN")

        End If

        If Language = "Indonesian" Then
            Label2.Text = "Jumlah Data : " & dgvSALESMAN.RowCount
        Else
            Label2.Text = "No Of Salesman : " & dgvSALESMAN.RowCount
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            Dim xDS_Salesman As New DataSet
            With xComm
                .Connection = xConn
                .CommandText = "SELECT SALESMAN_ID ," & _
                " SALESMAN_CODE ," & _
                " SALESMAN_NAME ," & _
                " SALESMAN_ADDRESS ," & _
                " SALESMAN_ADDRESS_CITY ," & _
                " SALESMAN_ADDRESS_STATE ," & _
                " SALESMAN_PHONE1 ," & _
                " SALESMAN_PHONE2 ," & _
                " SALESMAN_PHONE3 ," & _
                " EFFECTIVE_START_DATE," & _
                " EFFECTIVE_END_DATE" & _
                " FROM SALESMAN " & _
                " WHERE SALESMAN_NAME LIKE '%" & txtSalesmanName.Text.ToUpper & "%'" & _
                " AND EFFECTIVE_END_DATE > GETDATE()" & _
                " ORDER BY SALESMAN_NAME"
                .CommandType = CommandType.Text
            End With
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            Me.xAdoAdapter.Fill(xDS_Salesman, "SALESMAN")
            Me.dgvSALESMAN.DataSource = xDS_Salesman.Tables("SALESMAN")

        Else
            Dim xDS_Salesman As New DataSet
            With xComm
                .Connection = xConn
                .CommandText = "SELECT SALESMAN_ID ," & _
                " SALESMAN_CODE ," & _
                " SALESMAN_NAME ," & _
                " SALESMAN_ADDRESS ," & _
                " SALESMAN_ADDRESS_CITY ," & _
                " SALESMAN_ADDRESS_STATE ," & _
                " SALESMAN_PHONE1 ," & _
                " SALESMAN_PHONE2 ," & _
                " SALESMAN_PHONE3 ," & _
                " EFFECTIVE_START_DATE," & _
                " EFFECTIVE_END_DATE" & _
                " FROM SALESMAN " & _
                " WHERE SALESMAN_NAME LIKE '%" & txtSalesmanName.Text.ToUpper & "%'" & _
                " ORDER BY SALESMAN_NAME"
                .CommandType = CommandType.Text
            End With
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            Me.xAdoAdapter.Fill(xDS_Salesman, "SALESMAN")
            Me.dgvSALESMAN.DataSource = xDS_Salesman.Tables("SALESMAN")

        End If
        If Language = "Indonesian" Then
            Label2.Text = "Jumlah Data : " & dgvSALESMAN.RowCount
        Else
            Label2.Text = "No Of Salesman : " & dgvSALESMAN.RowCount
        End If

    End Sub

    Private Sub dgvSALESMAN_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvSALESMAN.CellDoubleClick
        Try
            HideAllForms()
            Dim tmpSalesmanId As Integer = dgvSALESMAN.Item("SALESMAN_ID", e.RowIndex).Value
            frmEntrySalesman.MdiParent = ObjFormMain
            frmEntrySalesman.Show()
            tmpRecent = New frmEntrySalesman
            frmEntrySalesman.SALESMANBindingSource.Position = frmEntrySalesman.SALESMANBindingSource.Find("SALESMAN_ID", tmpSalesmanId)
            Me.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgvSALESMAN_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvSALESMAN.CellContentClick

    End Sub
End Class