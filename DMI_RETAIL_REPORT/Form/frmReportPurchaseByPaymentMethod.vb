﻿Imports System.Data.SqlClient
Imports System.Data
Public Class frmReportPurchaseByPaymentMethod

    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
   

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        'Me.SP_PURCHASE_REPORT_BY_PAYMENT_METHODTableAdapter.Fill(Me.DS_PURCHASE.SP_PURCHASE_REPORT_BY_PAYMENT_METHOD, dtpPeriod1.Value, dtpPeriod2.Value)
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC	    @return_value = [dbo].[SP_PURCHASE_REPORT_BY_PAYMENT_METHOD]" & vbCrLf & _
              "         @PERIOD1 = N'" & dtpPeriod1.Value & "'," & vbCrLf & _
              "    		@PERIOD2 = N'" & dtpPeriod2.Value & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.dgvRptPuchaseByPM.DataSource = dt
        If Language = "Indonesian" Then
            dgvRptPuchaseByPM.Columns("PAYMENT_METHOD").HeaderText = "Cara Pembayaran"
            dgvRptPuchaseByPM.Columns("NO_OF_ITEM").HeaderText = "Jumlah Item"
            dgvRptPuchaseByPM.Columns("NO_OF_INVOICE").HeaderText = "Jumlah Faktur"
            dgvRptPuchaseByPM.Columns("TOTAL_AMOUNT").HeaderText = "Nominal Jumlah"
            dgvRptPuchaseByPM.Columns("DATE_FROM").Visible = False
            dgvRptPuchaseByPM.Columns("DATE_TO").Visible = False
            dgvRptPuchaseByPM.Columns("NO_OF_ITEM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptPuchaseByPM.Columns("NO_OF_INVOICE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptPuchaseByPM.Columns("TOTAL_AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptPuchaseByPM.Columns("TOTAL_AMOUNT").DefaultCellStyle.Format = "n0"
            dgvRptPuchaseByPM.Columns("PAYMENT_METHOD").Width = 150
        Else
            dgvRptPuchaseByPM.Columns("PAYMENT_METHOD").HeaderText = "Payment Method"
            dgvRptPuchaseByPM.Columns("NO_OF_ITEM").HeaderText = "No Of Item"
            dgvRptPuchaseByPM.Columns("NO_OF_INVOICE").HeaderText = "No Of Invoidce"
            dgvRptPuchaseByPM.Columns("TOTAL_AMOUNT").HeaderText = "Total Amount"
            dgvRptPuchaseByPM.Columns("DATE_FROM").Visible = False
            dgvRptPuchaseByPM.Columns("DATE_TO").Visible = False
            dgvRptPuchaseByPM.Columns("NO_OF_ITEM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptPuchaseByPM.Columns("NO_OF_INVOICE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptPuchaseByPM.Columns("TOTAL_AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptPuchaseByPM.Columns("TOTAL_AMOUNT").DefaultCellStyle.Format = "n0"
            dgvRptPuchaseByPM.Columns("PAYMENT_METHOD").Width = 150
        End If
    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        Dim objf As New frmRepPurchaseByPaymentMethod
        objf.date1 = dtpPeriod1.Value
        objf.date2 = dtpPeriod2.Value
        objf.ReportViewer1.ShowRefreshButton = False
        objf.ReportViewer1.ZoomPercent = 100
        objf.WindowState = FormWindowState.Maximized
        objf.Show()
    End Sub

    Private Sub frmReportPurchaseByPaymentMethod_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dtpPeriod1.Value = DateSerial(Today.Year, Today.Month, 1)
        dtpPeriod2.Value = DateSerial(Today.Year, Today.Month + 1, 0)

        If Language = "Indonesian" Then
            cmdReport.Text = "Cetak"
            cmdGenerate.Text = "Proses"
            lblFrom.Text = "Periode"

            'dgvRptPuchaseByPM.Columns("PAYMENT_METHOD").HeaderText = "Cara Pembayaran"
            'dgvRptPuchaseByPM.Columns("NO_OF_ITEM").HeaderText = "Jumlah Item"
            'dgvRptPuchaseByPM.Columns("NO_OF_INVOICE").HeaderText = "Jumlah Faktur"
            'dgvRptPuchaseByPM.Columns("TOTAL_AMOUNT").HeaderText = "Nominal Jumlah"

        End If
    End Sub
End Class