﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListSalesmanInvoice
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListSalesmanInvoice))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblSalesman = New System.Windows.Forms.Label()
        Me.cmdSearchName = New System.Windows.Forms.PictureBox()
        Me.cmbSalesmanCode = New System.Windows.Forms.ComboBox()
        Me.SALESMANBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_SALE = New DMI_RETAIL_REPORT.DS_SALE()
        Me.txtSalesmanName = New System.Windows.Forms.TextBox()
        Me.cmdReport = New System.Windows.Forms.Button()
        Me.lblTo = New System.Windows.Forms.Label()
        Me.lblFrom = New System.Windows.Forms.Label()
        Me.cmdGenerate = New System.Windows.Forms.Button()
        Me.dtpPeriod2 = New System.Windows.Forms.DateTimePicker()
        Me.dtpPeriod1 = New System.Windows.Forms.DateTimePicker()
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.SP_LIST_SALESMAN_INVOICEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.dgvSalesmanInvoice = New System.Windows.Forms.DataGridView()
        Me.SALESMAN_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALE_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALE_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RECEIPT_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CUSTOMER_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PAYMENT_METHOD_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MATURITY_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PAYMENT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AMOUNT_DISCOUNT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DISCOUNT_2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DISCOUNT_3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TAX_AMOUNT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GRAND_TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.REMARK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WAREHOUSE_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SP_LIST_SALESMAN_INVOICETableAdapter = New DMI_RETAIL_REPORT.DS_SALETableAdapters.SP_LIST_SALESMAN_INVOICETableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_REPORT.DS_SALETableAdapters.TableAdapterManager()
        Me.SALESMANTableAdapter = New DMI_RETAIL_REPORT.DS_SALETableAdapters.SALESMANTableAdapter()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox2.SuspendLayout()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SALESMANBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_SALE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_LIST_SALESMAN_INVOICEBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator.SuspendLayout()
        CType(Me.SP_LIST_SALESMAN_INVOICEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSalesmanInvoice, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblSalesman)
        Me.GroupBox2.Controls.Add(Me.cmdSearchName)
        Me.GroupBox2.Controls.Add(Me.cmbSalesmanCode)
        Me.GroupBox2.Controls.Add(Me.txtSalesmanName)
        Me.GroupBox2.Controls.Add(Me.cmdReport)
        Me.GroupBox2.Controls.Add(Me.lblTo)
        Me.GroupBox2.Controls.Add(Me.lblFrom)
        Me.GroupBox2.Controls.Add(Me.cmdGenerate)
        Me.GroupBox2.Controls.Add(Me.dtpPeriod2)
        Me.GroupBox2.Controls.Add(Me.dtpPeriod1)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 28)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(826, 152)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        '
        'lblSalesman
        '
        Me.lblSalesman.Location = New System.Drawing.Point(117, 24)
        Me.lblSalesman.Name = "lblSalesman"
        Me.lblSalesman.Size = New System.Drawing.Size(95, 19)
        Me.lblSalesman.TabIndex = 24
        Me.lblSalesman.Text = "Salesman"
        Me.lblSalesman.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cmdSearchName
        '
        Me.cmdSearchName.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchName.Image = CType(resources.GetObject("cmdSearchName.Image"), System.Drawing.Image)
        Me.cmdSearchName.Location = New System.Drawing.Point(632, 20)
        Me.cmdSearchName.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchName.Name = "cmdSearchName"
        Me.cmdSearchName.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearchName.TabIndex = 23
        Me.cmdSearchName.TabStop = False
        '
        'cmbSalesmanCode
        '
        Me.cmbSalesmanCode.DataSource = Me.SALESMANBindingSource
        Me.cmbSalesmanCode.DisplayMember = "SALESMAN_CODE"
        Me.cmbSalesmanCode.FormattingEnabled = True
        Me.cmbSalesmanCode.Location = New System.Drawing.Point(218, 21)
        Me.cmbSalesmanCode.Name = "cmbSalesmanCode"
        Me.cmbSalesmanCode.Size = New System.Drawing.Size(121, 23)
        Me.cmbSalesmanCode.TabIndex = 21
        Me.cmbSalesmanCode.ValueMember = "SALESMAN_ID"
        '
        'SALESMANBindingSource
        '
        Me.SALESMANBindingSource.DataMember = "SALESMAN"
        Me.SALESMANBindingSource.DataSource = Me.DS_SALE
        '
        'DS_SALE
        '
        Me.DS_SALE.DataSetName = "DS_SALE"
        Me.DS_SALE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'txtSalesmanName
        '
        Me.txtSalesmanName.Location = New System.Drawing.Point(345, 21)
        Me.txtSalesmanName.Name = "txtSalesmanName"
        Me.txtSalesmanName.ReadOnly = True
        Me.txtSalesmanName.Size = New System.Drawing.Size(280, 22)
        Me.txtSalesmanName.TabIndex = 22
        '
        'cmdReport
        '
        Me.cmdReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdReport.Image = Global.DMI_RETAIL_REPORT.My.Resources.Resources.Printer_small
        Me.cmdReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdReport.Location = New System.Drawing.Point(730, 122)
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(90, 24)
        Me.cmdReport.TabIndex = 15
        Me.cmdReport.Text = "Print [F12]"
        Me.cmdReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.cmdReport.UseVisualStyleBackColor = True
        '
        'lblTo
        '
        Me.lblTo.AutoSize = True
        Me.lblTo.Location = New System.Drawing.Point(446, 63)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(23, 15)
        Me.lblTo.TabIndex = 8
        Me.lblTo.Text = "To"
        '
        'lblFrom
        '
        Me.lblFrom.AutoSize = True
        Me.lblFrom.Location = New System.Drawing.Point(216, 63)
        Me.lblFrom.Name = "lblFrom"
        Me.lblFrom.Size = New System.Drawing.Size(79, 15)
        Me.lblFrom.TabIndex = 7
        Me.lblFrom.Text = "From Period"
        '
        'cmdGenerate
        '
        Me.cmdGenerate.Image = CType(resources.GetObject("cmdGenerate.Image"), System.Drawing.Image)
        Me.cmdGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdGenerate.Location = New System.Drawing.Point(374, 97)
        Me.cmdGenerate.Name = "cmdGenerate"
        Me.cmdGenerate.Size = New System.Drawing.Size(99, 39)
        Me.cmdGenerate.TabIndex = 5
        Me.cmdGenerate.Text = "Generate"
        Me.cmdGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdGenerate.UseVisualStyleBackColor = True
        '
        'dtpPeriod2
        '
        Me.dtpPeriod2.CustomFormat = "dd-MMM-yyyy"
        Me.dtpPeriod2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPeriod2.Location = New System.Drawing.Point(475, 60)
        Me.dtpPeriod2.Name = "dtpPeriod2"
        Me.dtpPeriod2.Size = New System.Drawing.Size(120, 22)
        Me.dtpPeriod2.TabIndex = 4
        '
        'dtpPeriod1
        '
        Me.dtpPeriod1.CustomFormat = "dd-MMM-yyyy"
        Me.dtpPeriod1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPeriod1.Location = New System.Drawing.Point(301, 60)
        Me.dtpPeriod1.Name = "dtpPeriod1"
        Me.dtpPeriod1.Size = New System.Drawing.Size(123, 22)
        Me.dtpPeriod1.TabIndex = 3
        '
        'SP_LIST_SALESMAN_INVOICEBindingNavigator
        '
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator.AddNewItem = Nothing
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator.BindingSource = Me.SP_LIST_SALESMAN_INVOICEBindingSource
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator.DeleteItem = Nothing
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator.Name = "SP_LIST_SALESMAN_INVOICEBindingNavigator"
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator.Size = New System.Drawing.Size(850, 25)
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator.TabIndex = 12
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator.Text = "BindingNavigator1"
        '
        'SP_LIST_SALESMAN_INVOICEBindingSource
        '
        Me.SP_LIST_SALESMAN_INVOICEBindingSource.DataMember = "SP_LIST_SALESMAN_INVOICE"
        Me.SP_LIST_SALESMAN_INVOICEBindingSource.DataSource = Me.DS_SALE
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'dgvSalesmanInvoice
        '
        Me.dgvSalesmanInvoice.AllowUserToAddRows = False
        Me.dgvSalesmanInvoice.AllowUserToDeleteRows = False
        Me.dgvSalesmanInvoice.AutoGenerateColumns = False
        Me.dgvSalesmanInvoice.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSalesmanInvoice.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSalesmanInvoice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSalesmanInvoice.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SALESMAN_NAME, Me.SALE_ID, Me.SALE_DATE, Me.RECEIPT_NO, Me.CUSTOMER_NAME, Me.PAYMENT_METHOD_NAME, Me.MATURITY_DATE, Me.PAYMENT, Me.AMOUNT_DISCOUNT, Me.DISCOUNT_2, Me.DISCOUNT_3, Me.TAX_AMOUNT, Me.GRAND_TOTAL, Me.REMARK, Me.WAREHOUSE_NAME})
        Me.dgvSalesmanInvoice.DataSource = Me.SP_LIST_SALESMAN_INVOICEBindingSource
        Me.dgvSalesmanInvoice.Location = New System.Drawing.Point(12, 186)
        Me.dgvSalesmanInvoice.Name = "dgvSalesmanInvoice"
        Me.dgvSalesmanInvoice.ReadOnly = True
        Me.dgvSalesmanInvoice.RowHeadersWidth = 24
        Me.dgvSalesmanInvoice.Size = New System.Drawing.Size(826, 228)
        Me.dgvSalesmanInvoice.TabIndex = 13
        '
        'SALESMAN_NAME
        '
        Me.SALESMAN_NAME.DataPropertyName = "SALESMAN_NAME"
        Me.SALESMAN_NAME.HeaderText = "Salesman"
        Me.SALESMAN_NAME.Name = "SALESMAN_NAME"
        Me.SALESMAN_NAME.ReadOnly = True
        '
        'SALE_ID
        '
        Me.SALE_ID.DataPropertyName = "SALE_ID"
        Me.SALE_ID.HeaderText = "SALE_ID"
        Me.SALE_ID.Name = "SALE_ID"
        Me.SALE_ID.ReadOnly = True
        Me.SALE_ID.Visible = False
        '
        'SALE_DATE
        '
        Me.SALE_DATE.DataPropertyName = "SALE_DATE"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "dd-MMM-yyyy"
        Me.SALE_DATE.DefaultCellStyle = DataGridViewCellStyle2
        Me.SALE_DATE.HeaderText = "Sale Date"
        Me.SALE_DATE.Name = "SALE_DATE"
        Me.SALE_DATE.ReadOnly = True
        Me.SALE_DATE.Width = 70
        '
        'RECEIPT_NO
        '
        Me.RECEIPT_NO.DataPropertyName = "RECEIPT_NO"
        Me.RECEIPT_NO.HeaderText = "Receipt No"
        Me.RECEIPT_NO.Name = "RECEIPT_NO"
        Me.RECEIPT_NO.ReadOnly = True
        Me.RECEIPT_NO.Width = 65
        '
        'CUSTOMER_NAME
        '
        Me.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME"
        Me.CUSTOMER_NAME.HeaderText = "Customer Name"
        Me.CUSTOMER_NAME.Name = "CUSTOMER_NAME"
        Me.CUSTOMER_NAME.ReadOnly = True
        Me.CUSTOMER_NAME.Width = 90
        '
        'PAYMENT_METHOD_NAME
        '
        Me.PAYMENT_METHOD_NAME.DataPropertyName = "PAYMENT_METHOD_NAME"
        Me.PAYMENT_METHOD_NAME.HeaderText = "Payment Method"
        Me.PAYMENT_METHOD_NAME.Name = "PAYMENT_METHOD_NAME"
        Me.PAYMENT_METHOD_NAME.ReadOnly = True
        Me.PAYMENT_METHOD_NAME.Width = 70
        '
        'MATURITY_DATE
        '
        Me.MATURITY_DATE.DataPropertyName = "MATURITY_DATE"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "dd-MMM-yyyy"
        Me.MATURITY_DATE.DefaultCellStyle = DataGridViewCellStyle3
        Me.MATURITY_DATE.HeaderText = "Maturity Date"
        Me.MATURITY_DATE.Name = "MATURITY_DATE"
        Me.MATURITY_DATE.ReadOnly = True
        Me.MATURITY_DATE.Width = 70
        '
        'PAYMENT
        '
        Me.PAYMENT.DataPropertyName = "PAYMENT"
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.PAYMENT.DefaultCellStyle = DataGridViewCellStyle4
        Me.PAYMENT.HeaderText = "Payment"
        Me.PAYMENT.Name = "PAYMENT"
        Me.PAYMENT.ReadOnly = True
        Me.PAYMENT.Width = 70
        '
        'AMOUNT_DISCOUNT
        '
        Me.AMOUNT_DISCOUNT.DataPropertyName = "AMOUNT_DISCOUNT"
        Me.AMOUNT_DISCOUNT.HeaderText = "AMOUNT_DISCOUNT"
        Me.AMOUNT_DISCOUNT.Name = "AMOUNT_DISCOUNT"
        Me.AMOUNT_DISCOUNT.ReadOnly = True
        Me.AMOUNT_DISCOUNT.Visible = False
        '
        'DISCOUNT_2
        '
        Me.DISCOUNT_2.DataPropertyName = "DISCOUNT_2"
        Me.DISCOUNT_2.HeaderText = "DISCOUNT_2"
        Me.DISCOUNT_2.Name = "DISCOUNT_2"
        Me.DISCOUNT_2.ReadOnly = True
        Me.DISCOUNT_2.Visible = False
        '
        'DISCOUNT_3
        '
        Me.DISCOUNT_3.DataPropertyName = "DISCOUNT_3"
        Me.DISCOUNT_3.HeaderText = "DISCOUNT_3"
        Me.DISCOUNT_3.Name = "DISCOUNT_3"
        Me.DISCOUNT_3.ReadOnly = True
        Me.DISCOUNT_3.Visible = False
        '
        'TAX_AMOUNT
        '
        Me.TAX_AMOUNT.DataPropertyName = "TAX_AMOUNT"
        Me.TAX_AMOUNT.HeaderText = "TAX_AMOUNT"
        Me.TAX_AMOUNT.Name = "TAX_AMOUNT"
        Me.TAX_AMOUNT.ReadOnly = True
        Me.TAX_AMOUNT.Visible = False
        '
        'GRAND_TOTAL
        '
        Me.GRAND_TOTAL.DataPropertyName = "GRAND_TOTAL"
        DataGridViewCellStyle5.Format = "N0"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.GRAND_TOTAL.DefaultCellStyle = DataGridViewCellStyle5
        Me.GRAND_TOTAL.HeaderText = "Grand Total"
        Me.GRAND_TOTAL.Name = "GRAND_TOTAL"
        Me.GRAND_TOTAL.ReadOnly = True
        Me.GRAND_TOTAL.Width = 75
        '
        'REMARK
        '
        Me.REMARK.DataPropertyName = "REMARK"
        Me.REMARK.HeaderText = "Remark"
        Me.REMARK.Name = "REMARK"
        Me.REMARK.ReadOnly = True
        Me.REMARK.Width = 90
        '
        'WAREHOUSE_NAME
        '
        Me.WAREHOUSE_NAME.DataPropertyName = "WAREHOUSE_NAME"
        Me.WAREHOUSE_NAME.HeaderText = "Warehouse"
        Me.WAREHOUSE_NAME.Name = "WAREHOUSE_NAME"
        Me.WAREHOUSE_NAME.ReadOnly = True
        Me.WAREHOUSE_NAME.Width = 80
        '
        'SP_LIST_SALESMAN_INVOICETableAdapter
        '
        Me.SP_LIST_SALESMAN_INVOICETableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.CUSTOMERTableAdapter = Nothing
        Me.TableAdapterManager.PAYMENT_METHODTableAdapter = Nothing
        Me.TableAdapterManager.PRODUCTTableAdapter = Nothing
        Me.TableAdapterManager.RECEIVABLETableAdapter = Nothing
        Me.TableAdapterManager.SALE_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.SALESMANTableAdapter = Nothing
        Me.TableAdapterManager.SALETableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_REPORT.DS_SALETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.WAREHOUSETableAdapter = Nothing
        '
        'SALESMANTableAdapter
        '
        Me.SALESMANTableAdapter.ClearBeforeFill = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(42, 422)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 15)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Label1"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(594, 422)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 15)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Label2"
        '
        'frmListSalesmanInvoice
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(850, 448)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvSalesmanInvoice)
        Me.Controls.Add(Me.SP_LIST_SALESMAN_INVOICEBindingNavigator)
        Me.Controls.Add(Me.GroupBox2)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmListSalesmanInvoice"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "List Selling by Salesman"
        Me.Text = "List Selling by Salesman"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SALESMANBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_SALE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_LIST_SALESMAN_INVOICEBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator.ResumeLayout(False)
        Me.SP_LIST_SALESMAN_INVOICEBindingNavigator.PerformLayout()
        CType(Me.SP_LIST_SALESMAN_INVOICEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSalesmanInvoice, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdReport As System.Windows.Forms.Button
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents lblFrom As System.Windows.Forms.Label
    Friend WithEvents cmdGenerate As System.Windows.Forms.Button
    Friend WithEvents dtpPeriod2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpPeriod1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DS_SALE As DMI_RETAIL_REPORT.DS_SALE
    Friend WithEvents SP_LIST_SALESMAN_INVOICEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_LIST_SALESMAN_INVOICETableAdapter As DMI_RETAIL_REPORT.DS_SALETableAdapters.SP_LIST_SALESMAN_INVOICETableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_REPORT.DS_SALETableAdapters.TableAdapterManager
    Friend WithEvents SP_LIST_SALESMAN_INVOICEBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents dgvSalesmanInvoice As System.Windows.Forms.DataGridView
    Friend WithEvents lblSalesman As System.Windows.Forms.Label
    Friend WithEvents cmdSearchName As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSalesmanCode As System.Windows.Forms.ComboBox
    Friend WithEvents txtSalesmanName As System.Windows.Forms.TextBox
    Friend WithEvents SALESMANBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SALESMANTableAdapter As DMI_RETAIL_REPORT.DS_SALETableAdapters.SALESMANTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents SALESMAN_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALE_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALE_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RECEIPT_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CUSTOMER_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PAYMENT_METHOD_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MATURITY_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PAYMENT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AMOUNT_DISCOUNT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DISCOUNT_2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DISCOUNT_3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TAX_AMOUNT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GRAND_TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents REMARK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WAREHOUSE_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
