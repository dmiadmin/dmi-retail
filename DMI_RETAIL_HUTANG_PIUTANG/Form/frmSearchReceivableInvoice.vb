﻿Public Class frmSearchReceivableInvoice

    Private Sub frmSearchReceivableInvoice_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_PAYMENT_TRANSACTION.SP_RECEIVABLE_INVOICE' table. You can move, or remove it, as needed.
        Me.SP_RECEIVABLE_INVOICETableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_RECEIVABLE_INVOICE)

        txtCustomerName.Text = ""
        txtInvoiceNo.Text = ""

        tmpSearchResult = ""

        If mdlGeneral.tmpSearchMode = "Customer - Customer Name" Then
            txtCustomerName.Focus()
        ElseIf mdlGeneral.tmpSearchMode = "Customer - Receipt No" Then
            txtInvoiceNo.Focus()
        End If

        If Language = "Indonesian" Then
            Me.Text = "Daftar Faktur Piutang"

            dgvRECEIVABLE_INVOICE.Columns("CUSTOMER_NAME").HeaderText = "Nama Pelanggan"
            dgvRECEIVABLE_INVOICE.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            dgvRECEIVABLE_INVOICE.Columns("TOTAL_RECEIVABLE").HeaderText = "Jumlah Piutang"
            dgvRECEIVABLE_INVOICE.Columns("MATURITY_DATE").HeaderText = "Tgl Jatuh Tempo"

            lblCustomer.Text = "Nama Pelanggan"
            lblInvoice.Text = "No Faktur"

            lblNoOfInvoice.Text = "Jumlah Faktur : " & dgvRECEIVABLE_INVOICE.RowCount
        Else
            lblNoOfInvoice.Text = "No of Invoice(s) : " & dgvRECEIVABLE_INVOICE.RowCount
        End If

    End Sub

    Private Sub dgvRECEIVABLE_INVOICE_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvRECEIVABLE_INVOICE.DoubleClick
        Try
            tmpSearchResult = SP_RECEIVABLE_INVOICEBindingSource.Current("RECEIPT_ID")
        Catch
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data Faktur Piutang !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Receivable Invoice !", MsgBoxStyle.Critical, "DMI Retail")
            End If
        End Try
        Me.Close()
    End Sub

    Private Sub dgvRECEIVABLE_INVOICE_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvRECEIVABLE_INVOICE.KeyDown
        If e.KeyCode = Keys.Enter Then
            Try
                tmpSearchResult = SP_RECEIVABLE_INVOICEBindingSource.Current("RECEIPT_ID")
            Catch
                If Language = "Indonesian" Then
                    MsgBox("Tidak ada data Faktur Piutang !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("There is no Receivable Invoice !", MsgBoxStyle.Critical, "DMI Retail")
                End If
            End Try

            Me.Close()

        ElseIf e.KeyCode = Keys.Escape Then

            tmpSearchResult = ""

            Me.Close()
        End If
    End Sub

    Private Sub frmSearchReceivableInvoice_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
        txtCustomerName.Focus()
    End Sub

    Private Sub txtCustomerName_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCustomerName.KeyUp
        If e.KeyCode = Keys.Enter Then
            dgvRECEIVABLE_INVOICE.Focus()
            Exit Sub
        ElseIf e.KeyCode = Keys.Escape Then
            tmpSearchResult = ""
            Me.Close()
        End If
    End Sub

    Private Sub txtCustomerName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCustomerName.TextChanged
        Try
            txtInvoiceNo.Text = ""
            SP_RECEIVABLE_INVOICEBindingSource.Filter = "CUSTOMER_NAME LIKE '%" & txtCustomerName.Text.ToUpper & "%'"
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtCustomerName.Text = ""
            txtCustomerName.Focus()
        End Try
    End Sub

    Private Sub txtInvoiceNo_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtInvoiceNo.KeyUp
        If e.KeyCode = Keys.Enter Then
            dgvRECEIVABLE_INVOICE.Focus()
            Exit Sub
        ElseIf e.KeyCode = Keys.Escape Then
            tmpSearchResult = ""
            Me.Close()
        End If
    End Sub

    Private Sub txtInvoiceNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInvoiceNo.TextChanged
        Try
            txtCustomerName.Text = ""
            SP_RECEIVABLE_INVOICEBindingSource.Filter = "RECEIPT_NO LIKE '%" & txtInvoiceNo.Text.ToUpper & "%'"
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtInvoiceNo.Text = ""
            txtInvoiceNo.Focus()
        End Try
    End Sub
End Class