﻿Public Class frmReportPurchaseGeneral
    Dim tmpdate1, tmpdate2 As DateTime
    Public tmpDatePassing, tmpPeriod1, tmpPeriod2 As DateTime
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String,
                        ByVal ptmpType As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.tmpType = ptmpType
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    Private Sub frmReportPurchaseGeneral_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load



        If tmpType = "M" Then
            dtpPeriod.Value = DateSerial(Today.Year, Today.Month, 1)
            dtpPeriod2.Value = DateSerial(Today.Year, Today.Month + 1, 0)
            dtpPeriod.Format = DateTimePickerFormat.Custom
            dtpPeriod.CustomFormat = "dd - MMM - yyyy"
            dtpPeriod2.Format = DateTimePickerFormat.Custom
            dtpPeriod2.CustomFormat = "dd - MMM - yyyy"
        ElseIf tmpType = "Y" Then
            dtpPeriod.Format = DateTimePickerFormat.Custom
            dtpPeriod.CustomFormat = "MMMM- yyyy"
            dtpPeriod2.Format = DateTimePickerFormat.Custom
            dtpPeriod2.CustomFormat = "MMMM- yyyy"
        End If

        Dim tmpTotal As Integer = 0
        Dim tmpInvoice As Integer = 0
        Dim tmpItem As Integer = 0

        For n As Integer = 0 To dgvRptPurchaseGeneral.RowCount - 1
            tmpTotal = tmpTotal + dgvRptPurchaseGeneral.Item("GRAND_TOTAL", n).Value
            tmpTotal = tmpTotal
            tmpInvoice = tmpInvoice + dgvRptPurchaseGeneral.Item("INVOICE_TOTAL", n).Value
            tmpInvoice = tmpInvoice
            tmpItem = tmpItem + dgvRptPurchaseGeneral.Item("NO_OF_ITEM", n).Value
            tmpItem = tmpItem
        Next

        If Language = "Indonesian" Then
            Me.Text = "Laporan Pembelian"
            lblPeriod.Text = "Periode"
            cmdGenerate.Text = "Proses"
            cmdReport.Text = "Cetak"

            dgvRptPurchaseGeneral.Columns("PURCHASE_DATE").HeaderText = "Tgl. Pembelian"
            dgvRptPurchaseGeneral.Columns("NO_OF_ITEM").HeaderText = "Jumlah Item"
            dgvRptPurchaseGeneral.Columns("INVOICE_TOTAL").HeaderText = "Jumlah Faktur"

            lblTotal.Text = "Total Jumlah : " & FormatNumber(tmpTotal, 0)
            lblNoOfTrans.Text = "Jumlah Transaksi : " & dgvRptPurchaseGeneral.RowCount
            lblInvoice.Text = "Jumlah Faktur : " & tmpInvoice
            lblItem.Text = "Jumlah Item : " & tmpItem
        Else
            lblTotal.Text = "Total amount : " & FormatNumber(tmpTotal, 0)
            lblNoOfTrans.Text = "No of transaction(s) : " & dgvRptPurchaseGeneral.RowCount
            lblInvoice.Text = "Total Invoice : " & tmpInvoice
            lblItem.Text = "Total Item : " & tmpItem
        End If
    End Sub


    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        If tmpType = "" Then
            Exit Sub
        End If

        tmpdate1 = DateSerial(Year(dtpPeriod.Value), _
                            Month(dtpPeriod.Value), 1)
        tmpdate2 = DateSerial(Year(dtpPeriod2.Value), _
                              Month(dtpPeriod2.Value) + 1, 0)

        Cursor = Cursors.WaitCursor

        If tmpType = "M" Then
            Me.SP_PURCHASE_REPORT_DAILYTableAdapter.Fill(Me.DS_PURCHASE.SP_PURCHASE_REPORT_DAILY, dtpPeriod.Value, dtpPeriod2.Value)
            dgvRptPurchaseGeneral.DataSource = SP_PURCHASE_REPORT_DAILYBindingSource
        ElseIf tmpType = "Y" Then
            Me.SP_PURCHASE_REPORT_MONTHLYTableAdapter.Fill(Me.DS_PURCHASE.SP_PURCHASE_REPORT_MONTHLY, tmpdate1, tmpdate2)
            dgvRptPurchaseGeneral.DataSource = SP_PURCHASE_REPORT_MONTHLYBindingSource
        End If

        Cursor = Cursors.Default

        Dim tmpTotal As Integer = 0
        Dim tmpInvoice As Integer = 0
        Dim tmpItem As Integer = 0

        For n As Integer = 0 To dgvRptPurchaseGeneral.RowCount - 1
            tmpTotal = tmpTotal + dgvRptPurchaseGeneral.Item("GRAND_TOTAL", n).Value
            tmpTotal = tmpTotal
            tmpInvoice = tmpInvoice + dgvRptPurchaseGeneral.Item("INVOICE_TOTAL", n).Value
            tmpInvoice = tmpInvoice
            tmpItem = tmpItem + dgvRptPurchaseGeneral.Item("NO_OF_ITEM", n).Value
            tmpItem = tmpItem
        Next

        lblTotal.Text = "Total amount : " & FormatNumber(tmpTotal, 0)
        lblNoOfTrans.Text = "No of transaction(s) : " & dgvRptPurchaseGeneral.RowCount
        lblInvoice.Text = "Total Invoice : " & tmpInvoice
        lblItem.Text = "Total Item : " & tmpItem

    End Sub

    Private Sub dgvRptPurchaseGeneral_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvRptPurchaseGeneral.DoubleClick
        Try
            If tmpType = "M" Then
                tmpDatePassing = SP_PURCHASE_REPORT_DAILYBindingSource.Current("PURCHASE_DATE")
            ElseIf tmpType = "Y" Then
                tmpDatePassing = SP_PURCHASE_REPORT_MONTHLYBindingSource.Current("PURCHASE_DATE")
            End If
            frmReportPurchaseDetail.tmpDatePassing = tmpDatePassing
            frmReportPurchaseDetail.ShowDialog()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        If tmpType = "M" Then
            Dim RPV As New frmRepPurchaseGeneral
            RPV.date1 = dtpPeriod.Value
            RPV.date2 = dtpPeriod2.Value
            RPV.ReportViewer1.ZoomPercent = 100
            RPV.WindowState = FormWindowState.Maximized
            RPV.Show()
        ElseIf tmpType = "Y" Then
            tmpPeriod1 = DateSerial(dtpPeriod.Value.Year, dtpPeriod.Value.Month, 1)
            tmpPeriod2 = DateSerial(dtpPeriod2.Value.Year, dtpPeriod2.Value.Month + 1, 0)
            Dim RPV As New frmRepPurchaseMonthly
            RPV.tmpPeriod1 = tmpPeriod1
            RPV.tmpPeriod2 = tmpPeriod2
            RPV.ReportViewer1.ZoomPercent = 100
            RPV.WindowState = FormWindowState.Maximized
            RPV.Show()
        End If
    End Sub
End Class