﻿Public Class frmSearchPurchaseOrder
    Public tmpVendorId, tmpPurchaseOrderId, tmpTotItem As Integer
    Public tmpPurchaseOrderDate As DateTime
    Public tmpPurchaseOrderNo, tmpVendorName As String

    Private Sub frmSearchPurchaseOrder_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_PURCHASE_ORDER.SP_SELECT_PURCHASE_ORDER_HEADER' table. You can move, or remove it, as needed.
        Me.SP_SELECT_PURCHASE_ORDER_HEADERTableAdapter.Fill(Me.DS_PURCHASE_ORDER.SP_SELECT_PURCHASE_ORDER_HEADER)

    End Sub

    Private Sub txtPurchaseOrderNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPurchaseOrderNo.TextChanged
        Try
            SP_SELECT_PURCHASE_ORDER_HEADERBindingSource.Filter = "PURCHASE_ORDER_NUMBER LIKE '%" & txtPurchaseOrderNo.Text & _
                                                              "%' AND VENDOR LIKE '%" & txtVendor.Text & "%'"
        Catch ex As Exception
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtPurchaseOrderNo.Text = ""
            txtPurchaseOrderNo.Focus()
        End Try
    End Sub

    Private Sub txtVendor_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtVendor.TextChanged
        Try
            SP_SELECT_PURCHASE_ORDER_HEADERBindingSource.Filter = "PURCHASE_ORDER_NUMBER LIKE '%" & txtPurchaseOrderNo.Text & _
                                                               "%' AND VENDOR LIKE '%" & txtVendor.Text & "%'"
        Catch ex As Exception
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtVendor.Text = ""
            txtVendor.Focus()
        End Try
    End Sub

    Private Sub dgvSearchPO_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSearchPO.DoubleClick

        Try
            tmpPurchaseOrderId = SP_SELECT_PURCHASE_ORDER_HEADERBindingSource.Current("PURCHASE_ORDER_ID")
            tmpVendorId = SP_SELECT_PURCHASE_ORDER_HEADERBindingSource.Current("VENDOR_ID")
            tmpPurchaseOrderDate = SP_SELECT_PURCHASE_ORDER_HEADERBindingSource.Current("PURCHASE_ORDER_DATE")
            tmpPurchaseOrderNo = SP_SELECT_PURCHASE_ORDER_HEADERBindingSource.Current("PURCHASE_ORDER_NUMBER")
            tmpVendorName = SP_SELECT_PURCHASE_ORDER_HEADERBindingSource.Current("VENDOR")
            tmpTotItem = SP_SELECT_PURCHASE_ORDER_HEADERBindingSource.Current("NO_OF_ITEM")
            
            Me.Close()
        Catch ex As Exception

        End Try
        
    End Sub
End Class