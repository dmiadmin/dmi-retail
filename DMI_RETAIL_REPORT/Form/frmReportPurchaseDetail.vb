﻿Public Class frmReportPurchaseDetail

    Dim tmpQTy, tmpDisc, tmpTax, tmpTot As Integer
    Public tmpDatePassing As Date
    Private Sub frmReportPurchaseDetail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If tmpType = "M" Then
            dgvRptPurchaseDetail.Columns("PURCHASE_DATE").Visible = False
        Else
            dgvRptPurchaseDetail.Columns("PURCHASE_DATE").Visible = True
        End If

        Me.SP_PURCHASE_REPORT_DETAILTableAdapter.Fill(Me.DS_PURCHASE.SP_PURCHASE_REPORT_DETAIL, tmpType, tmpDatePassing)

        tmpQTy = 0
        tmpDisc = 0
        tmpTot = 0
        tmpTax = 0
        For x As Integer = 0 To dgvRptPurchaseDetail.RowCount - 1
            tmpQTy = tmpQTy + dgvRptPurchaseDetail.Item("QUANTITY", x).Value
            tmpQTy = tmpQTy
            tmpDisc = tmpDisc + dgvRptPurchaseDetail.Item("DISCOUNT", x).Value
            tmpDisc = tmpDisc
            tmpTax = tmpTax + dgvRptPurchaseDetail.Item("TAX", x).Value
            tmpTax = tmpTax
            tmpTot = tmpTot + (dgvRptPurchaseDetail.Item("QUANTITY", x).Value * dgvRptPurchaseDetail.Item("PRICE_PER_UNIT", x).Value)
            tmpTot = tmpTot
        Next

        If Language = "Indonesian" Then
            dgvRptPurchaseDetail.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            dgvRptPurchaseDetail.Columns("PAYMENT_METHOD_NAME").HeaderText = "Cara Pembayaran"
            dgvRptPurchaseDetail.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            dgvRptPurchaseDetail.Columns("VENDOR_NAME").HeaderText = "Supplier"
            dgvRptPurchaseDetail.Columns("PURCHASE_DATE").HeaderText = "Tgl. Pembelian"
            dgvRptPurchaseDetail.Columns("PRICE_PER_UNIT").HeaderText = "Harga Satuan"
            dgvRptPurchaseDetail.Columns("TAX").HeaderText = "Pajak"

            lblNoTrans.Text = "Jumlah Transaksi : " & dgvRptPurchaseDetail.RowCount
            lblQty.Text = "Jumlah Produk : " & tmpQTy
            lblDisc.Text = "Jumlah Diskon : " & FormatNumber(tmpDisc, 0)
            lblTot.Text = "Grand Total : " & FormatNumber(tmpTot, 0)
            lblTax.Text = "Jumlah Pajak : " & FormatNumber(tmpTax, 0)
        Else
            lblNoTrans.Text = "No Of Transaction : " & dgvRptPurchaseDetail.RowCount
            lblQty.Text = "Total Quantity : " & tmpQTy
            lblDisc.Text = "Total Discount : " & FormatNumber(tmpDisc, 0)
            lblTot.Text = "Grand Total : " & FormatNumber(tmpTot, 0)
            lblTax.Text = "Total Tax : " & FormatNumber(tmpTax, 0)
        End If
    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        Dim RPV As New frmRepPurchaseDetail
        RPV.tmpDatePassing = tmpDatePassing
        RPV.ReportViewer1.ZoomPercent = 100
        RPV.WindowState = FormWindowState.Maximized
        RPV.Show()
    End Sub
End Class