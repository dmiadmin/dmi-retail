﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListDeliveryOrder
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListDeliveryOrder))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdPrint = New System.Windows.Forms.Button()
        Me.cmdReport = New System.Windows.Forms.Button()
        Me.lblTo = New System.Windows.Forms.Label()
        Me.lblFrom = New System.Windows.Forms.Label()
        Me.cmdGenerate = New System.Windows.Forms.Button()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.DS_DELIVERY_SALE_ORDER = New DMI_RETAIL_SALE.DS_DELIVERY_SALE_ORDER()
        Me.SP_DELIVERY_ORDER_LISTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_DELIVERY_ORDER_LISTTableAdapter = New DMI_RETAIL_SALE.DS_DELIVERY_SALE_ORDERTableAdapters.SP_DELIVERY_ORDER_LISTTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_SALE.DS_DELIVERY_SALE_ORDERTableAdapters.TableAdapterManager()
        Me.SP_DELIVERY_ORDER_LISTDataGridView = New System.Windows.Forms.DataGridView()
        Me.DELIVERY_SALE_ORDER_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DELIVERY_SALE_ORDER_NUMBER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DELIVERED_DATE_TIME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SENDER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RECEIVIED_BY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RECEIVED_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_OF_ITEM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.REP_PERIOD1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.REP_PERIOD2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DS_DELIVERY_SALE_ORDER, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_DELIVERY_ORDER_LISTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_DELIVERY_ORDER_LISTDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdPrint)
        Me.GroupBox2.Controls.Add(Me.cmdReport)
        Me.GroupBox2.Controls.Add(Me.lblTo)
        Me.GroupBox2.Controls.Add(Me.lblFrom)
        Me.GroupBox2.Controls.Add(Me.cmdGenerate)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(735, 113)
        Me.GroupBox2.TabIndex = 12
        Me.GroupBox2.TabStop = False
        '
        'cmdPrint
        '
        Me.cmdPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdPrint.Image = Global.DMI_RETAIL_SALE.My.Resources.Resources.Printer_small
        Me.cmdPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdPrint.Location = New System.Drawing.Point(639, 70)
        Me.cmdPrint.Name = "cmdPrint"
        Me.cmdPrint.Size = New System.Drawing.Size(90, 24)
        Me.cmdPrint.TabIndex = 16
        Me.cmdPrint.Text = "Print [F12]"
        Me.cmdPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.cmdPrint.UseVisualStyleBackColor = True
        '
        'cmdReport
        '
        Me.cmdReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdReport.Image = Global.DMI_RETAIL_SALE.My.Resources.Resources.Printer_small
        Me.cmdReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdReport.Location = New System.Drawing.Point(889, 80)
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(90, 24)
        Me.cmdReport.TabIndex = 15
        Me.cmdReport.Text = "Print [F12]"
        Me.cmdReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.cmdReport.UseVisualStyleBackColor = True
        '
        'lblTo
        '
        Me.lblTo.AutoSize = True
        Me.lblTo.Location = New System.Drawing.Point(359, 21)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(23, 15)
        Me.lblTo.TabIndex = 8
        Me.lblTo.Text = "To"
        '
        'lblFrom
        '
        Me.lblFrom.AutoSize = True
        Me.lblFrom.Location = New System.Drawing.Point(141, 21)
        Me.lblFrom.Name = "lblFrom"
        Me.lblFrom.Size = New System.Drawing.Size(79, 15)
        Me.lblFrom.TabIndex = 7
        Me.lblFrom.Text = "From Period"
        '
        'cmdGenerate
        '
        Me.cmdGenerate.Image = CType(resources.GetObject("cmdGenerate.Image"), System.Drawing.Image)
        Me.cmdGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdGenerate.Location = New System.Drawing.Point(326, 55)
        Me.cmdGenerate.Name = "cmdGenerate"
        Me.cmdGenerate.Size = New System.Drawing.Size(99, 39)
        Me.cmdGenerate.TabIndex = 5
        Me.cmdGenerate.Text = "Generate"
        Me.cmdGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdGenerate.UseVisualStyleBackColor = True
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.CustomFormat = "dd-MMM-yyyy"
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker2.Location = New System.Drawing.Point(400, 18)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(120, 22)
        Me.DateTimePicker2.TabIndex = 4
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.CustomFormat = "dd-MMM-yyyy"
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker1.Location = New System.Drawing.Point(226, 18)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(123, 22)
        Me.DateTimePicker1.TabIndex = 3
        '
        'DS_DELIVERY_SALE_ORDER
        '
        Me.DS_DELIVERY_SALE_ORDER.DataSetName = "DS_DELIVERY_SALE_ORDER"
        Me.DS_DELIVERY_SALE_ORDER.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SP_DELIVERY_ORDER_LISTBindingSource
        '
        Me.SP_DELIVERY_ORDER_LISTBindingSource.DataMember = "SP_DELIVERY_ORDER_LIST"
        Me.SP_DELIVERY_ORDER_LISTBindingSource.DataSource = Me.DS_DELIVERY_SALE_ORDER
        '
        'SP_DELIVERY_ORDER_LISTTableAdapter
        '
        Me.SP_DELIVERY_ORDER_LISTTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.DELIVERY_SALE_ORDER_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.DELIVERY_SALE_ORDER_HEADERTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_SALE.DS_DELIVERY_SALE_ORDERTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'SP_DELIVERY_ORDER_LISTDataGridView
        '
        Me.SP_DELIVERY_ORDER_LISTDataGridView.AllowUserToAddRows = False
        Me.SP_DELIVERY_ORDER_LISTDataGridView.AllowUserToDeleteRows = False
        Me.SP_DELIVERY_ORDER_LISTDataGridView.AutoGenerateColumns = False
        Me.SP_DELIVERY_ORDER_LISTDataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SP_DELIVERY_ORDER_LISTDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.SP_DELIVERY_ORDER_LISTDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.SP_DELIVERY_ORDER_LISTDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DELIVERY_SALE_ORDER_ID, Me.DELIVERY_SALE_ORDER_NUMBER, Me.DELIVERED_DATE_TIME, Me.SENDER, Me.RECEIVIED_BY, Me.RECEIVED_DATE, Me.NO_OF_ITEM, Me.REP_PERIOD1, Me.REP_PERIOD2})
        Me.SP_DELIVERY_ORDER_LISTDataGridView.DataSource = Me.SP_DELIVERY_ORDER_LISTBindingSource
        Me.SP_DELIVERY_ORDER_LISTDataGridView.Location = New System.Drawing.Point(12, 121)
        Me.SP_DELIVERY_ORDER_LISTDataGridView.Name = "SP_DELIVERY_ORDER_LISTDataGridView"
        Me.SP_DELIVERY_ORDER_LISTDataGridView.ReadOnly = True
        Me.SP_DELIVERY_ORDER_LISTDataGridView.Size = New System.Drawing.Size(735, 220)
        Me.SP_DELIVERY_ORDER_LISTDataGridView.TabIndex = 14
        '
        'DELIVERY_SALE_ORDER_ID
        '
        Me.DELIVERY_SALE_ORDER_ID.DataPropertyName = "DELIVERY_SALE_ORDER_ID"
        Me.DELIVERY_SALE_ORDER_ID.HeaderText = "DELIVERY_SALE_ORDER_ID"
        Me.DELIVERY_SALE_ORDER_ID.Name = "DELIVERY_SALE_ORDER_ID"
        Me.DELIVERY_SALE_ORDER_ID.ReadOnly = True
        Me.DELIVERY_SALE_ORDER_ID.Visible = False
        '
        'DELIVERY_SALE_ORDER_NUMBER
        '
        Me.DELIVERY_SALE_ORDER_NUMBER.DataPropertyName = "DELIVERY_SALE_ORDER_NUMBER"
        Me.DELIVERY_SALE_ORDER_NUMBER.HeaderText = "Delivery Order Number"
        Me.DELIVERY_SALE_ORDER_NUMBER.Name = "DELIVERY_SALE_ORDER_NUMBER"
        Me.DELIVERY_SALE_ORDER_NUMBER.ReadOnly = True
        Me.DELIVERY_SALE_ORDER_NUMBER.Width = 150
        '
        'DELIVERED_DATE_TIME
        '
        Me.DELIVERED_DATE_TIME.DataPropertyName = "DELIVERED_DATE_TIME"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "dd-MMM-yyyy"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.DELIVERED_DATE_TIME.DefaultCellStyle = DataGridViewCellStyle2
        Me.DELIVERED_DATE_TIME.HeaderText = "Delivered Date Time"
        Me.DELIVERED_DATE_TIME.Name = "DELIVERED_DATE_TIME"
        Me.DELIVERED_DATE_TIME.ReadOnly = True
        Me.DELIVERED_DATE_TIME.Width = 125
        '
        'SENDER
        '
        Me.SENDER.DataPropertyName = "SENDER"
        Me.SENDER.HeaderText = "Sender"
        Me.SENDER.Name = "SENDER"
        Me.SENDER.ReadOnly = True
        '
        'RECEIVIED_BY
        '
        Me.RECEIVIED_BY.DataPropertyName = "RECEIVIED_BY"
        Me.RECEIVIED_BY.HeaderText = "Receivied By"
        Me.RECEIVIED_BY.Name = "RECEIVIED_BY"
        Me.RECEIVIED_BY.ReadOnly = True
        '
        'RECEIVED_DATE
        '
        Me.RECEIVED_DATE.DataPropertyName = "RECEIVED_DATE"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "dd-MMM-yyyy"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.RECEIVED_DATE.DefaultCellStyle = DataGridViewCellStyle3
        Me.RECEIVED_DATE.HeaderText = "Receivied date"
        Me.RECEIVED_DATE.Name = "RECEIVED_DATE"
        Me.RECEIVED_DATE.ReadOnly = True
        '
        'NO_OF_ITEM
        '
        Me.NO_OF_ITEM.DataPropertyName = "NO_OF_ITEM"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.NO_OF_ITEM.DefaultCellStyle = DataGridViewCellStyle4
        Me.NO_OF_ITEM.HeaderText = "No Of Item"
        Me.NO_OF_ITEM.Name = "NO_OF_ITEM"
        Me.NO_OF_ITEM.ReadOnly = True
        '
        'REP_PERIOD1
        '
        Me.REP_PERIOD1.DataPropertyName = "REP_PERIOD1"
        Me.REP_PERIOD1.HeaderText = "REP_PERIOD1"
        Me.REP_PERIOD1.Name = "REP_PERIOD1"
        Me.REP_PERIOD1.ReadOnly = True
        Me.REP_PERIOD1.Visible = False
        '
        'REP_PERIOD2
        '
        Me.REP_PERIOD2.DataPropertyName = "REP_PERIOD2"
        Me.REP_PERIOD2.HeaderText = "REP_PERIOD2"
        Me.REP_PERIOD2.Name = "REP_PERIOD2"
        Me.REP_PERIOD2.ReadOnly = True
        Me.REP_PERIOD2.Visible = False
        '
        'frmListDeliveryOrder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(758, 350)
        Me.Controls.Add(Me.SP_DELIVERY_ORDER_LISTDataGridView)
        Me.Controls.Add(Me.GroupBox2)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmListDeliveryOrder"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "List Delivery Order"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.DS_DELIVERY_SALE_ORDER, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_DELIVERY_ORDER_LISTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_DELIVERY_ORDER_LISTDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdPrint As System.Windows.Forms.Button
    Friend WithEvents cmdReport As System.Windows.Forms.Button
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents lblFrom As System.Windows.Forms.Label
    Friend WithEvents cmdGenerate As System.Windows.Forms.Button
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DS_DELIVERY_SALE_ORDER As DMI_RETAIL_SALE.DS_DELIVERY_SALE_ORDER
    Friend WithEvents SP_DELIVERY_ORDER_LISTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_DELIVERY_ORDER_LISTTableAdapter As DMI_RETAIL_SALE.DS_DELIVERY_SALE_ORDERTableAdapters.SP_DELIVERY_ORDER_LISTTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_SALE.DS_DELIVERY_SALE_ORDERTableAdapters.TableAdapterManager
    Friend WithEvents SP_DELIVERY_ORDER_LISTDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DELIVERY_SALE_ORDER_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DELIVERY_SALE_ORDER_NUMBER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DELIVERED_DATE_TIME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SENDER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RECEIVIED_BY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RECEIVED_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_OF_ITEM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents REP_PERIOD1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents REP_PERIOD2 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
