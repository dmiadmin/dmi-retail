﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Data

Public Class frmForgotPassword

    Public tmpUser, tmpHint As String
    Dim tmpform As Form
    Dim DMIUpdate As String
    Dim xConn As SqlConnection
    Dim xComm As SqlCommand
    Dim xReader As SqlDataReader

    Private Sub connection()
        xConn.Close()
        xConn.ConnectionString = "Data Source=" & ServerName & ";Initial Catalog=" & DatabaseName & ";Persist Security Info=True;User ID=sa;Password=SA;"
        xConn.Open()
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.Close()

    End Sub

    Private Sub frmForgotPassword_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtPassword.Text = ""
        Label2.Text = tmpHint
        tmpform = frmLogin

        If Language = "Indonesian" Then
            Me.Text = "Input Sandi"
            Label1.Text = "Petunjuk Sandi anda :"
            lblPassword.Text = "Kata Sandi"
            cmdCancel.Text = "&Batal"
        End If
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Dim Rs As ADODB.Recordset
        Rs = New ADODB.Recordset

        Rs.Open("SELECT PASSWORD FROM [USER] WHERE [USER_NAME] = '" & tmpUser & "'", Conn, _
                 ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

        If Rs.Fields(0).Value <> txtPassword.Text Then
            If Language = "Indonesian" Then
                MsgBox("Kata Sandi salah !" & vbCrLf & "Hubungi administrator anda untuk mendapatkan Kata Sandi baru.", _
                  MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Password !" & vbCrLf & "Please contact your Administrator to renew your Password.", _
                  MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        Rs = New ADODB.Recordset
        Rs.Open("SELECT ACCESS_PRIVILEDGE FROM [USER] WHERE [USER_NAME] = '" & tmpUser & "'", _
                Conn, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

        If Not Rs.EOF Then
            UserTypeId = Rs.Fields(0).Value
        Else
            If Language = "Indonesian" Then
                MsgBox("Nama User salah." & vbCrLf & "Periksa kembali Nama User pada Form Login.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong User Name." & vbCrLf & "Please check your User Name on Login Form.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If File.Exists(Application.StartupPath & "\DMI_UPDATE.sql") Then
            DMIUpdate = File.ReadAllText(Application.StartupPath & "\DMI_UPDATE.sql")
            Try
                xComm = New SqlCommand(DMIUpdate, xConn)
                xComm.ExecuteNonQuery()
                File.Delete(Application.StartupPath & "\DMI_UPDATE.sql")
            Catch
                If Language = "Indonesian" Then
                    MsgBox("Update gagal." & vbCrLf & "Periksa kembali isi dari file Update.", _
                       MsgBoxStyle.Information, "DMI Retail")
                Else
                    MsgBox("Update Failed." & vbCrLf & "Please check the Update File's content.", _
                       MsgBoxStyle.Information, "DMI Retail")
                End If
               
            End Try
        End If

        Rs = New ADODB.Recordset
        Rs.Open("UPDATE [USER] SET LAST_LOGIN = '" & Now & "' WHERE [USER_NAME] = '" & tmpUser & "'", _
               Conn, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

        frmMain.Show()
        Me.Close()
        tmpform.Close()
    End Sub
End Class