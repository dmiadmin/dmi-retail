﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.StatusLabelUser = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusLabelDatetime = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusLabelNotif = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecentFormToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ReferenceDataToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PaymentMethodToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListPaymentMethodToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryPaymentMethodToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CompanyInformationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UserToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListUserToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryUserToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccessPrivilegeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.NotepadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CalculatorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ChangePasswordToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator25 = New System.Windows.Forms.ToolStripSeparator()
        Me.LogOutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuMasterData = New System.Windows.Forms.ToolStripMenuItem()
        Me.CustomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListCustomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryCustomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VendorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListVendorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryVendorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalesmanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListSalesmanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntrySalesmanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WarehouseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListWarehouseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryWarehouseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LocationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListLocationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryLocationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem10 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryProductToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListAccountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryAccountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TransactionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpeningBalanceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.PurchaseOrderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem16 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryToolStripMenuItem13 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PurchaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator29 = New System.Windows.Forms.ToolStripSeparator()
        Me.SaleOrderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem14 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryToolStripMenuItem11 = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeliveryOrderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem15 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryToolStripMenuItem12 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator30 = New System.Windows.Forms.ToolStripSeparator()
        Me.AdjustmentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem6 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryToolStripMenuItem6 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.PayableToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem7 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryToolStripMenuItem7 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReceivableToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem5 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReceivableEntryToolStripMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.ReturnPurchaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem8 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryToolStripMenuItem8 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReturnSaleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem9 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryToolStripMenuItem9 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.CashRegisterToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PayableTransactionToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem12 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReceivableTransactionToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem13 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryToolStripMenuItem10 = New System.Windows.Forms.ToolStripMenuItem()
        Me.StockToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductDistributionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductDistributionListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductDistributionEntryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.ProductStockToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductWarehouseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.EndingStockToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StockCardToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator()
        Me.GroupingProductToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator()
        Me.SplitProductToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem11 = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryToolStripMenuItem5 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.StockToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.StockPerProductToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StockPerWarehouseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StockPerPriceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StockBalanceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TransactionToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.StockCardToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductMovementToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.SoldProductToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductDistributionToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator26 = New System.Windows.Forms.ToolStripSeparator()
        Me.PurchaseToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DailyPurchaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MonthlyPurchaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ByInvoiceToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ByVendorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ByPaymentMethodToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ByStatusToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ByPurchasingPriceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaleToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DailySalesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MonthlySalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ByInvoiceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BySalesmanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PerProductToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PerInvoiceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ByCustomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ByPaymentMethodToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ByStatusToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ByDailySoldProductToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator27 = New System.Windows.Forms.ToolStripSeparator()
        Me.ActivePayableToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActiveReceivableToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator28 = New System.Windows.Forms.ToolStripSeparator()
        Me.FinancialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BalanceSheetToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProfitLossToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.SoldProductToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalesReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator12 = New System.Windows.Forms.ToolStripSeparator()
        Me.DailyInvoiceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DailySalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator13 = New System.Windows.Forms.ToolStripSeparator()
        Me.ProductSoldPerInvoiceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalesPerInvoiceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalesPerProductToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator24 = New System.Windows.Forms.ToolStripSeparator()
        Me.ActivePayableToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActiveReceivableToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator15 = New System.Windows.Forms.ToolStripSeparator()
        Me.BalanceSheetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator23 = New System.Windows.Forms.ToolStripSeparator()
        Me.PurchasingPriceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductMovementToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataManagerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BackupDatabaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RestoreDatabaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator14 = New System.Windows.Forms.ToolStripSeparator()
        Me.ClosingPeriodToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MonthlyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.YearlyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator16 = New System.Windows.Forms.ToolStripSeparator()
        Me.ConfigurationToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.PurchaseButton1 = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator17 = New System.Windows.Forms.ToolStripSeparator()
        Me.SaleButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator18 = New System.Windows.Forms.ToolStripSeparator()
        Me.StockButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator19 = New System.Windows.Forms.ToolStripSeparator()
        Me.InvoiceButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator20 = New System.Windows.Forms.ToolStripSeparator()
        Me.StockCardButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator21 = New System.Windows.Forms.ToolStripSeparator()
        Me.WarehouseButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator22 = New System.Windows.Forms.ToolStripSeparator()
        Me.ConfigButton1 = New System.Windows.Forms.ToolStripButton()
        Me.TimerSafeStock = New System.Windows.Forms.Timer(Me.components)
        Me.USERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PARAMETERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_NOTIFICATION_SAFE_STOCKBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.StatusStrip1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        CType(Me.USERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PARAMETERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_NOTIFICATION_SAFE_STOCKBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StatusLabelUser, Me.ToolStripStatusLabel1, Me.StatusLabelDatetime, Me.ToolStripStatusLabel2, Me.StatusLabelNotif})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 585)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 16, 0)
        Me.StatusStrip1.ShowItemToolTips = True
        Me.StatusStrip1.Size = New System.Drawing.Size(906, 25)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'StatusLabelUser
        '
        Me.StatusLabelUser.Name = "StatusLabelUser"
        Me.StatusLabelUser.Size = New System.Drawing.Size(89, 20)
        Me.StatusLabelUser.Text = "User Name :"
        Me.StatusLabelUser.ToolTipText = "User Name"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.ToolStripStatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedOuter
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(4, 20)
        '
        'StatusLabelDatetime
        '
        Me.StatusLabelDatetime.Name = "StatusLabelDatetime"
        Me.StatusLabelDatetime.Size = New System.Drawing.Size(106, 20)
        Me.StatusLabelDatetime.Text = "dd-MMM-yyyy"
        Me.StatusLabelDatetime.ToolTipText = "Date and Time Now"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.ToolStripStatusLabel2.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedOuter
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(4, 20)
        '
        'StatusLabelNotif
        '
        Me.StatusLabelNotif.IsLink = True
        Me.StatusLabelNotif.Name = "StatusLabelNotif"
        Me.StatusLabelNotif.Size = New System.Drawing.Size(88, 20)
        Me.StatusLabelNotif.Text = "Notification"
        Me.StatusLabelNotif.ToolTipText = "Notification to User"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Empty
        Me.ImageList1.Images.SetKeyName(0, "button2.bmp")
        Me.ImageList1.Images.SetKeyName(1, "button.bmp")
        Me.ImageList1.Images.SetKeyName(2, "account recenble1.bmp")
        Me.ImageList1.Images.SetKeyName(3, "account recenble2.bmp")
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.menuMasterData, Me.TransactionToolStripMenuItem, Me.StockToolStripMenuItem, Me.ReportToolStripMenuItem, Me.ListToolStripMenuItem3, Me.DataManagerToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.MenuStrip1.Size = New System.Drawing.Size(906, 28)
        Me.MenuStrip1.TabIndex = 28
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RecentFormToolStripMenuItem, Me.ToolStripSeparator1, Me.ReferenceDataToolStripMenuItem, Me.ToolStripSeparator2, Me.NotepadToolStripMenuItem, Me.CalculatorToolStripMenuItem, Me.ToolStripSeparator3, Me.ChangePasswordToolStripMenuItem, Me.ToolStripSeparator25, Me.LogOutToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(44, 24)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'RecentFormToolStripMenuItem
        '
        Me.RecentFormToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RecentToolStripMenuItem})
        Me.RecentFormToolStripMenuItem.Name = "RecentFormToolStripMenuItem"
        Me.RecentFormToolStripMenuItem.Size = New System.Drawing.Size(221, 24)
        Me.RecentFormToolStripMenuItem.Text = "Recently Closed Form"
        '
        'RecentToolStripMenuItem
        '
        Me.RecentToolStripMenuItem.Name = "RecentToolStripMenuItem"
        Me.RecentToolStripMenuItem.Size = New System.Drawing.Size(161, 24)
        Me.RecentToolStripMenuItem.Text = "Form Recent"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(218, 6)
        '
        'ReferenceDataToolStripMenuItem
        '
        Me.ReferenceDataToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PaymentMethodToolStripMenuItem, Me.CompanyInformationToolStripMenuItem, Me.UserToolStripMenuItem1, Me.AccessPrivilegeToolStripMenuItem})
        Me.ReferenceDataToolStripMenuItem.Name = "ReferenceDataToolStripMenuItem"
        Me.ReferenceDataToolStripMenuItem.Size = New System.Drawing.Size(221, 24)
        Me.ReferenceDataToolStripMenuItem.Text = "&Reference Data"
        '
        'PaymentMethodToolStripMenuItem
        '
        Me.PaymentMethodToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListPaymentMethodToolStripMenuItem, Me.EntryPaymentMethodToolStripMenuItem})
        Me.PaymentMethodToolStripMenuItem.Name = "PaymentMethodToolStripMenuItem"
        Me.PaymentMethodToolStripMenuItem.Size = New System.Drawing.Size(223, 24)
        Me.PaymentMethodToolStripMenuItem.Text = "&Payment Method"
        '
        'ListPaymentMethodToolStripMenuItem
        '
        Me.ListPaymentMethodToolStripMenuItem.Name = "ListPaymentMethodToolStripMenuItem"
        Me.ListPaymentMethodToolStripMenuItem.Size = New System.Drawing.Size(228, 24)
        Me.ListPaymentMethodToolStripMenuItem.Text = "&List Payment Method"
        '
        'EntryPaymentMethodToolStripMenuItem
        '
        Me.EntryPaymentMethodToolStripMenuItem.Name = "EntryPaymentMethodToolStripMenuItem"
        Me.EntryPaymentMethodToolStripMenuItem.Size = New System.Drawing.Size(228, 24)
        Me.EntryPaymentMethodToolStripMenuItem.Text = "&Entry Payment Method"
        '
        'CompanyInformationToolStripMenuItem
        '
        Me.CompanyInformationToolStripMenuItem.Name = "CompanyInformationToolStripMenuItem"
        Me.CompanyInformationToolStripMenuItem.Size = New System.Drawing.Size(223, 24)
        Me.CompanyInformationToolStripMenuItem.Text = "C&ompany Information"
        '
        'UserToolStripMenuItem1
        '
        Me.UserToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListUserToolStripMenuItem, Me.EntryUserToolStripMenuItem1})
        Me.UserToolStripMenuItem1.Name = "UserToolStripMenuItem1"
        Me.UserToolStripMenuItem1.Size = New System.Drawing.Size(223, 24)
        Me.UserToolStripMenuItem1.Text = "&User"
        '
        'ListUserToolStripMenuItem
        '
        Me.ListUserToolStripMenuItem.Name = "ListUserToolStripMenuItem"
        Me.ListUserToolStripMenuItem.Size = New System.Drawing.Size(144, 24)
        Me.ListUserToolStripMenuItem.Text = "List User"
        '
        'EntryUserToolStripMenuItem1
        '
        Me.EntryUserToolStripMenuItem1.Name = "EntryUserToolStripMenuItem1"
        Me.EntryUserToolStripMenuItem1.Size = New System.Drawing.Size(144, 24)
        Me.EntryUserToolStripMenuItem1.Text = "Entry User"
        '
        'AccessPrivilegeToolStripMenuItem
        '
        Me.AccessPrivilegeToolStripMenuItem.Name = "AccessPrivilegeToolStripMenuItem"
        Me.AccessPrivilegeToolStripMenuItem.Size = New System.Drawing.Size(223, 24)
        Me.AccessPrivilegeToolStripMenuItem.Text = "&Access Privilege"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(218, 6)
        '
        'NotepadToolStripMenuItem
        '
        Me.NotepadToolStripMenuItem.Name = "NotepadToolStripMenuItem"
        Me.NotepadToolStripMenuItem.Size = New System.Drawing.Size(221, 24)
        Me.NotepadToolStripMenuItem.Text = "Notepad"
        '
        'CalculatorToolStripMenuItem
        '
        Me.CalculatorToolStripMenuItem.Name = "CalculatorToolStripMenuItem"
        Me.CalculatorToolStripMenuItem.Size = New System.Drawing.Size(221, 24)
        Me.CalculatorToolStripMenuItem.Text = "Calculator"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(218, 6)
        '
        'ChangePasswordToolStripMenuItem
        '
        Me.ChangePasswordToolStripMenuItem.Name = "ChangePasswordToolStripMenuItem"
        Me.ChangePasswordToolStripMenuItem.Size = New System.Drawing.Size(221, 24)
        Me.ChangePasswordToolStripMenuItem.Text = "Change  Password"
        '
        'ToolStripSeparator25
        '
        Me.ToolStripSeparator25.Name = "ToolStripSeparator25"
        Me.ToolStripSeparator25.Size = New System.Drawing.Size(218, 6)
        '
        'LogOutToolStripMenuItem
        '
        Me.LogOutToolStripMenuItem.Name = "LogOutToolStripMenuItem"
        Me.LogOutToolStripMenuItem.Size = New System.Drawing.Size(221, 24)
        Me.LogOutToolStripMenuItem.Text = "Log &Out"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(221, 24)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'menuMasterData
        '
        Me.menuMasterData.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CustomerToolStripMenuItem, Me.VendorToolStripMenuItem, Me.SalesmanToolStripMenuItem, Me.WarehouseToolStripMenuItem, Me.LocationToolStripMenuItem, Me.ProductToolStripMenuItem, Me.AccountToolStripMenuItem})
        Me.menuMasterData.Name = "menuMasterData"
        Me.menuMasterData.Size = New System.Drawing.Size(102, 24)
        Me.menuMasterData.Text = "&Master Data"
        '
        'CustomerToolStripMenuItem
        '
        Me.CustomerToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListCustomerToolStripMenuItem, Me.EntryCustomerToolStripMenuItem})
        Me.CustomerToolStripMenuItem.Name = "CustomerToolStripMenuItem"
        Me.CustomerToolStripMenuItem.Size = New System.Drawing.Size(152, 24)
        Me.CustomerToolStripMenuItem.Text = "&Customer"
        '
        'ListCustomerToolStripMenuItem
        '
        Me.ListCustomerToolStripMenuItem.Name = "ListCustomerToolStripMenuItem"
        Me.ListCustomerToolStripMenuItem.Size = New System.Drawing.Size(178, 24)
        Me.ListCustomerToolStripMenuItem.Text = "List Customer"
        '
        'EntryCustomerToolStripMenuItem
        '
        Me.EntryCustomerToolStripMenuItem.Name = "EntryCustomerToolStripMenuItem"
        Me.EntryCustomerToolStripMenuItem.Size = New System.Drawing.Size(178, 24)
        Me.EntryCustomerToolStripMenuItem.Text = "Entry Customer"
        '
        'VendorToolStripMenuItem
        '
        Me.VendorToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListVendorToolStripMenuItem, Me.EntryVendorToolStripMenuItem})
        Me.VendorToolStripMenuItem.Name = "VendorToolStripMenuItem"
        Me.VendorToolStripMenuItem.Size = New System.Drawing.Size(152, 24)
        Me.VendorToolStripMenuItem.Text = "&Vendor"
        '
        'ListVendorToolStripMenuItem
        '
        Me.ListVendorToolStripMenuItem.Name = "ListVendorToolStripMenuItem"
        Me.ListVendorToolStripMenuItem.Size = New System.Drawing.Size(163, 24)
        Me.ListVendorToolStripMenuItem.Text = "List Vendor"
        '
        'EntryVendorToolStripMenuItem
        '
        Me.EntryVendorToolStripMenuItem.Name = "EntryVendorToolStripMenuItem"
        Me.EntryVendorToolStripMenuItem.Size = New System.Drawing.Size(163, 24)
        Me.EntryVendorToolStripMenuItem.Text = "Entry Vendor"
        '
        'SalesmanToolStripMenuItem
        '
        Me.SalesmanToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListSalesmanToolStripMenuItem, Me.EntrySalesmanToolStripMenuItem})
        Me.SalesmanToolStripMenuItem.Name = "SalesmanToolStripMenuItem"
        Me.SalesmanToolStripMenuItem.Size = New System.Drawing.Size(152, 24)
        Me.SalesmanToolStripMenuItem.Text = "&Salesman"
        '
        'ListSalesmanToolStripMenuItem
        '
        Me.ListSalesmanToolStripMenuItem.Name = "ListSalesmanToolStripMenuItem"
        Me.ListSalesmanToolStripMenuItem.Size = New System.Drawing.Size(178, 24)
        Me.ListSalesmanToolStripMenuItem.Text = "List Salesman"
        '
        'EntrySalesmanToolStripMenuItem
        '
        Me.EntrySalesmanToolStripMenuItem.Name = "EntrySalesmanToolStripMenuItem"
        Me.EntrySalesmanToolStripMenuItem.Size = New System.Drawing.Size(178, 24)
        Me.EntrySalesmanToolStripMenuItem.Text = "Entry Salesman"
        '
        'WarehouseToolStripMenuItem
        '
        Me.WarehouseToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListWarehouseToolStripMenuItem, Me.EntryWarehouseToolStripMenuItem})
        Me.WarehouseToolStripMenuItem.Name = "WarehouseToolStripMenuItem"
        Me.WarehouseToolStripMenuItem.Size = New System.Drawing.Size(152, 24)
        Me.WarehouseToolStripMenuItem.Text = "&Warehouse"
        '
        'ListWarehouseToolStripMenuItem
        '
        Me.ListWarehouseToolStripMenuItem.Name = "ListWarehouseToolStripMenuItem"
        Me.ListWarehouseToolStripMenuItem.Size = New System.Drawing.Size(189, 24)
        Me.ListWarehouseToolStripMenuItem.Text = "List Warehouse"
        '
        'EntryWarehouseToolStripMenuItem
        '
        Me.EntryWarehouseToolStripMenuItem.Name = "EntryWarehouseToolStripMenuItem"
        Me.EntryWarehouseToolStripMenuItem.Size = New System.Drawing.Size(189, 24)
        Me.EntryWarehouseToolStripMenuItem.Text = "Entry Warehouse"
        '
        'LocationToolStripMenuItem
        '
        Me.LocationToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListLocationToolStripMenuItem, Me.EntryLocationToolStripMenuItem})
        Me.LocationToolStripMenuItem.Name = "LocationToolStripMenuItem"
        Me.LocationToolStripMenuItem.Size = New System.Drawing.Size(152, 24)
        Me.LocationToolStripMenuItem.Text = "L&ocation"
        '
        'ListLocationToolStripMenuItem
        '
        Me.ListLocationToolStripMenuItem.Name = "ListLocationToolStripMenuItem"
        Me.ListLocationToolStripMenuItem.Size = New System.Drawing.Size(172, 24)
        Me.ListLocationToolStripMenuItem.Text = "List Location"
        '
        'EntryLocationToolStripMenuItem
        '
        Me.EntryLocationToolStripMenuItem.Name = "EntryLocationToolStripMenuItem"
        Me.EntryLocationToolStripMenuItem.Size = New System.Drawing.Size(172, 24)
        Me.EntryLocationToolStripMenuItem.Text = "Entry Location"
        '
        'ProductToolStripMenuItem
        '
        Me.ProductToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListToolStripMenuItem10, Me.EntryProductToolStripMenuItem})
        Me.ProductToolStripMenuItem.Name = "ProductToolStripMenuItem"
        Me.ProductToolStripMenuItem.Size = New System.Drawing.Size(152, 24)
        Me.ProductToolStripMenuItem.Text = "&Product"
        '
        'ListToolStripMenuItem10
        '
        Me.ListToolStripMenuItem10.Name = "ListToolStripMenuItem10"
        Me.ListToolStripMenuItem10.Size = New System.Drawing.Size(166, 24)
        Me.ListToolStripMenuItem10.Text = "List Product"
        '
        'EntryProductToolStripMenuItem
        '
        Me.EntryProductToolStripMenuItem.Name = "EntryProductToolStripMenuItem"
        Me.EntryProductToolStripMenuItem.Size = New System.Drawing.Size(166, 24)
        Me.EntryProductToolStripMenuItem.Text = "Entry Product"
        '
        'AccountToolStripMenuItem
        '
        Me.AccountToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListAccountToolStripMenuItem, Me.EntryAccountToolStripMenuItem})
        Me.AccountToolStripMenuItem.Name = "AccountToolStripMenuItem"
        Me.AccountToolStripMenuItem.Size = New System.Drawing.Size(152, 24)
        Me.AccountToolStripMenuItem.Text = "&Account"
        '
        'ListAccountToolStripMenuItem
        '
        Me.ListAccountToolStripMenuItem.Name = "ListAccountToolStripMenuItem"
        Me.ListAccountToolStripMenuItem.Size = New System.Drawing.Size(169, 24)
        Me.ListAccountToolStripMenuItem.Text = "List Account"
        '
        'EntryAccountToolStripMenuItem
        '
        Me.EntryAccountToolStripMenuItem.Name = "EntryAccountToolStripMenuItem"
        Me.EntryAccountToolStripMenuItem.Size = New System.Drawing.Size(169, 24)
        Me.EntryAccountToolStripMenuItem.Text = "Entry Account"
        '
        'TransactionToolStripMenuItem
        '
        Me.TransactionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpeningBalanceToolStripMenuItem, Me.ToolStripSeparator5, Me.PurchaseOrderToolStripMenuItem, Me.PurchaseToolStripMenuItem, Me.ToolStripSeparator29, Me.SaleOrderToolStripMenuItem, Me.SaleToolStripMenuItem, Me.DeliveryOrderToolStripMenuItem, Me.ToolStripSeparator30, Me.AdjustmentToolStripMenuItem, Me.ToolStripSeparator7, Me.PayableToolStripMenuItem, Me.ReceivableToolStripMenuItem, Me.ToolStripSeparator6, Me.ReturnPurchaseToolStripMenuItem, Me.ReturnSaleToolStripMenuItem, Me.ToolStripSeparator4, Me.CashRegisterToolStripMenuItem1})
        Me.TransactionToolStripMenuItem.Name = "TransactionToolStripMenuItem"
        Me.TransactionToolStripMenuItem.Size = New System.Drawing.Size(97, 24)
        Me.TransactionToolStripMenuItem.Text = "&Transaction"
        '
        'OpeningBalanceToolStripMenuItem
        '
        Me.OpeningBalanceToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListToolStripMenuItem, Me.EntryToolStripMenuItem})
        Me.OpeningBalanceToolStripMenuItem.Name = "OpeningBalanceToolStripMenuItem"
        Me.OpeningBalanceToolStripMenuItem.Size = New System.Drawing.Size(191, 24)
        Me.OpeningBalanceToolStripMenuItem.Text = "&Opening Balance"
        '
        'ListToolStripMenuItem
        '
        Me.ListToolStripMenuItem.Name = "ListToolStripMenuItem"
        Me.ListToolStripMenuItem.Size = New System.Drawing.Size(152, 24)
        Me.ListToolStripMenuItem.Text = "List"
        '
        'EntryToolStripMenuItem
        '
        Me.EntryToolStripMenuItem.Name = "EntryToolStripMenuItem"
        Me.EntryToolStripMenuItem.Size = New System.Drawing.Size(152, 24)
        Me.EntryToolStripMenuItem.Text = "Entry"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(188, 6)
        '
        'PurchaseOrderToolStripMenuItem
        '
        Me.PurchaseOrderToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListToolStripMenuItem16, Me.EntryToolStripMenuItem13})
        Me.PurchaseOrderToolStripMenuItem.Name = "PurchaseOrderToolStripMenuItem"
        Me.PurchaseOrderToolStripMenuItem.Size = New System.Drawing.Size(191, 24)
        Me.PurchaseOrderToolStripMenuItem.Text = "Purchase Orde&r"
        Me.PurchaseOrderToolStripMenuItem.Visible = False
        '
        'ListToolStripMenuItem16
        '
        Me.ListToolStripMenuItem16.Name = "ListToolStripMenuItem16"
        Me.ListToolStripMenuItem16.Size = New System.Drawing.Size(152, 24)
        Me.ListToolStripMenuItem16.Text = "List"
        '
        'EntryToolStripMenuItem13
        '
        Me.EntryToolStripMenuItem13.Name = "EntryToolStripMenuItem13"
        Me.EntryToolStripMenuItem13.Size = New System.Drawing.Size(152, 24)
        Me.EntryToolStripMenuItem13.Text = "Entry"
        '
        'PurchaseToolStripMenuItem
        '
        Me.PurchaseToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListToolStripMenuItem1, Me.EntryToolStripMenuItem1})
        Me.PurchaseToolStripMenuItem.Name = "PurchaseToolStripMenuItem"
        Me.PurchaseToolStripMenuItem.Size = New System.Drawing.Size(191, 24)
        Me.PurchaseToolStripMenuItem.Text = "P&urchase"
        '
        'ListToolStripMenuItem1
        '
        Me.ListToolStripMenuItem1.Name = "ListToolStripMenuItem1"
        Me.ListToolStripMenuItem1.Size = New System.Drawing.Size(152, 24)
        Me.ListToolStripMenuItem1.Text = "List"
        '
        'EntryToolStripMenuItem1
        '
        Me.EntryToolStripMenuItem1.Name = "EntryToolStripMenuItem1"
        Me.EntryToolStripMenuItem1.Size = New System.Drawing.Size(152, 24)
        Me.EntryToolStripMenuItem1.Text = "Entry"
        '
        'ToolStripSeparator29
        '
        Me.ToolStripSeparator29.Name = "ToolStripSeparator29"
        Me.ToolStripSeparator29.Size = New System.Drawing.Size(188, 6)
        Me.ToolStripSeparator29.Visible = False
        '
        'SaleOrderToolStripMenuItem
        '
        Me.SaleOrderToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListToolStripMenuItem14, Me.EntryToolStripMenuItem11})
        Me.SaleOrderToolStripMenuItem.Name = "SaleOrderToolStripMenuItem"
        Me.SaleOrderToolStripMenuItem.Size = New System.Drawing.Size(191, 24)
        Me.SaleOrderToolStripMenuItem.Text = "Sale Or&der"
        Me.SaleOrderToolStripMenuItem.Visible = False
        '
        'ListToolStripMenuItem14
        '
        Me.ListToolStripMenuItem14.Name = "ListToolStripMenuItem14"
        Me.ListToolStripMenuItem14.Size = New System.Drawing.Size(111, 24)
        Me.ListToolStripMenuItem14.Text = "&List"
        '
        'EntryToolStripMenuItem11
        '
        Me.EntryToolStripMenuItem11.Name = "EntryToolStripMenuItem11"
        Me.EntryToolStripMenuItem11.Size = New System.Drawing.Size(111, 24)
        Me.EntryToolStripMenuItem11.Text = "&Entry"
        '
        'SaleToolStripMenuItem
        '
        Me.SaleToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListToolStripMenuItem2, Me.EntryToolStripMenuItem2})
        Me.SaleToolStripMenuItem.Name = "SaleToolStripMenuItem"
        Me.SaleToolStripMenuItem.Size = New System.Drawing.Size(191, 24)
        Me.SaleToolStripMenuItem.Text = "&Sale"
        '
        'ListToolStripMenuItem2
        '
        Me.ListToolStripMenuItem2.Name = "ListToolStripMenuItem2"
        Me.ListToolStripMenuItem2.Size = New System.Drawing.Size(111, 24)
        Me.ListToolStripMenuItem2.Text = "List"
        '
        'EntryToolStripMenuItem2
        '
        Me.EntryToolStripMenuItem2.Name = "EntryToolStripMenuItem2"
        Me.EntryToolStripMenuItem2.Size = New System.Drawing.Size(111, 24)
        Me.EntryToolStripMenuItem2.Text = "Entry"
        '
        'DeliveryOrderToolStripMenuItem
        '
        Me.DeliveryOrderToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListToolStripMenuItem15, Me.EntryToolStripMenuItem12})
        Me.DeliveryOrderToolStripMenuItem.Name = "DeliveryOrderToolStripMenuItem"
        Me.DeliveryOrderToolStripMenuItem.Size = New System.Drawing.Size(191, 24)
        Me.DeliveryOrderToolStripMenuItem.Text = "Deli&very Order"
        Me.DeliveryOrderToolStripMenuItem.Visible = False
        '
        'ListToolStripMenuItem15
        '
        Me.ListToolStripMenuItem15.Name = "ListToolStripMenuItem15"
        Me.ListToolStripMenuItem15.Size = New System.Drawing.Size(111, 24)
        Me.ListToolStripMenuItem15.Text = "&List"
        '
        'EntryToolStripMenuItem12
        '
        Me.EntryToolStripMenuItem12.Name = "EntryToolStripMenuItem12"
        Me.EntryToolStripMenuItem12.Size = New System.Drawing.Size(111, 24)
        Me.EntryToolStripMenuItem12.Text = "&Entry"
        '
        'ToolStripSeparator30
        '
        Me.ToolStripSeparator30.Name = "ToolStripSeparator30"
        Me.ToolStripSeparator30.Size = New System.Drawing.Size(188, 6)
        '
        'AdjustmentToolStripMenuItem
        '
        Me.AdjustmentToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListToolStripMenuItem6, Me.EntryToolStripMenuItem6})
        Me.AdjustmentToolStripMenuItem.Name = "AdjustmentToolStripMenuItem"
        Me.AdjustmentToolStripMenuItem.Size = New System.Drawing.Size(191, 24)
        Me.AdjustmentToolStripMenuItem.Text = "&Adjustment"
        '
        'ListToolStripMenuItem6
        '
        Me.ListToolStripMenuItem6.Name = "ListToolStripMenuItem6"
        Me.ListToolStripMenuItem6.Size = New System.Drawing.Size(152, 24)
        Me.ListToolStripMenuItem6.Text = "List"
        '
        'EntryToolStripMenuItem6
        '
        Me.EntryToolStripMenuItem6.Name = "EntryToolStripMenuItem6"
        Me.EntryToolStripMenuItem6.Size = New System.Drawing.Size(152, 24)
        Me.EntryToolStripMenuItem6.Text = "Entry"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(188, 6)
        '
        'PayableToolStripMenuItem
        '
        Me.PayableToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListToolStripMenuItem7, Me.EntryToolStripMenuItem7})
        Me.PayableToolStripMenuItem.Name = "PayableToolStripMenuItem"
        Me.PayableToolStripMenuItem.Size = New System.Drawing.Size(191, 24)
        Me.PayableToolStripMenuItem.Text = "&Payable"
        '
        'ListToolStripMenuItem7
        '
        Me.ListToolStripMenuItem7.Name = "ListToolStripMenuItem7"
        Me.ListToolStripMenuItem7.Size = New System.Drawing.Size(135, 24)
        Me.ListToolStripMenuItem7.Text = "List"
        '
        'EntryToolStripMenuItem7
        '
        Me.EntryToolStripMenuItem7.Name = "EntryToolStripMenuItem7"
        Me.EntryToolStripMenuItem7.Size = New System.Drawing.Size(135, 24)
        Me.EntryToolStripMenuItem7.Text = "Payment"
        '
        'ReceivableToolStripMenuItem
        '
        Me.ReceivableToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListToolStripMenuItem5, Me.ReceivableEntryToolStripMenu})
        Me.ReceivableToolStripMenuItem.Name = "ReceivableToolStripMenuItem"
        Me.ReceivableToolStripMenuItem.Size = New System.Drawing.Size(191, 24)
        Me.ReceivableToolStripMenuItem.Text = "&Receivable"
        '
        'ListToolStripMenuItem5
        '
        Me.ListToolStripMenuItem5.Name = "ListToolStripMenuItem5"
        Me.ListToolStripMenuItem5.Size = New System.Drawing.Size(135, 24)
        Me.ListToolStripMenuItem5.Text = "List"
        '
        'ReceivableEntryToolStripMenu
        '
        Me.ReceivableEntryToolStripMenu.Name = "ReceivableEntryToolStripMenu"
        Me.ReceivableEntryToolStripMenu.Size = New System.Drawing.Size(135, 24)
        Me.ReceivableEntryToolStripMenu.Text = "Payment"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(188, 6)
        '
        'ReturnPurchaseToolStripMenuItem
        '
        Me.ReturnPurchaseToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListToolStripMenuItem8, Me.EntryToolStripMenuItem8})
        Me.ReturnPurchaseToolStripMenuItem.Name = "ReturnPurchaseToolStripMenuItem"
        Me.ReturnPurchaseToolStripMenuItem.Size = New System.Drawing.Size(191, 24)
        Me.ReturnPurchaseToolStripMenuItem.Text = "Return Purc&hase"
        '
        'ListToolStripMenuItem8
        '
        Me.ListToolStripMenuItem8.Name = "ListToolStripMenuItem8"
        Me.ListToolStripMenuItem8.Size = New System.Drawing.Size(111, 24)
        Me.ListToolStripMenuItem8.Text = "List"
        '
        'EntryToolStripMenuItem8
        '
        Me.EntryToolStripMenuItem8.Name = "EntryToolStripMenuItem8"
        Me.EntryToolStripMenuItem8.Size = New System.Drawing.Size(111, 24)
        Me.EntryToolStripMenuItem8.Text = "Entry"
        '
        'ReturnSaleToolStripMenuItem
        '
        Me.ReturnSaleToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListToolStripMenuItem9, Me.EntryToolStripMenuItem9})
        Me.ReturnSaleToolStripMenuItem.Name = "ReturnSaleToolStripMenuItem"
        Me.ReturnSaleToolStripMenuItem.Size = New System.Drawing.Size(191, 24)
        Me.ReturnSaleToolStripMenuItem.Text = "Return Sal&e"
        '
        'ListToolStripMenuItem9
        '
        Me.ListToolStripMenuItem9.Name = "ListToolStripMenuItem9"
        Me.ListToolStripMenuItem9.Size = New System.Drawing.Size(111, 24)
        Me.ListToolStripMenuItem9.Text = "List"
        '
        'EntryToolStripMenuItem9
        '
        Me.EntryToolStripMenuItem9.Name = "EntryToolStripMenuItem9"
        Me.EntryToolStripMenuItem9.Size = New System.Drawing.Size(111, 24)
        Me.EntryToolStripMenuItem9.Text = "Entry"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(188, 6)
        '
        'CashRegisterToolStripMenuItem1
        '
        Me.CashRegisterToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PayableTransactionToolStripMenuItem1, Me.ReceivableTransactionToolStripMenuItem1})
        Me.CashRegisterToolStripMenuItem1.Name = "CashRegisterToolStripMenuItem1"
        Me.CashRegisterToolStripMenuItem1.Size = New System.Drawing.Size(191, 24)
        Me.CashRegisterToolStripMenuItem1.Text = "&Cash Register"
        '
        'PayableTransactionToolStripMenuItem1
        '
        Me.PayableTransactionToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListToolStripMenuItem12, Me.EntryToolStripMenuItem4})
        Me.PayableTransactionToolStripMenuItem1.Name = "PayableTransactionToolStripMenuItem1"
        Me.PayableTransactionToolStripMenuItem1.Size = New System.Drawing.Size(230, 24)
        Me.PayableTransactionToolStripMenuItem1.Text = "Payable Transaction"
        '
        'ListToolStripMenuItem12
        '
        Me.ListToolStripMenuItem12.Name = "ListToolStripMenuItem12"
        Me.ListToolStripMenuItem12.Size = New System.Drawing.Size(111, 24)
        Me.ListToolStripMenuItem12.Text = "List"
        '
        'EntryToolStripMenuItem4
        '
        Me.EntryToolStripMenuItem4.Name = "EntryToolStripMenuItem4"
        Me.EntryToolStripMenuItem4.Size = New System.Drawing.Size(111, 24)
        Me.EntryToolStripMenuItem4.Text = "Entry"
        '
        'ReceivableTransactionToolStripMenuItem1
        '
        Me.ReceivableTransactionToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListToolStripMenuItem13, Me.EntryToolStripMenuItem10})
        Me.ReceivableTransactionToolStripMenuItem1.Name = "ReceivableTransactionToolStripMenuItem1"
        Me.ReceivableTransactionToolStripMenuItem1.Size = New System.Drawing.Size(230, 24)
        Me.ReceivableTransactionToolStripMenuItem1.Text = "Receivable Transaction"
        '
        'ListToolStripMenuItem13
        '
        Me.ListToolStripMenuItem13.Name = "ListToolStripMenuItem13"
        Me.ListToolStripMenuItem13.Size = New System.Drawing.Size(111, 24)
        Me.ListToolStripMenuItem13.Text = "List"
        '
        'EntryToolStripMenuItem10
        '
        Me.EntryToolStripMenuItem10.Name = "EntryToolStripMenuItem10"
        Me.EntryToolStripMenuItem10.Size = New System.Drawing.Size(111, 24)
        Me.EntryToolStripMenuItem10.Text = "Entry"
        '
        'StockToolStripMenuItem
        '
        Me.StockToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProductDistributionToolStripMenuItem, Me.ToolStripSeparator8, Me.ProductStockToolStripMenuItem, Me.ProductWarehouseToolStripMenuItem, Me.ToolStripSeparator9, Me.EndingStockToolStripMenuItem, Me.StockCardToolStripMenuItem, Me.ToolStripSeparator10, Me.GroupingProductToolStripMenuItem, Me.ToolStripSeparator11, Me.SplitProductToolStripMenuItem})
        Me.StockToolStripMenuItem.Name = "StockToolStripMenuItem"
        Me.StockToolStripMenuItem.Size = New System.Drawing.Size(82, 24)
        Me.StockToolStripMenuItem.Text = "&Inventory"
        '
        'ProductDistributionToolStripMenuItem
        '
        Me.ProductDistributionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProductDistributionListToolStripMenuItem, Me.ProductDistributionEntryToolStripMenuItem})
        Me.ProductDistributionToolStripMenuItem.Name = "ProductDistributionToolStripMenuItem"
        Me.ProductDistributionToolStripMenuItem.Size = New System.Drawing.Size(211, 24)
        Me.ProductDistributionToolStripMenuItem.Text = "&Product Distribution"
        '
        'ProductDistributionListToolStripMenuItem
        '
        Me.ProductDistributionListToolStripMenuItem.Name = "ProductDistributionListToolStripMenuItem"
        Me.ProductDistributionListToolStripMenuItem.Size = New System.Drawing.Size(111, 24)
        Me.ProductDistributionListToolStripMenuItem.Text = "List"
        '
        'ProductDistributionEntryToolStripMenuItem
        '
        Me.ProductDistributionEntryToolStripMenuItem.Name = "ProductDistributionEntryToolStripMenuItem"
        Me.ProductDistributionEntryToolStripMenuItem.Size = New System.Drawing.Size(111, 24)
        Me.ProductDistributionEntryToolStripMenuItem.Text = "Entry"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(208, 6)
        Me.ToolStripSeparator8.Visible = False
        '
        'ProductStockToolStripMenuItem
        '
        Me.ProductStockToolStripMenuItem.Name = "ProductStockToolStripMenuItem"
        Me.ProductStockToolStripMenuItem.Size = New System.Drawing.Size(211, 24)
        Me.ProductStockToolStripMenuItem.Text = "Product &Stock"
        '
        'ProductWarehouseToolStripMenuItem
        '
        Me.ProductWarehouseToolStripMenuItem.Name = "ProductWarehouseToolStripMenuItem"
        Me.ProductWarehouseToolStripMenuItem.Size = New System.Drawing.Size(211, 24)
        Me.ProductWarehouseToolStripMenuItem.Text = "Product &Warehouse"
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(208, 6)
        '
        'EndingStockToolStripMenuItem
        '
        Me.EndingStockToolStripMenuItem.Name = "EndingStockToolStripMenuItem"
        Me.EndingStockToolStripMenuItem.Size = New System.Drawing.Size(211, 24)
        Me.EndingStockToolStripMenuItem.Text = "&Ending Stock"
        '
        'StockCardToolStripMenuItem
        '
        Me.StockCardToolStripMenuItem.Name = "StockCardToolStripMenuItem"
        Me.StockCardToolStripMenuItem.Size = New System.Drawing.Size(211, 24)
        Me.StockCardToolStripMenuItem.Text = "Stock &Card"
        '
        'ToolStripSeparator10
        '
        Me.ToolStripSeparator10.Name = "ToolStripSeparator10"
        Me.ToolStripSeparator10.Size = New System.Drawing.Size(208, 6)
        Me.ToolStripSeparator10.Visible = False
        '
        'GroupingProductToolStripMenuItem
        '
        Me.GroupingProductToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListToolStripMenuItem4, Me.EntryToolStripMenuItem3})
        Me.GroupingProductToolStripMenuItem.Name = "GroupingProductToolStripMenuItem"
        Me.GroupingProductToolStripMenuItem.Size = New System.Drawing.Size(211, 24)
        Me.GroupingProductToolStripMenuItem.Text = "&Grouping Product"
        '
        'ListToolStripMenuItem4
        '
        Me.ListToolStripMenuItem4.Name = "ListToolStripMenuItem4"
        Me.ListToolStripMenuItem4.Size = New System.Drawing.Size(111, 24)
        Me.ListToolStripMenuItem4.Text = "List"
        '
        'EntryToolStripMenuItem3
        '
        Me.EntryToolStripMenuItem3.Name = "EntryToolStripMenuItem3"
        Me.EntryToolStripMenuItem3.Size = New System.Drawing.Size(111, 24)
        Me.EntryToolStripMenuItem3.Text = "Entry"
        '
        'ToolStripSeparator11
        '
        Me.ToolStripSeparator11.Name = "ToolStripSeparator11"
        Me.ToolStripSeparator11.Size = New System.Drawing.Size(208, 6)
        Me.ToolStripSeparator11.Visible = False
        '
        'SplitProductToolStripMenuItem
        '
        Me.SplitProductToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListToolStripMenuItem11, Me.EntryToolStripMenuItem5})
        Me.SplitProductToolStripMenuItem.Name = "SplitProductToolStripMenuItem"
        Me.SplitProductToolStripMenuItem.Size = New System.Drawing.Size(211, 24)
        Me.SplitProductToolStripMenuItem.Text = "Split Pr&oduct"
        '
        'ListToolStripMenuItem11
        '
        Me.ListToolStripMenuItem11.Name = "ListToolStripMenuItem11"
        Me.ListToolStripMenuItem11.Size = New System.Drawing.Size(111, 24)
        Me.ListToolStripMenuItem11.Text = "List"
        '
        'EntryToolStripMenuItem5
        '
        Me.EntryToolStripMenuItem5.Name = "EntryToolStripMenuItem5"
        Me.EntryToolStripMenuItem5.Size = New System.Drawing.Size(111, 24)
        Me.EntryToolStripMenuItem5.Text = "Entry"
        '
        'ReportToolStripMenuItem
        '
        Me.ReportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProductToolStripMenuItem1, Me.ToolStripSeparator26, Me.PurchaseToolStripMenuItem1, Me.SaleToolStripMenuItem2, Me.ToolStripSeparator27, Me.ActivePayableToolStripMenuItem1, Me.ActiveReceivableToolStripMenuItem1, Me.ToolStripSeparator28, Me.FinancialToolStripMenuItem})
        Me.ReportToolStripMenuItem.Name = "ReportToolStripMenuItem"
        Me.ReportToolStripMenuItem.Size = New System.Drawing.Size(66, 24)
        Me.ReportToolStripMenuItem.Text = "&Report"
        '
        'ProductToolStripMenuItem1
        '
        Me.ProductToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StockToolStripMenuItem1, Me.TransactionToolStripMenuItem1})
        Me.ProductToolStripMenuItem1.Name = "ProductToolStripMenuItem1"
        Me.ProductToolStripMenuItem1.Size = New System.Drawing.Size(195, 24)
        Me.ProductToolStripMenuItem1.Text = "&Product"
        '
        'StockToolStripMenuItem1
        '
        Me.StockToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StockPerProductToolStripMenuItem, Me.StockPerWarehouseToolStripMenuItem, Me.StockPerPriceToolStripMenuItem, Me.StockBalanceToolStripMenuItem})
        Me.StockToolStripMenuItem1.Name = "StockToolStripMenuItem1"
        Me.StockToolStripMenuItem1.Size = New System.Drawing.Size(154, 24)
        Me.StockToolStripMenuItem1.Text = "&Stock"
        '
        'StockPerProductToolStripMenuItem
        '
        Me.StockPerProductToolStripMenuItem.Name = "StockPerProductToolStripMenuItem"
        Me.StockPerProductToolStripMenuItem.Size = New System.Drawing.Size(218, 24)
        Me.StockPerProductToolStripMenuItem.Text = "Stock per &Product"
        '
        'StockPerWarehouseToolStripMenuItem
        '
        Me.StockPerWarehouseToolStripMenuItem.Name = "StockPerWarehouseToolStripMenuItem"
        Me.StockPerWarehouseToolStripMenuItem.Size = New System.Drawing.Size(218, 24)
        Me.StockPerWarehouseToolStripMenuItem.Text = "Stock per &Warehouse"
        '
        'StockPerPriceToolStripMenuItem
        '
        Me.StockPerPriceToolStripMenuItem.Name = "StockPerPriceToolStripMenuItem"
        Me.StockPerPriceToolStripMenuItem.Size = New System.Drawing.Size(218, 24)
        Me.StockPerPriceToolStripMenuItem.Text = "Stock per P&rice"
        '
        'StockBalanceToolStripMenuItem
        '
        Me.StockBalanceToolStripMenuItem.Name = "StockBalanceToolStripMenuItem"
        Me.StockBalanceToolStripMenuItem.Size = New System.Drawing.Size(218, 24)
        Me.StockBalanceToolStripMenuItem.Text = "Stock &Balance"
        '
        'TransactionToolStripMenuItem1
        '
        Me.TransactionToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StockCardToolStripMenuItem1, Me.ProductMovementToolStripMenuItem1, Me.SoldProductToolStripMenuItem1, Me.ProductDistributionToolStripMenuItem1})
        Me.TransactionToolStripMenuItem1.Name = "TransactionToolStripMenuItem1"
        Me.TransactionToolStripMenuItem1.Size = New System.Drawing.Size(154, 24)
        Me.TransactionToolStripMenuItem1.Text = "&Transaction"
        '
        'StockCardToolStripMenuItem1
        '
        Me.StockCardToolStripMenuItem1.Name = "StockCardToolStripMenuItem1"
        Me.StockCardToolStripMenuItem1.Size = New System.Drawing.Size(211, 24)
        Me.StockCardToolStripMenuItem1.Text = "Stock &Card"
        '
        'ProductMovementToolStripMenuItem1
        '
        Me.ProductMovementToolStripMenuItem1.Name = "ProductMovementToolStripMenuItem1"
        Me.ProductMovementToolStripMenuItem1.Size = New System.Drawing.Size(211, 24)
        Me.ProductMovementToolStripMenuItem1.Text = "Product &Movement"
        '
        'SoldProductToolStripMenuItem1
        '
        Me.SoldProductToolStripMenuItem1.Name = "SoldProductToolStripMenuItem1"
        Me.SoldProductToolStripMenuItem1.Size = New System.Drawing.Size(211, 24)
        Me.SoldProductToolStripMenuItem1.Text = "&Sold Product"
        '
        'ProductDistributionToolStripMenuItem1
        '
        Me.ProductDistributionToolStripMenuItem1.Name = "ProductDistributionToolStripMenuItem1"
        Me.ProductDistributionToolStripMenuItem1.Size = New System.Drawing.Size(211, 24)
        Me.ProductDistributionToolStripMenuItem1.Text = "Product &Distribution"
        '
        'ToolStripSeparator26
        '
        Me.ToolStripSeparator26.Name = "ToolStripSeparator26"
        Me.ToolStripSeparator26.Size = New System.Drawing.Size(192, 6)
        '
        'PurchaseToolStripMenuItem1
        '
        Me.PurchaseToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GeneralToolStripMenuItem1, Me.ByInvoiceToolStripMenuItem1, Me.ByVendorToolStripMenuItem, Me.ByPaymentMethodToolStripMenuItem1, Me.ByStatusToolStripMenuItem1, Me.ByPurchasingPriceToolStripMenuItem})
        Me.PurchaseToolStripMenuItem1.Name = "PurchaseToolStripMenuItem1"
        Me.PurchaseToolStripMenuItem1.Size = New System.Drawing.Size(195, 24)
        Me.PurchaseToolStripMenuItem1.Text = "&Purchase"
        '
        'GeneralToolStripMenuItem1
        '
        Me.GeneralToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DailyPurchaseToolStripMenuItem, Me.MonthlyPurchaseToolStripMenuItem})
        Me.GeneralToolStripMenuItem1.Name = "GeneralToolStripMenuItem1"
        Me.GeneralToolStripMenuItem1.Size = New System.Drawing.Size(211, 24)
        Me.GeneralToolStripMenuItem1.Text = "&General"
        '
        'DailyPurchaseToolStripMenuItem
        '
        Me.DailyPurchaseToolStripMenuItem.Name = "DailyPurchaseToolStripMenuItem"
        Me.DailyPurchaseToolStripMenuItem.Size = New System.Drawing.Size(194, 24)
        Me.DailyPurchaseToolStripMenuItem.Text = "Daily Purchase"
        '
        'MonthlyPurchaseToolStripMenuItem
        '
        Me.MonthlyPurchaseToolStripMenuItem.Name = "MonthlyPurchaseToolStripMenuItem"
        Me.MonthlyPurchaseToolStripMenuItem.Size = New System.Drawing.Size(194, 24)
        Me.MonthlyPurchaseToolStripMenuItem.Text = "Monthly Purchase"
        '
        'ByInvoiceToolStripMenuItem1
        '
        Me.ByInvoiceToolStripMenuItem1.Name = "ByInvoiceToolStripMenuItem1"
        Me.ByInvoiceToolStripMenuItem1.Size = New System.Drawing.Size(211, 24)
        Me.ByInvoiceToolStripMenuItem1.Text = "By &Invoice"
        '
        'ByVendorToolStripMenuItem
        '
        Me.ByVendorToolStripMenuItem.Name = "ByVendorToolStripMenuItem"
        Me.ByVendorToolStripMenuItem.Size = New System.Drawing.Size(211, 24)
        Me.ByVendorToolStripMenuItem.Text = "By &Vendor"
        '
        'ByPaymentMethodToolStripMenuItem1
        '
        Me.ByPaymentMethodToolStripMenuItem1.Name = "ByPaymentMethodToolStripMenuItem1"
        Me.ByPaymentMethodToolStripMenuItem1.Size = New System.Drawing.Size(211, 24)
        Me.ByPaymentMethodToolStripMenuItem1.Text = "By &Payment Method"
        '
        'ByStatusToolStripMenuItem1
        '
        Me.ByStatusToolStripMenuItem1.Name = "ByStatusToolStripMenuItem1"
        Me.ByStatusToolStripMenuItem1.Size = New System.Drawing.Size(211, 24)
        Me.ByStatusToolStripMenuItem1.Text = "By &Status"
        '
        'ByPurchasingPriceToolStripMenuItem
        '
        Me.ByPurchasingPriceToolStripMenuItem.Name = "ByPurchasingPriceToolStripMenuItem"
        Me.ByPurchasingPriceToolStripMenuItem.Size = New System.Drawing.Size(211, 24)
        Me.ByPurchasingPriceToolStripMenuItem.Text = "By P&urchasing Price"
        '
        'SaleToolStripMenuItem2
        '
        Me.SaleToolStripMenuItem2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GeneralToolStripMenuItem, Me.ByInvoiceToolStripMenuItem, Me.BySalesmanToolStripMenuItem, Me.ByCustomerToolStripMenuItem, Me.ByPaymentMethodToolStripMenuItem, Me.ByStatusToolStripMenuItem, Me.ByDailySoldProductToolStripMenuItem})
        Me.SaleToolStripMenuItem2.Name = "SaleToolStripMenuItem2"
        Me.SaleToolStripMenuItem2.Size = New System.Drawing.Size(195, 24)
        Me.SaleToolStripMenuItem2.Text = "&Sale"
        '
        'GeneralToolStripMenuItem
        '
        Me.GeneralToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DailySalesToolStripMenuItem1, Me.MonthlySalesToolStripMenuItem})
        Me.GeneralToolStripMenuItem.Name = "GeneralToolStripMenuItem"
        Me.GeneralToolStripMenuItem.Size = New System.Drawing.Size(221, 24)
        Me.GeneralToolStripMenuItem.Text = "&General"
        '
        'DailySalesToolStripMenuItem1
        '
        Me.DailySalesToolStripMenuItem1.Name = "DailySalesToolStripMenuItem1"
        Me.DailySalesToolStripMenuItem1.Size = New System.Drawing.Size(170, 24)
        Me.DailySalesToolStripMenuItem1.Text = "Daily Sales"
        '
        'MonthlySalesToolStripMenuItem
        '
        Me.MonthlySalesToolStripMenuItem.Name = "MonthlySalesToolStripMenuItem"
        Me.MonthlySalesToolStripMenuItem.Size = New System.Drawing.Size(170, 24)
        Me.MonthlySalesToolStripMenuItem.Text = "Monthly Sales"
        '
        'ByInvoiceToolStripMenuItem
        '
        Me.ByInvoiceToolStripMenuItem.Name = "ByInvoiceToolStripMenuItem"
        Me.ByInvoiceToolStripMenuItem.Size = New System.Drawing.Size(221, 24)
        Me.ByInvoiceToolStripMenuItem.Text = "By &Invoice"
        '
        'BySalesmanToolStripMenuItem
        '
        Me.BySalesmanToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PerProductToolStripMenuItem, Me.PerInvoiceToolStripMenuItem})
        Me.BySalesmanToolStripMenuItem.Name = "BySalesmanToolStripMenuItem"
        Me.BySalesmanToolStripMenuItem.Size = New System.Drawing.Size(221, 24)
        Me.BySalesmanToolStripMenuItem.Text = "By &Salesman"
        '
        'PerProductToolStripMenuItem
        '
        Me.PerProductToolStripMenuItem.Name = "PerProductToolStripMenuItem"
        Me.PerProductToolStripMenuItem.Size = New System.Drawing.Size(154, 24)
        Me.PerProductToolStripMenuItem.Text = "Per &Product"
        '
        'PerInvoiceToolStripMenuItem
        '
        Me.PerInvoiceToolStripMenuItem.Name = "PerInvoiceToolStripMenuItem"
        Me.PerInvoiceToolStripMenuItem.Size = New System.Drawing.Size(154, 24)
        Me.PerInvoiceToolStripMenuItem.Text = "Per &Invoice"
        '
        'ByCustomerToolStripMenuItem
        '
        Me.ByCustomerToolStripMenuItem.Name = "ByCustomerToolStripMenuItem"
        Me.ByCustomerToolStripMenuItem.Size = New System.Drawing.Size(221, 24)
        Me.ByCustomerToolStripMenuItem.Text = "By &Customer"
        '
        'ByPaymentMethodToolStripMenuItem
        '
        Me.ByPaymentMethodToolStripMenuItem.Name = "ByPaymentMethodToolStripMenuItem"
        Me.ByPaymentMethodToolStripMenuItem.Size = New System.Drawing.Size(221, 24)
        Me.ByPaymentMethodToolStripMenuItem.Text = "By Payment &Method"
        '
        'ByStatusToolStripMenuItem
        '
        Me.ByStatusToolStripMenuItem.Name = "ByStatusToolStripMenuItem"
        Me.ByStatusToolStripMenuItem.Size = New System.Drawing.Size(221, 24)
        Me.ByStatusToolStripMenuItem.Text = "By S&tatus"
        '
        'ByDailySoldProductToolStripMenuItem
        '
        Me.ByDailySoldProductToolStripMenuItem.Name = "ByDailySoldProductToolStripMenuItem"
        Me.ByDailySoldProductToolStripMenuItem.Size = New System.Drawing.Size(221, 24)
        Me.ByDailySoldProductToolStripMenuItem.Text = "By Daily Sold Product"
        '
        'ToolStripSeparator27
        '
        Me.ToolStripSeparator27.Name = "ToolStripSeparator27"
        Me.ToolStripSeparator27.Size = New System.Drawing.Size(192, 6)
        '
        'ActivePayableToolStripMenuItem1
        '
        Me.ActivePayableToolStripMenuItem1.Name = "ActivePayableToolStripMenuItem1"
        Me.ActivePayableToolStripMenuItem1.Size = New System.Drawing.Size(195, 24)
        Me.ActivePayableToolStripMenuItem1.Text = "Active Payable"
        '
        'ActiveReceivableToolStripMenuItem1
        '
        Me.ActiveReceivableToolStripMenuItem1.Name = "ActiveReceivableToolStripMenuItem1"
        Me.ActiveReceivableToolStripMenuItem1.Size = New System.Drawing.Size(195, 24)
        Me.ActiveReceivableToolStripMenuItem1.Text = "Active Receivable"
        '
        'ToolStripSeparator28
        '
        Me.ToolStripSeparator28.Name = "ToolStripSeparator28"
        Me.ToolStripSeparator28.Size = New System.Drawing.Size(192, 6)
        '
        'FinancialToolStripMenuItem
        '
        Me.FinancialToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BalanceSheetToolStripMenuItem1, Me.ProfitLossToolStripMenuItem})
        Me.FinancialToolStripMenuItem.Name = "FinancialToolStripMenuItem"
        Me.FinancialToolStripMenuItem.Size = New System.Drawing.Size(195, 24)
        Me.FinancialToolStripMenuItem.Text = "&Financial"
        '
        'BalanceSheetToolStripMenuItem1
        '
        Me.BalanceSheetToolStripMenuItem1.Name = "BalanceSheetToolStripMenuItem1"
        Me.BalanceSheetToolStripMenuItem1.Size = New System.Drawing.Size(171, 24)
        Me.BalanceSheetToolStripMenuItem1.Text = "Balance Sheet"
        '
        'ProfitLossToolStripMenuItem
        '
        Me.ProfitLossToolStripMenuItem.Name = "ProfitLossToolStripMenuItem"
        Me.ProfitLossToolStripMenuItem.Size = New System.Drawing.Size(171, 24)
        Me.ProfitLossToolStripMenuItem.Text = "Profit && Loss"
        '
        'ListToolStripMenuItem3
        '
        Me.ListToolStripMenuItem3.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SoldProductToolStripMenuItem, Me.SalesReportToolStripMenuItem, Me.ToolStripSeparator12, Me.DailyInvoiceToolStripMenuItem, Me.DailySalesToolStripMenuItem, Me.ToolStripSeparator13, Me.ProductSoldPerInvoiceToolStripMenuItem, Me.ToolStripSeparator24, Me.ActivePayableToolStripMenuItem, Me.ActiveReceivableToolStripMenuItem, Me.ToolStripSeparator15, Me.BalanceSheetToolStripMenuItem, Me.ToolStripSeparator23, Me.PurchasingPriceToolStripMenuItem, Me.ProductMovementToolStripMenuItem})
        Me.ListToolStripMenuItem3.Name = "ListToolStripMenuItem3"
        Me.ListToolStripMenuItem3.Size = New System.Drawing.Size(43, 24)
        Me.ListToolStripMenuItem3.Text = "&List"
        Me.ListToolStripMenuItem3.Visible = False
        '
        'SoldProductToolStripMenuItem
        '
        Me.SoldProductToolStripMenuItem.Name = "SoldProductToolStripMenuItem"
        Me.SoldProductToolStripMenuItem.Size = New System.Drawing.Size(204, 24)
        Me.SoldProductToolStripMenuItem.Text = "S&old Product"
        '
        'SalesReportToolStripMenuItem
        '
        Me.SalesReportToolStripMenuItem.Name = "SalesReportToolStripMenuItem"
        Me.SalesReportToolStripMenuItem.Size = New System.Drawing.Size(204, 24)
        Me.SalesReportToolStripMenuItem.Text = "S&ale Report"
        '
        'ToolStripSeparator12
        '
        Me.ToolStripSeparator12.Name = "ToolStripSeparator12"
        Me.ToolStripSeparator12.Size = New System.Drawing.Size(201, 6)
        '
        'DailyInvoiceToolStripMenuItem
        '
        Me.DailyInvoiceToolStripMenuItem.Name = "DailyInvoiceToolStripMenuItem"
        Me.DailyInvoiceToolStripMenuItem.Size = New System.Drawing.Size(204, 24)
        Me.DailyInvoiceToolStripMenuItem.Text = "Daily In&voice"
        '
        'DailySalesToolStripMenuItem
        '
        Me.DailySalesToolStripMenuItem.Name = "DailySalesToolStripMenuItem"
        Me.DailySalesToolStripMenuItem.Size = New System.Drawing.Size(204, 24)
        Me.DailySalesToolStripMenuItem.Text = "Daily &Sales"
        '
        'ToolStripSeparator13
        '
        Me.ToolStripSeparator13.Name = "ToolStripSeparator13"
        Me.ToolStripSeparator13.Size = New System.Drawing.Size(201, 6)
        '
        'ProductSoldPerInvoiceToolStripMenuItem
        '
        Me.ProductSoldPerInvoiceToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SalesPerInvoiceToolStripMenuItem, Me.SalesPerProductToolStripMenuItem})
        Me.ProductSoldPerInvoiceToolStripMenuItem.Name = "ProductSoldPerInvoiceToolStripMenuItem"
        Me.ProductSoldPerInvoiceToolStripMenuItem.Size = New System.Drawing.Size(204, 24)
        Me.ProductSoldPerInvoiceToolStripMenuItem.Text = "Sales by Salesman"
        '
        'SalesPerInvoiceToolStripMenuItem
        '
        Me.SalesPerInvoiceToolStripMenuItem.Name = "SalesPerInvoiceToolStripMenuItem"
        Me.SalesPerInvoiceToolStripMenuItem.Size = New System.Drawing.Size(193, 24)
        Me.SalesPerInvoiceToolStripMenuItem.Text = "Sales per Invoice"
        '
        'SalesPerProductToolStripMenuItem
        '
        Me.SalesPerProductToolStripMenuItem.Name = "SalesPerProductToolStripMenuItem"
        Me.SalesPerProductToolStripMenuItem.Size = New System.Drawing.Size(193, 24)
        Me.SalesPerProductToolStripMenuItem.Text = "Sales per Product"
        '
        'ToolStripSeparator24
        '
        Me.ToolStripSeparator24.Name = "ToolStripSeparator24"
        Me.ToolStripSeparator24.Size = New System.Drawing.Size(201, 6)
        '
        'ActivePayableToolStripMenuItem
        '
        Me.ActivePayableToolStripMenuItem.Name = "ActivePayableToolStripMenuItem"
        Me.ActivePayableToolStripMenuItem.Size = New System.Drawing.Size(204, 24)
        Me.ActivePayableToolStripMenuItem.Text = "Active &Payable"
        '
        'ActiveReceivableToolStripMenuItem
        '
        Me.ActiveReceivableToolStripMenuItem.Name = "ActiveReceivableToolStripMenuItem"
        Me.ActiveReceivableToolStripMenuItem.Size = New System.Drawing.Size(204, 24)
        Me.ActiveReceivableToolStripMenuItem.Text = "Active &Receivable"
        '
        'ToolStripSeparator15
        '
        Me.ToolStripSeparator15.Name = "ToolStripSeparator15"
        Me.ToolStripSeparator15.Size = New System.Drawing.Size(201, 6)
        '
        'BalanceSheetToolStripMenuItem
        '
        Me.BalanceSheetToolStripMenuItem.Name = "BalanceSheetToolStripMenuItem"
        Me.BalanceSheetToolStripMenuItem.Size = New System.Drawing.Size(204, 24)
        Me.BalanceSheetToolStripMenuItem.Text = "Stock &Balance"
        '
        'ToolStripSeparator23
        '
        Me.ToolStripSeparator23.Name = "ToolStripSeparator23"
        Me.ToolStripSeparator23.Size = New System.Drawing.Size(201, 6)
        '
        'PurchasingPriceToolStripMenuItem
        '
        Me.PurchasingPriceToolStripMenuItem.Name = "PurchasingPriceToolStripMenuItem"
        Me.PurchasingPriceToolStripMenuItem.Size = New System.Drawing.Size(204, 24)
        Me.PurchasingPriceToolStripMenuItem.Text = "Purc&hasing Price"
        '
        'ProductMovementToolStripMenuItem
        '
        Me.ProductMovementToolStripMenuItem.Name = "ProductMovementToolStripMenuItem"
        Me.ProductMovementToolStripMenuItem.Size = New System.Drawing.Size(204, 24)
        Me.ProductMovementToolStripMenuItem.Text = "Product Movement"
        '
        'DataManagerToolStripMenuItem
        '
        Me.DataManagerToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BackupDatabaseToolStripMenuItem, Me.RestoreDatabaseToolStripMenuItem, Me.ToolStripSeparator14, Me.ClosingPeriodToolStripMenuItem, Me.ToolStripSeparator16, Me.ConfigurationToolStripMenuItem1})
        Me.DataManagerToolStripMenuItem.Name = "DataManagerToolStripMenuItem"
        Me.DataManagerToolStripMenuItem.Size = New System.Drawing.Size(116, 24)
        Me.DataManagerToolStripMenuItem.Text = "&Data Manager"
        '
        'BackupDatabaseToolStripMenuItem
        '
        Me.BackupDatabaseToolStripMenuItem.Name = "BackupDatabaseToolStripMenuItem"
        Me.BackupDatabaseToolStripMenuItem.Size = New System.Drawing.Size(329, 24)
        Me.BackupDatabaseToolStripMenuItem.Text = "&Backup, Delete Dan Restore Database"
        '
        'RestoreDatabaseToolStripMenuItem
        '
        Me.RestoreDatabaseToolStripMenuItem.Name = "RestoreDatabaseToolStripMenuItem"
        Me.RestoreDatabaseToolStripMenuItem.Size = New System.Drawing.Size(329, 24)
        Me.RestoreDatabaseToolStripMenuItem.Text = "&Restore Database"
        Me.RestoreDatabaseToolStripMenuItem.Visible = False
        '
        'ToolStripSeparator14
        '
        Me.ToolStripSeparator14.Name = "ToolStripSeparator14"
        Me.ToolStripSeparator14.Size = New System.Drawing.Size(326, 6)
        '
        'ClosingPeriodToolStripMenuItem
        '
        Me.ClosingPeriodToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MonthlyToolStripMenuItem, Me.YearlyToolStripMenuItem})
        Me.ClosingPeriodToolStripMenuItem.Name = "ClosingPeriodToolStripMenuItem"
        Me.ClosingPeriodToolStripMenuItem.Size = New System.Drawing.Size(329, 24)
        Me.ClosingPeriodToolStripMenuItem.Text = "Closing &Period"
        '
        'MonthlyToolStripMenuItem
        '
        Me.MonthlyToolStripMenuItem.Name = "MonthlyToolStripMenuItem"
        Me.MonthlyToolStripMenuItem.Size = New System.Drawing.Size(132, 24)
        Me.MonthlyToolStripMenuItem.Text = "Monthly"
        '
        'YearlyToolStripMenuItem
        '
        Me.YearlyToolStripMenuItem.Name = "YearlyToolStripMenuItem"
        Me.YearlyToolStripMenuItem.Size = New System.Drawing.Size(132, 24)
        Me.YearlyToolStripMenuItem.Text = "Yearly"
        '
        'ToolStripSeparator16
        '
        Me.ToolStripSeparator16.Name = "ToolStripSeparator16"
        Me.ToolStripSeparator16.Size = New System.Drawing.Size(326, 6)
        '
        'ConfigurationToolStripMenuItem1
        '
        Me.ConfigurationToolStripMenuItem1.Name = "ConfigurationToolStripMenuItem1"
        Me.ConfigurationToolStripMenuItem1.Size = New System.Drawing.Size(329, 24)
        Me.ConfigurationToolStripMenuItem1.Text = "&Configuration"
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'ToolStrip1
        '
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PurchaseButton1, Me.toolStripSeparator17, Me.SaleButton1, Me.ToolStripSeparator18, Me.StockButton1, Me.ToolStripSeparator19, Me.InvoiceButton1, Me.ToolStripSeparator20, Me.StockCardButton1, Me.ToolStripSeparator21, Me.WarehouseButton1, Me.ToolStripSeparator22, Me.ConfigButton1})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 28)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.ToolStrip1.Size = New System.Drawing.Size(906, 31)
        Me.ToolStrip1.TabIndex = 32
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'PurchaseButton1
        '
        Me.PurchaseButton1.BackColor = System.Drawing.SystemColors.Control
        Me.PurchaseButton1.Image = CType(resources.GetObject("PurchaseButton1.Image"), System.Drawing.Image)
        Me.PurchaseButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.PurchaseButton1.Name = "PurchaseButton1"
        Me.PurchaseButton1.Size = New System.Drawing.Size(95, 28)
        Me.PurchaseButton1.Text = "Purchase"
        Me.PurchaseButton1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.PurchaseButton1.ToolTipText = "Entry New Purchase"
        '
        'toolStripSeparator17
        '
        Me.toolStripSeparator17.Name = "toolStripSeparator17"
        Me.toolStripSeparator17.Size = New System.Drawing.Size(6, 31)
        '
        'SaleButton1
        '
        Me.SaleButton1.Image = CType(resources.GetObject("SaleButton1.Image"), System.Drawing.Image)
        Me.SaleButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.SaleButton1.Name = "SaleButton1"
        Me.SaleButton1.Size = New System.Drawing.Size(65, 28)
        Me.SaleButton1.Text = "Sale"
        Me.SaleButton1.ToolTipText = "Entry New Sale"
        '
        'ToolStripSeparator18
        '
        Me.ToolStripSeparator18.Name = "ToolStripSeparator18"
        Me.ToolStripSeparator18.Size = New System.Drawing.Size(6, 31)
        '
        'StockButton1
        '
        Me.StockButton1.Image = CType(resources.GetObject("StockButton1.Image"), System.Drawing.Image)
        Me.StockButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.StockButton1.Name = "StockButton1"
        Me.StockButton1.Size = New System.Drawing.Size(73, 28)
        Me.StockButton1.Text = "Stock"
        Me.StockButton1.ToolTipText = "List of Product Stock"
        '
        'ToolStripSeparator19
        '
        Me.ToolStripSeparator19.Name = "ToolStripSeparator19"
        Me.ToolStripSeparator19.Size = New System.Drawing.Size(6, 31)
        '
        'InvoiceButton1
        '
        Me.InvoiceButton1.Image = CType(resources.GetObject("InvoiceButton1.Image"), System.Drawing.Image)
        Me.InvoiceButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.InvoiceButton1.Name = "InvoiceButton1"
        Me.InvoiceButton1.Size = New System.Drawing.Size(84, 28)
        Me.InvoiceButton1.Text = "Invoice"
        Me.InvoiceButton1.ToolTipText = "List of Daily Invoice"
        '
        'ToolStripSeparator20
        '
        Me.ToolStripSeparator20.Name = "ToolStripSeparator20"
        Me.ToolStripSeparator20.Size = New System.Drawing.Size(6, 31)
        '
        'StockCardButton1
        '
        Me.StockCardButton1.Image = CType(resources.GetObject("StockCardButton1.Image"), System.Drawing.Image)
        Me.StockCardButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.StockCardButton1.Name = "StockCardButton1"
        Me.StockCardButton1.Size = New System.Drawing.Size(108, 28)
        Me.StockCardButton1.Text = "Stock Card"
        Me.StockCardButton1.ToolTipText = "List of Stock Card"
        '
        'ToolStripSeparator21
        '
        Me.ToolStripSeparator21.Name = "ToolStripSeparator21"
        Me.ToolStripSeparator21.Size = New System.Drawing.Size(6, 31)
        '
        'WarehouseButton1
        '
        Me.WarehouseButton1.Image = CType(resources.GetObject("WarehouseButton1.Image"), System.Drawing.Image)
        Me.WarehouseButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.WarehouseButton1.Name = "WarehouseButton1"
        Me.WarehouseButton1.Size = New System.Drawing.Size(111, 28)
        Me.WarehouseButton1.Text = "Warehouse"
        Me.WarehouseButton1.ToolTipText = "List of Product Warehouse"
        '
        'ToolStripSeparator22
        '
        Me.ToolStripSeparator22.Name = "ToolStripSeparator22"
        Me.ToolStripSeparator22.Size = New System.Drawing.Size(6, 31)
        '
        'ConfigButton1
        '
        Me.ConfigButton1.Image = CType(resources.GetObject("ConfigButton1.Image"), System.Drawing.Image)
        Me.ConfigButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ConfigButton1.Name = "ConfigButton1"
        Me.ConfigButton1.Size = New System.Drawing.Size(81, 28)
        Me.ConfigButton1.Text = "Config"
        Me.ConfigButton1.ToolTipText = "Configuration Menu"
        '
        'TimerSafeStock
        '
        Me.TimerSafeStock.Enabled = True
        Me.TimerSafeStock.Interval = 1000
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.BackgroundImage = Global.DMI_RETAIL.My.Resources.Resources.Background___White
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(906, 610)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DMI Retail"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.USERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PARAMETERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_NOTIFICATION_SAFE_STOCKBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents menuMasterData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CustomerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VendorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WarehouseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LocationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TransactionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpeningBalanceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PurchaseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaleToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdjustmentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StockToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductStockToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductWarehouseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductDistributionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EndingStockToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StockCardToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SoldProductToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalesReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DailyInvoiceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DailySalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActivePayableToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActiveReceivableToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataManagerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BackupDatabaseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RestoreDatabaseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClosingPeriodToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MonthlyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents YearlyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusLabelUser As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusLabelDatetime As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusLabelNotif As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents BalanceSheetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupingProductToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListToolStripMenuItem4 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ProductDistributionListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductDistributionEntryToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PayableToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReceivableToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListToolStripMenuItem5 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReceivableEntryToolStripMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SplitProductToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListToolStripMenuItem11 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryToolStripMenuItem5 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryToolStripMenuItem6 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListToolStripMenuItem6 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListToolStripMenuItem7 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryToolStripMenuItem7 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReturnPurchaseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReturnSaleToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListToolStripMenuItem8 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryToolStripMenuItem8 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListToolStripMenuItem9 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryToolStripMenuItem9 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReferenceDataToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PaymentMethodToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CompanyInformationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UserToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryUserToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NotepadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CalculatorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RecentFormToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LogOutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CashRegisterToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PayableTransactionToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReceivableTransactionToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ConfigurationToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator12 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator13 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator15 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator14 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator16 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents PurchaseButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents SaleButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents RecentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StockButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolStripSeparator17 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator18 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator19 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents InvoiceButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator20 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ConfigButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator21 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents StockCardButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents WarehouseButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator22 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents USERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AccessPrivilegeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListToolStripMenuItem10 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryProductToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListCustomerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryCustomerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListVendorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryVendorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalesmanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListWarehouseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryWarehouseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListAccountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryAccountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListLocationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryLocationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListSalesmanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntrySalesmanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PurchasingPriceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator23 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ProductMovementToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListToolStripMenuItem12 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryToolStripMenuItem4 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListToolStripMenuItem13 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryToolStripMenuItem10 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductSoldPerInvoiceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalesPerInvoiceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalesPerProductToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator24 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ListPaymentMethodToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryPaymentMethodToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ChangePasswordToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator25 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ListUserToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StockToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StockPerProductToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StockPerWarehouseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StockPerPriceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TransactionToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StockCardToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductMovementToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SoldProductToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductDistributionToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaleToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PurchaseToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FinancialToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ByInvoiceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BySalesmanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PerProductToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PerInvoiceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ByCustomerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ByPaymentMethodToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ByStatusToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GeneralToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GeneralToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ByInvoiceToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ByVendorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ByPaymentMethodToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ByStatusToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BalanceSheetToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProfitLossToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TimerSafeStock As System.Windows.Forms.Timer
    Friend WithEvents PARAMETERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_NOTIFICATION_SAFE_STOCKBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ToolStripSeparator26 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator27 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ActivePayableToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActiveReceivableToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator28 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ByPurchasingPriceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StockBalanceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ByDailySoldProductToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DailySalesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MonthlySalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DailyPurchaseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MonthlyPurchaseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaleOrderToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListToolStripMenuItem14 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryToolStripMenuItem11 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeliveryOrderToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListToolStripMenuItem15 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryToolStripMenuItem12 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PurchaseOrderToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListToolStripMenuItem16 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryToolStripMenuItem13 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator29 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator30 As System.Windows.Forms.ToolStripSeparator
End Class
