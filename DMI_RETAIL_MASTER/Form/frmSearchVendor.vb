﻿Imports System.Data.SqlClient

Public Class frmSearchVendor
    Public VENDORBindingSource As New BindingSource

    Private Sub GetData()
        sql = "SELECT VENDOR_ID, VENDOR_CODE, VENDOR_NAME, VENDOR_ADDRESS, VENDOR_PHONE, VENDOR_CITY  FROM VENDOR"
        sqladapter = New SqlDataAdapter(sql, xConn)
        dt = New DataTable
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        VENDORBindingSource.DataSource = dt
        VENDORBindingNavigator.BindingSource = VENDORBindingSource
        dvgSearchVendor.DataSource = VENDORBindingSource
        dvgSearchVendor.Columns("VENDOR_ID").Visible = False
    End Sub

    Private Sub frmSearchVendor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_VENDOR.VENDOR' table. You can move, or remove it, as needed.
        'Me.VENDORTableAdapter.Fill(Me.DS_VENDOR.VENDOR)
        txtVendor.Text = ""
        txtVendor.Focus()
        getdata()
        tmpSearchResult = ""
        If tmpSearchMode = "Vendor Name" Then
            If Language = "Indonesian" Then
                lblVendor.Text = "Nama Supplier"
            Else
                lblVendor.Text = "Vendor Name"
            End If
        ElseIf tmpSearchMode = "Vendor Code" Then
            If Language = "Indonesian" Then
                lblVendor.Text = "Kode Supplier"
            Else
                lblVendor.Text = "Vendor Code"
            End If
        End If

        If Language = "Indonesian" Then
            dvgSearchVendor.Columns("VENDOR_CODE").HeaderText = "Kode Supplier"
            dvgSearchVendor.Columns("VENDOR_NAME").HeaderText = "Nama"
            dvgSearchVendor.Columns("VENDOR_ADDRESS").HeaderText = "Alamat"
            dvgSearchVendor.Columns("VENDOR_CITY").HeaderText = "Kota"
            dvgSearchVendor.Columns("VENDOR_PHONE").HeaderText = "Telp"
        Else
            dvgSearchVendor.Columns("VENDOR_CODE").HeaderText = "Vendor Code"
            dvgSearchVendor.Columns("VENDOR_NAME").HeaderText = "Vendor Name"
            dvgSearchVendor.Columns("VENDOR_ADDRESS").HeaderText = "Address"
            dvgSearchVendor.Columns("VENDOR_CITY").HeaderText = "City"
            dvgSearchVendor.Columns("VENDOR_PHONE").HeaderText = "Phone"
        End If
    End Sub

    Private Sub dvgSearchVendor_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dvgSearchVendor.DoubleClick
        Try
            tmpSearchResult = VENDORBindingSource.Current("VENDOR_ID")
        Catch
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data Supplier !" & vbCrLf & _
                    "Silahkan input setidaknya satu data supplier !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no data Vendor !" & vbCrLf & _
                       "Please entry at least one Vendor data !", MsgBoxStyle.Critical, "DMI Retail")
            End If
        End Try
        Me.Close()
    End Sub

    Private Sub dvgSearchVendor_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dvgSearchVendor.KeyDown
        If e.KeyCode = Keys.Enter Then
            Try
                tmpSearchResult = VENDORBindingSource.Current("VENDOR_ID")
            Catch
                If Language = "Indonesian" Then
                    MsgBox("Tidak ada satupun data supplier !" & vbCrLf &
                            "Silahkan input setidaknya satu data supplier !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("There is no data Vendor !" & vbCrLf &
                        "Please entry at least one Vendor data !", MsgBoxStyle.Critical, "DMI Retail")
                End If
            End Try
            Me.Close()
        ElseIf e.KeyCode = Keys.Escape Then
            tmpSearchResult = ""
            Me.Close()
        End If
    End Sub

    Private Sub txtVendor_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtVendor.TextChanged
        Try
            If tmpSearchMode = "Vendor Name" Then
                VENDORBindingSource.Filter = "VENDOR_NAME LIKE '%" & txtVendor.Text.ToUpper & "%'"
            ElseIf tmpSearchMode = "Vendor Code" Then
                VENDORBindingSource.Filter = "VENDOR_CODE LIKE '%" & txtVendor.Text.ToUpper & "%'"
            End If
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda memasukan huruf yang salah !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong input character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtVendor.Text = ""
            txtVendor.Focus()
        End Try
        txtVendor.Focus()
    End Sub
End Class