﻿Public Class frmListSalesmanProduct

    Public tmpSalesmanId, tmpTotal As Integer
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    Private Sub frmListSalesmanProduct_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F12 Then
            cmdReport_Click(Nothing, Nothing)
        End If
    End Sub


    Private Sub frmListSalesmanProduct_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_SALE.SALESMAN' table. You can move, or remove it, as needed.
        Me.SALESMANTableAdapter.Fill(Me.DS_SALE.SALESMAN)

        If SALESMANBindingSource.Count = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data Sales." & vbCrLf & "Masukkan setidaknya satu Sales !", _
                       MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Salesman." & vbCrLf & "Please input at least one Salesman !", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
        End If


        dtpPeriod1.Value = DateSerial(Today.Year, Today.Month, 1)
        dtpPeriod2.Value = DateSerial(Today.Year, Today.Month + 1, 0)

        For n As Integer = 0 To dgvSalesmanProduct.RowCount - 1
            tmpTotal = tmpTotal + CInt(dgvSalesmanProduct.Item(2, n).Value)
        Next

        If Language = "Indonesian" Then
            Me.Text = "Daftar Penjualan Sales per Faktur"
            cmdReport.Text = "Cetak"
            cmdGenerate.Text = "Proses"
            lblSalesman.Text = "Nama Sales"
            lblFrom.Text = "Periode"

            dgvSalesmanProduct.Columns("SALESMAN_NAME").HeaderText = "Nama Sales"
            dgvSalesmanProduct.Columns("PRODUCT_NAME").HeaderText = "Nama Product"
            dgvSalesmanProduct.Columns("TOTAL").HeaderText = "Total Terjual"

            Label1.Text = "Jumlah Item : " & dgvSalesmanProduct.RowCount
            Label2.Text = "Total Terjual : " & FormatNumber(tmpTotal, 0)
        Else
            Label1.Text = "No of Item(s) : " & dgvSalesmanProduct.RowCount
            Label2.Text = "Total Sold : " & FormatNumber(tmpTotal, 0)
        End If

        cmbSalesmanCode.Text = ""
    End Sub

    Private Sub cmbSalesmanCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSalesmanCode.TextChanged
        SALESMANTableAdapter.SP_GET_SALESMAN_NAME(cmbSalesmanCode.Text, txtSalesmanName.Text)
    End Sub

    Private Sub cmdSearchName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearchName.Click
        mdlGeneral.tmpSearchMode = "Salesman Name"
        frmSearchSalesman.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        SALESMANBindingSource.Position = SALESMANBindingSource.Find("SALESMAN_ID", tmpSearchResult)
        cmbSalesmanCode.Text = SALESMANBindingSource.Current("SALESMAN_CODE")
    End Sub

    Private Sub cmdGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click

        If SALESMANBindingSource.Count = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data Sales." & vbCrLf & "Form ini akan ditutup !", _
                       MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Salesman." & vbCrLf & "This form will be closed !", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Me.Close()
            Exit Sub
        End If


        If Not ValidateAllComboBox(Me) Then
            If Language = "Indonesian" Then
                MsgBox("Anda memasukkan nilai yang tidak terdaftar.", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You input value which is not in the list.", MsgBoxStyle.Critical, "DMI Retail")
            End If
            cmbSalesmanCode.Focus()
            Exit Sub
        End If

        Cursor = Cursors.WaitCursor
        tmpSalesmanId = IIf(cmbSalesmanCode.Text.Length = 0, 0, SALESMANBindingSource.Current("SALESMAN_ID"))

        SP_LIST_SALESMAN_PRODUCTTableAdapter.Fill(Me.DS_SALE.SP_LIST_SALESMAN_PRODUCT, tmpSalesmanId, dtpPeriod1.Value, dtpPeriod2.Value)
        Cursor = Cursors.Default

        tmpTotal = 0

        For n As Integer = 0 To dgvSalesmanProduct.RowCount - 1
            tmpTotal = tmpTotal + CInt(dgvSalesmanProduct.Item(2, n).Value)
        Next

        If Language = "Indonesian" Then
            Label1.Text = "Jumlah Item : " & dgvSalesmanProduct.RowCount
            Label2.Text = "Total Terjual : " & FormatNumber(tmpTotal, 0)
        Else
            Label1.Text = "No of Item(s) : " & dgvSalesmanProduct.RowCount
            Label2.Text = "Total Sold : " & FormatNumber(tmpTotal, 0)
        End If
    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        If SALESMANBindingSource.Count = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data Sales." & vbCrLf & "Form ini akan ditutup !", _
                       MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Salesman." & vbCrLf & "This form will be closed !", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Me.Close()
            Exit Sub
        End If

        Dim RPV As New frmRepSalesmanProduct
        RPV.date1 = dtpPeriod1.Value
        RPV.date2 = dtpPeriod2.Value
        RPV.tmpSalesmanId = tmpSalesmanId
        RPV.ReportViewer1.ZoomPercent = 100
        RPV.WindowState = FormWindowState.Maximized
        RPV.Show()
    End Sub
End Class