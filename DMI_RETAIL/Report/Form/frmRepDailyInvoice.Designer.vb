﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRepDailyInvoice
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource2 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRepDailyInvoice))
        Me.SP_LIST_DAILY_INVOICEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DB_DMI_RETAILDataSet1 = New DMI_RETAIL.DB_DMI_RETAILDataSet1()
        Me.COMPANY_INFORMATIONBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.SP_LIST_DAILY_INVOICETableAdapter = New DMI_RETAIL.DB_DMI_RETAILDataSet1TableAdapters.SP_LIST_DAILY_INVOICETableAdapter()
        Me.COMPANY_INFORMATIONTableAdapter = New DMI_RETAIL.DB_DMI_RETAILDataSet1TableAdapters.COMPANY_INFORMATIONTableAdapter()
        CType(Me.SP_LIST_DAILY_INVOICEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DB_DMI_RETAILDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.COMPANY_INFORMATIONBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SP_LIST_DAILY_INVOICEBindingSource
        '
        Me.SP_LIST_DAILY_INVOICEBindingSource.DataMember = "SP_LIST_DAILY_INVOICE"
        Me.SP_LIST_DAILY_INVOICEBindingSource.DataSource = Me.DB_DMI_RETAILDataSet1
        '
        'DB_DMI_RETAILDataSet1
        '
        Me.DB_DMI_RETAILDataSet1.DataSetName = "DB_DMI_RETAILDataSet1"
        Me.DB_DMI_RETAILDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'COMPANY_INFORMATIONBindingSource
        '
        Me.COMPANY_INFORMATIONBindingSource.DataMember = "COMPANY_INFORMATION"
        Me.COMPANY_INFORMATIONBindingSource.DataSource = Me.DB_DMI_RETAILDataSet1
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "DS_RPT_DAILYINVOICE"
        ReportDataSource1.Value = Me.SP_LIST_DAILY_INVOICEBindingSource
        ReportDataSource2.Name = "DS_RPT_COMPANY_INFO"
        ReportDataSource2.Value = Me.COMPANY_INFORMATIONBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource2)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "DMI_RETAIL.repDailyInvoice.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(457, 324)
        Me.ReportViewer1.TabIndex = 0
        '
        'SP_LIST_DAILY_INVOICETableAdapter
        '
        Me.SP_LIST_DAILY_INVOICETableAdapter.ClearBeforeFill = True
        '
        'COMPANY_INFORMATIONTableAdapter
        '
        Me.COMPANY_INFORMATIONTableAdapter.ClearBeforeFill = True
        '
        'frmRepDailyInvoice
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(457, 324)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmRepDailyInvoice"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Report Daily Invoice"
        CType(Me.SP_LIST_DAILY_INVOICEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DB_DMI_RETAILDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.COMPANY_INFORMATIONBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents SP_LIST_DAILY_INVOICEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DB_DMI_RETAILDataSet1 As DMI_RETAIL.DB_DMI_RETAILDataSet1
    Friend WithEvents COMPANY_INFORMATIONBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_LIST_DAILY_INVOICETableAdapter As DMI_RETAIL.DB_DMI_RETAILDataSet1TableAdapters.SP_LIST_DAILY_INVOICETableAdapter
    Friend WithEvents COMPANY_INFORMATIONTableAdapter As DMI_RETAIL.DB_DMI_RETAILDataSet1TableAdapters.COMPANY_INFORMATIONTableAdapter
End Class
