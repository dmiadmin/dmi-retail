﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListDailyInvoice
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListDailyInvoice))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmdReport = New System.Windows.Forms.Button()
        Me.dtpInvoice = New System.Windows.Forms.DateTimePicker()
        Me.lblPeriod = New System.Windows.Forms.Label()
        Me.cmdGenerate = New System.Windows.Forms.Button()
        Me.SP_LIST_DAILY_INVOICEDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RECEIPT_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRODUCT_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRICE_PER_UNIT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DISCOUNT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QUANTITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VOID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SP_LIST_DAILY_INVOICEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblSumTrans = New System.Windows.Forms.Label()
        Me.txtProductName = New System.Windows.Forms.TextBox()
        Me.lblProductName = New System.Windows.Forms.Label()
        Me.lblTotalInvoice = New System.Windows.Forms.Label()
        Me.lblTotalPrice = New System.Windows.Forms.Label()
        Me.lblVoidInvoice = New System.Windows.Forms.Label()
        Me.lblVoidPrice = New System.Windows.Forms.Label()
        Me.lblQuantity = New System.Windows.Forms.Label()
        Me.lblVoidQuantity = New System.Windows.Forms.Label()
        Me.lblAllTrans = New System.Windows.Forms.Label()
        Me.lblActiveTrans = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.SP_LIST_DAILY_INVOICEDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_LIST_DAILY_INVOICEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmdReport)
        Me.GroupBox1.Controls.Add(Me.dtpInvoice)
        Me.GroupBox1.Controls.Add(Me.lblPeriod)
        Me.GroupBox1.Controls.Add(Me.cmdGenerate)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(666, 99)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'cmdReport
        '
        Me.cmdReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdReport.Image = Global.DMI_RETAIL.My.Resources.Resources.Printer_small
        Me.cmdReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdReport.Location = New System.Drawing.Point(570, 69)
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(90, 24)
        Me.cmdReport.TabIndex = 12
        Me.cmdReport.Text = "Print [F12]"
        Me.cmdReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.cmdReport.UseVisualStyleBackColor = True
        '
        'dtpInvoice
        '
        Me.dtpInvoice.CustomFormat = "dd - MMM - yyyy"
        Me.dtpInvoice.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpInvoice.Location = New System.Drawing.Point(300, 19)
        Me.dtpInvoice.Name = "dtpInvoice"
        Me.dtpInvoice.Size = New System.Drawing.Size(141, 22)
        Me.dtpInvoice.TabIndex = 1
        '
        'lblPeriod
        '
        Me.lblPeriod.AutoSize = True
        Me.lblPeriod.Location = New System.Drawing.Point(219, 22)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(77, 15)
        Me.lblPeriod.TabIndex = 11
        Me.lblPeriod.Text = "Time Period"
        '
        'cmdGenerate
        '
        Me.cmdGenerate.Image = CType(resources.GetObject("cmdGenerate.Image"), System.Drawing.Image)
        Me.cmdGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdGenerate.Location = New System.Drawing.Point(284, 56)
        Me.cmdGenerate.Name = "cmdGenerate"
        Me.cmdGenerate.Size = New System.Drawing.Size(99, 33)
        Me.cmdGenerate.TabIndex = 2
        Me.cmdGenerate.Text = "Generate"
        Me.cmdGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdGenerate.UseVisualStyleBackColor = True
        '
        'SP_LIST_DAILY_INVOICEDataGridView
        '
        Me.SP_LIST_DAILY_INVOICEDataGridView.AllowUserToAddRows = False
        Me.SP_LIST_DAILY_INVOICEDataGridView.AllowUserToDeleteRows = False
        Me.SP_LIST_DAILY_INVOICEDataGridView.AutoGenerateColumns = False
        Me.SP_LIST_DAILY_INVOICEDataGridView.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SP_LIST_DAILY_INVOICEDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.SP_LIST_DAILY_INVOICEDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.SP_LIST_DAILY_INVOICEDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.RECEIPT_NO, Me.PRODUCT_NAME, Me.PRICE_PER_UNIT, Me.DISCOUNT, Me.TOTAL, Me.QUANTITY, Me.VOID})
        Me.SP_LIST_DAILY_INVOICEDataGridView.DataSource = Me.SP_LIST_DAILY_INVOICEBindingSource
        Me.SP_LIST_DAILY_INVOICEDataGridView.Location = New System.Drawing.Point(6, 56)
        Me.SP_LIST_DAILY_INVOICEDataGridView.Name = "SP_LIST_DAILY_INVOICEDataGridView"
        Me.SP_LIST_DAILY_INVOICEDataGridView.ReadOnly = True
        Me.SP_LIST_DAILY_INVOICEDataGridView.RowHeadersWidth = 19
        Me.SP_LIST_DAILY_INVOICEDataGridView.Size = New System.Drawing.Size(654, 355)
        Me.SP_LIST_DAILY_INVOICEDataGridView.TabIndex = 3
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "SALE_ID"
        Me.DataGridViewTextBoxColumn1.HeaderText = "SALE_ID"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'RECEIPT_NO
        '
        Me.RECEIPT_NO.DataPropertyName = "RECEIPT_NO"
        Me.RECEIPT_NO.HeaderText = "Invoice No."
        Me.RECEIPT_NO.Name = "RECEIPT_NO"
        Me.RECEIPT_NO.ReadOnly = True
        Me.RECEIPT_NO.Width = 90
        '
        'PRODUCT_NAME
        '
        Me.PRODUCT_NAME.DataPropertyName = "PRODUCT_NAME"
        Me.PRODUCT_NAME.HeaderText = "Product Name"
        Me.PRODUCT_NAME.Name = "PRODUCT_NAME"
        Me.PRODUCT_NAME.ReadOnly = True
        Me.PRODUCT_NAME.Width = 135
        '
        'PRICE_PER_UNIT
        '
        Me.PRICE_PER_UNIT.DataPropertyName = "PRICE_PER_UNIT"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = "0"
        Me.PRICE_PER_UNIT.DefaultCellStyle = DataGridViewCellStyle2
        Me.PRICE_PER_UNIT.HeaderText = "Selling Price"
        Me.PRICE_PER_UNIT.Name = "PRICE_PER_UNIT"
        Me.PRICE_PER_UNIT.ReadOnly = True
        Me.PRICE_PER_UNIT.Width = 85
        '
        'DISCOUNT
        '
        Me.DISCOUNT.DataPropertyName = "DISCOUNT"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N0"
        DataGridViewCellStyle3.NullValue = "0"
        Me.DISCOUNT.DefaultCellStyle = DataGridViewCellStyle3
        Me.DISCOUNT.HeaderText = "Total Disc."
        Me.DISCOUNT.Name = "DISCOUNT"
        Me.DISCOUNT.ReadOnly = True
        Me.DISCOUNT.Width = 75
        '
        'TOTAL
        '
        Me.TOTAL.DataPropertyName = "TOTAL"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.TOTAL.DefaultCellStyle = DataGridViewCellStyle4
        Me.TOTAL.HeaderText = "Sold Price"
        Me.TOTAL.Name = "TOTAL"
        Me.TOTAL.ReadOnly = True
        Me.TOTAL.Width = 85
        '
        'QUANTITY
        '
        Me.QUANTITY.DataPropertyName = "QUANTITY"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.QUANTITY.DefaultCellStyle = DataGridViewCellStyle5
        Me.QUANTITY.HeaderText = "Qty."
        Me.QUANTITY.Name = "QUANTITY"
        Me.QUANTITY.ReadOnly = True
        Me.QUANTITY.Width = 55
        '
        'VOID
        '
        Me.VOID.DataPropertyName = "VOID"
        Me.VOID.HeaderText = "Void"
        Me.VOID.Name = "VOID"
        Me.VOID.ReadOnly = True
        Me.VOID.Width = 80
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblSumTrans)
        Me.GroupBox2.Controls.Add(Me.txtProductName)
        Me.GroupBox2.Controls.Add(Me.lblProductName)
        Me.GroupBox2.Controls.Add(Me.SP_LIST_DAILY_INVOICEDataGridView)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 117)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(666, 417)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        '
        'lblSumTrans
        '
        Me.lblSumTrans.AutoSize = True
        Me.lblSumTrans.Location = New System.Drawing.Point(436, 26)
        Me.lblSumTrans.Name = "lblSumTrans"
        Me.lblSumTrans.Size = New System.Drawing.Size(130, 15)
        Me.lblSumTrans.TabIndex = 6
        Me.lblSumTrans.Text = "No of Transaction(s)"
        '
        'txtProductName
        '
        Me.txtProductName.Location = New System.Drawing.Point(102, 19)
        Me.txtProductName.Name = "txtProductName"
        Me.txtProductName.Size = New System.Drawing.Size(165, 22)
        Me.txtProductName.TabIndex = 3
        '
        'lblProductName
        '
        Me.lblProductName.AutoSize = True
        Me.lblProductName.Location = New System.Drawing.Point(6, 22)
        Me.lblProductName.Name = "lblProductName"
        Me.lblProductName.Size = New System.Drawing.Size(90, 15)
        Me.lblProductName.TabIndex = 4
        Me.lblProductName.Text = "Product Name"
        '
        'lblTotalInvoice
        '
        Me.lblTotalInvoice.AutoSize = True
        Me.lblTotalInvoice.Location = New System.Drawing.Point(158, 577)
        Me.lblTotalInvoice.Name = "lblTotalInvoice"
        Me.lblTotalInvoice.Size = New System.Drawing.Size(94, 15)
        Me.lblTotalInvoice.TabIndex = 5
        Me.lblTotalInvoice.Text = "lblTotalInvoice"
        '
        'lblTotalPrice
        '
        Me.lblTotalPrice.AutoSize = True
        Me.lblTotalPrice.Location = New System.Drawing.Point(309, 577)
        Me.lblTotalPrice.Name = "lblTotalPrice"
        Me.lblTotalPrice.Size = New System.Drawing.Size(80, 15)
        Me.lblTotalPrice.TabIndex = 6
        Me.lblTotalPrice.Text = "lblTotalPrice"
        '
        'lblVoidInvoice
        '
        Me.lblVoidInvoice.AutoSize = True
        Me.lblVoidInvoice.Location = New System.Drawing.Point(158, 542)
        Me.lblVoidInvoice.Name = "lblVoidInvoice"
        Me.lblVoidInvoice.Size = New System.Drawing.Size(90, 15)
        Me.lblVoidInvoice.TabIndex = 7
        Me.lblVoidInvoice.Text = "lblVoidInvoice"
        '
        'lblVoidPrice
        '
        Me.lblVoidPrice.AutoSize = True
        Me.lblVoidPrice.Location = New System.Drawing.Point(309, 542)
        Me.lblVoidPrice.Name = "lblVoidPrice"
        Me.lblVoidPrice.Size = New System.Drawing.Size(76, 15)
        Me.lblVoidPrice.TabIndex = 8
        Me.lblVoidPrice.Text = "lblVoidPrice"
        '
        'lblQuantity
        '
        Me.lblQuantity.AutoSize = True
        Me.lblQuantity.Location = New System.Drawing.Point(527, 577)
        Me.lblQuantity.Name = "lblQuantity"
        Me.lblQuantity.Size = New System.Drawing.Size(71, 15)
        Me.lblQuantity.TabIndex = 9
        Me.lblQuantity.Text = "lblQuantity"
        '
        'lblVoidQuantity
        '
        Me.lblVoidQuantity.AutoSize = True
        Me.lblVoidQuantity.Location = New System.Drawing.Point(527, 542)
        Me.lblVoidQuantity.Name = "lblVoidQuantity"
        Me.lblVoidQuantity.Size = New System.Drawing.Size(98, 15)
        Me.lblVoidQuantity.TabIndex = 10
        Me.lblVoidQuantity.Text = "lblVoidQuantity"
        '
        'lblAllTrans
        '
        Me.lblAllTrans.AutoSize = True
        Me.lblAllTrans.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllTrans.Location = New System.Drawing.Point(9, 537)
        Me.lblAllTrans.Name = "lblAllTrans"
        Me.lblAllTrans.Size = New System.Drawing.Size(106, 30)
        Me.lblAllTrans.TabIndex = 11
        Me.lblAllTrans.Text = "All Transaction" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(incl. Void)"
        '
        'lblActiveTrans
        '
        Me.lblActiveTrans.AutoSize = True
        Me.lblActiveTrans.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActiveTrans.Location = New System.Drawing.Point(9, 577)
        Me.lblActiveTrans.Name = "lblActiveTrans"
        Me.lblActiveTrans.Size = New System.Drawing.Size(131, 15)
        Me.lblActiveTrans.TabIndex = 12
        Me.lblActiveTrans.Text = "Active Transaction"
        '
        'frmListDailyInvoice
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(690, 608)
        Me.Controls.Add(Me.lblActiveTrans)
        Me.Controls.Add(Me.lblAllTrans)
        Me.Controls.Add(Me.lblVoidQuantity)
        Me.Controls.Add(Me.lblQuantity)
        Me.Controls.Add(Me.lblVoidPrice)
        Me.Controls.Add(Me.lblVoidInvoice)
        Me.Controls.Add(Me.lblTotalPrice)
        Me.Controls.Add(Me.lblTotalInvoice)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmListDailyInvoice"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Daily Invoice"
        Me.Text = "Daily Invoice"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.SP_LIST_DAILY_INVOICEDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_LIST_DAILY_INVOICEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdGenerate As System.Windows.Forms.Button
    Friend WithEvents dtpInvoice As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents SP_LIST_DAILY_INVOICEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_LIST_DAILY_INVOICEDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtProductName As System.Windows.Forms.TextBox
    Friend WithEvents lblProductName As System.Windows.Forms.Label
    Friend WithEvents lblTotalInvoice As System.Windows.Forms.Label
    Friend WithEvents lblTotalPrice As System.Windows.Forms.Label
    Friend WithEvents lblVoidInvoice As System.Windows.Forms.Label
    Friend WithEvents lblVoidPrice As System.Windows.Forms.Label
    Friend WithEvents lblQuantity As System.Windows.Forms.Label
    Friend WithEvents lblVoidQuantity As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RECEIPT_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRODUCT_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRICE_PER_UNIT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DISCOUNT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QUANTITY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VOID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblAllTrans As System.Windows.Forms.Label
    Friend WithEvents lblActiveTrans As System.Windows.Forms.Label
    Friend WithEvents lblSumTrans As System.Windows.Forms.Label
    Friend WithEvents cmdReport As System.Windows.Forms.Button
End Class
