﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frmListStockCard
    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Dim tmpVar As Integer = 1
    Dim tmpOpen As Integer = 0
    Dim tmpIOB, tmpIP, tmpIS, tmpIA, tmpIL, _
        tmpOA, tmpOS, tmpOL, _
        tmpSRI, tmpSRO, tmpPRI, tmpPRO, tmpDI, tmpDO As Integer

    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub

    'Private Sub frmListStockCard_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
    '    If e.KeyCode = Keys.F12 Then
    '        cmdReport_Click(Nothing, Nothing)
    '    End If
    'End Sub

    Private Sub frmListStockCard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtProductName.Text = ""
        dtpPeriod.Value = DateSerial(Today.Year, Today.Month, 1)

        Dim tmpTransIn, tmpTransOut As Integer
        For x As Integer = 0 To dgvStockCard.RowCount - 1
            tmpTransIn = tmpTransIn + dgvStockCard.Item("TRANSACTION_IN", x).Value
            tmpTransIn = tmpTransIn

            tmpTransOut = tmpTransOut + dgvStockCard.Item("TRANSACTION_OUT", x).Value
            tmpTransOut = tmpTransOut
        Next
        If Language = "Indonesian" Then
            Me.Text = "Kartu Stok"
            Label1.Text = "Periode"
            lblProductName.Text = "Nama Produk"
            'dgvStockCard.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            'dgvStockCard.Columns("OPENING_BALANCE").HeaderText = "Saldo Awal"
            cmdGenerate.Text = "PROSES"
            cmdReport.Text = "Cetak"
            lblTotalProd.Text = "Total Produk : " & dgvStockCard.RowCount
            lblTotalTransIn.Text = "Total Transaksi masuk : " & tmpTransIn
            lblTotalTransOut.Text = "Total Transaksi keluar : " & tmpTransOut
        Else
            lblTotalProd.Text = "Total Product : " & dgvStockCard.RowCount
            lblTotalTransIn.Text = "Total Transaction In : " & tmpTransIn
            lblTotalTransOut.Text = "Total Transaction Out : " & tmpTransOut
        End If



        'dgvStockCard.Columns("TRANSACTION_IN").Visible = False
        'dgvStockCard.Columns("TRANSACTION_OUT").Visible = False
        'dgvStockCard.Columns("BALANCE").Visible = False
        txtProductName.Enabled = False
    End Sub

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click

        Me.Cursor = Cursors.WaitCursor
        'SP_STOCK_CARDTableAdapter.Fill(Me.DS_PRODUCT_STOCK.SP_STOCK_CARD, dtpPeriod.Value, txtProductName.Text)
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC	    @return_value = [dbo].[SP_STOCK_CARD]" & vbCrLf & _
              "		    @PERIOD = N'" & dtpPeriod.Value & "'," & vbCrLf & _
              "		    @PRODUCT_NAME = N'" & txtProductName.Text & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        dgvStockCard.DataSource = dt
        Me.Cursor = Cursors.Default

        If tmpVar = 1 Then
            dgvStockCard.Columns.Add("TRANSACTION_IN", "Transaction In")
            dgvStockCard.Columns.Add("TRANSACTION_OUT", "Transaction Out")
            dgvStockCard.Columns.Add("BALANCE", "Balance")
            dgvStockCard.Columns("TRANSACTION_IN").Visible = True
            dgvStockCard.Columns("TRANSACTION_OUT").Visible = True
            dgvStockCard.Columns("BALANCE").Visible = True
        End If

        txtProductName.Text = ""

        Dim tmpTransIn, tmpTransOut As Integer

        For n As Integer = 0 To dgvStockCard.RowCount - 1
            If dgvStockCard.Item("IN_OPENING_BALANCE", n).Value Is DBNull.Value Then
                tmpIOB = 0
            Else
                tmpIOB = dgvStockCard.Item("IN_OPENING_BALANCE", n).Value
            End If

            If dgvStockCard.Item("IN_PURCHASE", n).Value Is DBNull.Value Then
                tmpIP = 0
            Else
                tmpIP = dgvStockCard.Item("IN_PURCHASE", n).Value
            End If

            If dgvStockCard.Item("IN_ADJUSTMENT", n).Value Is DBNull.Value Then
                tmpIA = 0
            Else
                tmpIA = dgvStockCard.Item("IN_ADJUSTMENT", n).Value
            End If

            If dgvStockCard.Item("IN_SPLIT_PRODUCT", n).Value Is DBNull.Value Then
                tmpIL = 0
            Else
                tmpIL = dgvStockCard.Item("IN_SPLIT_PRODUCT", n).Value
            End If

            If dgvStockCard.Item("OUT_SALE", n).Value Is DBNull.Value Then
                tmpOS = 0
            Else
                tmpOS = dgvStockCard.Item("OUT_SALE", n).Value
            End If

            If dgvStockCard.Item("OUT_ADJUSTMENT", n).Value Is DBNull.Value Then
                tmpOA = 0
            Else
                tmpOA = dgvStockCard.Item("OUT_ADJUSTMENT", n).Value
            End If

            If dgvStockCard.Item("OUT_SPLIT_PRODUCT", n).Value Is DBNull.Value Then
                tmpOL = 0
            Else
                tmpOL = dgvStockCard.Item("OUT_SPLIT_PRODUCT", n).Value
            End If

            If dgvStockCard.Item("SALE_RETURN_IN", n).Value Is DBNull.Value Then
                tmpSRI = 0
            Else
                tmpSRI = dgvStockCard.Item("SALE_RETURN_IN", n).Value
            End If

            If dgvStockCard.Item("PURCHASE_RETURN_IN", n).Value Is DBNull.Value Then
                tmpPRI = 0
            Else
                tmpPRI = dgvStockCard.Item("PURCHASE_RETURN_IN", n).Value
            End If

            If dgvStockCard.Item("DISTRIBUTION_IN", n).Value Is DBNull.Value Then
                tmpDI = 0
            Else
                tmpDI = dgvStockCard.Item("DISTRIBUTION_IN", n).Value
            End If

            If dgvStockCard.Item("SALE_RETURN_OUT", n).Value Is DBNull.Value Then
                tmpSRO = 0
            Else
                tmpSRO = dgvStockCard.Item("SALE_RETURN_OUT", n).Value
            End If

            If dgvStockCard.Item("PURCHASE_RETURN_OUT", n).Value Is DBNull.Value Then
                tmpPRO = 0
            Else
                tmpPRO = dgvStockCard.Item("PURCHASE_RETURN_OUT", n).Value
            End If

            If dgvStockCard.Item("DISTRIBUTION_OUT", n).Value Is DBNull.Value Then
                tmpDO = 0
            Else
                tmpDO = dgvStockCard.Item("DISTRIBUTION_OUT", n).Value
            End If


            dgvStockCard.Item("TRANSACTION_IN", n).Value = tmpIOB + tmpIP + tmpIA + tmpIL + tmpDI + tmpSRI + tmpPRI
            dgvStockCard.Item("TRANSACTION_OUT", n).Value = tmpOS + tmpOA + tmpOL + tmpDO + tmpSRO + tmpPRO
            If IsDBNull(dgvStockCard.Item("OPENING_BALANCE", n).Value) Then
                tmpOpen = 0
            Else
                tmpOpen = dgvStockCard.Item("OPENING_BALANCE", n).Value
            End If
            dgvStockCard.Item("BALANCE", n).Value = tmpOpen + _
                                                    dgvStockCard.Item("TRANSACTION_IN", n).Value - _
                                                    dgvStockCard.Item("TRANSACTION_OUT", n).Value
        Next

        For x As Integer = 0 To dgvStockCard.RowCount - 1
            tmpTransIn = tmpTransIn + dgvStockCard.Item("TRANSACTION_IN", x).Value
            tmpTransIn = tmpTransIn

            tmpTransOut = tmpTransOut + dgvStockCard.Item("TRANSACTION_OUT", x).Value
            tmpTransOut = tmpTransOut
        Next
        If Language = "Indonesian" Then
            lblTotalProd.Text = "Total Produk : " & dgvStockCard.RowCount
            lblTotalTransIn.Text = "Total Transaksi masuk : " & tmpTransIn
            lblTotalTransOut.Text = "Total Transaksi keluar : " & tmpTransOut
            dgvStockCard.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            dgvStockCard.Columns("OPENING_BALANCE").HeaderText = "Saldo Awal"
            dgvStockCard.Columns("TRANSACTION_IN").HeaderText = "Transaksi Masuk"
            dgvStockCard.Columns("TRANSACTION_OUT").HeaderText = "Transaksi Keluar"
            dgvStockCard.Columns("BALANCE").HeaderText = "Saldo"
            dgvStockCard.Columns("OPENING_BALANCE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvStockCard.Columns("TRANSACTION_IN").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvStockCard.Columns("TRANSACTION_OUT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvStockCard.Columns("BALANCE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvStockCard.Columns("PRODUCT_NAME").Width = 175
            dgvStockCard.Columns("IN_OPENING_BALANCE").Visible = False
            dgvStockCard.Columns("IN_PURCHASE").Visible = False
            dgvStockCard.Columns("IN_ADJUSTMENT").Visible = False
            dgvStockCard.Columns("IN_SPLIT_PRODUCT").Visible = False
            dgvStockCard.Columns("OUT_SALE").Visible = False
            dgvStockCard.Columns("OUT_ADJUSTMENT").Visible = False
            dgvStockCard.Columns("OUT_SPLIT_PRODUCT").Visible = False
            dgvStockCard.Columns("SALE_RETURN_IN").Visible = False
            dgvStockCard.Columns("PURCHASE_RETURN_IN").Visible = False
            dgvStockCard.Columns("DISTRIBUTION_IN").Visible = False
            dgvStockCard.Columns("SALE_RETURN_OUT").Visible = False
            dgvStockCard.Columns("PURCHASE_RETURN_OUT").Visible = False
            dgvStockCard.Columns("DISTRIBUTION_OUT").Visible = False
            dgvStockCard.Columns("PRODUCT_ID").Visible = False
            dgvStockCard.Columns("PERIOD").Visible = False
        Else
            dgvStockCard.Columns("PRODUCT_NAME").HeaderText = "Product Name"
            dgvStockCard.Columns("OPENING_BALANCE").HeaderText = "Opening Balance"
            dgvStockCard.Columns("TRANSACTION_IN").HeaderText = "Transaction In"
            dgvStockCard.Columns("TRANSACTION_OUT").HeaderText = "Transaction Out"
            dgvStockCard.Columns("BALANCE").HeaderText = "Balance"
            dgvStockCard.Columns("OPENING_BALANCE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvStockCard.Columns("TRANSACTION_IN").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvStockCard.Columns("TRANSACTION_OUT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvStockCard.Columns("BALANCE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvStockCard.Columns("PRODUCT_NAME").Width = 175
            dgvStockCard.Columns("IN_OPENING_BALANCE").Visible = False
            dgvStockCard.Columns("IN_PURCHASE").Visible = False
            dgvStockCard.Columns("IN_ADJUSTMENT").Visible = False
            dgvStockCard.Columns("IN_SPLIT_PRODUCT").Visible = False
            dgvStockCard.Columns("OUT_SALE").Visible = False
            dgvStockCard.Columns("OUT_ADJUSTMENT").Visible = False
            dgvStockCard.Columns("OUT_SPLIT_PRODUCT").Visible = False
            dgvStockCard.Columns("SALE_RETURN_IN").Visible = False
            dgvStockCard.Columns("PURCHASE_RETURN_IN").Visible = False
            dgvStockCard.Columns("DISTRIBUTION_IN").Visible = False
            dgvStockCard.Columns("SALE_RETURN_OUT").Visible = False
            dgvStockCard.Columns("PURCHASE_RETURN_OUT").Visible = False
            dgvStockCard.Columns("DISTRIBUTION_OUT").Visible = False
            dgvStockCard.Columns("PRODUCT_ID").Visible = False
            dgvStockCard.Columns("PERIOD").Visible = False
            lblTotalProd.Text = "Total Product : " & dgvStockCard.RowCount
            lblTotalTransIn.Text = "Total Transaction In : " & tmpTransIn
            lblTotalTransOut.Text = "Total Transaction Out : " & tmpTransOut
        End If

        txtProductName.Enabled = True
        tmpVar += 1
    End Sub

    Private Sub dtpPeriod_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpPeriod.ValueChanged
        txtProductName.Text = ""
    End Sub

    'Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
    '    Dim RPV As New frmRepStockCard
    '    RPV.period = dtpPeriod.Value
    '    RPV.productname = txtProductName.Text
    '    RPV.ReportViewer1.ZoomPercent = 100
    '    RPV.WindowState = FormWindowState.Maximized
    '    RPV.Show()
    'End Sub

    Private Sub dgvStockCard_Sorted(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvStockCard.Sorted
        Dim tmpTransIn, tmpTransOut As Integer

        For n As Integer = 0 To dgvStockCard.RowCount - 1
            If dgvStockCard.Item("IN_OPENING_BALANCE", n).Value Is DBNull.Value Then
                tmpIOB = 0
            Else
                tmpIOB = dgvStockCard.Item("IN_OPENING_BALANCE", n).Value
            End If

            If dgvStockCard.Item("IN_PURCHASE", n).Value Is DBNull.Value Then
                tmpIP = 0
            Else
                tmpIP = dgvStockCard.Item("IN_PURCHASE", n).Value
            End If

            If dgvStockCard.Item("IN_ADJUSTMENT", n).Value Is DBNull.Value Then
                tmpIA = 0
            Else
                tmpIA = dgvStockCard.Item("IN_ADJUSTMENT", n).Value
            End If

            If dgvStockCard.Item("IN_SPLIT_PRODUCT", n).Value Is DBNull.Value Then
                tmpIL = 0
            Else
                tmpIL = dgvStockCard.Item("IN_SPLIT_PRODUCT", n).Value
            End If

            If dgvStockCard.Item("OUT_SALE", n).Value Is DBNull.Value Then
                tmpOS = 0
            Else
                tmpOS = dgvStockCard.Item("OUT_SALE", n).Value
            End If

            If dgvStockCard.Item("OUT_ADJUSTMENT", n).Value Is DBNull.Value Then
                tmpOA = 0
            Else
                tmpOA = dgvStockCard.Item("OUT_ADJUSTMENT", n).Value
            End If

            If dgvStockCard.Item("OUT_SPLIT_PRODUCT", n).Value Is DBNull.Value Then
                tmpOL = 0
            Else
                tmpOL = dgvStockCard.Item("OUT_SPLIT_PRODUCT", n).Value
            End If

            If dgvStockCard.Item("SALE_RETURN_IN", n).Value Is DBNull.Value Then
                tmpSRI = 0
            Else
                tmpSRI = dgvStockCard.Item("SALE_RETURN_IN", n).Value
            End If

            If dgvStockCard.Item("PURCHASE_RETURN_IN", n).Value Is DBNull.Value Then
                tmpPRI = 0
            Else
                tmpPRI = dgvStockCard.Item("PURCHASE_RETURN_IN", n).Value
            End If

            If dgvStockCard.Item("DISTRIBUTION_IN", n).Value Is DBNull.Value Then
                tmpDI = 0
            Else
                tmpDI = dgvStockCard.Item("DISTRIBUTION_IN", n).Value
            End If

            If dgvStockCard.Item("SALE_RETURN_OUT", n).Value Is DBNull.Value Then
                tmpSRO = 0
            Else
                tmpSRO = dgvStockCard.Item("SALE_RETURN_OUT", n).Value
            End If

            If dgvStockCard.Item("PURCHASE_RETURN_OUT", n).Value Is DBNull.Value Then
                tmpPRO = 0
            Else
                tmpPRO = dgvStockCard.Item("PURCHASE_RETURN_OUT", n).Value
            End If

            If dgvStockCard.Item("DISTRIBUTION_OUT", n).Value Is DBNull.Value Then
                tmpDO = 0
            Else
                tmpDO = dgvStockCard.Item("DISTRIBUTION_OUT", n).Value
            End If


            dgvStockCard.Item("TRANSACTION_IN", n).Value = tmpIOB + tmpIP + tmpIA + tmpIL + tmpDI + tmpSRI + tmpPRI
            dgvStockCard.Item("TRANSACTION_OUT", n).Value = tmpOS + tmpOA + tmpOL + tmpDO + tmpSRO + tmpPRO
            If IsDBNull(dgvStockCard.Item("OPENING_BALANCE", n).Value) Then
                tmpOpen = 0
            Else
                tmpOpen = dgvStockCard.Item("OPENING_BALANCE", n).Value
            End If
            dgvStockCard.Item("BALANCE", n).Value = tmpOpen + _
                                                    dgvStockCard.Item("TRANSACTION_IN", n).Value - _
                                                    dgvStockCard.Item("TRANSACTION_OUT", n).Value
        Next

        For x As Integer = 0 To dgvStockCard.RowCount - 1
            tmpTransIn = tmpTransIn + dgvStockCard.Item("TRANSACTION_IN", x).Value
            tmpTransIn = tmpTransIn

            tmpTransOut = tmpTransOut + dgvStockCard.Item("TRANSACTION_OUT", x).Value
            tmpTransOut = tmpTransOut
        Next
        If Language = "Indonesian" Then
            lblTotalProd.Text = "Total Produk : " & dgvStockCard.RowCount
            lblTotalTransIn.Text = "Total Transaksi masuk : " & tmpTransIn
            lblTotalTransOut.Text = "Total Transaksi keluar : " & tmpTransOut
        Else
            lblTotalProd.Text = "Total Product : " & dgvStockCard.RowCount
            lblTotalTransIn.Text = "Total Transaction In : " & tmpTransIn
            lblTotalTransOut.Text = "Total Transaction Out : " & tmpTransOut
        End If

    End Sub

    Private Sub txtProductName_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtProductName.KeyUp
        If e.KeyCode = Keys.Enter Then dgvStockCard.Focus()
    End Sub

    Private Sub txtProductName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtProductName.TextChanged
        Try
            ' SP_STOCK_CARDBindingSource.Filter = "PRODUCT_NAME LIKE '%" & txtProductName.Text.ToUpper & "%'"
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = "DECLARE	@return_value int" & vbCrLf & _
                  "EXEC	    @return_value = [dbo].[SP_STOCK_CARD]" & vbCrLf & _
                  "		    @PERIOD = N'" & dtpPeriod.Value & "'," & vbCrLf & _
                  "		    @PRODUCT_NAME = N'" & txtProductName.Text & "'" & vbCrLf & _
                  "SELECT	'Return Value' = @return_value"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            dgvStockCard.DataSource = dt
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtProductName.Text = ""
            txtProductName.Focus()
        End Try


        Dim tmpTransIn, tmpTransOut As Integer

        For n As Integer = 0 To dgvStockCard.RowCount - 1
            If dgvStockCard.Item("IN_OPENING_BALANCE", n).Value Is DBNull.Value Then
                tmpIOB = 0
            Else
                tmpIOB = dgvStockCard.Item("IN_OPENING_BALANCE", n).Value
            End If

            If dgvStockCard.Item("IN_PURCHASE", n).Value Is DBNull.Value Then
                tmpIP = 0
            Else
                tmpIP = dgvStockCard.Item("IN_PURCHASE", n).Value
            End If

            If dgvStockCard.Item("IN_ADJUSTMENT", n).Value Is DBNull.Value Then
                tmpIA = 0
            Else
                tmpIA = dgvStockCard.Item("IN_ADJUSTMENT", n).Value
            End If

            If dgvStockCard.Item("IN_SPLIT_PRODUCT", n).Value Is DBNull.Value Then
                tmpIL = 0
            Else
                tmpIL = dgvStockCard.Item("IN_SPLIT_PRODUCT", n).Value
            End If

            If dgvStockCard.Item("OUT_SALE", n).Value Is DBNull.Value Then
                tmpOS = 0
            Else
                tmpOS = dgvStockCard.Item("OUT_SALE", n).Value
            End If

            If dgvStockCard.Item("OUT_ADJUSTMENT", n).Value Is DBNull.Value Then
                tmpOA = 0
            Else
                tmpOA = dgvStockCard.Item("OUT_ADJUSTMENT", n).Value
            End If

            If dgvStockCard.Item("OUT_SPLIT_PRODUCT", n).Value Is DBNull.Value Then
                tmpOL = 0
            Else
                tmpOL = dgvStockCard.Item("OUT_SPLIT_PRODUCT", n).Value
            End If

            If dgvStockCard.Item("SALE_RETURN_IN", n).Value Is DBNull.Value Then
                tmpSRI = 0
            Else
                tmpSRI = dgvStockCard.Item("SALE_RETURN_IN", n).Value
            End If

            If dgvStockCard.Item("PURCHASE_RETURN_IN", n).Value Is DBNull.Value Then
                tmpPRI = 0
            Else
                tmpPRI = dgvStockCard.Item("PURCHASE_RETURN_IN", n).Value
            End If

            If dgvStockCard.Item("DISTRIBUTION_IN", n).Value Is DBNull.Value Then
                tmpDI = 0
            Else
                tmpDI = dgvStockCard.Item("DISTRIBUTION_IN", n).Value
            End If

            If dgvStockCard.Item("SALE_RETURN_OUT", n).Value Is DBNull.Value Then
                tmpSRO = 0
            Else
                tmpSRO = dgvStockCard.Item("SALE_RETURN_OUT", n).Value
            End If

            If dgvStockCard.Item("PURCHASE_RETURN_OUT", n).Value Is DBNull.Value Then
                tmpPRO = 0
            Else
                tmpPRO = dgvStockCard.Item("PURCHASE_RETURN_OUT", n).Value
            End If

            If dgvStockCard.Item("DISTRIBUTION_OUT", n).Value Is DBNull.Value Then
                tmpDO = 0
            Else
                tmpDO = dgvStockCard.Item("DISTRIBUTION_OUT", n).Value
            End If


            dgvStockCard.Item("TRANSACTION_IN", n).Value = tmpIOB + tmpIP + tmpIA + tmpIL + tmpDI + tmpSRI + tmpPRI
            dgvStockCard.Item("TRANSACTION_OUT", n).Value = tmpOS + tmpOA + tmpOL + tmpDO + tmpSRO + tmpPRO
            dgvStockCard.Item("BALANCE", n).Value = dgvStockCard.Item("OPENING_BALANCE", n).Value + _
                                                    dgvStockCard.Item("TRANSACTION_IN", n).Value - _
                                                    dgvStockCard.Item("TRANSACTION_OUT", n).Value
        Next

        For x As Integer = 0 To dgvStockCard.RowCount - 1
            tmpTransIn = tmpTransIn + dgvStockCard.Item("TRANSACTION_IN", x).Value
            tmpTransIn = tmpTransIn

            tmpTransOut = tmpTransOut + dgvStockCard.Item("TRANSACTION_OUT", x).Value
            tmpTransOut = tmpTransOut
        Next
        If Language = "Indonesian" Then
            lblTotalProd.Text = "Total Produk : " & dgvStockCard.RowCount
            lblTotalTransIn.Text = "Total Transaksi masuk : " & tmpTransIn
            lblTotalTransOut.Text = "Total Transaksi keluar : " & tmpTransOut
        Else
            lblTotalProd.Text = "Total Product : " & dgvStockCard.RowCount
            lblTotalTransIn.Text = "Total Transaction In : " & tmpTransIn
            lblTotalTransOut.Text = "Total Transaction Out : " & tmpTransOut
        End If

    End Sub

End Class