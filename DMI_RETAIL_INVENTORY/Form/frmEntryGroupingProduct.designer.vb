﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEntryGroupingProduct
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEntryGroupingProduct))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DS_GROUPING_PRODUCT = New DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCT()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblParent = New System.Windows.Forms.Label()
        Me.PRODUCT_CODEComboBox = New System.Windows.Forms.ComboBox()
        Me.PRODUCT1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PRODUCT2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PRODUCT_NAMETextBox = New System.Windows.Forms.TextBox()
        Me.cmdSearchParent = New System.Windows.Forms.PictureBox()
        Me.PRODUCT1BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.PRODUCTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblF6 = New System.Windows.Forms.Label()
        Me.lblF5 = New System.Windows.Forms.Label()
        Me.dgvGroupingProduct = New System.Windows.Forms.DataGridView()
        Me.CHILD_PRODUCT_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CHILD_PRODUCT_CODE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CHILD_PRODUCT_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QUANTITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DELETE = New System.Windows.Forms.DataGridViewImageColumn()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.TableAdapterManager = New DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.TableAdapterManager()
        Me.PRODUCTTableAdapter = New DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.PRODUCTTableAdapter()
        Me.GROUPING_PRODUCTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GROUPING_PRODUCTTableAdapter = New DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.GROUPING_PRODUCTTableAdapter()
        Me.PRODUCT1TableAdapter = New DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.PRODUCT1TableAdapter()
        Me.SP_LIST_PRODUCT_GOODSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_LIST_PRODUCT_GOODSTableAdapter = New DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.SP_LIST_PRODUCT_GOODSTableAdapter()
        Me.PRODUCT2TableAdapter = New DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.PRODUCT2TableAdapter()
        Me.SP_GET_CHILD_PRODUCTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_GET_CHILD_PRODUCTTableAdapter = New DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.SP_GET_CHILD_PRODUCTTableAdapter()
        CType(Me.DS_GROUPING_PRODUCT, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PRODUCT1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PRODUCT2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmdSearchParent, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PRODUCT1BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvGroupingProduct, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.GROUPING_PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_LIST_PRODUCT_GOODSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_GET_CHILD_PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DS_GROUPING_PRODUCT
        '
        Me.DS_GROUPING_PRODUCT.DataSetName = "DS_GROUPING_PRODUCT"
        Me.DS_GROUPING_PRODUCT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblParent)
        Me.GroupBox1.Controls.Add(Me.PRODUCT_CODEComboBox)
        Me.GroupBox1.Controls.Add(Me.PRODUCT_NAMETextBox)
        Me.GroupBox1.Controls.Add(Me.cmdSearchParent)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(515, 84)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        '
        'lblParent
        '
        Me.lblParent.AutoSize = True
        Me.lblParent.Location = New System.Drawing.Point(20, 18)
        Me.lblParent.Name = "lblParent"
        Me.lblParent.Size = New System.Drawing.Size(94, 15)
        Me.lblParent.TabIndex = 23
        Me.lblParent.Text = "Parent Product"
        '
        'PRODUCT_CODEComboBox
        '
        Me.PRODUCT_CODEComboBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(202, Byte), Integer))
        Me.PRODUCT_CODEComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PRODUCT1BindingSource, "PRODUCT_CODE", True))
        Me.PRODUCT_CODEComboBox.DataSource = Me.PRODUCT2BindingSource
        Me.PRODUCT_CODEComboBox.DisplayMember = "PRODUCT_CODE"
        Me.PRODUCT_CODEComboBox.FormattingEnabled = True
        Me.PRODUCT_CODEComboBox.Location = New System.Drawing.Point(23, 42)
        Me.PRODUCT_CODEComboBox.Name = "PRODUCT_CODEComboBox"
        Me.PRODUCT_CODEComboBox.Size = New System.Drawing.Size(121, 23)
        Me.PRODUCT_CODEComboBox.TabIndex = 1
        Me.PRODUCT_CODEComboBox.Tag = "M"
        Me.PRODUCT_CODEComboBox.ValueMember = "PRODUCT_ID"
        '
        'PRODUCT1BindingSource
        '
        Me.PRODUCT1BindingSource.DataMember = "PRODUCT1"
        Me.PRODUCT1BindingSource.DataSource = Me.DS_GROUPING_PRODUCT
        '
        'PRODUCT2BindingSource
        '
        Me.PRODUCT2BindingSource.DataMember = "PRODUCT2"
        Me.PRODUCT2BindingSource.DataSource = Me.DS_GROUPING_PRODUCT
        '
        'PRODUCT_NAMETextBox
        '
        Me.PRODUCT_NAMETextBox.BackColor = System.Drawing.SystemColors.Control
        Me.PRODUCT_NAMETextBox.Location = New System.Drawing.Point(150, 43)
        Me.PRODUCT_NAMETextBox.Name = "PRODUCT_NAMETextBox"
        Me.PRODUCT_NAMETextBox.ReadOnly = True
        Me.PRODUCT_NAMETextBox.Size = New System.Drawing.Size(281, 22)
        Me.PRODUCT_NAMETextBox.TabIndex = 2
        '
        'cmdSearchParent
        '
        Me.cmdSearchParent.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchParent.Image = CType(resources.GetObject("cmdSearchParent.Image"), System.Drawing.Image)
        Me.cmdSearchParent.Location = New System.Drawing.Point(438, 42)
        Me.cmdSearchParent.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchParent.Name = "cmdSearchParent"
        Me.cmdSearchParent.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearchParent.TabIndex = 22
        Me.cmdSearchParent.TabStop = False
        '
        'PRODUCT1BindingSource1
        '
        Me.PRODUCT1BindingSource1.DataMember = "PRODUCT1"
        Me.PRODUCT1BindingSource1.DataSource = Me.DS_GROUPING_PRODUCT
        '
        'PRODUCTBindingSource
        '
        Me.PRODUCTBindingSource.DataMember = "PRODUCT"
        Me.PRODUCTBindingSource.DataSource = Me.DS_GROUPING_PRODUCT
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblF6)
        Me.GroupBox2.Controls.Add(Me.lblF5)
        Me.GroupBox2.Controls.Add(Me.dgvGroupingProduct)
        Me.GroupBox2.Location = New System.Drawing.Point(13, 102)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(515, 169)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        '
        'lblF6
        '
        Me.lblF6.AutoSize = True
        Me.lblF6.Location = New System.Drawing.Point(251, 15)
        Me.lblF6.Name = "lblF6"
        Me.lblF6.Size = New System.Drawing.Size(180, 15)
        Me.lblF6.TabIndex = 25
        Me.lblF6.Text = "F6 : Browse Product by Name"
        '
        'lblF5
        '
        Me.lblF5.AutoSize = True
        Me.lblF5.Location = New System.Drawing.Point(3, 15)
        Me.lblF5.Name = "lblF5"
        Me.lblF5.Size = New System.Drawing.Size(179, 15)
        Me.lblF5.TabIndex = 24
        Me.lblF5.Text = "F5 : Browse Product by Code"
        '
        'dgvGroupingProduct
        '
        Me.dgvGroupingProduct.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGroupingProduct.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvGroupingProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGroupingProduct.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CHILD_PRODUCT_ID, Me.CHILD_PRODUCT_CODE, Me.CHILD_PRODUCT_NAME, Me.QUANTITY, Me.DELETE})
        Me.dgvGroupingProduct.Location = New System.Drawing.Point(6, 33)
        Me.dgvGroupingProduct.Name = "dgvGroupingProduct"
        Me.dgvGroupingProduct.RowHeadersWidth = 32
        Me.dgvGroupingProduct.Size = New System.Drawing.Size(503, 130)
        Me.dgvGroupingProduct.TabIndex = 7
        Me.dgvGroupingProduct.Tag = "M"
        '
        'CHILD_PRODUCT_ID
        '
        Me.CHILD_PRODUCT_ID.HeaderText = "CHILD_PRODUCT_ID"
        Me.CHILD_PRODUCT_ID.Name = "CHILD_PRODUCT_ID"
        Me.CHILD_PRODUCT_ID.Visible = False
        Me.CHILD_PRODUCT_ID.Width = 80
        '
        'CHILD_PRODUCT_CODE
        '
        Me.CHILD_PRODUCT_CODE.HeaderText = "Child Product Code"
        Me.CHILD_PRODUCT_CODE.Name = "CHILD_PRODUCT_CODE"
        Me.CHILD_PRODUCT_CODE.ReadOnly = True
        Me.CHILD_PRODUCT_CODE.Width = 120
        '
        'CHILD_PRODUCT_NAME
        '
        Me.CHILD_PRODUCT_NAME.HeaderText = "Child Product Name"
        Me.CHILD_PRODUCT_NAME.Name = "CHILD_PRODUCT_NAME"
        Me.CHILD_PRODUCT_NAME.ReadOnly = True
        Me.CHILD_PRODUCT_NAME.Width = 185
        '
        'QUANTITY
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.QUANTITY.DefaultCellStyle = DataGridViewCellStyle2
        Me.QUANTITY.HeaderText = "Quantity"
        Me.QUANTITY.Name = "QUANTITY"
        Me.QUANTITY.Width = 85
        '
        'DELETE
        '
        Me.DELETE.HeaderText = "Del"
        Me.DELETE.Image = Global.DMI_RETAIL_INVENTORY.My.Resources.Resources.BindingNavigatorDeleteItem_Image
        Me.DELETE.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch
        Me.DELETE.Name = "DELETE"
        Me.DELETE.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DELETE.Width = 40
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cmdUndo)
        Me.GroupBox3.Controls.Add(Me.cmdSave)
        Me.GroupBox3.Controls.Add(Me.cmdEdit)
        Me.GroupBox3.Controls.Add(Me.cmdDelete)
        Me.GroupBox3.Controls.Add(Me.cmdAdd)
        Me.GroupBox3.Location = New System.Drawing.Point(13, 277)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox3.Size = New System.Drawing.Size(515, 55)
        Me.GroupBox3.TabIndex = 7
        Me.GroupBox3.TabStop = False
        '
        'cmdUndo
        '
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(220, 17)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(74, 29)
        Me.cmdUndo.TabIndex = 12
        Me.cmdUndo.Text = "&Undo"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSave.Location = New System.Drawing.Point(314, 17)
        Me.cmdSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(86, 29)
        Me.cmdSave.TabIndex = 13
        Me.cmdSave.Text = "&Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Enabled = False
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(124, 17)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(74, 29)
        Me.cmdEdit.TabIndex = 11
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(422, 17)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(74, 29)
        Me.cmdDelete.TabIndex = 14
        Me.cmdDelete.Text = "&Delete"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Enabled = False
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(18, 17)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(83, 29)
        Me.cmdAdd.TabIndex = 10
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.GROUPING_PRODUCTTableAdapter = Nothing
        Me.TableAdapterManager.PRODUCT_UNITTableAdapter = Nothing
        Me.TableAdapterManager.PRODUCT1TableAdapter = Nothing
        Me.TableAdapterManager.PRODUCTTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'PRODUCTTableAdapter
        '
        Me.PRODUCTTableAdapter.ClearBeforeFill = True
        '
        'GROUPING_PRODUCTBindingSource
        '
        Me.GROUPING_PRODUCTBindingSource.DataMember = "GROUPING_PRODUCT"
        Me.GROUPING_PRODUCTBindingSource.DataSource = Me.DS_GROUPING_PRODUCT
        '
        'GROUPING_PRODUCTTableAdapter
        '
        Me.GROUPING_PRODUCTTableAdapter.ClearBeforeFill = True
        '
        'PRODUCT1TableAdapter
        '
        Me.PRODUCT1TableAdapter.ClearBeforeFill = True
        '
        'SP_LIST_PRODUCT_GOODSBindingSource
        '
        Me.SP_LIST_PRODUCT_GOODSBindingSource.DataMember = "SP_LIST_PRODUCT_GOODS"
        Me.SP_LIST_PRODUCT_GOODSBindingSource.DataSource = Me.DS_GROUPING_PRODUCT
        '
        'SP_LIST_PRODUCT_GOODSTableAdapter
        '
        Me.SP_LIST_PRODUCT_GOODSTableAdapter.ClearBeforeFill = True
        '
        'PRODUCT2TableAdapter
        '
        Me.PRODUCT2TableAdapter.ClearBeforeFill = True
        '
        'SP_GET_CHILD_PRODUCTBindingSource
        '
        Me.SP_GET_CHILD_PRODUCTBindingSource.DataMember = "SP_GET_CHILD_PRODUCT"
        Me.SP_GET_CHILD_PRODUCTBindingSource.DataSource = Me.DS_GROUPING_PRODUCT
        '
        'SP_GET_CHILD_PRODUCTTableAdapter
        '
        Me.SP_GET_CHILD_PRODUCTTableAdapter.ClearBeforeFill = True
        '
        'frmEntryGroupingProduct
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(541, 352)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmEntryGroupingProduct"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Grouping Product"
        Me.Text = "Grouping Product"
        CType(Me.DS_GROUPING_PRODUCT, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PRODUCT1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PRODUCT2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmdSearchParent, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PRODUCT1BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvGroupingProduct, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.GROUPING_PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_LIST_PRODUCT_GOODSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_GET_CHILD_PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DS_GROUPING_PRODUCT As DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCT
    Friend WithEvents TableAdapterManager As DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.TableAdapterManager
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents cmdSearchParent As System.Windows.Forms.PictureBox
    Friend WithEvents PRODUCTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PRODUCTTableAdapter As DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.PRODUCTTableAdapter
    Friend WithEvents dgvGroupingProduct As System.Windows.Forms.DataGridView
    Friend WithEvents GROUPING_PRODUCTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GROUPING_PRODUCTTableAdapter As DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.GROUPING_PRODUCTTableAdapter
    Friend WithEvents PRODUCT1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PRODUCT1TableAdapter As DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.PRODUCT1TableAdapter
    Friend WithEvents PRODUCT_NAMETextBox As System.Windows.Forms.TextBox
    Friend WithEvents PRODUCT_CODEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents PRODUCT1BindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents SP_LIST_PRODUCT_GOODSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_LIST_PRODUCT_GOODSTableAdapter As DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.SP_LIST_PRODUCT_GOODSTableAdapter
    Friend WithEvents PRODUCT2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PRODUCT2TableAdapter As DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.PRODUCT2TableAdapter
    Friend WithEvents lblParent As System.Windows.Forms.Label
    Friend WithEvents lblF6 As System.Windows.Forms.Label
    Friend WithEvents lblF5 As System.Windows.Forms.Label
    Friend WithEvents CHILD_PRODUCT_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CHILD_PRODUCT_CODE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CHILD_PRODUCT_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QUANTITY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DELETE As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents SP_GET_CHILD_PRODUCTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_GET_CHILD_PRODUCTTableAdapter As DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.SP_GET_CHILD_PRODUCTTableAdapter
End Class
