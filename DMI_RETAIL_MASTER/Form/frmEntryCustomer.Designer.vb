﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEntryCustomer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DISCOUNTLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEntryCustomer))
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkActive = New System.Windows.Forms.CheckBox()
        Me.DISCOUNTTextBox = New System.Windows.Forms.TextBox()
        Me.lblMaxDay = New System.Windows.Forms.Label()
        Me.lblMaxAr = New System.Windows.Forms.Label()
        Me.lblContact = New System.Windows.Forms.Label()
        Me.lblCity = New System.Windows.Forms.Label()
        Me.lblFax = New System.Windows.Forms.Label()
        Me.lblPhone = New System.Windows.Forms.Label()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblCode = New System.Windows.Forms.Label()
        Me.MAX_DAYTextBox = New System.Windows.Forms.TextBox()
        Me.MAX_ARTextBox = New System.Windows.Forms.TextBox()
        Me.cmdSearchName = New System.Windows.Forms.PictureBox()
        Me.cmdSearchCode = New System.Windows.Forms.PictureBox()
        Me.CUSTOMER_NOTextBox = New System.Windows.Forms.TextBox()
        Me.CUSTOMER_NAMETextBox = New System.Windows.Forms.TextBox()
        Me.CUSTOMER_ADDRESSTextBox = New System.Windows.Forms.TextBox()
        Me.CUSTOMER_PHONETextBox = New System.Windows.Forms.TextBox()
        Me.CUSTOMER_FAXTextBox = New System.Windows.Forms.TextBox()
        Me.CUSTOMER_CITYTextBox = New System.Windows.Forms.TextBox()
        Me.CUSTOMER_CONTACT_PERSONTextBox = New System.Windows.Forms.TextBox()
        Me.CUSTOMERBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        DISCOUNTLabel = New System.Windows.Forms.Label()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmdSearchCode, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CUSTOMERBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CUSTOMERBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'DISCOUNTLabel
        '
        DISCOUNTLabel.AutoSize = True
        DISCOUNTLabel.Location = New System.Drawing.Point(38, 243)
        DISCOUNTLabel.Name = "DISCOUNTLabel"
        DISCOUNTLabel.Size = New System.Drawing.Size(76, 17)
        DISCOUNTLabel.TabIndex = 14
        DISCOUNTLabel.Text = "Discount"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdUndo)
        Me.GroupBox2.Controls.Add(Me.cmdSave)
        Me.GroupBox2.Controls.Add(Me.cmdEdit)
        Me.GroupBox2.Controls.Add(Me.cmdDelete)
        Me.GroupBox2.Controls.Add(Me.cmdAdd)
        Me.GroupBox2.Location = New System.Drawing.Point(13, 309)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Size = New System.Drawing.Size(515, 55)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        '
        'cmdUndo
        '
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(220, 17)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(74, 29)
        Me.cmdUndo.TabIndex = 2
        Me.cmdUndo.Text = "&Undo"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSave.Location = New System.Drawing.Point(321, 17)
        Me.cmdSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(74, 29)
        Me.cmdSave.TabIndex = 3
        Me.cmdSave.Text = "&Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(119, 17)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(74, 29)
        Me.cmdEdit.TabIndex = 1
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(422, 17)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(74, 29)
        Me.cmdDelete.TabIndex = 4
        Me.cmdDelete.Text = "&Delete"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(18, 17)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(74, 29)
        Me.cmdAdd.TabIndex = 0
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkActive)
        Me.GroupBox1.Controls.Add(DISCOUNTLabel)
        Me.GroupBox1.Controls.Add(Me.DISCOUNTTextBox)
        Me.GroupBox1.Controls.Add(Me.lblMaxDay)
        Me.GroupBox1.Controls.Add(Me.lblMaxAr)
        Me.GroupBox1.Controls.Add(Me.lblContact)
        Me.GroupBox1.Controls.Add(Me.lblCity)
        Me.GroupBox1.Controls.Add(Me.lblFax)
        Me.GroupBox1.Controls.Add(Me.lblPhone)
        Me.GroupBox1.Controls.Add(Me.lblAddress)
        Me.GroupBox1.Controls.Add(Me.lblName)
        Me.GroupBox1.Controls.Add(Me.lblCode)
        Me.GroupBox1.Controls.Add(Me.MAX_DAYTextBox)
        Me.GroupBox1.Controls.Add(Me.MAX_ARTextBox)
        Me.GroupBox1.Controls.Add(Me.cmdSearchName)
        Me.GroupBox1.Controls.Add(Me.cmdSearchCode)
        Me.GroupBox1.Controls.Add(Me.CUSTOMER_NOTextBox)
        Me.GroupBox1.Controls.Add(Me.CUSTOMER_NAMETextBox)
        Me.GroupBox1.Controls.Add(Me.CUSTOMER_ADDRESSTextBox)
        Me.GroupBox1.Controls.Add(Me.CUSTOMER_PHONETextBox)
        Me.GroupBox1.Controls.Add(Me.CUSTOMER_FAXTextBox)
        Me.GroupBox1.Controls.Add(Me.CUSTOMER_CITYTextBox)
        Me.GroupBox1.Controls.Add(Me.CUSTOMER_CONTACT_PERSONTextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 28)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(515, 275)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'chkActive
        '
        Me.chkActive.AutoSize = True
        Me.chkActive.Location = New System.Drawing.Point(432, 242)
        Me.chkActive.Name = "chkActive"
        Me.chkActive.Size = New System.Drawing.Size(76, 21)
        Me.chkActive.TabIndex = 16
        Me.chkActive.Text = "Active"
        Me.chkActive.UseVisualStyleBackColor = True
        '
        'DISCOUNTTextBox
        '
        Me.DISCOUNTTextBox.Location = New System.Drawing.Point(160, 240)
        Me.DISCOUNTTextBox.Name = "DISCOUNTTextBox"
        Me.DISCOUNTTextBox.Size = New System.Drawing.Size(81, 25)
        Me.DISCOUNTTextBox.TabIndex = 15
        Me.DISCOUNTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMaxDay
        '
        Me.lblMaxDay.AutoSize = True
        Me.lblMaxDay.Enabled = False
        Me.lblMaxDay.Location = New System.Drawing.Point(38, 371)
        Me.lblMaxDay.Name = "lblMaxDay"
        Me.lblMaxDay.Size = New System.Drawing.Size(71, 17)
        Me.lblMaxDay.TabIndex = 28
        Me.lblMaxDay.Text = "Max Day"
        Me.lblMaxDay.Visible = False
        '
        'lblMaxAr
        '
        Me.lblMaxAr.AutoSize = True
        Me.lblMaxAr.Enabled = False
        Me.lblMaxAr.Location = New System.Drawing.Point(38, 340)
        Me.lblMaxAr.Name = "lblMaxAr"
        Me.lblMaxAr.Size = New System.Drawing.Size(65, 17)
        Me.lblMaxAr.TabIndex = 27
        Me.lblMaxAr.Text = "Max AR"
        Me.lblMaxAr.Visible = False
        '
        'lblContact
        '
        Me.lblContact.AutoSize = True
        Me.lblContact.Location = New System.Drawing.Point(38, 215)
        Me.lblContact.Name = "lblContact"
        Me.lblContact.Size = New System.Drawing.Size(122, 17)
        Me.lblContact.TabIndex = 12
        Me.lblContact.Text = "Contact Person"
        '
        'lblCity
        '
        Me.lblCity.AutoSize = True
        Me.lblCity.Location = New System.Drawing.Point(38, 187)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(38, 17)
        Me.lblCity.TabIndex = 10
        Me.lblCity.Text = "City"
        '
        'lblFax
        '
        Me.lblFax.AutoSize = True
        Me.lblFax.Location = New System.Drawing.Point(38, 159)
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Size = New System.Drawing.Size(32, 17)
        Me.lblFax.TabIndex = 8
        Me.lblFax.Text = "Fax"
        '
        'lblPhone
        '
        Me.lblPhone.AutoSize = True
        Me.lblPhone.Location = New System.Drawing.Point(38, 135)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.Size = New System.Drawing.Size(54, 17)
        Me.lblPhone.TabIndex = 6
        Me.lblPhone.Text = "Phone"
        '
        'lblAddress
        '
        Me.lblAddress.AutoSize = True
        Me.lblAddress.Location = New System.Drawing.Point(37, 74)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(70, 17)
        Me.lblAddress.TabIndex = 4
        Me.lblAddress.Text = "Address"
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Location = New System.Drawing.Point(38, 46)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(50, 17)
        Me.lblName.TabIndex = 2
        Me.lblName.Text = "Name"
        '
        'lblCode
        '
        Me.lblCode.AutoSize = True
        Me.lblCode.Location = New System.Drawing.Point(38, 18)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(48, 17)
        Me.lblCode.TabIndex = 0
        Me.lblCode.Text = "Code"
        '
        'MAX_DAYTextBox
        '
        Me.MAX_DAYTextBox.Enabled = False
        Me.MAX_DAYTextBox.Location = New System.Drawing.Point(160, 368)
        Me.MAX_DAYTextBox.MaxLength = 3
        Me.MAX_DAYTextBox.Name = "MAX_DAYTextBox"
        Me.MAX_DAYTextBox.Size = New System.Drawing.Size(81, 25)
        Me.MAX_DAYTextBox.TabIndex = 19
        Me.MAX_DAYTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.MAX_DAYTextBox.Visible = False
        '
        'MAX_ARTextBox
        '
        Me.MAX_ARTextBox.Enabled = False
        Me.MAX_ARTextBox.Location = New System.Drawing.Point(160, 340)
        Me.MAX_ARTextBox.MaxLength = 12
        Me.MAX_ARTextBox.Name = "MAX_ARTextBox"
        Me.MAX_ARTextBox.Size = New System.Drawing.Size(132, 25)
        Me.MAX_ARTextBox.TabIndex = 18
        Me.MAX_ARTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.MAX_ARTextBox.Visible = False
        '
        'cmdSearchName
        '
        Me.cmdSearchName.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchName.Image = CType(resources.GetObject("cmdSearchName.Image"), System.Drawing.Image)
        Me.cmdSearchName.Location = New System.Drawing.Point(403, 43)
        Me.cmdSearchName.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchName.Name = "cmdSearchName"
        Me.cmdSearchName.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearchName.TabIndex = 17
        Me.cmdSearchName.TabStop = False
        '
        'cmdSearchCode
        '
        Me.cmdSearchCode.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchCode.Image = CType(resources.GetObject("cmdSearchCode.Image"), System.Drawing.Image)
        Me.cmdSearchCode.Location = New System.Drawing.Point(248, 15)
        Me.cmdSearchCode.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchCode.Name = "cmdSearchCode"
        Me.cmdSearchCode.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearchCode.TabIndex = 16
        Me.cmdSearchCode.TabStop = False
        '
        'CUSTOMER_NOTextBox
        '
        Me.CUSTOMER_NOTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CUSTOMER_NOTextBox.Location = New System.Drawing.Point(160, 15)
        Me.CUSTOMER_NOTextBox.Name = "CUSTOMER_NOTextBox"
        Me.CUSTOMER_NOTextBox.Size = New System.Drawing.Size(81, 25)
        Me.CUSTOMER_NOTextBox.TabIndex = 1
        Me.CUSTOMER_NOTextBox.Tag = "M"
        '
        'CUSTOMER_NAMETextBox
        '
        Me.CUSTOMER_NAMETextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CUSTOMER_NAMETextBox.Location = New System.Drawing.Point(160, 43)
        Me.CUSTOMER_NAMETextBox.Name = "CUSTOMER_NAMETextBox"
        Me.CUSTOMER_NAMETextBox.Size = New System.Drawing.Size(235, 25)
        Me.CUSTOMER_NAMETextBox.TabIndex = 3
        Me.CUSTOMER_NAMETextBox.Tag = "M"
        '
        'CUSTOMER_ADDRESSTextBox
        '
        Me.CUSTOMER_ADDRESSTextBox.Location = New System.Drawing.Point(160, 71)
        Me.CUSTOMER_ADDRESSTextBox.Multiline = True
        Me.CUSTOMER_ADDRESSTextBox.Name = "CUSTOMER_ADDRESSTextBox"
        Me.CUSTOMER_ADDRESSTextBox.Size = New System.Drawing.Size(235, 51)
        Me.CUSTOMER_ADDRESSTextBox.TabIndex = 5
        '
        'CUSTOMER_PHONETextBox
        '
        Me.CUSTOMER_PHONETextBox.Location = New System.Drawing.Point(160, 128)
        Me.CUSTOMER_PHONETextBox.Name = "CUSTOMER_PHONETextBox"
        Me.CUSTOMER_PHONETextBox.Size = New System.Drawing.Size(134, 25)
        Me.CUSTOMER_PHONETextBox.TabIndex = 7
        '
        'CUSTOMER_FAXTextBox
        '
        Me.CUSTOMER_FAXTextBox.Location = New System.Drawing.Point(160, 156)
        Me.CUSTOMER_FAXTextBox.Name = "CUSTOMER_FAXTextBox"
        Me.CUSTOMER_FAXTextBox.Size = New System.Drawing.Size(134, 25)
        Me.CUSTOMER_FAXTextBox.TabIndex = 9
        '
        'CUSTOMER_CITYTextBox
        '
        Me.CUSTOMER_CITYTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CUSTOMER_CITYTextBox.Location = New System.Drawing.Point(160, 184)
        Me.CUSTOMER_CITYTextBox.Name = "CUSTOMER_CITYTextBox"
        Me.CUSTOMER_CITYTextBox.Size = New System.Drawing.Size(134, 25)
        Me.CUSTOMER_CITYTextBox.TabIndex = 11
        Me.CUSTOMER_CITYTextBox.Tag = "M"
        '
        'CUSTOMER_CONTACT_PERSONTextBox
        '
        Me.CUSTOMER_CONTACT_PERSONTextBox.Location = New System.Drawing.Point(160, 212)
        Me.CUSTOMER_CONTACT_PERSONTextBox.Name = "CUSTOMER_CONTACT_PERSONTextBox"
        Me.CUSTOMER_CONTACT_PERSONTextBox.Size = New System.Drawing.Size(134, 25)
        Me.CUSTOMER_CONTACT_PERSONTextBox.TabIndex = 13
        '
        'CUSTOMERBindingNavigator
        '
        Me.CUSTOMERBindingNavigator.AddNewItem = Nothing
        Me.CUSTOMERBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.CUSTOMERBindingNavigator.DeleteItem = Nothing
        Me.CUSTOMERBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.CUSTOMERBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.CUSTOMERBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CUSTOMERBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.CUSTOMERBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.CUSTOMERBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.CUSTOMERBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.CUSTOMERBindingNavigator.Name = "CUSTOMERBindingNavigator"
        Me.CUSTOMERBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.CUSTOMERBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.CUSTOMERBindingNavigator.Size = New System.Drawing.Size(541, 25)
        Me.CUSTOMERBindingNavigator.TabIndex = 0
        Me.CUSTOMERBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(45, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'frmEntryCustomer
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(541, 372)
        Me.Controls.Add(Me.CUSTOMERBindingNavigator)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmEntryCustomer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Entry Customer"
        Me.Text = "Entry Customer"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmdSearchCode, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CUSTOMERBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CUSTOMERBindingNavigator.ResumeLayout(False)
        Me.CUSTOMERBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CUSTOMERBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CUSTOMER_NOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CUSTOMER_NAMETextBox As System.Windows.Forms.TextBox
    Friend WithEvents CUSTOMER_ADDRESSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CUSTOMER_PHONETextBox As System.Windows.Forms.TextBox
    Friend WithEvents CUSTOMER_FAXTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CUSTOMER_CITYTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CUSTOMER_CONTACT_PERSONTextBox As System.Windows.Forms.TextBox
    Friend WithEvents cmdSearchName As System.Windows.Forms.PictureBox
    Friend WithEvents cmdSearchCode As System.Windows.Forms.PictureBox
    Friend WithEvents MAX_DAYTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MAX_ARTextBox As System.Windows.Forms.TextBox
    Friend WithEvents lblFax As System.Windows.Forms.Label
    Friend WithEvents lblPhone As System.Windows.Forms.Label
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents lblMaxDay As System.Windows.Forms.Label
    Friend WithEvents lblMaxAr As System.Windows.Forms.Label
    Friend WithEvents lblContact As System.Windows.Forms.Label
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents DISCOUNTTextBox As System.Windows.Forms.TextBox
    Friend WithEvents chkActive As System.Windows.Forms.CheckBox
End Class
