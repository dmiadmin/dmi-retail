﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRepProfitLoss
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource2 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource3 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource4 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource5 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRepProfitLoss))
        Me.FS_PROFITLOSS_STATEMENT1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DB_DMI_RETAILDataSetALL = New DMI_RETAIL_REPORT.DB_DMI_RETAILDataSetALL()
        Me.FS_PROFITLOSS_STATEMENT2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FS_PROFITLOSS_STATEMENT3BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FS_PROFITLOSS_STATEMENT4BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.FS_PROFITLOSS_STATEMENT1TableAdapter = New DMI_RETAIL_REPORT.DB_DMI_RETAILDataSetALLTableAdapters.FS_PROFITLOSS_STATEMENT1TableAdapter()
        Me.FS_PROFITLOSS_STATEMENT2TableAdapter = New DMI_RETAIL_REPORT.DB_DMI_RETAILDataSetALLTableAdapters.FS_PROFITLOSS_STATEMENT2TableAdapter()
        Me.FS_PROFITLOSS_STATEMENT3TableAdapter = New DMI_RETAIL_REPORT.DB_DMI_RETAILDataSetALLTableAdapters.FS_PROFITLOSS_STATEMENT3TableAdapter()
        Me.FS_PROFITLOSS_STATEMENT4TableAdapter = New DMI_RETAIL_REPORT.DB_DMI_RETAILDataSetALLTableAdapters.FS_PROFITLOSS_STATEMENT4TableAdapter()
        Me.COMPANY_INFORMATIONBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.COMPANY_INFORMATIONTableAdapter = New DMI_RETAIL_REPORT.DB_DMI_RETAILDataSetALLTableAdapters.COMPANY_INFORMATIONTableAdapter()
        CType(Me.FS_PROFITLOSS_STATEMENT1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DB_DMI_RETAILDataSetALL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FS_PROFITLOSS_STATEMENT2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FS_PROFITLOSS_STATEMENT3BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FS_PROFITLOSS_STATEMENT4BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.COMPANY_INFORMATIONBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'FS_PROFITLOSS_STATEMENT1BindingSource
        '
        Me.FS_PROFITLOSS_STATEMENT1BindingSource.DataMember = "FS_PROFITLOSS_STATEMENT1"
        Me.FS_PROFITLOSS_STATEMENT1BindingSource.DataSource = Me.DB_DMI_RETAILDataSetALL
        '
        'DB_DMI_RETAILDataSetALL
        '
        Me.DB_DMI_RETAILDataSetALL.DataSetName = "DB_DMI_RETAILDataSetALL"
        Me.DB_DMI_RETAILDataSetALL.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'FS_PROFITLOSS_STATEMENT2BindingSource
        '
        Me.FS_PROFITLOSS_STATEMENT2BindingSource.DataMember = "FS_PROFITLOSS_STATEMENT2"
        Me.FS_PROFITLOSS_STATEMENT2BindingSource.DataSource = Me.DB_DMI_RETAILDataSetALL
        '
        'FS_PROFITLOSS_STATEMENT3BindingSource
        '
        Me.FS_PROFITLOSS_STATEMENT3BindingSource.DataMember = "FS_PROFITLOSS_STATEMENT3"
        Me.FS_PROFITLOSS_STATEMENT3BindingSource.DataSource = Me.DB_DMI_RETAILDataSetALL
        '
        'FS_PROFITLOSS_STATEMENT4BindingSource
        '
        Me.FS_PROFITLOSS_STATEMENT4BindingSource.DataMember = "FS_PROFITLOSS_STATEMENT4"
        Me.FS_PROFITLOSS_STATEMENT4BindingSource.DataSource = Me.DB_DMI_RETAILDataSetALL
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "DS_RPT_PROFIT_INCOME"
        ReportDataSource1.Value = Me.FS_PROFITLOSS_STATEMENT1BindingSource
        ReportDataSource2.Name = "DS_RPT_PROFIT_COGS"
        ReportDataSource2.Value = Me.FS_PROFITLOSS_STATEMENT2BindingSource
        ReportDataSource3.Name = "DS_RPT_PROFIT_EXPENSES"
        ReportDataSource3.Value = Me.FS_PROFITLOSS_STATEMENT3BindingSource
        ReportDataSource4.Name = "DS_RPT_PROFIT_NOPINCOME"
        ReportDataSource4.Value = Me.FS_PROFITLOSS_STATEMENT4BindingSource
        ReportDataSource5.Name = "DS_RPT_COMPANY_INFO"
        ReportDataSource5.Value = Me.COMPANY_INFORMATIONBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource2)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource3)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource4)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource5)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "DMI_RETAIL.repProfitLoss.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(284, 262)
        Me.ReportViewer1.TabIndex = 0
        '
        'FS_PROFITLOSS_STATEMENT1TableAdapter
        '
        Me.FS_PROFITLOSS_STATEMENT1TableAdapter.ClearBeforeFill = True
        '
        'FS_PROFITLOSS_STATEMENT2TableAdapter
        '
        Me.FS_PROFITLOSS_STATEMENT2TableAdapter.ClearBeforeFill = True
        '
        'FS_PROFITLOSS_STATEMENT3TableAdapter
        '
        Me.FS_PROFITLOSS_STATEMENT3TableAdapter.ClearBeforeFill = True
        '
        'FS_PROFITLOSS_STATEMENT4TableAdapter
        '
        Me.FS_PROFITLOSS_STATEMENT4TableAdapter.ClearBeforeFill = True
        '
        'COMPANY_INFORMATIONBindingSource
        '
        Me.COMPANY_INFORMATIONBindingSource.DataMember = "COMPANY_INFORMATION"
        Me.COMPANY_INFORMATIONBindingSource.DataSource = Me.DB_DMI_RETAILDataSetALL
        '
        'COMPANY_INFORMATIONTableAdapter
        '
        Me.COMPANY_INFORMATIONTableAdapter.ClearBeforeFill = True
        '
        'frmRepProfitLoss
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmRepProfitLoss"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Profit & Loss Report"
        CType(Me.FS_PROFITLOSS_STATEMENT1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DB_DMI_RETAILDataSetALL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FS_PROFITLOSS_STATEMENT2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FS_PROFITLOSS_STATEMENT3BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FS_PROFITLOSS_STATEMENT4BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.COMPANY_INFORMATIONBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents FS_PROFITLOSS_STATEMENT1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DB_DMI_RETAILDataSetALL As DMI_RETAIL_REPORT.DB_DMI_RETAILDataSetALL
    Friend WithEvents FS_PROFITLOSS_STATEMENT2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents FS_PROFITLOSS_STATEMENT3BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents FS_PROFITLOSS_STATEMENT4BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents FS_PROFITLOSS_STATEMENT1TableAdapter As DMI_RETAIL_REPORT.DB_DMI_RETAILDataSetALLTableAdapters.FS_PROFITLOSS_STATEMENT1TableAdapter
    Friend WithEvents FS_PROFITLOSS_STATEMENT2TableAdapter As DMI_RETAIL_REPORT.DB_DMI_RETAILDataSetALLTableAdapters.FS_PROFITLOSS_STATEMENT2TableAdapter
    Friend WithEvents FS_PROFITLOSS_STATEMENT3TableAdapter As DMI_RETAIL_REPORT.DB_DMI_RETAILDataSetALLTableAdapters.FS_PROFITLOSS_STATEMENT3TableAdapter
    Friend WithEvents FS_PROFITLOSS_STATEMENT4TableAdapter As DMI_RETAIL_REPORT.DB_DMI_RETAILDataSetALLTableAdapters.FS_PROFITLOSS_STATEMENT4TableAdapter
    Friend WithEvents COMPANY_INFORMATIONBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents COMPANY_INFORMATIONTableAdapter As DMI_RETAIL_REPORT.DB_DMI_RETAILDataSetALLTableAdapters.COMPANY_INFORMATIONTableAdapter
End Class
