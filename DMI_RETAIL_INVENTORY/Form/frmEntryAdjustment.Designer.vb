﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEntryAdjustment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEntryAdjustment))
        Me.ADJUSTMENTBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ADJUSTMENTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_ADJUSTMENT = New DMI_RETAIL_INVENTORY.DS_ADJUSTMENT()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ADJUSTMENT_NOTextBox = New System.Windows.Forms.TextBox()
        Me.ADJUSTMENT_DATEDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.QUANTITYTextBox = New System.Windows.Forms.TextBox()
        Me.PRICE_PER_UNITTextBox = New System.Windows.Forms.TextBox()
        Me.DESCRIPTIONTextBox = New System.Windows.Forms.TextBox()
        Me.ADJUSTMENTTableAdapter = New DMI_RETAIL_INVENTORY.DS_ADJUSTMENTTableAdapters.ADJUSTMENTTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_INVENTORY.DS_ADJUSTMENTTableAdapters.TableAdapterManager()
        Me.PRODUCT_WAREHOUSETableAdapter = New DMI_RETAIL_INVENTORY.DS_ADJUSTMENTTableAdapters.PRODUCT_WAREHOUSETableAdapter()
        Me.PRODUCTTableAdapter = New DMI_RETAIL_INVENTORY.DS_ADJUSTMENTTableAdapters.PRODUCTTableAdapter()
        Me.PRODUCT_IDComboBox = New System.Windows.Forms.ComboBox()
        Me.SP_LIST_PRODUCT_GOODSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_PRODUCT_WAREHOUSE = New DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSE()
        Me.PRODUCTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.lblWarehouse = New System.Windows.Forms.Label()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.lblUnitPrice = New System.Windows.Forms.Label()
        Me.lblQuantity = New System.Windows.Forms.Label()
        Me.lblProduct = New System.Windows.Forms.Label()
        Me.lblDate = New System.Windows.Forms.Label()
        Me.lblAdjustmenNo = New System.Windows.Forms.Label()
        Me.WAREHOUSE_IDComboBox = New System.Windows.Forms.ComboBox()
        Me.WAREHOUSEBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.txtDTP = New System.Windows.Forms.TextBox()
        Me.cmdSearchProduct = New System.Windows.Forms.PictureBox()
        Me.PRODUCT_NAMETextBox = New System.Windows.Forms.TextBox()
        Me.WAREHOUSEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.PRODUCT_WAREHOUSEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter = New DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSETableAdapters.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter()
        Me.TableAdapterManager1 = New DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSETableAdapters.TableAdapterManager()
        Me.WAREHOUSETableAdapter = New DMI_RETAIL_INVENTORY.DS_ADJUSTMENTTableAdapters.WAREHOUSETableAdapter()
        Me.DS_PARAMETER = New DMI_RETAIL_INVENTORY.DS_PARAMETER()
        Me.PARAMETERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PARAMETERTableAdapter = New DMI_RETAIL_INVENTORY.DS_PARAMETERTableAdapters.PARAMETERTableAdapter()
        Me.TableAdapterManager2 = New DMI_RETAIL_INVENTORY.DS_PARAMETERTableAdapters.TableAdapterManager()
        Me.SP_LIST_PRODUCT_GOODSTableAdapter = New DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSETableAdapters.SP_LIST_PRODUCT_GOODSTableAdapter()
        CType(Me.ADJUSTMENTBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ADJUSTMENTBindingNavigator.SuspendLayout()
        CType(Me.ADJUSTMENTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_ADJUSTMENT, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_LIST_PRODUCT_GOODSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_PRODUCT_WAREHOUSE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.WAREHOUSEBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmdSearchProduct, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.PRODUCT_WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_PARAMETER, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PARAMETERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ADJUSTMENTBindingNavigator
        '
        Me.ADJUSTMENTBindingNavigator.AddNewItem = Nothing
        Me.ADJUSTMENTBindingNavigator.BindingSource = Me.ADJUSTMENTBindingSource
        Me.ADJUSTMENTBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.ADJUSTMENTBindingNavigator.DeleteItem = Nothing
        Me.ADJUSTMENTBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ADJUSTMENTBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.ADJUSTMENTBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ADJUSTMENTBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.ADJUSTMENTBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.ADJUSTMENTBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.ADJUSTMENTBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.ADJUSTMENTBindingNavigator.Name = "ADJUSTMENTBindingNavigator"
        Me.ADJUSTMENTBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.ADJUSTMENTBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.ADJUSTMENTBindingNavigator.Size = New System.Drawing.Size(545, 25)
        Me.ADJUSTMENTBindingNavigator.TabIndex = 0
        Me.ADJUSTMENTBindingNavigator.Text = "BindingNavigator1"
        '
        'ADJUSTMENTBindingSource
        '
        Me.ADJUSTMENTBindingSource.DataMember = "ADJUSTMENT"
        Me.ADJUSTMENTBindingSource.DataSource = Me.DS_ADJUSTMENT
        '
        'DS_ADJUSTMENT
        '
        Me.DS_ADJUSTMENT.DataSetName = "DS_ADJUSTMENT"
        Me.DS_ADJUSTMENT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'ADJUSTMENT_NOTextBox
        '
        Me.ADJUSTMENT_NOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ADJUSTMENTBindingSource, "ADJUSTMENT_NO", True))
        Me.ADJUSTMENT_NOTextBox.Location = New System.Drawing.Point(115, 22)
        Me.ADJUSTMENT_NOTextBox.Name = "ADJUSTMENT_NOTextBox"
        Me.ADJUSTMENT_NOTextBox.Size = New System.Drawing.Size(121, 22)
        Me.ADJUSTMENT_NOTextBox.TabIndex = 1
        Me.ADJUSTMENT_NOTextBox.Tag = "M"
        '
        'ADJUSTMENT_DATEDateTimePicker
        '
        Me.ADJUSTMENT_DATEDateTimePicker.CustomFormat = "dd-MMM-yyyy"
        Me.ADJUSTMENT_DATEDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.ADJUSTMENTBindingSource, "ADJUSTMENT_DATE", True))
        Me.ADJUSTMENT_DATEDateTimePicker.Enabled = False
        Me.ADJUSTMENT_DATEDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.ADJUSTMENT_DATEDateTimePicker.Location = New System.Drawing.Point(242, -33)
        Me.ADJUSTMENT_DATEDateTimePicker.Name = "ADJUSTMENT_DATEDateTimePicker"
        Me.ADJUSTMENT_DATEDateTimePicker.Size = New System.Drawing.Size(121, 22)
        Me.ADJUSTMENT_DATEDateTimePicker.TabIndex = 1
        Me.ADJUSTMENT_DATEDateTimePicker.TabStop = False
        Me.ADJUSTMENT_DATEDateTimePicker.Tag = "M"
        '
        'QUANTITYTextBox
        '
        Me.QUANTITYTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ADJUSTMENTBindingSource, "QUANTITY", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.QUANTITYTextBox.Location = New System.Drawing.Point(115, 107)
        Me.QUANTITYTextBox.Name = "QUANTITYTextBox"
        Me.QUANTITYTextBox.Size = New System.Drawing.Size(70, 22)
        Me.QUANTITYTextBox.TabIndex = 5
        Me.QUANTITYTextBox.Tag = "M"
        Me.QUANTITYTextBox.Text = "0"
        Me.QUANTITYTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'PRICE_PER_UNITTextBox
        '
        Me.PRICE_PER_UNITTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ADJUSTMENTBindingSource, "PRICE_PER_UNIT", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.PRICE_PER_UNITTextBox.Location = New System.Drawing.Point(115, 135)
        Me.PRICE_PER_UNITTextBox.Name = "PRICE_PER_UNITTextBox"
        Me.PRICE_PER_UNITTextBox.Size = New System.Drawing.Size(121, 22)
        Me.PRICE_PER_UNITTextBox.TabIndex = 6
        Me.PRICE_PER_UNITTextBox.Tag = "M"
        Me.PRICE_PER_UNITTextBox.Text = "0"
        Me.PRICE_PER_UNITTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'DESCRIPTIONTextBox
        '
        Me.DESCRIPTIONTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ADJUSTMENTBindingSource, "DESCRIPTION", True))
        Me.DESCRIPTIONTextBox.Location = New System.Drawing.Point(115, 163)
        Me.DESCRIPTIONTextBox.Multiline = True
        Me.DESCRIPTIONTextBox.Name = "DESCRIPTIONTextBox"
        Me.DESCRIPTIONTextBox.Size = New System.Drawing.Size(230, 51)
        Me.DESCRIPTIONTextBox.TabIndex = 7
        '
        'ADJUSTMENTTableAdapter
        '
        Me.ADJUSTMENTTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ADJUSTMENTTableAdapter = Me.ADJUSTMENTTableAdapter
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.PRODUCT_WAREHOUSETableAdapter = Me.PRODUCT_WAREHOUSETableAdapter
        Me.TableAdapterManager.PRODUCTTableAdapter = Me.PRODUCTTableAdapter
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_INVENTORY.DS_ADJUSTMENTTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'PRODUCT_WAREHOUSETableAdapter
        '
        Me.PRODUCT_WAREHOUSETableAdapter.ClearBeforeFill = True
        '
        'PRODUCTTableAdapter
        '
        Me.PRODUCTTableAdapter.ClearBeforeFill = True
        '
        'PRODUCT_IDComboBox
        '
        Me.PRODUCT_IDComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.ADJUSTMENTBindingSource, "PRODUCT_ID", True))
        Me.PRODUCT_IDComboBox.DataSource = Me.SP_LIST_PRODUCT_GOODSBindingSource
        Me.PRODUCT_IDComboBox.DisplayMember = "PRODUCT_CODE"
        Me.PRODUCT_IDComboBox.FormattingEnabled = True
        Me.PRODUCT_IDComboBox.Location = New System.Drawing.Point(115, 78)
        Me.PRODUCT_IDComboBox.Name = "PRODUCT_IDComboBox"
        Me.PRODUCT_IDComboBox.Size = New System.Drawing.Size(121, 23)
        Me.PRODUCT_IDComboBox.TabIndex = 3
        Me.PRODUCT_IDComboBox.Tag = "M"
        Me.PRODUCT_IDComboBox.ValueMember = "PRODUCT_ID"
        '
        'SP_LIST_PRODUCT_GOODSBindingSource
        '
        Me.SP_LIST_PRODUCT_GOODSBindingSource.DataMember = "SP_LIST_PRODUCT_GOODS"
        Me.SP_LIST_PRODUCT_GOODSBindingSource.DataSource = Me.DS_PRODUCT_WAREHOUSE
        '
        'DS_PRODUCT_WAREHOUSE
        '
        Me.DS_PRODUCT_WAREHOUSE.DataSetName = "DS_PRODUCT_WAREHOUSE"
        Me.DS_PRODUCT_WAREHOUSE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PRODUCTBindingSource
        '
        Me.PRODUCTBindingSource.DataMember = "PRODUCT"
        Me.PRODUCTBindingSource.DataSource = Me.DS_ADJUSTMENT
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblStatus)
        Me.GroupBox1.Controls.Add(Me.lblWarehouse)
        Me.GroupBox1.Controls.Add(Me.lblDescription)
        Me.GroupBox1.Controls.Add(Me.lblUnitPrice)
        Me.GroupBox1.Controls.Add(Me.lblQuantity)
        Me.GroupBox1.Controls.Add(Me.lblProduct)
        Me.GroupBox1.Controls.Add(Me.lblDate)
        Me.GroupBox1.Controls.Add(Me.lblAdjustmenNo)
        Me.GroupBox1.Controls.Add(Me.WAREHOUSE_IDComboBox)
        Me.GroupBox1.Controls.Add(Me.txtDTP)
        Me.GroupBox1.Controls.Add(Me.cmdSearchProduct)
        Me.GroupBox1.Controls.Add(Me.PRODUCT_NAMETextBox)
        Me.GroupBox1.Controls.Add(Me.ADJUSTMENT_NOTextBox)
        Me.GroupBox1.Controls.Add(Me.PRODUCT_IDComboBox)
        Me.GroupBox1.Controls.Add(Me.DESCRIPTIONTextBox)
        Me.GroupBox1.Controls.Add(Me.PRICE_PER_UNITTextBox)
        Me.GroupBox1.Controls.Add(Me.ADJUSTMENT_DATEDateTimePicker)
        Me.GroupBox1.Controls.Add(Me.QUANTITYTextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 28)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(521, 250)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Lucida Bright", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(411, 217)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(104, 28)
        Me.lblStatus.TabIndex = 27
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblStatus.Visible = False
        '
        'lblWarehouse
        '
        Me.lblWarehouse.Location = New System.Drawing.Point(10, 220)
        Me.lblWarehouse.Name = "lblWarehouse"
        Me.lblWarehouse.Size = New System.Drawing.Size(97, 15)
        Me.lblWarehouse.TabIndex = 22
        Me.lblWarehouse.Text = "Warehouse"
        Me.lblWarehouse.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblDescription
        '
        Me.lblDescription.Location = New System.Drawing.Point(9, 166)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(97, 15)
        Me.lblDescription.TabIndex = 21
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblUnitPrice
        '
        Me.lblUnitPrice.Location = New System.Drawing.Point(10, 138)
        Me.lblUnitPrice.Name = "lblUnitPrice"
        Me.lblUnitPrice.Size = New System.Drawing.Size(97, 15)
        Me.lblUnitPrice.TabIndex = 20
        Me.lblUnitPrice.Text = "Unit Price"
        Me.lblUnitPrice.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblQuantity
        '
        Me.lblQuantity.Location = New System.Drawing.Point(10, 110)
        Me.lblQuantity.Name = "lblQuantity"
        Me.lblQuantity.Size = New System.Drawing.Size(97, 15)
        Me.lblQuantity.TabIndex = 19
        Me.lblQuantity.Text = "Quantity"
        Me.lblQuantity.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblProduct
        '
        Me.lblProduct.Location = New System.Drawing.Point(10, 79)
        Me.lblProduct.Name = "lblProduct"
        Me.lblProduct.Size = New System.Drawing.Size(97, 15)
        Me.lblProduct.TabIndex = 18
        Me.lblProduct.Text = "Product"
        Me.lblProduct.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblDate
        '
        Me.lblDate.Location = New System.Drawing.Point(9, 53)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(97, 15)
        Me.lblDate.TabIndex = 17
        Me.lblDate.Text = "Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblAdjustmenNo
        '
        Me.lblAdjustmenNo.AutoSize = True
        Me.lblAdjustmenNo.Location = New System.Drawing.Point(9, 25)
        Me.lblAdjustmenNo.Name = "lblAdjustmenNo"
        Me.lblAdjustmenNo.Size = New System.Drawing.Size(97, 15)
        Me.lblAdjustmenNo.TabIndex = 16
        Me.lblAdjustmenNo.Text = "Adjustment No"
        '
        'WAREHOUSE_IDComboBox
        '
        Me.WAREHOUSE_IDComboBox.DataSource = Me.WAREHOUSEBindingSource1
        Me.WAREHOUSE_IDComboBox.DisplayMember = "WAREHOUSE_NAME"
        Me.WAREHOUSE_IDComboBox.FormattingEnabled = True
        Me.WAREHOUSE_IDComboBox.Location = New System.Drawing.Point(116, 220)
        Me.WAREHOUSE_IDComboBox.Name = "WAREHOUSE_IDComboBox"
        Me.WAREHOUSE_IDComboBox.Size = New System.Drawing.Size(121, 23)
        Me.WAREHOUSE_IDComboBox.TabIndex = 9
        Me.WAREHOUSE_IDComboBox.ValueMember = "WAREHOUSE_ID"
        '
        'WAREHOUSEBindingSource1
        '
        Me.WAREHOUSEBindingSource1.DataMember = "ADJUSTMENT_WAREHOUSE"
        Me.WAREHOUSEBindingSource1.DataSource = Me.ADJUSTMENTBindingSource
        '
        'txtDTP
        '
        Me.txtDTP.Location = New System.Drawing.Point(116, 50)
        Me.txtDTP.Name = "txtDTP"
        Me.txtDTP.Size = New System.Drawing.Size(103, 22)
        Me.txtDTP.TabIndex = 2
        Me.txtDTP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cmdSearchProduct
        '
        Me.cmdSearchProduct.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchProduct.Image = CType(resources.GetObject("cmdSearchProduct.Image"), System.Drawing.Image)
        Me.cmdSearchProduct.Location = New System.Drawing.Point(471, 78)
        Me.cmdSearchProduct.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchProduct.Name = "cmdSearchProduct"
        Me.cmdSearchProduct.Size = New System.Drawing.Size(42, 23)
        Me.cmdSearchProduct.TabIndex = 15
        Me.cmdSearchProduct.TabStop = False
        '
        'PRODUCT_NAMETextBox
        '
        Me.PRODUCT_NAMETextBox.Location = New System.Drawing.Point(242, 79)
        Me.PRODUCT_NAMETextBox.Name = "PRODUCT_NAMETextBox"
        Me.PRODUCT_NAMETextBox.ReadOnly = True
        Me.PRODUCT_NAMETextBox.Size = New System.Drawing.Size(222, 22)
        Me.PRODUCT_NAMETextBox.TabIndex = 4
        Me.PRODUCT_NAMETextBox.TabStop = False
        '
        'WAREHOUSEBindingSource
        '
        Me.WAREHOUSEBindingSource.DataMember = "WAREHOUSE"
        Me.WAREHOUSEBindingSource.DataSource = Me.DS_ADJUSTMENT
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdUndo)
        Me.GroupBox2.Controls.Add(Me.cmdSave)
        Me.GroupBox2.Controls.Add(Me.cmdEdit)
        Me.GroupBox2.Controls.Add(Me.cmdDelete)
        Me.GroupBox2.Controls.Add(Me.cmdAdd)
        Me.GroupBox2.Location = New System.Drawing.Point(13, 284)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Size = New System.Drawing.Size(520, 55)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'cmdUndo
        '
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(220, 17)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(74, 29)
        Me.cmdUndo.TabIndex = 13
        Me.cmdUndo.Text = "&Undo"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSave.Location = New System.Drawing.Point(321, 17)
        Me.cmdSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(74, 29)
        Me.cmdSave.TabIndex = 14
        Me.cmdSave.Text = "&Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(119, 17)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(74, 29)
        Me.cmdEdit.TabIndex = 12
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(422, 17)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(74, 29)
        Me.cmdDelete.TabIndex = 15
        Me.cmdDelete.Text = "&Delete"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(18, 17)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(74, 29)
        Me.cmdAdd.TabIndex = 11
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'PRODUCT_WAREHOUSEBindingSource
        '
        Me.PRODUCT_WAREHOUSEBindingSource.DataMember = "PRODUCT_WAREHOUSE"
        Me.PRODUCT_WAREHOUSEBindingSource.DataSource = Me.DS_ADJUSTMENT
        '
        'SP_PRODUCT_WAREHOUSE_DETAILBindingSource
        '
        Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource.DataMember = "SP_PRODUCT_WAREHOUSE_DETAIL"
        Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource.DataSource = Me.DS_PRODUCT_WAREHOUSE
        '
        'SP_PRODUCT_WAREHOUSE_DETAILTableAdapter
        '
        Me.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager1
        '
        Me.TableAdapterManager1.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager1.Connection = Nothing
        Me.TableAdapterManager1.PRODUCT_WAREHOUSETableAdapter = Nothing
        Me.TableAdapterManager1.UpdateOrder = DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'WAREHOUSETableAdapter
        '
        Me.WAREHOUSETableAdapter.ClearBeforeFill = True
        '
        'DS_PARAMETER
        '
        Me.DS_PARAMETER.DataSetName = "DS_PARAMETER"
        Me.DS_PARAMETER.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PARAMETERBindingSource
        '
        Me.PARAMETERBindingSource.DataMember = "PARAMETER"
        Me.PARAMETERBindingSource.DataSource = Me.DS_PARAMETER
        '
        'PARAMETERTableAdapter
        '
        Me.PARAMETERTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager2
        '
        Me.TableAdapterManager2.ACCOUNTTableAdapter = Nothing
        Me.TableAdapterManager2.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager2.PARAMETERTableAdapter = Me.PARAMETERTableAdapter
        Me.TableAdapterManager2.UpdateOrder = DMI_RETAIL_INVENTORY.DS_PARAMETERTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager2.WAREHOUSETableAdapter = Nothing
        '
        'SP_LIST_PRODUCT_GOODSTableAdapter
        '
        Me.SP_LIST_PRODUCT_GOODSTableAdapter.ClearBeforeFill = True
        '
        'frmEntryAdjustment
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(545, 351)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ADJUSTMENTBindingNavigator)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmEntryAdjustment"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Entry Adjustment"
        Me.Text = "Entry Adjustment"
        Me.TransparencyKey = System.Drawing.Color.White
        CType(Me.ADJUSTMENTBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ADJUSTMENTBindingNavigator.ResumeLayout(False)
        Me.ADJUSTMENTBindingNavigator.PerformLayout()
        CType(Me.ADJUSTMENTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_ADJUSTMENT, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_LIST_PRODUCT_GOODSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_PRODUCT_WAREHOUSE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.WAREHOUSEBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmdSearchProduct, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.PRODUCT_WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_PARAMETER, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PARAMETERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DS_ADJUSTMENT As DMI_RETAIL_INVENTORY.DS_ADJUSTMENT
    Friend WithEvents ADJUSTMENTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ADJUSTMENTTableAdapter As DMI_RETAIL_INVENTORY.DS_ADJUSTMENTTableAdapters.ADJUSTMENTTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_INVENTORY.DS_ADJUSTMENTTableAdapters.TableAdapterManager
    Friend WithEvents ADJUSTMENTBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ADJUSTMENT_NOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ADJUSTMENT_DATEDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents QUANTITYTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PRICE_PER_UNITTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DESCRIPTIONTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PRODUCT_IDComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents PRODUCTTableAdapter As DMI_RETAIL_INVENTORY.DS_ADJUSTMENTTableAdapters.PRODUCTTableAdapter
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents PRODUCTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PRODUCT_NAMETextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents cmdSearchProduct As System.Windows.Forms.PictureBox
    Friend WithEvents PRODUCT_WAREHOUSETableAdapter As DMI_RETAIL_INVENTORY.DS_ADJUSTMENTTableAdapters.PRODUCT_WAREHOUSETableAdapter
    Friend WithEvents PRODUCT_WAREHOUSEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DS_PRODUCT_WAREHOUSE As DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSE
    Friend WithEvents SP_PRODUCT_WAREHOUSE_DETAILBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_PRODUCT_WAREHOUSE_DETAILTableAdapter As DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSETableAdapters.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter
    Friend WithEvents TableAdapterManager1 As DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSETableAdapters.TableAdapterManager
    Friend WithEvents txtDTP As System.Windows.Forms.TextBox
    Friend WithEvents WAREHOUSEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents WAREHOUSETableAdapter As DMI_RETAIL_INVENTORY.DS_ADJUSTMENTTableAdapters.WAREHOUSETableAdapter
    Friend WithEvents WAREHOUSE_IDComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents WAREHOUSEBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents DS_PARAMETER As DMI_RETAIL_INVENTORY.DS_PARAMETER
    Friend WithEvents PARAMETERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PARAMETERTableAdapter As DMI_RETAIL_INVENTORY.DS_PARAMETERTableAdapters.PARAMETERTableAdapter
    Friend WithEvents TableAdapterManager2 As DMI_RETAIL_INVENTORY.DS_PARAMETERTableAdapters.TableAdapterManager
    Friend WithEvents SP_LIST_PRODUCT_GOODSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_LIST_PRODUCT_GOODSTableAdapter As DMI_RETAIL_INVENTORY.DS_PRODUCT_WAREHOUSETableAdapters.SP_LIST_PRODUCT_GOODSTableAdapter
    Friend WithEvents lblWarehouse As System.Windows.Forms.Label
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblUnitPrice As System.Windows.Forms.Label
    Friend WithEvents lblQuantity As System.Windows.Forms.Label
    Friend WithEvents lblProduct As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblAdjustmenNo As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
End Class
