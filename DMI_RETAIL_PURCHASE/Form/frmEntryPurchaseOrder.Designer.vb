﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEntryPurchaseOrder
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEntryPurchaseOrder))
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblNoOfItem = New System.Windows.Forms.Label()
        Me.lblDel = New System.Windows.Forms.Label()
        Me.lblF6 = New System.Windows.Forms.Label()
        Me.lblF5 = New System.Windows.Forms.Label()
        Me.dgvPurchaseOrder = New System.Windows.Forms.DataGridView()
        Me.PRODUCT_CODE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRODUCT_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QUANTITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRICE_PER_UNIT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DISCOUNT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DISCOUNT2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.STATUS = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.DELETE = New System.Windows.Forms.DataGridViewImageColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.VENDOR_NAMEComboBox = New System.Windows.Forms.ComboBox()
        Me.VENDORBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_PURCHASE_ORDER = New DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDER()
        Me.dtpRequestDate = New System.Windows.Forms.DateTimePicker()
        Me.dtpOrderDate = New System.Windows.Forms.DateTimePicker()
        Me.txtShipTo = New System.Windows.Forms.TextBox()
        Me.txtPONo = New System.Windows.Forms.TextBox()
        Me.lblVendor = New System.Windows.Forms.Label()
        Me.lblShipTo = New System.Windows.Forms.Label()
        Me.lblRequestDate = New System.Windows.Forms.Label()
        Me.lblOrderDate = New System.Windows.Forms.Label()
        Me.lblPOno = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.lblTax = New System.Windows.Forms.Label()
        Me.TAX_AMOUNTTextBox = New System.Windows.Forms.TextBox()
        Me.TAXPercentageTEXTBOX = New System.Windows.Forms.TextBox()
        Me.lblGrandTotal = New System.Windows.Forms.Label()
        Me.lblDiscount = New System.Windows.Forms.Label()
        Me.GRAND_TOTALTextBox = New System.Windows.Forms.TextBox()
        Me.AMOUNT_DISCOUNTTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.cmdPrint = New System.Windows.Forms.Button()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.VENDORTableAdapter = New DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.VENDORTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.TableAdapterManager()
        Me.PURCHASE_ORDER_HEADERTableAdapter = New DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.PURCHASE_ORDER_HEADERTableAdapter()
        Me.PURCHASE_ORDER_HEADERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PRODUCTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PRODUCTTableAdapter = New DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.PRODUCTTableAdapter()
        Me.PURCHASE_ORDER_DETAILBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PURCHASE_ORDER_DETAILTableAdapter = New DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.PURCHASE_ORDER_DETAILTableAdapter()
        Me.SP_DETAIL_PURCHASE_ORDERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_DETAIL_PURCHASE_ORDERTableAdapter = New DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.SP_DETAIL_PURCHASE_ORDERTableAdapter()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvPurchaseOrder, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.VENDORBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_PURCHASE_ORDER, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.PURCHASE_ORDER_HEADERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PURCHASE_ORDER_DETAILBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_DETAIL_PURCHASE_ORDERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblNoOfItem)
        Me.GroupBox2.Controls.Add(Me.lblDel)
        Me.GroupBox2.Controls.Add(Me.lblF6)
        Me.GroupBox2.Controls.Add(Me.lblF5)
        Me.GroupBox2.Controls.Add(Me.dgvPurchaseOrder)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 152)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(765, 260)
        Me.GroupBox2.TabIndex = 18
        Me.GroupBox2.TabStop = False
        '
        'lblNoOfItem
        '
        Me.lblNoOfItem.AutoSize = True
        Me.lblNoOfItem.Location = New System.Drawing.Point(20, 237)
        Me.lblNoOfItem.Name = "lblNoOfItem"
        Me.lblNoOfItem.Size = New System.Drawing.Size(107, 15)
        Me.lblNoOfItem.TabIndex = 40
        Me.lblNoOfItem.Text = "No of Item(s) :  0"
        '
        'lblDel
        '
        Me.lblDel.Location = New System.Drawing.Point(521, 17)
        Me.lblDel.Name = "lblDel"
        Me.lblDel.Size = New System.Drawing.Size(198, 14)
        Me.lblDel.TabIndex = 39
        Me.lblDel.Text = "Del : Remove Product from List"
        Me.lblDel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblF6
        '
        Me.lblF6.Location = New System.Drawing.Point(269, 18)
        Me.lblF6.Name = "lblF6"
        Me.lblF6.Size = New System.Drawing.Size(180, 14)
        Me.lblF6.TabIndex = 38
        Me.lblF6.Text = "F6 : Browse Product by Name"
        Me.lblF6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblF5
        '
        Me.lblF5.Location = New System.Drawing.Point(21, 16)
        Me.lblF5.Name = "lblF5"
        Me.lblF5.Size = New System.Drawing.Size(186, 14)
        Me.lblF5.TabIndex = 37
        Me.lblF5.Text = "F5 : Browse Product by Code"
        Me.lblF5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'dgvPurchaseOrder
        '
        Me.dgvPurchaseOrder.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPurchaseOrder.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPurchaseOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPurchaseOrder.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PRODUCT_CODE, Me.PRODUCT_NAME, Me.QUANTITY, Me.PRICE_PER_UNIT, Me.DISCOUNT, Me.DISCOUNT2, Me.TOTAL, Me.STATUS, Me.DELETE})
        Me.dgvPurchaseOrder.Location = New System.Drawing.Point(18, 37)
        Me.dgvPurchaseOrder.Name = "dgvPurchaseOrder"
        Me.dgvPurchaseOrder.Size = New System.Drawing.Size(729, 193)
        Me.dgvPurchaseOrder.TabIndex = 16
        '
        'PRODUCT_CODE
        '
        Me.PRODUCT_CODE.HeaderText = "Product Code"
        Me.PRODUCT_CODE.Name = "PRODUCT_CODE"
        Me.PRODUCT_CODE.Width = 110
        '
        'PRODUCT_NAME
        '
        Me.PRODUCT_NAME.HeaderText = "Product Name"
        Me.PRODUCT_NAME.Name = "PRODUCT_NAME"
        Me.PRODUCT_NAME.ReadOnly = True
        Me.PRODUCT_NAME.Width = 110
        '
        'QUANTITY
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.QUANTITY.DefaultCellStyle = DataGridViewCellStyle2
        Me.QUANTITY.HeaderText = "Qty"
        Me.QUANTITY.Name = "QUANTITY"
        Me.QUANTITY.Width = 50
        '
        'PRICE_PER_UNIT
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N0"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.PRICE_PER_UNIT.DefaultCellStyle = DataGridViewCellStyle3
        Me.PRICE_PER_UNIT.HeaderText = "Unit Price"
        Me.PRICE_PER_UNIT.Name = "PRICE_PER_UNIT"
        Me.PRICE_PER_UNIT.Width = 80
        '
        'DISCOUNT
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.DISCOUNT.DefaultCellStyle = DataGridViewCellStyle4
        Me.DISCOUNT.HeaderText = "Disc (%)"
        Me.DISCOUNT.Name = "DISCOUNT"
        Me.DISCOUNT.Width = 50
        '
        'DISCOUNT2
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N0"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.DISCOUNT2.DefaultCellStyle = DataGridViewCellStyle5
        Me.DISCOUNT2.HeaderText = "Disc (Rp)"
        Me.DISCOUNT2.Name = "DISCOUNT2"
        Me.DISCOUNT2.Width = 75
        '
        'TOTAL
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N0"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.TOTAL.DefaultCellStyle = DataGridViewCellStyle6
        Me.TOTAL.HeaderText = "Total"
        Me.TOTAL.Name = "TOTAL"
        Me.TOTAL.ReadOnly = True
        Me.TOTAL.Width = 90
        '
        'STATUS
        '
        Me.STATUS.HeaderText = "Status"
        Me.STATUS.Items.AddRange(New Object() {"Open", "Close"})
        Me.STATUS.Name = "STATUS"
        Me.STATUS.Width = 60
        '
        'DELETE
        '
        Me.DELETE.HeaderText = "Del"
        Me.DELETE.Image = Global.DMI_RETAIL_PURCHASE.My.Resources.Resources.BindingNavigatorDeleteItem_Image
        Me.DELETE.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch
        Me.DELETE.Name = "DELETE"
        Me.DELETE.Width = 50
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.VENDOR_NAMEComboBox)
        Me.GroupBox1.Controls.Add(Me.dtpRequestDate)
        Me.GroupBox1.Controls.Add(Me.dtpOrderDate)
        Me.GroupBox1.Controls.Add(Me.txtShipTo)
        Me.GroupBox1.Controls.Add(Me.txtPONo)
        Me.GroupBox1.Controls.Add(Me.lblVendor)
        Me.GroupBox1.Controls.Add(Me.lblShipTo)
        Me.GroupBox1.Controls.Add(Me.lblRequestDate)
        Me.GroupBox1.Controls.Add(Me.lblOrderDate)
        Me.GroupBox1.Controls.Add(Me.lblPOno)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(765, 143)
        Me.GroupBox1.TabIndex = 19
        Me.GroupBox1.TabStop = False
        '
        'VENDOR_NAMEComboBox
        '
        Me.VENDOR_NAMEComboBox.DataSource = Me.VENDORBindingSource
        Me.VENDOR_NAMEComboBox.DisplayMember = "VENDOR_NAME"
        Me.VENDOR_NAMEComboBox.FormattingEnabled = True
        Me.VENDOR_NAMEComboBox.Location = New System.Drawing.Point(471, 100)
        Me.VENDOR_NAMEComboBox.Name = "VENDOR_NAMEComboBox"
        Me.VENDOR_NAMEComboBox.Size = New System.Drawing.Size(274, 23)
        Me.VENDOR_NAMEComboBox.TabIndex = 10
        Me.VENDOR_NAMEComboBox.ValueMember = "VENDOR_ID"
        '
        'VENDORBindingSource
        '
        Me.VENDORBindingSource.DataMember = "VENDOR"
        Me.VENDORBindingSource.DataSource = Me.DS_PURCHASE_ORDER
        '
        'DS_PURCHASE_ORDER
        '
        Me.DS_PURCHASE_ORDER.DataSetName = "DS_PURCHASE_ORDER"
        Me.DS_PURCHASE_ORDER.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'dtpRequestDate
        '
        Me.dtpRequestDate.CustomFormat = "dd-MMM-yyyy"
        Me.dtpRequestDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpRequestDate.Location = New System.Drawing.Point(174, 100)
        Me.dtpRequestDate.Name = "dtpRequestDate"
        Me.dtpRequestDate.Size = New System.Drawing.Size(138, 22)
        Me.dtpRequestDate.TabIndex = 8
        '
        'dtpOrderDate
        '
        Me.dtpOrderDate.CustomFormat = "dd-MMM-yyyy"
        Me.dtpOrderDate.Enabled = False
        Me.dtpOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpOrderDate.Location = New System.Drawing.Point(174, 55)
        Me.dtpOrderDate.Name = "dtpOrderDate"
        Me.dtpOrderDate.Size = New System.Drawing.Size(138, 22)
        Me.dtpOrderDate.TabIndex = 7
        '
        'txtShipTo
        '
        Me.txtShipTo.Location = New System.Drawing.Point(471, 15)
        Me.txtShipTo.Multiline = True
        Me.txtShipTo.Name = "txtShipTo"
        Me.txtShipTo.Size = New System.Drawing.Size(274, 78)
        Me.txtShipTo.TabIndex = 6
        '
        'txtPONo
        '
        Me.txtPONo.Location = New System.Drawing.Point(174, 15)
        Me.txtPONo.Name = "txtPONo"
        Me.txtPONo.ReadOnly = True
        Me.txtPONo.Size = New System.Drawing.Size(138, 22)
        Me.txtPONo.TabIndex = 5
        '
        'lblVendor
        '
        Me.lblVendor.AutoSize = True
        Me.lblVendor.Location = New System.Drawing.Point(397, 106)
        Me.lblVendor.Name = "lblVendor"
        Me.lblVendor.Size = New System.Drawing.Size(50, 15)
        Me.lblVendor.TabIndex = 4
        Me.lblVendor.Text = "Vendor"
        '
        'lblShipTo
        '
        Me.lblShipTo.AutoSize = True
        Me.lblShipTo.Location = New System.Drawing.Point(395, 18)
        Me.lblShipTo.Name = "lblShipTo"
        Me.lblShipTo.Size = New System.Drawing.Size(52, 15)
        Me.lblShipTo.TabIndex = 3
        Me.lblShipTo.Text = "Ship To"
        '
        'lblRequestDate
        '
        Me.lblRequestDate.AutoSize = True
        Me.lblRequestDate.Location = New System.Drawing.Point(19, 106)
        Me.lblRequestDate.Name = "lblRequestDate"
        Me.lblRequestDate.Size = New System.Drawing.Size(139, 15)
        Me.lblRequestDate.TabIndex = 2
        Me.lblRequestDate.Text = "Delivery Request Date"
        '
        'lblOrderDate
        '
        Me.lblOrderDate.AutoSize = True
        Me.lblOrderDate.Location = New System.Drawing.Point(19, 61)
        Me.lblOrderDate.Name = "lblOrderDate"
        Me.lblOrderDate.Size = New System.Drawing.Size(131, 15)
        Me.lblOrderDate.TabIndex = 1
        Me.lblOrderDate.Text = "Purchase Order Date"
        '
        'lblPOno
        '
        Me.lblPOno.AutoSize = True
        Me.lblPOno.Location = New System.Drawing.Point(19, 18)
        Me.lblPOno.Name = "lblPOno"
        Me.lblPOno.Size = New System.Drawing.Size(119, 15)
        Me.lblPOno.TabIndex = 0
        Me.lblPOno.Text = "Purchase Order No"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lblTax)
        Me.GroupBox3.Controls.Add(Me.TAX_AMOUNTTextBox)
        Me.GroupBox3.Controls.Add(Me.TAXPercentageTEXTBOX)
        Me.GroupBox3.Controls.Add(Me.lblGrandTotal)
        Me.GroupBox3.Controls.Add(Me.lblDiscount)
        Me.GroupBox3.Controls.Add(Me.GRAND_TOTALTextBox)
        Me.GroupBox3.Controls.Add(Me.AMOUNT_DISCOUNTTextBox)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 412)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(765, 80)
        Me.GroupBox3.TabIndex = 20
        Me.GroupBox3.TabStop = False
        '
        'lblTax
        '
        Me.lblTax.Location = New System.Drawing.Point(73, 50)
        Me.lblTax.Name = "lblTax"
        Me.lblTax.Size = New System.Drawing.Size(59, 14)
        Me.lblTax.TabIndex = 49
        Me.lblTax.Text = "Tax [F2]"
        Me.lblTax.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'TAX_AMOUNTTextBox
        '
        Me.TAX_AMOUNTTextBox.Location = New System.Drawing.Point(188, 47)
        Me.TAX_AMOUNTTextBox.MaxLength = 9
        Me.TAX_AMOUNTTextBox.Name = "TAX_AMOUNTTextBox"
        Me.TAX_AMOUNTTextBox.Size = New System.Drawing.Size(123, 22)
        Me.TAX_AMOUNTTextBox.TabIndex = 16
        Me.TAX_AMOUNTTextBox.Text = "0"
        Me.TAX_AMOUNTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TAXPercentageTEXTBOX
        '
        Me.TAXPercentageTEXTBOX.Location = New System.Drawing.Point(138, 47)
        Me.TAXPercentageTEXTBOX.MaxLength = 5
        Me.TAXPercentageTEXTBOX.Name = "TAXPercentageTEXTBOX"
        Me.TAXPercentageTEXTBOX.Size = New System.Drawing.Size(44, 22)
        Me.TAXPercentageTEXTBOX.TabIndex = 15
        Me.TAXPercentageTEXTBOX.Text = "0.00"
        Me.TAXPercentageTEXTBOX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblGrandTotal
        '
        Me.lblGrandTotal.Location = New System.Drawing.Point(435, 25)
        Me.lblGrandTotal.Name = "lblGrandTotal"
        Me.lblGrandTotal.Size = New System.Drawing.Size(85, 14)
        Me.lblGrandTotal.TabIndex = 37
        Me.lblGrandTotal.Text = "Grand Total"
        Me.lblGrandTotal.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblDiscount
        '
        Me.lblDiscount.Location = New System.Drawing.Point(108, 22)
        Me.lblDiscount.Name = "lblDiscount"
        Me.lblDiscount.Size = New System.Drawing.Size(75, 14)
        Me.lblDiscount.TabIndex = 38
        Me.lblDiscount.Text = "Discount"
        Me.lblDiscount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GRAND_TOTALTextBox
        '
        Me.GRAND_TOTALTextBox.Location = New System.Drawing.Point(526, 22)
        Me.GRAND_TOTALTextBox.Name = "GRAND_TOTALTextBox"
        Me.GRAND_TOTALTextBox.ReadOnly = True
        Me.GRAND_TOTALTextBox.Size = New System.Drawing.Size(113, 22)
        Me.GRAND_TOTALTextBox.TabIndex = 17
        Me.GRAND_TOTALTextBox.Text = "0"
        Me.GRAND_TOTALTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AMOUNT_DISCOUNTTextBox
        '
        Me.AMOUNT_DISCOUNTTextBox.Location = New System.Drawing.Point(188, 19)
        Me.AMOUNT_DISCOUNTTextBox.Name = "AMOUNT_DISCOUNTTextBox"
        Me.AMOUNT_DISCOUNTTextBox.ReadOnly = True
        Me.AMOUNT_DISCOUNTTextBox.Size = New System.Drawing.Size(123, 22)
        Me.AMOUNT_DISCOUNTTextBox.TabIndex = 10
        Me.AMOUNT_DISCOUNTTextBox.Text = "0"
        Me.AMOUNT_DISCOUNTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.cmdPrint)
        Me.GroupBox4.Controls.Add(Me.cmdUndo)
        Me.GroupBox4.Controls.Add(Me.cmdSave)
        Me.GroupBox4.Controls.Add(Me.cmdEdit)
        Me.GroupBox4.Controls.Add(Me.cmdDelete)
        Me.GroupBox4.Controls.Add(Me.cmdAdd)
        Me.GroupBox4.Location = New System.Drawing.Point(74, 492)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox4.Size = New System.Drawing.Size(636, 55)
        Me.GroupBox4.TabIndex = 50
        Me.GroupBox4.TabStop = False
        '
        'cmdPrint
        '
        Me.cmdPrint.AutoSize = True
        Me.cmdPrint.Font = New System.Drawing.Font("Lucida Bright", 7.0!)
        Me.cmdPrint.Image = Global.DMI_RETAIL_PURCHASE.My.Resources.Resources.Printer
        Me.cmdPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdPrint.Location = New System.Drawing.Point(517, 11)
        Me.cmdPrint.Name = "cmdPrint"
        Me.cmdPrint.Size = New System.Drawing.Size(108, 38)
        Me.cmdPrint.TabIndex = 25
        Me.cmdPrint.Text = "Save && &Print" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F12]"
        Me.cmdPrint.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.cmdPrint.UseVisualStyleBackColor = True
        '
        'cmdUndo
        '
        Me.cmdUndo.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(216, 11)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(83, 38)
        Me.cmdUndo.TabIndex = 21
        Me.cmdUndo.Text = "&Undo" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F9]"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSave.Location = New System.Drawing.Point(317, 11)
        Me.cmdSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(83, 38)
        Me.cmdSave.TabIndex = 22
        Me.cmdSave.Text = "&Save" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F10]"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(115, 11)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(83, 38)
        Me.cmdEdit.TabIndex = 20
        Me.cmdEdit.Text = "&Edit" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F8]"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(418, 11)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(83, 38)
        Me.cmdDelete.TabIndex = 23
        Me.cmdDelete.Text = "&Delete" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F11]"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(14, 11)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(83, 38)
        Me.cmdAdd.TabIndex = 19
        Me.cmdAdd.Text = "&Add" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F7]"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'VENDORTableAdapter
        '
        Me.VENDORTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DELIVERY_PURCHASE_ORDER_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.DELIVERY_PURCHASE_ORDER_HEADERTableAdapter = Nothing
        Me.TableAdapterManager.PRODUCTTableAdapter = Nothing
        Me.TableAdapterManager.PURCHASE_ORDER_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.PURCHASE_ORDER_HEADERTableAdapter = Me.PURCHASE_ORDER_HEADERTableAdapter
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.VENDORTableAdapter = Me.VENDORTableAdapter
        '
        'PURCHASE_ORDER_HEADERTableAdapter
        '
        Me.PURCHASE_ORDER_HEADERTableAdapter.ClearBeforeFill = True
        '
        'PURCHASE_ORDER_HEADERBindingSource
        '
        Me.PURCHASE_ORDER_HEADERBindingSource.DataMember = "PURCHASE_ORDER_HEADER"
        Me.PURCHASE_ORDER_HEADERBindingSource.DataSource = Me.DS_PURCHASE_ORDER
        '
        'PRODUCTBindingSource
        '
        Me.PRODUCTBindingSource.DataMember = "PRODUCT"
        Me.PRODUCTBindingSource.DataSource = Me.DS_PURCHASE_ORDER
        '
        'PRODUCTTableAdapter
        '
        Me.PRODUCTTableAdapter.ClearBeforeFill = True
        '
        'PURCHASE_ORDER_DETAILBindingSource
        '
        Me.PURCHASE_ORDER_DETAILBindingSource.DataMember = "PURCHASE_ORDER_DETAIL"
        Me.PURCHASE_ORDER_DETAILBindingSource.DataSource = Me.DS_PURCHASE_ORDER
        '
        'PURCHASE_ORDER_DETAILTableAdapter
        '
        Me.PURCHASE_ORDER_DETAILTableAdapter.ClearBeforeFill = True
        '
        'SP_DETAIL_PURCHASE_ORDERBindingSource
        '
        Me.SP_DETAIL_PURCHASE_ORDERBindingSource.DataMember = "SP_DETAIL_PURCHASE_ORDER"
        Me.SP_DETAIL_PURCHASE_ORDERBindingSource.DataSource = Me.DS_PURCHASE_ORDER
        '
        'SP_DETAIL_PURCHASE_ORDERTableAdapter
        '
        Me.SP_DETAIL_PURCHASE_ORDERTableAdapter.ClearBeforeFill = True
        '
        'frmEntryPurchaseOrder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(785, 553)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmEntryPurchaseOrder"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Entry Purchase Order"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvPurchaseOrder, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.VENDORBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_PURCHASE_ORDER, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.PURCHASE_ORDER_HEADERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PURCHASE_ORDER_DETAILBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_DETAIL_PURCHASE_ORDERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblNoOfItem As System.Windows.Forms.Label
    Friend WithEvents lblDel As System.Windows.Forms.Label
    Friend WithEvents lblF6 As System.Windows.Forms.Label
    Friend WithEvents lblF5 As System.Windows.Forms.Label
    Friend WithEvents dgvPurchaseOrder As System.Windows.Forms.DataGridView
    Friend WithEvents PRODUCT_CODE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRODUCT_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QUANTITY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRICE_PER_UNIT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DISCOUNT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DISCOUNT2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STATUS As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents DELETE As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblRequestDate As System.Windows.Forms.Label
    Friend WithEvents lblOrderDate As System.Windows.Forms.Label
    Friend WithEvents lblPOno As System.Windows.Forms.Label
    Friend WithEvents dtpRequestDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpOrderDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtShipTo As System.Windows.Forms.TextBox
    Friend WithEvents txtPONo As System.Windows.Forms.TextBox
    Friend WithEvents lblVendor As System.Windows.Forms.Label
    Friend WithEvents lblShipTo As System.Windows.Forms.Label
    Friend WithEvents DS_PURCHASE_ORDER As DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDER
    Friend WithEvents VENDORBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VENDORTableAdapter As DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.VENDORTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.TableAdapterManager
    Friend WithEvents VENDOR_NAMEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents lblTax As System.Windows.Forms.Label
    Friend WithEvents TAX_AMOUNTTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TAXPercentageTEXTBOX As System.Windows.Forms.TextBox
    Friend WithEvents lblGrandTotal As System.Windows.Forms.Label
    Friend WithEvents lblDiscount As System.Windows.Forms.Label
    Friend WithEvents GRAND_TOTALTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AMOUNT_DISCOUNTTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PURCHASE_ORDER_HEADERTableAdapter As DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.PURCHASE_ORDER_HEADERTableAdapter
    Friend WithEvents PURCHASE_ORDER_HEADERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdPrint As System.Windows.Forms.Button
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents PRODUCTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PRODUCTTableAdapter As DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.PRODUCTTableAdapter
    Friend WithEvents PURCHASE_ORDER_DETAILBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PURCHASE_ORDER_DETAILTableAdapter As DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.PURCHASE_ORDER_DETAILTableAdapter
    Friend WithEvents SP_DETAIL_PURCHASE_ORDERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_DETAIL_PURCHASE_ORDERTableAdapter As DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.SP_DETAIL_PURCHASE_ORDERTableAdapter
End Class
