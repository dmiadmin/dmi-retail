﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEntryPurchase
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEntryPurchase))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmdSearchSaleOrder = New System.Windows.Forms.PictureBox()
        Me.dtpPoDate = New System.Windows.Forms.DateTimePicker()
        Me.lblOrderDate = New System.Windows.Forms.Label()
        Me.lblPoNo = New System.Windows.Forms.Label()
        Me.txtPoNo = New System.Windows.Forms.TextBox()
        Me.lblWarehouse = New System.Windows.Forms.Label()
        Me.lblMaturity = New System.Windows.Forms.Label()
        Me.lblPayment = New System.Windows.Forms.Label()
        Me.lblVendor = New System.Windows.Forms.Label()
        Me.lblPurchaseDate = New System.Windows.Forms.Label()
        Me.lblReceiptNo = New System.Windows.Forms.Label()
        Me.WAREHOUSE_NAMEComboBox = New System.Windows.Forms.ComboBox()
        Me.WAREHOUSEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_PURCHASE = New DMI_RETAIL_PURCHASE.DS_PURCHASE()
        Me.txtDTP = New System.Windows.Forms.TextBox()
        Me.RECEIPT_NOTextBox = New System.Windows.Forms.TextBox()
        Me.PURCHASEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PURCHASE_DATEDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.VENDOR_IDComboBox = New System.Windows.Forms.ComboBox()
        Me.VENDORBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MATURITY_DATEDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.PAYMENT_METHOD_IDComboBox = New System.Windows.Forms.ComboBox()
        Me.PAYMENT_METHODBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PURCHASETableAdapter = New DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.PURCHASETableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.TableAdapterManager()
        Me.PRODUCTTableAdapter = New DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.PRODUCTTableAdapter()
        Me.PURCHASE_DETAILTableAdapter = New DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.PURCHASE_DETAILTableAdapter()
        Me.VENDORTableAdapter = New DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.VENDORTableAdapter()
        Me.WAREHOUSETableAdapter = New DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.WAREHOUSETableAdapter()
        Me.PURCHASEBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.GRAND_TOTALTextBox = New System.Windows.Forms.TextBox()
        Me.REMARKTextBox = New System.Windows.Forms.TextBox()
        Me.PAYMENT_METHODTableAdapter = New DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.PAYMENT_METHODTableAdapter()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblDel = New System.Windows.Forms.Label()
        Me.lblF6 = New System.Windows.Forms.Label()
        Me.lblF5 = New System.Windows.Forms.Label()
        Me.lblNoOfItem = New System.Windows.Forms.Label()
        Me.dgvPurchaseDetail = New System.Windows.Forms.DataGridView()
        Me.PRODUCT_CODE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRODUCT_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QUANTITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRICE_PER_UNIT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DISCOUNT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DISCOUNT2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DELETE = New System.Windows.Forms.DataGridViewImageColumn()
        Me.AMOUNT_DISCOUNTTextBox = New System.Windows.Forms.TextBox()
        Me.PRODUCTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TAX_AMOUNTTextBox = New System.Windows.Forms.TextBox()
        Me.TAXPercentageTEXTBOX = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.lblDp = New System.Windows.Forms.Label()
        Me.lblDiscount = New System.Windows.Forms.Label()
        Me.lblTax = New System.Windows.Forms.Label()
        Me.lblRemark = New System.Windows.Forms.Label()
        Me.DOWN_PAYMENTTextBox = New System.Windows.Forms.TextBox()
        Me.PURCHASE_DETAILBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VIEW_LIST_PURCHASEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VIEW_LIST_PURCHASETableAdapter = New DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.VIEW_LIST_PURCHASETableAdapter()
        Me.VIEW_LIST_PURCHASE_DETAILBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VIEW_LIST_PURCHASE_DETAILTableAdapter = New DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.VIEW_LIST_PURCHASE_DETAILTableAdapter()
        Me.DS_PRODUCT_WAREHOUSE = New DMI_RETAIL_PURCHASE.DS_PRODUCT_WAREHOUSE()
        Me.PRODUCT_WAREHOUSEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PRODUCT_WAREHOUSETableAdapter = New DMI_RETAIL_PURCHASE.DS_PRODUCT_WAREHOUSETableAdapters.PRODUCT_WAREHOUSETableAdapter()
        Me.TableAdapterManager1 = New DMI_RETAIL_PURCHASE.DS_PRODUCT_WAREHOUSETableAdapters.TableAdapterManager()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.PURCHASEWAREHOUSEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PURCHASEWAREHOUSEBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_PURCHASE_ORDER = New DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDER()
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_SELECT_PURCHASE_ORDER_HEADERTableAdapter = New DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.SP_SELECT_PURCHASE_ORDER_HEADERTableAdapter()
        Me.TableAdapterManager2 = New DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.TableAdapterManager()
        Me.SP_SELECT_PURCHASE_ORDER_DETAILBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_SELECT_PURCHASE_ORDER_DETAILTableAdapter = New DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.SP_SELECT_PURCHASE_ORDER_DETAILTableAdapter()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cmdSearchSaleOrder, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_PURCHASE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PURCHASEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VENDORBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PAYMENT_METHODBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PURCHASEBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PURCHASEBindingNavigator.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvPurchaseDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.PURCHASE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VIEW_LIST_PURCHASEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VIEW_LIST_PURCHASE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_PRODUCT_WAREHOUSE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PRODUCT_WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.PURCHASEWAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PURCHASEWAREHOUSEBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_PURCHASE_ORDER, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_SELECT_PURCHASE_ORDER_DETAILBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmdSearchSaleOrder)
        Me.GroupBox1.Controls.Add(Me.dtpPoDate)
        Me.GroupBox1.Controls.Add(Me.lblOrderDate)
        Me.GroupBox1.Controls.Add(Me.lblPoNo)
        Me.GroupBox1.Controls.Add(Me.txtPoNo)
        Me.GroupBox1.Controls.Add(Me.lblWarehouse)
        Me.GroupBox1.Controls.Add(Me.lblMaturity)
        Me.GroupBox1.Controls.Add(Me.lblPayment)
        Me.GroupBox1.Controls.Add(Me.lblVendor)
        Me.GroupBox1.Controls.Add(Me.lblPurchaseDate)
        Me.GroupBox1.Controls.Add(Me.lblReceiptNo)
        Me.GroupBox1.Controls.Add(Me.WAREHOUSE_NAMEComboBox)
        Me.GroupBox1.Controls.Add(Me.txtDTP)
        Me.GroupBox1.Controls.Add(Me.RECEIPT_NOTextBox)
        Me.GroupBox1.Controls.Add(Me.PURCHASE_DATEDateTimePicker)
        Me.GroupBox1.Controls.Add(Me.VENDOR_IDComboBox)
        Me.GroupBox1.Controls.Add(Me.MATURITY_DATEDateTimePicker)
        Me.GroupBox1.Controls.Add(Me.PAYMENT_METHOD_IDComboBox)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 23)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(759, 134)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'cmdSearchSaleOrder
        '
        Me.cmdSearchSaleOrder.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchSaleOrder.Image = CType(resources.GetObject("cmdSearchSaleOrder.Image"), System.Drawing.Image)
        Me.cmdSearchSaleOrder.Location = New System.Drawing.Point(671, 74)
        Me.cmdSearchSaleOrder.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchSaleOrder.Name = "cmdSearchSaleOrder"
        Me.cmdSearchSaleOrder.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearchSaleOrder.TabIndex = 37
        Me.cmdSearchSaleOrder.TabStop = False
        '
        'dtpPoDate
        '
        Me.dtpPoDate.CustomFormat = "dd-MMM-yyyy"
        Me.dtpPoDate.Enabled = False
        Me.dtpPoDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPoDate.Location = New System.Drawing.Point(539, 103)
        Me.dtpPoDate.Name = "dtpPoDate"
        Me.dtpPoDate.Size = New System.Drawing.Size(115, 20)
        Me.dtpPoDate.TabIndex = 31
        '
        'lblOrderDate
        '
        Me.lblOrderDate.Location = New System.Drawing.Point(412, 108)
        Me.lblOrderDate.Name = "lblOrderDate"
        Me.lblOrderDate.Size = New System.Drawing.Size(121, 14)
        Me.lblOrderDate.TabIndex = 30
        Me.lblOrderDate.Text = "Purchase Order Date"
        Me.lblOrderDate.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblPoNo
        '
        Me.lblPoNo.Location = New System.Drawing.Point(417, 78)
        Me.lblPoNo.Name = "lblPoNo"
        Me.lblPoNo.Size = New System.Drawing.Size(116, 14)
        Me.lblPoNo.TabIndex = 29
        Me.lblPoNo.Text = "Purchase Order No"
        Me.lblPoNo.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtPoNo
        '
        Me.txtPoNo.Location = New System.Drawing.Point(539, 75)
        Me.txtPoNo.Name = "txtPoNo"
        Me.txtPoNo.ReadOnly = True
        Me.txtPoNo.Size = New System.Drawing.Size(115, 20)
        Me.txtPoNo.TabIndex = 28
        '
        'lblWarehouse
        '
        Me.lblWarehouse.Location = New System.Drawing.Point(12, 108)
        Me.lblWarehouse.Name = "lblWarehouse"
        Me.lblWarehouse.Size = New System.Drawing.Size(102, 14)
        Me.lblWarehouse.TabIndex = 27
        Me.lblWarehouse.Text = "Warehouse Name"
        Me.lblWarehouse.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblMaturity
        '
        Me.lblMaturity.Location = New System.Drawing.Point(434, 51)
        Me.lblMaturity.Name = "lblMaturity"
        Me.lblMaturity.Size = New System.Drawing.Size(99, 14)
        Me.lblMaturity.TabIndex = 26
        Me.lblMaturity.Text = "Maturity"
        Me.lblMaturity.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblPayment
        '
        Me.lblPayment.Location = New System.Drawing.Point(396, 21)
        Me.lblPayment.Name = "lblPayment"
        Me.lblPayment.Size = New System.Drawing.Size(137, 14)
        Me.lblPayment.TabIndex = 25
        Me.lblPayment.Text = "Payment Method"
        Me.lblPayment.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblVendor
        '
        Me.lblVendor.Location = New System.Drawing.Point(31, 78)
        Me.lblVendor.Name = "lblVendor"
        Me.lblVendor.Size = New System.Drawing.Size(82, 14)
        Me.lblVendor.TabIndex = 24
        Me.lblVendor.Text = "Vendor"
        Me.lblVendor.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblPurchaseDate
        '
        Me.lblPurchaseDate.Location = New System.Drawing.Point(19, 51)
        Me.lblPurchaseDate.Name = "lblPurchaseDate"
        Me.lblPurchaseDate.Size = New System.Drawing.Size(95, 14)
        Me.lblPurchaseDate.TabIndex = 23
        Me.lblPurchaseDate.Text = "Purchase Date"
        Me.lblPurchaseDate.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblReceiptNo
        '
        Me.lblReceiptNo.Location = New System.Drawing.Point(31, 23)
        Me.lblReceiptNo.Name = "lblReceiptNo"
        Me.lblReceiptNo.Size = New System.Drawing.Size(85, 13)
        Me.lblReceiptNo.TabIndex = 22
        Me.lblReceiptNo.Text = "Receipt No"
        Me.lblReceiptNo.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'WAREHOUSE_NAMEComboBox
        '
        Me.WAREHOUSE_NAMEComboBox.DataSource = Me.WAREHOUSEBindingSource
        Me.WAREHOUSE_NAMEComboBox.DisplayMember = "WAREHOUSE_NAME"
        Me.WAREHOUSE_NAMEComboBox.FormattingEnabled = True
        Me.WAREHOUSE_NAMEComboBox.Location = New System.Drawing.Point(120, 105)
        Me.WAREHOUSE_NAMEComboBox.Name = "WAREHOUSE_NAMEComboBox"
        Me.WAREHOUSE_NAMEComboBox.Size = New System.Drawing.Size(200, 22)
        Me.WAREHOUSE_NAMEComboBox.TabIndex = 7
        Me.WAREHOUSE_NAMEComboBox.ValueMember = "WAREHOUSE_ID"
        '
        'WAREHOUSEBindingSource
        '
        Me.WAREHOUSEBindingSource.DataMember = "WAREHOUSE"
        Me.WAREHOUSEBindingSource.DataSource = Me.DS_PURCHASE
        '
        'DS_PURCHASE
        '
        Me.DS_PURCHASE.DataSetName = "DS_PURCHASE"
        Me.DS_PURCHASE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'txtDTP
        '
        Me.txtDTP.Location = New System.Drawing.Point(120, 48)
        Me.txtDTP.Name = "txtDTP"
        Me.txtDTP.Size = New System.Drawing.Size(100, 20)
        Me.txtDTP.TabIndex = 3
        '
        'RECEIPT_NOTextBox
        '
        Me.RECEIPT_NOTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.RECEIPT_NOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PURCHASEBindingSource, "RECEIPT_NO", True))
        Me.RECEIPT_NOTextBox.Location = New System.Drawing.Point(120, 20)
        Me.RECEIPT_NOTextBox.Name = "RECEIPT_NOTextBox"
        Me.RECEIPT_NOTextBox.Size = New System.Drawing.Size(115, 20)
        Me.RECEIPT_NOTextBox.TabIndex = 1
        Me.RECEIPT_NOTextBox.Tag = "M"
        '
        'PURCHASEBindingSource
        '
        Me.PURCHASEBindingSource.DataMember = "PURCHASE"
        Me.PURCHASEBindingSource.DataSource = Me.DS_PURCHASE
        '
        'PURCHASE_DATEDateTimePicker
        '
        Me.PURCHASE_DATEDateTimePicker.CustomFormat = "dd-MMM-yyyy"
        Me.PURCHASE_DATEDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.PURCHASEBindingSource, "PURCHASE_DATE", True))
        Me.PURCHASE_DATEDateTimePicker.Enabled = False
        Me.PURCHASE_DATEDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.PURCHASE_DATEDateTimePicker.Location = New System.Drawing.Point(120, 48)
        Me.PURCHASE_DATEDateTimePicker.Name = "PURCHASE_DATEDateTimePicker"
        Me.PURCHASE_DATEDateTimePicker.Size = New System.Drawing.Size(115, 20)
        Me.PURCHASE_DATEDateTimePicker.TabIndex = 4
        Me.PURCHASE_DATEDateTimePicker.Tag = "M"
        Me.PURCHASE_DATEDateTimePicker.Visible = False
        '
        'VENDOR_IDComboBox
        '
        Me.VENDOR_IDComboBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.VENDOR_IDComboBox.DataSource = Me.VENDORBindingSource
        Me.VENDOR_IDComboBox.DisplayMember = "VENDOR_NAME"
        Me.VENDOR_IDComboBox.FormattingEnabled = True
        Me.VENDOR_IDComboBox.Location = New System.Drawing.Point(120, 75)
        Me.VENDOR_IDComboBox.Name = "VENDOR_IDComboBox"
        Me.VENDOR_IDComboBox.Size = New System.Drawing.Size(200, 22)
        Me.VENDOR_IDComboBox.TabIndex = 6
        Me.VENDOR_IDComboBox.Tag = "M"
        Me.VENDOR_IDComboBox.ValueMember = "VENDOR_ID"
        '
        'VENDORBindingSource
        '
        Me.VENDORBindingSource.DataMember = "VENDOR"
        Me.VENDORBindingSource.DataSource = Me.DS_PURCHASE
        '
        'MATURITY_DATEDateTimePicker
        '
        Me.MATURITY_DATEDateTimePicker.CustomFormat = "dd-MMM-yyyy"
        Me.MATURITY_DATEDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.PURCHASEBindingSource, "MATURITY_DATE", True))
        Me.MATURITY_DATEDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.MATURITY_DATEDateTimePicker.Location = New System.Drawing.Point(539, 48)
        Me.MATURITY_DATEDateTimePicker.Name = "MATURITY_DATEDateTimePicker"
        Me.MATURITY_DATEDateTimePicker.Size = New System.Drawing.Size(115, 20)
        Me.MATURITY_DATEDateTimePicker.TabIndex = 5
        '
        'PAYMENT_METHOD_IDComboBox
        '
        Me.PAYMENT_METHOD_IDComboBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PAYMENT_METHOD_IDComboBox.DataSource = Me.PAYMENT_METHODBindingSource
        Me.PAYMENT_METHOD_IDComboBox.DisplayMember = "PAYMENT_METHOD_NAME"
        Me.PAYMENT_METHOD_IDComboBox.FormattingEnabled = True
        Me.PAYMENT_METHOD_IDComboBox.Location = New System.Drawing.Point(539, 18)
        Me.PAYMENT_METHOD_IDComboBox.Name = "PAYMENT_METHOD_IDComboBox"
        Me.PAYMENT_METHOD_IDComboBox.Size = New System.Drawing.Size(200, 22)
        Me.PAYMENT_METHOD_IDComboBox.TabIndex = 2
        Me.PAYMENT_METHOD_IDComboBox.Tag = "M"
        Me.PAYMENT_METHOD_IDComboBox.ValueMember = "PAYMENT_METHOD_ID"
        '
        'PAYMENT_METHODBindingSource
        '
        Me.PAYMENT_METHODBindingSource.DataMember = "PAYMENT_METHOD"
        Me.PAYMENT_METHODBindingSource.DataSource = Me.DS_PURCHASE
        '
        'PURCHASETableAdapter
        '
        Me.PURCHASETableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.PRODUCTTableAdapter = Me.PRODUCTTableAdapter
        Me.TableAdapterManager.PURCHASE_DETAILTableAdapter = Me.PURCHASE_DETAILTableAdapter
        Me.TableAdapterManager.PURCHASETableAdapter = Me.PURCHASETableAdapter
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.VENDORTableAdapter = Me.VENDORTableAdapter
        Me.TableAdapterManager.WAREHOUSETableAdapter = Me.WAREHOUSETableAdapter
        '
        'PRODUCTTableAdapter
        '
        Me.PRODUCTTableAdapter.ClearBeforeFill = True
        '
        'PURCHASE_DETAILTableAdapter
        '
        Me.PURCHASE_DETAILTableAdapter.ClearBeforeFill = True
        '
        'VENDORTableAdapter
        '
        Me.VENDORTableAdapter.ClearBeforeFill = True
        '
        'WAREHOUSETableAdapter
        '
        Me.WAREHOUSETableAdapter.ClearBeforeFill = True
        '
        'PURCHASEBindingNavigator
        '
        Me.PURCHASEBindingNavigator.AddNewItem = Nothing
        Me.PURCHASEBindingNavigator.BindingSource = Me.PURCHASEBindingSource
        Me.PURCHASEBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.PURCHASEBindingNavigator.DeleteItem = Nothing
        Me.PURCHASEBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.PURCHASEBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.PURCHASEBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.PURCHASEBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.PURCHASEBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.PURCHASEBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.PURCHASEBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.PURCHASEBindingNavigator.Name = "PURCHASEBindingNavigator"
        Me.PURCHASEBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.PURCHASEBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.PURCHASEBindingNavigator.Size = New System.Drawing.Size(791, 25)
        Me.PURCHASEBindingNavigator.TabIndex = 1
        Me.PURCHASEBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'GRAND_TOTALTextBox
        '
        Me.GRAND_TOTALTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PURCHASEBindingSource, "GRAND_TOTAL", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.GRAND_TOTALTextBox.Location = New System.Drawing.Point(630, 50)
        Me.GRAND_TOTALTextBox.Name = "GRAND_TOTALTextBox"
        Me.GRAND_TOTALTextBox.Size = New System.Drawing.Size(113, 20)
        Me.GRAND_TOTALTextBox.TabIndex = 16
        Me.GRAND_TOTALTextBox.Text = "0"
        Me.GRAND_TOTALTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'REMARKTextBox
        '
        Me.REMARKTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PURCHASEBindingSource, "REMARK", True))
        Me.REMARKTextBox.Location = New System.Drawing.Point(82, 19)
        Me.REMARKTextBox.Multiline = True
        Me.REMARKTextBox.Name = "REMARKTextBox"
        Me.REMARKTextBox.Size = New System.Drawing.Size(197, 51)
        Me.REMARKTextBox.TabIndex = 11
        '
        'PAYMENT_METHODTableAdapter
        '
        Me.PAYMENT_METHODTableAdapter.ClearBeforeFill = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblDel)
        Me.GroupBox2.Controls.Add(Me.lblF6)
        Me.GroupBox2.Controls.Add(Me.lblF5)
        Me.GroupBox2.Controls.Add(Me.lblNoOfItem)
        Me.GroupBox2.Controls.Add(Me.dgvPurchaseDetail)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 157)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(750, 291)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        '
        'lblDel
        '
        Me.lblDel.AutoSize = True
        Me.lblDel.Location = New System.Drawing.Point(450, 19)
        Me.lblDel.Name = "lblDel"
        Me.lblDel.Size = New System.Drawing.Size(174, 14)
        Me.lblDel.TabIndex = 30
        Me.lblDel.Text = "Del : Remove Product from list"
        '
        'lblF6
        '
        Me.lblF6.AutoSize = True
        Me.lblF6.Location = New System.Drawing.Point(225, 19)
        Me.lblF6.Name = "lblF6"
        Me.lblF6.Size = New System.Drawing.Size(165, 14)
        Me.lblF6.TabIndex = 29
        Me.lblF6.Text = "F6 : Browse Product by Name"
        '
        'lblF5
        '
        Me.lblF5.AutoSize = True
        Me.lblF5.Location = New System.Drawing.Point(3, 18)
        Me.lblF5.Name = "lblF5"
        Me.lblF5.Size = New System.Drawing.Size(162, 14)
        Me.lblF5.TabIndex = 28
        Me.lblF5.Text = "F5 : Browse Product by Code"
        '
        'lblNoOfItem
        '
        Me.lblNoOfItem.AutoSize = True
        Me.lblNoOfItem.Location = New System.Drawing.Point(7, 269)
        Me.lblNoOfItem.Name = "lblNoOfItem"
        Me.lblNoOfItem.Size = New System.Drawing.Size(96, 14)
        Me.lblNoOfItem.TabIndex = 19
        Me.lblNoOfItem.Text = "No of Item(s) :  0"
        '
        'dgvPurchaseDetail
        '
        Me.dgvPurchaseDetail.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPurchaseDetail.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPurchaseDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPurchaseDetail.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PRODUCT_CODE, Me.PRODUCT_NAME, Me.QUANTITY, Me.PRICE_PER_UNIT, Me.DISCOUNT, Me.DISCOUNT2, Me.TOTAL, Me.DELETE})
        Me.dgvPurchaseDetail.Location = New System.Drawing.Point(6, 36)
        Me.dgvPurchaseDetail.Name = "dgvPurchaseDetail"
        Me.dgvPurchaseDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvPurchaseDetail.Size = New System.Drawing.Size(737, 224)
        Me.dgvPurchaseDetail.TabIndex = 10
        '
        'PRODUCT_CODE
        '
        Me.PRODUCT_CODE.HeaderText = "Product Code"
        Me.PRODUCT_CODE.Name = "PRODUCT_CODE"
        Me.PRODUCT_CODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'PRODUCT_NAME
        '
        Me.PRODUCT_NAME.HeaderText = "Product Name"
        Me.PRODUCT_NAME.Name = "PRODUCT_NAME"
        Me.PRODUCT_NAME.ReadOnly = True
        Me.PRODUCT_NAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.PRODUCT_NAME.Width = 180
        '
        'QUANTITY
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.QUANTITY.DefaultCellStyle = DataGridViewCellStyle2
        Me.QUANTITY.HeaderText = "Quantity"
        Me.QUANTITY.Name = "QUANTITY"
        Me.QUANTITY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.QUANTITY.Width = 60
        '
        'PRICE_PER_UNIT
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N0"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.PRICE_PER_UNIT.DefaultCellStyle = DataGridViewCellStyle3
        Me.PRICE_PER_UNIT.HeaderText = "Unit Price"
        Me.PRICE_PER_UNIT.Name = "PRICE_PER_UNIT"
        Me.PRICE_PER_UNIT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.PRICE_PER_UNIT.Width = 75
        '
        'DISCOUNT
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.DISCOUNT.DefaultCellStyle = DataGridViewCellStyle4
        Me.DISCOUNT.HeaderText = "Discount (%)"
        Me.DISCOUNT.Name = "DISCOUNT"
        Me.DISCOUNT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DISCOUNT.Width = 60
        '
        'DISCOUNT2
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N0"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.DISCOUNT2.DefaultCellStyle = DataGridViewCellStyle5
        Me.DISCOUNT2.HeaderText = "Discount (Rp)"
        Me.DISCOUNT2.Name = "DISCOUNT2"
        Me.DISCOUNT2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DISCOUNT2.Width = 75
        '
        'TOTAL
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N0"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.TOTAL.DefaultCellStyle = DataGridViewCellStyle6
        Me.TOTAL.HeaderText = "Total"
        Me.TOTAL.Name = "TOTAL"
        Me.TOTAL.ReadOnly = True
        Me.TOTAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.TOTAL.Width = 80
        '
        'DELETE
        '
        Me.DELETE.HeaderText = "Del"
        Me.DELETE.Image = Global.DMI_RETAIL_PURCHASE.My.Resources.Resources.BindingNavigatorDeleteItem_Image
        Me.DELETE.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch
        Me.DELETE.Name = "DELETE"
        Me.DELETE.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DELETE.Width = 40
        '
        'AMOUNT_DISCOUNTTextBox
        '
        Me.AMOUNT_DISCOUNTTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PURCHASEBindingSource, "AMOUNT_DISCOUNT", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.AMOUNT_DISCOUNTTextBox.Location = New System.Drawing.Point(396, 16)
        Me.AMOUNT_DISCOUNTTextBox.Name = "AMOUNT_DISCOUNTTextBox"
        Me.AMOUNT_DISCOUNTTextBox.Size = New System.Drawing.Size(113, 20)
        Me.AMOUNT_DISCOUNTTextBox.TabIndex = 12
        Me.AMOUNT_DISCOUNTTextBox.Text = "0"
        Me.AMOUNT_DISCOUNTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'PRODUCTBindingSource
        '
        Me.PRODUCTBindingSource.DataMember = "PRODUCT"
        Me.PRODUCTBindingSource.DataSource = Me.DS_PURCHASE
        '
        'TAX_AMOUNTTextBox
        '
        Me.TAX_AMOUNTTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PURCHASEBindingSource, "TAX_AMOUNT", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.TAX_AMOUNTTextBox.Location = New System.Drawing.Point(396, 50)
        Me.TAX_AMOUNTTextBox.MaxLength = 50
        Me.TAX_AMOUNTTextBox.Name = "TAX_AMOUNTTextBox"
        Me.TAX_AMOUNTTextBox.Size = New System.Drawing.Size(113, 20)
        Me.TAX_AMOUNTTextBox.TabIndex = 14
        Me.TAX_AMOUNTTextBox.Text = "0"
        Me.TAX_AMOUNTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TAXPercentageTEXTBOX
        '
        Me.TAXPercentageTEXTBOX.Location = New System.Drawing.Point(346, 50)
        Me.TAXPercentageTEXTBOX.Name = "TAXPercentageTEXTBOX"
        Me.TAXPercentageTEXTBOX.Size = New System.Drawing.Size(44, 20)
        Me.TAXPercentageTEXTBOX.TabIndex = 13
        Me.TAXPercentageTEXTBOX.Text = "0.00"
        Me.TAXPercentageTEXTBOX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lblTotal)
        Me.GroupBox3.Controls.Add(Me.lblDp)
        Me.GroupBox3.Controls.Add(Me.lblDiscount)
        Me.GroupBox3.Controls.Add(Me.lblTax)
        Me.GroupBox3.Controls.Add(Me.lblRemark)
        Me.GroupBox3.Controls.Add(Me.DOWN_PAYMENTTextBox)
        Me.GroupBox3.Controls.Add(Me.REMARKTextBox)
        Me.GroupBox3.Controls.Add(Me.TAXPercentageTEXTBOX)
        Me.GroupBox3.Controls.Add(Me.AMOUNT_DISCOUNTTextBox)
        Me.GroupBox3.Controls.Add(Me.TAX_AMOUNTTextBox)
        Me.GroupBox3.Controls.Add(Me.GRAND_TOTALTextBox)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 450)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(750, 90)
        Me.GroupBox3.TabIndex = 1
        Me.GroupBox3.TabStop = False
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(555, 53)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(69, 14)
        Me.lblTotal.TabIndex = 19
        Me.lblTotal.Text = "Grand Total"
        '
        'lblDp
        '
        Me.lblDp.AutoSize = True
        Me.lblDp.Location = New System.Drawing.Point(538, 19)
        Me.lblDp.Name = "lblDp"
        Me.lblDp.Size = New System.Drawing.Size(89, 28)
        Me.lblDp.TabIndex = 18
        Me.lblDp.Text = "Down Payment" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F3]"
        '
        'lblDiscount
        '
        Me.lblDiscount.AutoSize = True
        Me.lblDiscount.Location = New System.Drawing.Point(333, 19)
        Me.lblDiscount.Name = "lblDiscount"
        Me.lblDiscount.Size = New System.Drawing.Size(57, 14)
        Me.lblDiscount.TabIndex = 16
        Me.lblDiscount.Text = "Discount"
        '
        'lblTax
        '
        Me.lblTax.Location = New System.Drawing.Point(278, 53)
        Me.lblTax.Name = "lblTax"
        Me.lblTax.Size = New System.Drawing.Size(66, 28)
        Me.lblTax.TabIndex = 17
        Me.lblTax.Text = "Tax [F2]"
        Me.lblTax.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblRemark
        '
        Me.lblRemark.AutoSize = True
        Me.lblRemark.Location = New System.Drawing.Point(6, 19)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(52, 28)
        Me.lblRemark.TabIndex = 16
        Me.lblRemark.Text = " Remark" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " [F1]"
        '
        'DOWN_PAYMENTTextBox
        '
        Me.DOWN_PAYMENTTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.PURCHASEBindingSource, "DOWN_PAYMENT", True))
        Me.DOWN_PAYMENTTextBox.Location = New System.Drawing.Point(630, 19)
        Me.DOWN_PAYMENTTextBox.MaxLength = 9
        Me.DOWN_PAYMENTTextBox.Name = "DOWN_PAYMENTTextBox"
        Me.DOWN_PAYMENTTextBox.Size = New System.Drawing.Size(113, 20)
        Me.DOWN_PAYMENTTextBox.TabIndex = 15
        Me.DOWN_PAYMENTTextBox.Text = "0"
        Me.DOWN_PAYMENTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'PURCHASE_DETAILBindingSource
        '
        Me.PURCHASE_DETAILBindingSource.DataMember = "PURCHASE_PURCHASE_DETAIL"
        Me.PURCHASE_DETAILBindingSource.DataSource = Me.PURCHASEBindingSource
        '
        'VIEW_LIST_PURCHASEBindingSource
        '
        Me.VIEW_LIST_PURCHASEBindingSource.DataMember = "VIEW_LIST_PURCHASE"
        Me.VIEW_LIST_PURCHASEBindingSource.DataSource = Me.DS_PURCHASE
        '
        'VIEW_LIST_PURCHASETableAdapter
        '
        Me.VIEW_LIST_PURCHASETableAdapter.ClearBeforeFill = True
        '
        'VIEW_LIST_PURCHASE_DETAILBindingSource
        '
        Me.VIEW_LIST_PURCHASE_DETAILBindingSource.DataMember = "VIEW_LIST_PURCHASE_DETAIL"
        Me.VIEW_LIST_PURCHASE_DETAILBindingSource.DataSource = Me.DS_PURCHASE
        '
        'VIEW_LIST_PURCHASE_DETAILTableAdapter
        '
        Me.VIEW_LIST_PURCHASE_DETAILTableAdapter.ClearBeforeFill = True
        '
        'DS_PRODUCT_WAREHOUSE
        '
        Me.DS_PRODUCT_WAREHOUSE.DataSetName = "DS_PRODUCT_WAREHOUSE"
        Me.DS_PRODUCT_WAREHOUSE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PRODUCT_WAREHOUSEBindingSource
        '
        Me.PRODUCT_WAREHOUSEBindingSource.DataMember = "PRODUCT_WAREHOUSE"
        Me.PRODUCT_WAREHOUSEBindingSource.DataSource = Me.DS_PRODUCT_WAREHOUSE
        '
        'PRODUCT_WAREHOUSETableAdapter
        '
        Me.PRODUCT_WAREHOUSETableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager1
        '
        Me.TableAdapterManager1.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager1.PRODUCT_WAREHOUSETableAdapter = Me.PRODUCT_WAREHOUSETableAdapter
        Me.TableAdapterManager1.UpdateOrder = DMI_RETAIL_PURCHASE.DS_PRODUCT_WAREHOUSETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.cmdUndo)
        Me.GroupBox4.Controls.Add(Me.cmdSave)
        Me.GroupBox4.Controls.Add(Me.cmdEdit)
        Me.GroupBox4.Controls.Add(Me.cmdDelete)
        Me.GroupBox4.Controls.Add(Me.cmdAdd)
        Me.GroupBox4.Location = New System.Drawing.Point(135, 546)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox4.Size = New System.Drawing.Size(515, 55)
        Me.GroupBox4.TabIndex = 15
        Me.GroupBox4.TabStop = False
        '
        'cmdUndo
        '
        Me.cmdUndo.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(216, 11)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(83, 38)
        Me.cmdUndo.TabIndex = 19
        Me.cmdUndo.Text = "&Undo" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F9]"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSave.Location = New System.Drawing.Point(317, 11)
        Me.cmdSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(83, 38)
        Me.cmdSave.TabIndex = 20
        Me.cmdSave.Text = "&Save" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F10]"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(115, 11)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(83, 38)
        Me.cmdEdit.TabIndex = 18
        Me.cmdEdit.Text = "&Edit" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F8]"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(418, 11)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(83, 38)
        Me.cmdDelete.TabIndex = 21
        Me.cmdDelete.Text = "&Delete" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F11]"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(14, 11)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(83, 38)
        Me.cmdAdd.TabIndex = 17
        Me.cmdAdd.Text = "&Add" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F7]"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'PURCHASEWAREHOUSEBindingSource
        '
        Me.PURCHASEWAREHOUSEBindingSource.DataMember = "PURCHASE_WAREHOUSE"
        Me.PURCHASEWAREHOUSEBindingSource.DataSource = Me.PURCHASEBindingSource
        '
        'PURCHASEWAREHOUSEBindingSource1
        '
        Me.PURCHASEWAREHOUSEBindingSource1.DataMember = "PURCHASE_WAREHOUSE"
        Me.PURCHASEWAREHOUSEBindingSource1.DataSource = Me.PURCHASEBindingSource
        '
        'DS_PURCHASE_ORDER
        '
        Me.DS_PURCHASE_ORDER.DataSetName = "DS_PURCHASE_ORDER"
        Me.DS_PURCHASE_ORDER.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SP_SELECT_PURCHASE_ORDER_HEADERBindingSource
        '
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingSource.DataMember = "SP_SELECT_PURCHASE_ORDER_HEADER"
        Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingSource.DataSource = Me.DS_PURCHASE_ORDER
        '
        'SP_SELECT_PURCHASE_ORDER_HEADERTableAdapter
        '
        Me.SP_SELECT_PURCHASE_ORDER_HEADERTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager2
        '
        Me.TableAdapterManager2.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager2.Connection = Nothing
        Me.TableAdapterManager2.DELIVERY_PURCHASE_ORDER_DETAILTableAdapter = Nothing
        Me.TableAdapterManager2.DELIVERY_PURCHASE_ORDER_HEADERTableAdapter = Nothing
        Me.TableAdapterManager2.PRODUCTTableAdapter = Nothing
        Me.TableAdapterManager2.PURCHASE_ORDER_DETAILTableAdapter = Nothing
        Me.TableAdapterManager2.PURCHASE_ORDER_HEADERTableAdapter = Nothing
        Me.TableAdapterManager2.UpdateOrder = DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager2.VENDORTableAdapter = Nothing
        '
        'SP_SELECT_PURCHASE_ORDER_DETAILBindingSource
        '
        Me.SP_SELECT_PURCHASE_ORDER_DETAILBindingSource.DataMember = "SP_SELECT_PURCHASE_ORDER_DETAIL"
        Me.SP_SELECT_PURCHASE_ORDER_DETAILBindingSource.DataSource = Me.DS_PURCHASE_ORDER
        '
        'SP_SELECT_PURCHASE_ORDER_DETAILTableAdapter
        '
        Me.SP_SELECT_PURCHASE_ORDER_DETAILTableAdapter.ClearBeforeFill = True
        '
        'frmEntryPurchase
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(791, 605)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.PURCHASEBindingNavigator)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmEntryPurchase"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Entry Purchase"
        Me.Text = "Entry Purchase"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cmdSearchSaleOrder, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_PURCHASE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PURCHASEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VENDORBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PAYMENT_METHODBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PURCHASEBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PURCHASEBindingNavigator.ResumeLayout(False)
        Me.PURCHASEBindingNavigator.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvPurchaseDetail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.PURCHASE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VIEW_LIST_PURCHASEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VIEW_LIST_PURCHASE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_PRODUCT_WAREHOUSE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PRODUCT_WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.PURCHASEWAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PURCHASEWAREHOUSEBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_PURCHASE_ORDER, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_SELECT_PURCHASE_ORDER_HEADERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_SELECT_PURCHASE_ORDER_DETAILBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents DS_PURCHASE As DMI_RETAIL_PURCHASE.DS_PURCHASE
    Friend WithEvents PURCHASEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PURCHASETableAdapter As DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.PURCHASETableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.TableAdapterManager
    Friend WithEvents PURCHASEBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents RECEIPT_NOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PURCHASE_DATEDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents VENDOR_IDComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents GRAND_TOTALTextBox As System.Windows.Forms.TextBox
    Friend WithEvents REMARKTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MATURITY_DATEDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents PAYMENT_METHOD_IDComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents VENDORTableAdapter As DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.VENDORTableAdapter
    Friend WithEvents VENDORBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PAYMENT_METHODBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PAYMENT_METHODTableAdapter As DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.PAYMENT_METHODTableAdapter
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvPurchaseDetail As System.Windows.Forms.DataGridView
    Friend WithEvents AMOUNT_DISCOUNTTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PRODUCTTableAdapter As DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.PRODUCTTableAdapter
    Friend WithEvents PRODUCTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TAX_AMOUNTTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TAXPercentageTEXTBOX As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents PURCHASE_DETAILTableAdapter As DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.PURCHASE_DETAILTableAdapter
    Friend WithEvents PURCHASE_DETAILBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VIEW_LIST_PURCHASEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VIEW_LIST_PURCHASETableAdapter As DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.VIEW_LIST_PURCHASETableAdapter
    Friend WithEvents VIEW_LIST_PURCHASE_DETAILBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VIEW_LIST_PURCHASE_DETAILTableAdapter As DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.VIEW_LIST_PURCHASE_DETAILTableAdapter
    Friend WithEvents lblNoOfItem As System.Windows.Forms.Label
    Friend WithEvents DS_PRODUCT_WAREHOUSE As DMI_RETAIL_PURCHASE.DS_PRODUCT_WAREHOUSE
    Friend WithEvents PRODUCT_WAREHOUSEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PRODUCT_WAREHOUSETableAdapter As DMI_RETAIL_PURCHASE.DS_PRODUCT_WAREHOUSETableAdapters.PRODUCT_WAREHOUSETableAdapter
    Friend WithEvents TableAdapterManager1 As DMI_RETAIL_PURCHASE.DS_PRODUCT_WAREHOUSETableAdapters.TableAdapterManager
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents txtDTP As System.Windows.Forms.TextBox
    Friend WithEvents WAREHOUSETableAdapter As DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.WAREHOUSETableAdapter
    Friend WithEvents WAREHOUSEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PURCHASEWAREHOUSEBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents PURCHASEWAREHOUSEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents WAREHOUSE_NAMEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents DOWN_PAYMENTTextBox As System.Windows.Forms.TextBox
    Friend WithEvents lblWarehouse As System.Windows.Forms.Label
    Friend WithEvents lblMaturity As System.Windows.Forms.Label
    Friend WithEvents lblPayment As System.Windows.Forms.Label
    Friend WithEvents lblVendor As System.Windows.Forms.Label
    Friend WithEvents lblPurchaseDate As System.Windows.Forms.Label
    Friend WithEvents lblReceiptNo As System.Windows.Forms.Label
    Friend WithEvents lblDel As System.Windows.Forms.Label
    Friend WithEvents lblF6 As System.Windows.Forms.Label
    Friend WithEvents lblF5 As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents lblDp As System.Windows.Forms.Label
    Friend WithEvents lblDiscount As System.Windows.Forms.Label
    Friend WithEvents lblTax As System.Windows.Forms.Label
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents PRODUCT_CODE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRODUCT_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QUANTITY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRICE_PER_UNIT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DISCOUNT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DISCOUNT2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DELETE As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dtpPoDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblOrderDate As System.Windows.Forms.Label
    Friend WithEvents lblPoNo As System.Windows.Forms.Label
    Friend WithEvents txtPoNo As System.Windows.Forms.TextBox
    Friend WithEvents cmdSearchSaleOrder As System.Windows.Forms.PictureBox
    Friend WithEvents DS_PURCHASE_ORDER As DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDER
    Friend WithEvents SP_SELECT_PURCHASE_ORDER_HEADERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_SELECT_PURCHASE_ORDER_HEADERTableAdapter As DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.SP_SELECT_PURCHASE_ORDER_HEADERTableAdapter
    Friend WithEvents TableAdapterManager2 As DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.TableAdapterManager
    Friend WithEvents SP_SELECT_PURCHASE_ORDER_DETAILBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_SELECT_PURCHASE_ORDER_DETAILTableAdapter As DMI_RETAIL_PURCHASE.DS_PURCHASE_ORDERTableAdapters.SP_SELECT_PURCHASE_ORDER_DETAILTableAdapter
End Class
