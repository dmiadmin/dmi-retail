﻿Public Class frmSearchSaleOrder
    Public tmpCustomerId, tmpSalesmanId, tmpSaleOrderId, tmpTotItem As Integer
    Public tmpSaleOrderDate As DateTime
    Public tmpSaleOrderNo, tmpCustomerName, tmpSalesmanName As String

    Private Sub frmSearchSaleOrder_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_SALE_ORDER.SP_SELECT_SALE_ORDER_HEADER' table. You can move, or remove it, as needed.
        Me.SP_SELECT_SALE_ORDER_HEADERTableAdapter.Fill(Me.DS_SALE_ORDER.SP_SELECT_SALE_ORDER_HEADER)

    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSaleOrderNo.TextChanged
        Try
            txtCustomer.Text = ""
            SP_SELECT_SALE_ORDER_HEADERBindingSource.Filter = "SALE_ORDER_NUMBER LIKE '%" & txtSaleOrderNo.Text.ToUpper & "%'"
        Catch ex As Exception
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtSaleOrderNo.Text = ""
            txtSaleOrderNo.Focus()
        End Try

    End Sub

    Private Sub txtCustomer_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCustomer.TextChanged
        Try
            txtSaleOrderNo.Text = ""
            SP_SELECT_SALE_ORDER_HEADERBindingSource.Filter = "CUSTOMER LIKE '%" & txtCustomer.Text.ToUpper & "%'"
        Catch ex As Exception
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtCustomer.Text = ""
            txtCustomer.Focus()
        End Try

    End Sub

    Private Sub SP_SELECT_SALE_ORDER_HEADERDataGridView_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles SP_SELECT_SALE_ORDER_HEADERDataGridView.DoubleClick
        Try

            tmpCustomerId = SP_SELECT_SALE_ORDER_HEADERBindingSource.Current("CUSTOMER_ID")
            tmpSalesmanId = SP_SELECT_SALE_ORDER_HEADERBindingSource.Current("SALESMAN_ID")
            tmpSaleOrderDate = SP_SELECT_SALE_ORDER_HEADERBindingSource.Current("SALE_ORDER_DATE")
            tmpSaleOrderNo = SP_SELECT_SALE_ORDER_HEADERBindingSource.Current("SALE_ORDER_NUMBER")
            tmpSalesmanName = SP_SELECT_SALE_ORDER_HEADERBindingSource.Current("SALESMAN")
            tmpCustomerName = SP_SELECT_SALE_ORDER_HEADERBindingSource.Current("CUSTOMER")
            If Not IsDBNull(SP_SELECT_SALE_ORDER_HEADERBindingSource.Current("TAX_AMOUNT")) Then
                frmEntrySale.TAX_AMOUNTTextBox.Text = SP_SELECT_SALE_ORDER_HEADERBindingSource.Current("TAX_AMOUNT")
                frmEntrySale.TAXPercentageTEXTBOX.Text = SP_SELECT_SALE_ORDER_HEADERBindingSource.Current("TAX_PERCENTAGE")
            End If
           
            Me.Close()
        Catch

        End Try

    End Sub

End Class