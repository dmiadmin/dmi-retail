﻿Imports System.Data.SqlClient
Imports System.Data
Public Class frmListAdjustment


    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click

        Me.Cursor = Cursors.WaitCursor
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = " DECLARE	@return_value int" & _
                                "  EXEC	@return_value = [dbo].[SP_LIST_ADJUSTMENT]" & _
                                "  @PERIOD1 = '" & dtpPeriod.Value & "'," & _
                                "  @PERIOD2 = '" & dtpPeriod2.Value & "'" & _
                                "  SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.SP_LIST_ADJUSTMENTDataGridView.DataSource = dt

        Me.Cursor = Cursors.Default
        lblNoItem.Visible = True
        If Language = "Indonesian" Then
            SP_LIST_ADJUSTMENTDataGridView.Columns("ADJUSTMENT_NO").HeaderText = "No Penyesuaian"
            SP_LIST_ADJUSTMENTDataGridView.Columns("ADJUSTMENT_DATE").HeaderText = "Tanggal Penyesuaian"
            SP_LIST_ADJUSTMENTDataGridView.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            SP_LIST_ADJUSTMENTDataGridView.Columns("PRICE_PER_UNIT").HeaderText = "Harga Satuan"
            SP_LIST_ADJUSTMENTDataGridView.Columns("QUANTITY").HeaderText = "Jumlah"
            SP_LIST_ADJUSTMENTDataGridView.Columns("DESCRIPTION").HeaderText = "Keterangan"
            SP_LIST_ADJUSTMENTDataGridView.Columns("WAREHOUSE_NAME").HeaderText = "Gudang"
            SP_LIST_ADJUSTMENTDataGridView.Columns("ADJUSTMENT_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            SP_LIST_ADJUSTMENTDataGridView.Columns("ADJUSTMENT_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            SP_LIST_ADJUSTMENTDataGridView.Columns("PRICE_PER_UNIT").DefaultCellStyle.Format = "n0"
            SP_LIST_ADJUSTMENTDataGridView.Columns("PRICE_PER_UNIT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            SP_LIST_ADJUSTMENTDataGridView.Columns("QUANTITY").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            SP_LIST_ADJUSTMENTDataGridView.Columns("REP_PERIOD1").Visible = False
            SP_LIST_ADJUSTMENTDataGridView.Columns("REP_PERIOD2").Visible = False
            lblNoItem.Text = "Jumlah Item (s) : " & SP_LIST_ADJUSTMENTDataGridView.RowCount
        Else
            SP_LIST_ADJUSTMENTDataGridView.Columns("ADJUSTMENT_NO").HeaderText = "Adjustment No"
            SP_LIST_ADJUSTMENTDataGridView.Columns("ADJUSTMENT_DATE").HeaderText = "Adjustment Date"
            SP_LIST_ADJUSTMENTDataGridView.Columns("PRODUCT_NAME").HeaderText = "Product Name"
            SP_LIST_ADJUSTMENTDataGridView.Columns("PRICE_PER_UNIT").HeaderText = "Price"
            SP_LIST_ADJUSTMENTDataGridView.Columns("QUANTITY").HeaderText = "Quantity"
            SP_LIST_ADJUSTMENTDataGridView.Columns("DESCRIPTION").HeaderText = "Description"
            SP_LIST_ADJUSTMENTDataGridView.Columns("WAREHOUSE_NAME").HeaderText = "Warehouse"
            SP_LIST_ADJUSTMENTDataGridView.Columns("ADJUSTMENT_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            SP_LIST_ADJUSTMENTDataGridView.Columns("ADJUSTMENT_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            SP_LIST_ADJUSTMENTDataGridView.Columns("PRICE_PER_UNIT").DefaultCellStyle.Format = "n0"
            SP_LIST_ADJUSTMENTDataGridView.Columns("PRICE_PER_UNIT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            SP_LIST_ADJUSTMENTDataGridView.Columns("QUANTITY").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            SP_LIST_ADJUSTMENTDataGridView.Columns("REP_PERIOD1").Visible = False
            SP_LIST_ADJUSTMENTDataGridView.Columns("REP_PERIOD2").Visible = False
            lblNoItem.Text = "No Of Item (s) : " & SP_LIST_ADJUSTMENTDataGridView.RowCount
        End If
    End Sub

    Private Sub frmListAdjustment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Language = "Indonesian" Then
            Me.Text = "Daftar Penyesuaian"
            'SP_LIST_ADJUSTMENTDataGridView.Columns("ADJUSTMENT_NO").HeaderText = "No Penyesuaian"
            'SP_LIST_ADJUSTMENTDataGridView.Columns("ADJUSTMENT_DATE").HeaderText = "Tanggal Penyesuaian"
            'SP_LIST_ADJUSTMENTDataGridView.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            'SP_LIST_ADJUSTMENTDataGridView.Columns("PRICE_PER_UNIT").HeaderText = "Harga Satuan"
            'SP_LIST_ADJUSTMENTDataGridView.Columns("QUANTITY").HeaderText = "Jumlah"
            'SP_LIST_ADJUSTMENTDataGridView.Columns("DESCRIPTION").HeaderText = "Keterangan"
            'SP_LIST_ADJUSTMENTDataGridView.Columns("WAREHOUSE_NAME").HeaderText = "Gudang"
            Label1.Text = "Dari Periode"
            Label2.Text = "s/d"
            cmdGenerate.Text = "PROSES"
            cmdReport.Text = "Cetak"
        End If

        dtpPeriod.Value = DateSerial(Today.Year, Today.Month, 1)
        dtpPeriod2.Value = DateSerial(Today.Year, Today.Month + 1, 0)
    End Sub

    'Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
    '    Dim objf As New frmRepAdjustment
    '    objf.date1 = dtpPeriod.Value
    '    objf.date2 = dtpPeriod2.Value
    '    objf.ReportViewer1.ShowRefreshButton = False
    '    objf.ReportViewer1.ZoomPercent = 100
    '    objf.WindowState = FormWindowState.Maximized
    '    objf.Show()
    'End Sub
End Class