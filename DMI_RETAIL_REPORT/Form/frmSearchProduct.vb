﻿Public Class frmSearchProduct
    Public tmpCode As String = ""
    Private Sub frmSearchProduct_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.SP_LIST_PRODUCT_ACTIVETableAdapter.Fill(Me.DS_SALE_ORDER.SP_LIST_PRODUCT_ACTIVE, Now)
        Me.SP_LIST_PRODUCT_GOODSTableAdapter.Fill(Me.DS_SALE_ORDER.SP_LIST_PRODUCT_GOODS, Now)
        'TODO: This line of code loads data into the 'DS_SALE_ORDER.PRODUCT' table. You can move, or remove it, as needed.
        Me.PRODUCTTableAdapter.Fill(Me.DS_SALE_ORDER.PRODUCT)

        If mdlGeneral.tmpSearchMode = "SO-Product Code" Or _
            mdlGeneral.tmpSearchMode = "Entry Product - Product Code" Or _
            mdlGeneral.tmpSearchMode = "PURCHASE - Product Code" Or _
            mdlGeneral.tmpSearchMode = "SALE - Product Code" Then

            If Language = "Indonesian" Then
                Label1.Text = "Kode Produk"
            Else
                Label1.Text = "Product Code"
            End If

        ElseIf mdlGeneral.tmpSearchMode = "SO-Product Name" Or _
            mdlGeneral.tmpSearchMode = "Entry Product - Product Name" Or _
            mdlGeneral.tmpSearchMode = "PURCHASE - Product Name" Or _
            mdlGeneral.tmpSearchMode = "SALE - Product Name" Or _
            mdlGeneral.tmpSearchMode = "Entry Opening Balance - Product Name" Or _
            mdlGeneral.tmpSearchMode = "Entry Adjustment - Product Name" Or _
            mdlGeneral.tmpSearchMode = "Product Warehouse - Product Name" Or _
            mdlGeneral.tmpSearchMode = "PRODUCT LIST - Product Name" Or _
            mdlGeneral.tmpSearchMode = "Entry Product Dist - Product Name" Then

            If Language = "Indonesian" Then
                Label1.Text = "Nama Produk"
            Else
                Label1.Text = "Product Name"
            End If
        End If

        If tmpSearchMode = "PURCHASE - Product Code" Or tmpSearchMode = "PURCHASE - Product Name" Or _
            tmpSearchMode = "Entry Opening Balance - Product Name" Or _
            tmpSearchMode = "Entry Product Dist - Product Name" Or _
            tmpSearchMode = "Product Stock - Product Name" Or _
            tmpSearchMode = "Entry Adjustment - Product Name" Or _
            tmpSearchMode = "PRODUCT LIST - Product Name" Then
            PRODUCTDataGridView.DataSource = SP_LIST_PRODUCT_GOODSBindingSource
            PRODUCTBindingNavigator.BindingSource = SP_LIST_PRODUCT_GOODSBindingSource
        Else
            PRODUCTDataGridView.DataSource = SP_LIST_PRODUCT_ACTIVEBindingSource
            PRODUCTBindingNavigator.BindingSource = SP_LIST_PRODUCT_ACTIVEBindingSource
        End If
        txtProductCode.Text = ""
        txtProductCode.Focus()
        tmpSearchResult = ""

        If Language = "Indonesian" Then
            Me.Text = "Pencarian Produk"

            PRODUCTDataGridView.Columns("PRODUCT_CODE").HeaderText = "Kode Produk"
            PRODUCTDataGridView.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
        End If

    End Sub

    Private Sub txtProductCode_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtProductCode.KeyUp
        If e.KeyCode = Keys.Enter Then
            PRODUCTDataGridView.Focus()
            Exit Sub
        ElseIf e.KeyCode = Keys.Escape Then
            tmpSearchResult = ""
            Me.Close()
        End If
    End Sub

    Private Sub txtProductCode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProductCode.TextChanged
        Try
            If tmpSearchMode = "SO-Product Code" Or tmpSearchMode = "Entry Product - Product Code" Or _
               tmpSearchMode = "SALE - Product Code" Then
                SP_LIST_PRODUCT_ACTIVEBindingSource.Filter = "PRODUCT_CODE LIKE '%" & txtProductCode.Text.ToUpper & "%'"
            ElseIf tmpSearchMode = "SO-Product Name" Or tmpSearchMode = "Entry Product - Product Name" Or _
                tmpSearchMode = "SALE - Product Name" Or _
                tmpSearchMode = "Entry Adjustment - Product Name" Or _
                mdlGeneral.tmpSearchMode = "Product Warehouse - Product Name" Then
                SP_LIST_PRODUCT_ACTIVEBindingSource.Filter = "PRODUCT_NAME LIKE '%" & txtProductCode.Text.ToUpper & "%'"
            ElseIf tmpSearchMode = "PURCHASE - Product Code" Then
                SP_LIST_PRODUCT_GOODSBindingSource.Filter = "PRODUCT_CODE LIKE '%" & txtProductCode.Text.ToUpper & "%'"
            ElseIf tmpSearchMode = "PURCHASE - Product Name" Or _
                tmpSearchMode = "Entry Opening Balance - Product Name" Or _
                tmpSearchMode = "Entry Product Dist - Product Name" Or _
                tmpSearchMode = "Product Stock - Product Name" Or _
                tmpSearchMode = "Entry Adjustment - Product Name" Or _
                tmpSearchMode = "PRODUCT LIST - Product Name" Then
                SP_LIST_PRODUCT_GOODSBindingSource.Filter = "PRODUCT_NAME LIKE '%" & txtProductCode.Text.ToUpper & "%'"
            End If

        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtProductCode.Text = ""
            txtProductCode.Focus()
        End Try
    End Sub

    Private Sub PRODUCTDataGridView_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles PRODUCTDataGridView.DoubleClick
        Try
            If PRODUCTDataGridView.DataSource Is SP_LIST_PRODUCT_ACTIVEBindingSource Then
                tmpSearchResult = SP_LIST_PRODUCT_ACTIVEBindingSource.Current("PRODUCT_ID")
            ElseIf PRODUCTDataGridView.DataSource Is SP_LIST_PRODUCT_GOODSBindingSource Then
                tmpSearchResult = SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID")
            End If

        Catch
            MsgBox("There is no Product to store !!", MsgBoxStyle.Critical, "DMI Retail")
            
        End Try
        tmpCode = PRODUCTDataGridView.CurrentRow.Cells("PRODUCT_CODE").Value
        Me.Close()
    End Sub

    Private Sub PRODUCTDataGridView_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles PRODUCTDataGridView.KeyDown
        If PRODUCTDataGridView.DataSource Is SP_LIST_PRODUCT_ACTIVEBindingSource Then
            If e.KeyCode = Keys.Enter Then
                Try
                    tmpSearchResult = SP_LIST_PRODUCT_ACTIVEBindingSource.Current("PRODUCT_ID")
                Catch
                    MsgBox("There is no Product to store !!", MsgBoxStyle.Critical, "DMI Retail")
                End Try
                Me.Close()

            ElseIf e.KeyCode = Keys.Escape Then
                tmpSearchResult = ""
                Me.Close()
            End If

        ElseIf PRODUCTDataGridView.DataSource Is SP_LIST_PRODUCT_GOODSBindingSource Then
            If e.KeyCode = Keys.Enter Then
                Try
                    tmpSearchResult = SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID")
                Catch
                    MsgBox("There is no Product to store !!", MsgBoxStyle.Critical, "DMI Retail")
                End Try
                Me.Close()

            ElseIf e.KeyCode = Keys.Escape Then
                tmpSearchResult = ""
                Me.Close()
            End If
        End If
    End Sub

    Private Sub frmSearchProduct_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
        txtProductCode.Focus()
    End Sub



    
End Class