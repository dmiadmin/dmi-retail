﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayableTransaction
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPayableTransaction))
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.VIEW_PAYABLE_TRANSACTIONBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_PAYABLE_TRANSACTION = New DMI_RETAIL_HUTANG_PIUTANG.DS_PAYABLE_TRANSACTION()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.cmdSearchName = New System.Windows.Forms.PictureBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.lblAmount = New System.Windows.Forms.Label()
        Me.lblTransDate = New System.Windows.Forms.Label()
        Me.lblAccount = New System.Windows.Forms.Label()
        Me.lblTransNo = New System.Windows.Forms.Label()
        Me.txtDtp = New System.Windows.Forms.TextBox()
        Me.PAYABLE_TRANSACTION_NUMBERTextBox = New System.Windows.Forms.TextBox()
        Me.DESCRIPTIONTextBox = New System.Windows.Forms.TextBox()
        Me.AMOUNTTextBox = New System.Windows.Forms.TextBox()
        Me.TRANSACTION_DATEDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.ACCOUNT_NAMETextBox = New System.Windows.Forms.TextBox()
        Me.ACCOUNT_NUMBERTextBox = New System.Windows.Forms.TextBox()
        Me.PAYABLE_TRANSACTIONBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VIEW_PAYABLE_TRANSACTIONTableAdapter = New DMI_RETAIL_HUTANG_PIUTANG.DS_PAYABLE_TRANSACTIONTableAdapters.VIEW_PAYABLE_TRANSACTIONTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_HUTANG_PIUTANG.DS_PAYABLE_TRANSACTIONTableAdapters.TableAdapterManager()
        Me.ACCOUNTTableAdapter = New DMI_RETAIL_HUTANG_PIUTANG.DS_PAYABLE_TRANSACTIONTableAdapters.ACCOUNTTableAdapter()
        Me.PAYABLE_TRANSACTIONTableAdapter = New DMI_RETAIL_HUTANG_PIUTANG.DS_PAYABLE_TRANSACTIONTableAdapters.PAYABLE_TRANSACTIONTableAdapter()
        Me.VIEW_PAYABLE_ACCOUNTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VIEW_PAYABLE_ACCOUNTTableAdapter = New DMI_RETAIL_HUTANG_PIUTANG.DS_PAYABLE_TRANSACTIONTableAdapters.VIEW_PAYABLE_ACCOUNTTableAdapter()
        Me.ACCOUNTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator.SuspendLayout()
        CType(Me.VIEW_PAYABLE_TRANSACTIONBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_PAYABLE_TRANSACTION, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PAYABLE_TRANSACTIONBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VIEW_PAYABLE_ACCOUNTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ACCOUNTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'VIEW_PAYABLE_TRANSACTIONBindingNavigator
        '
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator.AddNewItem = Nothing
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator.BindingSource = Me.VIEW_PAYABLE_TRANSACTIONBindingSource
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator.DeleteItem = Nothing
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator.Name = "VIEW_PAYABLE_TRANSACTIONBindingNavigator"
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator.Size = New System.Drawing.Size(541, 25)
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator.TabIndex = 0
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator.Text = "BindingNavigator1"
        '
        'VIEW_PAYABLE_TRANSACTIONBindingSource
        '
        Me.VIEW_PAYABLE_TRANSACTIONBindingSource.DataMember = "VIEW_PAYABLE_TRANSACTION"
        Me.VIEW_PAYABLE_TRANSACTIONBindingSource.DataSource = Me.DS_PAYABLE_TRANSACTION
        '
        'DS_PAYABLE_TRANSACTION
        '
        Me.DS_PAYABLE_TRANSACTION.DataSetName = "DS_PAYABLE_TRANSACTION"
        Me.DS_PAYABLE_TRANSACTION.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdUndo)
        Me.GroupBox2.Controls.Add(Me.cmdSave)
        Me.GroupBox2.Controls.Add(Me.cmdEdit)
        Me.GroupBox2.Controls.Add(Me.cmdDelete)
        Me.GroupBox2.Controls.Add(Me.cmdAdd)
        Me.GroupBox2.Location = New System.Drawing.Point(13, 269)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Size = New System.Drawing.Size(515, 55)
        Me.GroupBox2.TabIndex = 21
        Me.GroupBox2.TabStop = False
        '
        'cmdUndo
        '
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(220, 17)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(79, 29)
        Me.cmdUndo.TabIndex = 9
        Me.cmdUndo.Text = "&Undo"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSave.Location = New System.Drawing.Point(321, 17)
        Me.cmdSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(79, 29)
        Me.cmdSave.TabIndex = 10
        Me.cmdSave.Text = "&Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(119, 17)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(79, 29)
        Me.cmdEdit.TabIndex = 8
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(422, 17)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(79, 29)
        Me.cmdDelete.TabIndex = 11
        Me.cmdDelete.Text = "&Delete"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(18, 17)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(79, 29)
        Me.cmdAdd.TabIndex = 7
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'cmdSearchName
        '
        Me.cmdSearchName.Image = CType(resources.GetObject("cmdSearchName.Image"), System.Drawing.Image)
        Me.cmdSearchName.Location = New System.Drawing.Point(448, 52)
        Me.cmdSearchName.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchName.Name = "cmdSearchName"
        Me.cmdSearchName.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearchName.TabIndex = 20
        Me.cmdSearchName.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblDescription)
        Me.GroupBox1.Controls.Add(Me.lblAmount)
        Me.GroupBox1.Controls.Add(Me.lblTransDate)
        Me.GroupBox1.Controls.Add(Me.lblAccount)
        Me.GroupBox1.Controls.Add(Me.lblTransNo)
        Me.GroupBox1.Controls.Add(Me.txtDtp)
        Me.GroupBox1.Controls.Add(Me.PAYABLE_TRANSACTION_NUMBERTextBox)
        Me.GroupBox1.Controls.Add(Me.DESCRIPTIONTextBox)
        Me.GroupBox1.Controls.Add(Me.AMOUNTTextBox)
        Me.GroupBox1.Controls.Add(Me.TRANSACTION_DATEDateTimePicker)
        Me.GroupBox1.Controls.Add(Me.ACCOUNT_NAMETextBox)
        Me.GroupBox1.Controls.Add(Me.ACCOUNT_NUMBERTextBox)
        Me.GroupBox1.Controls.Add(Me.cmdSearchName)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 38)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(515, 231)
        Me.GroupBox1.TabIndex = 28
        Me.GroupBox1.TabStop = False
        '
        'lblDescription
        '
        Me.lblDescription.Location = New System.Drawing.Point(6, 152)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(117, 22)
        Me.lblDescription.TabIndex = 36
        Me.lblDescription.Text = "Description :"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblAmount
        '
        Me.lblAmount.Location = New System.Drawing.Point(15, 120)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(108, 19)
        Me.lblAmount.TabIndex = 35
        Me.lblAmount.Text = "Amount :"
        Me.lblAmount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblTransDate
        '
        Me.lblTransDate.Location = New System.Drawing.Point(6, 88)
        Me.lblTransDate.Name = "lblTransDate"
        Me.lblTransDate.Size = New System.Drawing.Size(117, 19)
        Me.lblTransDate.TabIndex = 34
        Me.lblTransDate.Text = "Transaction Date :"
        Me.lblTransDate.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblAccount
        '
        Me.lblAccount.Location = New System.Drawing.Point(12, 56)
        Me.lblAccount.Name = "lblAccount"
        Me.lblAccount.Size = New System.Drawing.Size(111, 19)
        Me.lblAccount.TabIndex = 33
        Me.lblAccount.Text = "Account :"
        Me.lblAccount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblTransNo
        '
        Me.lblTransNo.Location = New System.Drawing.Point(12, 24)
        Me.lblTransNo.Name = "lblTransNo"
        Me.lblTransNo.Size = New System.Drawing.Size(111, 19)
        Me.lblTransNo.TabIndex = 32
        Me.lblTransNo.Text = "Transaction No :"
        Me.lblTransNo.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtDtp
        '
        Me.txtDtp.Location = New System.Drawing.Point(130, 82)
        Me.txtDtp.Name = "txtDtp"
        Me.txtDtp.ReadOnly = True
        Me.txtDtp.Size = New System.Drawing.Size(154, 22)
        Me.txtDtp.TabIndex = 4
        Me.txtDtp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'PAYABLE_TRANSACTION_NUMBERTextBox
        '
        Me.PAYABLE_TRANSACTION_NUMBERTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VIEW_PAYABLE_TRANSACTIONBindingSource, "PAYABLE_TRANSACTION_NUMBER", True))
        Me.PAYABLE_TRANSACTION_NUMBERTextBox.Location = New System.Drawing.Point(130, 21)
        Me.PAYABLE_TRANSACTION_NUMBERTextBox.Name = "PAYABLE_TRANSACTION_NUMBERTextBox"
        Me.PAYABLE_TRANSACTION_NUMBERTextBox.Size = New System.Drawing.Size(105, 22)
        Me.PAYABLE_TRANSACTION_NUMBERTextBox.TabIndex = 1
        Me.PAYABLE_TRANSACTION_NUMBERTextBox.Tag = "M"
        '
        'DESCRIPTIONTextBox
        '
        Me.DESCRIPTIONTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VIEW_PAYABLE_TRANSACTIONBindingSource, "DESCRIPTION", True))
        Me.DESCRIPTIONTextBox.Location = New System.Drawing.Point(130, 149)
        Me.DESCRIPTIONTextBox.Multiline = True
        Me.DESCRIPTIONTextBox.Name = "DESCRIPTIONTextBox"
        Me.DESCRIPTIONTextBox.Size = New System.Drawing.Size(185, 70)
        Me.DESCRIPTIONTextBox.TabIndex = 6
        '
        'AMOUNTTextBox
        '
        Me.AMOUNTTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VIEW_PAYABLE_TRANSACTIONBindingSource, "AMOUNT", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.AMOUNTTextBox.Location = New System.Drawing.Point(130, 117)
        Me.AMOUNTTextBox.Name = "AMOUNTTextBox"
        Me.AMOUNTTextBox.Size = New System.Drawing.Size(185, 22)
        Me.AMOUNTTextBox.TabIndex = 5
        Me.AMOUNTTextBox.Tag = "M"
        Me.AMOUNTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TRANSACTION_DATEDateTimePicker
        '
        Me.TRANSACTION_DATEDateTimePicker.CustomFormat = "dd-MMM-yyyy"
        Me.TRANSACTION_DATEDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.VIEW_PAYABLE_TRANSACTIONBindingSource, "TRANSACTION_DATE", True))
        Me.TRANSACTION_DATEDateTimePicker.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right
        Me.TRANSACTION_DATEDateTimePicker.Enabled = False
        Me.TRANSACTION_DATEDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.TRANSACTION_DATEDateTimePicker.Location = New System.Drawing.Point(130, 82)
        Me.TRANSACTION_DATEDateTimePicker.Name = "TRANSACTION_DATEDateTimePicker"
        Me.TRANSACTION_DATEDateTimePicker.Size = New System.Drawing.Size(185, 22)
        Me.TRANSACTION_DATEDateTimePicker.TabIndex = 4
        Me.TRANSACTION_DATEDateTimePicker.Tag = "M"
        '
        'ACCOUNT_NAMETextBox
        '
        Me.ACCOUNT_NAMETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VIEW_PAYABLE_TRANSACTIONBindingSource, "ACCOUNT_NAME", True))
        Me.ACCOUNT_NAMETextBox.Location = New System.Drawing.Point(241, 53)
        Me.ACCOUNT_NAMETextBox.Name = "ACCOUNT_NAMETextBox"
        Me.ACCOUNT_NAMETextBox.Size = New System.Drawing.Size(200, 22)
        Me.ACCOUNT_NAMETextBox.TabIndex = 3
        '
        'ACCOUNT_NUMBERTextBox
        '
        Me.ACCOUNT_NUMBERTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ACCOUNT_NUMBERTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.VIEW_PAYABLE_TRANSACTIONBindingSource, "ACCOUNT_NUMBER", True))
        Me.ACCOUNT_NUMBERTextBox.Location = New System.Drawing.Point(130, 53)
        Me.ACCOUNT_NUMBERTextBox.Name = "ACCOUNT_NUMBERTextBox"
        Me.ACCOUNT_NUMBERTextBox.Size = New System.Drawing.Size(105, 22)
        Me.ACCOUNT_NUMBERTextBox.TabIndex = 2
        Me.ACCOUNT_NUMBERTextBox.Tag = "M"
        '
        'PAYABLE_TRANSACTIONBindingSource
        '
        Me.PAYABLE_TRANSACTIONBindingSource.DataMember = "PAYABLE_TRANSACTION"
        Me.PAYABLE_TRANSACTIONBindingSource.DataSource = Me.DS_PAYABLE_TRANSACTION
        '
        'VIEW_PAYABLE_TRANSACTIONTableAdapter
        '
        Me.VIEW_PAYABLE_TRANSACTIONTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.ACCOUNTTableAdapter = Me.ACCOUNTTableAdapter
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.PAYABLE_TRANSACTIONTableAdapter = Me.PAYABLE_TRANSACTIONTableAdapter
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_HUTANG_PIUTANG.DS_PAYABLE_TRANSACTIONTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'ACCOUNTTableAdapter
        '
        Me.ACCOUNTTableAdapter.ClearBeforeFill = True
        '
        'PAYABLE_TRANSACTIONTableAdapter
        '
        Me.PAYABLE_TRANSACTIONTableAdapter.ClearBeforeFill = True
        '
        'VIEW_PAYABLE_ACCOUNTBindingSource
        '
        Me.VIEW_PAYABLE_ACCOUNTBindingSource.DataMember = "VIEW_PAYABLE_ACCOUNT"
        Me.VIEW_PAYABLE_ACCOUNTBindingSource.DataSource = Me.DS_PAYABLE_TRANSACTION
        '
        'VIEW_PAYABLE_ACCOUNTTableAdapter
        '
        Me.VIEW_PAYABLE_ACCOUNTTableAdapter.ClearBeforeFill = True
        '
        'ACCOUNTBindingSource
        '
        Me.ACCOUNTBindingSource.DataMember = "ACCOUNT"
        Me.ACCOUNTBindingSource.DataSource = Me.DS_PAYABLE_TRANSACTION
        '
        'frmPayableTransaction
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(541, 336)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmPayableTransaction"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Payable Transaction"
        Me.Text = "Payable Transaction"
        CType(Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator.ResumeLayout(False)
        Me.VIEW_PAYABLE_TRANSACTIONBindingNavigator.PerformLayout()
        CType(Me.VIEW_PAYABLE_TRANSACTIONBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_PAYABLE_TRANSACTION, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PAYABLE_TRANSACTIONBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VIEW_PAYABLE_ACCOUNTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ACCOUNTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DS_PAYABLE_TRANSACTION As DMI_RETAIL_HUTANG_PIUTANG.DS_PAYABLE_TRANSACTION
    Friend WithEvents VIEW_PAYABLE_TRANSACTIONBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VIEW_PAYABLE_TRANSACTIONTableAdapter As DMI_RETAIL_HUTANG_PIUTANG.DS_PAYABLE_TRANSACTIONTableAdapters.VIEW_PAYABLE_TRANSACTIONTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_HUTANG_PIUTANG.DS_PAYABLE_TRANSACTIONTableAdapters.TableAdapterManager
    Friend WithEvents VIEW_PAYABLE_TRANSACTIONBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdSearchName As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents VIEW_PAYABLE_ACCOUNTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VIEW_PAYABLE_ACCOUNTTableAdapter As DMI_RETAIL_HUTANG_PIUTANG.DS_PAYABLE_TRANSACTIONTableAdapters.VIEW_PAYABLE_ACCOUNTTableAdapter
    Friend WithEvents PAYABLE_TRANSACTIONTableAdapter As DMI_RETAIL_HUTANG_PIUTANG.DS_PAYABLE_TRANSACTIONTableAdapters.PAYABLE_TRANSACTIONTableAdapter
    Friend WithEvents PAYABLE_TRANSACTIONBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents DESCRIPTIONTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AMOUNTTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TRANSACTION_DATEDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents ACCOUNT_NAMETextBox As System.Windows.Forms.TextBox
    Friend WithEvents ACCOUNT_NUMBERTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ACCOUNTTableAdapter As DMI_RETAIL_HUTANG_PIUTANG.DS_PAYABLE_TRANSACTIONTableAdapters.ACCOUNTTableAdapter
    Friend WithEvents ACCOUNTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PAYABLE_TRANSACTION_NUMBERTextBox As System.Windows.Forms.TextBox
    Friend WithEvents txtDtp As System.Windows.Forms.TextBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents lblTransDate As System.Windows.Forms.Label
    Friend WithEvents lblAccount As System.Windows.Forms.Label
    Friend WithEvents lblTransNo As System.Windows.Forms.Label
End Class
