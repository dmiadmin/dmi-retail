﻿Public Class frmEntryGroupingProduct
    Dim tmpInput As String
    Dim TmpGrid As Integer = 0
    Dim tmpDataGridViewLoaded, tmpChange As Boolean
    Dim tmpPosition As String
    Dim tmpCategory As Integer
    Dim tmpKeyMode As Boolean
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub

    Private Sub frmEntryGroupingProduct_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If tmpChange = True Then
            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah Anda ingin menyimpan perubahan " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If
            If tmpvar = 6 Then
                cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub frmEntryGroupingProduct_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F5 Then
            If tmpKeyMode = True Then Exit Sub
            tmpInput = "Browse"
            dgvGroupingProduct.CancelEdit()
            mdlGeneral.tmpSearchMode = "PURCHASE - Product Code"
            frmSearchProduct.ShowDialog(Me)
            If tmpSearchResult = "" Then
                tmpInput = ""
                Exit Sub
            End If
            SP_LIST_PRODUCT_GOODSBindingSource.Filter = "PRODUCT_ID = " & tmpSearchResult
            dgvGroupingProduct.Rows.Insert(dgvGroupingProduct.RowCount - 1)
            dgvGroupingProduct.CurrentCell = dgvGroupingProduct.Rows(dgvGroupingProduct.RowCount - 1).Cells(1)

            dgvGroupingProduct.Item(0, dgvGroupingProduct.RowCount - 2).Value = _
                SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID")
            dgvGroupingProduct.Item(1, dgvGroupingProduct.RowCount - 2).Value = _
                SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_CODE")
            dgvGroupingProduct.Item(2, dgvGroupingProduct.RowCount - 2).Value = _
               SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_NAME")

            dgvGroupingProduct.Item("QUANTITY", dgvGroupingProduct.RowCount - 2).Value = 1

            If dgvGroupingProduct.Item("CHILD_PRODUCT_CODE", dgvGroupingProduct.RowCount - 2).Value = PRODUCT_CODEComboBox.Text Then
                If Language = "Indonesian" Then
                    MsgBox("Produk turunan mesti berbeda dengan Produk Induk !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Child Product must be different than Parent Product !", MsgBoxStyle.Critical, "DMI Retail")
                End If
                dgvGroupingProduct.Rows.RemoveAt(dgvGroupingProduct.RowCount - 2)
                tmpPosition = PRODUCT_CODEComboBox.Text
                Me.PRODUCT2TableAdapter.Fill(Me.DS_GROUPING_PRODUCT.PRODUCT2)
                PRODUCT_CODEComboBox.Text = tmpPosition
                Exit Sub
            End If

            If dgvGroupingProduct.RowCount > 2 Then
                For x As Integer = 0 To dgvGroupingProduct.RowCount - 3
                    If dgvGroupingProduct.Item("CHILD_PRODUCT_CODE", dgvGroupingProduct.RowCount - 2).Value = _
                        dgvGroupingProduct.Item("CHILD_PRODUCT_CODE", x).Value Then
                        dgvGroupingProduct.Item("QUANTITY", x).Value = CInt(dgvGroupingProduct.Item("QUANTITY", x).Value) + 1

                        TmpGrid = 1
                    End If
                Next

                If TmpGrid <> 0 Then
                    dgvGroupingProduct.Rows.RemoveAt(dgvGroupingProduct.RowCount - 2)
                End If
            End If

            TmpGrid = 0

        ElseIf e.KeyCode = Keys.F6 Then
            If tmpKeyMode = True Then Exit Sub
            tmpInput = "Browse"
            dgvGroupingProduct.CancelEdit()
            mdlGeneral.tmpSearchMode = "PURCHASE - Product Name"
            frmSearchProduct.ShowDialog(Me)
            If tmpSearchResult = "" Then
                tmpInput = ""
                Exit Sub
            End If
            SP_LIST_PRODUCT_GOODSBindingSource.Filter = "PRODUCT_ID = " & tmpSearchResult
            dgvGroupingProduct.Rows.Insert(dgvGroupingProduct.RowCount - 1)
            dgvGroupingProduct.CurrentCell = dgvGroupingProduct.Rows(dgvGroupingProduct.RowCount - 1).Cells(1)

            dgvGroupingProduct.Item(0, dgvGroupingProduct.RowCount - 2).Value = _
                SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID")
            dgvGroupingProduct.Item(1, dgvGroupingProduct.RowCount - 2).Value = _
                SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_CODE")
            dgvGroupingProduct.Item(2, dgvGroupingProduct.RowCount - 2).Value = _
               SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_NAME")

            dgvGroupingProduct.Item("QUANTITY", dgvGroupingProduct.RowCount - 2).Value = 1

            If dgvGroupingProduct.Item("CHILD_PRODUCT_CODE", dgvGroupingProduct.RowCount - 2).Value = PRODUCT_CODEComboBox.Text Then
                If Language = "Indonesian" Then
                    MsgBox("Produk turunan mesti berbeda dengan Produk Induk !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Child Product must be different than Parent Product !", MsgBoxStyle.Critical, "DMI Retail")
                End If
                dgvGroupingProduct.Rows.RemoveAt(dgvGroupingProduct.RowCount - 2)
                tmpPosition = PRODUCT_CODEComboBox.Text
                Me.PRODUCT2TableAdapter.Fill(Me.DS_GROUPING_PRODUCT.PRODUCT2)
                PRODUCT_CODEComboBox.Text = tmpPosition
                Exit Sub
            End If

            If dgvGroupingProduct.RowCount > 2 Then
                For x As Integer = 0 To dgvGroupingProduct.RowCount - 3
                    If dgvGroupingProduct.Item("CHILD_PRODUCT_CODE", dgvGroupingProduct.RowCount - 2).Value = _
                        dgvGroupingProduct.Item("CHILD_PRODUCT_CODE", x).Value Then
                        dgvGroupingProduct.Item("QUANTITY", x).Value = CInt(dgvGroupingProduct.Item("QUANTITY", x).Value) + 1

                        TmpGrid = 1
                    End If
                Next

                If TmpGrid <> 0 Then
                    dgvGroupingProduct.Rows.RemoveAt(dgvGroupingProduct.RowCount - 2)
                End If
            End If

            TmpGrid = 0
        End If
    End Sub

    Private Sub frmGroupingProduct_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_GROUPING_PRODUCT.PRODUCT2' table. You can move, or remove it, as needed.
        Me.PRODUCT2TableAdapter.Fill(Me.DS_GROUPING_PRODUCT.PRODUCT2)
        'TODO: This line of code loads data into the 'DS_GROUPING_PRODUCT.SP_LIST_PRODUCT_GOODS' table. You can move, or remove it, as needed.
        Me.SP_LIST_PRODUCT_GOODSTableAdapter.Fill(Me.DS_GROUPING_PRODUCT.SP_LIST_PRODUCT_GOODS, Now)
        'TODO: This line of code loads data into the 'DS_GROUPING_PRODUCT.PRODUCT1' table. You can move, or remove it, as needed.
        Me.PRODUCT1TableAdapter.Fill(Me.DS_GROUPING_PRODUCT.PRODUCT1)
        'TODO: This line of code loads data into the 'DS_GROUPING_PRODUCT.GROUPING_PRODUCT' table. You can move, or remove it, as needed.
        Me.GROUPING_PRODUCTTableAdapter.Fill(Me.DS_GROUPING_PRODUCT.GROUPING_PRODUCT)
        'TODO: This line of code loads data into the 'DS_GROUPING_PRODUCT.PRODUCT' table. You can move, or remove it, as needed.
        Me.PRODUCTTableAdapter.Fill(Me.DS_GROUPING_PRODUCT.PRODUCT)

        If Language = "Indonesian" Then
            lblParent.Text = "Produk Induk"
            lblF5.Text = "F5 : Cari Produk Berdasarkan Kode"
            lblF6.Text = "F6 : Cari Produk Berdasarkan Nama"
            cmdAdd.Text = "Tambah"
            cmdEdit.Text = "Ubah"
            cmdUndo.Text = "Batal"
            cmdSave.Text = "Simpan"
            cmdDelete.Text = "Hapus"
            Me.Text = "Input Pengelompokan Produk"
            dgvGroupingProduct.Columns("CHILD_PRODUCT_CODE").HeaderText = "Kode Produk Turunan"
            dgvGroupingProduct.Columns("CHILD_PRODUCT_NAME").HeaderText = "Nama Produk Turunan"
            dgvGroupingProduct.Columns("QUANTITY").HeaderText = "Jumlah"
            dgvGroupingProduct.Columns("DELETE").HeaderText = "Hapus"
        End If

        tmpDataGridViewLoaded = True

        PRODUCT_CODEComboBox.Text = ""

        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdDelete.Enabled = False
        cmdSave.Enabled = True

        tmpKeyMode = False
        tmpChange = False
    End Sub

    Private Sub cmdSearchParent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearchParent.Click

        'Dim tmpResult As Integer

        'GROUPING_PRODUCTTableAdapter.SP_CHECK_GROUPING_PRODUCT(tmpSearchResult, tmpResult)
        'If tmpResult <> 0 Then
        '    If Language = "Indonesian" Then
        '        MsgBox("Produk ini telah ditetapkan sebagai Produk Induk !" & vbCrLf & "Produk tidak dapat didefinisikan dua kali !", _
        '           MsgBoxStyle.Critical, "DMI Retail")
        '    Else
        '        MsgBox("This Product has been defined as Parent Product !" & vbCrLf & "a Product can't be defined twice !", _
        '           MsgBoxStyle.Critical, "DMI Retail")
        '    End If

        '    Me.PRODUCT2TableAdapter.Fill(Me.DS_GROUPING_PRODUCT.PRODUCT2)

        '    PRODUCT_CODEComboBox.Text = ""
        '    Exit Sub
        'End If

        mdlGeneral.tmpSearchMode = "PRODUCT LIST - Product Name"
        frmSearchProduct.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub

        PRODUCT1BindingSource1.Position = PRODUCT1BindingSource1.Find("PRODUCT_ID", tmpSearchResult)
        PRODUCT_CODEComboBox.Text = PRODUCT1BindingSource1.Current("PRODUCT_CODE")

        For x As Integer = 0 To dgvGroupingProduct.RowCount - 2
            If PRODUCT_CODEComboBox.Text = dgvGroupingProduct.Item("CHILD_PRODUCT_CODE", x).Value Then
                If Language = "Indonesian" Then
                    MsgBox("Produk Induk harus berbeda dari Produk Turunan di baris : " & x + 1 & "!", _
                    MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Parent Product must be different than Child Product at Row : " & x + 1 & "!", _
                    MsgBoxStyle.Critical, "DMI Retail")
                End If
                tmpPosition = PRODUCT_CODEComboBox.Text
                Me.PRODUCT2TableAdapter.Fill(Me.DS_GROUPING_PRODUCT.PRODUCT2)
                PRODUCT_CODEComboBox.Text = tmpPosition
                Exit Sub
            End If
        Next

        tmpChange = False
    End Sub



    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click

        If PRODUCT_NAMETextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan isi kolom Produk Induk !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please fill Parent Product fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Me.PRODUCT2TableAdapter.Fill(Me.DS_GROUPING_PRODUCT.PRODUCT2)
            PRODUCT_CODEComboBox.Text = ""
            PRODUCT_CODEComboBox.Focus()
            Exit Sub
        ElseIf dgvGroupingProduct.Item(1, 0).Value = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan isi kolom Produk Turunan !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please fill Child Product fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            tmpPosition = PRODUCT_CODEComboBox.Text
            Me.PRODUCT2TableAdapter.Fill(Me.DS_GROUPING_PRODUCT.PRODUCT2)
            PRODUCT_CODEComboBox.Text = tmpPosition
            dgvGroupingProduct.Focus()
            Exit Sub
        ElseIf CStr(dgvGroupingProduct.Item(3, 0).Value) = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan isi kolom Jumlah !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please fill Quantity fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            tmpPosition = PRODUCT_CODEComboBox.Text
            Me.PRODUCT2TableAdapter.Fill(Me.DS_GROUPING_PRODUCT.PRODUCT2)
            PRODUCT_CODEComboBox.Text = tmpPosition
            dgvGroupingProduct.Focus()
            Exit Sub
        End If

        ''<<This block is to check if datagrid has non-expected value
        For h As Integer = 0 To dgvGroupingProduct.RowCount - 2
            If CStr(dgvGroupingProduct.Item(3, h).Value) <> "" And
                dgvGroupingProduct.Item(1, h).Value = "" Then
                If Language = "Indonesian" Then
                    MsgBox("Silahkan isi semua kolom yang dibutuhkan !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Please fill all required fields !", MsgBoxStyle.Critical, "DMI Retail")
                End If
                dgvGroupingProduct.Rows.RemoveAt(h)
                Exit Sub
            End If

        Next


        ''<<This block of code is command to check a Product to be only once defined as Parent Product>>
        Dim tmpResult As Integer
        'GROUPING_PRODUCTTableAdapter.SP_CHECK_GROUPING_PRODUCT(PRODUCT1BindingSource1.Current("PRODUCT_ID"), tmpResult)
        GROUPING_PRODUCTTableAdapter.SP_CHECK_GROUPING_PRODUCT(PRODUCT2BindingSource.Current("PRODUCT_ID"), tmpResult)
        If tmpResult <> 0 Then
            If Language = "Indonesian" Then
                MsgBox("Produk ini telah didefinisikan sebagai Produk Induk !" & vbCrLf & "Silahkan ganti Produk Induk !", _
                   MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("This Product has been defined as Parent Product !" & vbCrLf & "Please change the Parent Product !", _
                   MsgBoxStyle.Critical, "DMI Retail")
            End If
            Me.PRODUCT2TableAdapter.Fill(Me.DS_GROUPING_PRODUCT.PRODUCT2)
            PRODUCT_CODEComboBox.Text = ""
            PRODUCT_CODEComboBox.Focus()
            Exit Sub
        End If



        ''<<This block of code is command to check if Child Product is grand parenting from Parent Product>>
        Dim tmpcheck As Integer
        For n As Integer = 0 To dgvGroupingProduct.RowCount - 2
            GROUPING_PRODUCTTableAdapter.SP_CHECK_GROUPING_VALIDATION(dgvGroupingProduct.Item(0, n).Value, PRODUCT2BindingSource.Current("PRODUCT_ID"), tmpcheck)

            If tmpcheck > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Produk Turunan sudah terdaftar sebagai Produk Induk bersangkutan !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Child Unit has defined as Grand Parent from this Parent Product !", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Me.PRODUCT2TableAdapter.Fill(Me.DS_GROUPING_PRODUCT.PRODUCT2)
                PRODUCT_CODEComboBox.Text = ""
                PRODUCT_CODEComboBox.Focus()
                Exit Sub
            ElseIf tmpcheck < 0 Then
                If Language = "Indonesian" Then
                    If MsgBox("Mohon periksa Daftar Pengelompokkan !" & vbCrLf & _
                           "Produk Induk sudah terdaftar dua kali sebagai Turunan." & vbCrLf & _
                           "Apakah anda yakin ingin melanjutkan ?", MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
                Else
                    If MsgBox("Please Check Grouping List !" & vbCrLf & _
                           "Parent Product has defined twice as Child Unit." & vbCrLf & _
                           "Do you really want to continue ?", MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
                End If
            End If
        Next


        For x As Integer = 0 To dgvGroupingProduct.RowCount - 2
            If PRODUCT_CODEComboBox.Text = dgvGroupingProduct.Item("CHILD_PRODUCT_CODE", x).Value Then
                If Language = "Indonesian" Then
                    MsgBox("Produk Turunan harus berbeda dengan Produk Induk !" & vbCrLf & _
                       "Row : " & x + 1, MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Child Product must be different than Parent Product !" & vbCrLf & _
                       "Row : " & x + 1, MsgBoxStyle.Critical, "DMI Retail")
                End If
                tmpPosition = PRODUCT_CODEComboBox.Text

                Me.PRODUCT2TableAdapter.Fill(Me.DS_GROUPING_PRODUCT.PRODUCT2)

                PRODUCT_CODEComboBox.Text = tmpPosition
                Exit Sub
            End If


            If dgvGroupingProduct.Item("QUANTITY", x).Value = 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Ada produk dengan jumlah '0 '." & vbCrLf & "Jumlah kosong tidak diperbolehkan !", _
                     MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("There's a product with '0' quantity." & vbCrLf & "Zero quantity doesn't allowed !", _
                     MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If
        Next


        ''<<This is command to insert Grouping Product to Database>>
        For n As Integer = 0 To dgvGroupingProduct.RowCount - 2

            GROUPING_PRODUCTTableAdapter.SP_GROUPING_PRODUCT("I", _
                                                             0, _
                                                             PRODUCT_CODEComboBox.Text, _
                                                             CInt(dgvGroupingProduct.Item(0, n).Value), _
                                                             CInt(dgvGroupingProduct.Item(3, n).Value), _
                                                             0, _
                                                             mdlGeneral.USER_ID, _
                                                             Now, _
                                                             0, _
                                                             DateSerial(4000, 12, 31))

        Next

        If Language = "Indonesian" Then
            MsgBox("Data Berhasil disimpan !", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully saved !", MsgBoxStyle.Information, "DMI Retail")
        End If

        PRODUCTTableAdapter.Fill(Me.DS_GROUPING_PRODUCT.PRODUCT)
        GROUPING_PRODUCTTableAdapter.Fill(Me.DS_GROUPING_PRODUCT.GROUPING_PRODUCT)
        PRODUCT2TableAdapter.Fill(Me.DS_GROUPING_PRODUCT.PRODUCT2)

        cmdAdd_Click(Nothing, Nothing)

        tmpChange = False
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        Me.Close()

        tmpChange = False
    End Sub


    Private Sub dgvGroupingProduct_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvGroupingProduct.CellContentClick

        If dgvGroupingProduct.Item("CHILD_PRODUCT_CODE", 0).Value = "" Then Exit Sub
        Try
            If dgvGroupingProduct.CurrentCell.ColumnIndex = 4 Then
                dgvGroupingProduct.Rows.RemoveAt(dgvGroupingProduct.CurrentCell.RowIndex)
            End If
        Catch
        End Try
        tmpChange = True
    End Sub

    Private Sub PRODUCT_CODEComboBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles PRODUCT_CODEComboBox.LostFocus

        If PRODUCT_CODEComboBox.Text = "" Then
            Exit Sub
        End If

        Try
            PRODUCT2TableAdapter.SP_GET_CATEGORY_PRODUCT(PRODUCT_CODEComboBox.Text, tmpCategory)
        Catch
        End Try

        If tmpCategory <> 1 Then
            If Language = "Indonesian" Then
                MsgBox("Produk yang anda input bukan barang !" & vbCrLf & _
                   "Silahkan gunakan Form Pencarian untuk menemukan produk yang benar !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You input Product which is not Goods !" & vbCrLf & _
                       "Please use the Search Form to find the right Products !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            PRODUCT_NAMETextBox.Text = ""
            PRODUCT_CODEComboBox.Text = ""

        End If

    End Sub


    Private Sub PRODUCT_CODEComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PRODUCT_CODEComboBox.TextChanged
        PRODUCT2TableAdapter.SP_GET_PRODUCT_NAME(PRODUCT_CODEComboBox.Text, PRODUCT_NAMETextBox.Text)

        ''dgvGroupingProduct.Item("QUANTITY", 0).Value = 0
        Try

            SP_GET_CHILD_PRODUCTTableAdapter.Fill(Me.DS_GROUPING_PRODUCT.SP_GET_CHILD_PRODUCT, PRODUCT_CODEComboBox.Text)

            If SP_GET_CHILD_PRODUCTBindingSource.Count > 0 Then
                tmpFillingDGV = True

                dgvGroupingProduct.Rows.Clear()

                dgvGroupingProduct.Rows.Insert(dgvGroupingProduct.RowCount - 1, SP_GET_CHILD_PRODUCTBindingSource.Count)

                For n As Integer = 0 To SP_GET_CHILD_PRODUCTBindingSource.Count - 1
                    SP_GET_CHILD_PRODUCTBindingSource.Position = n

                    dgvGroupingProduct.Item(1, n).Value = SP_GET_CHILD_PRODUCTBindingSource.Current("CHILD_PRODUCT_CODE")
                    dgvGroupingProduct.Item(2, n).Value = SP_GET_CHILD_PRODUCTBindingSource.Current("CHILD_PRODUCT_NAME")
                    dgvGroupingProduct.Item(3, n).Value = SP_GET_CHILD_PRODUCTBindingSource.Current("QUANTITY")
                    'n = n + 1
                Next

                dgvGroupingProduct.Enabled = False
                cmdDelete.Enabled = True
                cmdSave.Enabled = False
                tmpKeyMode = True

            Else

                cmdDelete.Enabled = False
                cmdSave.Enabled = True
                dgvGroupingProduct.Enabled = True
                dgvGroupingProduct.Rows.Clear()
                tmpKeyMode = False
            End If
        Catch

        End Try


        tmpChange = True
    End Sub


    Private Sub PRODUCT_NAMETextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PRODUCT_NAMETextBox.TextChanged
        If PRODUCT_NAMETextBox.Text <> "" Then
            cmdSave.Enabled = True
            'Else
            '    cmdSave.Enabled = False
        End If

        tmpChange = True
    End Sub

    Private Sub dgvGroupingProduct_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvGroupingProduct.CellValueChanged

        If tmpDataGridViewLoaded Then
            If dgvGroupingProduct.CurrentCell.ColumnIndex = 3 Then
                If Not IsNumeric(dgvGroupingProduct.CurrentCell.Value) Then
                    If Language = "Indonesian" Then
                        MsgBox("Anda memasukan nilai tak terduga !" & vbCrLf & _
                          "Baris Nomor : " & _
                           dgvGroupingProduct.CurrentCell.RowIndex + 1, MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("You entered unexpected quantity !" & vbCrLf & _
                          "Row number : " & _
                           dgvGroupingProduct.CurrentCell.RowIndex + 1, MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    dgvGroupingProduct.CurrentCell.Value = "0"
                    Exit Sub
                End If
            End If

            If tmpFillingDGV = False Then
                If dgvGroupingProduct.CurrentCell.ColumnIndex = 1 Then
                    Dim tmpProductName As String = ""

                    SP_LIST_PRODUCT_GOODSTableAdapter.SP_GET_PRODUCT_NAME(dgvGroupingProduct.CurrentCell.Value, tmpProductName)
                    Try
                        SP_LIST_PRODUCT_GOODSTableAdapter.SP_GET_CATEGORY_PRODUCT(dgvGroupingProduct.CurrentCell.Value, tmpCategory)
                    Catch
                    End Try
                    If tmpProductName = "" Then
                        If dgvGroupingProduct.CurrentCell.Value <> "" Then
                            If Language = "Indonesian" Then
                                MsgBox("Produk tidak ditemukan !", MsgBoxStyle.Critical, "DMI Retail")
                            Else
                                MsgBox("Product not found!", MsgBoxStyle.Critical, "DMI Retail")
                            End If
                            dgvGroupingProduct.Rows.RemoveAt(dgvGroupingProduct.CurrentCell.RowIndex)
                        End If
                        Exit Sub
                    End If

                    If tmpCategory <> 1 Then
                        If dgvGroupingProduct.CurrentCell.Value <> "" Then
                            If Language = "Indonesian" Then
                                MsgBox("Produk yang anda input bukan barang !" & vbCrLf & _
                                   "Silahkan gunakan Form Pencarian untuk menemukan produk yang benar !", MsgBoxStyle.Critical, "DMI Retail")
                            Else
                                MsgBox("You input Product which is not Goods !" & vbCrLf & _
                                       "Please use the Search Form to find the right Products !", MsgBoxStyle.Critical, "DMI Retail")
                            End If
                            dgvGroupingProduct.Rows.RemoveAt(dgvGroupingProduct.CurrentCell.RowIndex)
                        End If
                        Exit Sub
                    End If

                    If UCase(Trim(dgvGroupingProduct.CurrentCell.Value.ToString)) = UCase(PRODUCT_CODEComboBox.Text) Then
                        MsgBox("Child Product must be different than Parent Product !", MsgBoxStyle.Critical, "DMI Retail")
                        dgvGroupingProduct.Rows.RemoveAt(dgvGroupingProduct.CurrentCell.RowIndex)
                        Exit Sub
                    End If

                    tmpFillingDGV = True
                    AddDataToGrid2(dgvGroupingProduct, _
                                   dgvGroupingProduct.CurrentCell.Value, _
                                   1, _
                                   tmpProductName, _
                                   2, _
                                   3)

                End If
            End If
        End If

        tmpChange = True
    End Sub

    Private Sub dgvGroupingProduct_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvGroupingProduct.KeyUp
        If e.KeyCode = Keys.Delete Then
            If dgvGroupingProduct.CurrentRow.Index = dgvGroupingProduct.RowCount - 1 Then Exit Sub
            dgvGroupingProduct.Rows.RemoveAt(dgvGroupingProduct.CurrentCell.RowIndex)
        End If

        tmpChange = True
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        PRODUCT2TableAdapter.Fill(Me.DS_GROUPING_PRODUCT.PRODUCT2)
        GROUPING_PRODUCTTableAdapter.Fill(Me.DS_GROUPING_PRODUCT.GROUPING_PRODUCT)
        SP_LIST_PRODUCT_GOODSTableAdapter.Fill(Me.DS_GROUPING_PRODUCT.SP_LIST_PRODUCT_GOODS, Now)

        PRODUCT_CODEComboBox.Text = ""
        dgvGroupingProduct.Rows.Clear()

        tmpChange = False
    End Sub

    Private Sub PRODUCT_CODEComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PRODUCT_CODEComboBox.SelectedIndexChanged
        tmpChange = True
    End Sub

    
    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If Language = "Indonesian" Then
            If MsgBox("Apakah anda ingin menghapus pengelompokkan untuk produk ini ?", MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
        Else
            If MsgBox("Do you really want to delete this Grouping Product ?", MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
        End If

        GROUPING_PRODUCTTableAdapter.SP_GROUPING_PRODUCT("D", _
                                                         0, _
                                                         PRODUCT_CODEComboBox.Text, _
                                                         0, _
                                                         0, _
                                                         0, _
                                                         0, _
                                                         DateSerial(Today.Year, Today.Month, Today.Day), _
                                                         0, _
                                                         DateSerial(Today.Year, Today.Month, Today.Day))

        If Language = "Indonesian" Then
            MsgBox("Data pengelompokkan berhasil dihapus.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Grouping Product Successfully deleted.", MsgBoxStyle.Information, "DMI Retail")
        End If

        GROUPING_PRODUCTBindingSource.RemoveCurrent()
        GROUPING_PRODUCTTableAdapter.Fill(Me.DS_GROUPING_PRODUCT.GROUPING_PRODUCT)
        PRODUCT2TableAdapter.Fill(Me.DS_GROUPING_PRODUCT.PRODUCT2)
        'PRODUCT2BindingSource.Position = 0
        PRODUCT1TableAdapter.Fill(Me.DS_GROUPING_PRODUCT.PRODUCT1)

        cmdAdd_Click(Nothing, Nothing)

        tmpChange = False
    End Sub
End Class