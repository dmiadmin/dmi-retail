﻿Imports System.Data.SqlClient
Imports System.Data
Public Class frmListSplitProduct

    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
   

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        'Me.SP_LIST_PRODUCT_SPLITTableAdapter.Fill(DS_GROUPING_PRODUCT.SP_LIST_PRODUCT_SPLIT, dtpPeriod.Value)
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC	    @return_value = [dbo].[SP_LIST_PRODUCT_SPLIT]" & vbCrLf & _
              "         @PERIOD = N'" & dtpPeriod.Value & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.SP_LIST_PRODUCT_SPLITDataGridView.DataSource = dt
        If Language = "Indonesian" Then
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("DATE_SPLIT").HeaderText = "Tanggal"
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("PARENT_NAME").HeaderText = "Nama Induk"
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("QUANTITY_PARENT").HeaderText = "Jumlah"
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("WAREHOUSE_SOURCE").HeaderText = "Gudang Asal"
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("WAREHOUSE_DESTINATION").HeaderText = "Gudang Tujuan"
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("DESCRIPTION").HeaderText = "Keterangan"
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("QUANTITY_CHILD").Visible = False
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("PERIOD").Visible = False
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("CHILD_NAME").Visible = False
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("QUANTITY_PARENT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("DATE_SPLIT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("DATE_SPLIT").DefaultCellStyle.Format = "dd-MMM-yyyy"
        Else
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("DATE_SPLIT").HeaderText = "Date Split"
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("PARENT_NAME").HeaderText = "Parent Name"
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("QUANTITY_PARENT").HeaderText = "Qty"
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("WAREHOUSE_SOURCE").HeaderText = "Source"
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("WAREHOUSE_DESTINATION").HeaderText = "Destination"
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("DESCRIPTION").HeaderText = "Description"
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("QUANTITY_CHILD").Visible = False
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("CHILD_NAME").Visible = False
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("PERIOD").Visible = False
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("QUANTITY_PARENT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("DATE_SPLIT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            SP_LIST_PRODUCT_SPLITDataGridView.Columns("DATE_SPLIT").DefaultCellStyle.Format = "dd-MMM-yyyy"
        End If
    End Sub

    'Private Sub frmListSplitProduct_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
    '    If e.KeyCode = Keys.F12 Then
    '        cmdReport_Click(Nothing, Nothing)
    '    End If
    'End Sub

    Private Sub frmListSplitProduct_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Language = "Indonesian" Then
            Label1.Text = "Periode"
            cmdGenerate.Text = "PROSES"
            Me.Text = "Daftar Pemecahan Produk"
            cmdReport.Text = "Cetak"
            'SP_LIST_PRODUCT_SPLITDataGridView.Columns("DATE_SPLIT").HeaderText = "Tanggal"
            'SP_LIST_PRODUCT_SPLITDataGridView.Columns("PARENT_NAME").HeaderText = "Nama Induk"
            'SP_LIST_PRODUCT_SPLITDataGridView.Columns("QUANTITY").HeaderText = "Jumlah"
            'SP_LIST_PRODUCT_SPLITDataGridView.Columns("WAREHOUSE_SOURCE").HeaderText = "Gudang Asal"
            'SP_LIST_PRODUCT_SPLITDataGridView.Columns("WAREHOUSE_DESTINATION").HeaderText = "Gudang Tujuan"
            'SP_LIST_PRODUCT_SPLITDataGridView.Columns("DESCRIPTION").HeaderText = "Keterangan"
        End If
    End Sub

    'Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
    '    Dim RPV As New frmRepSplitProduct
    '    RPV.period = dtpPeriod.Value
    '    RPV.ReportViewer1.ZoomPercent = 100
    '    RPV.WindowState = FormWindowState.Maximized
    '    RPV.Show()
    'End Sub

End Class