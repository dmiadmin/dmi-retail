﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConfig
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmConfig))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.SALESMANBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SALE_WAREHOUSEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SALE_PAYMENT_METHODBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CUSTOMERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PURCHASE_WAREHOUSEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PURCHASE_PAYMENT_METHODBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VENDORBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PD_DESTINATION_WAREHOUSEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NEW_PD_SOURCE_WAREHOUSEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ACCOUNTBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.ACCOUNTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PD_SOURCE_WAREHOUSEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.PARAMETERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TabPage10 = New System.Windows.Forms.TabPage()
        Me.GroupBox12 = New System.Windows.Forms.GroupBox()
        Me.ACCOUNT_TYPELabel2 = New System.Windows.Forms.Label()
        Me.ACCOUNT_NAMELabel2 = New System.Windows.Forms.Label()
        Me.lblTypeAccountRS = New System.Windows.Forms.Label()
        Me.lblNameAccountRS = New System.Windows.Forms.Label()
        Me.lblNoAccountRS = New System.Windows.Forms.Label()
        Me.cmbAcountNumberSR = New System.Windows.Forms.ComboBox()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.ACCOUNT_TYPELabel1 = New System.Windows.Forms.Label()
        Me.ACCOUNT_NAMELabel1 = New System.Windows.Forms.Label()
        Me.lblTypeAccountRP = New System.Windows.Forms.Label()
        Me.lblNameAccountRP = New System.Windows.Forms.Label()
        Me.lblNoAccountRP = New System.Windows.Forms.Label()
        Me.cmbAccountNumberPR = New System.Windows.Forms.ComboBox()
        Me.TabPage9 = New System.Windows.Forms.TabPage()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.dgvMasterDataCode = New System.Windows.Forms.DataGridView()
        Me.MASTER_DATA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CODE_MASTER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabPage8 = New System.Windows.Forms.TabPage()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.cmbProductCategory = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.dgvDocCode = New System.Windows.Forms.DataGridView()
        Me.DOCUMENT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CODE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.PD_DESTINATION_WAREHOUSE_NAMEComboBox = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.PD_SOURCE_WAREHOUSE_NAMEComboBox = New System.Windows.Forms.ComboBox()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.chkAutoSplit = New System.Windows.Forms.CheckBox()
        Me.chkAutoProduct = New System.Windows.Forms.CheckBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.chkLockPrice = New System.Windows.Forms.CheckBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.SALESMAN_NAMEComboBox = New System.Windows.Forms.ComboBox()
        Me.WAREHOUSE_NAMELabel = New System.Windows.Forms.Label()
        Me.SALE_WAREHOUSE_NAMEComboBox = New System.Windows.Forms.ComboBox()
        Me.SALE_PAYMENT_METHOD_NAMEComboBox = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.CUSTOMER_NAMEComboBox = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.PURCHASE_WAREHOUSE_NAMEComboBox = New System.Windows.Forms.ComboBox()
        Me.PURCHASE_PAYMENT_METHOD_NAMEComboBox = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.VENDOR_NAMEComboBox = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblSafeStockMinute = New System.Windows.Forms.Label()
        Me.txtSafeStockInterval = New System.Windows.Forms.TextBox()
        Me.lblSafeStockInterval = New System.Windows.Forms.Label()
        Me.cmbLanguage = New System.Windows.Forms.ComboBox()
        Me.txtMaxPass = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.GroupBox5.SuspendLayout()
        CType(Me.SALESMANBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SALE_WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SALE_PAYMENT_METHODBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CUSTOMERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PURCHASE_WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PURCHASE_PAYMENT_METHODBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VENDORBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PD_DESTINATION_WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NEW_PD_SOURCE_WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ACCOUNTBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ACCOUNTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PD_SOURCE_WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PARAMETERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage10.SuspendLayout()
        Me.GroupBox12.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.TabPage9.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        CType(Me.dgvMasterDataCode, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage8.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.TabPage7.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        CType(Me.dgvDocCode, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage6.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.cmdUndo)
        Me.GroupBox5.Controls.Add(Me.Button1)
        Me.GroupBox5.Controls.Add(Me.cmdEdit)
        Me.GroupBox5.Controls.Add(Me.cmdDelete)
        Me.GroupBox5.Controls.Add(Me.cmdAdd)
        Me.GroupBox5.Location = New System.Drawing.Point(9, 258)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(530, 67)
        Me.GroupBox5.TabIndex = 9
        Me.GroupBox5.TabStop = False
        '
        'cmdUndo
        '
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(227, 21)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(74, 29)
        Me.cmdUndo.TabIndex = 26
        Me.cmdUndo.Text = "&Undo"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(328, 21)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(74, 29)
        Me.Button1.TabIndex = 27
        Me.Button1.Text = "&Save"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Enabled = False
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(125, 21)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(74, 29)
        Me.cmdEdit.TabIndex = 24
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Enabled = False
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(430, 21)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(74, 29)
        Me.cmdDelete.TabIndex = 25
        Me.cmdDelete.Text = "&Delete"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Enabled = False
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(21, 21)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(74, 29)
        Me.cmdAdd.TabIndex = 23
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'SALESMANBindingSource
        '
        Me.SALESMANBindingSource.DataMember = "SALESMAN"
        '
        'SALE_WAREHOUSEBindingSource
        '
        Me.SALE_WAREHOUSEBindingSource.DataMember = "WAREHOUSE"
        '
        'SALE_PAYMENT_METHODBindingSource
        '
        Me.SALE_PAYMENT_METHODBindingSource.DataMember = "PAYMENT_METHOD"
        '
        'PURCHASE_PAYMENT_METHODBindingSource
        '
        Me.PURCHASE_PAYMENT_METHODBindingSource.DataMember = "PAYMENT_METHOD"
        '
        'PD_DESTINATION_WAREHOUSEBindingSource
        '
        Me.PD_DESTINATION_WAREHOUSEBindingSource.DataMember = "WAREHOUSE"
        '
        'NEW_PD_SOURCE_WAREHOUSEBindingSource
        '
        Me.NEW_PD_SOURCE_WAREHOUSEBindingSource.DataMember = "WAREHOUSE"
        '
        'ACCOUNTBindingSource1
        '
        Me.ACCOUNTBindingSource1.DataMember = "ACCOUNT"
        '
        'ACCOUNTBindingSource
        '
        Me.ACCOUNTBindingSource.DataMember = "ACCOUNT"
        '
        'PD_SOURCE_WAREHOUSEBindingSource
        '
        Me.PD_SOURCE_WAREHOUSEBindingSource.DataMember = "WAREHOUSE"
        '
        'PARAMETERBindingSource
        '
        Me.PARAMETERBindingSource.DataMember = "PARAMETER"
        '
        'TabPage10
        '
        Me.TabPage10.AutoScroll = True
        Me.TabPage10.Controls.Add(Me.GroupBox12)
        Me.TabPage10.Controls.Add(Me.GroupBox11)
        Me.TabPage10.Location = New System.Drawing.Point(4, 44)
        Me.TabPage10.Name = "TabPage10"
        Me.TabPage10.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage10.Size = New System.Drawing.Size(522, 192)
        Me.TabPage10.TabIndex = 9
        Me.TabPage10.Text = " All Return  "
        Me.TabPage10.UseVisualStyleBackColor = True
        '
        'GroupBox12
        '
        Me.GroupBox12.Controls.Add(Me.ACCOUNT_TYPELabel2)
        Me.GroupBox12.Controls.Add(Me.ACCOUNT_NAMELabel2)
        Me.GroupBox12.Controls.Add(Me.lblTypeAccountRS)
        Me.GroupBox12.Controls.Add(Me.lblNameAccountRS)
        Me.GroupBox12.Controls.Add(Me.lblNoAccountRS)
        Me.GroupBox12.Controls.Add(Me.cmbAcountNumberSR)
        Me.GroupBox12.Location = New System.Drawing.Point(263, 8)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(253, 146)
        Me.GroupBox12.TabIndex = 19
        Me.GroupBox12.TabStop = False
        Me.GroupBox12.Text = "Sale Return"
        '
        'ACCOUNT_TYPELabel2
        '
        Me.ACCOUNT_TYPELabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ACCOUNTBindingSource1, "ACCOUNT_TYPE", True))
        Me.ACCOUNT_TYPELabel2.Location = New System.Drawing.Point(111, 101)
        Me.ACCOUNT_TYPELabel2.Name = "ACCOUNT_TYPELabel2"
        Me.ACCOUNT_TYPELabel2.Size = New System.Drawing.Size(100, 23)
        Me.ACCOUNT_TYPELabel2.TabIndex = 19
        Me.ACCOUNT_TYPELabel2.Text = "Label15"
        '
        'ACCOUNT_NAMELabel2
        '
        Me.ACCOUNT_NAMELabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ACCOUNTBindingSource1, "ACCOUNT_NAME", True))
        Me.ACCOUNT_NAMELabel2.Location = New System.Drawing.Point(111, 64)
        Me.ACCOUNT_NAMELabel2.Name = "ACCOUNT_NAMELabel2"
        Me.ACCOUNT_NAMELabel2.Size = New System.Drawing.Size(100, 23)
        Me.ACCOUNT_NAMELabel2.TabIndex = 18
        Me.ACCOUNT_NAMELabel2.Text = "Label15"
        '
        'lblTypeAccountRS
        '
        Me.lblTypeAccountRS.AutoSize = True
        Me.lblTypeAccountRS.Location = New System.Drawing.Point(6, 102)
        Me.lblTypeAccountRS.Name = "lblTypeAccountRS"
        Me.lblTypeAccountRS.Size = New System.Drawing.Size(90, 15)
        Me.lblTypeAccountRS.TabIndex = 17
        Me.lblTypeAccountRS.Text = "Account Type"
        '
        'lblNameAccountRS
        '
        Me.lblNameAccountRS.AutoSize = True
        Me.lblNameAccountRS.Location = New System.Drawing.Point(6, 65)
        Me.lblNameAccountRS.Name = "lblNameAccountRS"
        Me.lblNameAccountRS.Size = New System.Drawing.Size(94, 15)
        Me.lblNameAccountRS.TabIndex = 16
        Me.lblNameAccountRS.Text = "Account Name"
        '
        'lblNoAccountRS
        '
        Me.lblNoAccountRS.AutoSize = True
        Me.lblNoAccountRS.Location = New System.Drawing.Point(6, 29)
        Me.lblNoAccountRS.Name = "lblNoAccountRS"
        Me.lblNoAccountRS.Size = New System.Drawing.Size(107, 15)
        Me.lblNoAccountRS.TabIndex = 15
        Me.lblNoAccountRS.Text = "Account Number"
        '
        'cmbAcountNumberSR
        '
        Me.cmbAcountNumberSR.DataSource = Me.ACCOUNTBindingSource1
        Me.cmbAcountNumberSR.DisplayMember = "ACCOUNT_NUMBER"
        Me.cmbAcountNumberSR.FormattingEnabled = True
        Me.cmbAcountNumberSR.Location = New System.Drawing.Point(114, 26)
        Me.cmbAcountNumberSR.Name = "cmbAcountNumberSR"
        Me.cmbAcountNumberSR.Size = New System.Drawing.Size(123, 23)
        Me.cmbAcountNumberSR.TabIndex = 7
        Me.cmbAcountNumberSR.ValueMember = "ACCOUNT_ID"
        '
        'GroupBox11
        '
        Me.GroupBox11.Controls.Add(Me.ACCOUNT_TYPELabel1)
        Me.GroupBox11.Controls.Add(Me.ACCOUNT_NAMELabel1)
        Me.GroupBox11.Controls.Add(Me.lblTypeAccountRP)
        Me.GroupBox11.Controls.Add(Me.lblNameAccountRP)
        Me.GroupBox11.Controls.Add(Me.lblNoAccountRP)
        Me.GroupBox11.Controls.Add(Me.cmbAccountNumberPR)
        Me.GroupBox11.Location = New System.Drawing.Point(3, 7)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(254, 147)
        Me.GroupBox11.TabIndex = 18
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "Purchase Return"
        '
        'ACCOUNT_TYPELabel1
        '
        Me.ACCOUNT_TYPELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ACCOUNTBindingSource, "ACCOUNT_TYPE", True))
        Me.ACCOUNT_TYPELabel1.Location = New System.Drawing.Point(113, 102)
        Me.ACCOUNT_TYPELabel1.Name = "ACCOUNT_TYPELabel1"
        Me.ACCOUNT_TYPELabel1.Size = New System.Drawing.Size(100, 23)
        Me.ACCOUNT_TYPELabel1.TabIndex = 16
        Me.ACCOUNT_TYPELabel1.Text = "Label15"
        '
        'ACCOUNT_NAMELabel1
        '
        Me.ACCOUNT_NAMELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ACCOUNTBindingSource, "ACCOUNT_NAME", True))
        Me.ACCOUNT_NAMELabel1.Location = New System.Drawing.Point(113, 66)
        Me.ACCOUNT_NAMELabel1.Name = "ACCOUNT_NAMELabel1"
        Me.ACCOUNT_NAMELabel1.Size = New System.Drawing.Size(100, 23)
        Me.ACCOUNT_NAMELabel1.TabIndex = 15
        Me.ACCOUNT_NAMELabel1.Text = "Label15"
        '
        'lblTypeAccountRP
        '
        Me.lblTypeAccountRP.AutoSize = True
        Me.lblTypeAccountRP.Location = New System.Drawing.Point(6, 102)
        Me.lblTypeAccountRP.Name = "lblTypeAccountRP"
        Me.lblTypeAccountRP.Size = New System.Drawing.Size(90, 15)
        Me.lblTypeAccountRP.TabIndex = 14
        Me.lblTypeAccountRP.Text = "Account Type"
        '
        'lblNameAccountRP
        '
        Me.lblNameAccountRP.AutoSize = True
        Me.lblNameAccountRP.Location = New System.Drawing.Point(6, 65)
        Me.lblNameAccountRP.Name = "lblNameAccountRP"
        Me.lblNameAccountRP.Size = New System.Drawing.Size(94, 15)
        Me.lblNameAccountRP.TabIndex = 13
        Me.lblNameAccountRP.Text = "Account Name"
        '
        'lblNoAccountRP
        '
        Me.lblNoAccountRP.AutoSize = True
        Me.lblNoAccountRP.Location = New System.Drawing.Point(6, 29)
        Me.lblNoAccountRP.Name = "lblNoAccountRP"
        Me.lblNoAccountRP.Size = New System.Drawing.Size(107, 15)
        Me.lblNoAccountRP.TabIndex = 12
        Me.lblNoAccountRP.Text = "Account Number"
        '
        'cmbAccountNumberPR
        '
        Me.cmbAccountNumberPR.DataSource = Me.ACCOUNTBindingSource
        Me.cmbAccountNumberPR.DisplayMember = "ACCOUNT_NUMBER"
        Me.cmbAccountNumberPR.FormattingEnabled = True
        Me.cmbAccountNumberPR.Location = New System.Drawing.Point(116, 26)
        Me.cmbAccountNumberPR.Name = "cmbAccountNumberPR"
        Me.cmbAccountNumberPR.Size = New System.Drawing.Size(121, 23)
        Me.cmbAccountNumberPR.TabIndex = 1
        Me.cmbAccountNumberPR.ValueMember = "ACCOUNT_ID"
        '
        'TabPage9
        '
        Me.TabPage9.Controls.Add(Me.GroupBox10)
        Me.TabPage9.Location = New System.Drawing.Point(4, 44)
        Me.TabPage9.Name = "TabPage9"
        Me.TabPage9.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage9.Size = New System.Drawing.Size(522, 192)
        Me.TabPage9.TabIndex = 8
        Me.TabPage9.Text = " Master Data Code"
        Me.TabPage9.UseVisualStyleBackColor = True
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.dgvMasterDataCode)
        Me.GroupBox10.Location = New System.Drawing.Point(7, 0)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(505, 172)
        Me.GroupBox10.TabIndex = 0
        Me.GroupBox10.TabStop = False
        '
        'dgvMasterDataCode
        '
        Me.dgvMasterDataCode.AllowUserToAddRows = False
        Me.dgvMasterDataCode.AllowUserToDeleteRows = False
        Me.dgvMasterDataCode.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvMasterDataCode.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvMasterDataCode.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMasterDataCode.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.MASTER_DATA, Me.CODE_MASTER})
        Me.dgvMasterDataCode.Location = New System.Drawing.Point(69, 16)
        Me.dgvMasterDataCode.Name = "dgvMasterDataCode"
        Me.dgvMasterDataCode.Size = New System.Drawing.Size(357, 141)
        Me.dgvMasterDataCode.TabIndex = 0
        '
        'MASTER_DATA
        '
        Me.MASTER_DATA.HeaderText = "Master Data"
        Me.MASTER_DATA.Name = "MASTER_DATA"
        Me.MASTER_DATA.ReadOnly = True
        Me.MASTER_DATA.Width = 200
        '
        'CODE_MASTER
        '
        Me.CODE_MASTER.HeaderText = "Code"
        Me.CODE_MASTER.Name = "CODE_MASTER"
        '
        'TabPage8
        '
        Me.TabPage8.Controls.Add(Me.GroupBox9)
        Me.TabPage8.Location = New System.Drawing.Point(4, 44)
        Me.TabPage8.Name = "TabPage8"
        Me.TabPage8.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage8.Size = New System.Drawing.Size(522, 192)
        Me.TabPage8.TabIndex = 7
        Me.TabPage8.Text = " Product Category"
        Me.TabPage8.UseVisualStyleBackColor = True
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.cmbProductCategory)
        Me.GroupBox9.Controls.Add(Me.Label14)
        Me.GroupBox9.Location = New System.Drawing.Point(11, 6)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(501, 151)
        Me.GroupBox9.TabIndex = 2
        Me.GroupBox9.TabStop = False
        '
        'cmbProductCategory
        '
        Me.cmbProductCategory.FormattingEnabled = True
        Me.cmbProductCategory.Items.AddRange(New Object() {"Services", "Product", "Non Stock"})
        Me.cmbProductCategory.Location = New System.Drawing.Point(234, 54)
        Me.cmbProductCategory.Name = "cmbProductCategory"
        Me.cmbProductCategory.Size = New System.Drawing.Size(140, 23)
        Me.cmbProductCategory.TabIndex = 1
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(120, 57)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(108, 15)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Default Category"
        '
        'TabPage7
        '
        Me.TabPage7.Controls.Add(Me.GroupBox8)
        Me.TabPage7.Location = New System.Drawing.Point(4, 44)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage7.Size = New System.Drawing.Size(522, 192)
        Me.TabPage7.TabIndex = 6
        Me.TabPage7.Text = " Document Code"
        Me.TabPage7.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.dgvDocCode)
        Me.GroupBox8.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(516, 157)
        Me.GroupBox8.TabIndex = 1
        Me.GroupBox8.TabStop = False
        '
        'dgvDocCode
        '
        Me.dgvDocCode.AllowUserToAddRows = False
        Me.dgvDocCode.AllowUserToDeleteRows = False
        Me.dgvDocCode.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDocCode.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvDocCode.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDocCode.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DOCUMENT, Me.CODE})
        Me.dgvDocCode.Location = New System.Drawing.Point(76, 21)
        Me.dgvDocCode.Name = "dgvDocCode"
        Me.dgvDocCode.Size = New System.Drawing.Size(368, 114)
        Me.dgvDocCode.TabIndex = 0
        '
        'DOCUMENT
        '
        Me.DOCUMENT.HeaderText = "Document"
        Me.DOCUMENT.Name = "DOCUMENT"
        Me.DOCUMENT.ReadOnly = True
        Me.DOCUMENT.Width = 200
        '
        'CODE
        '
        Me.CODE.HeaderText = "Code"
        Me.CODE.Name = "CODE"
        '
        'TabPage6
        '
        Me.TabPage6.AutoScroll = True
        Me.TabPage6.Controls.Add(Me.GroupBox7)
        Me.TabPage6.Location = New System.Drawing.Point(4, 44)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(522, 192)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "Product Distribution"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.Label10)
        Me.GroupBox7.Controls.Add(Me.PD_DESTINATION_WAREHOUSE_NAMEComboBox)
        Me.GroupBox7.Controls.Add(Me.Label12)
        Me.GroupBox7.Controls.Add(Me.PD_SOURCE_WAREHOUSE_NAMEComboBox)
        Me.GroupBox7.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(506, 131)
        Me.GroupBox7.TabIndex = 5
        Me.GroupBox7.TabStop = False
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(48, 42)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(211, 20)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Default Warehouse Source"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'PD_DESTINATION_WAREHOUSE_NAMEComboBox
        '
        Me.PD_DESTINATION_WAREHOUSE_NAMEComboBox.DataSource = Me.PD_DESTINATION_WAREHOUSEBindingSource
        Me.PD_DESTINATION_WAREHOUSE_NAMEComboBox.DisplayMember = "WAREHOUSE_NAME"
        Me.PD_DESTINATION_WAREHOUSE_NAMEComboBox.FormattingEnabled = True
        Me.PD_DESTINATION_WAREHOUSE_NAMEComboBox.Location = New System.Drawing.Point(265, 71)
        Me.PD_DESTINATION_WAREHOUSE_NAMEComboBox.Name = "PD_DESTINATION_WAREHOUSE_NAMEComboBox"
        Me.PD_DESTINATION_WAREHOUSE_NAMEComboBox.Size = New System.Drawing.Size(144, 23)
        Me.PD_DESTINATION_WAREHOUSE_NAMEComboBox.TabIndex = 16
        Me.PD_DESTINATION_WAREHOUSE_NAMEComboBox.ValueMember = "WAREHOUSE_ID"
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(45, 74)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(214, 20)
        Me.Label12.TabIndex = 1
        Me.Label12.Text = "Default Warehouse Destination"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'PD_SOURCE_WAREHOUSE_NAMEComboBox
        '
        Me.PD_SOURCE_WAREHOUSE_NAMEComboBox.DataSource = Me.NEW_PD_SOURCE_WAREHOUSEBindingSource
        Me.PD_SOURCE_WAREHOUSE_NAMEComboBox.DisplayMember = "WAREHOUSE_NAME"
        Me.PD_SOURCE_WAREHOUSE_NAMEComboBox.FormattingEnabled = True
        Me.PD_SOURCE_WAREHOUSE_NAMEComboBox.Location = New System.Drawing.Point(265, 39)
        Me.PD_SOURCE_WAREHOUSE_NAMEComboBox.Name = "PD_SOURCE_WAREHOUSE_NAMEComboBox"
        Me.PD_SOURCE_WAREHOUSE_NAMEComboBox.Size = New System.Drawing.Size(144, 23)
        Me.PD_SOURCE_WAREHOUSE_NAMEComboBox.TabIndex = 15
        Me.PD_SOURCE_WAREHOUSE_NAMEComboBox.ValueMember = "WAREHOUSE_ID"
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.GroupBox4)
        Me.TabPage4.Location = New System.Drawing.Point(4, 44)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(522, 192)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Inventory"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.chkAutoSplit)
        Me.GroupBox4.Controls.Add(Me.chkAutoProduct)
        Me.GroupBox4.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(508, 171)
        Me.GroupBox4.TabIndex = 7
        Me.GroupBox4.TabStop = False
        '
        'chkAutoSplit
        '
        Me.chkAutoSplit.Location = New System.Drawing.Point(35, 47)
        Me.chkAutoSplit.Name = "chkAutoSplit"
        Me.chkAutoSplit.Size = New System.Drawing.Size(590, 20)
        Me.chkAutoSplit.TabIndex = 14
        Me.chkAutoSplit.Text = "Auto Split Product"
        Me.chkAutoSplit.UseVisualStyleBackColor = True
        '
        'chkAutoProduct
        '
        Me.chkAutoProduct.Enabled = False
        Me.chkAutoProduct.Location = New System.Drawing.Point(35, 21)
        Me.chkAutoProduct.Name = "chkAutoProduct"
        Me.chkAutoProduct.Size = New System.Drawing.Size(590, 20)
        Me.chkAutoProduct.TabIndex = 13
        Me.chkAutoProduct.Text = "Auto Product Movement"
        Me.chkAutoProduct.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.AutoScroll = True
        Me.TabPage3.Controls.Add(Me.GroupBox3)
        Me.TabPage3.Location = New System.Drawing.Point(4, 44)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(522, 192)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Sale"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.chkLockPrice)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.SALESMAN_NAMEComboBox)
        Me.GroupBox3.Controls.Add(Me.WAREHOUSE_NAMELabel)
        Me.GroupBox3.Controls.Add(Me.SALE_WAREHOUSE_NAMEComboBox)
        Me.GroupBox3.Controls.Add(Me.SALE_PAYMENT_METHOD_NAMEComboBox)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.CUSTOMER_NAMEComboBox)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(508, 180)
        Me.GroupBox3.TabIndex = 6
        Me.GroupBox3.TabStop = False
        '
        'chkLockPrice
        '
        Me.chkLockPrice.AutoSize = True
        Me.chkLockPrice.Location = New System.Drawing.Point(234, 137)
        Me.chkLockPrice.Name = "chkLockPrice"
        Me.chkLockPrice.Size = New System.Drawing.Size(127, 19)
        Me.chkLockPrice.TabIndex = 17
        Me.chkLockPrice.Text = "Lock Selling Price"
        Me.chkLockPrice.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(47, 111)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(181, 19)
        Me.Label13.TabIndex = 15
        Me.Label13.Text = "Default Salesman"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'SALESMAN_NAMEComboBox
        '
        Me.SALESMAN_NAMEComboBox.DataSource = Me.SALESMANBindingSource
        Me.SALESMAN_NAMEComboBox.DisplayMember = "SALESMAN_NAME"
        Me.SALESMAN_NAMEComboBox.FormattingEnabled = True
        Me.SALESMAN_NAMEComboBox.Location = New System.Drawing.Point(234, 108)
        Me.SALESMAN_NAMEComboBox.Name = "SALESMAN_NAMEComboBox"
        Me.SALESMAN_NAMEComboBox.Size = New System.Drawing.Size(158, 23)
        Me.SALESMAN_NAMEComboBox.TabIndex = 14
        Me.SALESMAN_NAMEComboBox.ValueMember = "SALESMAN_ID"
        '
        'WAREHOUSE_NAMELabel
        '
        Me.WAREHOUSE_NAMELabel.Location = New System.Drawing.Point(44, 53)
        Me.WAREHOUSE_NAMELabel.Name = "WAREHOUSE_NAMELabel"
        Me.WAREHOUSE_NAMELabel.Size = New System.Drawing.Size(184, 20)
        Me.WAREHOUSE_NAMELabel.TabIndex = 13
        Me.WAREHOUSE_NAMELabel.Text = "Default Warehouse"
        Me.WAREHOUSE_NAMELabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'SALE_WAREHOUSE_NAMEComboBox
        '
        Me.SALE_WAREHOUSE_NAMEComboBox.DataSource = Me.SALE_WAREHOUSEBindingSource
        Me.SALE_WAREHOUSE_NAMEComboBox.DisplayMember = "WAREHOUSE_NAME"
        Me.SALE_WAREHOUSE_NAMEComboBox.FormattingEnabled = True
        Me.SALE_WAREHOUSE_NAMEComboBox.Location = New System.Drawing.Point(234, 50)
        Me.SALE_WAREHOUSE_NAMEComboBox.Name = "SALE_WAREHOUSE_NAMEComboBox"
        Me.SALE_WAREHOUSE_NAMEComboBox.Size = New System.Drawing.Size(158, 23)
        Me.SALE_WAREHOUSE_NAMEComboBox.TabIndex = 11
        Me.SALE_WAREHOUSE_NAMEComboBox.ValueMember = "WAREHOUSE_ID"
        '
        'SALE_PAYMENT_METHOD_NAMEComboBox
        '
        Me.SALE_PAYMENT_METHOD_NAMEComboBox.DataSource = Me.SALE_PAYMENT_METHODBindingSource
        Me.SALE_PAYMENT_METHOD_NAMEComboBox.DisplayMember = "PAYMENT_METHOD_NAME"
        Me.SALE_PAYMENT_METHOD_NAMEComboBox.FormattingEnabled = True
        Me.SALE_PAYMENT_METHOD_NAMEComboBox.Location = New System.Drawing.Point(234, 21)
        Me.SALE_PAYMENT_METHOD_NAMEComboBox.Name = "SALE_PAYMENT_METHOD_NAMEComboBox"
        Me.SALE_PAYMENT_METHOD_NAMEComboBox.Size = New System.Drawing.Size(158, 23)
        Me.SALE_PAYMENT_METHOD_NAMEComboBox.TabIndex = 10
        Me.SALE_PAYMENT_METHOD_NAMEComboBox.ValueMember = "PAYMENT_METHOD_ID"
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(44, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(184, 20)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Default Payment Method"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'CUSTOMER_NAMEComboBox
        '
        Me.CUSTOMER_NAMEComboBox.DataSource = Me.CUSTOMERBindingSource
        Me.CUSTOMER_NAMEComboBox.DisplayMember = "CUSTOMER_NAME"
        Me.CUSTOMER_NAMEComboBox.FormattingEnabled = True
        Me.CUSTOMER_NAMEComboBox.Location = New System.Drawing.Point(234, 79)
        Me.CUSTOMER_NAMEComboBox.Name = "CUSTOMER_NAMEComboBox"
        Me.CUSTOMER_NAMEComboBox.Size = New System.Drawing.Size(158, 23)
        Me.CUSTOMER_NAMEComboBox.TabIndex = 12
        Me.CUSTOMER_NAMEComboBox.ValueMember = "CUSTOMER_ID"
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(47, 83)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(181, 19)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Default Customer"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'TabPage2
        '
        Me.TabPage2.AutoScroll = True
        Me.TabPage2.Controls.Add(Me.GroupBox2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 44)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(522, 192)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Purchase"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.PURCHASE_WAREHOUSE_NAMEComboBox)
        Me.GroupBox2.Controls.Add(Me.PURCHASE_PAYMENT_METHOD_NAMEComboBox)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.VENDOR_NAMEComboBox)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(508, 151)
        Me.GroupBox2.TabIndex = 8
        Me.GroupBox2.TabStop = False
        '
        'PURCHASE_WAREHOUSE_NAMEComboBox
        '
        Me.PURCHASE_WAREHOUSE_NAMEComboBox.DataSource = Me.PURCHASE_WAREHOUSEBindingSource
        Me.PURCHASE_WAREHOUSE_NAMEComboBox.DisplayMember = "WAREHOUSE_NAME"
        Me.PURCHASE_WAREHOUSE_NAMEComboBox.FormattingEnabled = True
        Me.PURCHASE_WAREHOUSE_NAMEComboBox.Location = New System.Drawing.Point(217, 64)
        Me.PURCHASE_WAREHOUSE_NAMEComboBox.Name = "PURCHASE_WAREHOUSE_NAMEComboBox"
        Me.PURCHASE_WAREHOUSE_NAMEComboBox.Size = New System.Drawing.Size(158, 23)
        Me.PURCHASE_WAREHOUSE_NAMEComboBox.TabIndex = 8
        Me.PURCHASE_WAREHOUSE_NAMEComboBox.ValueMember = "WAREHOUSE_ID"
        '
        'PURCHASE_PAYMENT_METHOD_NAMEComboBox
        '
        Me.PURCHASE_PAYMENT_METHOD_NAMEComboBox.DataSource = Me.PURCHASE_PAYMENT_METHODBindingSource
        Me.PURCHASE_PAYMENT_METHOD_NAMEComboBox.DisplayMember = "PAYMENT_METHOD_NAME"
        Me.PURCHASE_PAYMENT_METHOD_NAMEComboBox.FormattingEnabled = True
        Me.PURCHASE_PAYMENT_METHOD_NAMEComboBox.Location = New System.Drawing.Point(217, 35)
        Me.PURCHASE_PAYMENT_METHOD_NAMEComboBox.Name = "PURCHASE_PAYMENT_METHOD_NAMEComboBox"
        Me.PURCHASE_PAYMENT_METHOD_NAMEComboBox.Size = New System.Drawing.Size(158, 23)
        Me.PURCHASE_PAYMENT_METHOD_NAMEComboBox.TabIndex = 7
        Me.PURCHASE_PAYMENT_METHOD_NAMEComboBox.ValueMember = "PAYMENT_METHOD_ID"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(30, 38)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(181, 20)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Default Payment Method"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'VENDOR_NAMEComboBox
        '
        Me.VENDOR_NAMEComboBox.DataSource = Me.VENDORBindingSource
        Me.VENDOR_NAMEComboBox.DisplayMember = "VENDOR_NAME"
        Me.VENDOR_NAMEComboBox.FormattingEnabled = True
        Me.VENDOR_NAMEComboBox.Location = New System.Drawing.Point(217, 93)
        Me.VENDOR_NAMEComboBox.Name = "VENDOR_NAMEComboBox"
        Me.VENDOR_NAMEComboBox.Size = New System.Drawing.Size(158, 23)
        Me.VENDOR_NAMEComboBox.TabIndex = 9
        Me.VENDOR_NAMEComboBox.ValueMember = "VENDOR_ID"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(33, 67)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(178, 20)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Default Warehouse"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(33, 97)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(178, 19)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Default Vendor"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 44)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(522, 192)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblSafeStockMinute)
        Me.GroupBox1.Controls.Add(Me.txtSafeStockInterval)
        Me.GroupBox1.Controls.Add(Me.lblSafeStockInterval)
        Me.GroupBox1.Controls.Add(Me.cmbLanguage)
        Me.GroupBox1.Controls.Add(Me.txtMaxPass)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(510, 171)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        '
        'lblSafeStockMinute
        '
        Me.lblSafeStockMinute.AutoSize = True
        Me.lblSafeStockMinute.Location = New System.Drawing.Point(328, 92)
        Me.lblSafeStockMinute.Name = "lblSafeStockMinute"
        Me.lblSafeStockMinute.Size = New System.Drawing.Size(46, 15)
        Me.lblSafeStockMinute.TabIndex = 15
        Me.lblSafeStockMinute.Text = "Minute"
        '
        'txtSafeStockInterval
        '
        Me.txtSafeStockInterval.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtSafeStockInterval.Location = New System.Drawing.Point(273, 89)
        Me.txtSafeStockInterval.MaxLength = 3
        Me.txtSafeStockInterval.Name = "txtSafeStockInterval"
        Me.txtSafeStockInterval.Size = New System.Drawing.Size(49, 22)
        Me.txtSafeStockInterval.TabIndex = 14
        Me.txtSafeStockInterval.Tag = "M"
        Me.txtSafeStockInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblSafeStockInterval
        '
        Me.lblSafeStockInterval.Location = New System.Drawing.Point(80, 92)
        Me.lblSafeStockInterval.Name = "lblSafeStockInterval"
        Me.lblSafeStockInterval.Size = New System.Drawing.Size(187, 18)
        Me.lblSafeStockInterval.TabIndex = 13
        Me.lblSafeStockInterval.Text = "Safe Stock Checking Interval"
        Me.lblSafeStockInterval.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cmbLanguage
        '
        Me.cmbLanguage.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.cmbLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLanguage.FormattingEnabled = True
        Me.cmbLanguage.Items.AddRange(New Object() {"Indonesian", "English"})
        Me.cmbLanguage.Location = New System.Drawing.Point(273, 32)
        Me.cmbLanguage.Name = "cmbLanguage"
        Me.cmbLanguage.Size = New System.Drawing.Size(119, 23)
        Me.cmbLanguage.TabIndex = 1
        Me.cmbLanguage.Tag = "M"
        '
        'txtMaxPass
        '
        Me.txtMaxPass.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtMaxPass.Location = New System.Drawing.Point(273, 61)
        Me.txtMaxPass.MaxLength = 2
        Me.txtMaxPass.Name = "txtMaxPass"
        Me.txtMaxPass.Size = New System.Drawing.Size(48, 22)
        Me.txtMaxPass.TabIndex = 2
        Me.txtMaxPass.Tag = "M"
        Me.txtMaxPass.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(106, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(161, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Language"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(77, 64)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(190, 15)
        Me.Label11.TabIndex = 12
        Me.Label11.Text = "Maximum Wrong Password"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.TabPage7)
        Me.TabControl1.Controls.Add(Me.TabPage8)
        Me.TabControl1.Controls.Add(Me.TabPage9)
        Me.TabControl1.Controls.Add(Me.TabPage10)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Multiline = True
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(530, 240)
        Me.TabControl1.TabIndex = 0
        '
        'frmConfig
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(551, 327)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.TabControl1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmConfig"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Configuration"
        Me.Text = "Configuration"
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.SALESMANBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SALE_WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SALE_PAYMENT_METHODBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CUSTOMERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PURCHASE_WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PURCHASE_PAYMENT_METHODBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VENDORBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PD_DESTINATION_WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NEW_PD_SOURCE_WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ACCOUNTBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ACCOUNTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PD_SOURCE_WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PARAMETERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage10.ResumeLayout(False)
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox12.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox11.PerformLayout()
        Me.TabPage9.ResumeLayout(False)
        Me.GroupBox10.ResumeLayout(False)
        CType(Me.dgvMasterDataCode, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage8.ResumeLayout(False)
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.TabPage7.ResumeLayout(False)
        Me.GroupBox8.ResumeLayout(False)
        CType(Me.dgvDocCode, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage6.ResumeLayout(False)
        Me.GroupBox7.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents PARAMETERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents PURCHASE_PAYMENT_METHODBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PURCHASE_WAREHOUSEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VENDORBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SALE_PAYMENT_METHODBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SALE_WAREHOUSEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CUSTOMERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PD_SOURCE_WAREHOUSEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PD_DESTINATION_WAREHOUSEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SALESMANBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents NEW_PD_SOURCE_WAREHOUSEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ACCOUNTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ACCOUNTBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents TabPage10 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
    Friend WithEvents ACCOUNT_TYPELabel2 As System.Windows.Forms.Label
    Friend WithEvents ACCOUNT_NAMELabel2 As System.Windows.Forms.Label
    Friend WithEvents lblTypeAccountRS As System.Windows.Forms.Label
    Friend WithEvents lblNameAccountRS As System.Windows.Forms.Label
    Friend WithEvents lblNoAccountRS As System.Windows.Forms.Label
    Friend WithEvents cmbAcountNumberSR As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents ACCOUNT_TYPELabel1 As System.Windows.Forms.Label
    Friend WithEvents ACCOUNT_NAMELabel1 As System.Windows.Forms.Label
    Friend WithEvents lblTypeAccountRP As System.Windows.Forms.Label
    Friend WithEvents lblNameAccountRP As System.Windows.Forms.Label
    Friend WithEvents lblNoAccountRP As System.Windows.Forms.Label
    Friend WithEvents cmbAccountNumberPR As System.Windows.Forms.ComboBox
    Friend WithEvents TabPage9 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvMasterDataCode As System.Windows.Forms.DataGridView
    Friend WithEvents MASTER_DATA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CODE_MASTER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TabPage8 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbProductCategory As System.Windows.Forms.ComboBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TabPage7 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvDocCode As System.Windows.Forms.DataGridView
    Friend WithEvents DOCUMENT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CODE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents PD_DESTINATION_WAREHOUSE_NAMEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents PD_SOURCE_WAREHOUSE_NAMEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents chkAutoSplit As System.Windows.Forms.CheckBox
    Friend WithEvents chkAutoProduct As System.Windows.Forms.CheckBox
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents SALESMAN_NAMEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents WAREHOUSE_NAMELabel As System.Windows.Forms.Label
    Friend WithEvents SALE_WAREHOUSE_NAMEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents SALE_PAYMENT_METHOD_NAMEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents CUSTOMER_NAMEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents PURCHASE_WAREHOUSE_NAMEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents PURCHASE_PAYMENT_METHOD_NAMEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents VENDOR_NAMEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblSafeStockMinute As System.Windows.Forms.Label
    Friend WithEvents txtSafeStockInterval As System.Windows.Forms.TextBox
    Friend WithEvents lblSafeStockInterval As System.Windows.Forms.Label
    Friend WithEvents cmbLanguage As System.Windows.Forms.ComboBox
    Friend WithEvents txtMaxPass As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents chkLockPrice As System.Windows.Forms.CheckBox
End Class
