﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListInvoicePurchase
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListInvoicePurchase))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.cmdGenerate = New System.Windows.Forms.Button()
        Me.dtpPeriod = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.SP_GET_INVOICE_PURCHASEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_GET_INVOICE_PURCHASEDataGridView = New System.Windows.Forms.DataGridView()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.PURCHASE_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RECEIPT_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MATURITY_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VENDOR_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BALANCE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        CType(Me.SP_GET_INVOICE_PURCHASEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_GET_INVOICE_PURCHASEDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdGenerate
        '
        Me.cmdGenerate.Image = CType(resources.GetObject("cmdGenerate.Image"), System.Drawing.Image)
        Me.cmdGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdGenerate.Location = New System.Drawing.Point(254, 51)
        Me.cmdGenerate.Name = "cmdGenerate"
        Me.cmdGenerate.Size = New System.Drawing.Size(99, 39)
        Me.cmdGenerate.TabIndex = 5
        Me.cmdGenerate.Text = "Generate"
        Me.cmdGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdGenerate.UseVisualStyleBackColor = True
        '
        'dtpPeriod
        '
        Me.dtpPeriod.CustomFormat = "MMMM- yyyy"
        Me.dtpPeriod.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPeriod.Location = New System.Drawing.Point(230, 18)
        Me.dtpPeriod.Name = "dtpPeriod"
        Me.dtpPeriod.ShowUpDown = True
        Me.dtpPeriod.Size = New System.Drawing.Size(147, 22)
        Me.dtpPeriod.TabIndex = 4
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmdGenerate)
        Me.GroupBox1.Controls.Add(Me.dtpPeriod)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(606, 106)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        '
        'SP_GET_INVOICE_PURCHASEBindingSource
        '
        Me.SP_GET_INVOICE_PURCHASEBindingSource.DataMember = "SP_GET_INVOICE_PURCHASE"
        '
        'SP_GET_INVOICE_PURCHASETableAdapter
        '
        '
        'SP_GET_INVOICE_PURCHASEDataGridView
        '
        Me.SP_GET_INVOICE_PURCHASEDataGridView.AllowUserToAddRows = False
        Me.SP_GET_INVOICE_PURCHASEDataGridView.AllowUserToDeleteRows = False
        Me.SP_GET_INVOICE_PURCHASEDataGridView.AutoGenerateColumns = False
        Me.SP_GET_INVOICE_PURCHASEDataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SP_GET_INVOICE_PURCHASEDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.SP_GET_INVOICE_PURCHASEDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.SP_GET_INVOICE_PURCHASEDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PURCHASE_DATE, Me.RECEIPT_NO, Me.MATURITY_DATE, Me.VENDOR_NAME, Me.BALANCE})
        Me.SP_GET_INVOICE_PURCHASEDataGridView.DataSource = Me.SP_GET_INVOICE_PURCHASEBindingSource
        Me.SP_GET_INVOICE_PURCHASEDataGridView.Location = New System.Drawing.Point(6, 21)
        Me.SP_GET_INVOICE_PURCHASEDataGridView.Name = "SP_GET_INVOICE_PURCHASEDataGridView"
        Me.SP_GET_INVOICE_PURCHASEDataGridView.ReadOnly = True
        Me.SP_GET_INVOICE_PURCHASEDataGridView.RowHeadersWidth = 28
        Me.SP_GET_INVOICE_PURCHASEDataGridView.Size = New System.Drawing.Size(594, 255)
        Me.SP_GET_INVOICE_PURCHASEDataGridView.TabIndex = 8
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.SP_GET_INVOICE_PURCHASEDataGridView)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 124)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(606, 282)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        '
        'PURCHASE_DATE
        '
        Me.PURCHASE_DATE.DataPropertyName = "PURCHASE_DATE"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "dd-MMM-yyyy"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.PURCHASE_DATE.DefaultCellStyle = DataGridViewCellStyle2
        Me.PURCHASE_DATE.HeaderText = "Purchase Date"
        Me.PURCHASE_DATE.Name = "PURCHASE_DATE"
        Me.PURCHASE_DATE.ReadOnly = True
        '
        'RECEIPT_NO
        '
        Me.RECEIPT_NO.DataPropertyName = "RECEIPT_NO"
        Me.RECEIPT_NO.HeaderText = "Receipt No"
        Me.RECEIPT_NO.Name = "RECEIPT_NO"
        Me.RECEIPT_NO.ReadOnly = True
        '
        'MATURITY_DATE
        '
        Me.MATURITY_DATE.DataPropertyName = "MATURITY_DATE"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "dd-MMM-yyyy"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.MATURITY_DATE.DefaultCellStyle = DataGridViewCellStyle3
        Me.MATURITY_DATE.HeaderText = "Maturity Date"
        Me.MATURITY_DATE.Name = "MATURITY_DATE"
        Me.MATURITY_DATE.ReadOnly = True
        '
        'VENDOR_NAME
        '
        Me.VENDOR_NAME.DataPropertyName = "VENDOR_NAME"
        Me.VENDOR_NAME.HeaderText = "Vendor Name"
        Me.VENDOR_NAME.Name = "VENDOR_NAME"
        Me.VENDOR_NAME.ReadOnly = True
        Me.VENDOR_NAME.Width = 140
        '
        'BALANCE
        '
        Me.BALANCE.DataPropertyName = "BALANCE"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.BALANCE.DefaultCellStyle = DataGridViewCellStyle4
        Me.BALANCE.HeaderText = "Balance"
        Me.BALANCE.Name = "BALANCE"
        Me.BALANCE.ReadOnly = True
        '
        'frmListInvoicePurchase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(630, 418)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmListInvoicePurchase"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "List Invoice Purchase"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.SP_GET_INVOICE_PURCHASEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_GET_INVOICE_PURCHASEDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmdGenerate As System.Windows.Forms.Button
    Friend WithEvents dtpPeriod As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents SP_GET_INVOICE_PURCHASEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_GET_INVOICE_PURCHASEDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents PURCHASE_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RECEIPT_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MATURITY_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VENDOR_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BALANCE As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
