﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReceivablePayment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReceivablePayment))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.RECEIPT_NOTextBox = New System.Windows.Forms.TextBox()
        Me.SP_RECEIVABLE_PAYMENT_HEADERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_PAYMENT_TRANSACTION = New DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTION()
        Me.CUSTOMER_NAMETextBox = New System.Windows.Forms.TextBox()
        Me.dtpMATURITY_DATE = New System.Windows.Forms.DateTimePicker()
        Me.lblReceiptNo = New System.Windows.Forms.Label()
        Me.lblMarturity = New System.Windows.Forms.Label()
        Me.lblGrandTotal = New System.Windows.Forms.Label()
        Me.TOTAL_RECEIVABLETextBox = New System.Windows.Forms.TextBox()
        Me.lblCustomer = New System.Windows.Forms.Label()
        Me.cmdSearchReceipt = New System.Windows.Forms.PictureBox()
        Me.cmdSearchCustomer = New System.Windows.Forms.PictureBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.BALANCETextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtPaymentNo = New System.Windows.Forms.TextBox()
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblPayNo = New System.Windows.Forms.Label()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.lblPayMethod = New System.Windows.Forms.Label()
        Me.lblPayAmount = New System.Windows.Forms.Label()
        Me.lblPayDate = New System.Windows.Forms.Label()
        Me.PAYMENT_METHOD_IDComboBox = New System.Windows.Forms.ComboBox()
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.SPRECEIVABLEPAYMENTHEADERSPRECEIVABLEPAYMENTDETAILBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.PAYMENT_DATEDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.PAYMENT_AMOUNTTextBox = New System.Windows.Forms.TextBox()
        Me.DESCRIPTIONTextBox = New System.Windows.Forms.TextBox()
        Me.SP_RECEIVABLE_PAYMENT_DETAILTableAdapter = New DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.SP_RECEIVABLE_PAYMENT_DETAILTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.TableAdapterManager()
        Me.PAYMENT_METHODTableAdapter = New DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.PAYMENT_METHODTableAdapter()
        Me.RECEIVABLE_PAYMENTTableAdapter = New DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.RECEIVABLE_PAYMENTTableAdapter()
        Me.SP_RECEIVABLE_PAYMENT_HEADERTableAdapter = New DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.SP_RECEIVABLE_PAYMENT_HEADERTableAdapter()
        Me.RECEIVABLE_PAYMENTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PAYMENT_METHODBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblBalance = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.SP_RECEIVABLE_PAYMENT_HEADERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_PAYMENT_TRANSACTION, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmdSearchReceipt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmdSearchCustomer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.SP_RECEIVABLE_PAYMENT_DETAILBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_RECEIVABLE_PAYMENT_DETAILBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SPRECEIVABLEPAYMENTHEADERSPRECEIVABLEPAYMENTDETAILBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.SuspendLayout()
        CType(Me.RECEIVABLE_PAYMENTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PAYMENT_METHODBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.RECEIPT_NOTextBox)
        Me.GroupBox1.Controls.Add(Me.CUSTOMER_NAMETextBox)
        Me.GroupBox1.Controls.Add(Me.dtpMATURITY_DATE)
        Me.GroupBox1.Controls.Add(Me.lblReceiptNo)
        Me.GroupBox1.Controls.Add(Me.lblMarturity)
        Me.GroupBox1.Controls.Add(Me.lblGrandTotal)
        Me.GroupBox1.Controls.Add(Me.TOTAL_RECEIVABLETextBox)
        Me.GroupBox1.Controls.Add(Me.lblCustomer)
        Me.GroupBox1.Controls.Add(Me.cmdSearchReceipt)
        Me.GroupBox1.Controls.Add(Me.cmdSearchCustomer)
        Me.GroupBox1.Cursor = System.Windows.Forms.Cursors.Default
        Me.GroupBox1.Location = New System.Drawing.Point(13, 15)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(515, 149)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        '
        'RECEIPT_NOTextBox
        '
        Me.RECEIPT_NOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SP_RECEIVABLE_PAYMENT_HEADERBindingSource, "RECEIPT_NO", True))
        Me.RECEIPT_NOTextBox.Location = New System.Drawing.Point(181, 50)
        Me.RECEIPT_NOTextBox.Name = "RECEIPT_NOTextBox"
        Me.RECEIPT_NOTextBox.ReadOnly = True
        Me.RECEIPT_NOTextBox.Size = New System.Drawing.Size(200, 22)
        Me.RECEIPT_NOTextBox.TabIndex = 2
        '
        'SP_RECEIVABLE_PAYMENT_HEADERBindingSource
        '
        Me.SP_RECEIVABLE_PAYMENT_HEADERBindingSource.DataMember = "SP_RECEIVABLE_PAYMENT_HEADER"
        Me.SP_RECEIVABLE_PAYMENT_HEADERBindingSource.DataSource = Me.DS_PAYMENT_TRANSACTION
        '
        'DS_PAYMENT_TRANSACTION
        '
        Me.DS_PAYMENT_TRANSACTION.DataSetName = "DS_PAYMENT_TRANSACTION"
        Me.DS_PAYMENT_TRANSACTION.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CUSTOMER_NAMETextBox
        '
        Me.CUSTOMER_NAMETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SP_RECEIVABLE_PAYMENT_HEADERBindingSource, "CUSTOMER_NAME", True))
        Me.CUSTOMER_NAMETextBox.Location = New System.Drawing.Point(181, 21)
        Me.CUSTOMER_NAMETextBox.Name = "CUSTOMER_NAMETextBox"
        Me.CUSTOMER_NAMETextBox.ReadOnly = True
        Me.CUSTOMER_NAMETextBox.Size = New System.Drawing.Size(200, 22)
        Me.CUSTOMER_NAMETextBox.TabIndex = 1
        '
        'dtpMATURITY_DATE
        '
        Me.dtpMATURITY_DATE.CustomFormat = "dd MMM yyyy"
        Me.dtpMATURITY_DATE.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.SP_RECEIVABLE_PAYMENT_HEADERBindingSource, "MATURITY_DATE", True))
        Me.dtpMATURITY_DATE.Enabled = False
        Me.dtpMATURITY_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpMATURITY_DATE.Location = New System.Drawing.Point(181, 79)
        Me.dtpMATURITY_DATE.Name = "dtpMATURITY_DATE"
        Me.dtpMATURITY_DATE.Size = New System.Drawing.Size(134, 22)
        Me.dtpMATURITY_DATE.TabIndex = 3
        '
        'lblReceiptNo
        '
        Me.lblReceiptNo.Location = New System.Drawing.Point(68, 53)
        Me.lblReceiptNo.Name = "lblReceiptNo"
        Me.lblReceiptNo.Size = New System.Drawing.Size(107, 15)
        Me.lblReceiptNo.TabIndex = 17
        Me.lblReceiptNo.Text = "Receipt No :"
        Me.lblReceiptNo.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblMarturity
        '
        Me.lblMarturity.Location = New System.Drawing.Point(54, 85)
        Me.lblMarturity.Name = "lblMarturity"
        Me.lblMarturity.Size = New System.Drawing.Size(121, 15)
        Me.lblMarturity.TabIndex = 18
        Me.lblMarturity.Text = "Marturity Date :"
        Me.lblMarturity.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblGrandTotal
        '
        Me.lblGrandTotal.Location = New System.Drawing.Point(90, 110)
        Me.lblGrandTotal.Name = "lblGrandTotal"
        Me.lblGrandTotal.Size = New System.Drawing.Size(85, 15)
        Me.lblGrandTotal.TabIndex = 19
        Me.lblGrandTotal.Text = "Grand Total :"
        Me.lblGrandTotal.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'TOTAL_RECEIVABLETextBox
        '
        Me.TOTAL_RECEIVABLETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SP_RECEIVABLE_PAYMENT_HEADERBindingSource, "TOTAL_RECEIVABLE", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.TOTAL_RECEIVABLETextBox.Location = New System.Drawing.Point(181, 107)
        Me.TOTAL_RECEIVABLETextBox.Name = "TOTAL_RECEIVABLETextBox"
        Me.TOTAL_RECEIVABLETextBox.ReadOnly = True
        Me.TOTAL_RECEIVABLETextBox.Size = New System.Drawing.Size(162, 22)
        Me.TOTAL_RECEIVABLETextBox.TabIndex = 4
        Me.TOTAL_RECEIVABLETextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCustomer
        '
        Me.lblCustomer.Location = New System.Drawing.Point(38, 24)
        Me.lblCustomer.Name = "lblCustomer"
        Me.lblCustomer.Size = New System.Drawing.Size(137, 13)
        Me.lblCustomer.TabIndex = 16
        Me.lblCustomer.Text = "Customer Name :"
        Me.lblCustomer.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cmdSearchReceipt
        '
        Me.cmdSearchReceipt.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchReceipt.Image = CType(resources.GetObject("cmdSearchReceipt.Image"), System.Drawing.Image)
        Me.cmdSearchReceipt.Location = New System.Drawing.Point(388, 50)
        Me.cmdSearchReceipt.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchReceipt.Name = "cmdSearchReceipt"
        Me.cmdSearchReceipt.Size = New System.Drawing.Size(42, 23)
        Me.cmdSearchReceipt.TabIndex = 24
        Me.cmdSearchReceipt.TabStop = False
        '
        'cmdSearchCustomer
        '
        Me.cmdSearchCustomer.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchCustomer.Image = CType(resources.GetObject("cmdSearchCustomer.Image"), System.Drawing.Image)
        Me.cmdSearchCustomer.Location = New System.Drawing.Point(388, 21)
        Me.cmdSearchCustomer.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchCustomer.Name = "cmdSearchCustomer"
        Me.cmdSearchCustomer.Size = New System.Drawing.Size(42, 23)
        Me.cmdSearchCustomer.TabIndex = 16
        Me.cmdSearchCustomer.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdUndo)
        Me.GroupBox2.Controls.Add(Me.cmdSave)
        Me.GroupBox2.Controls.Add(Me.cmdEdit)
        Me.GroupBox2.Controls.Add(Me.cmdDelete)
        Me.GroupBox2.Controls.Add(Me.cmdAdd)
        Me.GroupBox2.Location = New System.Drawing.Point(13, 419)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Size = New System.Drawing.Size(515, 55)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        '
        'cmdUndo
        '
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(220, 17)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(74, 29)
        Me.cmdUndo.TabIndex = 13
        Me.cmdUndo.Text = "&Undo"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSave.Location = New System.Drawing.Point(321, 17)
        Me.cmdSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(74, 29)
        Me.cmdSave.TabIndex = 14
        Me.cmdSave.Text = "&Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(119, 17)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(74, 29)
        Me.cmdEdit.TabIndex = 12
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(422, 17)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(74, 29)
        Me.cmdDelete.TabIndex = 15
        Me.cmdDelete.Text = "&Delete"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Enabled = False
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(18, 17)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(74, 29)
        Me.cmdAdd.TabIndex = 11
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'BALANCETextBox
        '
        Me.BALANCETextBox.Location = New System.Drawing.Point(428, 391)
        Me.BALANCETextBox.Name = "BALANCETextBox"
        Me.BALANCETextBox.ReadOnly = True
        Me.BALANCETextBox.Size = New System.Drawing.Size(100, 22)
        Me.BALANCETextBox.TabIndex = 10
        Me.BALANCETextBox.Text = "0"
        Me.BALANCETextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtPaymentNo)
        Me.GroupBox3.Controls.Add(Me.lblPayNo)
        Me.GroupBox3.Controls.Add(Me.lblDescription)
        Me.GroupBox3.Controls.Add(Me.lblPayMethod)
        Me.GroupBox3.Controls.Add(Me.lblPayAmount)
        Me.GroupBox3.Controls.Add(Me.lblPayDate)
        Me.GroupBox3.Controls.Add(Me.PAYMENT_METHOD_IDComboBox)
        Me.GroupBox3.Controls.Add(Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator)
        Me.GroupBox3.Controls.Add(Me.PAYMENT_DATEDateTimePicker)
        Me.GroupBox3.Controls.Add(Me.PAYMENT_AMOUNTTextBox)
        Me.GroupBox3.Controls.Add(Me.DESCRIPTIONTextBox)
        Me.GroupBox3.Location = New System.Drawing.Point(13, 169)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(515, 218)
        Me.GroupBox3.TabIndex = 13
        Me.GroupBox3.TabStop = False
        '
        'txtPaymentNo
        '
        Me.txtPaymentNo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SP_RECEIVABLE_PAYMENT_DETAILBindingSource, "RECEIVABLE_PAYMENT_NUMBER", True))
        Me.txtPaymentNo.Location = New System.Drawing.Point(145, 56)
        Me.txtPaymentNo.Name = "txtPaymentNo"
        Me.txtPaymentNo.Size = New System.Drawing.Size(100, 22)
        Me.txtPaymentNo.TabIndex = 5
        '
        'SP_RECEIVABLE_PAYMENT_DETAILBindingSource
        '
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingSource.DataMember = "SP_RECEIVABLE_PAYMENT_DETAIL"
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingSource.DataSource = Me.DS_PAYMENT_TRANSACTION
        '
        'lblPayNo
        '
        Me.lblPayNo.Location = New System.Drawing.Point(15, 59)
        Me.lblPayNo.Name = "lblPayNo"
        Me.lblPayNo.Size = New System.Drawing.Size(124, 19)
        Me.lblPayNo.TabIndex = 30
        Me.lblPayNo.Text = "Payment No :"
        Me.lblPayNo.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblDescription
        '
        Me.lblDescription.Location = New System.Drawing.Point(27, 179)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(112, 15)
        Me.lblDescription.TabIndex = 23
        Me.lblDescription.Text = "Description :"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblPayMethod
        '
        Me.lblPayMethod.Location = New System.Drawing.Point(15, 148)
        Me.lblPayMethod.Name = "lblPayMethod"
        Me.lblPayMethod.Size = New System.Drawing.Size(124, 15)
        Me.lblPayMethod.TabIndex = 22
        Me.lblPayMethod.Text = "Payment Method :"
        Me.lblPayMethod.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblPayAmount
        '
        Me.lblPayAmount.Location = New System.Drawing.Point(12, 118)
        Me.lblPayAmount.Name = "lblPayAmount"
        Me.lblPayAmount.Size = New System.Drawing.Size(127, 15)
        Me.lblPayAmount.TabIndex = 21
        Me.lblPayAmount.Text = "Payment Amount :"
        Me.lblPayAmount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblPayDate
        '
        Me.lblPayDate.Location = New System.Drawing.Point(15, 89)
        Me.lblPayDate.Name = "lblPayDate"
        Me.lblPayDate.Size = New System.Drawing.Size(124, 15)
        Me.lblPayDate.TabIndex = 20
        Me.lblPayDate.Text = "Payment Date :"
        Me.lblPayDate.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'PAYMENT_METHOD_IDComboBox
        '
        Me.PAYMENT_METHOD_IDComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SP_RECEIVABLE_PAYMENT_DETAILBindingSource1, "PAYMENT_METHOD_ID", True))
        Me.PAYMENT_METHOD_IDComboBox.DataSource = Me.SPRECEIVABLEPAYMENTHEADERSPRECEIVABLEPAYMENTDETAILBindingSource
        Me.PAYMENT_METHOD_IDComboBox.DisplayMember = "PAYMENT_METHOD_ID"
        Me.PAYMENT_METHOD_IDComboBox.FormattingEnabled = True
        Me.PAYMENT_METHOD_IDComboBox.Location = New System.Drawing.Point(145, 145)
        Me.PAYMENT_METHOD_IDComboBox.Name = "PAYMENT_METHOD_IDComboBox"
        Me.PAYMENT_METHOD_IDComboBox.Size = New System.Drawing.Size(158, 23)
        Me.PAYMENT_METHOD_IDComboBox.TabIndex = 8
        Me.PAYMENT_METHOD_IDComboBox.ValueMember = "PAYMENT_METHOD_ID"
        '
        'SP_RECEIVABLE_PAYMENT_DETAILBindingSource1
        '
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingSource1.AllowNew = True
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingSource1.DataMember = "SP_RECEIVABLE_PAYMENT_HEADER_SP_RECEIVABLE_PAYMENT_DETAIL"
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingSource1.DataSource = Me.SP_RECEIVABLE_PAYMENT_HEADERBindingSource
        '
        'SPRECEIVABLEPAYMENTHEADERSPRECEIVABLEPAYMENTDETAILBindingSource
        '
        Me.SPRECEIVABLEPAYMENTHEADERSPRECEIVABLEPAYMENTDETAILBindingSource.DataMember = "SP_RECEIVABLE_PAYMENT_HEADER_SP_RECEIVABLE_PAYMENT_DETAIL"
        Me.SPRECEIVABLEPAYMENTHEADERSPRECEIVABLEPAYMENTDETAILBindingSource.DataSource = Me.SP_RECEIVABLE_PAYMENT_HEADERBindingSource
        '
        'SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator
        '
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.AddNewItem = Nothing
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.BindingSource = Me.SP_RECEIVABLE_PAYMENT_DETAILBindingSource
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.DeleteItem = Nothing
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.Location = New System.Drawing.Point(3, 18)
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.Name = "SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator"
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.Size = New System.Drawing.Size(509, 25)
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.TabIndex = 14
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'PAYMENT_DATEDateTimePicker
        '
        Me.PAYMENT_DATEDateTimePicker.CustomFormat = "dd MMM yyyy"
        Me.PAYMENT_DATEDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.SP_RECEIVABLE_PAYMENT_DETAILBindingSource, "PAYMENT_DATE", True))
        Me.PAYMENT_DATEDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.PAYMENT_DATEDateTimePicker.Location = New System.Drawing.Point(145, 85)
        Me.PAYMENT_DATEDateTimePicker.Name = "PAYMENT_DATEDateTimePicker"
        Me.PAYMENT_DATEDateTimePicker.Size = New System.Drawing.Size(130, 22)
        Me.PAYMENT_DATEDateTimePicker.TabIndex = 6
        '
        'PAYMENT_AMOUNTTextBox
        '
        Me.PAYMENT_AMOUNTTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SP_RECEIVABLE_PAYMENT_DETAILBindingSource, "PAYMENT_AMOUNT", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.PAYMENT_AMOUNTTextBox.Location = New System.Drawing.Point(145, 115)
        Me.PAYMENT_AMOUNTTextBox.Name = "PAYMENT_AMOUNTTextBox"
        Me.PAYMENT_AMOUNTTextBox.Size = New System.Drawing.Size(158, 22)
        Me.PAYMENT_AMOUNTTextBox.TabIndex = 7
        Me.PAYMENT_AMOUNTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'DESCRIPTIONTextBox
        '
        Me.DESCRIPTIONTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SP_RECEIVABLE_PAYMENT_DETAILBindingSource, "DESCRIPTION", True))
        Me.DESCRIPTIONTextBox.Location = New System.Drawing.Point(145, 176)
        Me.DESCRIPTIONTextBox.Name = "DESCRIPTIONTextBox"
        Me.DESCRIPTIONTextBox.Size = New System.Drawing.Size(245, 22)
        Me.DESCRIPTIONTextBox.TabIndex = 9
        '
        'SP_RECEIVABLE_PAYMENT_DETAILTableAdapter
        '
        Me.SP_RECEIVABLE_PAYMENT_DETAILTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.CUSTOMERTableAdapter = Nothing
        Me.TableAdapterManager.PAYABLE_PAYMENTTableAdapter = Nothing
        Me.TableAdapterManager.PAYABLETableAdapter = Nothing
        Me.TableAdapterManager.PAYMENT_METHODTableAdapter = Me.PAYMENT_METHODTableAdapter
        Me.TableAdapterManager.RECEIVABLE_PAYMENTTableAdapter = Me.RECEIVABLE_PAYMENTTableAdapter
        Me.TableAdapterManager.RECEIVABLETableAdapter = Nothing
        Me.TableAdapterManager.SALETableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'PAYMENT_METHODTableAdapter
        '
        Me.PAYMENT_METHODTableAdapter.ClearBeforeFill = True
        '
        'RECEIVABLE_PAYMENTTableAdapter
        '
        Me.RECEIVABLE_PAYMENTTableAdapter.ClearBeforeFill = True
        '
        'SP_RECEIVABLE_PAYMENT_HEADERTableAdapter
        '
        Me.SP_RECEIVABLE_PAYMENT_HEADERTableAdapter.ClearBeforeFill = True
        '
        'RECEIVABLE_PAYMENTBindingSource
        '
        Me.RECEIVABLE_PAYMENTBindingSource.DataMember = "RECEIVABLE_PAYMENT"
        Me.RECEIVABLE_PAYMENTBindingSource.DataSource = Me.DS_PAYMENT_TRANSACTION
        '
        'PAYMENT_METHODBindingSource
        '
        Me.PAYMENT_METHODBindingSource.DataMember = "PAYMENT_METHOD"
        Me.PAYMENT_METHODBindingSource.DataSource = Me.DS_PAYMENT_TRANSACTION
        '
        'lblBalance
        '
        Me.lblBalance.Location = New System.Drawing.Point(348, 394)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(60, 15)
        Me.lblBalance.TabIndex = 24
        Me.lblBalance.Text = "Balance :"
        Me.lblBalance.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'frmReceivablePayment
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(541, 483)
        Me.Controls.Add(Me.lblBalance)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.BALANCETextBox)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmReceivablePayment"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Receivable Payment"
        Me.Text = "Receivable Payment"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.SP_RECEIVABLE_PAYMENT_HEADERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_PAYMENT_TRANSACTION, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmdSearchReceipt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmdSearchCustomer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.SP_RECEIVABLE_PAYMENT_DETAILBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_RECEIVABLE_PAYMENT_DETAILBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SPRECEIVABLEPAYMENTHEADERSPRECEIVABLEPAYMENTDETAILBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.ResumeLayout(False)
        Me.SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator.PerformLayout()
        CType(Me.RECEIVABLE_PAYMENTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PAYMENT_METHODBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents BALANCETextBox As System.Windows.Forms.TextBox
    Friend WithEvents cmdSearchCustomer As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdSearchReceipt As System.Windows.Forms.PictureBox
    Friend WithEvents DS_PAYMENT_TRANSACTION As DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTION
    Friend WithEvents SP_RECEIVABLE_PAYMENT_DETAILBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_RECEIVABLE_PAYMENT_DETAILTableAdapter As DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.SP_RECEIVABLE_PAYMENT_DETAILTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.TableAdapterManager
    Friend WithEvents SP_RECEIVABLE_PAYMENT_DETAILBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PAYMENT_DATEDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents PAYMENT_AMOUNTTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DESCRIPTIONTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SP_RECEIVABLE_PAYMENT_HEADERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_RECEIVABLE_PAYMENT_HEADERTableAdapter As DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.SP_RECEIVABLE_PAYMENT_HEADERTableAdapter
    Friend WithEvents dtpMATURITY_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents TOTAL_RECEIVABLETextBox As System.Windows.Forms.TextBox
    Friend WithEvents RECEIVABLE_PAYMENTTableAdapter As DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.RECEIVABLE_PAYMENTTableAdapter
    Friend WithEvents RECEIVABLE_PAYMENTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PAYMENT_METHODTableAdapter As DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.PAYMENT_METHODTableAdapter
    Friend WithEvents PAYMENT_METHODBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_RECEIVABLE_PAYMENT_DETAILBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents PAYMENT_METHOD_IDComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents SPRECEIVABLEPAYMENTHEADERSPRECEIVABLEPAYMENTDETAILBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents lblReceiptNo As System.Windows.Forms.Label
    Friend WithEvents lblMarturity As System.Windows.Forms.Label
    Friend WithEvents lblGrandTotal As System.Windows.Forms.Label
    Friend WithEvents lblCustomer As System.Windows.Forms.Label
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblPayMethod As System.Windows.Forms.Label
    Friend WithEvents lblPayAmount As System.Windows.Forms.Label
    Friend WithEvents lblPayDate As System.Windows.Forms.Label
    Friend WithEvents lblBalance As System.Windows.Forms.Label
    Friend WithEvents lblPayNo As System.Windows.Forms.Label
    Friend WithEvents txtPaymentNo As System.Windows.Forms.TextBox
    Friend WithEvents RECEIPT_NOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CUSTOMER_NAMETextBox As System.Windows.Forms.TextBox
End Class
