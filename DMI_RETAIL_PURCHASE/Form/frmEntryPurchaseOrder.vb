﻿Public Class frmEntryPurchaseOrder
    Public tmpSaveMode, tmpNumber As String
    Public tmpVendor, tmpPurchaseOrderId, tmpTotalItem As Integer
    Dim tmpInput As String
    Dim tmpDataGridViewLoaded, tmpChange, tmpPrint As Boolean
    Public void As String
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    Private Sub VENDORBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.VENDORBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.DS_PURCHASE_ORDER)

    End Sub

    Private Sub frmEntryPurchaseOrder_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If tmpSaveMode = "Update" Then
            If e.KeyCode = Keys.F1 Then Exit Sub
            If e.KeyCode = Keys.F2 Then Exit Sub
            If e.KeyCode = Keys.F3 Then Exit Sub
            If e.KeyCode = Keys.F5 Then Exit Sub
            If e.KeyCode = Keys.F6 Then Exit Sub
            If e.KeyCode = Keys.F7 Then Exit Sub
            If e.KeyCode = Keys.F10 Then Exit Sub
        Else

            If e.KeyCode = Keys.F2 Then
                TAXPercentageTEXTBOX.Focus()
            ElseIf e.KeyCode = Keys.F5 Then
                'CUSTOMER_NAMEComboBox.Enabled = False
                tmpInput = "Browse"
                dgvPurchaseOrder.CancelEdit()
                mdlGeneral.tmpSearchMode = "PURCHASE - Product Code"
                frmSearchProduct.ShowDialog(Me)
                If tmpSearchResult = "" Then
                    tmpInput = ""
                    Exit Sub
                End If
                PRODUCTBindingSource.Filter = "PRODUCT_ID = " & tmpSearchResult
                dgvPurchaseOrder.Rows.Insert(dgvPurchaseOrder.RowCount - 1)
                dgvPurchaseOrder.CurrentCell = dgvPurchaseOrder.Rows(dgvPurchaseOrder.RowCount - 1).Cells(0)
                dgvPurchaseOrder.Item(0, dgvPurchaseOrder.RowCount - 1).Value = _
                    PRODUCTBindingSource.Current("PRODUCT_CODE")
                dgvPurchaseOrder.Item(0, dgvPurchaseOrder.RowCount - 1).Value = ""
                dgvPurchaseOrder.Item(7, dgvPurchaseOrder.RowCount - 2).Value = "Open"
            ElseIf e.KeyCode = Keys.F6 Then
                'CUSTOMER_NAMEComboBox.Enabled = False
                tmpInput = "Browse"
                dgvPurchaseOrder.CancelEdit()
                mdlGeneral.tmpSearchMode = "PURCHASE - Product Name"
                frmSearchProduct.ShowDialog(Me)
                If tmpSearchResult = "" Then
                    tmpInput = ""
                    Exit Sub
                End If
                PRODUCTBindingSource.Filter = "PRODUCT_ID = " & tmpSearchResult
                dgvPurchaseOrder.Rows.Insert(dgvPurchaseOrder.RowCount - 1)
                dgvPurchaseOrder.CurrentCell = dgvPurchaseOrder.Rows(dgvPurchaseOrder.RowCount - 1).Cells(0)
                dgvPurchaseOrder.Item(0, dgvPurchaseOrder.RowCount - 1).Value = _
                    PRODUCTBindingSource.Current("PRODUCT_CODE")
                dgvPurchaseOrder.Item(0, dgvPurchaseOrder.RowCount - 1).Value = ""
                dgvPurchaseOrder.Item(7, dgvPurchaseOrder.RowCount - 2).Value = "Open"
            ElseIf e.KeyCode = Keys.F7 Then
                If cmdAdd.Enabled = False Then Exit Sub
                cmdAdd_Click(Nothing, Nothing)
            ElseIf e.KeyCode = Keys.F10 Then
                If cmdSave.Enabled = False Then Exit Sub
                cmdSave_Click(Nothing, Nothing)
            End If

        End If

        If e.KeyCode = Keys.F12 Then
            If cmdPrint.Enabled = False Then Exit Sub
            'cmdPrint_Click(Nothing, Nothing)
        ElseIf e.KeyCode = Keys.F9 Then
            If cmdUndo.Enabled = False Then Exit Sub
            cmdUndo_Click(Nothing, Nothing)
        ElseIf e.KeyCode = Keys.F11 Then
            If cmdDelete.Enabled = False Then Exit Sub
            cmdDelete_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub frmEntryPurchaseOrder_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_PURCHASE_ORDER.PURCHASE_ORDER_DETAIL' table. You can move, or remove it, as needed.
        Me.PURCHASE_ORDER_DETAILTableAdapter.Fill(Me.DS_PURCHASE_ORDER.PURCHASE_ORDER_DETAIL)
        'TODO: This line of code loads data into the 'DS_PURCHASE_ORDER.PRODUCT' table. You can move, or remove it, as needed.
        Me.PRODUCTTableAdapter.Fill(Me.DS_PURCHASE_ORDER.PRODUCT)
        'TODO: This line of code loads data into the 'DS_PURCHASE_ORDER.PURCHASE_ORDER_HEADER' table. You can move, or remove it, as needed.
        Me.PURCHASE_ORDER_HEADERTableAdapter.Fill(Me.DS_PURCHASE_ORDER.PURCHASE_ORDER_HEADER)
        'TODO: This line of code loads data into the 'DS_PURCHASE_ORDER.VENDOR' table. You can move, or remove it, as needed.
        Me.VENDORTableAdapter.Fill(Me.DS_PURCHASE_ORDER.VENDOR)


        If tmpSaveMode = "Update" Then
            cmdEdit_Click(Nothing, Nothing)
            VENDORBindingSource.Position = VENDORBindingSource.Find("VENDOR_ID", tmpVendor)
            

            For X As Integer = 0 To tmpTotalItem - 1
                tmpNumber = txtPONo.Text
                Me.SP_DETAIL_PURCHASE_ORDERTableAdapter.Fill(DS_PURCHASE_ORDER.SP_DETAIL_PURCHASE_ORDER, tmpPurchaseOrderId)
                dgvPurchaseOrder.Rows.Add()
                SP_DETAIL_PURCHASE_ORDERBindingSource.Position = X
                dgvPurchaseOrder.Item("PRODUCT_CODE", X).Value = SP_DETAIL_PURCHASE_ORDERBindingSource.Current("PRODUCT_CODE")
                dgvPurchaseOrder.Item("PRODUCT_NAME", X).Value = SP_DETAIL_PURCHASE_ORDERBindingSource.Current("PRODUCT_NAME")
                dgvPurchaseOrder.Item("QUANTITY", X).Value = SP_DETAIL_PURCHASE_ORDERBindingSource.Current("QUANTITY")
                dgvPurchaseOrder.Item("PRICE_PER_UNIT", X).Value = SP_DETAIL_PURCHASE_ORDERBindingSource.Current("PRICE_PER_UNIT")
                dgvPurchaseOrder.Item("DISCOUNT", X).Value = SP_DETAIL_PURCHASE_ORDERBindingSource.Current("DISC_PERCENTAGE")
                dgvPurchaseOrder.Item("DISCOUNT2", X).Value = SP_DETAIL_PURCHASE_ORDERBindingSource.Current("DISC_AMOUNT")
                dgvPurchaseOrder.Item("TOTAL", X).Value = SP_DETAIL_PURCHASE_ORDERBindingSource.Current("TOTAL")
                dgvPurchaseOrder.Item("STATUS", X).Value = SP_DETAIL_PURCHASE_ORDERBindingSource.Current("STATUS")
                TAX_AMOUNTTextBox.Text = SP_DETAIL_PURCHASE_ORDERBindingSource.Current("TAX_AMOUNT")
                TAXPercentageTEXTBOX.Text = SP_DETAIL_PURCHASE_ORDERBindingSource.Current("TAX_PERCENTAGE")
            Next
        Else
            tmpSaveMode = "Insert"
            tmpDataGridViewLoaded = True
            cmdAdd.Enabled = False
            cmdEdit.Enabled = False
            cmdUndo.Enabled = False
            cmdDelete.Enabled = False
            tmpPrint = False
            txtShipTo.Text = ""
            cmdAdd_Click(Nothing, Nothing)
            dtpOrderDate.Value = Now
            dtpRequestDate.Value = Now
        End If

    End Sub

    Private Sub dgvPurchaseOrder_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPurchaseOrder.CellContentClick
        If dgvPurchaseOrder.Item(0, dgvPurchaseOrder.CurrentCell.RowIndex).Value = "" Then Exit Sub

        If dgvPurchaseOrder.CurrentCell.ColumnIndex = 8 Then
            dgvPurchaseOrder.Rows.RemoveAt(dgvPurchaseOrder.CurrentCell.RowIndex)
            CalculationAll(dgvPurchaseOrder, _
                                     5, _
                                     6, _
                                     AMOUNT_DISCOUNTTextBox, _
                                     TAXPercentageTEXTBOX, _
                                     TAX_AMOUNTTextBox, _
                                     GRAND_TOTALTextBox, _
                                     GRAND_TOTALTextBox)
            lblNoOfItem.Text = "No of Item(s) : " & dgvPurchaseOrder.RowCount - 1


        End If

        If dgvPurchaseOrder.Item("PRODUCT_CODE", 0).Value = "" Then
            TAXPercentageTEXTBOX.Text = "0"
            TAX_AMOUNTTextBox.Text = "0"
        End If
    End Sub

    Private Sub dgvPurchaseOrder_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPurchaseOrder.CellValueChanged
        If tmpSaveMode = "Update" Then Exit Sub

        If tmpDataGridViewLoaded Then

            If dgvPurchaseOrder.CurrentCell.ColumnIndex = 2 Or dgvPurchaseOrder.CurrentCell.ColumnIndex = 3 Or dgvPurchaseOrder.CurrentCell.ColumnIndex = 4 Or dgvPurchaseOrder.CurrentCell.ColumnIndex = 5 Then
                If Not IsNumeric(dgvPurchaseOrder.CurrentCell.Value) Then
                    If Language = "Indonesian" Then
                        MsgBox("Anda memasukan nilai yang salah !" & vbCrLf & _
                          "Untuk Kolom : " & _
                           dgvPurchaseOrder.Columns(dgvPurchaseOrder.CurrentCell.ColumnIndex).HeaderText & vbCrLf & _
                           "Baris Nomor : " & _
                           dgvPurchaseOrder.CurrentCell.RowIndex + 1, MsgBoxStyle.Critical, "DMI Retail")
                        'give value "0" to empty fields or unexpected fields
                        dgvPurchaseOrder.CurrentCell.Value = 0
                    Else
                        MsgBox("You entered unexpected value !" & vbCrLf & _
                          "For Column : " & _
                           dgvPurchaseOrder.Columns(dgvPurchaseOrder.CurrentCell.ColumnIndex).HeaderText & vbCrLf & _
                           "Row number : " & _
                           dgvPurchaseOrder.CurrentCell.RowIndex + 1, MsgBoxStyle.Critical, "DMI Retail")
                        'give value "0" to empty fields or unexpected fields
                        dgvPurchaseOrder.CurrentCell.Value = 0
                    End If
                    Exit Sub
                End If
            End If

            If tmpFillingDGV = False Then
                If dgvPurchaseOrder.CurrentCell.ColumnIndex = 0 Then
                    Dim tmpProductName As String = ""
                    Dim tmpUnitPrice As Integer = 0

                    PRODUCTTableAdapter.SP_GET_PRODUCT_NAME(dgvPurchaseOrder.CurrentCell.Value, tmpProductName)
                    PRODUCTTableAdapter.SP_GET_SELLING_PRICE(dgvPurchaseOrder.CurrentCell.Value, tmpUnitPrice)

                    If tmpProductName = "" Then
                        If dgvPurchaseOrder.CurrentCell.Value <> "" Then
                            If Language = "Indonesian" Then
                                MsgBox("Produk tidak ditemukan", MsgBoxStyle.Critical, "DMI Retail")
                                dgvPurchaseOrder.Rows.RemoveAt(dgvPurchaseOrder.CurrentCell.RowIndex)
                            Else
                                MsgBox("Product not found!", MsgBoxStyle.Critical, "DMI Retail")
                                dgvPurchaseOrder.Rows.RemoveAt(dgvPurchaseOrder.CurrentCell.RowIndex)
                            End If
                        End If
                        Exit Sub
                    End If

                    tmpFillingDGV = True
                    AddDataToGrid(dgvPurchaseOrder, _
                                  dgvPurchaseOrder.CurrentCell.Value, _
                                  0, _
                                  tmpProductName, _
                                  1, _
                                  2, _
                                  tmpUnitPrice, _
                                  3, _
                                  4, _
                                  5, _
                                  6)
                    CalculationAll(dgvPurchaseOrder, _
                                     5, _
                                     6, _
                                     AMOUNT_DISCOUNTTextBox, _
                                     TAXPercentageTEXTBOX, _
                                     TAX_AMOUNTTextBox, _
                                     GRAND_TOTALTextBox, _
                                     GRAND_TOTALTextBox)
                    lblNoOfItem.Text = "No of Item(s) : " & dgvPurchaseOrder.RowCount - 1

                    ''Calculate Tax Amount when adding Product
                    Dim tmpGrandTotal As Integer = 0
                    For x As Integer = 0 To dgvPurchaseOrder.RowCount - 1
                        tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvPurchaseOrder.Item(6, x).Value)
                    Next
                    TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
                    TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)


                ElseIf dgvPurchaseOrder.CurrentCell.ColumnIndex = 4 Then

                    If dgvPurchaseOrder.Item(0, dgvPurchaseOrder.CurrentCell.RowIndex).Value = "" Then
                        dgvPurchaseOrder.Rows.RemoveAt(dgvPurchaseOrder.CurrentCell.RowIndex)
                        Exit Sub
                    End If

                    dgvPurchaseOrder.Item(5, dgvPurchaseOrder.CurrentCell.RowIndex).Value = _
                        CInt(dgvPurchaseOrder.Item(2, dgvPurchaseOrder.CurrentCell.RowIndex).Value) * _
                        CInt(dgvPurchaseOrder.Item(3, dgvPurchaseOrder.CurrentCell.RowIndex).Value) * _
                        CInt(dgvPurchaseOrder.Item(4, dgvPurchaseOrder.CurrentCell.RowIndex).Value) / 100

                    Calculation(dgvPurchaseOrder.Item(2, dgvPurchaseOrder.CurrentCell.RowIndex).Value, _
                                dgvPurchaseOrder.Item(3, dgvPurchaseOrder.CurrentCell.RowIndex).Value, _
                                dgvPurchaseOrder.Item(5, dgvPurchaseOrder.CurrentCell.RowIndex).Value, _
                                dgvPurchaseOrder.Item(6, dgvPurchaseOrder.CurrentCell.RowIndex).Value)
                    CalculationAll(dgvPurchaseOrder, _
                                     5, _
                                     6, _
                                     AMOUNT_DISCOUNTTextBox, _
                                     TAXPercentageTEXTBOX, _
                                     TAX_AMOUNTTextBox, _
                                     GRAND_TOTALTextBox, _
                                     GRAND_TOTALTextBox)
                    lblNoOfItem.Text = "No of Item(s) : " & dgvPurchaseOrder.RowCount - 1

                    ''Calculate Tax Amount when adding Product
                    Dim tmpGrandTotal As Integer = 0
                    For x As Integer = 0 To dgvPurchaseOrder.RowCount - 1
                        tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvPurchaseOrder.Item(6, x).Value)
                    Next
                    TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
                    TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)

                    If CInt(dgvPurchaseOrder.Item(4, dgvPurchaseOrder.CurrentCell.RowIndex).Value) > 100 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih besar dari 100%" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not greater than 100%" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvPurchaseOrder.Item(4, dgvPurchaseOrder.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                    If CInt(dgvPurchaseOrder.Item(4, dgvPurchaseOrder.CurrentCell.RowIndex).Value) < 0 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih kecil dari 0" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not smaller than 0" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvPurchaseOrder.Item(4, dgvPurchaseOrder.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                ElseIf dgvPurchaseOrder.CurrentCell.ColumnIndex = 5 Then

                    If dgvPurchaseOrder.Item(0, dgvPurchaseOrder.CurrentCell.RowIndex).Value = "" Then
                        dgvPurchaseOrder.Rows.Remove(dgvPurchaseOrder.CurrentRow)
                        Exit Sub
                    End If

                    dgvPurchaseOrder.Item(4, dgvPurchaseOrder.CurrentCell.RowIndex).Value = _
                        FormatNumber(CInt(dgvPurchaseOrder.Item(5, dgvPurchaseOrder.CurrentCell.RowIndex).Value) / _
                        (CInt(dgvPurchaseOrder.Item(2, dgvPurchaseOrder.CurrentCell.RowIndex).Value) * _
                        CInt(dgvPurchaseOrder.Item(3, dgvPurchaseOrder.CurrentCell.RowIndex).Value)) * 100, 2)

                    Calculation(dgvPurchaseOrder.Item(2, dgvPurchaseOrder.CurrentCell.RowIndex).Value, _
                                dgvPurchaseOrder.Item(3, dgvPurchaseOrder.CurrentCell.RowIndex).Value, _
                                dgvPurchaseOrder.Item(5, dgvPurchaseOrder.CurrentCell.RowIndex).Value, _
                                dgvPurchaseOrder.Item(6, dgvPurchaseOrder.CurrentCell.RowIndex).Value)
                    CalculationAll(dgvPurchaseOrder, _
                                     5, _
                                     6, _
                                     AMOUNT_DISCOUNTTextBox, _
                                     TAXPercentageTEXTBOX, _
                                     TAX_AMOUNTTextBox, _
                                     GRAND_TOTALTextBox, _
                                     GRAND_TOTALTextBox)
                    lblNoOfItem.Text = "No of Item(s) : " & dgvPurchaseOrder.RowCount - 1

                    ''Calculate Tax Amount when adding Product
                    Dim tmpGrandTotal As Integer = 0
                    For x As Integer = 0 To dgvPurchaseOrder.RowCount - 1
                        tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvPurchaseOrder.Item(6, x).Value)
                    Next
                    TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
                    TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)

                    If CInt(dgvPurchaseOrder.Item("DISCOUNT", dgvPurchaseOrder.CurrentCell.RowIndex).Value) > 100 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih besar dari 100%" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not greater than 100%" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvPurchaseOrder.Item("DISCOUNT", dgvPurchaseOrder.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                    If CInt(dgvPurchaseOrder.Item(5, dgvPurchaseOrder.CurrentCell.RowIndex).Value) < 0 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih kecil dari 0" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not smaller than 0" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvPurchaseOrder.Item(5, dgvPurchaseOrder.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                ElseIf dgvPurchaseOrder.CurrentCell.ColumnIndex = 7 Then

                    If dgvPurchaseOrder.Item(0, dgvPurchaseOrder.CurrentCell.RowIndex).Value = "" Then
                        dgvPurchaseOrder.Rows.Remove(dgvPurchaseOrder.CurrentRow)
                        Exit Sub
                    End If


                ElseIf dgvPurchaseOrder.CurrentCell.ColumnIndex = 2 Or _
                    dgvPurchaseOrder.CurrentCell.ColumnIndex = 3 Or _
                    dgvPurchaseOrder.CurrentCell.ColumnIndex = 4 Or _
                    dgvPurchaseOrder.CurrentCell.ColumnIndex = 5 Or
                    dgvPurchaseOrder.CurrentCell.ColumnIndex = 7 Then

                    If dgvPurchaseOrder.Item(0, dgvPurchaseOrder.CurrentCell.RowIndex).Value = "" Then
                        dgvPurchaseOrder.Rows.Remove(dgvPurchaseOrder.CurrentRow)
                        Exit Sub
                    End If


                    dgvPurchaseOrder.Item(5, dgvPurchaseOrder.CurrentCell.RowIndex).Value = _
                        CInt(dgvPurchaseOrder.Item(2, dgvPurchaseOrder.CurrentCell.RowIndex).Value) * _
                        CInt(dgvPurchaseOrder.Item(3, dgvPurchaseOrder.CurrentCell.RowIndex).Value) * _
                        CInt(dgvPurchaseOrder.Item(4, dgvPurchaseOrder.CurrentCell.RowIndex).Value) / 100

                    Calculation(dgvPurchaseOrder.Item(2, dgvPurchaseOrder.CurrentCell.RowIndex).Value, _
                                dgvPurchaseOrder.Item(3, dgvPurchaseOrder.CurrentCell.RowIndex).Value, _
                                dgvPurchaseOrder.Item(5, dgvPurchaseOrder.CurrentCell.RowIndex).Value, _
                                dgvPurchaseOrder.Item(6, dgvPurchaseOrder.CurrentCell.RowIndex).Value)
                    CalculationAll(dgvPurchaseOrder, _
                                     5, _
                                     6, _
                                     AMOUNT_DISCOUNTTextBox, _
                                     TAXPercentageTEXTBOX, _
                                     TAX_AMOUNTTextBox, _
                                     GRAND_TOTALTextBox, _
                                     GRAND_TOTALTextBox)
                    lblNoOfItem.Text = "No of Item(s) : " & dgvPurchaseOrder.RowCount - 1

                    ''Calculate Tax Amount when adding Product
                    Dim tmpGrandTotal As Integer = 0
                    For x As Integer = 0 To dgvPurchaseOrder.RowCount - 1
                        tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvPurchaseOrder.Item(6, x).Value)
                    Next
                    TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
                    TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)

                    If CInt(dgvPurchaseOrder.Item(4, dgvPurchaseOrder.CurrentCell.RowIndex).Value) > 100 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih besar dari 100%" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not greater than 100%" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvPurchaseOrder.Item(4, dgvPurchaseOrder.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                    If CInt(dgvPurchaseOrder.Item(4, dgvPurchaseOrder.CurrentCell.RowIndex).Value) < 0 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih kecil dari 0" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not smaller than 0" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvPurchaseOrder.Item(4, dgvPurchaseOrder.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                End If
            End If
        End If
    End Sub

    Private Sub TAX_AMOUNTTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TAX_AMOUNTTextBox.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""
    End Sub

    Private Sub TAX_AMOUNTTextBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TAX_AMOUNTTextBox.LostFocus
        TAX_AMOUNTTextBox.Text = IIf(TAX_AMOUNTTextBox.Text = "", "0", TAX_AMOUNTTextBox.Text)
        Dim tmpGrandTotal As Integer = 0
        For x As Integer = 0 To dgvPurchaseOrder.RowCount - 1
            tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvPurchaseOrder.Item(6, x).Value)
        Next
        If TAX_AMOUNTTextBox.Text = "0" Then
            TAXPercentageTEXTBOX.Text = "0.00"
        Else
            TAXPercentageTEXTBOX.Text = FormatNumber((TAX_AMOUNTTextBox.Text / tmpGrandTotal) * 100)
        End If
    End Sub

    Private Sub TAX_AMOUNTTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TAX_AMOUNTTextBox.TextChanged
        If GRAND_TOTALTextBox.Text = "0" Then
            TAX_AMOUNTTextBox.Text = "0"
        Else
            CalculationAll(dgvPurchaseOrder, _
                         5, _
                         6, _
                         AMOUNT_DISCOUNTTextBox, _
                         TAXPercentageTEXTBOX, _
                         TAX_AMOUNTTextBox, _
                         GRAND_TOTALTextBox, _
                         GRAND_TOTALTextBox)
            lblNoOfItem.Text = "No of Item(s) : " & dgvPurchaseOrder.RowCount - 1
        End If
        tmpChange = True
    End Sub

    Private Sub TAXPercentageTEXTBOX_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TAXPercentageTEXTBOX.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""
    End Sub

    Private Sub TAXPercentageTEXTBOX_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TAXPercentageTEXTBOX.LostFocus
        If GRAND_TOTALTextBox.Text <> "0" Then
            Dim tmpGrandTotal As Integer = 0
            For x As Integer = 0 To dgvPurchaseOrder.RowCount - 1
                tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvPurchaseOrder.Item(6, x).Value)
            Next
            TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
            TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)
        Else
            TAXPercentageTEXTBOX.Text = "0.00"
        End If
    End Sub

    Private Sub TAXPercentageTEXTBOX_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TAXPercentageTEXTBOX.TextChanged
        If GRAND_TOTALTextBox.Text = "0" Then
            If TAX_AMOUNTTextBox.Text = "0" Then
                TAXPercentageTEXTBOX.Text = "0.00"
            End If
        End If
        tmpChange = True
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        PURCHASE_ORDER_HEADERTableAdapter.SP_GENERATE_PURCHASE_ORDER_NO(txtPONo.Text)
        VENDOR_NAMEComboBox.SelectedIndex = -1
        dgvPurchaseOrder.Rows.Clear()
        lblNoOfItem.Text = "No of Item(s) :  0"

        AMOUNT_DISCOUNTTextBox.Text = "0"
        TAXPercentageTEXTBOX.Text = "0.00"
        TAX_AMOUNTTextBox.Text = "0"
        GRAND_TOTALTextBox.Text = "0"
        VENDOR_NAMEComboBox.Enabled = True
        dgvPurchaseOrder.Focus()
        cmdAdd.Enabled = False
        cmdUndo.Enabled = False
        txtShipTo.Text = ""
        tmpChange = False
    End Sub

    'Private Sub cmdPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPrint.Click
    '    If tmpSaveMode = "Insert" Then
    '        cmdSave_Click(Nothing, Nothing)
    '        If tmpPrint = False Then
    '            If Language = "Indonesian" Then
    '                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
    '            Else
    '                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
    '            End If
    '            Exit Sub
    '        End If

    '        Dim objf As New frmRepPurchaseOrder
    '        objf.tmpnumber = tmpNumber
    '        objf.ReportViewer1.ShowRefreshButton = False
    '        objf.ReportViewer1.ZoomPercent = 100
    '        objf.WindowState = FormWindowState.Maximized
    '        objf.Show()
    '    ElseIf tmpSaveMode = "Update" Then
    '        Dim objf As New frmRepPurchaseOrder
    '        objf.tmpnumber = tmpNumber
    '        objf.ReportViewer1.ShowRefreshButton = False
    '        objf.ReportViewer1.ZoomPercent = 100
    '        objf.WindowState = FormWindowState.Maximized
    '        objf.Show()
    '    End If
    'End Sub

    Private Sub cmdUndo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        Me.Close()
    End Sub

    Private Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Dim tmpCheck As String = ""
        tmpNumber = txtPONo.Text

        PURCHASE_ORDER_HEADERTableAdapter.SP_CHECK_CLOSING_PERIOD(dtpOrderDate.Value, tmpCheck)
        If tmpCheck = "TRUE" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menghapus transaksi ini !" & vbCrLf & "Transaksi ini telah masuk dalam proses Tutup Buku", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Delete this Transaction!" & vbCrLf & "This Transaction has been in Closed Period", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If Not ValidateComboBox(VENDOR_NAMEComboBox) Then
            If Language = "Indonesian" Then
                MsgBox("Nilai yang anda masukan tidak terdapat dalam daftar.", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You input value which is not in the list.", MsgBoxStyle.Critical, "DMI Retail")
            End If
            VENDOR_NAMEComboBox.Focus()
            Exit Sub
        End If

        If txtPONo.Text = "" Or txtShipTo.Text = "" Or VENDOR_NAMEComboBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        For x As Integer = 0 To dgvPurchaseOrder.RowCount - 2
            If dgvPurchaseOrder.Item("STATUS", x).Value.ToString = "" Or dgvPurchaseOrder.Item("QUANTITY", x).Value.ToString = "" Or dgvPurchaseOrder.Item("PRICE_PER_UNIT", x).Value.ToString = "" Then
                If Language = "Indonesian" Then
                    MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If

            If CInt(dgvPurchaseOrder.Item("PRICE_PER_UNIT", x).Value) = 0 Or CInt(dgvPurchaseOrder.Item("QUANTITY", x).Value) = 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If
        Next

        If tmpSaveMode = "Insert" Then
            Dim tmpPurchaseOrderNo As Integer
            PURCHASE_ORDER_HEADERTableAdapter.SP_CHECK_PURCHASE_ORDER_NO(txtPONo.Text, tmpPurchaseOrderNo)
            If tmpPurchaseOrderNo > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("No Transaksi telah digunakan oleh user lain." & vbCrLf & _
                                               "Nomor transaksi baru akan digunakan untuk transaksi ini.", _
                                               MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Receipt Number is used by another User." & vbCrLf & _
                                                        "New Receipt Number will be assigned to this transaction.", _
                                                        MsgBoxStyle.Critical, "DMI Retail")
                End If
            End If

            While tmpPurchaseOrderNo > 0
                PURCHASE_ORDER_HEADERTableAdapter.SP_GENERATE_PURCHASE_ORDER_NO(txtPONo.Text)
                PURCHASE_ORDER_HEADERTableAdapter.SP_CHECK_PURCHASE_ORDER_NO(txtPONo.Text, tmpPurchaseOrderNo)
            End While

            PURCHASE_ORDER_HEADERTableAdapter.SP_PURCHASE_ORDER_HEADER("I", _
                                                                         0, _
                                                                         txtPONo.Text, _
                                                                         dtpOrderDate.Value, _
                                                                         dtpRequestDate.Value, _
                                                                         VENDORBindingSource.Current("VENDOR_ID"), _
                                                                         txtShipTo.Text, _
                                                                         USER_ID, _
                                                                         Now, _
                                                                         0, _
                                                                         DateSerial(4000, 12, 31), _
                                                                         CInt(GRAND_TOTALTextBox.Text))

            For I As Integer = 0 To dgvPurchaseOrder.RowCount - 2
                Dim tmpProductID As Integer
                Dim tmpStatusPurchase, tmpStatusDelivery As String
                If dgvPurchaseOrder.Item("STATUS", I).Value = "Open" Then
                    tmpStatusPurchase = "Open"
                    tmpStatusDelivery = "Open"
                Else
                    tmpStatusPurchase = "Close"
                    tmpStatusDelivery = "Close"
                End If
                PRODUCTTableAdapter.SP_GET_PRODUCT_ID(dgvPurchaseOrder.Item("PRODUCT_CODE", I).Value, tmpProductID)

                PURCHASE_ORDER_HEADERTableAdapter.SP_PURCHASE_ORDER_DETAIL("I", _
                                                                             txtPONo.Text, _
                                                                             0, _
                                                                             tmpProductID, _
                                                                             CInt(dgvPurchaseOrder.Item("QUANTITY", I).Value), _
                                                                             0, _
                                                                             CInt(dgvPurchaseOrder.Item("PRICE_PER_UNIT", I).Value), _
                                                                             CInt(dgvPurchaseOrder.Item("DISCOUNT", I).Value), _
                                                                             CInt(dgvPurchaseOrder.Item("DISCOUNT2", I).Value), _
                                                                             CInt(TAXPercentageTEXTBOX.Text), _
                                                                             CInt(TAX_AMOUNTTextBox.Text), _
                                                                             CInt(dgvPurchaseOrder.Item("TOTAL", I).Value), _
                                                                             tmpStatusDelivery, _
                                                                             dgvPurchaseOrder.Item("STATUS", I).Value, _
                                                                             tmpStatusPurchase, _
                                                                             USER_ID, _
                                                                             Now, _
                                                                             0, _
                                                                             DateSerial(4000, 12, 31))

            Next

            If Language = "Indonesian" Then
                MsgBox("Data Berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
            Else
                MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
            End If
            cmdAdd_Click(Nothing, Nothing)
            tmpChange = True
            tmpPrint = True

        ElseIf tmpSaveMode = "Update" Then
            For I As Integer = 0 To dgvPurchaseOrder.RowCount - 2
                Dim tmpProductID As Integer
                Dim tmpStatusPurchase, tmpStatusDelivery As String
                If dgvPurchaseOrder.Item("STATUS", I).Value = "Open" Then
                    tmpStatusPurchase = "Open"
                    tmpStatusDelivery = "Open"
                Else
                    tmpStatusPurchase = "Close"
                    tmpStatusDelivery = "Close"
                End If
                PRODUCTTableAdapter.SP_GET_PRODUCT_ID(dgvPurchaseOrder.Item("PRODUCT_CODE", I).Value, tmpProductID)

                PURCHASE_ORDER_HEADERTableAdapter.SP_PURCHASE_ORDER_DETAIL("U", _
                                                                             txtPONo.Text, _
                                                                             SP_DETAIL_PURCHASE_ORDERBindingSource.Current("PURCHASE_ORDER_ID"), _
                                                                             tmpProductID, _
                                                                             CInt(dgvPurchaseOrder.Item("QUANTITY", I).Value), _
                                                                             0, _
                                                                             CInt(dgvPurchaseOrder.Item("PRICE_PER_UNIT", I).Value), _
                                                                             CInt(dgvPurchaseOrder.Item("DISCOUNT", I).Value), _
                                                                             CInt(dgvPurchaseOrder.Item("DISCOUNT2", I).Value), _
                                                                             CInt(TAXPercentageTEXTBOX.Text), _
                                                                             CInt(TAX_AMOUNTTextBox.Text), _
                                                                             CInt(dgvPurchaseOrder.Item("TOTAL", I).Value), _
                                                                             tmpStatusDelivery, _
                                                                             dgvPurchaseOrder.Item("STATUS", I).Value, _
                                                                             tmpStatusPurchase, _
                                                                             USER_ID, _
                                                                             Now, _
                                                                             0, _
                                                                             DateSerial(4000, 12, 31))

            Next

            If Language = "Indonesian" Then
                MsgBox("Data Berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
            Else
                MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
            End If
            Me.Close()
        End If

    End Sub

    Private Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim tmpCheck As String = ""

        PURCHASE_ORDER_HEADERTableAdapter.SP_CHECK_CLOSING_PERIOD(dtpOrderDate.Value, tmpCheck)
        If tmpCheck = "TRUE" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menghapus transaksi ini !" & vbCrLf & "Transaksi ini telah masuk dalam proses Tutup Buku", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Delete this Transaction!" & vbCrLf & "This Transaction has been in Closed Period", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If Language = "Indonesian" Then
            If MsgBox("Batalkan transaksi ini?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = _
                MsgBoxResult.No Then Exit Sub
        Else
            If MsgBox("Void this transaction?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = _
               MsgBoxResult.No Then Exit Sub
        End If

        PURCHASE_ORDER_HEADERTableAdapter.SP_PURCHASE_ORDER_HEADER("V", _
                                                                    PURCHASE_ORDER_HEADERBindingSource.Current("PURCHASE_ORDER_ID"), _
                                                                    txtPONo.Text, _
                                                                    dtpOrderDate.Value, _
                                                                    dtpRequestDate.Value, _
                                                                    VENDORBindingSource.Current("VENDOR_ID"), _
                                                                    txtShipTo.Text, _
                                                                    USER_ID, _
                                                                    Now, _
                                                                    USER_ID, _
                                                                    Now, _
                                                                    CInt(GRAND_TOTALTextBox.Text))

        If Language = "Indonesian" Then
            MsgBox("Data Berhasil dihapus.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully deleted.", MsgBoxStyle.Information, "DMI Retail")
        End If
        Me.Close()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        tmpSaveMode = "Update"
        dgvPurchaseOrder.Columns("PRODUCT_CODE").ReadOnly = True
        dgvPurchaseOrder.Columns("PRODUCT_NAME").ReadOnly = True
        dgvPurchaseOrder.Columns("QUANTITY").ReadOnly = True
        dgvPurchaseOrder.Columns("PRICE_PER_UNIT").ReadOnly = True
        dgvPurchaseOrder.Columns("DISCOUNT").ReadOnly = True
        dgvPurchaseOrder.Columns("DISCOUNT2").ReadOnly = True
        dgvPurchaseOrder.Columns("TOTAL").ReadOnly = True
        dgvPurchaseOrder.Columns("DELETE").Visible = False
        DisableInputBox(Me)
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = False
    End Sub

End Class