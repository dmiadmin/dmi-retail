﻿Public Class frmRepProductMovement
    Public date1 As Date
    Public date2 As Date
    Public tmpProdIdMov As Integer
    Private Sub frmRepProductMovement_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DB_DMI_RETAILDataSetALL.COMPANY_INFORMATION' table. You can move, or remove it, as needed.
        Me.COMPANY_INFORMATIONTableAdapter.Fill(Me.DB_DMI_RETAILDataSetALL.COMPANY_INFORMATION)
        'TODO: This line of code loads data into the 'DB_DMI_RETAILDataSetALL.SP_LIST_PRODUCT_MOVEMENT' table. You can move, or remove it, as needed.
        Me.SP_LIST_PRODUCT_MOVEMENTTableAdapter.Fill(Me.DB_DMI_RETAILDataSetALL.SP_LIST_PRODUCT_MOVEMENT, tmpProdIdMov, _
                                                     date1, date2)

        Me.ReportViewer1.RefreshReport()

        Dim newPageSettings As New System.Drawing.Printing.PageSettings
        newPageSettings.Margins = New System.Drawing.Printing.Margins(50, 50, 35, 35)
        newPageSettings.PaperSize = New System.Drawing.Printing.PaperSize("A4", 827, 1169)
        Me.ReportViewer1.SetPageSettings(newPageSettings)

        If Not tmpReport Is Nothing Then tmpReport.Close()
        tmpReport = Me
    End Sub
End Class