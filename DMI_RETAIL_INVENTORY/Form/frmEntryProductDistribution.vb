﻿Public Class frmEntryProductDistribution
    Dim tmpWarehouseDestinationID As Integer
    Dim tmpProductID, tmpCategory As Integer
    Dim tmpChange As Boolean
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    Private Sub frmEntryProductDistribution_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
     
        If tmpChange = True And PRODUCT_CODEComboBox.Enabled = True Then
            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah Anda ingin menyimpan perubahan " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If
            If tmpvar = 6 Then
                cmdMode_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub frmEntryProductDistribution_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_PRODUCT.SP_LIST_PRODUCT_GOODS' table. You can move, or remove it, as needed.
        Me.SP_LIST_PRODUCT_GOODSTableAdapter.Fill(Me.DS_PRODUCT.SP_LIST_PRODUCT_GOODS, Now)
        'TODO: This line of code loads data into the 'DS_WAREHOUSE.WAREHOUSE1' table. You can move, or remove it, as needed.
        Me.WAREHOUSE1TableAdapter.Fill(Me.DS_WAREHOUSE.WAREHOUSE1)
        'TODO: This line of code loads data into the 'DS_PRODUCT_WAREHOUSE.PRODUCT_WAREHOUSE' table. You can move, or remove it, as needed.
        Me.PRODUCT_WAREHOUSETableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.PRODUCT_WAREHOUSE)
        'TODO: This line of code loads data into the 'DS_PRODUCT.PRODUCT' table. You can move, or remove it, as needed.
        Me.PRODUCTTableAdapter.Fill(Me.DS_PRODUCT.PRODUCT)
        'TODO: This line of code loads data into the 'DS_WAREHOUSE.WAREHOUSE' table. You can move, or remove it, as needed.
        Me.WAREHOUSETableAdapter.Fill(Me.DS_WAREHOUSE.WAREHOUSE)


        If WAREHOUSEBindingSource1 Is Nothing Or WAREHOUSEBindingSource1.Count < 1 Then
            If Language = "Indonesian" Then
                MsgBox("Tidak terdapat data gudang." & vbCrLf & "Silahkan masukan satu data gudang!", _
                      MsgBoxStyle.Critical, "DMI Retail")
                else
                MsgBox("There is no Warehouse data." & vbCrLf & "Please enter at least one Warehouse data!", _
                     MsgBoxStyle.Critical, "DMI Retail")
            End If
        Else
            If PRODUCT_DISTRIBUTIONTableAdapter.SP_SELECT_PARAMETER("PD WAREHOUSE SOURCE") = "0" Then
                WAREHOUSE_NAMEComboBox.SelectedIndex = -1
            Else
                WAREHOUSEBindingSource1.Position = _
                    WAREHOUSEBindingSource1.Find("WAREHOUSE_ID", _
                                                 PRODUCT_DISTRIBUTIONTableAdapter.SP_SELECT_PARAMETER("PD WAREHOUSE SOURCE"))
            End If
        End If

        If WAREHOUSE1BindingSource1 Is Nothing Or WAREHOUSE1BindingSource1.Count < 1 Then
            If Language = "Indonesian" Then
                MsgBox("Tidak terdapat data gudang." & vbCrLf & "Silahkan masukan satu data gudang!", _
                      MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Warehouse data." & vbCrLf & "Please enter at least one Warehouse data!", _
                     MsgBoxStyle.Critical, "DMI Retail")
            End If
        Else
            If PRODUCT_DISTRIBUTIONTableAdapter.SP_SELECT_PARAMETER("PD WAREHOUSE DESTINATION") = "0" Then
                WAREHOUSE_NAMEComboBox1.SelectedIndex = -1
            Else
                WAREHOUSE1BindingSource1.Position = _
                    WAREHOUSE1BindingSource1.Find("WAREHOUSE_ID", _
                                                  PRODUCT_DISTRIBUTIONTableAdapter.SP_SELECT_PARAMETER("PD WAREHOUSE DESTINATION"))
            End If
        End If

        txtDate.Text = Format(Now, "dd-MMM-yyyy")
        PRODUCT_DISTRIBUTIONTableAdapter.SP_PRODUCT_DISTRIBUTION_GENERATE_NO(PRODUCT_DISTRIBUTION_NUMBERTextBox.Text)
        PRODUCT_CODEComboBox.Text = ""
        txtQuantity.Text = "0"
        txtSourceQty.Text = ""

        PRODUCT_CODEComboBox.Focus()

        tmpChange = False

        If Language = "Indonesian" Then
            Me.Text = "Input Distribusi Produk"
            lblDate.Text = "Tanggal"
            lblProduct.Text = "Produk"
            lblNumber.Text = "Nomor"
            lblSource.Text = "Awal"
            lblDestination.Text = "Tujuan"
            lblQuantity.Text = "Jumlah"
            lblQuantity1.Text = "Jumlah"
            cmdUndo.Text = "Batal"
            cmdMode.Text = "Pindah"
            cmdAdd.Text = "Tambah"
            cmdDelete.Text = "Hapus"
            cmdEdit.Text = "Ubah"
        End If

    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        Me.Close()
    End Sub

    Private Sub txtQuantity_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtQuantity.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""
    End Sub

    Private Sub PRODUCT_CODEComboBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles PRODUCT_CODEComboBox.LostFocus
        If PRODUCT_CODEComboBox.Text = "" Then Exit Sub

        SP_LIST_PRODUCT_GOODSTableAdapter.SP_GET_CATEGORY_PRODUCT(PRODUCT_CODEComboBox.Text, tmpCategory)

        If tmpCategory <> 1 Then
            If Language = "Indonesian" Then
                MsgBox("Produk yang anda masukan bukan barang !" & vbCrLf & _
                   "Gunakan Form Pencarian untuk mencari produk yang benar !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You input Product which is not Goods !" & vbCrLf & _
                   "Use the Search Form to find the right Product !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            PRODUCT_NAMETextBox.Text = ""
            PRODUCT_CODEComboBox.Text = ""

        End If
    End Sub

    Private Sub PRODUCT_CODEComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PRODUCT_CODEComboBox.TextChanged
        SP_LIST_PRODUCT_GOODSTableAdapter.SP_GET_PRODUCT_NAME(PRODUCT_CODEComboBox.Text, PRODUCT_NAMETextBox.Text)
        If PRODUCT_NAMETextBox.Text <> "" Then
            SP_LIST_PRODUCT_GOODSBindingSource.Position = SP_LIST_PRODUCT_GOODSBindingSource.Find("PRODUCT_CODE", PRODUCT_CODEComboBox.Text)
            tmpProductID = SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID")

            If WAREHOUSE_NAMEComboBox.Text <> "" Then
                'SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.SP_PRODUCT_WAREHOUSE_DETAIL, _
                '                                        SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID"))
                'SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Filter = "WAREHOUSE_ID = " & _
                'WAREHOUSEBindingSource.Current("WAREHOUSE_ID")
                'If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Count < 1 Then
                'txtSourceQty.Text = 0
                'Else
                'txtSourceQty.Text = SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY")
                'End If
                ''--this block is to Get Product_Warehouse quantity, now it's move to Function TmpProductQuantity--

                SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.SP_PRODUCT_WAREHOUSE_DETAIL, _
                                                    SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID"))
                SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Filter = "WAREHOUSE_NAME = '" & _
                WAREHOUSE_NAMEComboBox.Text & "'"
                If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Count < 1 Then
                    tmpQuantity = 0
                Else
                    tmpQuantity = SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY")
                End If

                ''--Get value for tmpQuantity from Function TmpProductQuantity as quantity in Product_Warehouse--

                txtSourceQty.Text = tmpQuantity

            End If

        End If

    End Sub

    Private Sub cmdSearchName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchName.Click
        mdlGeneral.tmpSearchMode = "Entry Product Dist - Product Name"
        frmSearchProduct.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        SP_LIST_PRODUCT_GOODSBindingSource.Position = SP_LIST_PRODUCT_GOODSBindingSource.Find("PRODUCT_ID", tmpSearchResult)
        PRODUCT_CODEComboBox.Text = SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_CODE")
    End Sub

    Private Sub cmdMode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMode.Click
        If PRODUCT_CODEComboBox.Text = "" Or PRODUCT_NAMETextBox.Text = "" Or _
            WAREHOUSE_NAMEComboBox.Text = "" Or txtQuantity.Text = "" Or txtQuantity.Text = "0" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        PRODUCT_DISTRIBUTIONTableAdapter.SP_GET_CATEGORY_PRODUCT(PRODUCT_CODEComboBox.Text, tmpCategory)
        If tmpCategory = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan masukan produk lain !" & vbCrLf & _
                   "Tipe produk ini adalah jasa.", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter other product !" & vbCrLf & _
                        "This type's product is Service.", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        Dim strValue As Integer
        strValue = Val(txtQuantity.Text)
        If strValue = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If WAREHOUSE_NAMEComboBox.Text = WAREHOUSE_NAMEComboBox1.Text Then
            If Language = "Indonesian" Then
                MsgBox("Awal dan Tujuan tidak boleh memilih gudang yang sama!", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Source and Destination should not be the same Warehouse!", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If PRODUCT_WAREHOUSETableAdapter.SP_CLOSED_PERIOD_PROCESS("G", Now) = "TRUE" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menyimpan transaksi ini!" & vbCrLf & "Proses Tutup Buku sedang berlangsung", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Save this Transaction!" & vbCrLf & "The Closing Period is Running", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpWarehouseDestinationID = IIf(WAREHOUSE_NAMEComboBox1.Text.Length = 0, _
                                        0, _
                                        WAREHOUSE1BindingSource1.Current("WAREHOUSE_ID"))

        'SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.SP_PRODUCT_WAREHOUSE_DETAIL, _
        '                                             PRODUCTBindingSource.Current("PRODUCT_ID"))
        'SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Filter = "WAREHOUSE_ID = " & _
        'WAREHOUSEBindingSource.Current("WAREHOUSE_ID")
        'If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Count < 1 Then
        'tmpQuantity = 0
        'Else
        'tmpQuantity = SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY")
        'End If
        'MsgBox(tmpQuantity)
        'tmpQuantity = CInt(txtSourceQty.Text)
        ''--this block is to Get Product_Warehouse quantity, now it's move to Function TmpProductQuantity--

        SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.SP_PRODUCT_WAREHOUSE_DETAIL, _
                                                    SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID"))
        SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Filter = "WAREHOUSE_NAME = '" & _
        WAREHOUSE_NAMEComboBox.Text & "'"
        If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Count < 1 Then
            tmpQuantity = 0
        Else
            tmpQuantity = SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY")
        End If
        
        ''--Get value for tmpQuantity from Function TmpProductQuantity as quantity in Product_Warehouse--

        If tmpQuantity < CInt(txtQuantity.Text) Then
            If Language = "Indonesian" Then
                MsgBox("This Product has " & tmpQuantity & " kuantitas." & vbCrLf & _
                   "Mendistribusikan Produk dengan kuantitas melebihi saldo tidak diperkenankan.", _
                   MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Produk ini telah " & tmpQuantity & " quantity." & vbCrLf & _
                   "Distributing a Product with quantity exceed the balance is not allowed.", _
                   MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        PRODUCT_DISTRIBUTIONTableAdapter.SP_PRODUCT_DISTRIBUTION("I", _
                                                                 0, _
                                                                 Now, _
                                                                 PRODUCT_DISTRIBUTION_NUMBERTextBox.Text, _
                                                                 tmpProductID, _
                                                                 WAREHOUSEBindingSource1.Current("WAREHOUSE_ID"), _
                                                                 tmpWarehouseDestinationID, _
                                                                 CInt(txtQuantity.Text), _
                                                                 USER_ID, _
                                                                 Now, _
                                                                 0, _
                                                                 DateSerial(4000, 12, 31))

        PRODUCT_WAREHOUSETableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                           0, _
                                                           tmpProductID, _
                                                           WAREHOUSEBindingSource1.Current("WAREHOUSE_ID"), _
                                                           CInt(txtQuantity.Text) * (-1), _
                                                           USER_ID, _
                                                           Now, _
                                                           0, _
                                                           DateSerial(4000, 12, 31))

        PRODUCT_WAREHOUSETableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                           0, _
                                                           tmpProductID, _
                                                           tmpWarehouseDestinationID, _
                                                           CInt(txtQuantity.Text), _
                                                           USER_ID, _
                                                           Now, _
                                                           0, _
                                                           DateSerial(4000, 12, 31))

        If Language = "Indonesian" Then
            MsgBox("Data Berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
        End If

        frmEntryProductDistribution_Load(Nothing, Nothing)
    End Sub

    Private Sub PRODUCT_CODEComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PRODUCT_CODEComboBox.SelectedIndexChanged
        tmpChange = True
    End Sub

    Private Sub WAREHOUSE_NAMEComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tmpChange = True
    End Sub

    Private Sub WAREHOUSE_NAMEComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tmpChange = True
    End Sub

    Private Sub txtQuantity_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtQuantity.TextChanged
        tmpChange = True
    End Sub

    Private Sub txtSourceQty_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSourceQty.TextChanged
        tmpChange = True
    End Sub

    Private Sub WAREHOUSE_NAMEComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles WAREHOUSE_NAMEComboBox.TextChanged
        
        If WAREHOUSEBindingSource.Count < 1 Then
            txtSourceQty.Text = "0"
            Exit Sub
        End If

        If PRODUCT_CODEComboBox.Text = "" Then
            txtSourceQty.Text = "0"
            Exit Sub
        Else

            'SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.SP_PRODUCT_WAREHOUSE_DETAIL, _
            '                               PRODUCTBindingSource.Current("PRODUCT_ID"))
            'SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Filter = "WAREHOUSE_ID = " & _
            'WAREHOUSEBindingSource.Current("WAREHOUSE_ID")
            ''--this block is to Get Product_Warehouse quantity, now it's move to Function TmpProductQuantity--

            SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.SP_PRODUCT_WAREHOUSE_DETAIL, _
                                                    SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID"))
            SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Filter = "WAREHOUSE_NAME = '" & _
            WAREHOUSE_NAMEComboBox.Text & "'"
            If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Count < 1 Then
                tmpQuantity = 0
            Else
                tmpQuantity = SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY")
            End If

            ''--Get value for tmpQuantity from Function TmpProductQuantity as quantity in Product_Warehouse--

            txtSourceQty.Text = tmpQuantity

        End If

    End Sub

    Private Sub PRODUCT_DISTRIBUTION_NUMBERTextBox_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles PRODUCT_DISTRIBUTION_NUMBERTextBox.KeyDown
        If e.KeyCode = Keys.Delete Then
            e.Handled = True
        End If
    End Sub

    Private Sub PRODUCT_DISTRIBUTION_NUMBERTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PRODUCT_DISTRIBUTION_NUMBERTextBox.KeyPress
        e.KeyChar = ""
    End Sub

    Private Sub txtDate_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDate.KeyDown
        If e.KeyCode = Keys.Delete Then
            e.Handled = True
            Exit Sub
        End If
    End Sub

    
    Private Sub txtDate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDate.KeyPress
        e.KeyChar = ""
    End Sub
End Class