﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSearchPayableInvoice
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSearchPayableInvoice))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DS_PAYMENT_TRANSACTION = New DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTION()
        Me.SP_PAYABLE_INVOICEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_PAYABLE_INVOICETableAdapter = New DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.SP_PAYABLE_INVOICETableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.TableAdapterManager()
        Me.SP_PAYABLE_INVOICEBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.dgvPAYABLE_INVOICE = New System.Windows.Forms.DataGridView()
        Me.lblInvoice = New System.Windows.Forms.Label()
        Me.txtInvoiceNo = New System.Windows.Forms.TextBox()
        Me.txtVendorName = New System.Windows.Forms.TextBox()
        Me.lblCustomer = New System.Windows.Forms.Label()
        Me.lblNoOfInvoice = New System.Windows.Forms.Label()
        Me.VENDOR_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RECEIPT_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RECEIPT_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL_PAYABLE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MATURITY_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DS_PAYMENT_TRANSACTION, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_PAYABLE_INVOICEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_PAYABLE_INVOICEBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SP_PAYABLE_INVOICEBindingNavigator.SuspendLayout()
        CType(Me.dgvPAYABLE_INVOICE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DS_PAYMENT_TRANSACTION
        '
        Me.DS_PAYMENT_TRANSACTION.DataSetName = "DS_PAYMENT_TRANSACTION"
        Me.DS_PAYMENT_TRANSACTION.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SP_PAYABLE_INVOICEBindingSource
        '
        Me.SP_PAYABLE_INVOICEBindingSource.DataMember = "SP_PAYABLE_INVOICE"
        Me.SP_PAYABLE_INVOICEBindingSource.DataSource = Me.DS_PAYMENT_TRANSACTION
        '
        'SP_PAYABLE_INVOICETableAdapter
        '
        Me.SP_PAYABLE_INVOICETableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.CUSTOMERTableAdapter = Nothing
        Me.TableAdapterManager.PAYABLE_PAYMENTTableAdapter = Nothing
        Me.TableAdapterManager.PAYABLETableAdapter = Nothing
        Me.TableAdapterManager.PAYMENT_METHODTableAdapter = Nothing
        Me.TableAdapterManager.RECEIVABLE_PAYMENTTableAdapter = Nothing
        Me.TableAdapterManager.RECEIVABLETableAdapter = Nothing
        Me.TableAdapterManager.SALETableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'SP_PAYABLE_INVOICEBindingNavigator
        '
        Me.SP_PAYABLE_INVOICEBindingNavigator.AddNewItem = Nothing
        Me.SP_PAYABLE_INVOICEBindingNavigator.BindingSource = Me.SP_PAYABLE_INVOICEBindingSource
        Me.SP_PAYABLE_INVOICEBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.SP_PAYABLE_INVOICEBindingNavigator.DeleteItem = Nothing
        Me.SP_PAYABLE_INVOICEBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.SP_PAYABLE_INVOICEBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.SP_PAYABLE_INVOICEBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.SP_PAYABLE_INVOICEBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.SP_PAYABLE_INVOICEBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.SP_PAYABLE_INVOICEBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.SP_PAYABLE_INVOICEBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.SP_PAYABLE_INVOICEBindingNavigator.Name = "SP_PAYABLE_INVOICEBindingNavigator"
        Me.SP_PAYABLE_INVOICEBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.SP_PAYABLE_INVOICEBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.SP_PAYABLE_INVOICEBindingNavigator.Size = New System.Drawing.Size(590, 25)
        Me.SP_PAYABLE_INVOICEBindingNavigator.TabIndex = 0
        Me.SP_PAYABLE_INVOICEBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'dgvPAYABLE_INVOICE
        '
        Me.dgvPAYABLE_INVOICE.AllowUserToAddRows = False
        Me.dgvPAYABLE_INVOICE.AllowUserToDeleteRows = False
        Me.dgvPAYABLE_INVOICE.AutoGenerateColumns = False
        Me.dgvPAYABLE_INVOICE.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPAYABLE_INVOICE.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPAYABLE_INVOICE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPAYABLE_INVOICE.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.VENDOR_NAME, Me.RECEIPT_ID, Me.RECEIPT_NO, Me.TOTAL_PAYABLE, Me.MATURITY_DATE})
        Me.dgvPAYABLE_INVOICE.DataSource = Me.SP_PAYABLE_INVOICEBindingSource
        Me.dgvPAYABLE_INVOICE.Location = New System.Drawing.Point(12, 94)
        Me.dgvPAYABLE_INVOICE.Name = "dgvPAYABLE_INVOICE"
        Me.dgvPAYABLE_INVOICE.ReadOnly = True
        Me.dgvPAYABLE_INVOICE.RowHeadersWidth = 30
        Me.dgvPAYABLE_INVOICE.Size = New System.Drawing.Size(566, 259)
        Me.dgvPAYABLE_INVOICE.TabIndex = 3
        '
        'lblInvoice
        '
        Me.lblInvoice.Location = New System.Drawing.Point(15, 69)
        Me.lblInvoice.Name = "lblInvoice"
        Me.lblInvoice.Size = New System.Drawing.Size(106, 19)
        Me.lblInvoice.TabIndex = 11
        Me.lblInvoice.Text = "Invoice No."
        Me.lblInvoice.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtInvoiceNo
        '
        Me.txtInvoiceNo.Location = New System.Drawing.Point(127, 66)
        Me.txtInvoiceNo.Name = "txtInvoiceNo"
        Me.txtInvoiceNo.Size = New System.Drawing.Size(166, 22)
        Me.txtInvoiceNo.TabIndex = 2
        '
        'txtVendorName
        '
        Me.txtVendorName.Location = New System.Drawing.Point(127, 38)
        Me.txtVendorName.Name = "txtVendorName"
        Me.txtVendorName.Size = New System.Drawing.Size(166, 22)
        Me.txtVendorName.TabIndex = 1
        '
        'lblCustomer
        '
        Me.lblCustomer.Location = New System.Drawing.Point(12, 41)
        Me.lblCustomer.Name = "lblCustomer"
        Me.lblCustomer.Size = New System.Drawing.Size(109, 19)
        Me.lblCustomer.TabIndex = 8
        Me.lblCustomer.Text = "Vendor Name"
        Me.lblCustomer.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblNoOfInvoice
        '
        Me.lblNoOfInvoice.AutoSize = True
        Me.lblNoOfInvoice.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoOfInvoice.Location = New System.Drawing.Point(22, 361)
        Me.lblNoOfInvoice.Name = "lblNoOfInvoice"
        Me.lblNoOfInvoice.Size = New System.Drawing.Size(100, 15)
        Me.lblNoOfInvoice.TabIndex = 12
        Me.lblNoOfInvoice.Text = "No of Invoice :"
        '
        'VENDOR_NAME
        '
        Me.VENDOR_NAME.DataPropertyName = "VENDOR_NAME"
        Me.VENDOR_NAME.HeaderText = "Vendor Name"
        Me.VENDOR_NAME.Name = "VENDOR_NAME"
        Me.VENDOR_NAME.ReadOnly = True
        Me.VENDOR_NAME.Width = 160
        '
        'RECEIPT_ID
        '
        Me.RECEIPT_ID.DataPropertyName = "RECEIPT_ID"
        Me.RECEIPT_ID.HeaderText = "RECEIPT_ID"
        Me.RECEIPT_ID.Name = "RECEIPT_ID"
        Me.RECEIPT_ID.ReadOnly = True
        Me.RECEIPT_ID.Visible = False
        '
        'RECEIPT_NO
        '
        Me.RECEIPT_NO.DataPropertyName = "RECEIPT_NO"
        Me.RECEIPT_NO.HeaderText = "Receipt No"
        Me.RECEIPT_NO.Name = "RECEIPT_NO"
        Me.RECEIPT_NO.ReadOnly = True
        Me.RECEIPT_NO.Width = 110
        '
        'TOTAL_PAYABLE
        '
        Me.TOTAL_PAYABLE.DataPropertyName = "TOTAL_PAYABLE"
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.TOTAL_PAYABLE.DefaultCellStyle = DataGridViewCellStyle2
        Me.TOTAL_PAYABLE.HeaderText = "Total Payable"
        Me.TOTAL_PAYABLE.Name = "TOTAL_PAYABLE"
        Me.TOTAL_PAYABLE.ReadOnly = True
        Me.TOTAL_PAYABLE.Width = 115
        '
        'MATURITY_DATE
        '
        Me.MATURITY_DATE.DataPropertyName = "MATURITY_DATE"
        DataGridViewCellStyle3.Format = "dd-MMM-yyyy"
        Me.MATURITY_DATE.DefaultCellStyle = DataGridViewCellStyle3
        Me.MATURITY_DATE.HeaderText = "Maturity Date"
        Me.MATURITY_DATE.Name = "MATURITY_DATE"
        Me.MATURITY_DATE.ReadOnly = True
        Me.MATURITY_DATE.Width = 125
        '
        'frmSearchPayableInvoice
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(590, 396)
        Me.Controls.Add(Me.lblNoOfInvoice)
        Me.Controls.Add(Me.lblInvoice)
        Me.Controls.Add(Me.txtInvoiceNo)
        Me.Controls.Add(Me.txtVendorName)
        Me.Controls.Add(Me.lblCustomer)
        Me.Controls.Add(Me.dgvPAYABLE_INVOICE)
        Me.Controls.Add(Me.SP_PAYABLE_INVOICEBindingNavigator)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmSearchPayableInvoice"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "List Payable Invoice"
        CType(Me.DS_PAYMENT_TRANSACTION, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_PAYABLE_INVOICEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_PAYABLE_INVOICEBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SP_PAYABLE_INVOICEBindingNavigator.ResumeLayout(False)
        Me.SP_PAYABLE_INVOICEBindingNavigator.PerformLayout()
        CType(Me.dgvPAYABLE_INVOICE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DS_PAYMENT_TRANSACTION As DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTION
    Friend WithEvents SP_PAYABLE_INVOICEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_PAYABLE_INVOICETableAdapter As DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.SP_PAYABLE_INVOICETableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.TableAdapterManager
    Friend WithEvents SP_PAYABLE_INVOICEBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents dgvPAYABLE_INVOICE As System.Windows.Forms.DataGridView
    Friend WithEvents lblInvoice As System.Windows.Forms.Label
    Friend WithEvents txtInvoiceNo As System.Windows.Forms.TextBox
    Friend WithEvents txtVendorName As System.Windows.Forms.TextBox
    Friend WithEvents lblCustomer As System.Windows.Forms.Label
    Friend WithEvents lblNoOfInvoice As System.Windows.Forms.Label
    Friend WithEvents VENDOR_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RECEIPT_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RECEIPT_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL_PAYABLE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MATURITY_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
