﻿Imports System.Data.SqlClient
Imports System.Data
Public Class frmListReceivable
    Dim tmpAmount As Integer
    Public tmpPeriod3 As Date

    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
   

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        Me.Cursor = Cursors.WaitCursor
        'SP_LIST_RECEIVABLETableAdapter.Fill(Me.DS_SALE.SP_LIST_RECEIVABLE, dtpPeriod.Value, dtpPeriod2.Value)
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC 	@return_value = [dbo].[SP_LIST_RECEIVABLE]" & vbCrLf & _
              "         @PERIOD1 = N'" & dtpPeriod.Value & "'," & vbCrLf & _
              "    		@PERIOD2 = N'" & dtpPeriod2.Value & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.dgvLIST_RECEIVABLE.DataSource = dt
        Me.Cursor = Cursors.Default


        For x As Integer = 0 To dgvLIST_RECEIVABLE.RowCount - 1
            tmpAmount = tmpAmount + dgvLIST_RECEIVABLE.Item("AMOUNT", x).Value
            tmpAmount = tmpAmount
        Next

        If Language = "Indonesian" Then
            Label2.Text = "Jumlah Faktur : " & dgvLIST_RECEIVABLE.RowCount
            Label3.Text = "Jumlah Nominal : " & FormatNumber(tmpAmount, 0)
            dgvLIST_RECEIVABLE.Columns("INVOICE").HeaderText = "No Faktur"
            dgvLIST_RECEIVABLE.Columns("TRANSACTION_DATE").HeaderText = "Tgl Transaksi"
            dgvLIST_RECEIVABLE.Columns("MATURITY_DATE").HeaderText = "Tgl Jatuh Tempo"
            dgvLIST_RECEIVABLE.Columns("AMOUNT").HeaderText = "Jumlah"
            dgvLIST_RECEIVABLE.Columns("DESCRIPTION").HeaderText = "Keterangan"
            dgvLIST_RECEIVABLE.Columns("BALANCE").HeaderText = "Saldo"
            dgvLIST_RECEIVABLE.Columns("RECEIVABLE_ID").Visible = False
            dgvLIST_RECEIVABLE.Columns("PERIOD1").Visible = False
            dgvLIST_RECEIVABLE.Columns("PERIOD2").Visible = False
            dgvLIST_RECEIVABLE.Columns("TRANSACTION_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_RECEIVABLE.Columns("MATURITY_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_RECEIVABLE.Columns("AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_RECEIVABLE.Columns("BALANCE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_RECEIVABLE.Columns("TRANSACTION_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvLIST_RECEIVABLE.Columns("MATURITY_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvLIST_RECEIVABLE.Columns("AMOUNT").DefaultCellStyle.Format = "n0"
            dgvLIST_RECEIVABLE.Columns("BALANCE").DefaultCellStyle.Format = "n0"
        Else
            dgvLIST_RECEIVABLE.Columns("INVOICE").HeaderText = "Invoice No"
            dgvLIST_RECEIVABLE.Columns("TRANSACTION_DATE").HeaderText = "Transaction Date"
            dgvLIST_RECEIVABLE.Columns("MATURITY_DATE").HeaderText = "Maturity Date"
            dgvLIST_RECEIVABLE.Columns("AMOUNT").HeaderText = "Amount"
            dgvLIST_RECEIVABLE.Columns("DESCRIPTION").HeaderText = "Description"
            dgvLIST_RECEIVABLE.Columns("BALANCE").HeaderText = "Balance"
            dgvLIST_RECEIVABLE.Columns("RECEIVABLE_ID").Visible = False
            dgvLIST_RECEIVABLE.Columns("PERIOD1").Visible = False
            dgvLIST_RECEIVABLE.Columns("PERIOD2").Visible = False
            dgvLIST_RECEIVABLE.Columns("TRANSACTION_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_RECEIVABLE.Columns("MATURITY_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_RECEIVABLE.Columns("AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_RECEIVABLE.Columns("BALANCE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_RECEIVABLE.Columns("TRANSACTION_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvLIST_RECEIVABLE.Columns("MATURITY_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvLIST_RECEIVABLE.Columns("AMOUNT").DefaultCellStyle.Format = "n0"
            dgvLIST_RECEIVABLE.Columns("BALANCE").DefaultCellStyle.Format = "n0"
            Label2.Text = "Total Invoice : " & dgvLIST_RECEIVABLE.RowCount
            Label3.Text = "Total Amount : " & FormatNumber(tmpAmount, 0)
        End If

    End Sub

    'Private Sub frmListReceivable_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
    '    If e.KeyCode = Keys.F12 Then
    '        cmdReport_Click(Nothing, Nothing)
    '    End If
    'End Sub

    Private Sub frmListReceivable_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Language = "Indonesian" Then
            Me.Text = "Daftar Piutang"
            cmdGenerate.Text = "Proses"
            cmdReport.Text = "Cetak"
            lblPeriod.Text = "Periode"

            'dgvLIST_RECEIVABLE.Columns("INVOICE").HeaderText = "No Faktur"
            'dgvLIST_RECEIVABLE.Columns("TRANSACTION_DATE").HeaderText = "Tgl Transaksi"
            'dgvLIST_RECEIVABLE.Columns("MATURITY_DATE").HeaderText = "Tgl Jatuh Tempo"
            'dgvLIST_RECEIVABLE.Columns("AMOUNT").HeaderText = "Jumlah"
            'dgvLIST_RECEIVABLE.Columns("DESCRIPTION").HeaderText = "Keterangan"

            Label2.Text = "Jumlah Faktur : " & dgvLIST_RECEIVABLE.RowCount
            Label3.Text = "Jumlah Nominal : " & FormatNumber(tmpAmount, 0)
        Else
            Label2.Text = "Total Invoice : " & dgvLIST_RECEIVABLE.RowCount
            Label3.Text = "Total Amount : " & FormatNumber(tmpAmount, 0)
        End If

        dtpPeriod.Value = DateSerial(Today.Year, Today.Month, 1)
        dtpPeriod2.Value = DateSerial(Today.Year, Today.Month + 1, 0)

        tmpPeriod3 = DateSerial(4000, 12, 31)
        tmpAmount = 0
    End Sub

    'Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
    '    Dim RPV As New frmRepActiveReceivable
    '    RPV.date1 = dtpPeriod.Value
    '    RPV.date2 = dtpPeriod2.Value
    '    RPV.ReportViewer1.ZoomPercent = 100
    '    RPV.WindowState = FormWindowState.Maximized
    '    RPV.Show()
    'End Sub

    Private Sub dtpPeriod_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpPeriod.ValueChanged

        If dgvLIST_RECEIVABLE.RowCount > 0 Then
            'SP_LIST_RECEIVABLETableAdapter.Fill(Me.DS_SALE.SP_LIST_RECEIVABLE, tmpPeriod3, tmpPeriod3)
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = "DECLARE	@return_value int" & vbCrLf & _
                  "EXEC 	@return_value = [dbo].[SP_LIST_RECEIVABLE]" & vbCrLf & _
                  "         @PERIOD1 = N'" & tmpPeriod3 & "'," & vbCrLf & _
                  "    		@PERIOD2 = N'" & tmpPeriod3 & "'" & vbCrLf & _
                  "SELECT	'Return Value' = @return_value"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.dgvLIST_RECEIVABLE.DataSource = dt
        End If
    End Sub

    Private Sub dtpPeriod2_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpPeriod2.ValueChanged
        If dgvLIST_RECEIVABLE.RowCount > 0 Then
            'SP_LIST_RECEIVABLETableAdapter.Fill(Me.DS_SALE.SP_LIST_RECEIVABLE, tmpPeriod3, tmpPeriod3)
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = "DECLARE	@return_value int" & vbCrLf & _
                  "EXEC 	@return_value = [dbo].[SP_LIST_RECEIVABLE]" & vbCrLf & _
                  "         @PERIOD1 = N'" & tmpPeriod3 & "'," & vbCrLf & _
                  "    		@PERIOD2 = N'" & tmpPeriod3 & "'" & vbCrLf & _
                  "SELECT	'Return Value' = @return_value"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.dgvLIST_RECEIVABLE.DataSource = dt
        End If
    End Sub

End Class