﻿Imports System.Data.SqlClient

Public Class frmEntryWarehouse
    Dim tmpSaveMode As String
    Dim tmpKey, tmpWarehouseName, tmpUsedWarehouse, tmpEditCode As String
    Dim tmpChange As Boolean
    Dim tmpResult As Integer
    Dim tmpWarehouseId As Integer = 0
    Dim tmpEndDate As DateTime
    Public WAREHOUSEBindingSource As New BindingSource

    Private Sub GetData()
        sql = "SELECT WAREHOUSE_ID, WAREHOUSE_CODE, WAREHOUSE_NAME, DESCRIPTION, WAREHOUSE_ADDRESS, WAREHOUSE_CITY, WAREHOUSE_COUNTRY, PIC, PHONE, FAX, EMAIL, EFFECTIVE_START_DATE, EFFECTIVE_END_DATE, USER_ID_INPUT, INPUT_DATE, USER_ID_UPDATE, UPDATE_DATE FROM dbo.WAREHOUSE"
        dt = New DataTable
        sqladapter = New SqlDataAdapter(sql, xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        WAREHOUSEBindingSource.DataSource = dt
        WAREHOUSEBindingNavigator.BindingSource = WAREHOUSEBindingSource

    End Sub

    Private Sub frmEntryWarehouse_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If tmpChange = True And WAREHOUSE_NAMETextBox.Enabled = True Then
            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah Anda ingin menyimpan perubahan " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If
            If tmpvar = 6 Then
                cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub frmEntryWarehouse_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_WAREHOUSE.SP_GENERATE_WAREHOUSE_ID' table. You can move, or remove it, as needed.
        'Me.SP_GENERATE_WAREHOUSE_IDTableAdapter.Fill(Me.DS_WAREHOUSE.SP_GENERATE_WAREHOUSE_ID)
        'TODO: This line of code loads data into the 'DS_WAREHOUSE.WAREHOUSE' table. You can move, or remove it, as needed.
        'Me.WAREHOUSETableAdapter.Fill(Me.DS_WAREHOUSE.WAREHOUSE)
        Me.MdiParent = ObjFormMain
        GetData()
        accFormName = Me.Text

        WAREHOUSE_CODETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", WAREHOUSEBindingSource, "WAREHOUSE_CODE", True))
        WAREHOUSE_NAMETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", WAREHOUSEBindingSource, "WAREHOUSE_NAME", True))
        DESCRIPTIONTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", WAREHOUSEBindingSource, "DESCRIPTION", True))
        WAREHOUSE_ADDRESSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", WAREHOUSEBindingSource, "WAREHOUSE_ADDRESS", True))
        WAREHOUSE_CITYTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", WAREHOUSEBindingSource, "WAREHOUSE_CITY", True))
        WAREHOUSE_COUNTRYTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", WAREHOUSEBindingSource, "WAREHOUSE_COUNTRY", True))
        PICTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", WAREHOUSEBindingSource, "PIC", True))
        PHONETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", WAREHOUSEBindingSource, "PHONE", True))
        FAXTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", WAREHOUSEBindingSource, "FAX", True))
        EMAILTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", WAREHOUSEBindingSource, "EMAIL", True))
        DisableInputBox(Me)

        cmdSearch.Visible = True
        WAREHOUSEBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        tmpChange = False
        chkActive.Enabled = False

        If WAREHOUSE_NAMETextBox.Text <> "" Then
            If WAREHOUSEBindingSource.Current("WAREHOUSE_ID") Is Nothing Or _
                 IsDBNull(WAREHOUSEBindingSource.Current("WAREHOUSE_ID")) Then
                chkRummage.Checked = False
                Exit Sub
            End If
            If WAREHOUSEBindingSource.Current("WAREHOUSE_ID") = -1 Then
                chkRummage.Checked = True
            Else
                chkRummage.Checked = False
            End If

            If WAREHOUSEBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        End If

        If Language = "Indonesian" Then
            Me.Text = "Input Gudang"
            lblName.Text = "Nama"
            lblAddress.Text = "Alamat"
            lblCity.Text = "Kota"
            lblDesc.Text = "Keterangan"
            lblPhone.Text = "Telpon"
            lblPic.Text = "Kode Pos"
            chkRummage.Text = "Gudang Barang Sisa"
            cmdAdd.Text = "Tambah"
            cmdEdit.Text = "Ubah"
            cmdUndo.Text = "Batal"
            cmdSave.Text = "Simpan"
            cmdDelete.Text = "Hapus"
        End If

        chkRummage.Enabled = False

    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If Trim(RsAccessPrivilege.Item("ALLOW_ADD").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Insert"

        EnableInputBox(Me)
        WAREHOUSEBindingSource.AddNew()
        xComm = New SqlCommand("SP_GENERATE_MASTER_CODE", xConn)
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@DOCUMENT_TYPE", "WAREHOUSE")
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdreader = xComm.ExecuteReader
        xdreader.Read()
        If xdreader.HasRows Then
            WAREHOUSE_CODETextBox.Text = xdreader.Item(0)
        End If
        'WAREHOUSETableAdapter.SP_GENERATE_MASTER_CODE("WAREHOUSE", WAREHOUSE_CODETextBox.Text)

        chkActive.Enabled = True
        chkActive.Checked = True
        cmdSearch.Visible = False
        WAREHOUSEBindingNavigator.Enabled = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        tmpChange = False
        chkRummage.Enabled = True

    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        If WAREHOUSE_NAMETextBox.Text = "" Then Exit Sub
        If Trim(RsAccessPrivilege.Item("ALLOW_EDIT").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpKey = WAREHOUSE_NAMETextBox.Text

        tmpSaveMode = "Update"

        EnableInputBox(Me)

        cmdSearch.Visible = False
        WAREHOUSEBindingNavigator.Enabled = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        chkRummage.Enabled = False
        chkActive.Enabled = True

        tmpEditCode = WAREHOUSE_CODETextBox.Text

        tmpWarehouseName = WAREHOUSE_NAMETextBox.Text

        tmpChange = False
        If WAREHOUSEBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
            chkActive.Checked = False
        Else
            chkActive.Checked = True
        End If

    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        tmpSaveMode = ""

        DisableInputBox(Me)
        WAREHOUSEBindingSource.CancelEdit()

        cmdSearch.Visible = True
        chkActive.Enabled = False
        WAREHOUSEBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        tmpChange = False
        chkRummage.Enabled = False
        WAREHOUSEBindingSource_CurrentChanged(Nothing, Nothing)

    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Me.Validate()
        Dim tmpX As Integer

        If WAREHOUSE_NAMETextBox.Text = "" Or WAREHOUSE_CITYTextBox.Text = "" Or PICTextBox.Text = "" Or WAREHOUSE_CODETextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpKey = WAREHOUSE_NAMETextBox.Text

        If tmpSaveMode = "Insert" Then
            xComm = New SqlCommand("SP_CHECK_MASTER_CODE", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@DOCUMENT_TYPE", "WAREHOUSE")
            xComm.Parameters.AddWithValue("@DOC_NO", WAREHOUSE_CODETextBox.Text)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xdreader = xComm.ExecuteReader
            xdreader.Read()
            tmpResult = xdreader.Item(0)
            ' WAREHOUSETableAdapter.SP_CHECK_MASTER_CODE("WAREHOUSE", WAREHOUSE_CODETextBox.Text, tmpResult)
            If tmpResult > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("No Transaksi telah digunakan oleh user lain." & vbCrLf & _
                                               "Nomor transaksi baru akan digunakan untuk transaksi ini.", _
                                               MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Receipt Number is used by another User." & vbCrLf & _
                                                        "New Receipt Number will be assigned to this transaction.", _
                                                        MsgBoxStyle.Critical, "DMI Retail")
                End If
                If tmpResult > 0 Then
                    xComm = New SqlCommand("SP_GENERATE_MASTER_CODE", xConn)
                    xComm.CommandType = CommandType.StoredProcedure
                    xComm.Parameters.AddWithValue("@DOCUMENT_TYPE", "WAREHOUSE")
                    If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                    xdreader = xComm.ExecuteReader
                    xdreader.Read()
                    If xdreader.HasRows Then
                        WAREHOUSE_CODETextBox.Text = xdreader.Item(0)
                    End If
                    'WAREHOUSETableAdapter.SP_GENERATE_MASTER_CODE("WAREHOUSE", WAREHOUSE_CODETextBox.Text)
                    xComm = New SqlCommand("SP_CHECK_MASTER_CODE", xConn)
                    xComm.CommandType = CommandType.StoredProcedure
                    xComm.Parameters.AddWithValue("@DOCUMENT_TYPE", "WAREHOUSE")
                    xComm.Parameters.AddWithValue("@DOC_NO", WAREHOUSE_CODETextBox.Text)
                    If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                    xdreader = xComm.ExecuteReader
                    xdreader.Read()
                    tmpResult = xdreader.Item(0)
                    'WAREHOUSETableAdapter.SP_CHECK_MASTER_CODE("WAREHOUSE", WAREHOUSE_CODETextBox.Text, tmpResult)
                End If
                Exit Sub
            End If

            If chkRummage.Checked = True Then
                xComm = New SqlCommand("SP_GENERATE_WAREHOUSE_ID", xConn)
                xComm.CommandType = CommandType.StoredProcedure
                If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                xdreader = xComm.ExecuteReader
                xdreader.Read()
                If xdreader.HasRows Then
                    If Language = "Indonesian" Then
                        MsgBox("Gudang barang sisa telah ada di database" & vbCrLf & _
                                "Silahkan pilih gudang lain", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("Rummage is exists in database" & vbCrLf & _
                            "Please enter other Warehouse", MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    chkRummage.Checked = False
                    Exit Sub
                Else
                    tmpWarehouseId = -1

                End If
            Else
                tmpWarehouseId = 0

            End If
            xComm = New SqlCommand("SP_WAREHOUSE_CHECK_NAME", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@WAREHOUSE_NAME", WAREHOUSE_NAMETextBox.Text)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xdreader = xComm.ExecuteReader
            xdreader.Read()
            tmpX = xdreader.Item(0)
            'WAREHOUSETableAdapter.SP_WAREHOUSE_CHECK_NAME(WAREHOUSE_NAMETextBox.Text, tmpX)
            If tmpX > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Nama gudang telah ada di database." & vbCrLf & _
                           "Silahkan masukan nama yang lain!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Warehouse Name already exist in database." & vbCrLf & _
                       "Please enter another Name!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If
            xComm = New SqlCommand("SP_WAREHOUSE", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "I")
            xComm.Parameters.AddWithValue("@WAREHOUSE_ID", tmpWarehouseId)
            xComm.Parameters.AddWithValue("@WAREHOUSE_CODE", WAREHOUSE_CODETextBox.Text)
            xComm.Parameters.AddWithValue("@WAREHOUSE_NAME", WAREHOUSE_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@DESCRIPTION", DESCRIPTIONTextBox.Text)
            xComm.Parameters.AddWithValue("@WAREHOUSE_ADDRESS", WAREHOUSE_ADDRESSTextBox.Text)
            xComm.Parameters.AddWithValue("@WAREHOUSE_CITY", WAREHOUSE_CITYTextBox.Text)
            xComm.Parameters.AddWithValue("@WAREHOUSE_COUNTRY", WAREHOUSE_COUNTRYTextBox.Text)
            xComm.Parameters.AddWithValue("@PIC", PICTextBox.Text)
            xComm.Parameters.AddWithValue("@PHONE", PHONETextBox.Text)
            xComm.Parameters.AddWithValue("@FAX", FAXTextBox.Text)
            xComm.Parameters.AddWithValue("@EMAIL", EMAILTextBox.Text)
            xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", Now)
            xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", DateSerial(4000, 12, 31))
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", UserId)
            xComm.Parameters.AddWithValue("@INPUT_DATE", Now)
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", 0)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", DateSerial(4000, 12, 31))
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()

        ElseIf tmpSaveMode = "Update" Then
            If tmpEditCode <> WAREHOUSE_CODETextBox.Text Then
                xComm = New SqlCommand("SP_CHECK_MASTER_CODE", xConn)
                xComm.CommandType = CommandType.StoredProcedure
                xComm.Parameters.AddWithValue("@DOCUMENT_TYPE", "WAREHOUSE")
                xComm.Parameters.AddWithValue("@DOC_NO", WAREHOUSE_CODETextBox.Text)
                If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                xdreader = xComm.ExecuteReader
                xdreader.Read()
                tmpResult = xdreader.Item(0)
                'WAREHOUSETableAdapter.SP_CHECK_MASTER_CODE("WAREHOUSE", WAREHOUSE_CODETextBox.Text, tmpResult)
                If tmpResult > 0 Then
                    If Language = "Indonesian" Then
                        MsgBox("No Transaksi telah digunakan oleh user lain." & vbCrLf & _
                                                   "Nomor transaksi baru akan digunakan untuk transaksi ini.", _
                                                   MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("Receipt Number is used by another User." & vbCrLf & _
                                                            "New Receipt Number will be assigned to this transaction.", _
                                                            MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    WAREHOUSE_CODETextBox.Focus()
                    Exit Sub
                End If
            End If

            If chkActive.Checked = True Then
                tmpEndDate = DateSerial(4000, 12, 31)
            Else
                tmpEndDate = Now
            End If

            If tmpWarehouseName = WAREHOUSE_NAMETextBox.Text Then
                tmpX = 0
            Else
                xComm = New SqlCommand("SP_WAREHOUSE_CHECK_NAME", xConn)
                xComm.CommandType = CommandType.StoredProcedure
                xComm.Parameters.AddWithValue("@WAREHOUSE_NAME", WAREHOUSE_NAMETextBox.Text)
                If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                xdreader = xComm.ExecuteReader
                xdreader.Read()
                tmpX = xdreader.Item(0)
                'WAREHOUSETableAdapter.SP_WAREHOUSE_CHECK_NAME(WAREHOUSE_NAMETextBox.Text, tmpX)
            End If
            If tmpX > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Nama gudang telah ada di database." & vbCrLf & _
                           "Silahkan masukan nama yang lain!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Warehouse Name already exist in database." & vbCrLf & _
                       "Please enter another Name!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If
            xComm = New SqlCommand("SP_WAREHOUSE", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "U")
            xComm.Parameters.AddWithValue("@WAREHOUSE_ID", WAREHOUSEBindingSource.Current("WAREHOUSE_ID"))
            xComm.Parameters.AddWithValue("@WAREHOUSE_CODE", WAREHOUSE_CODETextBox.Text)
            xComm.Parameters.AddWithValue("@WAREHOUSE_NAME", WAREHOUSE_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@DESCRIPTION", DESCRIPTIONTextBox.Text)
            xComm.Parameters.AddWithValue("@WAREHOUSE_ADDRESS", WAREHOUSE_ADDRESSTextBox.Text)
            xComm.Parameters.AddWithValue("@WAREHOUSE_CITY", WAREHOUSE_CITYTextBox.Text)
            xComm.Parameters.AddWithValue("@WAREHOUSE_COUNTRY", WAREHOUSE_COUNTRYTextBox.Text)
            xComm.Parameters.AddWithValue("@PIC", PICTextBox.Text)
            xComm.Parameters.AddWithValue("@PHONE", PHONETextBox.Text)
            xComm.Parameters.AddWithValue("@FAX", FAXTextBox.Text)
            xComm.Parameters.AddWithValue("@EMAIL", EMAILTextBox.Text)
            xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", WAREHOUSEBindingSource.Current("EFFECTIVE_START_DATE"))
            xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", tmpEndDate)
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", WAREHOUSEBindingSource.Current("USER_ID_INPUT"))
            xComm.Parameters.AddWithValue("@INPUT_DATE", WAREHOUSEBindingSource.Current("INPUT_DATE"))
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", UserId)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", Now)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()
        End If

        tmpSaveMode = ""

        DisableInputBox(Me)
        WAREHOUSEBindingSource.CancelEdit()

        cmdSearch.Visible = True
        WAREHOUSEBindingNavigator.Enabled = True
        chkRummage.Enabled = False
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        chkActive.Enabled = False
        getdata()
        ' Me.WAREHOUSETableAdapter.Fill(Me.DS_WAREHOUSE.WAREHOUSE)
        WAREHOUSEBindingSource.Position = WAREHOUSEBindingSource.Find("WAREHOUSE_NAME", tmpKey)
        WAREHOUSEBindingSource_CurrentChanged(Nothing, Nothing)

        If Language = "Indonesian" Then
            MsgBox("Data berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
        End If
        tmpChange = False

    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If WAREHOUSE_NAMETextBox.Text = "" Then Exit Sub

        If Trim(RsAccessPrivilege.Item("ALLOW_DELETE").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If Language = "Indonesian" Then
            If MsgBox("Hapus Gudang?" & vbCrLf & WAREHOUSE_NAMETextBox.Text, _
                      MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub

            xComm = New SqlCommand("SP_CHECK_WAREHOUSE_USED", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@WAREHOUSE_NAME", WAREHOUSE_NAMETextBox.Text)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xdreader = xComm.ExecuteReader
            xdreader.Read()
            tmpUsedWarehouse = xdreader.Item(0)
            'WAREHOUSETableAdapter.SP_CHECK_WAREHOUSE_USED(WAREHOUSE_NAMETextBox.Text, tmpUsedWarehouse)
            If tmpUsedWarehouse > 0 Then
                MsgBox("Gudang ini telah di pakai di transaksi lainnya !" & vbCrLf & _
                       "Anda tidak dapat menghapus data ini !", _
                        MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If
        Else
            If MsgBox("Delete WAREHOUSE?" & vbCrLf & WAREHOUSE_NAMETextBox.Text, _
                 MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
            xComm = New SqlCommand("SP_CHECK_WAREHOUSE_USED", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@WAREHOUSE_NAME", WAREHOUSE_NAMETextBox.Text)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xdreader = xComm.ExecuteReader
            xdreader.Read()
            tmpUsedWarehouse = xdreader.Item(0)
            ' WAREHOUSETableAdapter.SP_CHECK_WAREHOUSE_USED(WAREHOUSE_NAMETextBox.Text, tmpUsedWarehouse)
            If tmpUsedWarehouse > 0 Then
                MsgBox("This Warehouse is used by another transaction !" & vbCrLf & _
                      "You can not delete this data !", MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If
        End If

        xComm = New SqlCommand("SP_WAREHOUSE", xConn)
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@METHOD", "D")
        xComm.Parameters.AddWithValue("@WAREHOUSE_ID", WAREHOUSEBindingSource.Current("WAREHOUSE_ID"))
        xComm.Parameters.AddWithValue("@WAREHOUSE_CODE", WAREHOUSEBindingSource.Current("WAREHOUSE_CODE"))
        xComm.Parameters.AddWithValue("@WAREHOUSE_NAME", WAREHOUSEBindingSource.Current("WAREHOUSE_NAME"))
        xComm.Parameters.AddWithValue("@DESCRIPTION", WAREHOUSEBindingSource.Current("DESCRIPTION"))
        xComm.Parameters.AddWithValue("@WAREHOUSE_ADDRESS", WAREHOUSEBindingSource.Current("WAREHOUSE_ADDRESS"))
        xComm.Parameters.AddWithValue("@WAREHOUSE_CITY", WAREHOUSEBindingSource.Current("WAREHOUSE_CITY"))
        xComm.Parameters.AddWithValue("@WAREHOUSE_COUNTRY", WAREHOUSEBindingSource.Current("WAREHOUSE_COUNTRY"))
        xComm.Parameters.AddWithValue("@PIC", WAREHOUSEBindingSource.Current("PIC"))
        xComm.Parameters.AddWithValue("@PHONE", WAREHOUSEBindingSource.Current("PHONE"))
        xComm.Parameters.AddWithValue("@FAX", WAREHOUSEBindingSource.Current("FAX"))
        xComm.Parameters.AddWithValue("@EMAIL", WAREHOUSEBindingSource.Current("EMAIL"))
        xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", WAREHOUSEBindingSource.Current("EFFECTIVE_START_DATE"))
        xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", Now)
        xComm.Parameters.AddWithValue("@USER_ID_INPUT", WAREHOUSEBindingSource.Current("USER_ID_INPUT"))
        xComm.Parameters.AddWithValue("@INPUT_DATE", WAREHOUSEBindingSource.Current("INPUT_DATE"))
        xComm.Parameters.AddWithValue("@USER_ID_UPDATE", WAREHOUSEBindingSource.Current("USER_ID_UPDATE"))
        xComm.Parameters.AddWithValue("@UPDATE_DATE", WAREHOUSEBindingSource.Current("UPDATE_DATE"))
        xComm.ExecuteNonQuery()

        WAREHOUSEBindingSource.RemoveCurrent()
        GetData()
        'Me.WAREHOUSETableAdapter.Fill(Me.DS_WAREHOUSE.WAREHOUSE)
        WAREHOUSEBindingSource.Position = WAREHOUSEBindingSource.Find("WAREHOUSE_NAME", tmpKey)
        Try
            If WAREHOUSEBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        Catch
        End Try
        tmpChange = False
        chkRummage.Enabled = False
        chkActive.Enabled = False

    End Sub

    Private Sub WAREHOUSE_NAMETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WAREHOUSE_NAMETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub DESCRIPTIONTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DESCRIPTIONTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub WAREHOUSE_ADDRESSTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WAREHOUSE_ADDRESSTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub WAREHOUSE_CITYTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WAREHOUSE_CITYTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub WAREHOUSE_COUNTRYTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WAREHOUSE_COUNTRYTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub PICTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PICTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub PHONETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PHONETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub FAXTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FAXTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub EMAILTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EMAILTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub WAREHOUSEBindingSource_CurrentChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If WAREHOUSE_NAMETextBox.Text <> "" And chkRummage.Enabled = False Then
            If WAREHOUSEBindingSource.Current("WAREHOUSE_ID") Is Nothing Or _
                 IsDBNull(WAREHOUSEBindingSource.Current("WAREHOUSE_ID")) Then
                chkRummage.Checked = False
                Exit Sub
            End If
            If WAREHOUSEBindingSource.Current("WAREHOUSE_ID") = -1 Then
                chkRummage.Checked = True
            Else
                chkRummage.Checked = False
            End If

            If WAREHOUSEBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        End If
    End Sub

    Private Sub cmdSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        frmSearchWarehouse.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        WAREHOUSEBindingSource.Position = WAREHOUSEBindingSource.Find("WAREHOUSE_ID", tmpSearchResult)
    End Sub

    Private Sub BindingNavigatorPositionItem_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BindingNavigatorPositionItem.TextChanged
        Try
            If WAREHOUSE_NAMETextBox.Text <> "" Then
                If WAREHOUSEBindingSource.Current("WAREHOUSE_ID") Is Nothing Or _
                     IsDBNull(WAREHOUSEBindingSource.Current("WAREHOUSE_ID")) Then
                    chkRummage.Checked = False
                    Exit Sub
                End If
                If WAREHOUSEBindingSource.Current("WAREHOUSE_ID") = -1 Then
                    chkRummage.Checked = True
                Else
                    chkRummage.Checked = False
                End If

                If WAREHOUSEBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                    chkActive.Checked = False
                Else
                    chkActive.Checked = True
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        AccessPrivilege(Me.Tag)
    End Sub
End Class