﻿Public Class frmSearchCustomer

    Private Sub frmSearchCustomer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_SALE_ORDER.CUSTOMER' table. You can move, or remove it, as needed.
        Me.CUSTOMERTableAdapter.Fill(Me.DS_SALE_ORDER.CUSTOMER)

        If tmpSearchMode = "Customer - Customer Name" Then
            If Language = "Indonesian" Then
                lblCustomer.Text = "Nama Pelanggan"
            Else
                lblCustomer.Text = "Customer Name"
            End If

        ElseIf tmpSearchMode = "Customer - Customer Code" Then
            If Language = "Indonesian" Then
                lblCustomer.Text = "No Pelanggan"
            Else
                lblCustomer.Text = "Customer No"
            End If
        End If

        CUSTOMER_NAMETextBox.Text = ""
        CUSTOMER_NAMETextBox.Focus()
        tmpSearchResult = ""

        If Language = "Indonesian" Then
            Me.Text = "Pencarian Pelanggan"

            dgvCustomer.Columns("CUSTOMER_NO").HeaderText = "No."
            dgvCustomer.Columns("CUSTOMER_NAME").HeaderText = "Nama Pelanggan"
            dgvCustomer.Columns("CUSTOMER_ADDRESS").HeaderText = "Alamat"
        End If

    End Sub

    Private Sub dgvCustomer_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvCustomer.DoubleClick
        Try
            tmpSearchResult = CUSTOMERBindingSource.Current("CUSTOMER_ID")
        Catch
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data Pelanggan !" & vbCrLf & _
                       "Silahkan input setidaknya satu data Pelanggan !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Customer data !" & vbCrLf & _
                       "Please input at least one Customer data !", MsgBoxStyle.Critical, "DMI Retail")
            End If
        End Try

        Me.Close()
    End Sub

    Private Sub dgvCustomer_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvCustomer.KeyDown
        If e.KeyCode = Keys.Enter Then
            Try
                tmpSearchResult = CUSTOMERBindingSource.Current("CUSTOMER_ID")
            Catch
                If Language = "Indonesian" Then
                    MsgBox("Tidak ada data Pelanggan !" & vbCrLf & _
                           "Silahkan input setidaknya satu data Pelanggan !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("There is no Customer data !" & vbCrLf & _
                           "Please input at least one Customer data !", MsgBoxStyle.Critical, "DMI Retail")
                End If
            End Try
            Me.Close()

        ElseIf e.KeyCode = Keys.Escape Then
            tmpSearchResult = ""
            Me.Close()
        End If
    End Sub

    Private Sub frmSearchCustomer_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
        CUSTOMER_NAMETextBox.Focus()
    End Sub

    Private Sub CUSTOMER_NAMETextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles CUSTOMER_NAMETextBox.KeyPress
        'If Asc(e.KeyChar) > Asc("9") And Asc(e.KeyChar) < Asc("0") Or _
        '       Asc(e.KeyChar) > Asc("z") And Asc(e.KeyChar) < Asc("a") Or _
        '       Asc(e.KeyChar) > Asc("Z") And Asc(e.KeyChar) < Asc("A") Then
        '    e.KeyChar = ""
        'End If
    End Sub

    Private Sub CUSTOMER_NAMETextBox_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CUSTOMER_NAMETextBox.KeyUp
        If e.KeyCode = Keys.Enter Then
            dgvCustomer.Focus()
            Exit Sub
        ElseIf e.KeyCode = Keys.Escape Then
            tmpSearchResult = ""
            Me.Close()
        End If
    End Sub

    Private Sub CUSTOMER_NAMETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CUSTOMER_NAMETextBox.TextChanged
        Try
            If tmpSearchMode = "Customer - Customer Name" Then
                CUSTOMERBindingSource.Filter = "CUSTOMER_NAME LIKE '%" & CUSTOMER_NAMETextBox.Text.ToUpper & "%'"
            ElseIf tmpSearchMode = "Customer - Customer Code" Then
                CUSTOMERBindingSource.Filter = "CUSTOMER_NO LIKE '%" & CUSTOMER_NAMETextBox.Text.ToUpper & "%'"
            End If
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            CUSTOMER_NAMETextBox.Text = ""
            CUSTOMER_NAMETextBox.Focus()
        End Try
        CUSTOMER_NAMETextBox.Focus()
    End Sub
End Class