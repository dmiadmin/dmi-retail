﻿Public Class frmListDailySoldProduct

    Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        txtProductName.Text = ""

        Me.Cursor = Cursors.WaitCursor

        SP_LIST_DAILY_PRODUCT_SOLDTableAdapter.Fill(Me.DS_SALE.SP_LIST_DAILY_PRODUCT_SOLD, dtpSoldProduct.Value)

        Me.Cursor = Cursors.Default

        Dim tmpQty, tmpPrice As Integer
        For n As Integer = 0 To SP_LIST_DAILY_PRODUCT_SOLDDataGridView.RowCount - 1
            tmpQty = tmpQty + SP_LIST_DAILY_PRODUCT_SOLDDataGridView.Item("QUANTITY", n).Value
            tmpQty = tmpQty

            tmpPrice = tmpPrice + SP_LIST_DAILY_PRODUCT_SOLDDataGridView.Item("TOTAL", n).Value
            tmpPrice = tmpPrice
        Next

        If Language = "Indonesian" Then
            lblTotalPrice.Text = "Nominal terjual : " & FormatNumber(tmpPrice, 0)
            lblTotalQty.Text = "Jumlah Produk : " & tmpQty
        Else
            lblTotalPrice.Text = "Total Sold Price : " & FormatNumber(tmpPrice, 0)
            lblTotalQty.Text = "Total Quantity : " & tmpQty
        End If

    End Sub

    Private Sub txtProductName_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtProductName.KeyUp
        If e.KeyCode = Keys.Enter Then SP_LIST_DAILY_PRODUCT_SOLDDataGridView.Focus()
    End Sub


    Private Sub txtProductName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtProductName.TextChanged
        Try
            SP_LIST_DAILY_PRODUCT_SOLDBindingSource.Filter = "PRODUCT_NAME LIKE '%" & txtProductName.Text.ToUpper & "%'"
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtProductName.Text = ""
            txtProductName.Focus()
        End Try
    End Sub

    Private Sub dtpSoldProduct_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpSoldProduct.ValueChanged
        txtProductName.Text = ""
    End Sub

    Private Sub frmListDailySoldProduct_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F12 Then
            cmdReport_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub frmListDailySoldProduct_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim tmpQty, tmpPrice As Integer

        For n As Integer = 0 To SP_LIST_DAILY_PRODUCT_SOLDDataGridView.RowCount - 1

            tmpQty = tmpQty + SP_LIST_DAILY_PRODUCT_SOLDDataGridView.Item("QUANTITY", n).Value
            tmpQty = tmpQty

            tmpPrice = tmpPrice + SP_LIST_DAILY_PRODUCT_SOLDDataGridView.Item("TOTAL", n).Value
            tmpPrice = tmpPrice

        Next

        If Language = "Indonesian" Then
            Me.Text = "Daftar Penjualan Harian"
            cmdGenerate.Text = "Proses"
            lblPeriod.Text = "Periode"
            lblProductName.Text = "Nama Produk"
            cmdReport.Text = "Cetak"

            SP_LIST_DAILY_PRODUCT_SOLDDataGridView.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            SP_LIST_DAILY_PRODUCT_SOLDDataGridView.Columns("SELLING_PRICE").HeaderText = "Harga Jual"
            SP_LIST_DAILY_PRODUCT_SOLDDataGridView.Columns("DISCOUNT").HeaderText = "Discount"
            SP_LIST_DAILY_PRODUCT_SOLDDataGridView.Columns("TOTAL").HeaderText = "Jumlah"

            lblTotalPrice.Text = "Nominal terjual : " & FormatNumber(tmpPrice, 0)
            lblTotalQty.Text = "Jumlah Produk : " & tmpQty
        Else
            lblTotalPrice.Text = "Total Sold Price : " & FormatNumber(tmpPrice, 0)
            lblTotalQty.Text = "Total Quantity : " & tmpQty
        End If
        
    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        Dim RPV As New frmRepDailySales
        RPV.period = dtpSoldProduct.Value
        RPV.ReportViewer1.ZoomPercent = 100
        RPV.WindowState = FormWindowState.Maximized
        RPV.Show()
    End Sub
End Class