﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListSalesmanProduct
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListSalesmanProduct))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.SP_LIST_SALESMAN_PRODUCTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_SALE = New DMI_RETAIL_REPORT.DS_SALE()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.dgvSalesmanProduct = New System.Windows.Forms.DataGridView()
        Me.SALESMAN_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRODUCT_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblSalesman = New System.Windows.Forms.Label()
        Me.cmdSearchName = New System.Windows.Forms.PictureBox()
        Me.cmbSalesmanCode = New System.Windows.Forms.ComboBox()
        Me.SALESMANBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.txtSalesmanName = New System.Windows.Forms.TextBox()
        Me.cmdReport = New System.Windows.Forms.Button()
        Me.lblTo = New System.Windows.Forms.Label()
        Me.lblFrom = New System.Windows.Forms.Label()
        Me.cmdGenerate = New System.Windows.Forms.Button()
        Me.dtpPeriod2 = New System.Windows.Forms.DateTimePicker()
        Me.dtpPeriod1 = New System.Windows.Forms.DateTimePicker()
        Me.SP_LIST_SALESMAN_PRODUCTTableAdapter = New DMI_RETAIL_REPORT.DS_SALETableAdapters.SP_LIST_SALESMAN_PRODUCTTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_REPORT.DS_SALETableAdapters.TableAdapterManager()
        Me.SALESMANTableAdapter = New DMI_RETAIL_REPORT.DS_SALETableAdapters.SALESMANTableAdapter()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        CType(Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator.SuspendLayout()
        CType(Me.SP_LIST_SALESMAN_PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_SALE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSalesmanProduct, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SALESMANBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SP_LIST_SALESMAN_PRODUCTBindingNavigator
        '
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator.AddNewItem = Nothing
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator.BindingSource = Me.SP_LIST_SALESMAN_PRODUCTBindingSource
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator.DeleteItem = Nothing
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator.Name = "SP_LIST_SALESMAN_PRODUCTBindingNavigator"
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator.Size = New System.Drawing.Size(582, 25)
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator.TabIndex = 0
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator.Text = "BindingNavigator1"
        '
        'SP_LIST_SALESMAN_PRODUCTBindingSource
        '
        Me.SP_LIST_SALESMAN_PRODUCTBindingSource.DataMember = "SP_LIST_SALESMAN_PRODUCT"
        Me.SP_LIST_SALESMAN_PRODUCTBindingSource.DataSource = Me.DS_SALE
        '
        'DS_SALE
        '
        Me.DS_SALE.DataSetName = "DS_SALE"
        Me.DS_SALE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'dgvSalesmanProduct
        '
        Me.dgvSalesmanProduct.AllowUserToAddRows = False
        Me.dgvSalesmanProduct.AllowUserToDeleteRows = False
        Me.dgvSalesmanProduct.AutoGenerateColumns = False
        Me.dgvSalesmanProduct.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSalesmanProduct.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSalesmanProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSalesmanProduct.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SALESMAN_NAME, Me.PRODUCT_NAME, Me.TOTAL})
        Me.dgvSalesmanProduct.DataSource = Me.SP_LIST_SALESMAN_PRODUCTBindingSource
        Me.dgvSalesmanProduct.Location = New System.Drawing.Point(12, 186)
        Me.dgvSalesmanProduct.Name = "dgvSalesmanProduct"
        Me.dgvSalesmanProduct.ReadOnly = True
        Me.dgvSalesmanProduct.RowHeadersWidth = 32
        Me.dgvSalesmanProduct.Size = New System.Drawing.Size(557, 220)
        Me.dgvSalesmanProduct.TabIndex = 2
        '
        'SALESMAN_NAME
        '
        Me.SALESMAN_NAME.DataPropertyName = "SALESMAN_NAME"
        Me.SALESMAN_NAME.HeaderText = "Salesman Name"
        Me.SALESMAN_NAME.Name = "SALESMAN_NAME"
        Me.SALESMAN_NAME.ReadOnly = True
        Me.SALESMAN_NAME.Width = 150
        '
        'PRODUCT_NAME
        '
        Me.PRODUCT_NAME.DataPropertyName = "PRODUCT_NAME"
        Me.PRODUCT_NAME.HeaderText = "Product Name"
        Me.PRODUCT_NAME.Name = "PRODUCT_NAME"
        Me.PRODUCT_NAME.ReadOnly = True
        Me.PRODUCT_NAME.Width = 200
        '
        'TOTAL
        '
        Me.TOTAL.DataPropertyName = "TOTAL"
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.TOTAL.DefaultCellStyle = DataGridViewCellStyle2
        Me.TOTAL.HeaderText = "Total Sold"
        Me.TOTAL.Name = "TOTAL"
        Me.TOTAL.ReadOnly = True
        Me.TOTAL.Width = 110
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblSalesman)
        Me.GroupBox2.Controls.Add(Me.cmdSearchName)
        Me.GroupBox2.Controls.Add(Me.cmbSalesmanCode)
        Me.GroupBox2.Controls.Add(Me.txtSalesmanName)
        Me.GroupBox2.Controls.Add(Me.cmdReport)
        Me.GroupBox2.Controls.Add(Me.lblTo)
        Me.GroupBox2.Controls.Add(Me.lblFrom)
        Me.GroupBox2.Controls.Add(Me.cmdGenerate)
        Me.GroupBox2.Controls.Add(Me.dtpPeriod2)
        Me.GroupBox2.Controls.Add(Me.dtpPeriod1)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 28)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(557, 152)
        Me.GroupBox2.TabIndex = 12
        Me.GroupBox2.TabStop = False
        '
        'lblSalesman
        '
        Me.lblSalesman.Location = New System.Drawing.Point(6, 24)
        Me.lblSalesman.Name = "lblSalesman"
        Me.lblSalesman.Size = New System.Drawing.Size(84, 20)
        Me.lblSalesman.TabIndex = 24
        Me.lblSalesman.Text = "Salesman"
        Me.lblSalesman.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cmdSearchName
        '
        Me.cmdSearchName.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchName.Image = CType(resources.GetObject("cmdSearchName.Image"), System.Drawing.Image)
        Me.cmdSearchName.Location = New System.Drawing.Point(487, 20)
        Me.cmdSearchName.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchName.Name = "cmdSearchName"
        Me.cmdSearchName.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearchName.TabIndex = 23
        Me.cmdSearchName.TabStop = False
        '
        'cmbSalesmanCode
        '
        Me.cmbSalesmanCode.DataSource = Me.SALESMANBindingSource
        Me.cmbSalesmanCode.DisplayMember = "SALESMAN_NAME"
        Me.cmbSalesmanCode.FormattingEnabled = True
        Me.cmbSalesmanCode.Location = New System.Drawing.Point(91, 21)
        Me.cmbSalesmanCode.Name = "cmbSalesmanCode"
        Me.cmbSalesmanCode.Size = New System.Drawing.Size(121, 23)
        Me.cmbSalesmanCode.TabIndex = 21
        Me.cmbSalesmanCode.ValueMember = "SALESMAN_ID"
        '
        'SALESMANBindingSource
        '
        Me.SALESMANBindingSource.DataMember = "SALESMAN"
        Me.SALESMANBindingSource.DataSource = Me.DS_SALE
        '
        'txtSalesmanName
        '
        Me.txtSalesmanName.Location = New System.Drawing.Point(218, 21)
        Me.txtSalesmanName.Name = "txtSalesmanName"
        Me.txtSalesmanName.ReadOnly = True
        Me.txtSalesmanName.Size = New System.Drawing.Size(262, 22)
        Me.txtSalesmanName.TabIndex = 22
        '
        'cmdReport
        '
        Me.cmdReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdReport.Image = Global.DMI_RETAIL_REPORT.My.Resources.Resources.Printer_small
        Me.cmdReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdReport.Location = New System.Drawing.Point(461, 122)
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(90, 24)
        Me.cmdReport.TabIndex = 15
        Me.cmdReport.Text = "Print [F12]"
        Me.cmdReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.cmdReport.UseVisualStyleBackColor = True
        '
        'lblTo
        '
        Me.lblTo.AutoSize = True
        Me.lblTo.Location = New System.Drawing.Point(301, 63)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(23, 15)
        Me.lblTo.TabIndex = 8
        Me.lblTo.Text = "To"
        '
        'lblFrom
        '
        Me.lblFrom.AutoSize = True
        Me.lblFrom.Location = New System.Drawing.Point(71, 63)
        Me.lblFrom.Name = "lblFrom"
        Me.lblFrom.Size = New System.Drawing.Size(79, 15)
        Me.lblFrom.TabIndex = 7
        Me.lblFrom.Text = "From Period"
        '
        'cmdGenerate
        '
        Me.cmdGenerate.Image = CType(resources.GetObject("cmdGenerate.Image"), System.Drawing.Image)
        Me.cmdGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdGenerate.Location = New System.Drawing.Point(229, 97)
        Me.cmdGenerate.Name = "cmdGenerate"
        Me.cmdGenerate.Size = New System.Drawing.Size(99, 39)
        Me.cmdGenerate.TabIndex = 5
        Me.cmdGenerate.Text = "Generate"
        Me.cmdGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdGenerate.UseVisualStyleBackColor = True
        '
        'dtpPeriod2
        '
        Me.dtpPeriod2.CustomFormat = "dd-MMM-yyyy"
        Me.dtpPeriod2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPeriod2.Location = New System.Drawing.Point(330, 60)
        Me.dtpPeriod2.Name = "dtpPeriod2"
        Me.dtpPeriod2.Size = New System.Drawing.Size(120, 22)
        Me.dtpPeriod2.TabIndex = 4
        '
        'dtpPeriod1
        '
        Me.dtpPeriod1.CustomFormat = "dd-MMM-yyyy"
        Me.dtpPeriod1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPeriod1.Location = New System.Drawing.Point(156, 60)
        Me.dtpPeriod1.Name = "dtpPeriod1"
        Me.dtpPeriod1.Size = New System.Drawing.Size(123, 22)
        Me.dtpPeriod1.TabIndex = 3
        '
        'SP_LIST_SALESMAN_PRODUCTTableAdapter
        '
        Me.SP_LIST_SALESMAN_PRODUCTTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.CUSTOMERTableAdapter = Nothing
        Me.TableAdapterManager.PAYMENT_METHODTableAdapter = Nothing
        Me.TableAdapterManager.PRODUCTTableAdapter = Nothing
        Me.TableAdapterManager.RECEIVABLETableAdapter = Nothing
        Me.TableAdapterManager.SALE_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.SALESMANTableAdapter = Nothing
        Me.TableAdapterManager.SALETableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_REPORT.DS_SALETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.WAREHOUSETableAdapter = Nothing
        '
        'SALESMANTableAdapter
        '
        Me.SALESMANTableAdapter.ClearBeforeFill = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(28, 416)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 15)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Label1"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(360, 416)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 15)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Label2"
        '
        'frmListSalesmanProduct
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(582, 448)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.dgvSalesmanProduct)
        Me.Controls.Add(Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmListSalesmanProduct"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "List Product Sold by Salesman"
        Me.Text = "List Product Sold by Salesman"
        CType(Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator.ResumeLayout(False)
        Me.SP_LIST_SALESMAN_PRODUCTBindingNavigator.PerformLayout()
        CType(Me.SP_LIST_SALESMAN_PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_SALE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSalesmanProduct, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SALESMANBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DS_SALE As DMI_RETAIL_REPORT.DS_SALE
    Friend WithEvents SP_LIST_SALESMAN_PRODUCTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_LIST_SALESMAN_PRODUCTTableAdapter As DMI_RETAIL_REPORT.DS_SALETableAdapters.SP_LIST_SALESMAN_PRODUCTTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_REPORT.DS_SALETableAdapters.TableAdapterManager
    Friend WithEvents SP_LIST_SALESMAN_PRODUCTBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents dgvSalesmanProduct As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblSalesman As System.Windows.Forms.Label
    Friend WithEvents cmdSearchName As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSalesmanCode As System.Windows.Forms.ComboBox
    Friend WithEvents txtSalesmanName As System.Windows.Forms.TextBox
    Friend WithEvents cmdReport As System.Windows.Forms.Button
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents lblFrom As System.Windows.Forms.Label
    Friend WithEvents cmdGenerate As System.Windows.Forms.Button
    Friend WithEvents dtpPeriod2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpPeriod1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents SALESMAN_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRODUCT_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALESMANBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SALESMANTableAdapter As DMI_RETAIL_REPORT.DS_SALETableAdapters.SALESMANTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
