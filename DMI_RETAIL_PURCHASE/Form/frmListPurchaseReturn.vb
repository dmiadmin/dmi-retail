﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frmListPurchaseReturn
    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Dim xDreader As SqlDataReader

    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub

    'Private Sub frmListPurchaseReturn_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
    '    If e.KeyCode = Keys.F12 Then
    '        cmdReport_Click(Nothing, Nothing)
    '    End If
    'End Sub

    Private Sub frmListPurchaseReturn_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        dtpPeriod.Value = DateSerial(Today.Year, Today.Month, 1)
        dtpPeriod2.Value = DateSerial(Today.Year, Today.Month + 1, 0)

        If Language = "Indonesian" Then
            Me.Text = "Daftar Retur Pembelian"
            lblInvoiceNo.Text = "No Faktur"
            cmdReport.Text = "Cetak"

            lblNoOfItem.Text = "Jumlah Item : " & dgvLIST_PURCHASE_RETURN.RowCount
        Else
            lblNoOfItem.Text = "No Of Transaction(s) : " & dgvLIST_PURCHASE_RETURN.RowCount
        End If

        txtInvoiceNo.Focus()

    End Sub

    Private Sub txtInvoiceNo_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtInvoiceNo.KeyUp
        If e.KeyCode = Keys.Enter Then
            dgvLIST_PURCHASE_RETURN.Focus()
        End If
    End Sub

    Private Sub txtInvoiceNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInvoiceNo.TextChanged
        Try
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = "DECLARE	@return_value int" & vbCrLf & _
                  "EXEC 	@return_value = [dbo].[SP_LIST_PURCHASE_RETURN]" & vbCrLf & _
                  "		    @PERIOD1 = N'" & dtpPeriod.Value & "'," & vbCrLf & _
                  "		    @PERIOD2 = N'" & dtpPeriod2.Value & "'," & vbCrLf & _
                  "		    @INVOICE_NO = N'" & txtInvoiceNo.Text & "'" & vbCrLf & _
                  "SELECT	'Return Value' = @return_value"

            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.dgvLIST_PURCHASE_RETURN.DataSource = dt
            If Language = "Indonesian" Then
                lblNoOfItem.Text = "Jumlah Transaksi : " & dgvLIST_PURCHASE_RETURN.RowCount
            Else
                lblNoOfItem.Text = "No Of Transaction(s) : " & dgvLIST_PURCHASE_RETURN.RowCount
            End If
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtInvoiceNo.Text = ""
            txtInvoiceNo.Focus()
        End Try
    End Sub

    'Private Sub dgvLIST_PURCHASE_RETURN_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLIST_PURCHASE_RETURN.DoubleClick
    '    Try
    '        With frmEntryReturnPurchase
    '            .RETURN_NOTextBox.Text = SP_LIST_PURCHASE_RETURNBindingSource.Current("RETURN_NO")
    '            .txtPaymentMethod.Text = SP_LIST_PURCHASE_RETURNBindingSource.Current("PAYMENT_METHOD")
    '            .txtDate.Text = Format(SP_LIST_PURCHASE_RETURNBindingSource.Current("PURCHASE_DATE"), "dd-MMM-yyyy")
    '            .txtReturnDate.Text = Format(SP_LIST_PURCHASE_RETURNBindingSource.Current("RETURN_DATE"), "dd-MMM-yyyy")
    '            .txtVendor.Text = SP_LIST_PURCHASE_RETURNBindingSource.Current("VENDOR_NAME")
    '            .txtReceiptNo.Text = SP_LIST_PURCHASE_RETURNBindingSource.Current("RECEIPT_NO")
    '            .tmpWarehouseName = SP_LIST_PURCHASE_RETURNBindingSource.Current("WAREHOUSE_NAME")
    '            .RETURN_TYPEComboBox.Text = SP_LIST_PURCHASE_RETURNBindingSource.Current("RETURN_TYPE")
    '            .txtDescription.Text = SP_LIST_PURCHASE_RETURNBindingSource.Current("DESCRIPTION")
    '            .tmpSavemode = "Update"
    '            .getPurchaseId = SP_LIST_PURCHASE_RETURNBindingSource.Current("RECEIPT_ID")
    '            If IsDBNull(SP_LIST_PURCHASE_RETURNBindingSource.Current("QUANTITY")) Then
    '                .getQuantity = 0
    '            Else
    '                .getQuantity = SP_LIST_PURCHASE_RETURNBindingSource.Current("QUANTITY")
    '            End If
    '        End With
    '        frmEntryReturnPurchase.ShowDialog(Me)
    '        frmEntryReturnPurchase.Close()
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Private Sub cmdGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click

        ' Me.SP_LIST_PURCHASE_RETURNTableAdapter.Fill(Me.DS_RETURN_PURCHASE.SP_LIST_PURCHASE_RETURN, dtpPeriod.Value, dtpPeriod2.Value)

        Me.Cursor = Cursors.WaitCursor
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC 	@return_value = [dbo].[SP_LIST_PURCHASE_RETURN]" & vbCrLf & _
              "		    @PERIOD1 = N'" & dtpPeriod.Value & "'," & vbCrLf & _
              "		    @PERIOD2 = N'" & dtpPeriod2.Value & "'," & vbCrLf & _
              "		    @INVOICE_NO = N'" & txtInvoiceNo.Text & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.dgvLIST_PURCHASE_RETURN.DataSource = dt
        Me.Cursor = Cursors.Default

        If Language = "Indonesian" Then
            dgvLIST_PURCHASE_RETURN.Columns("PURCHASE_RETURN_ID").Visible = False
            dgvLIST_PURCHASE_RETURN.Columns("RECEIPT_ID").Visible = False
            dgvLIST_PURCHASE_RETURN.Columns("PURCHASE_DATE").Visible = False
            dgvLIST_PURCHASE_RETURN.Columns("VENDOR_NAME").Visible = False
            dgvLIST_PURCHASE_RETURN.Columns("REUSABLE_STATUS").Visible = False
            dgvLIST_PURCHASE_RETURN.Columns("PAYMENT_METHOD").Visible = False
            dgvLIST_PURCHASE_RETURN.Columns("WAREHOUSE_ID").Visible = False
            dgvLIST_PURCHASE_RETURN.Columns("REP_PERIOD1").Visible = False
            dgvLIST_PURCHASE_RETURN.Columns("REP_PERIOD2").Visible = False
            dgvLIST_PURCHASE_RETURN.Columns("RETURN_NO").Width = 50
            dgvLIST_PURCHASE_RETURN.Columns("RETURN_DATE").Width = 80
            dgvLIST_PURCHASE_RETURN.Columns("RECEIPT_NO").Width = 75
            dgvLIST_PURCHASE_RETURN.Columns("TOTAL").Width = 65
            dgvLIST_PURCHASE_RETURN.Columns("NO_OF_ITEM").Width = 50
            dgvLIST_PURCHASE_RETURN.Columns("QUANTITY").Width = 50
            dgvLIST_PURCHASE_RETURN.Columns("RETURN_TYPE").Width = 80
            dgvLIST_PURCHASE_RETURN.Columns("DESCRIPTION").Width = 125
            dgvLIST_PURCHASE_RETURN.Columns("RETURN_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvLIST_PURCHASE_RETURN.Columns("RETURN_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_PURCHASE_RETURN.Columns("NO_OF_ITEM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_PURCHASE_RETURN.Columns("TOTAL").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_PURCHASE_RETURN.Columns("QUANTITY").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_PURCHASE_RETURN.Columns("RETURN_NO").HeaderText = "No Retur"
            dgvLIST_PURCHASE_RETURN.Columns("RETURN_DATE").HeaderText = "Tanggal Retur"
            dgvLIST_PURCHASE_RETURN.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            dgvLIST_PURCHASE_RETURN.Columns("RETURN_TYPE").HeaderText = "Tipe Retur"
            dgvLIST_PURCHASE_RETURN.Columns("TOTAL").HeaderText = "Total"
            dgvLIST_PURCHASE_RETURN.Columns("NO_OF_ITEM").HeaderText = "Jumlah Item"
            dgvLIST_PURCHASE_RETURN.Columns("QUANTITY").HeaderText = "Jumlah Retur"
            dgvLIST_PURCHASE_RETURN.Columns("DESCRIPTION").HeaderText = "Keterangan"
            dgvLIST_PURCHASE_RETURN.Columns("WAREHOUSE_NAME").HeaderText = "Gudang"
            lblNoOfItem.Text = "Jumlah Item : " & dgvLIST_PURCHASE_RETURN.RowCount
        Else
            dgvLIST_PURCHASE_RETURN.Columns("PURCHASE_RETURN_ID").Visible = False
            dgvLIST_PURCHASE_RETURN.Columns("RECEIPT_ID").Visible = False
            dgvLIST_PURCHASE_RETURN.Columns("PURCHASE_DATE").Visible = False
            dgvLIST_PURCHASE_RETURN.Columns("VENDOR_NAME").Visible = False
            dgvLIST_PURCHASE_RETURN.Columns("REUSABLE_STATUS").Visible = False
            dgvLIST_PURCHASE_RETURN.Columns("PAYMENT_METHOD").Visible = False
            dgvLIST_PURCHASE_RETURN.Columns("WAREHOUSE_ID").Visible = False
            dgvLIST_PURCHASE_RETURN.Columns("REP_PERIOD1").Visible = False
            dgvLIST_PURCHASE_RETURN.Columns("REP_PERIOD2").Visible = False
            dgvLIST_PURCHASE_RETURN.Columns("RETURN_NO").Width = 50
            dgvLIST_PURCHASE_RETURN.Columns("RETURN_DATE").Width = 80
            dgvLIST_PURCHASE_RETURN.Columns("RECEIPT_NO").Width = 75
            dgvLIST_PURCHASE_RETURN.Columns("TOTAL").Width = 65
            dgvLIST_PURCHASE_RETURN.Columns("NO_OF_ITEM").Width = 50
            dgvLIST_PURCHASE_RETURN.Columns("QUANTITY").Width = 50
            dgvLIST_PURCHASE_RETURN.Columns("RETURN_TYPE").Width = 80
            dgvLIST_PURCHASE_RETURN.Columns("DESCRIPTION").Width = 125
            dgvLIST_PURCHASE_RETURN.Columns("RETURN_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvLIST_PURCHASE_RETURN.Columns("RETURN_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_PURCHASE_RETURN.Columns("NO_OF_ITEM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_PURCHASE_RETURN.Columns("TOTAL").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_PURCHASE_RETURN.Columns("QUANTITY").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_PURCHASE_RETURN.Columns("RETURN_NO").HeaderText = "Return No"
            dgvLIST_PURCHASE_RETURN.Columns("RETURN_DATE").HeaderText = "Return Date"
            dgvLIST_PURCHASE_RETURN.Columns("RECEIPT_NO").HeaderText = "Receipt No"
            dgvLIST_PURCHASE_RETURN.Columns("RETURN_TYPE").HeaderText = "Return Type"
            dgvLIST_PURCHASE_RETURN.Columns("TOTAL").HeaderText = "Total"
            dgvLIST_PURCHASE_RETURN.Columns("NO_OF_ITEM").HeaderText = "No Of Item"
            dgvLIST_PURCHASE_RETURN.Columns("QUANTITY").HeaderText = "Qty"
            dgvLIST_PURCHASE_RETURN.Columns("DESCRIPTION").HeaderText = "Description"
            dgvLIST_PURCHASE_RETURN.Columns("WAREHOUSE_NAME").HeaderText = "Warehouse"
            lblNoOfItem.Text = "No Of Transaction(s) : " & dgvLIST_PURCHASE_RETURN.RowCount
        End If
    End Sub

    'Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
    '    Dim objf As New frmRepReturnPurchase
    '    objf.date1 = dtpPeriod.Value
    '    objf.date2 = dtpPeriod2.Value
    '    objf.purchaseNo = txtInvoiceNo.Text
    '    objf.ReportViewer1.ShowRefreshButton = False
    '    objf.ReportViewer1.ZoomPercent = 100
    '    objf.WindowState = FormWindowState.Maximized
    '    objf.Show()
    'End Sub

    Private Sub dgvLIST_PURCHASE_RETURN_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLIST_PURCHASE_RETURN.CellDoubleClick
        Try
            With frmEntryReturnPurchase
                .getPurchaseReturnId = dgvLIST_PURCHASE_RETURN.Item("PURCHASE_RETURN_ID", e.RowIndex).Value
                .getNoOfItem = dgvLIST_PURCHASE_RETURN.Item("NO_OF_ITEM", e.RowIndex).Value
                .RETURN_NOTextBox.Text = dgvLIST_PURCHASE_RETURN.Item("RETURN_NO", e.RowIndex).Value
                .txtPaymentMethod.Text = dgvLIST_PURCHASE_RETURN.Item("PAYMENT_METHOD", e.RowIndex).Value
                .txtDate.Text = Format(dgvLIST_PURCHASE_RETURN.Item("PURCHASE_DATE", e.RowIndex).Value, "dd-MMM-yyyy")
                .txtReturnDate.Text = Format(dgvLIST_PURCHASE_RETURN.Item("RETURN_DATE", e.RowIndex).Value, "dd-MMM-yyyy")
                .txtVendor.Text = dgvLIST_PURCHASE_RETURN.Item("VENDOR_NAME", e.RowIndex).Value
                .txtReceiptNo.Text = dgvLIST_PURCHASE_RETURN.Item("RECEIPT_NO", e.RowIndex).Value
                .RETURN_TYPEComboBox.Text = dgvLIST_PURCHASE_RETURN.Item("RETURN_TYPE", e.RowIndex).Value
                .txtDescription.Text = dgvLIST_PURCHASE_RETURN.Item("DESCRIPTION", e.RowIndex).Value
                .tmpSavemode = "Update"
                .getPurchaseId = dgvLIST_PURCHASE_RETURN.Item("RECEIPT_ID", e.RowIndex).Value
                If IsDBNull(dgvLIST_PURCHASE_RETURN.Item("QUANTITY", e.RowIndex).Value) Then
                    .getQuantity = 0
                Else
                    .getQuantity = dgvLIST_PURCHASE_RETURN.Item("QUANTITY", e.RowIndex).Value
                End If
                If dgvLIST_PURCHASE_RETURN.Item("REUSABLE_STATUS", e.RowIndex).Value = "0" Then
                    .REUSABLE_STATUSCheckBox.Checked = False
                Else
                    .REUSABLE_STATUSCheckBox.Checked = True
                End If
                If dgvLIST_PURCHASE_RETURN.Item("WAREHOUSE_NAME", e.RowIndex).Value.ToString = "" Then
                    .tmpWarehouseName = ""
                Else
                    .tmpWarehouseName = dgvLIST_PURCHASE_RETURN.Item("WAREHOUSE_NAME", e.RowIndex).Value
                End If

            End With
            frmEntryReturnPurchase.ShowDialog(Me)
            frmEntryReturnPurchase.Close()
        Catch ex As Exception

        End Try
    End Sub
End Class