﻿Imports System.IO

Public Class frmTerminalConfig

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        If MsgBox("DMI Retail application has not been configured yet." & vbCrLf & _
                  "Do you really want to cancel the Configuration ?", MsgBoxStyle.YesNo, "DMI Retail") = _
              MsgBoxResult.No Then Exit Sub

        Me.Close()
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        If Not ValidateAllComboBox(Me) Then cmbLanguage.Text = ""
        If txtServerName.Text = "" Or txtDBName.Text = "" Or cmbLanguage.Text = "" Then
            MsgBox("Please enter all fields !", MsgBoxStyle.Critical, "DMI Retail")
            Exit Sub
        End If

        tmpConfig(0) = "[Language]=" & cmbLanguage.Text
        tmpConfig(1) = "[Server Name]=" & txtServerName.Text
        tmpConfig(2) = "[Database Name]=" & txtDBName.Text
        File.WriteAllLines(Application.StartupPath & "\Config.inf", tmpConfig)

        MsgBox("Configuration has been updated." & vbCrLf & "Please launch the application again.", _
               MsgBoxStyle.Information, "DMI Retail")
        Me.Close()
    End Sub

End Class