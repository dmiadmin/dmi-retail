﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frmListAccount
    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Dim xDS_Akun As New DataSet
    Dim xdReader As SqlDataReader
    Dim tmpAccountId As Integer

    Private Sub ACCOUNTBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xDS_Akun As New DataSet
        Dim bs As New BindingSource
        With xComm
            .Connection = xConn
            .CommandText = "SELECT ACCOUNT_ID, ACCOUNT_NUMBER, ACCOUNT_NAME, ACCOUNT_TYPE," & _
                           "DESCRIPTION, EFFECTIVE_START_DATE, EFFECTIVE_END_DATE " & _
                           "FROM ACCOUNT ORDER BY ACCOUNT_NAME"
            .CommandType = CommandType.Text
        End With
        xAdoAdapter.SelectCommand = xComm
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        Me.xAdoAdapter.Fill(xDS_Akun, "ACCOUNT")
        Me.dgvListAccount.DataSource = xDS_Akun.Tables("ACCOUNT")
        bs.DataSource = xDS_Akun.Tables("ACCOUNT")
        'ACCOUNTBindingNavigator.BindingSource = bs
        'Me.Validate()
        'Me.bs.EndEdit()
        'Me.TableAdapterManager.UpdateAll(Me.DS_ACCOUNT)

    End Sub

    Private Sub frmListAccount_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F12 Then
            cmdReport_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub frmListAccount_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim xDS_Akun As New DataSet
        With xComm
            .Connection = xConn
            .CommandText = "SELECT ACCOUNT_ID, ACCOUNT_NUMBER, ACCOUNT_NAME, ACCOUNT_TYPE, " & _
                           "DESCRIPTION, EFFECTIVE_START_DATE, EFFECTIVE_END_DATE " & _
                           "FROM ACCOUNT ORDER BY ACCOUNT_NAME"
            .CommandType = CommandType.Text
        End With
        xAdoAdapter.SelectCommand = xComm
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        Me.xAdoAdapter.Fill(xDS_Akun, "ACCOUNT")
        Me.dgvListAccount.DataSource = xDS_Akun.Tables("ACCOUNT")
        Label2.Visible = True
        If Language = "Indonesian" Then
            Me.Text = "Daftar Akun"
            Label1.Text = "Nama Akun"
            cmdReport.Text = "Cetak"
            CheckBox1.Text = "Tidak Terhapus"
            dgvListAccount.Columns("ACCOUNT_ID").Visible = False
            dgvListAccount.Columns("EFFECTIVE_START_DATE").Visible = False
            dgvListAccount.Columns("EFFECTIVE_END_DATE").Visible = False
            dgvListAccount.Columns("ACCOUNT_NAME").HeaderText = "Nama Akun"
            dgvListAccount.Columns("ACCOUNT_NAME").Width = 150
            dgvListAccount.Columns("ACCOUNT_TYPE").HeaderText = "Type"
            dgvListAccount.Columns("ACCOUNT_TYPE").Width = 100
            dgvListAccount.Columns("ACCOUNT_NUMBER").HeaderText = "Nomor Akun"
            dgvListAccount.Columns("ACCOUNT_NUMBER").Width = 125
            dgvListAccount.Columns("DESCRIPTION").HeaderText = "Keterangan"
            Label2.Text = "Jumlah Barang : " & dgvListAccount.RowCount
        Else
            dgvListAccount.Columns("ACCOUNT_ID").Visible = False
            dgvListAccount.Columns("EFFECTIVE_START_DATE").Visible = False
            dgvListAccount.Columns("EFFECTIVE_END_DATE").Visible = False
            dgvListAccount.Columns("ACCOUNT_NAME").HeaderText = "Account Name"
            dgvListAccount.Columns("ACCOUNT_NAME").Width = 150
            dgvListAccount.Columns("ACCOUNT_TYPE").HeaderText = "Type"
            dgvListAccount.Columns("ACCOUNT_TYPE").Width = 100
            dgvListAccount.Columns("ACCOUNT_NUMBER").HeaderText = "Account No"
            dgvListAccount.Columns("ACCOUNT_NUMBER").Width = 125
            dgvListAccount.Columns("DESCRIPTION").HeaderText = "Description"
            Label2.Text = "No Of Item : " & dgvListAccount.RowCount
        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        If CheckBox1.Checked = True Then
            Dim xDS_Akun As New DataSet
            With xComm
                .Connection = xConn
                .CommandText = "SELECT ACCOUNT_ID, ACCOUNT_NAME, ACCOUNT_NUMBER, ACCOUNT_TYPE, " & _
                               "DESCRIPTION, EFFECTIVE_START_DATE, EFFECTIVE_END_DATE FROM " & _
                               "ACCOUNT WHERE ACCOUNT_NAME LIKE '%" & TextBox1.Text.ToUpper & _
                               "%' AND EFFECTIVE_END_DATE > GETDATE() ORDER BY ACCOUNT_NAME"
                .CommandType = CommandType.Text
            End With
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            Me.xAdoAdapter.Fill(xDS_Akun, "ACCOUNT")
            Me.dgvListAccount.DataSource = xDS_Akun.Tables("ACCOUNT")
        Else
            Dim xDS_Akun As New DataSet
            With xComm
                .Connection = xConn
                .CommandText = "SELECT ACCOUNT_ID, ACCOUNT_NAME, ACCOUNT_NUMBER, ACCOUNT_TYPE, " & _
                               "DESCRIPTION, EFFECTIVE_START_DATE, EFFECTIVE_END_DATE FROM ACCOUNT " & _
                               "WHERE ACCOUNT_NAME LIKE '%" & TextBox1.Text.ToUpper & "%' ORDER BY ACCOUNT_NAME"
                .CommandType = CommandType.Text
            End With
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            Me.xAdoAdapter.Fill(xDS_Akun, "ACCOUNT")
            Me.dgvListAccount.DataSource = xDS_Akun.Tables("ACCOUNT")
        End If

        If Language = "Indonesian" Then
            Label2.Text = "Jumlah Barang : " & dgvListAccount.RowCount
        Else
            Label2.Text = "No Of Item : " & dgvListAccount.RowCount
        End If
    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        Dim RPV As New frmRepAccount
        RPV.WindowState = FormWindowState.Maximized
        RPV.Show()
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            Dim xDS_Akun As New DataSet
            With xComm
                .Connection = xConn
                .CommandText = "SELECT ACCOUNT_ID, ACCOUNT_NAME, ACCOUNT_NUMBER, ACCOUNT_TYPE, " & _
                               "DESCRIPTION, EFFECTIVE_START_DATE, EFFECTIVE_END_DATE FROM ACCOUNT " & _
                               "WHERE ACCOUNT_NAME LIKE '%" & TextBox1.Text.ToUpper & _
                               "%' AND EFFECTIVE_END_DATE > GETDATE() ORDER BY ACCOUNT_NAME"
                .CommandType = CommandType.Text
            End With
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            Me.xAdoAdapter.Fill(xDS_Akun, "ACCOUNT")
            Me.dgvListAccount.DataSource = xDS_Akun.Tables("ACCOUNT")
        Else
            Dim xDS_Akun As New DataSet
            With xComm
                .Connection = xConn
                .CommandText = "SELECT ACCOUNT_ID, ACCOUNT_NAME, ACCOUNT_NUMBER, ACCOUNT_TYPE, " & _
                               "DESCRIPTION, EFFECTIVE_START_DATE, EFFECTIVE_END_DATE FROM ACCOUNT " & _
                               "WHERE ACCOUNT_NAME LIKE '%" & TextBox1.Text.ToUpper & "%' ORDER BY ACCOUNT_NAME"
                .CommandType = CommandType.Text
            End With
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            Me.xAdoAdapter.Fill(xDS_Akun, "ACCOUNT")
            Me.dgvListAccount.DataSource = xDS_Akun.Tables("ACCOUNT")
        End If
        
        If Language = "Indonesian" Then
            Label2.Text = "Jumlah Akun : " & dgvListAccount.RowCount
        Else
            Label2.Text = "No Of Account : " & dgvListAccount.RowCount
        End If

    End Sub

    Private Sub dgvListAccount_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListAccount.CellDoubleClick
        Try
            tmpAccountId = dgvListAccount.Item("ACCOUNT_ID", e.RowIndex).Value
            Cursor = Cursors.Default
            frmEntryAccount.MdiParent = ObjFormMain
            frmEntryAccount.Show()
            frmEntryAccount.BringToFront()
            tmpForm = frmEntryAccount
            tmpRecent = New frmEntryAccount
            frmEntryAccount.ACCOUNTBindingSource.Position = frmEntryAccount.ACCOUNTBindingSource.Find("ACCOUNT_ID", tmpAccountId)
            Me.Close()
        Catch ex As Exception

        End Try
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        AccessPrivilege(Me.Tag)
    End Sub
End Class