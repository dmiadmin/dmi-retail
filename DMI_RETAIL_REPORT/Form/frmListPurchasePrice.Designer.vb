﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListPurchasePrice
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListPurchasePrice))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DS_PURCHASE = New DMI_RETAIL_REPORT.DS_PURCHASE()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdSearchName = New System.Windows.Forms.PictureBox()
        Me.PRODUCT_CODEComboBox = New System.Windows.Forms.ComboBox()
        Me.SP_LIST_PRODUCT_GOODSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PRODUCT_NAMETextBox = New System.Windows.Forms.TextBox()
        Me.lblProduct = New System.Windows.Forms.Label()
        Me.cmdReport = New System.Windows.Forms.Button()
        Me.lblTo = New System.Windows.Forms.Label()
        Me.lblFrom = New System.Windows.Forms.Label()
        Me.cmdGenerate = New System.Windows.Forms.Button()
        Me.dtpPeriod2 = New System.Windows.Forms.DateTimePicker()
        Me.dtpPeriod1 = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SP_LIST_PRODUCT_GOODSTableAdapter = New DMI_RETAIL_REPORT.DS_PURCHASETableAdapters.SP_LIST_PRODUCT_GOODSTableAdapter()
        Me.SP_LIST_PURCHASE_PRICEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_LIST_PURCHASE_PRICETableAdapter = New DMI_RETAIL_REPORT.DS_PURCHASETableAdapters.SP_LIST_PURCHASE_PRICETableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_REPORT.DS_PURCHASETableAdapters.TableAdapterManager()
        Me.dgvPurchasePrice = New System.Windows.Forms.DataGridView()
        Me.TRANSACTION_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TRANSACTION_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TRANSACTION_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRODUCT_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRICE_PER_UNIT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QUANTITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DISCOUNT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WAREHOUSE_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DS_PURCHASE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_LIST_PRODUCT_GOODSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_LIST_PURCHASE_PRICEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPurchasePrice, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DS_PURCHASE
        '
        Me.DS_PURCHASE.DataSetName = "DS_PURCHASE"
        Me.DS_PURCHASE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdSearchName)
        Me.GroupBox2.Controls.Add(Me.PRODUCT_CODEComboBox)
        Me.GroupBox2.Controls.Add(Me.PRODUCT_NAMETextBox)
        Me.GroupBox2.Controls.Add(Me.lblProduct)
        Me.GroupBox2.Controls.Add(Me.cmdReport)
        Me.GroupBox2.Controls.Add(Me.lblTo)
        Me.GroupBox2.Controls.Add(Me.lblFrom)
        Me.GroupBox2.Controls.Add(Me.cmdGenerate)
        Me.GroupBox2.Controls.Add(Me.dtpPeriod2)
        Me.GroupBox2.Controls.Add(Me.dtpPeriod1)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 30)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(766, 141)
        Me.GroupBox2.TabIndex = 12
        Me.GroupBox2.TabStop = False
        '
        'cmdSearchName
        '
        Me.cmdSearchName.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchName.Image = CType(resources.GetObject("cmdSearchName.Image"), System.Drawing.Image)
        Me.cmdSearchName.Location = New System.Drawing.Point(558, 20)
        Me.cmdSearchName.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchName.Name = "cmdSearchName"
        Me.cmdSearchName.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearchName.TabIndex = 22
        Me.cmdSearchName.TabStop = False
        '
        'PRODUCT_CODEComboBox
        '
        Me.PRODUCT_CODEComboBox.DataSource = Me.SP_LIST_PRODUCT_GOODSBindingSource
        Me.PRODUCT_CODEComboBox.DisplayMember = "PRODUCT_CODE"
        Me.PRODUCT_CODEComboBox.FormattingEnabled = True
        Me.PRODUCT_CODEComboBox.Location = New System.Drawing.Point(241, 21)
        Me.PRODUCT_CODEComboBox.Name = "PRODUCT_CODEComboBox"
        Me.PRODUCT_CODEComboBox.Size = New System.Drawing.Size(109, 23)
        Me.PRODUCT_CODEComboBox.TabIndex = 20
        Me.PRODUCT_CODEComboBox.Tag = "M"
        Me.PRODUCT_CODEComboBox.ValueMember = "PRODUCT_ID"
        '
        'SP_LIST_PRODUCT_GOODSBindingSource
        '
        Me.SP_LIST_PRODUCT_GOODSBindingSource.DataMember = "SP_LIST_PRODUCT_GOODS"
        Me.SP_LIST_PRODUCT_GOODSBindingSource.DataSource = Me.DS_PURCHASE
        '
        'PRODUCT_NAMETextBox
        '
        Me.PRODUCT_NAMETextBox.Location = New System.Drawing.Point(356, 21)
        Me.PRODUCT_NAMETextBox.Name = "PRODUCT_NAMETextBox"
        Me.PRODUCT_NAMETextBox.ReadOnly = True
        Me.PRODUCT_NAMETextBox.Size = New System.Drawing.Size(195, 22)
        Me.PRODUCT_NAMETextBox.TabIndex = 21
        '
        'lblProduct
        '
        Me.lblProduct.Location = New System.Drawing.Point(147, 24)
        Me.lblProduct.Name = "lblProduct"
        Me.lblProduct.Size = New System.Drawing.Size(83, 21)
        Me.lblProduct.TabIndex = 17
        Me.lblProduct.Text = "Product"
        Me.lblProduct.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cmdReport
        '
        Me.cmdReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdReport.Image = Global.DMI_RETAIL_REPORT.My.Resources.Resources.Printer_small
        Me.cmdReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdReport.Location = New System.Drawing.Point(670, 111)
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(90, 24)
        Me.cmdReport.TabIndex = 16
        Me.cmdReport.Text = "Print [F12]"
        Me.cmdReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.cmdReport.UseVisualStyleBackColor = True
        '
        'lblTo
        '
        Me.lblTo.AutoSize = True
        Me.lblTo.Location = New System.Drawing.Point(392, 59)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(23, 15)
        Me.lblTo.TabIndex = 8
        Me.lblTo.Text = "To"
        '
        'lblFrom
        '
        Me.lblFrom.AutoSize = True
        Me.lblFrom.Location = New System.Drawing.Point(193, 59)
        Me.lblFrom.Name = "lblFrom"
        Me.lblFrom.Size = New System.Drawing.Size(44, 15)
        Me.lblFrom.TabIndex = 7
        Me.lblFrom.Text = "Period"
        '
        'cmdGenerate
        '
        Me.cmdGenerate.Image = CType(resources.GetObject("cmdGenerate.Image"), System.Drawing.Image)
        Me.cmdGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdGenerate.Location = New System.Drawing.Point(338, 90)
        Me.cmdGenerate.Name = "cmdGenerate"
        Me.cmdGenerate.Size = New System.Drawing.Size(99, 39)
        Me.cmdGenerate.TabIndex = 5
        Me.cmdGenerate.Text = "Generate"
        Me.cmdGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdGenerate.UseVisualStyleBackColor = True
        '
        'dtpPeriod2
        '
        Me.dtpPeriod2.CustomFormat = "dd-MMM-yyyy"
        Me.dtpPeriod2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPeriod2.Location = New System.Drawing.Point(423, 56)
        Me.dtpPeriod2.Name = "dtpPeriod2"
        Me.dtpPeriod2.Size = New System.Drawing.Size(128, 22)
        Me.dtpPeriod2.TabIndex = 4
        '
        'dtpPeriod1
        '
        Me.dtpPeriod1.CustomFormat = "dd-MMM-yyyy"
        Me.dtpPeriod1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPeriod1.Location = New System.Drawing.Point(241, 56)
        Me.dtpPeriod1.Name = "dtpPeriod1"
        Me.dtpPeriod1.Size = New System.Drawing.Size(128, 22)
        Me.dtpPeriod1.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(37, 438)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 15)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Label1"
        '
        'SP_LIST_PRODUCT_GOODSTableAdapter
        '
        Me.SP_LIST_PRODUCT_GOODSTableAdapter.ClearBeforeFill = True
        '
        'SP_LIST_PURCHASE_PRICEBindingSource
        '
        Me.SP_LIST_PURCHASE_PRICEBindingSource.DataMember = "SP_LIST_PURCHASE_PRICE"
        Me.SP_LIST_PURCHASE_PRICEBindingSource.DataSource = Me.DS_PURCHASE
        '
        'SP_LIST_PURCHASE_PRICETableAdapter
        '
        Me.SP_LIST_PURCHASE_PRICETableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.PRODUCTTableAdapter = Nothing
        Me.TableAdapterManager.PURCHASE_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.PURCHASETableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_REPORT.DS_PURCHASETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.VENDORTableAdapter = Nothing
        Me.TableAdapterManager.WAREHOUSETableAdapter = Nothing
        '
        'dgvPurchasePrice
        '
        Me.dgvPurchasePrice.AllowUserToAddRows = False
        Me.dgvPurchasePrice.AllowUserToDeleteRows = False
        Me.dgvPurchasePrice.AutoGenerateColumns = False
        Me.dgvPurchasePrice.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPurchasePrice.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPurchasePrice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPurchasePrice.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.TRANSACTION_NAME, Me.TRANSACTION_DATE, Me.TRANSACTION_NO, Me.PRODUCT_NAME, Me.PRICE_PER_UNIT, Me.QUANTITY, Me.DISCOUNT, Me.TOTAL, Me.WAREHOUSE_NAME})
        Me.dgvPurchasePrice.DataSource = Me.SP_LIST_PURCHASE_PRICEBindingSource
        Me.dgvPurchasePrice.Location = New System.Drawing.Point(12, 177)
        Me.dgvPurchasePrice.Name = "dgvPurchasePrice"
        Me.dgvPurchasePrice.ReadOnly = True
        Me.dgvPurchasePrice.RowHeadersWidth = 28
        Me.dgvPurchasePrice.Size = New System.Drawing.Size(766, 254)
        Me.dgvPurchasePrice.TabIndex = 14
        '
        'TRANSACTION_NAME
        '
        Me.TRANSACTION_NAME.DataPropertyName = "TRANSACTION_NAME"
        Me.TRANSACTION_NAME.HeaderText = "Transaction Name"
        Me.TRANSACTION_NAME.Name = "TRANSACTION_NAME"
        Me.TRANSACTION_NAME.ReadOnly = True
        '
        'TRANSACTION_DATE
        '
        Me.TRANSACTION_DATE.DataPropertyName = "TRANSACTION_DATE"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "dd-MMM-yyyy"
        Me.TRANSACTION_DATE.DefaultCellStyle = DataGridViewCellStyle2
        Me.TRANSACTION_DATE.HeaderText = "Transaction Date"
        Me.TRANSACTION_DATE.Name = "TRANSACTION_DATE"
        Me.TRANSACTION_DATE.ReadOnly = True
        Me.TRANSACTION_DATE.Width = 90
        '
        'TRANSACTION_NO
        '
        Me.TRANSACTION_NO.DataPropertyName = "TRANSACTION_NO"
        Me.TRANSACTION_NO.HeaderText = "Transaction No"
        Me.TRANSACTION_NO.Name = "TRANSACTION_NO"
        Me.TRANSACTION_NO.ReadOnly = True
        Me.TRANSACTION_NO.Width = 80
        '
        'PRODUCT_NAME
        '
        Me.PRODUCT_NAME.DataPropertyName = "PRODUCT_NAME"
        Me.PRODUCT_NAME.HeaderText = "Product Name"
        Me.PRODUCT_NAME.Name = "PRODUCT_NAME"
        Me.PRODUCT_NAME.ReadOnly = True
        Me.PRODUCT_NAME.Width = 120
        '
        'PRICE_PER_UNIT
        '
        Me.PRICE_PER_UNIT.DataPropertyName = "PRICE_PER_UNIT"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N0"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.PRICE_PER_UNIT.DefaultCellStyle = DataGridViewCellStyle3
        Me.PRICE_PER_UNIT.HeaderText = "Price / Unit"
        Me.PRICE_PER_UNIT.Name = "PRICE_PER_UNIT"
        Me.PRICE_PER_UNIT.ReadOnly = True
        Me.PRICE_PER_UNIT.Width = 65
        '
        'QUANTITY
        '
        Me.QUANTITY.DataPropertyName = "QUANTITY"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.QUANTITY.DefaultCellStyle = DataGridViewCellStyle4
        Me.QUANTITY.HeaderText = "Qty."
        Me.QUANTITY.Name = "QUANTITY"
        Me.QUANTITY.ReadOnly = True
        Me.QUANTITY.Width = 45
        '
        'DISCOUNT
        '
        Me.DISCOUNT.DataPropertyName = "DISCOUNT"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N0"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.DISCOUNT.DefaultCellStyle = DataGridViewCellStyle5
        Me.DISCOUNT.HeaderText = "Disc."
        Me.DISCOUNT.Name = "DISCOUNT"
        Me.DISCOUNT.ReadOnly = True
        Me.DISCOUNT.Width = 60
        '
        'TOTAL
        '
        Me.TOTAL.DataPropertyName = "TOTAL"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N0"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.TOTAL.DefaultCellStyle = DataGridViewCellStyle6
        Me.TOTAL.HeaderText = "Total"
        Me.TOTAL.Name = "TOTAL"
        Me.TOTAL.ReadOnly = True
        Me.TOTAL.Width = 70
        '
        'WAREHOUSE_NAME
        '
        Me.WAREHOUSE_NAME.DataPropertyName = "WAREHOUSE_NAME"
        Me.WAREHOUSE_NAME.HeaderText = "Warehouse"
        Me.WAREHOUSE_NAME.Name = "WAREHOUSE_NAME"
        Me.WAREHOUSE_NAME.ReadOnly = True
        Me.WAREHOUSE_NAME.Width = 90
        '
        'frmListPurchasePrice
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(790, 468)
        Me.Controls.Add(Me.dgvPurchasePrice)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmListPurchasePrice"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "List Purchasing Price"
        Me.Text = "List Purchasing Price"
        CType(Me.DS_PURCHASE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_LIST_PRODUCT_GOODSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_LIST_PURCHASE_PRICEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPurchasePrice, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DS_PURCHASE As DMI_RETAIL_REPORT.DS_PURCHASE
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdReport As System.Windows.Forms.Button
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents lblFrom As System.Windows.Forms.Label
    Friend WithEvents cmdGenerate As System.Windows.Forms.Button
    Friend WithEvents dtpPeriod2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpPeriod1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblProduct As System.Windows.Forms.Label
    Friend WithEvents cmdSearchName As System.Windows.Forms.PictureBox
    Friend WithEvents PRODUCT_CODEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents PRODUCT_NAMETextBox As System.Windows.Forms.TextBox
    Friend WithEvents SP_LIST_PRODUCT_GOODSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_LIST_PRODUCT_GOODSTableAdapter As DMI_RETAIL_REPORT.DS_PURCHASETableAdapters.SP_LIST_PRODUCT_GOODSTableAdapter
    Friend WithEvents SP_LIST_PURCHASE_PRICEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_LIST_PURCHASE_PRICETableAdapter As DMI_RETAIL_REPORT.DS_PURCHASETableAdapters.SP_LIST_PURCHASE_PRICETableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_REPORT.DS_PURCHASETableAdapters.TableAdapterManager
    Friend WithEvents dgvPurchasePrice As System.Windows.Forms.DataGridView
    Friend WithEvents TRANSACTION_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TRANSACTION_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TRANSACTION_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRODUCT_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRICE_PER_UNIT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QUANTITY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DISCOUNT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WAREHOUSE_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
