﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSearchProduct
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSearchProduct))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.PRODUCTBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.PRODUCTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_SALE_ORDER = New DMI_RETAIL_REPORT.DS_SALE_ORDER()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.PRODUCTDataGridView = New System.Windows.Forms.DataGridView()
        Me.PRODUCT_CODE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRODUCT_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtProductCode = New System.Windows.Forms.TextBox()
        Me.PRODUCTTableAdapter = New DMI_RETAIL_REPORT.DS_SALE_ORDERTableAdapters.PRODUCTTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_REPORT.DS_SALE_ORDERTableAdapters.TableAdapterManager()
        Me.SP_LIST_PRODUCT_GOODSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_LIST_PRODUCT_GOODSTableAdapter = New DMI_RETAIL_REPORT.DS_SALE_ORDERTableAdapters.SP_LIST_PRODUCT_GOODSTableAdapter()
        Me.SP_LIST_PRODUCT_ACTIVEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_LIST_PRODUCT_ACTIVETableAdapter = New DMI_RETAIL_REPORT.DS_SALE_ORDERTableAdapters.SP_LIST_PRODUCT_ACTIVETableAdapter()
        CType(Me.PRODUCTBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PRODUCTBindingNavigator.SuspendLayout()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_SALE_ORDER, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PRODUCTDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_LIST_PRODUCT_GOODSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_LIST_PRODUCT_ACTIVEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PRODUCTBindingNavigator
        '
        Me.PRODUCTBindingNavigator.AddNewItem = Nothing
        Me.PRODUCTBindingNavigator.BindingSource = Me.PRODUCTBindingSource
        Me.PRODUCTBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.PRODUCTBindingNavigator.DeleteItem = Nothing
        Me.PRODUCTBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.PRODUCTBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.PRODUCTBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.PRODUCTBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.PRODUCTBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.PRODUCTBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.PRODUCTBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.PRODUCTBindingNavigator.Name = "PRODUCTBindingNavigator"
        Me.PRODUCTBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.PRODUCTBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.PRODUCTBindingNavigator.Size = New System.Drawing.Size(425, 25)
        Me.PRODUCTBindingNavigator.TabIndex = 0
        Me.PRODUCTBindingNavigator.Text = "BindingNavigator1"
        '
        'PRODUCTBindingSource
        '
        Me.PRODUCTBindingSource.DataMember = "PRODUCT"
        Me.PRODUCTBindingSource.DataSource = Me.DS_SALE_ORDER
        '
        'DS_SALE_ORDER
        '
        Me.DS_SALE_ORDER.DataSetName = "DS_SALE_ORDER"
        Me.DS_SALE_ORDER.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'PRODUCTDataGridView
        '
        Me.PRODUCTDataGridView.AllowUserToAddRows = False
        Me.PRODUCTDataGridView.AllowUserToDeleteRows = False
        Me.PRODUCTDataGridView.AutoGenerateColumns = False
        Me.PRODUCTDataGridView.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.PRODUCTDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.PRODUCTDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.PRODUCTDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PRODUCT_CODE, Me.PRODUCT_NAME})
        Me.PRODUCTDataGridView.DataSource = Me.PRODUCTBindingSource
        Me.PRODUCTDataGridView.Location = New System.Drawing.Point(12, 64)
        Me.PRODUCTDataGridView.MultiSelect = False
        Me.PRODUCTDataGridView.Name = "PRODUCTDataGridView"
        Me.PRODUCTDataGridView.ReadOnly = True
        Me.PRODUCTDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.PRODUCTDataGridView.Size = New System.Drawing.Size(398, 280)
        Me.PRODUCTDataGridView.TabIndex = 1
        '
        'PRODUCT_CODE
        '
        Me.PRODUCT_CODE.DataPropertyName = "PRODUCT_CODE"
        Me.PRODUCT_CODE.HeaderText = "Product Code"
        Me.PRODUCT_CODE.Name = "PRODUCT_CODE"
        Me.PRODUCT_CODE.ReadOnly = True
        '
        'PRODUCT_NAME
        '
        Me.PRODUCT_NAME.DataPropertyName = "PRODUCT_NAME"
        Me.PRODUCT_NAME.HeaderText = "Product Name"
        Me.PRODUCT_NAME.Name = "PRODUCT_NAME"
        Me.PRODUCT_NAME.ReadOnly = True
        Me.PRODUCT_NAME.Width = 230
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(12, 41)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(93, 17)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Product Code"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtProductCode
        '
        Me.txtProductCode.Location = New System.Drawing.Point(111, 38)
        Me.txtProductCode.Name = "txtProductCode"
        Me.txtProductCode.Size = New System.Drawing.Size(118, 20)
        Me.txtProductCode.TabIndex = 0
        '
        'PRODUCTTableAdapter
        '
        Me.PRODUCTTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.SALE_ORDER_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.SALE_ORDER_HEADERTableAdapter = Nothing
        Me.TableAdapterManager.SALESMANTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_REPORT.DS_SALE_ORDERTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'SP_LIST_PRODUCT_GOODSBindingSource
        '
        Me.SP_LIST_PRODUCT_GOODSBindingSource.DataMember = "SP_LIST_PRODUCT_GOODS"
        Me.SP_LIST_PRODUCT_GOODSBindingSource.DataSource = Me.DS_SALE_ORDER
        '
        'SP_LIST_PRODUCT_GOODSTableAdapter
        '
        Me.SP_LIST_PRODUCT_GOODSTableAdapter.ClearBeforeFill = True
        '
        'SP_LIST_PRODUCT_ACTIVEBindingSource
        '
        Me.SP_LIST_PRODUCT_ACTIVEBindingSource.DataMember = "SP_LIST_PRODUCT_ACTIVE"
        Me.SP_LIST_PRODUCT_ACTIVEBindingSource.DataSource = Me.DS_SALE_ORDER
        '
        'SP_LIST_PRODUCT_ACTIVETableAdapter
        '
        Me.SP_LIST_PRODUCT_ACTIVETableAdapter.ClearBeforeFill = True
        '
        'frmSearchProduct
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(425, 361)
        Me.Controls.Add(Me.txtProductCode)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PRODUCTDataGridView)
        Me.Controls.Add(Me.PRODUCTBindingNavigator)
        Me.Font = New System.Drawing.Font("Lucida Bright", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSearchProduct"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Search Product"
        CType(Me.PRODUCTBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PRODUCTBindingNavigator.ResumeLayout(False)
        Me.PRODUCTBindingNavigator.PerformLayout()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_SALE_ORDER, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PRODUCTDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_LIST_PRODUCT_GOODSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_LIST_PRODUCT_ACTIVEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DS_SALE_ORDER As DMI_RETAIL_REPORT.DS_SALE_ORDER
    Friend WithEvents PRODUCTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PRODUCTTableAdapter As DMI_RETAIL_REPORT.DS_SALE_ORDERTableAdapters.PRODUCTTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_REPORT.DS_SALE_ORDERTableAdapters.TableAdapterManager
    Friend WithEvents PRODUCTBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PRODUCTDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtProductCode As System.Windows.Forms.TextBox
    Friend WithEvents PRODUCT_CODE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRODUCT_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SP_LIST_PRODUCT_GOODSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_LIST_PRODUCT_GOODSTableAdapter As DMI_RETAIL_REPORT.DS_SALE_ORDERTableAdapters.SP_LIST_PRODUCT_GOODSTableAdapter
    Friend WithEvents SP_LIST_PRODUCT_ACTIVEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_LIST_PRODUCT_ACTIVETableAdapter As DMI_RETAIL_REPORT.DS_SALE_ORDERTableAdapters.SP_LIST_PRODUCT_ACTIVETableAdapter
End Class
