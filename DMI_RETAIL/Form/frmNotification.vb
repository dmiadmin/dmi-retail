﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frmNotification
    Dim xComm As New SqlCommand
    Dim nReader As SqlDataReader
    Dim tmpDate, tmpdate1, tmpCheckMonthly As DateTime
    Dim tmpcheckyearly As DateTime
    Dim tmpChange As Boolean = False
    Dim getMaturitySale As Boolean = False
    Dim getmaturityPurchase As Boolean = False

    Private Sub connection()
        xConn.Close()
        'xConn.ConnectionString = "Data Source=" & ServerName & ";Initial Catalog=" & DatabaseName & ";Persist Security Info=True;User ID=sa;Password=SA;"
        xConn.ConnectionString = "Data Source=.\SQLExpress;Integrated Security=true;User Instance=true;AttachDbFilename=|DataDirectory|\DB_DMI_RETAIL.mdf;Trusted_Connection=Yes;"
        xConn.Open()
    End Sub


    Sub get_checkDate()
        tmpdate1 = DateSerial(Now.Year, _
                             Now.Month, _
                             Now.Day)

        tmpcheckyearly = DateSerial(tmpdate1.Year - 1, _
                                    1, _
                                    1)

        tmpDate = DateSerial(Now.Year, _
                             Now.Month, _
                             1)

        tmpCheckMonthly = DateSerial(tmpDate.Year, _
                                 tmpDate.Month - 1, _
                                 1)
    End Sub

    Private Sub frmNotification_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim tmpCheckSafe As Boolean
        Dim tmpValue As Integer = 0

        LinkLabel1.Visible = False
        LinkLabel2.Visible = False
        LinkLabel3.Visible = False

        ''Get Parameter Check
        get_checkDate()
        'tmpCheckSafe = Me.SP_NOTIFICATION_SAFE_STOCKTableAdapter.Fill(Me.DS_PRODUCT_STOCK.SP_NOTIFICATION_SAFE_STOCK)

        Dim tmpPeriod As DateTime
        ''For Check Closed Period Monthly
        Call connection()
        xComm = New SqlCommand("DECLARE	@return_value int" & vbCrLf & _
                                "EXEC	@return_value = [dbo].[SP_CLOSED_PERIOD_STOP] " & vbCrLf & _
                                "SELECT	'Return Value' = @return_value", xConn)
        nReader = xComm.ExecuteReader()
        If nReader.Read = True Then
            tmpPeriod = nReader.Item("PERIOD").ToString
        End If
        If tmpPeriod <> tmpCheckMonthly Then
            tmpChange = True
            If tmpChange = True And LinkLabel1.Visible = False Then
                If Language = "Indonesian" Then
                    LinkLabel1.Text = "Tutup Buku per Bulan " & tmpCheckMonthly.Month.ToString & " belum diproses"
                Else
                    LinkLabel1.Text = "Closing period monthly " & tmpCheckMonthly.Month.ToString & " is not processed yet"
                End If

                tmpChange = False
                LinkLabel1.Visible = True
                tmpValue += 1
                GroupBox1.Height = 52
                GroupBox1.Width = 320
                Me.Height = 194
                Me.Width = 372
            End If
        End If

        ''for Check Safe Stock
        If tmpCheckSafe = True Then
            tmpChange = True
            If tmpChange = True And LinkLabel1.Visible = False Then
                If Language = "Indonesian" Then
                    LinkLabel1.Text = "Produk dengan jumlah kurang dari Stok Aman"
                Else
                    LinkLabel1.Text = "Product have quantity less then safe stock"
                    End If
                tmpChange = False
                LinkLabel1.Visible = True
                tmpValue += 1
                GroupBox1.Height = 52
                GroupBox1.Width = 320
                Me.Height = 194
                Me.Width = 372

            ElseIf tmpChange = True And LinkLabel2.Visible = False Then
                If Language = "Indonesian" Then
                    LinkLabel2.Text = "Produk dengan jumlah kurang dari Stok Aman"
                Else
                    LinkLabel2.Text = "Product have quantity less then safe stock"
                    End If
                tmpChange = False
                LinkLabel2.Visible = True
                tmpValue += 1
                GroupBox1.Height = 88
                GroupBox1.Width = 320
                Me.Height = 231
                Me.Width = 372
                End If
            End If

        ''For Check Closed Period Yearly
        nReader.Close()
        Dim tmpCheckClosed As DateTime
        xComm = New SqlCommand("DECLARE	@return_value int" & vbCrLf & _
                                "EXEC	    @return_value = [dbo].[SP_GET_CLOSED_PERIOD_YEARLY]" & vbCrLf & _
                                "SELECT	'Return Value' = @return_value", xConn)
        nReader = xComm.ExecuteReader()
        If nReader.Read = True Then
            tmpCheckClosed = nReader.Item("PERIOD").ToString
        End If
        If tmpCheckClosed <> tmpcheckyearly Then
            tmpChange = True
            If tmpChange = True And LinkLabel1.Visible = False Then
                If Language = "Indonesian" Then
                    LinkLabel1.Text = "Tutup Buku per Tahun " & tmpcheckyearly.Year.ToString & " belum diproses"
                Else
                    LinkLabel1.Text = "Closing period yearly " & tmpcheckyearly.Year.ToString & " is not processed yet"
                End If
                tmpChange = False
                LinkLabel1.Visible = True
                tmpValue += 1
                GroupBox1.Height = 52
                GroupBox1.Width = 320
                Me.Height = 194
                Me.Width = 372

            ElseIf tmpChange = True And LinkLabel2.Visible = False Then
                If Language = "Indonesian" Then
                    LinkLabel2.Text = "Tutup Buku per Tahun " & tmpcheckyearly.Year.ToString & " belum diproses"
                Else
                    LinkLabel2.Text = "Closing period yearly " & tmpcheckyearly.Year.ToString & " is not processed yet"
                End If
                tmpChange = False
                LinkLabel2.Visible = True
                tmpValue += 1
                GroupBox1.Height = 88
                GroupBox1.Width = 320
                Me.Height = 231
                Me.Width = 372

            ElseIf tmpChange = True And LinkLabel3.Visible = False Then
                If Language = "Indonesian" Then
                    LinkLabel3.Text = "Tutup Buku per Tahun " & tmpcheckyearly.Year.ToString & " belum diproses"
                Else
                    LinkLabel3.Text = "Closing period yearly " & tmpcheckyearly.Year.ToString & " is not processed yet"
                End If
                tmpChange = False
                LinkLabel3.Visible = True
                tmpValue += 1
                GroupBox1.Height = 134
                GroupBox1.Width = 320
                Me.Height = 276
                Me.Width = 372
            End If
        End If

        ''Check invoice sale
        nReader.Close()
        xComm = New SqlCommand("DECLARE	@return_value int" & vbCrLf & _
                                "EXEC	@return_value = [dbo].[SP_NOTIF_INVOICE_SALE]" & vbCrLf & _
                                "SELECT	'Return Value' = @return_value", xConn)
        nReader = xComm.ExecuteReader()
        If nReader.Read = True Then
            getMaturitySale = True
        Else
            getMaturitySale = False
        End If

        'Me.SP_NOTIF_INVOICE_SALETableAdapter.Fill(Me.DS_SALE.SP_NOTIF_INVOICE_SALE)

        'If SP_NOTIF_INVOICE_SALEBindingSource.Count > 0 Then


        If getMaturitySale = True Then
            tmpChange = True
            If tmpChange = True And LinkLabel1.Visible = False Then
                If Language = "Indonesian" Then
                    LinkLabel1.Text = "Periksa Faktur Penjualan yang sudah Jatuh Tempo"
                Else
                    LinkLabel1.Text = "Sale Invoice is due please check your invoice"
                End If
                tmpChange = False
                LinkLabel1.Visible = True
                tmpValue += 1
                GroupBox1.Height = 52
                GroupBox1.Width = 320
                Me.Height = 194
                Me.Width = 372
            ElseIf tmpChange = True And LinkLabel2.Visible = False Then
                If Language = "Indonesian" Then
                    LinkLabel2.Text = "Periksa Faktur Penjualan yang sudah Jatuh Tempo"
                Else
                    LinkLabel2.Text = "Sale Invoice is due please check your invoice"
                End If
                tmpChange = False
                LinkLabel2.Visible = True
                tmpValue += 1
                GroupBox1.Height = 88
                GroupBox1.Width = 320
                Me.Height = 231
                Me.Width = 372
            ElseIf tmpChange = True And LinkLabel3.Visible = False Then
                If Language = "Indonesian" Then
                    LinkLabel3.Text = "Periksa Faktur Penjualan yang sudah Jatuh Tempo"
                Else
                    LinkLabel3.Text = "Sale Invoice is due please check your invoice"
                End If
                tmpChange = False
                LinkLabel3.Visible = True
                tmpValue += 1
                GroupBox1.Height = 134
                GroupBox1.Width = 320
                Me.Height = 276
                Me.Width = 372
            ElseIf tmpChange = True And LinkLabel4.Visible = False Then
                If Language = "Indonesian" Then
                    LinkLabel4.Text = "Periksa Faktur Penjualan yang sudah Jatuh Tempo"
                Else
                    LinkLabel4.Text = "Sale Invoice is due please check your invoice"
                End If
                tmpChange = False
                LinkLabel4.Visible = True
                tmpValue += 1
                GroupBox1.Height = 169
                GroupBox1.Width = 320
                Me.Height = 298
                Me.Width = 372
            End If
        End If

        ''Check Invoice Purchase
        nReader.Close()
        xComm = New SqlCommand("DECLARE	@return_value int" & vbCrLf & _
                                "EXEC	@return_value = [dbo].[SP_NOTIF_IVOICE_PURCHASE]" & vbCrLf & _
                                "SELECT	'Return Value' = @return_value", xConn)
        nReader = xComm.ExecuteReader()
        If nReader.Read = True Then
            getmaturityPurchase = True
        Else
            getmaturityPurchase = False
        End If

        'Me.SP_NOTIF_IVOICE_PURCHASETableAdapter.Fill(DS_PURCHASE.SP_NOTIF_IVOICE_PURCHASE)
        'If SP_NOTIF_IVOICE_PURCHASEBindingSource.Count > 0 Then
        '    getmaturityPurchase = True
        'Else
        '    getmaturityPurchase = False
        'End If
        If getmaturityPurchase = True Then
            tmpChange = True
            If tmpChange = True And LinkLabel1.Visible = False Then
                If Language = "Indonesian" Then
                    LinkLabel1.Text = "Periksa Faktur Pembelian yang sudah Jatuh Tempo"
                Else
                    LinkLabel1.Text = "Purchase Invoice is due please check your invoice"
                End If
                tmpChange = False
                LinkLabel1.Visible = True
                tmpValue += 1
                GroupBox1.Height = 52
                GroupBox1.Width = 320
                Me.Height = 194
                Me.Width = 372
            ElseIf tmpChange = True And LinkLabel2.Visible = False Then
                If Language = "Indonesian" Then
                    LinkLabel2.Text = "Periksa Faktur Pembelian yang sudah Jatuh Tempo"
                Else
                    LinkLabel2.Text = "Purchase Invoice is due please check your invoice"
                End If
                tmpChange = False
                LinkLabel2.Visible = True
                tmpValue += 1
                GroupBox1.Height = 88
                GroupBox1.Width = 320
                Me.Height = 231
                Me.Width = 372
            ElseIf tmpChange = True And LinkLabel3.Visible = False Then
                If Language = "Indonesian" Then
                    LinkLabel3.Text = "Periksa Faktur Pembelian yang sudah Jatuh Tempo"
                Else
                    LinkLabel3.Text = "Purchase Invoice is due please check your invoice"
                End If
                tmpChange = False
                LinkLabel3.Visible = True
                tmpValue += 1
                GroupBox1.Height = 134
                GroupBox1.Width = 320
                Me.Height = 276
                Me.Width = 372
            ElseIf tmpChange = True And LinkLabel4.Visible = False Then
                If Language = "Indonesian" Then
                    LinkLabel4.Text = "Periksa Faktur Pembelian yang sudah Jatuh Tempo"
                Else
                    LinkLabel4.Text = "Purchase Invoice is due please check your invoice"
                End If
                tmpChange = False
                LinkLabel4.Visible = True
                tmpValue += 1
                GroupBox1.Height = 169
                GroupBox1.Width = 320
                Me.Height = 298
                Me.Width = 372
            ElseIf tmpChange = True And LinkLabel5.Visible = False Then
                If Language = "Indonesian" Then
                    LinkLabel5.Text = "Periksa Faktur Pembelian yang sudah Jatuh Tempo"
                Else
                    LinkLabel5.Text = "Purchase Invoice is due please check your invoice"
                End If
                tmpChange = False
                LinkLabel5.Visible = True
                tmpValue += 1
                GroupBox1.Height = 208
                GroupBox1.Width = 320
                Me.Height = 341
                Me.Width = 372
            End If
        End If

        If Language = "Indonesian" Then
            Label1.Text = "Anda memiliki " & tmpValue.ToString & " Poin Pemberitahuan"
        Else
            Label1.Text = "You Have " & tmpValue.ToString & " Point Notification"
        End If

    End Sub


    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        get_checkDate()
        If Language = "Indonesian" Then
            If LinkLabel1.Text = "Tutup Buku per Bulan " & tmpCheckMonthly.Month.ToString & " belum diproses" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmClosingPeriod.MdiParent = frmMain
                    frmClosingPeriod.Show()
                    frmClosingPeriod.BringToFront()
                End If
            ElseIf LinkLabel1.Text = "Produk dengan jumlah kurang dari Stok Aman" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListSafeStock.MdiParent = frmMain
                    frmListSafeStock.Show()
                    frmListSafeStock.BringToFront()
                End If
            ElseIf LinkLabel1.Text = "Tutup Buku per Tahun " & tmpcheckyearly.Year.ToString & " belum diproses" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmClosingPeriodYearly.MdiParent = frmMain
                    frmClosingPeriodYearly.Show()
                    frmClosingPeriodYearly.BringToFront()
                End If
            ElseIf LinkLabel1.Text = "Periksa Faktur Penjualan yang sudah Jatuh Tempo" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListInvoiceSale.MdiParent = frmMain
                    frmListInvoiceSale.Show()
                    frmListInvoiceSale.BringToFront()
                End If
            ElseIf LinkLabel1.Text = "Periksa Faktur Pembelian yang sudah Jatuh Tempo" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListInvoicePurchase.MdiParent = frmMain
                    frmListInvoicePurchase.Show()
                    frmListInvoicePurchase.BringToFront()
                End If
            End If

        Else

            If LinkLabel1.Text = "Closing period monthly " & tmpCheckMonthly.Month.ToString & " is not processed yet" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmClosingPeriod.MdiParent = frmMain
                    frmClosingPeriod.Show()
                    frmClosingPeriod.BringToFront()
                End If
            ElseIf LinkLabel1.Text = "Product have quantity less then safe stock" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListSafeStock.MdiParent = frmMain
                    frmListSafeStock.Show()
                    frmListSafeStock.BringToFront()
                End If
            ElseIf LinkLabel1.Text = "Closing period yearly " & tmpcheckyearly.Year.ToString & " is not processed yet" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmClosingPeriodYearly.MdiParent = frmMain
                    frmClosingPeriodYearly.Show()
                    frmClosingPeriodYearly.BringToFront()
                End If
            ElseIf LinkLabel1.Text = "Sale Invoice is due please check your invoice" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListInvoiceSale.MdiParent = frmMain
                    frmListInvoiceSale.Show()
                    frmListInvoiceSale.BringToFront()
                End If
            ElseIf LinkLabel1.Text = "Purchase Invoice is due please check your invoice" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListInvoicePurchase.MdiParent = frmMain
                    frmListInvoicePurchase.Show()
                    frmListInvoicePurchase.BringToFront()
                End If
            End If
        End If

        Me.Close()
    End Sub

    Private Sub LinkLabel2_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        get_checkDate()

        If Language = "Indonesian" Then
            If LinkLabel2.Text = "Tutup Buku per Bulan " & tmpCheckMonthly.Month.ToString & " belum diproses" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmClosingPeriod.MdiParent = frmMain
                    frmClosingPeriod.Show()
                    frmClosingPeriod.BringToFront()
                End If
            ElseIf LinkLabel2.Text = "Produk dengan jumlah kurang dari Stok Aman" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListSafeStock.MdiParent = frmMain
                    frmListSafeStock.Show()
                    frmListSafeStock.BringToFront()
                End If
            ElseIf LinkLabel2.Text = "Tutup Buku per Tahun " & tmpcheckyearly.Year.ToString & " belum diproses" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmClosingPeriodYearly.MdiParent = frmMain
                    frmClosingPeriodYearly.Show()
                    frmClosingPeriodYearly.BringToFront()
                End If
            ElseIf LinkLabel2.Text = "Periksa Faktur Penjualan yang sudah Jatuh Tempo" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListInvoiceSale.MdiParent = frmMain
                    frmListInvoiceSale.Show()
                    frmListInvoiceSale.BringToFront()
                End If
            ElseIf LinkLabel2.Text = "Periksa Faktur Pembelian yang sudah Jatuh Tempo" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListInvoicePurchase.MdiParent = frmMain
                    frmListInvoicePurchase.Show()
                    frmListInvoicePurchase.BringToFront()
                End If
            End If

        Else

            If LinkLabel2.Text = "Closing period monthly " & tmpCheckMonthly.Month.ToString & " is not processed yet" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmClosingPeriod.MdiParent = frmMain
                    frmClosingPeriod.Show()
                    frmClosingPeriod.BringToFront()
                End If
            ElseIf LinkLabel2.Text = "Product have quantity less then safe stock" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListSafeStock.MdiParent = frmMain
                    frmListSafeStock.Show()
                    frmListSafeStock.BringToFront()
                End If
            ElseIf LinkLabel2.Text = "Closing period yearly " & tmpcheckyearly.Year.ToString & " is not processed yet" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmClosingPeriodYearly.MdiParent = frmMain
                    frmClosingPeriodYearly.Show()
                    frmClosingPeriodYearly.BringToFront()
                End If
            ElseIf LinkLabel2.Text = "Sale Invoice is due please check your invoice" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListInvoiceSale.MdiParent = frmMain
                    frmListInvoiceSale.Show()
                    frmListInvoiceSale.BringToFront()
                End If
            ElseIf LinkLabel2.Text = "Purchase Invoice is due please check your invoice" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListInvoicePurchase.MdiParent = frmMain
                    frmListInvoicePurchase.Show()
                    frmListInvoicePurchase.BringToFront()
                End If
            End If
        End If

        Me.Close()
    End Sub

    Private Sub LinkLabel3_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel3.LinkClicked
        get_checkDate()

        If Language = "Indonesian" Then
            If LinkLabel3.Text = "Tutup Buku per Bulan " & tmpCheckMonthly.Month.ToString & " belum diproses" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmClosingPeriod.MdiParent = frmMain
                    frmClosingPeriod.Show()
                    frmClosingPeriod.BringToFront()
                End If
            ElseIf LinkLabel3.Text = "Produk dengan jumlah kurang dari Stok Aman" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListSafeStock.MdiParent = frmMain
                    frmListSafeStock.Show()
                    frmListSafeStock.BringToFront()
                End If
            ElseIf LinkLabel3.Text = "Tutup Buku per Tahun " & tmpcheckyearly.Year.ToString & " belum diproses" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmClosingPeriodYearly.MdiParent = frmMain
                    frmClosingPeriodYearly.Show()
                    frmClosingPeriodYearly.BringToFront()
                End If
            ElseIf LinkLabel3.Text = "Periksa Faktur Penjualan yang sudah Jatuh Tempo" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListInvoiceSale.MdiParent = frmMain
                    frmListInvoiceSale.Show()
                    frmListInvoiceSale.BringToFront()
                End If
            ElseIf LinkLabel3.Text = "Periksa Faktur Pembelian yang sudah Jatuh Tempo" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListInvoicePurchase.MdiParent = frmMain
                    frmListInvoicePurchase.Show()
                    frmListInvoicePurchase.BringToFront()
                End If
            End If

        Else

            If LinkLabel3.Text = "Closing period monthly " & tmpCheckMonthly.Month.ToString & " is not processed yet" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmClosingPeriod.MdiParent = frmMain
                    frmClosingPeriod.Show()
                    frmClosingPeriod.BringToFront()
                End If
            ElseIf LinkLabel3.Text = "Product have quantity less then safe stock" Then
                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListSafeStock.MdiParent = frmMain
                    frmListSafeStock.Show()
                    frmListSafeStock.BringToFront()
                End If
            ElseIf LinkLabel3.Text = "Closing period yearly " & tmpcheckyearly.Year.ToString & " is not processed yet" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmClosingPeriodYearly.MdiParent = frmMain
                    frmClosingPeriodYearly.Show()
                    frmClosingPeriodYearly.BringToFront()
                End If
            ElseIf LinkLabel3.Text = "Sale Invoice is due please check your invoice" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListInvoiceSale.MdiParent = frmMain
                    frmListInvoiceSale.Show()
                    frmListInvoiceSale.BringToFront()
                End If
            ElseIf LinkLabel3.Text = "Purchase Invoice is due please check your invoice" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListInvoicePurchase.MdiParent = frmMain
                    frmListInvoicePurchase.Show()
                    frmListInvoicePurchase.BringToFront()
                End If
            End If
        End If

        Me.Close()
    End Sub

    Sub get_notification()
        frmNotification_Load(Nothing, Nothing)
    End Sub

    Private Sub LinkLabel4_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel4.LinkClicked
        get_checkDate()

        If Language = "Indonesian" Then
            If LinkLabel4.Text = "Tutup Buku per Bulan " & tmpCheckMonthly.Month.ToString & " belum diproses" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmClosingPeriod.MdiParent = frmMain
                    frmClosingPeriod.Show()
                    frmClosingPeriod.BringToFront()
                End If
            ElseIf LinkLabel4.Text = "Produk dengan jumlah kurang dari Stok Aman" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListSafeStock.MdiParent = frmMain
                    frmListSafeStock.Show()
                    frmListSafeStock.BringToFront()
                End If
            ElseIf LinkLabel4.Text = "Tutup Buku per Tahun " & tmpcheckyearly.Year.ToString & " belum diproses" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmClosingPeriodYearly.MdiParent = frmMain
                    frmClosingPeriodYearly.Show()
                    frmClosingPeriodYearly.BringToFront()
                End If
            ElseIf LinkLabel4.Text = "Periksa Faktur Penjualan yang sudah Jatuh Tempo" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListInvoiceSale.MdiParent = frmMain
                    frmListInvoiceSale.Show()
                    frmListInvoiceSale.BringToFront()
                End If
            ElseIf LinkLabel4.Text = "Periksa Faktur Pembelian yang sudah Jatuh Tempo" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListInvoicePurchase.MdiParent = frmMain
                    frmListInvoicePurchase.Show()
                    frmListInvoicePurchase.BringToFront()
                End If
            End If

        Else

            If LinkLabel4.Text = "Closing period monthly " & tmpCheckMonthly.Month.ToString & " is not processed yet" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmClosingPeriod.MdiParent = frmMain
                    frmClosingPeriod.Show()
                    frmClosingPeriod.BringToFront()
                End If
            ElseIf LinkLabel4.Text = "Product have quantity less then safe stock" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListSafeStock.MdiParent = frmMain
                    frmListSafeStock.Show()
                    frmListSafeStock.BringToFront()
                End If
            ElseIf LinkLabel4.Text = "Closing period yearly " & tmpcheckyearly.Year.ToString & " is not processed yet" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmClosingPeriodYearly.MdiParent = frmMain
                    frmClosingPeriodYearly.Show()
                    frmClosingPeriodYearly.BringToFront()
                End If
            ElseIf LinkLabel4.Text = "Sale Invoice is due please check your invoice" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListInvoiceSale.MdiParent = frmMain
                    frmListInvoiceSale.Show()
                    frmListInvoiceSale.BringToFront()
                End If
            ElseIf LinkLabel4.Text = "Purchase Invoice is due please check your invoice" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListInvoicePurchase.MdiParent = frmMain
                    frmListInvoicePurchase.Show()
                    frmListInvoicePurchase.BringToFront()
                End If
            End If
        End If
        Me.Close()
    End Sub

    Private Sub LinkLabel5_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel5.LinkClicked
        get_checkDate()

        If Language = "Indonesian" Then
            If LinkLabel5.Text = "Tutup Buku per Bulan " & tmpCheckMonthly.Month.ToString & " belum diproses" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmClosingPeriod.MdiParent = frmMain
                    frmClosingPeriod.Show()
                    frmClosingPeriod.BringToFront()
                End If
            ElseIf LinkLabel5.Text = "Produk dengan jumlah kurang dari Stok Aman" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListSafeStock.MdiParent = frmMain
                    frmListSafeStock.Show()
                    frmListSafeStock.BringToFront()
                End If
            ElseIf LinkLabel5.Text = "Tutup Buku per Tahun " & tmpcheckyearly.Year.ToString & " belum diproses" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmClosingPeriodYearly.MdiParent = frmMain
                    frmClosingPeriodYearly.Show()
                    frmClosingPeriodYearly.BringToFront()
                End If
            ElseIf LinkLabel5.Text = "Periksa Faktur Penjualan yang sudah Jatuh Tempo" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListInvoiceSale.MdiParent = frmMain
                    frmListInvoiceSale.Show()
                    frmListInvoiceSale.BringToFront()
                End If
            ElseIf LinkLabel5.Text = "Periksa Faktur Pembelian yang sudah Jatuh Tempo" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListInvoicePurchase.MdiParent = frmMain
                    frmListInvoicePurchase.Show()
                    frmListInvoicePurchase.BringToFront()
                End If
            End If

        Else

            If LinkLabel5.Text = "Closing period monthly " & tmpCheckMonthly.Month.ToString & " is not processed yet" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmClosingPeriod.MdiParent = frmMain
                    frmClosingPeriod.Show()
                    frmClosingPeriod.BringToFront()
                End If
            ElseIf LinkLabel5.Text = "Product have quantity less then safe stock" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListSafeStock.MdiParent = frmMain
                    frmListSafeStock.Show()
                    frmListSafeStock.BringToFront()
                End If
            ElseIf LinkLabel5.Text = "Closing period yearly " & tmpcheckyearly.Year.ToString & " is not processed yet" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmClosingPeriodYearly.MdiParent = frmMain
                    frmClosingPeriodYearly.Show()
                    frmClosingPeriodYearly.BringToFront()
                End If
            ElseIf LinkLabel5.Text = "Sale Invoice is due please check your invoice" Then

                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListInvoiceSale.MdiParent = frmMain
                    frmListInvoiceSale.Show()
                    frmListInvoiceSale.BringToFront()
                End If
            ElseIf LinkLabel5.Text = "Purchase Invoice is due please check your invoice" Then
                If tmpvar = 2 Then
                    Exit Sub
                Else
                    HideAllForms()
                    frmListInvoicePurchase.MdiParent = frmMain
                    frmListInvoicePurchase.Show()
                    frmListInvoicePurchase.BringToFront()
                End If

            End If
        End If
        Me.Close()
    End Sub
End Class