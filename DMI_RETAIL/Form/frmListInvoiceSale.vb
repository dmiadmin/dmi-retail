﻿Public Class frmListInvoiceSale

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        Me.Cursor = Cursors.WaitCursor
        'Me.SP_GET_INVOICE_SALETableAdapter.Fill(Me.DS_SALE.SP_GET_INVOICE_SALE, dtpPeriod.Value)
        Me.Cursor = Cursors.Default

        If Language = "Indonesian" Then

            SP_GET_INVOICE_SALEDataGridView.Columns("SALE_DATE").HeaderText = "Tgl Penjualan"
            SP_GET_INVOICE_SALEDataGridView.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            SP_GET_INVOICE_SALEDataGridView.Columns("CUSTOMER_NAME").HeaderText = "Nama Pelanggan"
            SP_GET_INVOICE_SALEDataGridView.Columns("BALANCE").HeaderText = "Saldo"
        End If
    End Sub

    Private Sub frmListInvoiceSale_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Language = "Indonesian" Then
            Me.Text = "Daftar Faktur Penjualan"
            cmdGenerate.Text = "Proses"

            SP_GET_INVOICE_SALEDataGridView.Columns("SALE_DATE").HeaderText = "Tgl Penjualan"
            SP_GET_INVOICE_SALEDataGridView.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            SP_GET_INVOICE_SALEDataGridView.Columns("CUSTOMER_NAME").HeaderText = "Nama Pelanggan"
            SP_GET_INVOICE_SALEDataGridView.Columns("BALANCE").HeaderText = "Saldo"
        End If
    End Sub

End Class