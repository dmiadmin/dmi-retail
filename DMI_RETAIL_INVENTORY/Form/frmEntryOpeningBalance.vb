﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frmEntryOpeningBalance
    Public tmpSaveMode As String
    Dim tmpNoOfProduct As Integer
    Public tmpWarehouseID As Integer
    Dim tmpOldProductID, tmpOldWarehouseID As Integer
    Dim tmpChange, tmpNoData As Boolean
    Dim tmpCategory As Integer
    'Dim xConn As New SqlConnection
    'Dim xComm As New SqlCommand
    'Dim xAdoAdapter As New SqlDataAdapter

    'Private Sub connection()
    '    xConn.Close()
    '    xConn.ConnectionString = "Data Source=DMI_NOTE_1\sqlexpress;Initial Catalog=DB_DMI_RETAIL;Trusted_Connection=Yes;Connect Timeout=60"
    '    xConn.Open()
    'End Sub
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    Private Sub frmEntryOpeningBalance_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        tmpSaveMode = ""
    End Sub

    Private Sub frmEntryOpeningBalance_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If tmpChange = True And PRODUCT_IDTextBox.Enabled = True Then
            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah Anda ingin menyimpan perubahan " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If
            If tmpvar = 6 Then
                cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub frmEntryOpeningBalance_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_PRODUCT_WAREHOUSE.PRODUCT_WAREHOUSE' table. You can move, or remove it, as needed.
        Me.PRODUCT_WAREHOUSETableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.PRODUCT_WAREHOUSE)
        'TODO: This line of code loads data into the 'DS_OPENING_BALANCE.WAREHOUSE' table. You can move, or remove it, as needed.
        Me.WAREHOUSETableAdapter.Fill(Me.DS_OPENING_BALANCE.WAREHOUSE)
        'TODO: This line of code loads data into the 'DS_OPENING_BALANCE.PRODUCT' table. You can move, or remove it, as needed.
        Me.PRODUCTTableAdapter.Fill(Me.DS_OPENING_BALANCE.PRODUCT)

        accFormName = Me.Text
        Call AccessPrivilege()

        If Language = "Indonesian" Then
            lblPeriod.Text = "Periode"
            lblProduct.Text = "Produk"
            lblPrice.Text = "Harga"
            lblQuantity.Text = "Jumlah"
            lblWarehouse.Text = "Gudang"
            cmdAdd.Text = "Tambah"
            cmdEdit.Text = "Ubah"
            cmdUndo.Text = "Batal"
            cmdSave.Text = "Simpan"
            cmdDelete.Text = "Hapus"
            Me.Text = "Input Saldo Awal"
            lblOpeningBalanceNo.Text = "No Saldo Awal"



            If PRODUCTBindingSource Is Nothing Or PRODUCTBindingSource.Count < 1 Then
                MsgBox("Tidak ada data produk." & vbCrLf & "Silakan input setidaknya satu produk untuk melanjutkan transaksi ini!", _
                       MsgBoxStyle.Critical, "DMI Retail")
                tmpNoData = True
            End If

            'If WAREHOUSEBindingSource Is Nothing Or WAREHOUSEBindingSource.Count < 1 Then
            '    If MsgBox("Tidak ada data Gudang." & vbCrLf & "Apakah anda ingin melanjutkan transaksi ini!", _
            '          MsgBoxStyle.OkCancel, "DMI Retail") = MsgBoxResult.No Then Exit Sub
            'End If

        Else

            If PRODUCTBindingSource Is Nothing Or PRODUCTBindingSource.Count < 1 Then
                MsgBox("There is no Product data." & vbCrLf & "Please insert at least one record to continue this process!", _
                       MsgBoxStyle.Critical, "DMI Retail")
                tmpNoData = True
            End If

            'If WAREHOUSEBindingSource Is Nothing Or WAREHOUSEBindingSource.Count < 1 Then
            '    If MsgBox("There is no Warehouse data." & vbCrLf & "Do you want continue this process!", _
            '           MsgBoxStyle.OkCancel, "DMI Retail") = MsgBoxResult.No Then Exit Sub
            'End If
        End If

        'Dim dt As New DataTable
        'Call connection()
        'Dim sql As String
        'sql = "SELECT WAREHOUSE_ID, WAREHOUSE_NAME FROM WAREHOUSE"
        'xAdoAdapter = New SqlDataAdapter(sql, xConn)
        'xAdoAdapter.Fill(dt)
        'Me.WAREHOUSE_IDComboBox.DataSource = dt
        'Me.WAREHOUSE_IDComboBox.DisplayMember = "WAREHOUSE_NAME"
        'Me.WAREHOUSE_IDComboBox.ValueMember = "WAREHOUSE_ID"

        If Me.tmpSaveMode = "Update" Then
            If tmpWarehouseID = 0 Then
                WAREHOUSE_IDComboBox.SelectedIndex = -1
            Else
                WAREHOUSE_IDComboBox.SelectedValue = tmpWarehouseID
            End If
            PRODUCTTableAdapter.SP_GET_PRODUCT_ID(PRODUCT_IDTextBox.Text, tmpOldProductID)
            tmpOldWarehouseID = tmpWarehouseID
            DisableInputBox(Me)
        Else
            OPENING_BALANCETableAdapter.SP_GENERATE_OPENING_BALANCE_NUMBER(txtOpeningBalanceNo.Text)
            cmdAdd_Click(Nothing, Nothing)
        End If

        tmpChange = False
        txtDTP.Text = Format(dtpPeriod.Value, "dd-MMM-yyyy")

    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        PRODUCT_IDTextBox.Text = ""
        QUANTITYTextBox.Text = "0"
        PRICETextBox.Text = "0"
        WAREHOUSE_IDComboBox.Text = ""

        cmdSearchCode.Visible = True
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False

        PRODUCT_IDTextBox.Focus()
        tmpChange = False
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        tmpSaveMode = ""
        Me.Close()
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        If PRODUCT_IDTextBox.Text = "" Or QUANTITYTextBox.Text = "" Or PRICETextBox.Text = "" Or _
            PRODUCT_NAMETextBox.Text = "" Or PRICETextBox.Text = "0" Or QUANTITYTextBox.Text = "0" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If


        Dim tmpCheck As String = ""
        OPENING_BALANCETableAdapter.SP_CHECK_CLOSING_PERIOD(dtpPeriod.Value, tmpCheck)
        If tmpCheck = "TRUE" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menyimpan transaksi ini !" & vbCrLf & "Transaksi ini telah melakukan penutupan periode", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Save this Transaction!" & vbCrLf & "This Transaction has been in Closed Period", MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If
        End If

        If Not ValidateAllComboBox(Me) Then
            If Language = "Indonesian" Then
                MsgBox("Nilai yang anda masukan tidak ada dalam daftar.", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You input value which is not in the list.", MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If
        End If

        If OPENING_BALANCETableAdapter.SP_CLOSED_PERIOD_PROCESS("G", dtpPeriod.Value) = "TRUE" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menyimpan transaksi ini !" & vbCrLf & "Penutupan periode sedang berlangsung", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Save this Transaction!" & vbCrLf & "The Closing Period is Running", MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If
        End If

        Dim tmpWarehouseID As Integer
        If WAREHOUSEBindingSource Is Nothing Or WAREHOUSEBindingSource.Count < 1 Then
            tmpWarehouseID = 0
        Else
            If Not ValidateComboBox(WAREHOUSE_IDComboBox) Then
                tmpWarehouseID = 0
            Else
                tmpWarehouseID = IIf(WAREHOUSE_IDComboBox.Text = "", 0, WAREHOUSEBindingSource.Current("WAREHOUSE_ID"))
            End If
        End If

        If tmpSaveMode = "" Then
            'OPENING_BALANCETableAdapter.SP_OPENING_BALANCE_CHECK_PRODUCT(PRODUCT_IDTextBox.Text, tmpNoOfProduct)
            'If tmpNoOfProduct > 0 Then
            '    MsgBox("Product already has opening balance." & vbCrLf & _
            '           "This operation is cancelled.", MsgBoxStyle.Critical, "DMI Retail")
            '    Exit Sub
            'End If

            Dim tmpProductID, tmpCheckProduct, tmpCheckOpeningNo As Integer
            Dim tmpDate As DateTime

            tmpDate = dtpPeriod.Value.Year & "/" & _
                  dtpPeriod.Value.Month & "/" & _
                  dtpPeriod.Value.Day & " " & _
                  Now.Hour & ":" & Now.Minute & ":" & Now.Second

            PRODUCTTableAdapter.SP_GET_PRODUCT_ID(PRODUCT_IDTextBox.Text, tmpProductID)
            OPENING_BALANCETableAdapter.SP_GET_CATEGORY_PRODUCT(PRODUCT_IDTextBox.Text, tmpCategory)
            OPENING_BALANCETableAdapter.SP_CHECK_OPENING_BALANCE_ALL(txtOpeningBalanceNo.Text, tmpProductID, tmpCheckOpeningNo, tmpCheckProduct)

            If tmpCategory <> 1 Then
                If Language = "Indonesian" Then
                    MsgBox("Produk yang anda input bukan barang !" & vbCrLf & _
                       "Silahkan gunakan Form Pencarian untuk menemukan produk yang benar !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("You input Product which is not Goods !" & vbCrLf & _
                           "Please use the Search Form to find the right Products !", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If

            ''''check product in table Opening_Balance''''
            If tmpCheckProduct > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Produk telah digunakan di transaksi saldo awal." & vbCrLf & _
                                               "Produk baru harus digunakan untuk transaksi ini.", _
                                               MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Receipt Number is used at transaction Opening Balance." & vbCrLf & _
                                                        "New Product must be assigned to this transaction.", _
                                                        MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If

            ''''Check Opening_Balance_Number''''
            If tmpCheckOpeningNo > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("No Transaksi telah digunakan oleh user lain." & vbCrLf & _
                                               "Nomor transaksi baru akan digunakan untuk transaksi ini.", _
                                               MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Receipt Number is used by another User." & vbCrLf & _
                                                        "New Receipt Number will be assigned to this transaction.", _
                                                        MsgBoxStyle.Critical, "DMI Retail")
                End If
            End If

            ''''get new Opening_Balance_No''''
            While tmpCheckOpeningNo > 0
                OPENING_BALANCETableAdapter.SP_GENERATE_OPENING_BALANCE_NUMBER(txtOpeningBalanceNo.Text)
                OPENING_BALANCETableAdapter.SP_CHECK_OPENING_BALANCE_ALL(txtOpeningBalanceNo.Text, tmpProductID, tmpCheckOpeningNo, tmpCheckProduct)
            End While

            OPENING_BALANCETableAdapter.SP_OPENING_BALANCE("I", _
                                                           0, _
                                                           txtOpeningBalanceNo.Text, _
                                                           tmpDate, _
                                                           tmpProductID, _
                                                           QUANTITYTextBox.Text, _
                                                           PRICETextBox.Text, _
                                                           tmpWarehouseID, _
                                                           mdlGeneral.USER_ID, _
                                                           Now, _
                                                           0, _
                                                           DateSerial(4000, 12, 31))

            PRODUCT_WAREHOUSETableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                               0, _
                                                               tmpProductID, _
                                                               tmpWarehouseID, _
                                                               QUANTITYTextBox.Text, _
                                                               USER_ID, _
                                                               Now, _
                                                               0, _
                                                               DateSerial(4000, 12, 31))

            MsgBox("Data sucessfully saved!", MsgBoxStyle.Information, "DMI Retail")
            cmdAdd_Click(Nothing, Nothing)
        ElseIf tmpSaveMode = "Update" Then

            Dim tmpProductID As Integer
            PRODUCTTableAdapter.SP_GET_PRODUCT_ID(PRODUCT_IDTextBox.Text, tmpProductID)

            Dim tmpPeriod As Date
            tmpPeriod = DateSerial(dtpPeriod.Value.Year, dtpPeriod.Value.Month, dtpPeriod.Value.Day) & " " & TimeSerial(Now.Hour, Now.Minute, Now.Second)

            OPENING_BALANCETableAdapter.SP_OPENING_BALANCE("U", _
                                                           0, _
                                                           txtOpeningBalanceNo.Text, _
                                                           tmpPeriod, _
                                                           tmpProductID, _
                                                           QUANTITYTextBox.Text, _
                                                           PRICETextBox.Text, _
                                                           tmpWarehouseID, _
                                                           0, _
                                                           DateSerial(4000, 12, 31), _
                                                           mdlGeneral.USER_ID, _
                                                           Now)

            PRODUCT_WAREHOUSETableAdapter.Update(tmpProductID, _
                                                 tmpWarehouseID, _
                                                 QUANTITYTextBox.Text, _
                                                 USER_ID, _
                                                 Now, _
                                                 tmpOldProductID, _
                                                 tmpOldWarehouseID)
            If Language = "Indonesian" Then
                MsgBox("Data berhasil disimpan!", MsgBoxStyle.Information, "DMI Retail")
            Else
                MsgBox("Data sucessfully saved!", MsgBoxStyle.Information, "DMI Retail")
            End If

            tmpChange = False
            Me.Close()
        End If
        OPENING_BALANCETableAdapter.SP_GENERATE_OPENING_BALANCE_NUMBER(txtOpeningBalanceNo.Text)
        tmpSaveMode = ""
    End Sub

    Private Sub QUANTITYTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles QUANTITYTextBox.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""
    End Sub

    Private Sub PRICETextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PRICETextBox.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""
    End Sub

    Private Sub PRODUCT_IDTextBox_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles PRODUCT_IDTextBox.KeyUp
        If e.KeyData = Keys.Enter Then QUANTITYTextBox.Focus()
    End Sub

    Private Sub PRODUCT_IDTextBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles PRODUCT_IDTextBox.LostFocus
        If PRODUCT_IDTextBox.Text = "" Then Exit Sub
        Try
            PRODUCTTableAdapter.SP_GET_CATEGORY_PRODUCT(PRODUCT_IDTextBox.Text, tmpCategory)
        Catch
        End Try
        If tmpCategory <> 1 Then
            If Language = "Indonesian" Then
                MsgBox("Anda masukan produk yang bukan barang !" & vbCrLf & _
                     "Gunajan Form Pencarian untuk menggunakan data yang tepat !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You input Product which is not Goods !" & vbCrLf & _
                       "Use the Search Form to find the right Product !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            PRODUCT_NAMETextBox.Text = ""
            PRODUCT_IDTextBox.Text = ""

        End If
    End Sub

    Private Sub PRODUCT_IDTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PRODUCT_IDTextBox.TextChanged
        PRODUCTTableAdapter.SP_GET_PRODUCT_NAME(PRODUCT_IDTextBox.Text, PRODUCT_NAMETextBox.Text)
        tmpChange = True
    End Sub

    Private Sub cmdSearchCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchCode.Click
        frmSearchProduct.tmpSearchMode = "Entry Opening Balance - Product Name"
        frmSearchProduct.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        PRODUCTBindingSource.Position = PRODUCTBindingSource.Find("PRODUCT_ID", tmpSearchResult)
        PRODUCT_IDTextBox.Text = PRODUCTBindingSource.Current("PRODUCT_CODE")
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        tmpChange = False
    End Sub

    Private Sub dtpPeriod_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpPeriod.ValueChanged
        tmpChange = True
    End Sub

    Private Sub QUANTITYTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QUANTITYTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub PRICETextBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles PRICETextBox.LostFocus
        PRICETextBox.Text = FormatNumber(PRICETextBox.Text, 0)
    End Sub

    Private Sub PRICETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PRICETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub WAREHOUSE_IDComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WAREHOUSE_IDComboBox.SelectedIndexChanged
        tmpChange = True
    End Sub

    Private Sub txtDTP_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDTP.KeyDown
        If e.KeyCode = Keys.Delete Then
            e.Handled = True
            Exit Sub
        End If
    End Sub

    Private Sub txtDTP_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDTP.KeyPress
        e.KeyChar = ""
    End Sub

    Private Sub frmEntryOpeningBalance_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
        If tmpNoData Then Me.Close()
    End Sub

    Private Sub WAREHOUSEBindingSource_CurrentChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles WAREHOUSEBindingSource.CurrentChanged
        If WAREHOUSE_IDComboBox.Text <> "" Then
            If WAREHOUSEBindingSource.Current("WAREHOUSE_ID") = -1 Then
                If Language = "Indonesian" Then
                    MsgBox("Silahkan pilih gudang lain ." & vbCrLf & _
                   "Ini adalah Gudang barang rusak !", MsgBoxStyle.Critical, "DMI Retail")

                Else
                    MsgBox("Please enter other warehouse ." & vbCrLf & _
                        "This Warehouse is Rummage !", MsgBoxStyle.Critical, "DMI Retail")
                End If
                WAREHOUSEBindingSource.Position = 0
                Exit Sub
            End If
        End If
    End Sub

    Private Sub txtOpeningBalanceNo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOpeningBalanceNo.KeyDown
        If e.KeyCode = Keys.Delete Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtOpeningBalanceNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtOpeningBalanceNo.KeyPress
        e.KeyChar = ""
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If txtOpeningBalanceNo.Text = "" Then Exit Sub

        If Trim(RsAccessPrivilege.Item("ALLOW_DELETE").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        Dim tmpCheck As String = ""
        Dim tmpResult As Integer

        OPENING_BALANCETableAdapter.SP_CHECK_CLOSING_PERIOD(dtpPeriod.Value, tmpCheck)
        OPENING_BALANCETableAdapter.SP_CHECK_OPENING_BALANCE_USED(txtOpeningBalanceNo.Text, tmpResult)

        If tmpResult > 0 Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menghapus transaksi ini!" & vbCrLf & _
                          "Produk ini telah digunakan di transaksi yang lain !", _
                       MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot delete this transaction !" & vbCrLf & _
                         "Product is used by another transaction!", _
                      MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpCheck = "TRUE" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menghapus transaksi in!" & vbCrLf & "Transaksi ini telah masuk dalam proses Tutup Buku", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Delete this Transaction!" & vbCrLf & "This Transaction has been in Closed Period", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If Language = "Indonesian" Then
            If MsgBox("Batalkan transaksi ini?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = _
                MsgBoxResult.No Then Exit Sub
        Else
            If MsgBox("Void this transaction?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = _
               MsgBoxResult.No Then Exit Sub
        End If

        Dim tmpProductID As Integer
        PRODUCTTableAdapter.SP_GET_PRODUCT_ID(PRODUCT_IDTextBox.Text, tmpProductID)

        ''''Void Opening Balance''''
        OPENING_BALANCETableAdapter.SP_OPENING_BALANCE("v", _
                                                     0, _
                                                     txtOpeningBalanceNo.Text, _
                                                     Now, _
                                                     0, _
                                                     0, _
                                                     0, _
                                                     0, _
                                                     0, _
                                                     DateSerial(4000, 12, 31), _
                                                     USER_ID, _
                                                     Now)

        ''''Insert into table Product Warehouse''''
        PRODUCT_WAREHOUSETableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                             0, _
                                                             tmpProductID, _
                                                             tmpWarehouseID, _
                                                             CInt(QUANTITYTextBox.Text) * (-1), _
                                                             USER_ID, _
                                                             Now, _
                                                             USER_ID, _
                                                             DateSerial(4000, 12, 31))

        If Language = "Indonesian" Then
            MsgBox("Transaksi ini telah di batalkan", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("This Transaction has been Voided", MsgBoxStyle.Information, "DMI Retail")
        End If
        Me.Close()

    End Sub
End Class