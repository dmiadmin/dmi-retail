﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCompanyInfo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCompanyInfo))
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblTax = New System.Windows.Forms.Label()
        Me.lblOwner = New System.Windows.Forms.Label()
        Me.lblFax = New System.Windows.Forms.Label()
        Me.lblPhone2 = New System.Windows.Forms.Label()
        Me.lblPhone1 = New System.Windows.Forms.Label()
        Me.lblPos = New System.Windows.Forms.Label()
        Me.lblState = New System.Windows.Forms.Label()
        Me.lblCity = New System.Windows.Forms.Label()
        Me.lblRegency = New System.Windows.Forms.Label()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.lblCompany = New System.Windows.Forms.Label()
        Me.COMPANY_NAMETextBox = New System.Windows.Forms.TextBox()
        Me.ADDRESS_LINE_1TextBox = New System.Windows.Forms.TextBox()
        Me.ADDRESS_LINE_2TextBox = New System.Windows.Forms.TextBox()
        Me.ADDRESS_LINE_3TextBox = New System.Windows.Forms.TextBox()
        Me.REGENCYTextBox = New System.Windows.Forms.TextBox()
        Me.CITYTextBox = New System.Windows.Forms.TextBox()
        Me.STATETextBox = New System.Windows.Forms.TextBox()
        Me.POSTAL_CODETextBox = New System.Windows.Forms.TextBox()
        Me.PHONE_1TextBox = New System.Windows.Forms.TextBox()
        Me.PHONE_2TextBox = New System.Windows.Forms.TextBox()
        Me.FAXTextBox = New System.Windows.Forms.TextBox()
        Me.OWNER_NAMETextBox = New System.Windows.Forms.TextBox()
        Me.TAX_NUMBERTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdUndo)
        Me.GroupBox2.Controls.Add(Me.cmdSave)
        Me.GroupBox2.Controls.Add(Me.cmdEdit)
        Me.GroupBox2.Controls.Add(Me.cmdDelete)
        Me.GroupBox2.Controls.Add(Me.cmdAdd)
        Me.GroupBox2.Location = New System.Drawing.Point(18, 408)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Size = New System.Drawing.Size(515, 55)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        '
        'cmdUndo
        '
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(220, 17)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(74, 29)
        Me.cmdUndo.TabIndex = 6
        Me.cmdUndo.Text = "&Undo"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSave.Location = New System.Drawing.Point(321, 17)
        Me.cmdSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(74, 29)
        Me.cmdSave.TabIndex = 7
        Me.cmdSave.Text = "&Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(119, 17)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(74, 29)
        Me.cmdEdit.TabIndex = 5
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(422, 17)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(74, 29)
        Me.cmdDelete.TabIndex = 8
        Me.cmdDelete.Text = "&Delete"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(18, 17)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(74, 29)
        Me.cmdAdd.TabIndex = 4
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblTax)
        Me.GroupBox1.Controls.Add(Me.lblOwner)
        Me.GroupBox1.Controls.Add(Me.lblFax)
        Me.GroupBox1.Controls.Add(Me.lblPhone2)
        Me.GroupBox1.Controls.Add(Me.lblPhone1)
        Me.GroupBox1.Controls.Add(Me.lblPos)
        Me.GroupBox1.Controls.Add(Me.lblState)
        Me.GroupBox1.Controls.Add(Me.lblCity)
        Me.GroupBox1.Controls.Add(Me.lblRegency)
        Me.GroupBox1.Controls.Add(Me.lblAddress)
        Me.GroupBox1.Controls.Add(Me.lblCompany)
        Me.GroupBox1.Controls.Add(Me.COMPANY_NAMETextBox)
        Me.GroupBox1.Controls.Add(Me.ADDRESS_LINE_1TextBox)
        Me.GroupBox1.Controls.Add(Me.ADDRESS_LINE_2TextBox)
        Me.GroupBox1.Controls.Add(Me.ADDRESS_LINE_3TextBox)
        Me.GroupBox1.Controls.Add(Me.REGENCYTextBox)
        Me.GroupBox1.Controls.Add(Me.CITYTextBox)
        Me.GroupBox1.Controls.Add(Me.STATETextBox)
        Me.GroupBox1.Controls.Add(Me.POSTAL_CODETextBox)
        Me.GroupBox1.Controls.Add(Me.PHONE_1TextBox)
        Me.GroupBox1.Controls.Add(Me.PHONE_2TextBox)
        Me.GroupBox1.Controls.Add(Me.FAXTextBox)
        Me.GroupBox1.Controls.Add(Me.OWNER_NAMETextBox)
        Me.GroupBox1.Controls.Add(Me.TAX_NUMBERTextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(527, 390)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        '
        'lblTax
        '
        Me.lblTax.Location = New System.Drawing.Point(6, 351)
        Me.lblTax.Name = "lblTax"
        Me.lblTax.Size = New System.Drawing.Size(170, 15)
        Me.lblTax.TabIndex = 36
        Me.lblTax.Text = "Tax Number"
        Me.lblTax.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblOwner
        '
        Me.lblOwner.Location = New System.Drawing.Point(6, 323)
        Me.lblOwner.Name = "lblOwner"
        Me.lblOwner.Size = New System.Drawing.Size(170, 15)
        Me.lblOwner.TabIndex = 35
        Me.lblOwner.Text = "Owner Name"
        Me.lblOwner.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblFax
        '
        Me.lblFax.Location = New System.Drawing.Point(6, 295)
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Size = New System.Drawing.Size(170, 15)
        Me.lblFax.TabIndex = 34
        Me.lblFax.Text = "Fax"
        Me.lblFax.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblPhone2
        '
        Me.lblPhone2.Location = New System.Drawing.Point(6, 267)
        Me.lblPhone2.Name = "lblPhone2"
        Me.lblPhone2.Size = New System.Drawing.Size(170, 15)
        Me.lblPhone2.TabIndex = 33
        Me.lblPhone2.Text = "Phone 2"
        Me.lblPhone2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblPhone1
        '
        Me.lblPhone1.Location = New System.Drawing.Point(6, 239)
        Me.lblPhone1.Name = "lblPhone1"
        Me.lblPhone1.Size = New System.Drawing.Size(170, 15)
        Me.lblPhone1.TabIndex = 32
        Me.lblPhone1.Text = "Phone 1"
        Me.lblPhone1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblPos
        '
        Me.lblPos.Location = New System.Drawing.Point(6, 211)
        Me.lblPos.Name = "lblPos"
        Me.lblPos.Size = New System.Drawing.Size(170, 15)
        Me.lblPos.TabIndex = 31
        Me.lblPos.Text = "Postal Code"
        Me.lblPos.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblState
        '
        Me.lblState.Location = New System.Drawing.Point(6, 183)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(170, 15)
        Me.lblState.TabIndex = 30
        Me.lblState.Text = "State"
        Me.lblState.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblCity
        '
        Me.lblCity.Location = New System.Drawing.Point(6, 155)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(170, 15)
        Me.lblCity.TabIndex = 29
        Me.lblCity.Text = "City"
        Me.lblCity.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblRegency
        '
        Me.lblRegency.Location = New System.Drawing.Point(6, 127)
        Me.lblRegency.Name = "lblRegency"
        Me.lblRegency.Size = New System.Drawing.Size(170, 15)
        Me.lblRegency.TabIndex = 28
        Me.lblRegency.Text = "Regency"
        Me.lblRegency.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblAddress
        '
        Me.lblAddress.Location = New System.Drawing.Point(6, 54)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(170, 15)
        Me.lblAddress.TabIndex = 27
        Me.lblAddress.Text = "Address"
        Me.lblAddress.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblCompany
        '
        Me.lblCompany.Location = New System.Drawing.Point(6, 29)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(170, 19)
        Me.lblCompany.TabIndex = 26
        Me.lblCompany.Text = "Company Name"
        Me.lblCompany.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'COMPANY_NAMETextBox
        '
        Me.COMPANY_NAMETextBox.Location = New System.Drawing.Point(182, 26)
        Me.COMPANY_NAMETextBox.Name = "COMPANY_NAMETextBox"
        Me.COMPANY_NAMETextBox.Size = New System.Drawing.Size(339, 25)
        Me.COMPANY_NAMETextBox.TabIndex = 1
        Me.COMPANY_NAMETextBox.Tag = "M"
        '
        'ADDRESS_LINE_1TextBox
        '
        Me.ADDRESS_LINE_1TextBox.Location = New System.Drawing.Point(182, 54)
        Me.ADDRESS_LINE_1TextBox.Name = "ADDRESS_LINE_1TextBox"
        Me.ADDRESS_LINE_1TextBox.Size = New System.Drawing.Size(339, 25)
        Me.ADDRESS_LINE_1TextBox.TabIndex = 3
        Me.ADDRESS_LINE_1TextBox.Tag = "M"
        '
        'ADDRESS_LINE_2TextBox
        '
        Me.ADDRESS_LINE_2TextBox.Location = New System.Drawing.Point(182, 75)
        Me.ADDRESS_LINE_2TextBox.Name = "ADDRESS_LINE_2TextBox"
        Me.ADDRESS_LINE_2TextBox.Size = New System.Drawing.Size(339, 25)
        Me.ADDRESS_LINE_2TextBox.TabIndex = 5
        '
        'ADDRESS_LINE_3TextBox
        '
        Me.ADDRESS_LINE_3TextBox.Location = New System.Drawing.Point(182, 96)
        Me.ADDRESS_LINE_3TextBox.Name = "ADDRESS_LINE_3TextBox"
        Me.ADDRESS_LINE_3TextBox.Size = New System.Drawing.Size(339, 25)
        Me.ADDRESS_LINE_3TextBox.TabIndex = 7
        '
        'REGENCYTextBox
        '
        Me.REGENCYTextBox.Location = New System.Drawing.Point(182, 124)
        Me.REGENCYTextBox.Name = "REGENCYTextBox"
        Me.REGENCYTextBox.Size = New System.Drawing.Size(171, 25)
        Me.REGENCYTextBox.TabIndex = 9
        '
        'CITYTextBox
        '
        Me.CITYTextBox.Location = New System.Drawing.Point(182, 152)
        Me.CITYTextBox.Name = "CITYTextBox"
        Me.CITYTextBox.Size = New System.Drawing.Size(171, 25)
        Me.CITYTextBox.TabIndex = 11
        Me.CITYTextBox.Tag = "M"
        '
        'STATETextBox
        '
        Me.STATETextBox.Location = New System.Drawing.Point(182, 180)
        Me.STATETextBox.Name = "STATETextBox"
        Me.STATETextBox.Size = New System.Drawing.Size(171, 25)
        Me.STATETextBox.TabIndex = 13
        '
        'POSTAL_CODETextBox
        '
        Me.POSTAL_CODETextBox.Location = New System.Drawing.Point(182, 208)
        Me.POSTAL_CODETextBox.MaxLength = 5
        Me.POSTAL_CODETextBox.Name = "POSTAL_CODETextBox"
        Me.POSTAL_CODETextBox.Size = New System.Drawing.Size(106, 25)
        Me.POSTAL_CODETextBox.TabIndex = 15
        '
        'PHONE_1TextBox
        '
        Me.PHONE_1TextBox.Location = New System.Drawing.Point(182, 236)
        Me.PHONE_1TextBox.Name = "PHONE_1TextBox"
        Me.PHONE_1TextBox.Size = New System.Drawing.Size(171, 25)
        Me.PHONE_1TextBox.TabIndex = 17
        '
        'PHONE_2TextBox
        '
        Me.PHONE_2TextBox.Location = New System.Drawing.Point(182, 264)
        Me.PHONE_2TextBox.Name = "PHONE_2TextBox"
        Me.PHONE_2TextBox.Size = New System.Drawing.Size(171, 25)
        Me.PHONE_2TextBox.TabIndex = 19
        '
        'FAXTextBox
        '
        Me.FAXTextBox.Location = New System.Drawing.Point(182, 292)
        Me.FAXTextBox.Name = "FAXTextBox"
        Me.FAXTextBox.Size = New System.Drawing.Size(171, 25)
        Me.FAXTextBox.TabIndex = 21
        '
        'OWNER_NAMETextBox
        '
        Me.OWNER_NAMETextBox.Location = New System.Drawing.Point(182, 320)
        Me.OWNER_NAMETextBox.Name = "OWNER_NAMETextBox"
        Me.OWNER_NAMETextBox.Size = New System.Drawing.Size(171, 25)
        Me.OWNER_NAMETextBox.TabIndex = 23
        '
        'TAX_NUMBERTextBox
        '
        Me.TAX_NUMBERTextBox.Location = New System.Drawing.Point(182, 348)
        Me.TAX_NUMBERTextBox.Name = "TAX_NUMBERTextBox"
        Me.TAX_NUMBERTextBox.Size = New System.Drawing.Size(171, 25)
        Me.TAX_NUMBERTextBox.TabIndex = 25
        '
        'frmCompanyInfo
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(551, 472)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmCompanyInfo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Company Information"
        Me.Text = "Company Information"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents COMPANY_INFORMATIONBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents COMPANY_NAMETextBox As System.Windows.Forms.TextBox
    Friend WithEvents ADDRESS_LINE_1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents ADDRESS_LINE_2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents ADDRESS_LINE_3TextBox As System.Windows.Forms.TextBox
    Friend WithEvents REGENCYTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CITYTextBox As System.Windows.Forms.TextBox
    Friend WithEvents STATETextBox As System.Windows.Forms.TextBox
    Friend WithEvents POSTAL_CODETextBox As System.Windows.Forms.TextBox
    Friend WithEvents PHONE_1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents PHONE_2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents FAXTextBox As System.Windows.Forms.TextBox
    Friend WithEvents OWNER_NAMETextBox As System.Windows.Forms.TextBox
    Friend WithEvents TAX_NUMBERTextBox As System.Windows.Forms.TextBox
    Friend WithEvents lblTax As System.Windows.Forms.Label
    Friend WithEvents lblOwner As System.Windows.Forms.Label
    Friend WithEvents lblFax As System.Windows.Forms.Label
    Friend WithEvents lblPhone2 As System.Windows.Forms.Label
    Friend WithEvents lblPhone1 As System.Windows.Forms.Label
    Friend WithEvents lblPos As System.Windows.Forms.Label
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents lblRegency As System.Windows.Forms.Label
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents lblCompany As System.Windows.Forms.Label
End Class
