﻿Public Class frmListSaleOrderHeader

    'Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Me.SP_LIST_SALE_ORDER_HEADERTableAdapter.Fill(Me.DS_SALE_ORDER.SP_LIST_SALE_ORDER_HEADER, New System.Nullable(Of Date)(CType(PERIOD1ToolStripTextBox.Text, Date)), New System.Nullable(Of Date)(CType(PERIOD2ToolStripTextBox.Text, Date)))
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    Private Sub frmListSaleOrderHeader_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DateTimePicker1.Value = DateSerial(Today.Year, Today.Month, 1)
        DateTimePicker2.Value = DateSerial(Today.Year, Today.Month + 1, 0)

        If Language = "Indonesian" Then
            Me.Text = "Daftar Pesanan Penjualan"
            lblFrom.Text = "Periode"
            cmdGenerate.Text = "Proses"
            cmdPrint.Text = "Cetak [F12]"

            SP_LIST_SALE_ORDER_HEADERDataGridView.Columns("SALE_ORDER_NUMBER").HeaderText = "No. Pemesanan"
            SP_LIST_SALE_ORDER_HEADERDataGridView.Columns("SALE_ORDER_DATE").HeaderText = "Tgl. Pemesanan"
            SP_LIST_SALE_ORDER_HEADERDataGridView.Columns("NO_OF_ITEM").HeaderText = "Jumlah Item"
            SP_LIST_SALE_ORDER_HEADERDataGridView.Columns("SHIP_TO").HeaderText = "Dikirim ke"
            SP_LIST_SALE_ORDER_HEADERDataGridView.Columns("DELIVERY_REQUEST_DATE").HeaderText = "Tgl. Pengiriman"
            SP_LIST_SALE_ORDER_HEADERDataGridView.Columns("SALESMAN").HeaderText = "Nama Sales"
            SP_LIST_SALE_ORDER_HEADERDataGridView.Columns("VOID").HeaderText = "Batal"
        End If
    End Sub

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        Me.SP_LIST_SALE_ORDER_HEADERTableAdapter.Fill(Me.DS_SALE_ORDER.SP_LIST_SALE_ORDER_HEADER, DateTimePicker1.Value, DateTimePicker2.Value)
    End Sub

    Private Sub SP_LIST_SALE_ORDER_HEADERDataGridView_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles SP_LIST_SALE_ORDER_HEADERDataGridView.DoubleClick
        Try
            With frmEntrySaleOrder
                .dgvSaleOrder.Rows.Clear()
                .tmpSaleOrderId = SP_LIST_SALE_ORDER_HEADERBindingSource.Current("SALE_ORDER_ID")
                '.SALE_ORDER_DATEDateTimePicker.Value = SP_LIST_SALE_ORDER_HEADERBindingSource.Current("SALE_ORDER_DATE")
                .tmpCustomer = SP_LIST_SALE_ORDER_HEADERBindingSource.Current("CUSTOMER_ID")
                .tmpTotalItem = SP_LIST_SALE_ORDER_HEADERBindingSource.Current("NO_OF_ITEM")
                '.SHIP_TOTextBox.Text = SP_LIST_SALE_ORDER_HEADERBindingSource.Current("SHIP_TO")
                '.DELIVERY_REQUEST_DATEDateTimePicker.Value = SP_LIST_SALE_ORDER_HEADERBindingSource.Current("DELIVERY_REQUEST_DATE")
                .tmpSalesman = SP_LIST_SALE_ORDER_HEADERBindingSource.Current("SALESMAN_ID")
                .GRAND_TOTALTextBox.Text = SP_LIST_SALE_ORDER_HEADERBindingSource.Current("GRAND_TOTAL")
                .SALE_ORDER_NUMBERTextBox.Text = SP_LIST_SALE_ORDER_HEADERBindingSource.Current("SALE_ORDER_NUMBER")
                .SALE_ORDER_DATEDateTimePicker.Value = SP_LIST_SALE_ORDER_HEADERBindingSource.Current("SALE_ORDER_DATE")
                .DELIVERY_REQUEST_DATEDateTimePicker.Value = SP_LIST_SALE_ORDER_HEADERBindingSource.Current("DELIVERY_REQUEST_DATE")
                .SHIP_TOTextBox.Text = SP_LIST_SALE_ORDER_HEADERBindingSource.Current("SHIP_TO")
                .tmpSaveMode = "Update"
            End With

        frmEntrySaleOrder.ShowDialog()
        frmEntrySaleOrder.Close()
        Catch
        End Try

    End Sub

    'Private Sub cmdPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrint.Click
    '    Dim objf As New frmRepSaleOrder
    '    objf.date1 = DateTimePicker1.Value
    '    objf.date2 = DateTimePicker2.Value
    '    objf.ReportViewer1.ShowRefreshButton = False
    '    objf.ReportViewer1.ZoomPercent = 100
    '    objf.WindowState = FormWindowState.Maximized
    '    objf.Show()
    'End Sub

End Class