﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Windows.Forms

Public Class frmListCustomer

    Private Sub frmListCustomer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_CUSTOMER.SP_LIST_CUSTOMER' table. You can move, or remove it, as needed.
        Dim xDS_Customer As New DataSet
        xComm = New SqlCommand

        With xComm
            .Connection = xConn
            .CommandText = "SELECT CUSTOMER_ID ," & _
            " CUSTOMER_NO," & _
            " CUSTOMER_NAME," & _
            " CUSTOMER_ADDRESS," & _
            " CUSTOMER_PHONE," & _
            " CUSTOMER_FAX," & _
            " CUSTOMER_CITY," & _
            " CUSTOMER_CONTACT_PERSON," & _
            " DISCOUNT," & _
            " EFFECTIVE_START_DATE," & _
            " EFFECTIVE_END_DATE" & _
            " FROM CUSTOMER" & _
            " ORDER BY CUSTOMER_NAME"
            .CommandType = CommandType.Text
        End With
        xAdoAdapter = New SqlDataAdapter
        xAdoAdapter.SelectCommand = xComm
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xAdoAdapter.Fill(xDS_Customer, "Customer")
        Me.dgvListCustomer.DataSource = xDS_Customer.Tables("Customer")

        Label2.Visible = True
        If Language = "Indonesian" Then
            Me.Text = "Daftar Pelanggan"
            Label1.Text = "Nama Pelanggan"
            CheckBox1.Text = "Aktif saja"
            dgvListCustomer.Columns("CUSTOMER_ID").Visible = False
            dgvListCustomer.Columns("EFFECTIVE_END_DATE").Visible = False
            dgvListCustomer.Columns("EFFECTIVE_START_DATE").Visible = False
            dgvListCustomer.Columns("CUSTOMER_ADDRESS").HeaderText = "Alamat "
            dgvListCustomer.Columns("CUSTOMER_CITY").HeaderText = "Kota"
            dgvListCustomer.Columns("CUSTOMER_CITY").Width = 75
            dgvListCustomer.Columns("CUSTOMER_NAME").HeaderText = "Nama Pelanggan"
            dgvListCustomer.Columns("CUSTOMER_NO").HeaderText = "No Pelanggan"
            dgvListCustomer.Columns("CUSTOMER_NO").Width = 75
            dgvListCustomer.Columns("CUSTOMER_PHONE").HeaderText = "Telp."
            dgvListCustomer.Columns("CUSTOMER_PHONE").Width = 75
            dgvListCustomer.Columns("CUSTOMER_FAX").HeaderText = "Fax"
            dgvListCustomer.Columns("CUSTOMER_FAX").Width = 75
            dgvListCustomer.Columns("CUSTOMER_CONTACT_PERSON").HeaderText = "Contact Person"
            dgvListCustomer.Columns("DISCOUNT").HeaderText = "Disc"
            dgvListCustomer.Columns("DISCOUNT").Width = 50
            Label2.Text = "Jumlah Data : " & dgvListCustomer.RowCount
        Else
            dgvListCustomer.Columns("CUSTOMER_ID").Visible = False
            dgvListCustomer.Columns("EFFECTIVE_END_DATE").Visible = False
            dgvListCustomer.Columns("EFFECTIVE_START_DATE").Visible = False
            dgvListCustomer.Columns("CUSTOMER_ADDRESS").HeaderText = "Address "
            dgvListCustomer.Columns("CUSTOMER_CITY").HeaderText = "City"
            dgvListCustomer.Columns("CUSTOMER_CITY").Width = 75
            dgvListCustomer.Columns("CUSTOMER_NAME").HeaderText = "Customer Name"
            dgvListCustomer.Columns("CUSTOMER_NO").HeaderText = "Customer No"
            dgvListCustomer.Columns("CUSTOMER_NO").Width = 75
            dgvListCustomer.Columns("CUSTOMER_PHONE").HeaderText = "Telp."
            dgvListCustomer.Columns("CUSTOMER_PHONE").Width = 75
            dgvListCustomer.Columns("CUSTOMER_FAX").HeaderText = "Fax"
            dgvListCustomer.Columns("CUSTOMER_FAX").Width = 75
            dgvListCustomer.Columns("CUSTOMER_CONTACT_PERSON").HeaderText = "Contact Person"
            dgvListCustomer.Columns("DISCOUNT").HeaderText = "Disc"
            dgvListCustomer.Columns("DISCOUNT").Width = 50
            Label2.Text = "No Of Customer : " & dgvListCustomer.RowCount
        End If

    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

        If CheckBox1.Checked = True Then
            Dim xDS_Customer As New DataSet
            xComm = New SqlCommand
            With xComm
                .Connection = xConn
                .CommandText = "SELECT CUSTOMER_ID ," & _
                " CUSTOMER_NO," & _
                " CUSTOMER_NAME," & _
                " CUSTOMER_ADDRESS," & _
                " CUSTOMER_PHONE," & _
                " CUSTOMER_FAX," & _
                " CUSTOMER_CITY," & _
                " CUSTOMER_CONTACT_PERSON," & _
                " DISCOUNT," & _
                " EFFECTIVE_START_DATE," & _
                " EFFECTIVE_END_DATE" & _
                " FROM CUSTOMER" & _
                " WHERE CUSTOMER_NAME LIKE '%" & TextBox1.Text.ToUpper & "%'" & _
                " AND EFFECTIVE_END_DATE > GETDATE()" & _
                " ORDER BY CUSTOMER_NAME"
                .CommandType = CommandType.Text
            End With
            xAdoAdapter = New SqlDataAdapter
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xAdoAdapter.Fill(xDS_Customer, "Customer")
            Me.dgvListCustomer.DataSource = xDS_Customer.Tables("Customer")
        Else
            Dim xDS_Customer As New DataSet
            xComm = New SqlCommand
            With xComm
                .Connection = xConn
                .CommandText = "SELECT CUSTOMER_ID ," & _
                " CUSTOMER_NO," & _
                " CUSTOMER_NAME," & _
                " CUSTOMER_ADDRESS," & _
                " CUSTOMER_PHONE," & _
                " CUSTOMER_FAX," & _
                " CUSTOMER_CITY," & _
                " CUSTOMER_CONTACT_PERSON," & _
                " DISCOUNT," & _
                " EFFECTIVE_START_DATE," & _
                " EFFECTIVE_END_DATE" & _
                " FROM CUSTOMER" & _
                " WHERE CUSTOMER_NAME LIKE '%" & TextBox1.Text.ToUpper & "%'" & _
                " ORDER BY CUSTOMER_NAME"
                .CommandType = CommandType.Text
            End With
            xAdoAdapter = New SqlDataAdapter
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xAdoAdapter.Fill(xDS_Customer, "Customer")
            Me.dgvListCustomer.DataSource = xDS_Customer.Tables("Customer")
        End If

        If Language = "Indonesian" Then
            Label2.Text = "Jumlah Data : " & dgvListCustomer.RowCount
        Else
            Label2.Text = "No Of Data : " & dgvListCustomer.RowCount
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            Dim xDS_Customer As New DataSet
            xComm = New SqlCommand
            With xComm
                .Connection = xConn
                .CommandText = "SELECT CUSTOMER_ID ," & _
                " CUSTOMER_NO," & _
                " CUSTOMER_NAME," & _
                " CUSTOMER_ADDRESS," & _
                " CUSTOMER_PHONE," & _
                " CUSTOMER_FAX," & _
                " CUSTOMER_CITY," & _
                " CUSTOMER_CONTACT_PERSON," & _
                " DISCOUNT," & _
                " EFFECTIVE_START_DATE," & _
                " EFFECTIVE_END_DATE" & _
                " FROM CUSTOMER" & _
                " WHERE CUSTOMER_NAME LIKE '%" & TextBox1.Text.ToUpper & "%'" & _
                " AND EFFECTIVE_END_DATE > GETDATE()" & _
                " ORDER BY CUSTOMER_NAME"
                .CommandType = CommandType.Text
            End With
            xAdoAdapter = New SqlDataAdapter
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xAdoAdapter.Fill(xDS_Customer, "Customer")
            Me.dgvListCustomer.DataSource = xDS_Customer.Tables("Customer")
        Else
            Dim xDS_Customer As New DataSet
            xComm = New SqlCommand
            With xComm
                .Connection = xConn
                .CommandText = "SELECT CUSTOMER_ID ," & _
                " CUSTOMER_NO," & _
                " CUSTOMER_NAME," & _
                " CUSTOMER_ADDRESS," & _
                " CUSTOMER_PHONE," & _
                " CUSTOMER_FAX," & _
                " CUSTOMER_CITY," & _
                " CUSTOMER_CONTACT_PERSON," & _
                " DISCOUNT," & _
                " EFFECTIVE_START_DATE," & _
                " EFFECTIVE_END_DATE" & _
                " FROM CUSTOMER" & _
                " WHERE CUSTOMER_NAME LIKE '%" & TextBox1.Text.ToUpper & "%'" & _
                " ORDER BY CUSTOMER_NAME"
                .CommandType = CommandType.Text
            End With
            xAdoAdapter = New SqlDataAdapter
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xAdoAdapter.Fill(xDS_Customer, "Customer")
            Me.dgvListCustomer.DataSource = xDS_Customer.Tables("Customer")
        End If
        If Language = "Indonesian" Then
            Label2.Text = "Jumlah Data : " & dgvListCustomer.RowCount
        Else
            Label2.Text = "No Of Data : " & dgvListCustomer.RowCount
        End If

    End Sub

    Private Sub dgvListCustomer_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListCustomer.CellDoubleClick
        HideAllForms()
        Dim tmpCustomerId As Integer = dgvListCustomer.Item("CUSTOMER_ID", e.RowIndex).Value
        frmEntryCustomer.MdiParent = ObjFormMain
        frmEntryCustomer.Show()
        tmpRecent = New frmEntryCustomer
        frmEntryCustomer.CUSTOMERBindingSource.Position = frmEntryCustomer.CUSTOMERBindingSource.Find("CUSTOMER_ID", tmpCustomerId)
        Me.Close()
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        AccessPrivilege(Me.Tag)
    End Sub

    Private Sub dgvListCustomer_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvListCustomer.CellContentClick

    End Sub
End Class