﻿Public Class frmClosingPeriod
    Dim tmpdate, tmpnow As Date


    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        If Language = "Indonesian" Then
            If MsgBox("Anda yakin ingin menutup Periode " & Format(DateTimePicker1.Value, "MMMM yyyy") & " ?" & vbCrLf & _
               "Setelah Tutup Buku, semua transaksi di bawah periode " & _
               Format(DateTimePicker1.Value, "MMMM yyyy") & " tidak dapat dilakukan!", _
               MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub

            'If CLOSED_PERIODTableAdapter.SP_CLOSED_PERIOD_PROCESS("G", DateTimePicker1.Value) = "TRUE" Then
            '    MsgBox("Tutup Buku sedang dilakukan oleh User lain!" & vbCrLf & _
            '            "Mohon tunggu hingga proses selesai!", MsgBoxStyle.Critical, "DMI Retail")
            '    Exit Sub
            'End If

        Else

            If MsgBox("Are You sure to Close " & Format(DateTimePicker1.Value, "MMMM yyyy") & " Period?" & vbCrLf & _
               "After closing this period, all the changes of period before " & _
               Format(DateTimePicker1.Value, "MMMM yyyy") & " will not allowed!", _
               MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub

            'If CLOSED_PERIODTableAdapter.SP_CLOSED_PERIOD_PROCESS("G", DateTimePicker1.Value) = "TRUE" Then
            '    MsgBox("Period is being closed by another user!" & vbCrLf & _
            '            "Please wait until this process commited!", MsgBoxStyle.Critical, "DMI Retail")
            '    Exit Sub
            'End If
        End If

        tmpnow = DateSerial(DateTimePicker1.Value.Year, DateTimePicker1.Value.Month, 1)
        tmpdate = DateSerial(tmpnow.Year, _
                           tmpnow.Month + 1, _
                           tmpnow.Day - 1)

        'CLOSED_PERIODTableAdapter.SP_CLOSED_PERIOD_PROCESS("P", DateTimePicker1.Value)

        'CLOSED_PERIODTableAdapter.SP_CLOSED_PERIOD("I", _
        '                                           0, _
        '                                           tmpnow, _
        '                                           USER_ID, _
        '                                           Now, _
        '                                           0, _
        '                                           DateSerial(4000, 12, 31))
        ProgressBar1.Visible = True
        Timer1.Enabled = True

        'CLOSED_PERIODTableAdapter.SP_CLOSED_PERIOD_PROCESS("C", DateTimePicker1.Value)

        'CLOSED_PERIODTableAdapter.SP_ENDING_STOCK(tmpdate, _
        '                                          mdlGeneral.USER_ID, _
        '                                          Now, _
        '                                          0, _
        '                                          DateSerial(4000, 12, 31))
        'Me.Close()
    End Sub

    Private Sub frmClosingPeriod_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DateTimePicker1.Value = DateSerial(Today.Year, Today.Month, 1)
        ProgressBar1.Visible = False

        If Language = "Indonesian" Then
            cmdSave.Text = "&Proses..!!"
            Me.Text = "Tutup Buku - Bulan"
        End If

    End Sub

    Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        ProgressBar1.Value += 2

        If Language = "Indonesian" Then
            If ProgressBar1.Value <= 50 Then
                Label1.Text = "Sedang Proses ..."
            Else
                Label1.Text = "Mohon Tunggu ..."
            End If
        Else
            If ProgressBar1.Value <= 50 Then
                Label1.Text = "loading..."
            Else
                Label1.Text = "please wait..."
            End If
        End If
        
        If ProgressBar1.Value = 100 Then
            Timer1.Enabled = False
            Me.Close()
            If Language = "Indonesian" Then
                MsgBox("Tutup Buku berhasil.", MsgBoxStyle.Information, "DMI Retail")
            Else
                MsgBox("Closing Period Successful.", MsgBoxStyle.Information, "DMI Retail")
            End If

        End If
    End Sub
End Class