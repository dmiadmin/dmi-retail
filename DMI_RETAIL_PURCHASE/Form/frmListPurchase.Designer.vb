﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListPurchase
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListPurchase))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblNoOfTrans = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdReport = New System.Windows.Forms.Button()
        Me.lblTo = New System.Windows.Forms.Label()
        Me.lblFrom = New System.Windows.Forms.Label()
        Me.cmdGenerate = New System.Windows.Forms.Button()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.VIEW_LIST_PURCHASE_DETAILBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VIEW_LIST_PURCHASE_DETAILTableAdapter = New DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.VIEW_LIST_PURCHASE_DETAILTableAdapter()
        Me.SP_LIST_PURCHASEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_LIST_PURCHASETableAdapter = New DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.SP_LIST_PURCHASETableAdapter()
        Me.SP_LIST_PURCHASEDataGridView = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2.SuspendLayout()
        CType(Me.VIEW_LIST_PURCHASE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_LIST_PURCHASEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_LIST_PURCHASEDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblNoOfTrans
        '
        Me.lblNoOfTrans.AutoSize = True
        Me.lblNoOfTrans.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoOfTrans.Location = New System.Drawing.Point(27, 416)
        Me.lblNoOfTrans.Name = "lblNoOfTrans"
        Me.lblNoOfTrans.Size = New System.Drawing.Size(48, 15)
        Me.lblNoOfTrans.TabIndex = 2
        Me.lblNoOfTrans.Text = "Label1"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdReport)
        Me.GroupBox2.Controls.Add(Me.lblTo)
        Me.GroupBox2.Controls.Add(Me.lblFrom)
        Me.GroupBox2.Controls.Add(Me.cmdGenerate)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox2.Location = New System.Drawing.Point(11, 15)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(826, 107)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        '
        'cmdReport
        '
        Me.cmdReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdReport.Image = Global.DMI_RETAIL_PURCHASE.My.Resources.Resources.Printer_small
        Me.cmdReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdReport.Location = New System.Drawing.Point(730, 77)
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(90, 24)
        Me.cmdReport.TabIndex = 16
        Me.cmdReport.Text = "Print [F12]"
        Me.cmdReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.cmdReport.UseVisualStyleBackColor = True
        '
        'lblTo
        '
        Me.lblTo.AutoSize = True
        Me.lblTo.Location = New System.Drawing.Point(439, 21)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(23, 15)
        Me.lblTo.TabIndex = 8
        Me.lblTo.Text = "To"
        '
        'lblFrom
        '
        Me.lblFrom.AutoSize = True
        Me.lblFrom.Location = New System.Drawing.Point(216, 21)
        Me.lblFrom.Name = "lblFrom"
        Me.lblFrom.Size = New System.Drawing.Size(79, 15)
        Me.lblFrom.TabIndex = 7
        Me.lblFrom.Text = "From Period"
        '
        'cmdGenerate
        '
        Me.cmdGenerate.Image = CType(resources.GetObject("cmdGenerate.Image"), System.Drawing.Image)
        Me.cmdGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdGenerate.Location = New System.Drawing.Point(374, 55)
        Me.cmdGenerate.Name = "cmdGenerate"
        Me.cmdGenerate.Size = New System.Drawing.Size(99, 39)
        Me.cmdGenerate.TabIndex = 5
        Me.cmdGenerate.Text = "Generate"
        Me.cmdGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdGenerate.UseVisualStyleBackColor = True
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.CustomFormat = "dd-MMM-yyyy"
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker2.Location = New System.Drawing.Point(468, 18)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(117, 22)
        Me.DateTimePicker2.TabIndex = 4
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.CustomFormat = "dd-MMM-yyyy"
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker1.Location = New System.Drawing.Point(301, 18)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(117, 22)
        Me.DateTimePicker1.TabIndex = 3
        '
        'VIEW_LIST_PURCHASE_DETAILTableAdapter
        '
        Me.VIEW_LIST_PURCHASE_DETAILTableAdapter.ClearBeforeFill = True
        '
        'SP_LIST_PURCHASETableAdapter
        '
        Me.SP_LIST_PURCHASETableAdapter.ClearBeforeFill = True
        '
        'SP_LIST_PURCHASEDataGridView
        '
        Me.SP_LIST_PURCHASEDataGridView.AllowUserToAddRows = False
        Me.SP_LIST_PURCHASEDataGridView.AllowUserToDeleteRows = False
        Me.SP_LIST_PURCHASEDataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SP_LIST_PURCHASEDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.SP_LIST_PURCHASEDataGridView.ColumnHeadersHeight = 53
        Me.SP_LIST_PURCHASEDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.SP_LIST_PURCHASEDataGridView.Location = New System.Drawing.Point(6, 21)
        Me.SP_LIST_PURCHASEDataGridView.Name = "SP_LIST_PURCHASEDataGridView"
        Me.SP_LIST_PURCHASEDataGridView.ReadOnly = True
        Me.SP_LIST_PURCHASEDataGridView.RowHeadersWidth = 32
        Me.SP_LIST_PURCHASEDataGridView.Size = New System.Drawing.Size(814, 385)
        Me.SP_LIST_PURCHASEDataGridView.TabIndex = 12
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.SP_LIST_PURCHASEDataGridView)
        Me.GroupBox1.Controls.Add(Me.lblNoOfTrans)
        Me.GroupBox1.Location = New System.Drawing.Point(11, 128)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(826, 443)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        '
        'frmListPurchase
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(848, 583)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmListPurchase"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "List Purchase"
        Me.Text = "List Purchase"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.VIEW_LIST_PURCHASE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_LIST_PURCHASEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_LIST_PURCHASEDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents VIEW_LIST_PURCHASE_DETAILBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VIEW_LIST_PURCHASE_DETAILTableAdapter As DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.VIEW_LIST_PURCHASE_DETAILTableAdapter
    Friend WithEvents lblNoOfTrans As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents lblFrom As System.Windows.Forms.Label
    Friend WithEvents cmdGenerate As System.Windows.Forms.Button
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents SP_LIST_PURCHASEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_LIST_PURCHASETableAdapter As DMI_RETAIL_PURCHASE.DS_PURCHASETableAdapters.SP_LIST_PURCHASETableAdapter
    Friend WithEvents SP_LIST_PURCHASEDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents cmdReport As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
End Class
