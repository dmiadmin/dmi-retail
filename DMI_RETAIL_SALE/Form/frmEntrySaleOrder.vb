﻿Public Class frmEntrySaleOrder
    Public tmpSaveMode, tmpNumber As String
    Public tmpCustomer, tmpSalesman, tmpSaleOrderId, tmpTotalItem As Integer
    Dim tmpInput As String
    Dim tmpDataGridViewLoaded, tmpChange, tmpPrint As Boolean
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    Private Sub frmEntrySaleOrder_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If tmpSaveMode = "Update" Then
            If e.KeyCode = Keys.F1 Then Exit Sub
            If e.KeyCode = Keys.F2 Then Exit Sub
            If e.KeyCode = Keys.F3 Then Exit Sub
            If e.KeyCode = Keys.F5 Then Exit Sub
            If e.KeyCode = Keys.F6 Then Exit Sub
            If e.KeyCode = Keys.F7 Then Exit Sub
            If e.KeyCode = Keys.F10 Then Exit Sub
        Else

            If e.KeyCode = Keys.F2 Then
                TAXPercentageTEXTBOX.Focus()
            ElseIf e.KeyCode = Keys.F5 Then
                'CUSTOMER_NAMEComboBox.Enabled = False
                tmpInput = "Browse"
                dgvSaleOrder.CancelEdit()
                mdlGeneral.tmpSearchMode = "SALE - Product Code"
                Dim frmSearchProduct As New frmSearchProduct
                frmSearchProduct.ShowDialog(Me)
                If tmpSearchResult = "" Then
                    tmpInput = ""
                    Exit Sub
                End If
                PRODUCTBindingSource.Filter = "PRODUCT_ID = " & tmpSearchResult
                dgvSaleOrder.Rows.Insert(dgvSaleOrder.RowCount - 1)
                dgvSaleOrder.CurrentCell = dgvSaleOrder.Rows(dgvSaleOrder.RowCount - 1).Cells(0)
                dgvSaleOrder.Item(0, dgvSaleOrder.RowCount - 1).Value = _
                    PRODUCTBindingSource.Current("PRODUCT_CODE")
                dgvSaleOrder.Item(0, dgvSaleOrder.RowCount - 1).Value = ""
                dgvSaleOrder.Item(7, dgvSaleOrder.RowCount - 2).Value = "Open"
            ElseIf e.KeyCode = Keys.F6 Then
                'CUSTOMER_NAMEComboBox.Enabled = False
                tmpInput = "Browse"
                dgvSaleOrder.CancelEdit()
                mdlGeneral.tmpSearchMode = "SALE - Product Name"
                Dim frmSearchProduct As New frmSearchProduct
                frmSearchProduct.ShowDialog(Me)
                If tmpSearchResult = "" Then
                    tmpInput = ""
                    Exit Sub
                End If
                PRODUCTBindingSource.Filter = "PRODUCT_ID = " & tmpSearchResult
                dgvSaleOrder.Rows.Insert(dgvSaleOrder.RowCount - 1)
                dgvSaleOrder.CurrentCell = dgvSaleOrder.Rows(dgvSaleOrder.RowCount - 1).Cells(0)
                dgvSaleOrder.Item(0, dgvSaleOrder.RowCount - 1).Value = _
                    PRODUCTBindingSource.Current("PRODUCT_CODE")
                dgvSaleOrder.Item(0, dgvSaleOrder.RowCount - 1).Value = ""
                dgvSaleOrder.Item(7, dgvSaleOrder.RowCount - 2).Value = "Open"
            ElseIf e.KeyCode = Keys.F7 Then
                If cmdAdd.Enabled = False Then Exit Sub
                cmdAdd_Click(Nothing, Nothing)
            ElseIf e.KeyCode = Keys.F10 Then
                If cmdSave.Enabled = False Then Exit Sub
                cmdSave_Click(Nothing, Nothing)
            End If

        End If

        If e.KeyCode = Keys.F12 Then
            If cmdPrint.Enabled = False Then Exit Sub
            'cmdPrint_Click(Nothing, Nothing)
        ElseIf e.KeyCode = Keys.F9 Then
            If cmdUndo.Enabled = False Then Exit Sub
            cmdUndo_Click(Nothing, Nothing)
        ElseIf e.KeyCode = Keys.F11 Then
            If cmdDelete.Enabled = False Then Exit Sub
            cmdDelete_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub frmEntrySaleOrder_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_SALE_ORDER.SALE_ORDER_DETAIL' table. You can move, or remove it, as needed.
        Me.SALE_ORDER_DETAILTableAdapter.Fill(Me.DS_SALE_ORDER.SALE_ORDER_DETAIL)
        'TODO: This line of code loads data into the 'DS_SALE_ORDER.SALESMAN' table. You can move, or remove it, as needed.
        Me.SALESMANTableAdapter.Fill(Me.DS_SALE_ORDER.SALESMAN)
        'TODO: This line of code loads data into the 'DS_SALE_ORDER.SALESMAN' table. You can move, or remove it, as needed.
        Me.SALESMANTableAdapter.Fill(Me.DS_SALE_ORDER.SALESMAN)
        'TODO: This line of code loads data into the 'DS_SALE_ORDER.PRODUCT' table. You can move, or remove it, as needed.
        Me.PRODUCTTableAdapter.Fill(Me.DS_SALE_ORDER.PRODUCT)
        'TODO: This line of code loads data into the 'DS_SALE_ORDER.CUSTOMER' table. You can move, or remove it, as needed.
        Me.CUSTOMERTableAdapter.Fill(Me.DS_SALE_ORDER.CUSTOMER)
        'TODO: This line of code loads data into the 'DS_SALE_ORDER.SALE_ORDER_HEADER' table. You can move, or remove it, as needed.
        Me.SALE_ORDER_HEADERTableAdapter.Fill(Me.DS_SALE_ORDER.SALE_ORDER_HEADER)


        If Language = "Indonesian" Then
            Me.Text = "Input Pemesanan Penjualan"
            lblDelRequestDate.Text = "Tgl. Pengiriman"
            lblSaleOrderNo.Text = "No. Pemesanan"
            lblSaleOrderDate.Text = "Tgl. Pemesanan"
            lblShip.Text = "Dikirim Ke"
            lblF5.Text = "F5 : Cari berdasarkan Kode"
            lblF6.Text = "F6 : Cari berdasarkan Nama"
            lblDel.Text = "Del : Hapus Produk dari List"

            dgvSaleOrder.Columns("PRODUCT_CODE").HeaderText = "Kode Produk"
            dgvSaleOrder.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            dgvSaleOrder.Columns("QUANTITY").HeaderText = "Jumlah"
            dgvSaleOrder.Columns("PRICE_PER_UNIT").HeaderText = "Harga Per Unit"
            dgvSaleOrder.Columns("DELETE").HeaderText = "Hapus"

            lblSalesman.Text = "Nama Sales"
            lblTax.Text = "Pajak [F2]"

            cmdAdd.Text = "Tambah [F7]"
            cmdEdit.Text = "Ubah [F8]"
            cmdUndo.Text = "Batal [F9]"
            cmdSave.Text = "Simpan [F10]"
            cmdDelete.Text = "Hapus [F11]"
            cmdPrint.Text = "Simpan&Cetak [F12]"

        End If


        If tmpSaveMode = "Update" Then
            cmdEdit_Click(Nothing, Nothing)
            CUSTOMERBindingSource.Position = CUSTOMERBindingSource.Find("CUSTOMER_ID", tmpCustomer)
            SALESMANBindingSource.Position = SALESMANBindingSource.Find("SALESMAN_ID", tmpSalesman)
           

            For X As Integer = 0 To tmpTotalItem - 1
                tmpNumber = SALE_ORDER_NUMBERTextBox.Text
                Me.SP_DETAIL_SALE_ORDERTableAdapter.Fill(DS_SALE_ORDER.SP_DETAIL_SALE_ORDER, tmpSaleOrderId)
                dgvSaleOrder.Rows.Add()
                SP_DETAIL_SALE_ORDERBindingSource.Position = X
                dgvSaleOrder.Item("PRODUCT_CODE", X).Value = SP_DETAIL_SALE_ORDERBindingSource.Current("PRODUCT_CODE")
                dgvSaleOrder.Item("PRODUCT_NAME", X).Value = SP_DETAIL_SALE_ORDERBindingSource.Current("PRODUCT_NAME")
                dgvSaleOrder.Item("QUANTITY", X).Value = SP_DETAIL_SALE_ORDERBindingSource.Current("QUANTITY")
                dgvSaleOrder.Item("PRICE_PER_UNIT", X).Value = SP_DETAIL_SALE_ORDERBindingSource.Current("PRICE_PER_UNIT")
                dgvSaleOrder.Item("DISCOUNT", X).Value = SP_DETAIL_SALE_ORDERBindingSource.Current("DISC_PERCENTAGE")
                dgvSaleOrder.Item("DISCOUNT2", X).Value = SP_DETAIL_SALE_ORDERBindingSource.Current("DISC_AMOUNT")
                dgvSaleOrder.Item("TOTAL", X).Value = SP_DETAIL_SALE_ORDERBindingSource.Current("TOTAL")
                dgvSaleOrder.Item("STATUS", X).Value = SP_DETAIL_SALE_ORDERBindingSource.Current("STATUS")
                TAX_AMOUNTTextBox.Text = SP_DETAIL_SALE_ORDERBindingSource.Current("TAX_AMOUNT")
                TAXPercentageTEXTBOX.Text = SP_DETAIL_SALE_ORDERBindingSource.Current("TAX_PERCENTAGE")
            Next
        Else
            tmpSaveMode = "Insert"
            tmpDataGridViewLoaded = True
            'SALE_ORDER_HEADERTableAdapter.SP_GENERATE_SALE_ORDER_NO(SALE_ORDER_NUMBERTextBox.Text)
            CUSTOMERBindingSource.Position = -1
            cmdAdd.Enabled = False
            cmdEdit.Enabled = False
            cmdUndo.Enabled = False
            cmdDelete.Enabled = False
            tmpPrint = False
            SHIP_TOTextBox.Text = ""
            cmdAdd_Click(Nothing, Nothing)
            SALE_ORDER_DATEDateTimePicker.Value = Now
            DELIVERY_REQUEST_DATEDateTimePicker.Value = Now
        End If

    End Sub

    Private Sub dgvSaleOrder_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvSaleOrder.CellContentClick
        If dgvSaleOrder.Item(0, dgvSaleOrder.CurrentCell.RowIndex).Value = "" Then Exit Sub

        If dgvSaleOrder.CurrentCell.ColumnIndex = 8 Then
            dgvSaleOrder.Rows.RemoveAt(dgvSaleOrder.CurrentCell.RowIndex)
            CalculationAll(dgvSaleOrder, _
                                     5, _
                                     6, _
                                     AMOUNT_DISCOUNTTextBox, _
                                     TAXPercentageTEXTBOX, _
                                     TAX_AMOUNTTextBox, _
                                     GRAND_TOTALTextBox, _
                                     GRAND_TOTALTextBox)
            lblNoOfItem.Text = "No of Item(s) : " & dgvSaleOrder.RowCount - 1


        End If

        If dgvSaleOrder.Item("PRODUCT_CODE", 0).Value = "" Then
            TAXPercentageTEXTBOX.Text = "0"
            TAX_AMOUNTTextBox.Text = "0"
        End If
    End Sub

    Private Sub dgvSaleOrder_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvSaleOrder.CellValueChanged
        If tmpSaveMode = "Update" Then Exit Sub

        If tmpDataGridViewLoaded Then

            If dgvSaleOrder.CurrentCell.ColumnIndex = 2 Or dgvSaleOrder.CurrentCell.ColumnIndex = 3 Or dgvSaleOrder.CurrentCell.ColumnIndex = 4 Or dgvSaleOrder.CurrentCell.ColumnIndex = 5 Then
                If Not IsNumeric(dgvSaleOrder.CurrentCell.Value) Then
                    If Language = "Indonesian" Then
                        MsgBox("Anda memasukan nilai yang salah !" & vbCrLf & _
                          "Untuk Kolom : " & _
                           dgvSaleOrder.Columns(dgvSaleOrder.CurrentCell.ColumnIndex).HeaderText & vbCrLf & _
                           "Baris Nomor : " & _
                           dgvSaleOrder.CurrentCell.RowIndex + 1, MsgBoxStyle.Critical, "DMI Retail")
                        'give value "0" to empty fields or unexpected fields
                        dgvSaleOrder.CurrentCell.Value = 0
                    Else
                        MsgBox("You entered unexpected value !" & vbCrLf & _
                          "For Column : " & _
                           dgvSaleOrder.Columns(dgvSaleOrder.CurrentCell.ColumnIndex).HeaderText & vbCrLf & _
                           "Row number : " & _
                           dgvSaleOrder.CurrentCell.RowIndex + 1, MsgBoxStyle.Critical, "DMI Retail")
                        'give value "0" to empty fields or unexpected fields
                        dgvSaleOrder.CurrentCell.Value = 0
                    End If
                    Exit Sub
                End If
            End If

            If tmpFillingDGV = False Then
                If dgvSaleOrder.CurrentCell.ColumnIndex = 0 Then
                    Dim tmpProductName As String = ""
                    Dim tmpUnitPrice As Integer = 0

                    PRODUCTTableAdapter.SP_GET_PRODUCT_NAME(dgvSaleOrder.CurrentCell.Value, tmpProductName)
                    PRODUCTTableAdapter.SP_GET_SELLING_PRICE(dgvSaleOrder.CurrentCell.Value, tmpUnitPrice)

                    If tmpProductName = "" Then
                        If dgvSaleOrder.CurrentCell.Value <> "" Then
                            If Language = "Indonesian" Then
                                MsgBox("Produk tidak ditemukan", MsgBoxStyle.Critical, "DMI Retail")
                                dgvSaleOrder.Rows.RemoveAt(dgvSaleOrder.CurrentCell.RowIndex)
                            Else
                                MsgBox("Product not found!", MsgBoxStyle.Critical, "DMI Retail")
                                dgvSaleOrder.Rows.RemoveAt(dgvSaleOrder.CurrentCell.RowIndex)
                            End If
                        End If
                        Exit Sub
                    End If

                    tmpFillingDGV = True
                    AddDataToGrid(dgvSaleOrder, _
                                  dgvSaleOrder.CurrentCell.Value, _
                                  0, _
                                  tmpProductName, _
                                  1, _
                                  2, _
                                  tmpUnitPrice, _
                                  3, _
                                  4, _
                                  5, _
                                  6)
                    CalculationAll(dgvSaleOrder, _
                                     5, _
                                     6, _
                                     AMOUNT_DISCOUNTTextBox, _
                                     TAXPercentageTEXTBOX, _
                                     TAX_AMOUNTTextBox, _
                                     GRAND_TOTALTextBox, _
                                     GRAND_TOTALTextBox)
                    lblNoOfItem.Text = "No of Item(s) : " & dgvSaleOrder.RowCount - 1

                    ''Calculate Tax Amount when adding Product
                    Dim tmpGrandTotal As Integer = 0
                    For x As Integer = 0 To dgvSaleOrder.RowCount - 1
                        tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvSaleOrder.Item(6, x).Value)
                    Next
                    TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
                    TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)


                ElseIf dgvSaleOrder.CurrentCell.ColumnIndex = 4 Then

                    If dgvSaleOrder.Item(0, dgvSaleOrder.CurrentCell.RowIndex).Value = "" Then
                        dgvSaleOrder.Rows.RemoveAt(dgvSaleOrder.CurrentCell.RowIndex)
                        Exit Sub
                    End If

                    dgvSaleOrder.Item(5, dgvSaleOrder.CurrentCell.RowIndex).Value = _
                        CInt(dgvSaleOrder.Item(2, dgvSaleOrder.CurrentCell.RowIndex).Value) * _
                        CInt(dgvSaleOrder.Item(3, dgvSaleOrder.CurrentCell.RowIndex).Value) * _
                        CInt(dgvSaleOrder.Item(4, dgvSaleOrder.CurrentCell.RowIndex).Value) / 100

                    Calculation(dgvSaleOrder.Item(2, dgvSaleOrder.CurrentCell.RowIndex).Value, _
                                dgvSaleOrder.Item(3, dgvSaleOrder.CurrentCell.RowIndex).Value, _
                                dgvSaleOrder.Item(5, dgvSaleOrder.CurrentCell.RowIndex).Value, _
                                dgvSaleOrder.Item(6, dgvSaleOrder.CurrentCell.RowIndex).Value)
                    CalculationAll(dgvSaleOrder, _
                                     5, _
                                     6, _
                                     AMOUNT_DISCOUNTTextBox, _
                                     TAXPercentageTEXTBOX, _
                                     TAX_AMOUNTTextBox, _
                                     GRAND_TOTALTextBox, _
                                     GRAND_TOTALTextBox)
                    lblNoOfItem.Text = "No of Item(s) : " & dgvSaleOrder.RowCount - 1

                    ''Calculate Tax Amount when adding Product
                    Dim tmpGrandTotal As Integer = 0
                    For x As Integer = 0 To dgvSaleOrder.RowCount - 1
                        tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvSaleOrder.Item(6, x).Value)
                    Next
                    TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
                    TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)

                    If CInt(dgvSaleOrder.Item(4, dgvSaleOrder.CurrentCell.RowIndex).Value) > 100 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih besar dari 100%" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not greater than 100%" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvSaleOrder.Item(4, dgvSaleOrder.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                    If CInt(dgvSaleOrder.Item(4, dgvSaleOrder.CurrentCell.RowIndex).Value) < 0 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih kecil dari 0" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not smaller than 0" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvSaleOrder.Item(4, dgvSaleOrder.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                ElseIf dgvSaleOrder.CurrentCell.ColumnIndex = 5 Then

                    If dgvSaleOrder.Item(0, dgvSaleOrder.CurrentCell.RowIndex).Value = "" Then
                        dgvSaleOrder.Rows.Remove(dgvSaleOrder.CurrentRow)
                        Exit Sub
                    End If

                    dgvSaleOrder.Item(4, dgvSaleOrder.CurrentCell.RowIndex).Value = _
                        FormatNumber(CInt(dgvSaleOrder.Item(5, dgvSaleOrder.CurrentCell.RowIndex).Value) / _
                        (CInt(dgvSaleOrder.Item(2, dgvSaleOrder.CurrentCell.RowIndex).Value) * _
                        CInt(dgvSaleOrder.Item(3, dgvSaleOrder.CurrentCell.RowIndex).Value)) * 100, 2)

                    Calculation(dgvSaleOrder.Item(2, dgvSaleOrder.CurrentCell.RowIndex).Value, _
                                dgvSaleOrder.Item(3, dgvSaleOrder.CurrentCell.RowIndex).Value, _
                                dgvSaleOrder.Item(5, dgvSaleOrder.CurrentCell.RowIndex).Value, _
                                dgvSaleOrder.Item(6, dgvSaleOrder.CurrentCell.RowIndex).Value)
                    CalculationAll(dgvSaleOrder, _
                                     5, _
                                     6, _
                                     AMOUNT_DISCOUNTTextBox, _
                                     TAXPercentageTEXTBOX, _
                                     TAX_AMOUNTTextBox, _
                                     GRAND_TOTALTextBox, _
                                     GRAND_TOTALTextBox)
                    lblNoOfItem.Text = "No of Item(s) : " & dgvSaleOrder.RowCount - 1

                    ''Calculate Tax Amount when adding Product
                    Dim tmpGrandTotal As Integer = 0
                    For x As Integer = 0 To dgvSaleOrder.RowCount - 1
                        tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvSaleOrder.Item(6, x).Value)
                    Next
                    TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
                    TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)

                    If CInt(dgvSaleOrder.Item("DISCOUNT", dgvSaleOrder.CurrentCell.RowIndex).Value) > 100 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih besar dari 100%" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not greater than 100%" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvSaleOrder.Item("DISCOUNT", dgvSaleOrder.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                    If CInt(dgvSaleOrder.Item(5, dgvSaleOrder.CurrentCell.RowIndex).Value) < 0 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih kecil dari 0" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not smaller than 0" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvSaleOrder.Item(5, dgvSaleOrder.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                ElseIf dgvSaleOrder.CurrentCell.ColumnIndex = 7 Then

                    If dgvSaleOrder.Item(0, dgvSaleOrder.CurrentCell.RowIndex).Value = "" Then
                        dgvSaleOrder.Rows.Remove(dgvSaleOrder.CurrentRow)
                        Exit Sub
                    End If
                    

                ElseIf dgvSaleOrder.CurrentCell.ColumnIndex = 2 Or _
                    dgvSaleOrder.CurrentCell.ColumnIndex = 3 Or _
                    dgvSaleOrder.CurrentCell.ColumnIndex = 4 Or _
                    dgvSaleOrder.CurrentCell.ColumnIndex = 5 Or
                    dgvSaleOrder.CurrentCell.ColumnIndex = 7 Then

                    If dgvSaleOrder.Item(0, dgvSaleOrder.CurrentCell.RowIndex).Value = "" Then
                        dgvSaleOrder.Rows.Remove(dgvSaleOrder.CurrentRow)
                        Exit Sub
                    End If


                    dgvSaleOrder.Item(5, dgvSaleOrder.CurrentCell.RowIndex).Value = _
                        CInt(dgvSaleOrder.Item(2, dgvSaleOrder.CurrentCell.RowIndex).Value) * _
                        CInt(dgvSaleOrder.Item(3, dgvSaleOrder.CurrentCell.RowIndex).Value) * _
                        CInt(dgvSaleOrder.Item(4, dgvSaleOrder.CurrentCell.RowIndex).Value) / 100

                    Calculation(dgvSaleOrder.Item(2, dgvSaleOrder.CurrentCell.RowIndex).Value, _
                                dgvSaleOrder.Item(3, dgvSaleOrder.CurrentCell.RowIndex).Value, _
                                dgvSaleOrder.Item(5, dgvSaleOrder.CurrentCell.RowIndex).Value, _
                                dgvSaleOrder.Item(6, dgvSaleOrder.CurrentCell.RowIndex).Value)
                    CalculationAll(dgvSaleOrder, _
                                     5, _
                                     6, _
                                     AMOUNT_DISCOUNTTextBox, _
                                     TAXPercentageTEXTBOX, _
                                     TAX_AMOUNTTextBox, _
                                     GRAND_TOTALTextBox, _
                                     GRAND_TOTALTextBox)
                    lblNoOfItem.Text = "No of Item(s) : " & dgvSaleOrder.RowCount - 1

                    ''Calculate Tax Amount when adding Product
                    Dim tmpGrandTotal As Integer = 0
                    For x As Integer = 0 To dgvSaleOrder.RowCount - 1
                        tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvSaleOrder.Item(6, x).Value)
                    Next
                    TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
                    TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)

                    If CInt(dgvSaleOrder.Item(4, dgvSaleOrder.CurrentCell.RowIndex).Value) > 100 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih besar dari 100%" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not greater than 100%" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvSaleOrder.Item(4, dgvSaleOrder.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                    If CInt(dgvSaleOrder.Item(4, dgvSaleOrder.CurrentCell.RowIndex).Value) < 0 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih kecil dari 0" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not smaller than 0" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvSaleOrder.Item(4, dgvSaleOrder.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                End If
            End If
        End If

    End Sub

    Private Sub TAX_AMOUNTTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TAX_AMOUNTTextBox.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""
    End Sub

    Private Sub TAX_AMOUNTTextBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TAX_AMOUNTTextBox.LostFocus
        TAX_AMOUNTTextBox.Text = IIf(TAX_AMOUNTTextBox.Text = "", "0", TAX_AMOUNTTextBox.Text)
        Dim tmpGrandTotal As Integer = 0
        For x As Integer = 0 To dgvSaleOrder.RowCount - 1
            tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvSaleOrder.Item(6, x).Value)
        Next
        If TAX_AMOUNTTextBox.Text = "0" Then
            TAXPercentageTEXTBOX.Text = "0.00"
        Else
            TAXPercentageTEXTBOX.Text = FormatNumber((TAX_AMOUNTTextBox.Text / tmpGrandTotal) * 100)
        End If
    End Sub

    Private Sub TAX_AMOUNTTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TAX_AMOUNTTextBox.TextChanged
        If GRAND_TOTALTextBox.Text = "0" Then
            TAX_AMOUNTTextBox.Text = "0"
        Else
            CalculationAll(dgvSaleOrder, _
                         5, _
                         6, _
                         AMOUNT_DISCOUNTTextBox, _
                         TAXPercentageTEXTBOX, _
                         TAX_AMOUNTTextBox, _
                         GRAND_TOTALTextBox, _
                         GRAND_TOTALTextBox)
            lblNoOfItem.Text = "No of Item(s) : " & dgvSaleOrder.RowCount - 1
        End If
        tmpChange = True
    End Sub

    Private Sub TAXPercentageTEXTBOX_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TAXPercentageTEXTBOX.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""
    End Sub

    Private Sub TAXPercentageTEXTBOX_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TAXPercentageTEXTBOX.LostFocus
        If GRAND_TOTALTextBox.Text <> "0" Then
            Dim tmpGrandTotal As Integer = 0
            For x As Integer = 0 To dgvSaleOrder.RowCount - 1
                tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvSaleOrder.Item(6, x).Value)
            Next
            TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
            TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)
        Else
            TAXPercentageTEXTBOX.Text = "0.00"
        End If
    End Sub

    Private Sub TAXPercentageTEXTBOX_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TAXPercentageTEXTBOX.TextChanged
        If GRAND_TOTALTextBox.Text = "0" Then
            If TAX_AMOUNTTextBox.Text = "0" Then
                TAXPercentageTEXTBOX.Text = "0.00"
            End If
        End If
        tmpChange = True
    End Sub

    Private Sub DOWN_PAYMENTTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""
    End Sub

    Private Sub cmdSearchName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchName.Click
        If tmpSaveMode = "Update" Then Exit Sub
        mdlGeneral.tmpSearchMode = "Customer - Customer Name"
        Dim frmSearchCustomer As New frmSearchCustomer
        frmSearchCustomer.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        CUSTOMERBindingSource.Position = CUSTOMERBindingSource.Find("CUSTOMER_ID", tmpSearchResult)
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        Me.Close()
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        SALE_ORDER_HEADERTableAdapter.SP_GENERATE_SALE_ORDER_NO(SALE_ORDER_NUMBERTextBox.Text)
        CUSTOMERBindingSource.Position = -1
        SALESMANBindingSource.Position = -1
        dgvSaleOrder.Rows.Clear()
        lblNoOfItem.Text = "No of Item(s) :  0"

        AMOUNT_DISCOUNTTextBox.Text = "0"
        TAXPercentageTEXTBOX.Text = "0.00"
        TAX_AMOUNTTextBox.Text = "0"
        GRAND_TOTALTextBox.Text = "0"
        CUSTOMER_NAMEComboBox.Enabled = True
        dgvSaleOrder.Focus()
        CUSTOMERBindingSource.Position = -1
        cmdAdd.Enabled = False
        cmdUndo.Enabled = False
        SHIP_TOTextBox.Text = ""
        tmpChange = False
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        tmpSaveMode = "Update"
        dgvSaleOrder.Columns("PRODUCT_CODE").ReadOnly = True
        dgvSaleOrder.Columns("PRODUCT_NAME").ReadOnly = True
        dgvSaleOrder.Columns("QUANTITY").ReadOnly = True
        dgvSaleOrder.Columns("PRICE_PER_UNIT").ReadOnly = True
        dgvSaleOrder.Columns("DISCOUNT").ReadOnly = True
        dgvSaleOrder.Columns("DISCOUNT2").ReadOnly = True
        dgvSaleOrder.Columns("TOTAL").ReadOnly = True
        dgvSaleOrder.Columns("DELETE").Visible = False
        DisableInputBox(Me)
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = False
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim tmpCheck As String = ""
        SALE_ORDER_HEADERTableAdapter.SP_CHECK_CLOSING_PERIOD(SALE_ORDER_DATEDateTimePicker.Value, tmpCheck)
        If tmpCheck = "TRUE" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menghapus transaksi ini !" & vbCrLf & "Transaksi ini telah masuk dalam proses Tutup Buku", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Delete this Transaction!" & vbCrLf & "This Transaction has been in Closed Period", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If Language = "Indonesian" Then
            If MsgBox("Batalkan transaksi ini?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = _
                MsgBoxResult.No Then Exit Sub
        Else
            If MsgBox("Void this transaction?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = _
               MsgBoxResult.No Then Exit Sub
        End If

        SALE_ORDER_HEADERTableAdapter.SP_SALE_ORDER_HEADER("V", _
                                                            0, _
                                                            SALE_ORDER_NUMBERTextBox.Text, _
                                                            SALE_ORDER_DATEDateTimePicker.Text, _
                                                            DELIVERY_REQUEST_DATEDateTimePicker.Value, _
                                                            CUSTOMER_NAMEComboBox.Text, _
                                                            SHIP_TOTextBox.Text, _
                                                            SALESMAN_IDComboBox.Text, _
                                                            GRAND_TOTALTextBox.Text, _
                                                            USER_ID, _
                                                            Now, _
                                                            USER_ID, _
                                                            Now)
        If Language = "Indonesian" Then
            MsgBox("Data Berhasil dihapus.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully deleted.", MsgBoxStyle.Information, "DMI Retail")
        End If
        Me.Close()
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Dim tmpCheck As String = ""
        tmpNumber = SALE_ORDER_NUMBERTextBox.Text

        SALE_ORDER_HEADERTableAdapter.SP_CHECK_CLOSING_PERIOD(SALE_ORDER_DATEDateTimePicker.Value, tmpCheck)
        If tmpCheck = "TRUE" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menghapus transaksi ini !" & vbCrLf & "Transaksi ini telah masuk dalam proses Tutup Buku", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Delete this Transaction!" & vbCrLf & "This Transaction has been in Closed Period", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If


        If Not ValidateComboBox(CUSTOMER_NAMEComboBox) Then
            If Language = "Indonesian" Then
                MsgBox("Nilai yang anda masukan tidak terdapat dalam daftar.", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You input value which is not in the list.", MsgBoxStyle.Critical, "DMI Retail")
            End If
            CUSTOMER_NAMEComboBox.Focus()
            Exit Sub
        End If

        If Not ValidateComboBox(SALESMAN_IDComboBox) Then
            If Language = "Indonesian" Then
                MsgBox("Nilai yang anda masukan tidak terdapat dalam daftar.", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You input value which is not in the list.", MsgBoxStyle.Critical, "DMI Retail")
            End If
            SALESMAN_IDComboBox.Focus()
            Exit Sub
        End If

        If SHIP_TOTextBox.Text = "" Or GRAND_TOTALTextBox.Text = "" Or SALESMAN_IDComboBox.Text = "" Or CUSTOMER_NAMEComboBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        For x As Integer = 0 To dgvSaleOrder.RowCount - 2
            If dgvSaleOrder.Item("STATUS", x).Value.ToString = "" Or dgvSaleOrder.Item("QUANTITY", x).Value.ToString = "" Or dgvSaleOrder.Item("PRICE_PER_UNIT", x).Value.ToString = "" Then
                If Language = "Indonesian" Then
                    MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If

            If CInt(dgvSaleOrder.Item("PRICE_PER_UNIT", x).Value) = 0 Or CInt(dgvSaleOrder.Item("QUANTITY", x).Value) = 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If
        Next

        If tmpSaveMode = "Insert" Then
            Dim tmpSaleOrderNo As Integer
            SALE_ORDER_HEADERTableAdapter.SP_CHECK_SALE_ORDER_NO(SALE_ORDER_NUMBERTextBox.Text, tmpSaleOrderNo)
            If tmpSaleOrderNo > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("No Transaksi telah digunakan oleh user lain." & vbCrLf & _
                                               "Nomor transaksi baru akan digunakan untuk transaksi ini.", _
                                               MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Receipt Number is used by another User." & vbCrLf & _
                                                        "New Receipt Number will be assigned to this transaction.", _
                                                        MsgBoxStyle.Critical, "DMI Retail")
                End If
            End If
            While tmpSaleOrderNo > 0
                SALE_ORDER_HEADERTableAdapter.SP_GENERATE_SALE_ORDER_NO(SALE_ORDER_NUMBERTextBox.Text)
                SALE_ORDER_HEADERTableAdapter.SP_CHECK_SALE_ORDER_NO(SALE_ORDER_NUMBERTextBox.Text, tmpSaleOrderNo)
            End While

            SALE_ORDER_HEADERTableAdapter.SP_SALE_ORDER_HEADER("I", _
                                                                 0, _
                                                                 SALE_ORDER_NUMBERTextBox.Text, _
                                                                 SALE_ORDER_DATEDateTimePicker.Text, _
                                                                 DELIVERY_REQUEST_DATEDateTimePicker.Value, _
                                                                 CUSTOMERBindingSource.Current("CUSTOMER_ID"), _
                                                                 SHIP_TOTextBox.Text, _
                                                                 SALESMANBindingSource.Current("SALESMAN_ID"), _
                                                                 GRAND_TOTALTextBox.Text, _
                                                                 USER_ID, _
                                                                 Now, _
                                                                 0, _
                                                                 DateSerial(4000, 12, 31))

            For x As Integer = 0 To dgvSaleOrder.RowCount - 2
                Dim tmpProductID As Integer
                Dim tmpStatusSale, tmpStatusDelivery As String
                If dgvSaleOrder.Item("STATUS", x).Value = "Open" Then
                    tmpStatusSale = "Open"
                    tmpStatusDelivery = "Open"
                Else
                    tmpStatusSale = "Close"
                    tmpStatusDelivery = "Close"
                End If
                PRODUCTTableAdapter.SP_GET_PRODUCT_ID(dgvSaleOrder.Item("PRODUCT_CODE", x).Value, tmpProductID)
                SALE_ORDER_DETAILTableAdapter.SP_SALE_ORDER_DETAIL("I", _
                                                                     0, _
                                                                     0, _
                                                                     tmpProductID, _
                                                                     CInt(dgvSaleOrder.Item("QUANTITY", x).Value), _
                                                                     CInt(dgvSaleOrder.Item("PRICE_PER_UNIT", x).Value), _
                                                                     CInt(dgvSaleOrder.Item("DISCOUNT", x).Value), _
                                                                     CInt(dgvSaleOrder.Item("DISCOUNT2", x).Value), _
                                                                     CInt(TAXPercentageTEXTBOX.Text), _
                                                                     CInt(TAX_AMOUNTTextBox.Text), _
                                                                     CInt(dgvSaleOrder.Item("TOTAL", x).Value), _
                                                                     tmpStatusDelivery, _
                                                                     dgvSaleOrder.Item("STATUS", x).Value, _
                                                                     tmpStatusSale, _
                                                                     USER_ID, _
                                                                     Now, _
                                                                     0, _
                                                                     DateSerial(4000, 12, 31))

            Next
            If Language = "Indonesian" Then
                MsgBox("Data Berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
            Else
                MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
            End If
            cmdAdd_Click(Nothing, Nothing)
            tmpChange = True
            tmpPrint = True
        ElseIf tmpSaveMode = "Update" Then
            For x As Integer = 0 To dgvSaleOrder.RowCount - 2
                Dim tmpProductID As Integer
                Dim tmpStatusSale, tmpStatusDelivery As String
                If dgvSaleOrder.Item("STATUS", x).Value = "Open" Then
                    tmpStatusSale = "Open"
                    tmpStatusDelivery = "Open"
                Else
                    tmpStatusSale = "Close"
                    tmpStatusDelivery = "Close"
                End If
                PRODUCTTableAdapter.SP_GET_PRODUCT_ID(dgvSaleOrder.Item("PRODUCT_CODE", x).Value, tmpProductID)
                SALE_ORDER_DETAILTableAdapter.SP_SALE_ORDER_DETAIL("U", _
                                                                     tmpSaleOrderId, _
                                                                     0, _
                                                                     tmpProductID, _
                                                                     CInt(dgvSaleOrder.Item("QUANTITY", x).Value), _
                                                                     CInt(dgvSaleOrder.Item("PRICE_PER_UNIT", x).Value), _
                                                                     CInt(dgvSaleOrder.Item("DISCOUNT", x).Value), _
                                                                     CInt(dgvSaleOrder.Item("DISCOUNT2", x).Value), _
                                                                     CInt(TAXPercentageTEXTBOX.Text), _
                                                                     CInt(TAX_AMOUNTTextBox.Text), _
                                                                     CInt(dgvSaleOrder.Item("TOTAL", x).Value), _
                                                                     tmpStatusDelivery, _
                                                                     dgvSaleOrder.Item("STATUS", x).Value, _
                                                                     tmpStatusSale, _
                                                                     USER_ID, _
                                                                     Now, _
                                                                     0, _
                                                                     DateSerial(4000, 12, 31))

            Next
            If Language = "Indonesian" Then
                MsgBox("Data Berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
            Else
                MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
            End If
            Me.Close()
        End If
    End Sub


    'Private Sub cmdPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrint.Click
    '    If tmpSaveMode = "Insert" Then
    '        cmdSave_Click(Nothing, Nothing)
    '        If tmpPrint = False Then
    '            If Language = "Indonesian" Then
    '                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
    '            Else
    '                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
    '            End If
    '            Exit Sub
    '        End If

    '        Dim objf As New frmRptSaleOrder
    '        objf.tmpnumber = tmpNumber
    '        objf.ReportViewer1.ShowRefreshButton = False
    '        objf.ReportViewer1.ZoomPercent = 100
    '        objf.WindowState = FormWindowState.Maximized
    '        objf.Show()
    '    ElseIf tmpSaveMode = "Update" Then
    '        Dim objf As New frmRptSaleOrder
    '        objf.tmpnumber = tmpNumber
    '        objf.ReportViewer1.ShowRefreshButton = False
    '        objf.ReportViewer1.ZoomPercent = 100
    '        objf.WindowState = FormWindowState.Maximized
    '        objf.Show()
    '    End If
    'End Sub

End Class

