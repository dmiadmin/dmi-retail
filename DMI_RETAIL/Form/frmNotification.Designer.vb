﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNotification
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNotification))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel2 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel3 = New System.Windows.Forms.LinkLabel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.LinkLabel5 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel4 = New System.Windows.Forms.LinkLabel()
        Me.CLOSED_PERIODBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CLOSED_PERIOD_YEARLYBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SALEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_NOTIF_INVOICE_SALEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PURCHASEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_NOTIF_IVOICE_PURCHASEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_NOTIFICATION_SAFE_STOCKBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox1.SuspendLayout()
        CType(Me.CLOSED_PERIODBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CLOSED_PERIOD_YEARLYBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SALEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_NOTIF_INVOICE_SALEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PURCHASEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_NOTIF_IVOICE_PURCHASEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_NOTIFICATION_SAFE_STOCKBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Lucida Bright", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(39, 34)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Label1"
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(17, 23)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(68, 15)
        Me.LinkLabel1.TabIndex = 1
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "LinkLabel1"
        Me.LinkLabel1.Visible = False
        '
        'LinkLabel2
        '
        Me.LinkLabel2.AutoSize = True
        Me.LinkLabel2.Location = New System.Drawing.Point(17, 59)
        Me.LinkLabel2.Name = "LinkLabel2"
        Me.LinkLabel2.Size = New System.Drawing.Size(68, 15)
        Me.LinkLabel2.TabIndex = 2
        Me.LinkLabel2.TabStop = True
        Me.LinkLabel2.Text = "LinkLabel2"
        Me.LinkLabel2.Visible = False
        '
        'LinkLabel3
        '
        Me.LinkLabel3.AutoSize = True
        Me.LinkLabel3.Location = New System.Drawing.Point(17, 95)
        Me.LinkLabel3.Name = "LinkLabel3"
        Me.LinkLabel3.Size = New System.Drawing.Size(68, 15)
        Me.LinkLabel3.TabIndex = 3
        Me.LinkLabel3.TabStop = True
        Me.LinkLabel3.Text = "LinkLabel3"
        Me.LinkLabel3.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.LinkLabel5)
        Me.GroupBox1.Controls.Add(Me.LinkLabel4)
        Me.GroupBox1.Controls.Add(Me.LinkLabel3)
        Me.GroupBox1.Controls.Add(Me.LinkLabel2)
        Me.GroupBox1.Controls.Add(Me.LinkLabel1)
        Me.GroupBox1.Location = New System.Drawing.Point(23, 73)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(346, 209)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        '
        'LinkLabel5
        '
        Me.LinkLabel5.AutoSize = True
        Me.LinkLabel5.Location = New System.Drawing.Point(17, 170)
        Me.LinkLabel5.Name = "LinkLabel5"
        Me.LinkLabel5.Size = New System.Drawing.Size(68, 15)
        Me.LinkLabel5.TabIndex = 5
        Me.LinkLabel5.TabStop = True
        Me.LinkLabel5.Text = "LinkLabel5"
        Me.LinkLabel5.Visible = False
        '
        'LinkLabel4
        '
        Me.LinkLabel4.AutoSize = True
        Me.LinkLabel4.Location = New System.Drawing.Point(17, 133)
        Me.LinkLabel4.Name = "LinkLabel4"
        Me.LinkLabel4.Size = New System.Drawing.Size(68, 15)
        Me.LinkLabel4.TabIndex = 4
        Me.LinkLabel4.TabStop = True
        Me.LinkLabel4.Text = "LinkLabel4"
        Me.LinkLabel4.Visible = False
        '
        'frmNotification
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(380, 299)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmNotification"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Notification"
        Me.Text = "Notification"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.CLOSED_PERIODBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CLOSED_PERIOD_YEARLYBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SALEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_NOTIF_INVOICE_SALEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PURCHASEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_NOTIF_IVOICE_PURCHASEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_NOTIFICATION_SAFE_STOCKBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents LinkLabel2 As System.Windows.Forms.LinkLabel
    Friend WithEvents LinkLabel3 As System.Windows.Forms.LinkLabel
    Friend WithEvents CLOSED_PERIODBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CLOSED_PERIOD_YEARLYBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LinkLabel4 As System.Windows.Forms.LinkLabel
    Friend WithEvents LinkLabel5 As System.Windows.Forms.LinkLabel
    Friend WithEvents SALEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PURCHASEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_NOTIF_INVOICE_SALEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_NOTIF_IVOICE_PURCHASEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_NOTIFICATION_SAFE_STOCKBindingSource As System.Windows.Forms.BindingSource
End Class
