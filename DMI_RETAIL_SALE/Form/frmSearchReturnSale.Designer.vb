﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSearchReturnSale
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSearchReturnSale))
        Me.DS_RETURN_SALE = New DMI_RETAIL_SALE.DS_RETURN_SALE()
        Me.SP_SEARCH_SALE_RETURNBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_SEARCH_SALE_RETURNTableAdapter = New DMI_RETAIL_SALE.DS_RETURN_SALETableAdapters.SP_SEARCH_SALE_RETURNTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_SALE.DS_RETURN_SALETableAdapters.TableAdapterManager()
        Me.dgvSearchReturnSale = New System.Windows.Forms.DataGridView()
        Me.CUSTOMER_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CUSTOMER_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALE_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RECEIPT_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALE_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PAYMENT_METHOD_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GRAND_TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblCustomer = New System.Windows.Forms.Label()
        Me.lblReceipt = New System.Windows.Forms.Label()
        Me.txtCustomer = New System.Windows.Forms.TextBox()
        Me.txtReceiptNo = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        CType(Me.DS_RETURN_SALE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_SEARCH_SALE_RETURNBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSearchReturnSale, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'DS_RETURN_SALE
        '
        Me.DS_RETURN_SALE.DataSetName = "DS_RETURN_SALE"
        Me.DS_RETURN_SALE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SP_SEARCH_SALE_RETURNBindingSource
        '
        Me.SP_SEARCH_SALE_RETURNBindingSource.DataMember = "SP_SEARCH_SALE_RETURN"
        Me.SP_SEARCH_SALE_RETURNBindingSource.DataSource = Me.DS_RETURN_SALE
        '
        'SP_SEARCH_SALE_RETURNTableAdapter
        '
        Me.SP_SEARCH_SALE_RETURNTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.SALE_RETURN_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.SALE_RETURNTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_SALE.DS_RETURN_SALETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'dgvSearchReturnSale
        '
        Me.dgvSearchReturnSale.AllowUserToAddRows = False
        Me.dgvSearchReturnSale.AllowUserToDeleteRows = False
        Me.dgvSearchReturnSale.AutoGenerateColumns = False
        Me.dgvSearchReturnSale.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSearchReturnSale.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSearchReturnSale.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSearchReturnSale.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CUSTOMER_ID, Me.CUSTOMER_NAME, Me.SALE_ID, Me.RECEIPT_NO, Me.SALE_DATE, Me.PAYMENT_METHOD_NAME, Me.GRAND_TOTAL, Me.TOTAL})
        Me.dgvSearchReturnSale.DataSource = Me.SP_SEARCH_SALE_RETURNBindingSource
        Me.dgvSearchReturnSale.Location = New System.Drawing.Point(12, 18)
        Me.dgvSearchReturnSale.Name = "dgvSearchReturnSale"
        Me.dgvSearchReturnSale.ReadOnly = True
        Me.dgvSearchReturnSale.RowHeadersWidth = 28
        Me.dgvSearchReturnSale.Size = New System.Drawing.Size(644, 220)
        Me.dgvSearchReturnSale.TabIndex = 1
        '
        'CUSTOMER_ID
        '
        Me.CUSTOMER_ID.DataPropertyName = "CUSTOMER_ID"
        Me.CUSTOMER_ID.HeaderText = "CUSTOMER_ID"
        Me.CUSTOMER_ID.Name = "CUSTOMER_ID"
        Me.CUSTOMER_ID.ReadOnly = True
        Me.CUSTOMER_ID.Visible = False
        '
        'CUSTOMER_NAME
        '
        Me.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME"
        Me.CUSTOMER_NAME.HeaderText = "Customer Name"
        Me.CUSTOMER_NAME.Name = "CUSTOMER_NAME"
        Me.CUSTOMER_NAME.ReadOnly = True
        '
        'SALE_ID
        '
        Me.SALE_ID.DataPropertyName = "SALE_ID"
        Me.SALE_ID.HeaderText = "SALE_ID"
        Me.SALE_ID.Name = "SALE_ID"
        Me.SALE_ID.ReadOnly = True
        Me.SALE_ID.Visible = False
        '
        'RECEIPT_NO
        '
        Me.RECEIPT_NO.DataPropertyName = "RECEIPT_NO"
        Me.RECEIPT_NO.HeaderText = "Receipt No"
        Me.RECEIPT_NO.Name = "RECEIPT_NO"
        Me.RECEIPT_NO.ReadOnly = True
        '
        'SALE_DATE
        '
        Me.SALE_DATE.DataPropertyName = "SALE_DATE"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "dd-MMM-yyyy"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.SALE_DATE.DefaultCellStyle = DataGridViewCellStyle2
        Me.SALE_DATE.HeaderText = "Sale Date"
        Me.SALE_DATE.Name = "SALE_DATE"
        Me.SALE_DATE.ReadOnly = True
        '
        'PAYMENT_METHOD_NAME
        '
        Me.PAYMENT_METHOD_NAME.DataPropertyName = "PAYMENT_METHOD_NAME"
        Me.PAYMENT_METHOD_NAME.HeaderText = "Payment Method"
        Me.PAYMENT_METHOD_NAME.Name = "PAYMENT_METHOD_NAME"
        Me.PAYMENT_METHOD_NAME.ReadOnly = True
        '
        'GRAND_TOTAL
        '
        Me.GRAND_TOTAL.DataPropertyName = "GRAND_TOTAL"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N0"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.GRAND_TOTAL.DefaultCellStyle = DataGridViewCellStyle3
        Me.GRAND_TOTAL.HeaderText = "Grand Total"
        Me.GRAND_TOTAL.Name = "GRAND_TOTAL"
        Me.GRAND_TOTAL.ReadOnly = True
        '
        'TOTAL
        '
        Me.TOTAL.DataPropertyName = "TOTAL"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.TOTAL.DefaultCellStyle = DataGridViewCellStyle4
        Me.TOTAL.HeaderText = "Total"
        Me.TOTAL.Name = "TOTAL"
        Me.TOTAL.ReadOnly = True
        '
        'lblCustomer
        '
        Me.lblCustomer.Location = New System.Drawing.Point(13, 27)
        Me.lblCustomer.Name = "lblCustomer"
        Me.lblCustomer.Size = New System.Drawing.Size(153, 19)
        Me.lblCustomer.TabIndex = 2
        Me.lblCustomer.Text = "Customer Name"
        Me.lblCustomer.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblReceipt
        '
        Me.lblReceipt.Location = New System.Drawing.Point(16, 57)
        Me.lblReceipt.Name = "lblReceipt"
        Me.lblReceipt.Size = New System.Drawing.Size(150, 19)
        Me.lblReceipt.TabIndex = 3
        Me.lblReceipt.Text = "Receipt No"
        Me.lblReceipt.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtCustomer
        '
        Me.txtCustomer.Location = New System.Drawing.Point(179, 24)
        Me.txtCustomer.Name = "txtCustomer"
        Me.txtCustomer.Size = New System.Drawing.Size(173, 22)
        Me.txtCustomer.TabIndex = 4
        '
        'txtReceiptNo
        '
        Me.txtReceiptNo.Location = New System.Drawing.Point(179, 54)
        Me.txtReceiptNo.Name = "txtReceiptNo"
        Me.txtReceiptNo.Size = New System.Drawing.Size(173, 22)
        Me.txtReceiptNo.TabIndex = 5
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtReceiptNo)
        Me.GroupBox1.Controls.Add(Me.txtCustomer)
        Me.GroupBox1.Controls.Add(Me.lblReceipt)
        Me.GroupBox1.Controls.Add(Me.lblCustomer)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 29)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(669, 96)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dgvSearchReturnSale)
        Me.GroupBox2.Location = New System.Drawing.Point(16, 135)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(667, 253)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.TabStop = False
        '
        'frmSearchReturnSale
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(703, 414)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmSearchReturnSale"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Search Return Sale"
        CType(Me.DS_RETURN_SALE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_SEARCH_SALE_RETURNBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSearchReturnSale, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DS_RETURN_SALE As DMI_RETAIL_SALE.DS_RETURN_SALE
    Friend WithEvents SP_SEARCH_SALE_RETURNBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_SEARCH_SALE_RETURNTableAdapter As DMI_RETAIL_SALE.DS_RETURN_SALETableAdapters.SP_SEARCH_SALE_RETURNTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_SALE.DS_RETURN_SALETableAdapters.TableAdapterManager
    Friend WithEvents dgvSearchReturnSale As System.Windows.Forms.DataGridView
    Friend WithEvents lblCustomer As System.Windows.Forms.Label
    Friend WithEvents lblReceipt As System.Windows.Forms.Label
    Friend WithEvents txtCustomer As System.Windows.Forms.TextBox
    Friend WithEvents txtReceiptNo As System.Windows.Forms.TextBox
    Friend WithEvents CUSTOMER_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CUSTOMER_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALE_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RECEIPT_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALE_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PAYMENT_METHOD_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GRAND_TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
End Class
