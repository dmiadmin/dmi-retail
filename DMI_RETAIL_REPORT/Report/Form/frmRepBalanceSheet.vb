﻿Public Class frmRepBalanceSheet
    Public year As Integer
    Public month As Integer
    Public productname As String
    Private Sub frmRepBalanceSheet_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DB_DMI_RETAILDataSetALL.COMPANY_INFORMATION' table. You can move, or remove it, as needed.
        Me.COMPANY_INFORMATIONTableAdapter.Fill(Me.DB_DMI_RETAILDataSetALL.COMPANY_INFORMATION)

        Dim tmpnow, tmpDate As Date

        tmpnow = DateSerial(year, month, 1)

        tmpDate = DateSerial(tmpnow.Year, _
                             tmpnow.Month + 1, _
                             tmpnow.Day - 1)

        'TODO: This line of code loads data into the 'DB_DMI_RETAILDataSetALL.SP_LIST_ENDING_STOCK' table. You can move, or remove it, as needed.
        Me.SP_LIST_ENDING_STOCKTableAdapter.Fill(Me.DB_DMI_RETAILDataSetALL.SP_LIST_ENDING_STOCK, tmpDate, productname)
      
        Me.ReportViewer1.RefreshReport()

        Dim newPageSettings As New System.Drawing.Printing.PageSettings
        newPageSettings.Margins = New System.Drawing.Printing.Margins(50, 50, 35, 35)
        newPageSettings.PaperSize = New System.Drawing.Printing.PaperSize("A4", 827, 1169)
        Me.ReportViewer1.SetPageSettings(newPageSettings)

        If Not tmpReport Is Nothing Then tmpReport.Close()
        tmpReport = Me
        Me.ReportViewer1.RefreshReport()
    End Sub
End Class