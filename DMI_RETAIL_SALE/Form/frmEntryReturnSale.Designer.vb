﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEntryReturnSale
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEntryReturnSale))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.txtCustomer = New System.Windows.Forms.TextBox()
        Me.txtSaleDate = New System.Windows.Forms.TextBox()
        Me.txtReceipNo = New System.Windows.Forms.TextBox()
        Me.txtPayment = New System.Windows.Forms.TextBox()
        Me.lblCustomer = New System.Windows.Forms.Label()
        Me.lblReceiptNo = New System.Windows.Forms.Label()
        Me.lblSaleDate = New System.Windows.Forms.Label()
        Me.lblPayment = New System.Windows.Forms.Label()
        Me.cmdSearch = New System.Windows.Forms.PictureBox()
        Me.dgvSaleReturn = New System.Windows.Forms.DataGridView()
        Me.PRODUCT_CODE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRODUCT_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QUANTITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RETURNED_QUANTITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Return_Quantity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RETURNED_DESCRIPTION = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SP_SALE_RETURN_DETAIL_LISTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_RETURN_SALE = New DMI_RETAIL_SALE.DS_RETURN_SALE()
        Me.txtReturnNo = New System.Windows.Forms.TextBox()
        Me.dtpReturnDate = New System.Windows.Forms.TextBox()
        Me.lblReturnDate = New System.Windows.Forms.Label()
        Me.ckbReusable = New System.Windows.Forms.CheckBox()
        Me.SP_SALE_RETURN_DETAIL_LISTTableAdapter = New DMI_RETAIL_SALE.DS_RETURN_SALETableAdapters.SP_SALE_RETURN_DETAIL_LISTTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_SALE.DS_RETURN_SALETableAdapters.TableAdapterManager()
        Me.SALE_RETURNBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SALE_RETURNTableAdapter = New DMI_RETAIL_SALE.DS_RETURN_SALETableAdapters.SALE_RETURNTableAdapter()
        Me.WAREHOUSEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.WAREHOUSETableAdapter = New DMI_RETAIL_SALE.DS_RETURN_SALETableAdapters.WAREHOUSETableAdapter()
        Me.WAREHOUSE_NAMEComboBox = New System.Windows.Forms.ComboBox()
        Me.cmbReturnType = New System.Windows.Forms.ComboBox()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblReturnType = New System.Windows.Forms.Label()
        Me.lblWarehouse = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.lblReturnNo = New System.Windows.Forms.Label()
        Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter = New DMI_RETAIL_SALE.DS_RETURN_SALETableAdapters.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter()
        Me.SP_SHOW_LIST_RETURN_SALEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_SHOW_LIST_RETURN_SALETableAdapter = New DMI_RETAIL_SALE.DS_RETURN_SALETableAdapters.SP_SHOW_LIST_RETURN_SALETableAdapter()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        CType(Me.cmdSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSaleReturn, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_SALE_RETURN_DETAIL_LISTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_RETURN_SALE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SALE_RETURNBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_SHOW_LIST_RETURN_SALEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtCustomer
        '
        Me.txtCustomer.Location = New System.Drawing.Point(415, 14)
        Me.txtCustomer.Name = "txtCustomer"
        Me.txtCustomer.ReadOnly = True
        Me.txtCustomer.Size = New System.Drawing.Size(173, 22)
        Me.txtCustomer.TabIndex = 3
        '
        'txtSaleDate
        '
        Me.txtSaleDate.Location = New System.Drawing.Point(415, 70)
        Me.txtSaleDate.Name = "txtSaleDate"
        Me.txtSaleDate.ReadOnly = True
        Me.txtSaleDate.Size = New System.Drawing.Size(173, 22)
        Me.txtSaleDate.TabIndex = 5
        '
        'txtReceipNo
        '
        Me.txtReceipNo.Location = New System.Drawing.Point(415, 42)
        Me.txtReceipNo.Name = "txtReceipNo"
        Me.txtReceipNo.ReadOnly = True
        Me.txtReceipNo.Size = New System.Drawing.Size(173, 22)
        Me.txtReceipNo.TabIndex = 4
        '
        'txtPayment
        '
        Me.txtPayment.Location = New System.Drawing.Point(415, 98)
        Me.txtPayment.Name = "txtPayment"
        Me.txtPayment.ReadOnly = True
        Me.txtPayment.Size = New System.Drawing.Size(173, 22)
        Me.txtPayment.TabIndex = 6
        '
        'lblCustomer
        '
        Me.lblCustomer.AutoSize = True
        Me.lblCustomer.Location = New System.Drawing.Point(302, 18)
        Me.lblCustomer.Name = "lblCustomer"
        Me.lblCustomer.Size = New System.Drawing.Size(66, 15)
        Me.lblCustomer.TabIndex = 8
        Me.lblCustomer.Text = "Customer"
        '
        'lblReceiptNo
        '
        Me.lblReceiptNo.AutoSize = True
        Me.lblReceiptNo.Location = New System.Drawing.Point(302, 45)
        Me.lblReceiptNo.Name = "lblReceiptNo"
        Me.lblReceiptNo.Size = New System.Drawing.Size(72, 15)
        Me.lblReceiptNo.TabIndex = 9
        Me.lblReceiptNo.Text = "Receipt No"
        '
        'lblSaleDate
        '
        Me.lblSaleDate.AutoSize = True
        Me.lblSaleDate.Location = New System.Drawing.Point(302, 73)
        Me.lblSaleDate.Name = "lblSaleDate"
        Me.lblSaleDate.Size = New System.Drawing.Size(63, 15)
        Me.lblSaleDate.TabIndex = 10
        Me.lblSaleDate.Text = "Sale Date"
        '
        'lblPayment
        '
        Me.lblPayment.AutoSize = True
        Me.lblPayment.Location = New System.Drawing.Point(302, 101)
        Me.lblPayment.Name = "lblPayment"
        Me.lblPayment.Size = New System.Drawing.Size(105, 15)
        Me.lblPayment.TabIndex = 11
        Me.lblPayment.Text = "Payment Method"
        '
        'cmdSearch
        '
        Me.cmdSearch.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearch.Image = CType(resources.GetObject("cmdSearch.Image"), System.Drawing.Image)
        Me.cmdSearch.Location = New System.Drawing.Point(604, 14)
        Me.cmdSearch.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearch.Name = "cmdSearch"
        Me.cmdSearch.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearch.TabIndex = 20
        Me.cmdSearch.TabStop = False
        '
        'dgvSaleReturn
        '
        Me.dgvSaleReturn.AllowUserToAddRows = False
        Me.dgvSaleReturn.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.dgvSaleReturn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSaleReturn.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PRODUCT_CODE, Me.PRODUCT_NAME, Me.QUANTITY, Me.RETURNED_QUANTITY, Me.Return_Quantity, Me.RETURNED_DESCRIPTION})
        Me.dgvSaleReturn.Location = New System.Drawing.Point(7, 16)
        Me.dgvSaleReturn.Name = "dgvSaleReturn"
        Me.dgvSaleReturn.Size = New System.Drawing.Size(657, 162)
        Me.dgvSaleReturn.TabIndex = 26
        '
        'PRODUCT_CODE
        '
        Me.PRODUCT_CODE.HeaderText = "Product Code"
        Me.PRODUCT_CODE.Name = "PRODUCT_CODE"
        Me.PRODUCT_CODE.ReadOnly = True
        '
        'PRODUCT_NAME
        '
        Me.PRODUCT_NAME.HeaderText = "Product Name"
        Me.PRODUCT_NAME.Name = "PRODUCT_NAME"
        Me.PRODUCT_NAME.ReadOnly = True
        Me.PRODUCT_NAME.Width = 150
        '
        'QUANTITY
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "N0"
        DataGridViewCellStyle1.NullValue = "0"
        Me.QUANTITY.DefaultCellStyle = DataGridViewCellStyle1
        Me.QUANTITY.HeaderText = "Qty"
        Me.QUANTITY.Name = "QUANTITY"
        Me.QUANTITY.ReadOnly = True
        Me.QUANTITY.Width = 50
        '
        'RETURNED_QUANTITY
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = "0"
        Me.RETURNED_QUANTITY.DefaultCellStyle = DataGridViewCellStyle2
        Me.RETURNED_QUANTITY.HeaderText = "Returned Quantity"
        Me.RETURNED_QUANTITY.Name = "RETURNED_QUANTITY"
        Me.RETURNED_QUANTITY.ReadOnly = True
        '
        'Return_Quantity
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N0"
        DataGridViewCellStyle3.NullValue = "0"
        Me.Return_Quantity.DefaultCellStyle = DataGridViewCellStyle3
        Me.Return_Quantity.HeaderText = "Return Quantity"
        Me.Return_Quantity.Name = "Return_Quantity"
        '
        'RETURNED_DESCRIPTION
        '
        Me.RETURNED_DESCRIPTION.HeaderText = "Description"
        Me.RETURNED_DESCRIPTION.Name = "RETURNED_DESCRIPTION"
        '
        'SP_SALE_RETURN_DETAIL_LISTBindingSource
        '
        Me.SP_SALE_RETURN_DETAIL_LISTBindingSource.DataMember = "SP_SALE_RETURN_DETAIL_LIST"
        Me.SP_SALE_RETURN_DETAIL_LISTBindingSource.DataSource = Me.DS_RETURN_SALE
        '
        'DS_RETURN_SALE
        '
        Me.DS_RETURN_SALE.DataSetName = "DS_RETURN_SALE"
        Me.DS_RETURN_SALE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'txtReturnNo
        '
        Me.txtReturnNo.Location = New System.Drawing.Point(116, 15)
        Me.txtReturnNo.Name = "txtReturnNo"
        Me.txtReturnNo.ReadOnly = True
        Me.txtReturnNo.Size = New System.Drawing.Size(100, 22)
        Me.txtReturnNo.TabIndex = 1
        '
        'dtpReturnDate
        '
        Me.dtpReturnDate.Location = New System.Drawing.Point(116, 47)
        Me.dtpReturnDate.Name = "dtpReturnDate"
        Me.dtpReturnDate.ReadOnly = True
        Me.dtpReturnDate.Size = New System.Drawing.Size(100, 22)
        Me.dtpReturnDate.TabIndex = 2
        '
        'lblReturnDate
        '
        Me.lblReturnDate.AutoSize = True
        Me.lblReturnDate.Location = New System.Drawing.Point(15, 50)
        Me.lblReturnDate.Name = "lblReturnDate"
        Me.lblReturnDate.Size = New System.Drawing.Size(78, 15)
        Me.lblReturnDate.TabIndex = 29
        Me.lblReturnDate.Text = "Return Date"
        '
        'ckbReusable
        '
        Me.ckbReusable.Location = New System.Drawing.Point(250, 198)
        Me.ckbReusable.Name = "ckbReusable"
        Me.ckbReusable.Size = New System.Drawing.Size(118, 19)
        Me.ckbReusable.TabIndex = 8
        Me.ckbReusable.Text = "Reusable"
        Me.ckbReusable.UseVisualStyleBackColor = True
        '
        'SP_SALE_RETURN_DETAIL_LISTTableAdapter
        '
        Me.SP_SALE_RETURN_DETAIL_LISTTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.SALE_RETURN_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.SALE_RETURNTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_SALE.DS_RETURN_SALETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'SALE_RETURNBindingSource
        '
        Me.SALE_RETURNBindingSource.DataMember = "SALE_RETURN"
        Me.SALE_RETURNBindingSource.DataSource = Me.DS_RETURN_SALE
        '
        'SALE_RETURNTableAdapter
        '
        Me.SALE_RETURNTableAdapter.ClearBeforeFill = True
        '
        'WAREHOUSEBindingSource
        '
        Me.WAREHOUSEBindingSource.DataMember = "WAREHOUSE"
        Me.WAREHOUSEBindingSource.DataSource = Me.DS_RETURN_SALE
        '
        'WAREHOUSETableAdapter
        '
        Me.WAREHOUSETableAdapter.ClearBeforeFill = True
        '
        'WAREHOUSE_NAMEComboBox
        '
        Me.WAREHOUSE_NAMEComboBox.DataSource = Me.WAREHOUSEBindingSource
        Me.WAREHOUSE_NAMEComboBox.DisplayMember = "WAREHOUSE_NAME"
        Me.WAREHOUSE_NAMEComboBox.FormattingEnabled = True
        Me.WAREHOUSE_NAMEComboBox.Location = New System.Drawing.Point(110, 224)
        Me.WAREHOUSE_NAMEComboBox.Name = "WAREHOUSE_NAMEComboBox"
        Me.WAREHOUSE_NAMEComboBox.Size = New System.Drawing.Size(121, 23)
        Me.WAREHOUSE_NAMEComboBox.TabIndex = 9
        Me.WAREHOUSE_NAMEComboBox.ValueMember = "WAREHOUSE_ID"
        '
        'cmbReturnType
        '
        Me.cmbReturnType.FormattingEnabled = True
        Me.cmbReturnType.Items.AddRange(New Object() {"Goods", "Non Goods"})
        Me.cmbReturnType.Location = New System.Drawing.Point(110, 195)
        Me.cmbReturnType.Name = "cmbReturnType"
        Me.cmbReturnType.Size = New System.Drawing.Size(121, 23)
        Me.cmbReturnType.TabIndex = 7
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(451, 195)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(213, 47)
        Me.txtDescription.TabIndex = 10
        '
        'lblDescription
        '
        Me.lblDescription.AutoSize = True
        Me.lblDescription.Location = New System.Drawing.Point(369, 198)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(76, 15)
        Me.lblDescription.TabIndex = 35
        Me.lblDescription.Text = "Description"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblReturnType)
        Me.GroupBox1.Controls.Add(Me.lblWarehouse)
        Me.GroupBox1.Controls.Add(Me.lblDescription)
        Me.GroupBox1.Controls.Add(Me.txtDescription)
        Me.GroupBox1.Controls.Add(Me.cmbReturnType)
        Me.GroupBox1.Controls.Add(Me.WAREHOUSE_NAMEComboBox)
        Me.GroupBox1.Controls.Add(Me.ckbReusable)
        Me.GroupBox1.Controls.Add(Me.dgvSaleReturn)
        Me.GroupBox1.Location = New System.Drawing.Point(17, 140)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(674, 260)
        Me.GroupBox1.TabIndex = 36
        Me.GroupBox1.TabStop = False
        '
        'lblReturnType
        '
        Me.lblReturnType.AutoSize = True
        Me.lblReturnType.Location = New System.Drawing.Point(22, 198)
        Me.lblReturnType.Name = "lblReturnType"
        Me.lblReturnType.Size = New System.Drawing.Size(79, 15)
        Me.lblReturnType.TabIndex = 37
        Me.lblReturnType.Text = "Return Type"
        '
        'lblWarehouse
        '
        Me.lblWarehouse.AutoSize = True
        Me.lblWarehouse.Location = New System.Drawing.Point(22, 228)
        Me.lblWarehouse.Name = "lblWarehouse"
        Me.lblWarehouse.Size = New System.Drawing.Size(73, 15)
        Me.lblWarehouse.TabIndex = 36
        Me.lblWarehouse.Text = "Warehouse"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.GroupBox3)
        Me.GroupBox2.Controls.Add(Me.cmdSearch)
        Me.GroupBox2.Controls.Add(Me.lblPayment)
        Me.GroupBox2.Controls.Add(Me.lblSaleDate)
        Me.GroupBox2.Controls.Add(Me.lblReceiptNo)
        Me.GroupBox2.Controls.Add(Me.lblCustomer)
        Me.GroupBox2.Controls.Add(Me.txtPayment)
        Me.GroupBox2.Controls.Add(Me.txtReceipNo)
        Me.GroupBox2.Controls.Add(Me.txtSaleDate)
        Me.GroupBox2.Controls.Add(Me.txtCustomer)
        Me.GroupBox2.Location = New System.Drawing.Point(17, 4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(674, 130)
        Me.GroupBox2.TabIndex = 37
        Me.GroupBox2.TabStop = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lblReturnNo)
        Me.GroupBox3.Controls.Add(Me.lblReturnDate)
        Me.GroupBox3.Controls.Add(Me.dtpReturnDate)
        Me.GroupBox3.Controls.Add(Me.txtReturnNo)
        Me.GroupBox3.Location = New System.Drawing.Point(26, 22)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(255, 83)
        Me.GroupBox3.TabIndex = 30
        Me.GroupBox3.TabStop = False
        '
        'lblReturnNo
        '
        Me.lblReturnNo.AutoSize = True
        Me.lblReturnNo.Location = New System.Drawing.Point(15, 18)
        Me.lblReturnNo.Name = "lblReturnNo"
        Me.lblReturnNo.Size = New System.Drawing.Size(66, 15)
        Me.lblReturnNo.TabIndex = 30
        Me.lblReturnNo.Text = "Return No"
        '
        'SP_PRODUCT_WAREHOUSE_DETAILBindingSource
        '
        Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource.DataMember = "SP_PRODUCT_WAREHOUSE_DETAIL"
        Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource.DataSource = Me.DS_RETURN_SALE
        '
        'SP_PRODUCT_WAREHOUSE_DETAILTableAdapter
        '
        Me.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.ClearBeforeFill = True
        '
        'SP_SHOW_LIST_RETURN_SALEBindingSource
        '
        Me.SP_SHOW_LIST_RETURN_SALEBindingSource.DataMember = "SP_SHOW_LIST_RETURN_SALE"
        Me.SP_SHOW_LIST_RETURN_SALEBindingSource.DataSource = Me.DS_RETURN_SALE
        '
        'SP_SHOW_LIST_RETURN_SALETableAdapter
        '
        Me.SP_SHOW_LIST_RETURN_SALETableAdapter.ClearBeforeFill = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.cmdUndo)
        Me.GroupBox4.Controls.Add(Me.cmdSave)
        Me.GroupBox4.Controls.Add(Me.cmdEdit)
        Me.GroupBox4.Controls.Add(Me.cmdDelete)
        Me.GroupBox4.Controls.Add(Me.cmdAdd)
        Me.GroupBox4.Location = New System.Drawing.Point(99, 406)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox4.Size = New System.Drawing.Size(515, 55)
        Me.GroupBox4.TabIndex = 38
        Me.GroupBox4.TabStop = False
        '
        'cmdUndo
        '
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(220, 17)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(74, 29)
        Me.cmdUndo.TabIndex = 16
        Me.cmdUndo.Text = "&Undo"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSave.Location = New System.Drawing.Point(321, 17)
        Me.cmdSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(74, 29)
        Me.cmdSave.TabIndex = 17
        Me.cmdSave.Text = "&Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Enabled = False
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(119, 17)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(74, 29)
        Me.cmdEdit.TabIndex = 15
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Enabled = False
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(422, 17)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(74, 29)
        Me.cmdDelete.TabIndex = 18
        Me.cmdDelete.Text = "&Delete"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Enabled = False
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(18, 17)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(74, 29)
        Me.cmdAdd.TabIndex = 14
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'frmEntryReturnSale
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(711, 472)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimizeBox = False
        Me.Name = "frmEntryReturnSale"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Entry Return Sale"
        Me.Text = "Entry Return Sale"
        CType(Me.cmdSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSaleReturn, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_SALE_RETURN_DETAIL_LISTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_RETURN_SALE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SALE_RETURNBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_SHOW_LIST_RETURN_SALEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtCustomer As System.Windows.Forms.TextBox
    Friend WithEvents txtSaleDate As System.Windows.Forms.TextBox
    Friend WithEvents txtReceipNo As System.Windows.Forms.TextBox
    Friend WithEvents txtPayment As System.Windows.Forms.TextBox
    Friend WithEvents lblCustomer As System.Windows.Forms.Label
    Friend WithEvents lblReceiptNo As System.Windows.Forms.Label
    Friend WithEvents lblSaleDate As System.Windows.Forms.Label
    Friend WithEvents lblPayment As System.Windows.Forms.Label
    Friend WithEvents cmdSearch As System.Windows.Forms.PictureBox
    Friend WithEvents DS_RETURN_SALE As DMI_RETAIL_SALE.DS_RETURN_SALE
    Friend WithEvents SP_SALE_RETURN_DETAIL_LISTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_SALE_RETURN_DETAIL_LISTTableAdapter As DMI_RETAIL_SALE.DS_RETURN_SALETableAdapters.SP_SALE_RETURN_DETAIL_LISTTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_SALE.DS_RETURN_SALETableAdapters.TableAdapterManager
    Friend WithEvents dgvSaleReturn As System.Windows.Forms.DataGridView
    Friend WithEvents SALE_RETURNBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SALE_RETURNTableAdapter As DMI_RETAIL_SALE.DS_RETURN_SALETableAdapters.SALE_RETURNTableAdapter
    Friend WithEvents txtReturnNo As System.Windows.Forms.TextBox
    Friend WithEvents dtpReturnDate As System.Windows.Forms.TextBox
    Friend WithEvents lblReturnDate As System.Windows.Forms.Label
    Friend WithEvents ckbReusable As System.Windows.Forms.CheckBox
    Friend WithEvents WAREHOUSEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents WAREHOUSETableAdapter As DMI_RETAIL_SALE.DS_RETURN_SALETableAdapters.WAREHOUSETableAdapter
    Friend WithEvents WAREHOUSE_NAMEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents cmbReturnType As System.Windows.Forms.ComboBox
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents SP_PRODUCT_WAREHOUSE_DETAILBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_PRODUCT_WAREHOUSE_DETAILTableAdapter As DMI_RETAIL_SALE.DS_RETURN_SALETableAdapters.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents lblReturnNo As System.Windows.Forms.Label
    Friend WithEvents lblReturnType As System.Windows.Forms.Label
    Friend WithEvents lblWarehouse As System.Windows.Forms.Label
    Friend WithEvents PRODUCT_CODE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRODUCT_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QUANTITY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RETURNED_QUANTITY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Return_Quantity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RETURNED_DESCRIPTION As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SP_SHOW_LIST_RETURN_SALEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_SHOW_LIST_RETURN_SALETableAdapter As DMI_RETAIL_SALE.DS_RETURN_SALETableAdapters.SP_SHOW_LIST_RETURN_SALETableAdapter
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
End Class
