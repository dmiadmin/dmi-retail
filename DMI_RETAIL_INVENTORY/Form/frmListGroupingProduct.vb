﻿Imports System.Data.SqlClient
Imports System.Data
Public Class frmListGroupingProduct

    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
   

    'Private Sub frmListGroupingProduct_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
    '    If e.KeyCode = Keys.F12 Then
    '        cmdReport_Click(Nothing, Nothing)
    '    End If
    'End Sub

    Private Sub frmListGroupingProduct_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_GROUPING_PRODUCT.SP_LIST_GROUPING_PRODUCT' table. You can move, or remove it, as needed.
        'Me.SP_LIST_GROUPING_PRODUCTTableAdapter.Fill(Me.DS_GROUPING_PRODUCT.SP_LIST_GROUPING_PRODUCT)
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC	    @return_value = [dbo].[SP_LIST_GROUPING_PRODUCT]" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.dgvListGroupingProduct.DataSource = dt

        If Language = "Indonesian" Then
            lblProductCode.Text = "Kode Produk"
            lblProductName.Text = "Nama Produk"
            dgvListGroupingProduct.Columns("PARENT_PRODUCT_CODE").HeaderText = "Kode Induk"
            dgvListGroupingProduct.Columns("PARENT_PRODUCT_NAME").HeaderText = "Nama Produk Induk"
            dgvListGroupingProduct.Columns("CHILD_PRODUCT_CODE").HeaderText = "Kode Turunan"
            dgvListGroupingProduct.Columns("CHILD_PRODUCT_NAME").HeaderText = "Nama Produk Turunan"
            dgvListGroupingProduct.Columns("QUANTITY").HeaderText = "Jumlah"
            dgvListGroupingProduct.Columns("QUANTITY").Width = 50
            dgvListGroupingProduct.Columns("QUANTITY").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvListGroupingProduct.Columns("PARENT_PRODUCT_ID").Visible = False
            dgvListGroupingProduct.Columns("CHILD_PRODUCT_ID").Visible = False
            Me.Text = "Daftar Pengelompokan Produk"
            cmdReport.Text = "Cetak"
            lblNoOfGrouping.Text = "No Pengelompokkan : " & dgvListGroupingProduct.RowCount
        Else
            dgvListGroupingProduct.Columns("PARENT_PRODUCT_CODE").HeaderText = "Parent Code"
            dgvListGroupingProduct.Columns("PARENT_PRODUCT_NAME").HeaderText = "Parent Product Name"
            dgvListGroupingProduct.Columns("CHILD_PRODUCT_CODE").HeaderText = "Child Code"
            dgvListGroupingProduct.Columns("CHILD_PRODUCT_NAME").HeaderText = "Child Product Name"
            dgvListGroupingProduct.Columns("QUANTITY").HeaderText = "Qty"
            dgvListGroupingProduct.Columns("QUANTITY").Width = 50
            dgvListGroupingProduct.Columns("QUANTITY").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvListGroupingProduct.Columns("PARENT_PRODUCT_ID").Visible = False
            dgvListGroupingProduct.Columns("CHILD_PRODUCT_ID").Visible = False
            lblNoOfGrouping.Text = "No of Grouping : " & dgvListGroupingProduct.RowCount
        End If

    End Sub

    Private Sub txtProductCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtProductCode.TextChanged
        'txtProductName.Text = ""

        'SP_LIST_GROUPING_PRODUCTBindingSource.Filter = "PARENT_PRODUCT_CODE LIKE '%" & txtProductCode.Text & "%'"
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC	    @return_value = [dbo].[SP_LIST_GROUPING_PRODUCT]" & vbCrLf & _
                        "@PRODUCT_NAME = N'" & txtProductName.Text & "'," & vbCrLf & _
                        "@PRODUCT_CODE = N'" & txtProductCode.Text & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.dgvListGroupingProduct.DataSource = dt
        lblNoOfGrouping.Text = "No of Grouping : " & dgvListGroupingProduct.RowCount
    End Sub

    Private Sub txtProductName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtProductName.TextChanged
        'txtProductCode.Text = ""

        'SP_LIST_GROUPING_PRODUCTBindingSource.Filter = "PARENT_PRODUCT_NAME LIKE '%" & txtProductName.Text & "%'"
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC	    @return_value = [dbo].[SP_LIST_GROUPING_PRODUCT]" & vbCrLf & _
                        "@PRODUCT_NAME = N'" & txtProductName.Text & "'," & vbCrLf & _
                        "@PRODUCT_CODE = N'" & txtProductCode.Text & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.dgvListGroupingProduct.DataSource = dt
        lblNoOfGrouping.Text = "No of Grouping : " & dgvListGroupingProduct.RowCount
    End Sub

    'Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
    '    Dim RPV As New frmRepGroupingProduct
    '    RPV.ReportViewer1.ZoomPercent = 100
    '    RPV.WindowState = FormWindowState.Maximized
    '    RPV.Show()
    'End Sub

End Class