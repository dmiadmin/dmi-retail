﻿Public Class frmPayableTransaction
    Dim tmpSaveMode As String
    Dim tmpChange As Boolean

    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub

    Private Sub frmPayableTransaction_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If tmpChange = True And DESCRIPTIONTextBox.Enabled = True Then

            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah anda ingin menyimpan perubahan " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If

            If tmpvar = 6 Then
                cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        Else
            Me.Dispose()
        End If
    End Sub

    Private Sub frmPayableTransaction_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_PAYABLE_TRANSACTION.ACCOUNT' table. You can move, or remove it, as needed.
        Me.ACCOUNTTableAdapter.Fill(Me.DS_PAYABLE_TRANSACTION.ACCOUNT)
        'TODO: This line of code loads data into the 'DS_PAYABLE_TRANSACTION.PAYABLE_TRANSACTION' table. You can move, or remove it, as needed.
        Me.PAYABLE_TRANSACTIONTableAdapter.Fill(Me.DS_PAYABLE_TRANSACTION.PAYABLE_TRANSACTION)
        'TODO: This line of code loads data into the 'DS_PAYABLE_TRANSACTION.VIEW_PAYABLE_ACCOUNT' table. You can move, or remove it, as needed.
        Me.VIEW_PAYABLE_ACCOUNTTableAdapter.Fill(Me.DS_PAYABLE_TRANSACTION.VIEW_PAYABLE_ACCOUNT)
        'TODO: This line of code loads data into the 'DS_PAYABLE_TRANSACTION.VIEW_PAYABLE_TRANSACTION' table. You can move, or remove it, as needed.
        Me.VIEW_PAYABLE_TRANSACTIONTableAdapter.Fill(Me.DS_PAYABLE_TRANSACTION.VIEW_PAYABLE_TRANSACTION)

        accFormName = Me.Text

        DisableInputBox(Me)

        PAYABLE_TRANSACTIONTableAdapter.SP_GENERATE_PAYABLE_TRANSACTION_NUMBER(PAYABLE_TRANSACTION_NUMBERTextBox.Text)

        cmdSearchName.Cursor = Cursors.Default
        cmdSearchName.Enabled = False
        cmdSearchName.Visible = False

        cmdAdd.Enabled = True
        cmdSave.Enabled = False
        cmdUndo.Enabled = False

        If ACCOUNT_NUMBERTextBox.Text = "" Then
            cmdEdit.Enabled = False
            cmdDelete.Enabled = False
        Else
            cmdEdit.Enabled = True
            cmdDelete.Enabled = True
        End If

        txtDtp.Text = Format(TRANSACTION_DATEDateTimePicker.Value, "dd-MMM-yyyy")

        tmpChange = False

        If Language = "Indonesian" Then
            Me.Text = "Transaksi Hutang"

            cmdAdd.Text = "&Tambah"
            cmdDelete.Text = "&Hapus"
            cmdUndo.Text = "&Batal"
            cmdEdit.Text = "&Ubah"
            cmdSave.Text = "&Simpan"

            lblAccount.Text = "Akun :"
            lblAmount.Text = "Jumlah :"
            lblDescription.Text = "Keterangan :"
            lblTransDate.Text = "Tgl Transaksi :"
            lblTransNo.Text = "No Transaksi :"
        End If

    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If Trim(RsAccessPrivilege.Item("ALLOW_ADD").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If ACCOUNTBindingSource.Count < 1 Then
            If Language = "English" Then
                MsgBox("There is no Account data." & vbCrLf & "Adding this transaction is cancelled!", vbCritical, "DMI Retail")
            Else
                MsgBox("Tidak ada data Akun." & vbCrLf & "Penambahan terhadap transaksi ini dibatalkan!", vbCritical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Insert"

        EnableInputBox(Me)
        VIEW_PAYABLE_TRANSACTIONBindingSource.AddNew()

        TRANSACTION_DATEDateTimePicker.Enabled = True
        TRANSACTION_DATEDateTimePicker.Value = Now

        PAYABLE_TRANSACTIONTableAdapter.SP_GENERATE_PAYABLE_TRANSACTION_NUMBER(PAYABLE_TRANSACTION_NUMBERTextBox.Text)

        AMOUNTTextBox.Text = "0"
        ACCOUNT_NAMETextBox.Enabled = False

        cmdSearchName.Cursor = Cursors.Hand
        cmdSearchName.Enabled = True
        cmdSearchName.Visible = True

        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdDelete.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        tmpChange = False

    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        tmpSaveMode = ""

        VIEW_PAYABLE_TRANSACTIONBindingSource.CancelEdit()

        DisableInputBox(Me)

        cmdSearchName.Cursor = Cursors.Default
        cmdSearchName.Enabled = False

        cmdAdd.Enabled = True
        cmdSave.Enabled = False
        cmdUndo.Enabled = False

        If ACCOUNT_NUMBERTextBox.Text = "" Then
            cmdEdit.Enabled = False
            cmdDelete.Enabled = False
        Else
            cmdEdit.Enabled = True
            cmdDelete.Enabled = True
        End If

        tmpChange = False

    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        If Trim(RsAccessPrivilege.Item("ALLOW_EDIT").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If ACCOUNT_NUMBERTextBox.Text = "" Or ACCOUNT_NAMETextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak ada Akun Hutang untuk diubah !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There's no Payable Account to edit !", MsgBoxStyle.Critical, "DMI Retail")
            End If

            Exit Sub
        End If

        tmpSaveMode = "Update"

        EnableInputBox(Me)

        PAYABLE_TRANSACTION_NUMBERTextBox.Enabled = False
        ACCOUNT_NAMETextBox.Enabled = False
        ACCOUNT_NUMBERTextBox.Enabled = False

        cmdSearchName.Cursor = Cursors.Default
        cmdSearchName.Enabled = False

        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdDelete.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        tmpChange = False

    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim tmpCheck As String = ""
        If Trim(RsAccessPrivilege.Item("ALLOW_DELETE").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        PAYABLE_TRANSACTIONTableAdapter.SP_CHECK_CLOSING_PERIOD(TRANSACTION_DATEDateTimePicker.Value, tmpCheck)

        If tmpCheck = "TRUE" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menghapus transaksi in!" & vbCrLf & "Transaksi ini telah masuk dalam proses Tutup Buku", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Delete this Transaction!" & vbCrLf & "This Transaction has been in Closed Period", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If ACCOUNT_NAMETextBox.Text = "" Or ACCOUNT_NUMBERTextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak ada Transaksi Hutang untuk dihapus !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There's no Payable Transaction to delete !", MsgBoxStyle.Critical, "DMI Retail")
            End If

            Exit Sub
        End If

        If Language = "Indonesian" Then
            If MsgBox("Hapus Akun ?" & vbCrLf & ACCOUNT_NAMETextBox.Text, _
                  MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
        Else
            If MsgBox("Delete Account ?" & vbCrLf & ACCOUNT_NAMETextBox.Text, _
                  MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
        End If

        PAYABLE_TRANSACTIONTableAdapter.SP_PAYABLE_TRANSACTION("D", _
                                                             VIEW_PAYABLE_TRANSACTIONBindingSource.Current("PAYABLE_TRANSACTION_ID"), _
                                                             VIEW_PAYABLE_TRANSACTIONBindingSource.Current("PAYABLE_TRANSACTION_NUMBER"), _
                                                             ACCOUNT_NUMBERTextBox.Text, _
                                                             VIEW_PAYABLE_TRANSACTIONBindingSource.Current("TRANSACTION_DATE"), _
                                                             VIEW_PAYABLE_TRANSACTIONBindingSource.Current("AMOUNT"), _
                                                             VIEW_PAYABLE_TRANSACTIONBindingSource.Current("DESCRIPTION"), _
                                                             PAYABLE_TRANSACTIONBindingSource.Current("USER_ID_INPUT"), _
                                                             PAYABLE_TRANSACTIONBindingSource.Current("INPUT_DATE"), _
                                                             PAYABLE_TRANSACTIONBindingSource.Current("USER_ID_UPDATE"), _
                                                             PAYABLE_TRANSACTIONBindingSource.Current("UPDATE_DATE"))

        VIEW_PAYABLE_TRANSACTIONBindingSource.RemoveCurrent()
        PAYABLE_TRANSACTIONBindingSource.RemoveCurrent()
        VIEW_PAYABLE_TRANSACTIONBindingSource.Position = 0

        tmpSaveMode = ""

        If Language = "Indonesian" Then
            MsgBox("Data berhasil dihapus !", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully deleted !", MsgBoxStyle.Information, "DMI Retail")
        End If

        DisableInputBox(Me)

        cmdAdd.Enabled = True
        cmdSave.Enabled = False
        cmdUndo.Enabled = False

        If ACCOUNT_NAMETextBox.Text = "" Then
            cmdEdit.Enabled = False
            cmdDelete.Enabled = False
        Else
            cmdEdit.Enabled = True
            cmdDelete.Enabled = True
        End If

        PAYABLE_TRANSACTIONTableAdapter.Fill(Me.DS_PAYABLE_TRANSACTION.PAYABLE_TRANSACTION)
        VIEW_PAYABLE_TRANSACTIONTableAdapter.Fill(Me.DS_PAYABLE_TRANSACTION.VIEW_PAYABLE_TRANSACTION)
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Dim tmpCheck As String = ""
        PAYABLE_TRANSACTIONTableAdapter.SP_CHECK_CLOSING_PERIOD(TRANSACTION_DATEDateTimePicker.Value, tmpCheck)
        If tmpCheck = "TRUE" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menyimpan transaksi ini!" & vbCrLf & "Transaksi ini sudah masuk dalam Tutup Buku", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Save this Transaction!" & vbCrLf & "This Transaction has been in Closed Period", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If
        If ACCOUNT_NUMBERTextBox.Text = "" Or AMOUNTTextBox.Text = "" Or ACCOUNT_NAMETextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Isi semua kolom yang diperlukan !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please fill all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If

            Exit Sub
        End If

        If tmpSaveMode = "Insert" Then
            PAYABLE_TRANSACTIONTableAdapter.SP_PAYABLE_TRANSACTION("I", _
                                                                   0, _
                                                                   PAYABLE_TRANSACTION_NUMBERTextBox.Text, _
                                                                   ACCOUNT_NUMBERTextBox.Text, _
                                                                   TRANSACTION_DATEDateTimePicker.Value, _
                                                                   CInt(AMOUNTTextBox.Text), _
                                                                   DESCRIPTIONTextBox.Text, _
                                                                   mdlGeneral.USER_ID, _
                                                                   Now, _
                                                                   0, _
                                                                   DateSerial(4000, 12, 31))
        ElseIf tmpSaveMode = "Update" Then
            PAYABLE_TRANSACTIONTableAdapter.SP_PAYABLE_TRANSACTION("U", _
                                                                   VIEW_PAYABLE_TRANSACTIONBindingSource.Current("PAYABLE_TRANSACTION_ID"), _
                                                                   VIEW_PAYABLE_TRANSACTIONBindingSource.Current("PAYABLE_TRANSACTION_NUMBER"), _
                                                                   ACCOUNT_NUMBERTextBox.Text, _
                                                                   TRANSACTION_DATEDateTimePicker.Value, _
                                                                   CInt(AMOUNTTextBox.Text), _
                                                                   DESCRIPTIONTextBox.Text, _
                                                                   PAYABLE_TRANSACTIONBindingSource.Current("USER_ID_INPUT"), _
                                                                   PAYABLE_TRANSACTIONBindingSource.Current("INPUT_DATE"), _
                                                                   mdlGeneral.USER_ID, _
                                                                   Now)
        End If

        PAYABLE_TRANSACTIONTableAdapter.Fill(Me.DS_PAYABLE_TRANSACTION.PAYABLE_TRANSACTION)
        VIEW_PAYABLE_TRANSACTIONTableAdapter.Fill(Me.DS_PAYABLE_TRANSACTION.VIEW_PAYABLE_TRANSACTION)

        tmpSaveMode = ""
        VIEW_PAYABLE_TRANSACTIONBindingSource.CancelEdit()
        PAYABLE_TRANSACTIONBindingSource.CancelEdit()

        DisableInputBox(Me)

        cmdSearchName.Cursor = Cursors.Default
        cmdSearchName.Enabled = False
        cmdSearchName.Visible = False

        cmdAdd.Enabled = True
        cmdSave.Enabled = False
        cmdUndo.Enabled = False

        If ACCOUNT_NUMBERTextBox.Text = "" Then
            cmdEdit.Enabled = False
            cmdDelete.Enabled = False
        Else
            cmdEdit.Enabled = True
            cmdDelete.Enabled = True
        End If

        If Language = "Indonesian" Then
            MsgBox("Data berhasil disimpan !", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully saved !", MsgBoxStyle.Information, "DMI Retail")
        End If

    End Sub

    Private Sub ACCOUNT_NUMBERTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ACCOUNT_NUMBERTextBox.TextChanged
        ACCOUNTTableAdapter.SP_GET_ACCOUNT_NAME(ACCOUNT_NUMBERTextBox.Text, ACCOUNT_NAMETextBox.Text)
        tmpChange = True
    End Sub

    Private Sub AMOUNTTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles AMOUNTTextBox.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""
    End Sub

    Private Sub cmdSearchName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearchName.Click
        frmSearchAccount.tmpSearchMode = "PayableAcc-AccountName"
        frmSearchAccount.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        VIEW_PAYABLE_ACCOUNTBindingSource.Position = VIEW_PAYABLE_ACCOUNTBindingSource.Find("ACCOUNT_ID", tmpSearchResult)
        ACCOUNT_NUMBERTextBox.Text = VIEW_PAYABLE_ACCOUNTBindingSource.Current("ACCOUNT_NUMBER")
    End Sub

    Private Sub PAYABLE_TRANSACTION_NUMBERTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PAYABLE_TRANSACTION_NUMBERTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub ACCOUNT_NAMETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ACCOUNT_NAMETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub TRANSACTION_DATEDateTimePicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TRANSACTION_DATEDateTimePicker.ValueChanged
        txtDtp.Text = Format(TRANSACTION_DATEDateTimePicker.Value, "dd-MMM-yyyy")
        tmpChange = True
    End Sub

    Private Sub AMOUNTTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AMOUNTTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub DESCRIPTIONTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DESCRIPTIONTextBox.TextChanged
        tmpChange = True
    End Sub

End Class