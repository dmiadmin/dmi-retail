﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frmListSale
    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter

    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub

    'Private Sub frmListSale_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
    '    If e.KeyCode = Keys.F12 Then
    '        cmdReport_Click(Nothing, Nothing)
    '    End If
    'End Sub

    Private Sub frmListSale_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_SALE.VIEW_LIST_SALE_DETAIL' table. You can move, or remove it, as needed.
        'Me.VIEW_LIST_SALE_DETAILTableAdapter.Fill(Me.DS_SALE.VIEW_LIST_SALE_DETAIL)

        DateTimePicker1.Value = DateSerial(Today.Year, Today.Month, 1)
        DateTimePicker2.Value = DateSerial(Today.Year, Today.Month + 1, 0)

        If Language = "Indonesian" Then
            Me.Text = "Daftar Penjualan"
            'SP_LIST_SALEDataGridView.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            'SP_LIST_SALEDataGridView.Columns("SALE_DATE").HeaderText = "Tanggal Penjualan"
            'SP_LIST_SALEDataGridView.Columns("REMARK").HeaderText = "Keterangan"
            'SP_LIST_SALEDataGridView.Columns("NUMBER_OF_ITEM").HeaderText = "Jumlah Barang"
            'SP_LIST_SALEDataGridView.Columns("GRAND_TOTAL").HeaderText = "Total"
            'SP_LIST_SALEDataGridView.Columns("WAREHOUSE_NAME").HeaderText = "Gudang"
            'SP_LIST_SALEDataGridView.Columns("VOID").HeaderText = "Batal"
            lblFrom.Text = "Dari Periode"
            lblTo.Text = "s/d"
            cmdGenerate.Text = "PROSES"
            cmdReport.Text = "Cetak"
            lblNoOfTrans.Text = "Jumlah Item (s) : " & SP_LIST_SALEDataGridView.RowCount
        Else
            lblNoOfTrans.Text = "No of transaction(s) : " & SP_LIST_SALEDataGridView.RowCount
        End If

        'SP_LIST_SALEDataGridView.Columns("WAREHOUSE_NAME").DataPropertyName = "WAREHOUSE_NAME"

    End Sub

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        Dim tmpDate1 As Date = DateTimePicker1.Value
        Dim tmpdate2 As Date = DateTimePicker2.Value

        sql = " DECLARE	@return_value int" & vbCrLf & _
              " EXEC	@return_value = [dbo].[SP_LIST_SALE]" & vbCrLf & _
              "         @PERIOD1 = '" & tmpDate1 & "'," & vbCrLf & _
              "         @PERIOD2 = '" & tmpdate2 & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"

        Try
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.SP_LIST_SALEDataGridView.DataSource = dt

            If Language = "Indonesian" Then
                SP_LIST_SALEDataGridView.Columns("RECEIPT_NO").HeaderText = "No Faktur"
                SP_LIST_SALEDataGridView.Columns("SALE_DATE").HeaderText = "Tanggal Penjualan"
                SP_LIST_SALEDataGridView.Columns("REMARK").HeaderText = "Keterangan"
                SP_LIST_SALEDataGridView.Columns("NUMBER_OF_ITEM").HeaderText = "Jumlah Barang"
                SP_LIST_SALEDataGridView.Columns("GRAND_TOTAL").HeaderText = "Total"
                SP_LIST_SALEDataGridView.Columns("WAREHOUSE_NAME").HeaderText = "Gudang"
                SP_LIST_SALEDataGridView.Columns("VOID").HeaderText = "Batal"
                SP_LIST_SALEDataGridView.Columns("AMOUNT_DISCOUNT").HeaderText = "Disc1"
                SP_LIST_SALEDataGridView.Columns("DISCOUNT_2").HeaderText = "Disc2"
                SP_LIST_SALEDataGridView.Columns("DISCOUNT_3").HeaderText = "Disc3"
                SP_LIST_SALEDataGridView.Columns("TAX_AMOUNT").HeaderText = "Pajak"
                SP_LIST_SALEDataGridView.Columns("DOWN_PAYMENT").HeaderText = "DP"
                SP_LIST_SALEDataGridView.Columns("SALESMAN").HeaderText = "Salesman"
                SP_LIST_SALEDataGridView.Columns("PAYMENT_METHOD_NAME").HeaderText = "Metode Pembayaran"
                SP_LIST_SALEDataGridView.Columns("MATURITY_DATE").HeaderText = "Jatuh Tempo"
                SP_LIST_SALEDataGridView.Columns("CUSTOMER_NAME").HeaderText = "Pelanggan"
                    ''''setting dgv''''
                SP_LIST_SALEDataGridView.Columns("SALE_ID").Visible = False
                SP_LIST_SALEDataGridView.Columns("CUSTOMER_ID").Visible = False
                SP_LIST_SALEDataGridView.Columns("WAREHOUSE_ID").Visible = False
                SP_LIST_SALEDataGridView.Columns("SALE_TYPE").Visible = False
                SP_LIST_SALEDataGridView.Columns("REP_PERIOD1").Visible = False
                SP_LIST_SALEDataGridView.Columns("REP_PERIOD2").Visible = False
                SP_LIST_SALEDataGridView.Columns("SALE_ORDER_ID").Visible = False
                SP_LIST_SALEDataGridView.Columns("SALE_ORDER_NO").Visible = False
                SP_LIST_SALEDataGridView.Columns("SALE_ORDER_DATE").Visible = False
                SP_LIST_SALEDataGridView.Columns("AMOUNT_DISCOUNT").Width = 50
                SP_LIST_SALEDataGridView.Columns("DISCOUNT_2").Width = 45
                SP_LIST_SALEDataGridView.Columns("DISCOUNT_3").Width = 45
                SP_LIST_SALEDataGridView.Columns("TAX_AMOUNT").Width = 45
                SP_LIST_SALEDataGridView.Columns("DOWN_PAYMENT").Width = 55
                SP_LIST_SALEDataGridView.Columns("SALESMAN").Width = 60
                SP_LIST_SALEDataGridView.Columns("PAYMENT_METHOD_NAME").Width = 55
                SP_LIST_SALEDataGridView.Columns("NUMBER_OF_ITEM").Width = 45
                SP_LIST_SALEDataGridView.Columns("RECEIPT_NO").Width = 65
                SP_LIST_SALEDataGridView.Columns("SALE_DATE").Width = 80
                SP_LIST_SALEDataGridView.Columns("REMARK").Width = 65
                SP_LIST_SALEDataGridView.Columns("WAREHOUSE_NAME").Width = 75
                SP_LIST_SALEDataGridView.Columns("VOID").Width = 40
                SP_LIST_SALEDataGridView.Columns("MATURITY_DATE").Width = 80
                SP_LIST_SALEDataGridView.Columns("CUSTOMER_NAME").Width = 65
                SP_LIST_SALEDataGridView.Columns("GRAND_TOTAL").Width = 75
                SP_LIST_SALEDataGridView.Columns("AMOUNT_DISCOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_SALEDataGridView.Columns("DISCOUNT_2").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_SALEDataGridView.Columns("DISCOUNT_3").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_SALEDataGridView.Columns("TAX_AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_SALEDataGridView.Columns("DOWN_PAYMENT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_SALEDataGridView.Columns("NUMBER_OF_ITEM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_SALEDataGridView.Columns("GRAND_TOTAL").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_SALEDataGridView.Columns("SALE_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_SALEDataGridView.Columns("MATURITY_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_SALEDataGridView.Columns("DISCOUNT_2").DefaultCellStyle.Format = "n0"
                SP_LIST_SALEDataGridView.Columns("DISCOUNT_3").DefaultCellStyle.Format = "n0"
                SP_LIST_SALEDataGridView.Columns("TAX_AMOUNT").DefaultCellStyle.Format = "n0"
                SP_LIST_SALEDataGridView.Columns("DOWN_PAYMENT").DefaultCellStyle.Format = "n0"
                SP_LIST_SALEDataGridView.Columns("GRAND_TOTAL").DefaultCellStyle.Format = "n0"
                SP_LIST_SALEDataGridView.Columns("SALE_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
                SP_LIST_SALEDataGridView.Columns("MATURITY_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"

                lblNoOfTrans.Text = "Jumlah Item (s) : " & SP_LIST_SALEDataGridView.RowCount
            Else
                SP_LIST_SALEDataGridView.Columns("RECEIPT_NO").HeaderText = "Receipt No"
                SP_LIST_SALEDataGridView.Columns("SALE_DATE").HeaderText = "Sale Date"
                SP_LIST_SALEDataGridView.Columns("REMARK").HeaderText = "Remark"
                SP_LIST_SALEDataGridView.Columns("NUMBER_OF_ITEM").HeaderText = "No of Item"
                SP_LIST_SALEDataGridView.Columns("GRAND_TOTAL").HeaderText = "Total"
                SP_LIST_SALEDataGridView.Columns("WAREHOUSE_NAME").HeaderText = "Warehouse"
                SP_LIST_SALEDataGridView.Columns("VOID").HeaderText = "Void"
                SP_LIST_SALEDataGridView.Columns("AMOUNT_DISCOUNT").HeaderText = "Disc1"
                SP_LIST_SALEDataGridView.Columns("DISCOUNT_2").HeaderText = "Disc2"
                SP_LIST_SALEDataGridView.Columns("DISCOUNT_3").HeaderText = "Disc3"
                SP_LIST_SALEDataGridView.Columns("TAX_AMOUNT").HeaderText = "Tax"
                SP_LIST_SALEDataGridView.Columns("DOWN_PAYMENT").HeaderText = "DP"
                SP_LIST_SALEDataGridView.Columns("SALESMAN").HeaderText = "Salesman"
                SP_LIST_SALEDataGridView.Columns("PAYMENT_METHOD_NAME").HeaderText = "Payment Method"
                SP_LIST_SALEDataGridView.Columns("MATURITY_DATE").HeaderText = "Marturity Date"
                SP_LIST_SALEDataGridView.Columns("CUSTOMER_NAME").HeaderText = "Customer"
                    ''''setting dgv''''
                SP_LIST_SALEDataGridView.Columns("SALE_ID").Visible = False
                SP_LIST_SALEDataGridView.Columns("CUSTOMER_ID").Visible = False
                SP_LIST_SALEDataGridView.Columns("WAREHOUSE_ID").Visible = False
                SP_LIST_SALEDataGridView.Columns("SALE_TYPE").Visible = False
                SP_LIST_SALEDataGridView.Columns("REP_PERIOD1").Visible = False
                SP_LIST_SALEDataGridView.Columns("REP_PERIOD2").Visible = False
                SP_LIST_SALEDataGridView.Columns("SALE_ORDER_ID").Visible = False
                SP_LIST_SALEDataGridView.Columns("SALE_ORDER_NO").Visible = False
                SP_LIST_SALEDataGridView.Columns("SALE_ORDER_DATE").Visible = False
                SP_LIST_SALEDataGridView.Columns("AMOUNT_DISCOUNT").Width = 50
                SP_LIST_SALEDataGridView.Columns("DISCOUNT_2").Width = 45
                SP_LIST_SALEDataGridView.Columns("DISCOUNT_3").Width = 45
                SP_LIST_SALEDataGridView.Columns("TAX_AMOUNT").Width = 45
                SP_LIST_SALEDataGridView.Columns("DOWN_PAYMENT").Width = 55
                SP_LIST_SALEDataGridView.Columns("SALESMAN").Width = 60
                SP_LIST_SALEDataGridView.Columns("PAYMENT_METHOD_NAME").Width = 55
                SP_LIST_SALEDataGridView.Columns("NUMBER_OF_ITEM").Width = 45
                SP_LIST_SALEDataGridView.Columns("RECEIPT_NO").Width = 65
                SP_LIST_SALEDataGridView.Columns("SALE_DATE").Width = 80
                SP_LIST_SALEDataGridView.Columns("REMARK").Width = 65
                SP_LIST_SALEDataGridView.Columns("WAREHOUSE_NAME").Width = 75
                SP_LIST_SALEDataGridView.Columns("VOID").Width = 40
                SP_LIST_SALEDataGridView.Columns("MATURITY_DATE").Width = 80
                SP_LIST_SALEDataGridView.Columns("CUSTOMER_NAME").Width = 65
                SP_LIST_SALEDataGridView.Columns("GRAND_TOTAL").Width = 75
                SP_LIST_SALEDataGridView.Columns("AMOUNT_DISCOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_SALEDataGridView.Columns("DISCOUNT_2").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_SALEDataGridView.Columns("DISCOUNT_3").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_SALEDataGridView.Columns("TAX_AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_SALEDataGridView.Columns("DOWN_PAYMENT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_SALEDataGridView.Columns("NUMBER_OF_ITEM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_SALEDataGridView.Columns("GRAND_TOTAL").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_SALEDataGridView.Columns("SALE_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_SALEDataGridView.Columns("MATURITY_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                SP_LIST_SALEDataGridView.Columns("DISCOUNT_2").DefaultCellStyle.Format = "n0"
                SP_LIST_SALEDataGridView.Columns("DISCOUNT_3").DefaultCellStyle.Format = "n0"
                SP_LIST_SALEDataGridView.Columns("TAX_AMOUNT").DefaultCellStyle.Format = "n0"
                SP_LIST_SALEDataGridView.Columns("DOWN_PAYMENT").DefaultCellStyle.Format = "n0"
                SP_LIST_SALEDataGridView.Columns("GRAND_TOTAL").DefaultCellStyle.Format = "n0"
                SP_LIST_SALEDataGridView.Columns("SALE_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
                SP_LIST_SALEDataGridView.Columns("MATURITY_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"

                lblNoOfTrans.Text = "No of transaction(s) : " & SP_LIST_SALEDataGridView.RowCount
                End If
        Catch ex As Exception

            End Try
    End Sub

    Private Sub SP_LIST_SALEDataGridView_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles SP_LIST_SALEDataGridView.CellDoubleClick
        Try
            With frmEntrySale
                .tmpSaleId = SP_LIST_SALEDataGridView.Item("SALE_ID", e.RowIndex).Value
                .SALE_DATEDateTimePicker.Value = Format(SP_LIST_SALEDataGridView.Item("SALE_DATE", e.RowIndex).Value, "dd-MMM-yyyy")
                .txtDTP.Text = Format(SP_LIST_SALEDataGridView.Item("SALE_DATE", e.RowIndex).Value, "dd-MMM-yyyy")
                .tmpReceiptNo = SP_LIST_SALEDataGridView.Item("RECEIPT_NO", e.RowIndex).Value
                If SP_LIST_SALEDataGridView.Item("CUSTOMER_NAME", e.RowIndex).Value.ToString = "" Then
                    .tmpCustomer = 0
                Else
                    .tmpCustomer = SP_LIST_SALEDataGridView.Item("CUSTOMER_ID", e.RowIndex).Value
                End If
                .MATURITY_DATEDateTimePicker.Value = SP_LIST_SALEDataGridView.Item("MATURITY_DATE", e.RowIndex).Value
                .REMARKTextBox.Text = SP_LIST_SALEDataGridView.Item("REMARK", e.RowIndex).Value
                .GRAND_TOTALTextBox.Text = FormatNumber(SP_LIST_SALEDataGridView.Item("GRAND_TOTAL", e.RowIndex).Value, 0)
                .DOWN_PAYMENTTextBox.Text = FormatNumber(SP_LIST_SALEDataGridView.Item("DOWN_PAYMENT", e.RowIndex).Value, 0)
                .TAX_AMOUNTTextBox.Text = FormatNumber(SP_LIST_SALEDataGridView.Item("TAX_AMOUNT", e.RowIndex).Value, 0)
                '.AMOUNT_DISCOUNTTextBox.Text = FormatNumber(SP_LIST_SALEBindingSource.Current("AMOUNT_DISCOUNT"), 0)
                If IsDBNull(SP_LIST_SALEDataGridView.Item("WAREHOUSE_ID", e.RowIndex).Value) Then
                    .tmpWarehouse = 0
                Else
                    .tmpWarehouse = .WAREHOUSEBindingSource.Find("WAREHOUSE_ID", SP_LIST_SALEDataGridView.Item("WAREHOUSE_ID", e.RowIndex).Value)
                End If
                .PAYMENT_METHOD_IDComboBox.Text = SP_LIST_SALEDataGridView.Item("PAYMENT_METHOD_NAME", e.RowIndex).Value.ToString
                .cmbSALESMAN_NAME.Text = SP_LIST_SALEDataGridView.Item("SALESMAN", e.RowIndex).Value.ToString
                .txtDisc2.Text = FormatNumber(SP_LIST_SALEDataGridView.Item("DISCOUNT_2", e.RowIndex).Value, 0)
                .txtDisc3.Text = FormatNumber(SP_LIST_SALEDataGridView("DISCOUNT_3", e.RowIndex).Value, 0)
                tmpReceiptNoForPrinting = SP_LIST_SALEDataGridView("RECEIPT_NO", e.RowIndex).Value
                .GRAND_TOTALTextBox.Text = FormatNumber(SP_LIST_SALEDataGridView("GRAND_TOTAL", e.RowIndex).Value, 0)
                If Not IsDBNull(SP_LIST_SALEDataGridView.Item("SALE_ORDER_ID", e.RowIndex).Value) Then
                    .tmpSaleOrderID = SP_LIST_SALEDataGridView.Item("SALE_ORDER_ID", e.RowIndex).Value
                End If

                If Not IsDBNull(SP_LIST_SALEDataGridView.Item("SALE_ORDER_NO", e.RowIndex).Value) Then
                    .tmpSaleOrderNo = SP_LIST_SALEDataGridView.Item("SALE_ORDER_NO", e.RowIndex).Value
                Else
                    .tmpSaleOrderNo = ""
                End If

                If Not IsDBNull(SP_LIST_SALEDataGridView.Item("SALE_ORDER_DATE", e.RowIndex).Value) Then
                    .tmpSaleOrderDate = SP_LIST_SALEDataGridView.Item("SALE_ORDER_DATE", e.RowIndex).Value
                Else
                    .tmpSaleOrderDate = Now
                End If

                .cmdAdd.Enabled = False
                .cmdEdit.Enabled = False
                .cmdUndo.Enabled = True
                .cmdSave.Enabled = False
                If SP_LIST_SALEDataGridView.Item("VOID", e.RowIndex).Value.ToString = "" Then
                    .cmdDelete.Enabled = True
                Else
                    If SP_LIST_SALEDataGridView.Item("VOID", e.RowIndex).Value.ToString = "TRUE" Then
                        .cmdDelete.Enabled = False
                    End If
                End If
                .tmpSaveMode = "Update"
            End With

            frmEntrySale.ShowDialog()
            frmEntrySale.Close()

            'Me.VIEW_LIST_SALE_DETAILTableAdapter.Fill(Me.DS_SALE.VIEW_LIST_SALE_DETAIL)
            'Me.SP_LIST_SALETableAdapter.Fill(Me.DS_SALE.SP_LIST_SALE, DateTimePicker1.Value, DateTimePicker2.Value)
        Catch ex As Exception

        End Try

        cmdGenerate_Click(Nothing, Nothing)
    End Sub

    'Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
    '    Dim objf As New frmRepSale
    '    objf.date1 = DateTimePicker1.Value
    '    objf.date2 = DateTimePicker2.Value
    '    objf.ReportViewer1.ShowRefreshButton = False
    '    objf.ReportViewer1.ZoomPercent = 100
    '    objf.WindowState = FormWindowState.Maximized
    '    objf.Show()
    'End Sub

End Class