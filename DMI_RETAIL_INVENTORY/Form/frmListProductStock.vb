﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frmListProductStock
    Dim tmpChange As Boolean
    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter

    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub

    'Private Sub frmListProductStock_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
    '    If e.KeyCode = Keys.F12 Then
    '        cmdReport_Click(Nothing, Nothing)
    '    End If
    'End Sub

    Private Sub frmListProductStock_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ''TODO: This line of code loads data into the 'DS_PRODUCT_STOCK.SP_LIST_PRODUCT_GOODS' table. You can move, or remove it, as needed.
        'Me.SP_LIST_PRODUCT_GOODSTableAdapter.Fill(Me.DS_PRODUCT_STOCK.SP_LIST_PRODUCT_GOODS, Now)
        ''TODO: This line of code loads data into the 'DS_PRODUCT_STOCK.PRODUCT' table. You can move, or remove it, as needed.
        'Me.PRODUCTTableAdapter.Fill(Me.DS_PRODUCT_STOCK.PRODUCT)
        If Language = "Indonesian" Then
            Me.Text = "Stok Produk"
            Label1.Text = "Nama Produk"

            cmdGenerate.Text = "Proses"
            cmdReport.Text = "Cetak"
            CheckBox1.Text = "Termasuk Gudang Barang Sisa"


        End If
        If tmpvar = 6 Then
            tmpChange = True
        End If
        txtFilter.Enabled = False
        CheckBox1.Enabled = False
    End Sub

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click

        If Not ValidateAllComboBox(Me) Then
            MsgBox("You input value which is not in the list.", MsgBoxStyle.Critical, "DMI Retail")
            Exit Sub
        End If

        Cursor = Cursors.WaitCursor


        If CheckBox1.Checked = True Then
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = "DECLARE	@return_value int" & vbCrLf & _
                  "EXEC	    @return_value = [dbo].[FC_PRODUCT_STOCK]" & vbCrLf & _
                  "		    @PRODUCT_NAME = N''," & vbCrLf & _
                  " 		@PERIOD = N'" & Now & "'," & vbCrLf & _
                  " 		@STATE = N''" & vbCrLf & _
                  "SELECT	'Return Value' = @return_value"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.FC_PRODUCT_STOCKDataGridView.DataSource = dt
        Else
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = "DECLARE	@return_value int" & vbCrLf & _
                  "EXEC	    @return_value = [dbo].[FC_PRODUCT_STOCK]" & vbCrLf & _
                  "		    @PRODUCT_NAME = N''," & vbCrLf & _
                  " 		@PERIOD = N'" & Now & "'," & vbCrLf & _
                  " 		@STATE = N'HIDE'" & vbCrLf & _
                  "SELECT	'Return Value' = @return_value"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.FC_PRODUCT_STOCKDataGridView.DataSource = dt
        End If
        If Language = "Indonesian" Then
            FC_PRODUCT_STOCKDataGridView.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            FC_PRODUCT_STOCKDataGridView.Columns("QUANTITY").HeaderText = "Jumlah"
            FC_PRODUCT_STOCKDataGridView.Columns("PRODUCT_ID").Visible = False
            FC_PRODUCT_STOCKDataGridView.Columns("PRODUCT_NAME").Width = 300
            FC_PRODUCT_STOCKDataGridView.Columns("QUANTITY").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        Else
            FC_PRODUCT_STOCKDataGridView.Columns("PRODUCT_NAME").HeaderText = "Product Name"
            FC_PRODUCT_STOCKDataGridView.Columns("QUANTITY").HeaderText = "Quantity"
            FC_PRODUCT_STOCKDataGridView.Columns("PRODUCT_ID").Visible = False
            FC_PRODUCT_STOCKDataGridView.Columns("PRODUCT_NAME").Width = 300
            FC_PRODUCT_STOCKDataGridView.Columns("QUANTITY").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End If

        Cursor = Cursors.Default
        txtFilter.Enabled = True
        CheckBox1.Enabled = True
    End Sub

    Private Sub frmListProductStock_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
        'If Me.Visible = True And SP_LIST_PRODUCT_GOODSBindingSource.Count < 1 Then
        '    MsgBox("There is no Product to be Processed." & vbCrLf & _
        '           "This form will be closed!", MsgBoxStyle.Critical, "DMI Retail")
        '    Me.Close()
        'End If

    End Sub

    'Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
    '    Dim RPV As New frmRepProductStock
    '    RPV.txtfilter = txtFilter.Text
    '    RPV.ReportViewer1.ZoomPercent = 100
    '    RPV.WindowState = FormWindowState.Maximized
    '    RPV.Show()
    'End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        Cursor = Cursors.WaitCursor

        If CheckBox1.Checked = True Then
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = "DECLARE	@return_value int" & vbCrLf & _
                  "EXEC	    @return_value = [dbo].[FC_PRODUCT_STOCK]" & vbCrLf & _
                  "		    @PRODUCT_NAME = N'" & txtFilter.Text & "'," & vbCrLf & _
                  " 		@PERIOD = N'" & Now & "'," & vbCrLf & _
                  " 		@STATE = N''" & vbCrLf & _
                  "SELECT	'Return Value' = @return_value"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.FC_PRODUCT_STOCKDataGridView.DataSource = dt
        Else
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = "DECLARE	@return_value int" & vbCrLf & _
                  "EXEC	    @return_value = [dbo].[FC_PRODUCT_STOCK]" & vbCrLf & _
                  "		    @PRODUCT_NAME = N'" & txtFilter.Text & "'," & vbCrLf & _
                  " 		@PERIOD = N'" & Now & "'," & vbCrLf & _
                  " 		@STATE = N'HIDE'" & vbCrLf & _
                  "SELECT	'Return Value' = @return_value"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.FC_PRODUCT_STOCKDataGridView.DataSource = dt
        End If

        Cursor = Cursors.Default
    End Sub

    Private Sub txtFilter_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFilter.TextChanged
        If CheckBox1.Checked = True Then
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = "DECLARE	@return_value int" & vbCrLf & _
                  "EXEC	    @return_value = [dbo].[FC_PRODUCT_STOCK]" & vbCrLf & _
                  "		    @PRODUCT_NAME = N'" & txtFilter.Text & "'," & vbCrLf & _
                  " 		@PERIOD = N'" & Now & "'," & vbCrLf & _
                  " 		@STATE = N''" & vbCrLf & _
                  "SELECT	'Return Value' = @return_value"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.FC_PRODUCT_STOCKDataGridView.DataSource = dt
        Else
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = "DECLARE	@return_value int" & vbCrLf & _
                  "EXEC	    @return_value = [dbo].[FC_PRODUCT_STOCK]" & vbCrLf & _
                  "		    @PRODUCT_NAME = N'" & txtFilter.Text & "'," & vbCrLf & _
                  " 		@PERIOD = N'" & Now & "'," & vbCrLf & _
                  " 		@STATE = N'HIDE'" & vbCrLf & _
                  "SELECT	'Return Value' = @return_value"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.FC_PRODUCT_STOCKDataGridView.DataSource = dt
        End If
    End Sub
End Class