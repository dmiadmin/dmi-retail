﻿Public Class frmEntryProductGroup
    Dim tmpSaveMode, tmpkey, tmpEditNo, tmpEditName As String
    Dim tmpNoOfPCName, tmpResult As Integer
    Dim tmpEndDate As DateTime
    
    Private Sub PRODUCT_GROUPBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.PRODUCT_GROUPBindingSource.EndEdit()
        'Me.TableAdapterManager.UpdateAll(Me.DS_PRODUCT_GROUP)
    End Sub

    Private Sub frmEntryProductGroup_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_PRODUCT_GROUP.PRODUCT_GROUP' table. You can move, or remove it, as needed.
        'Me.PRODUCT_GROUPTableAdapter.Fill(Me.DS_PRODUCT_GROUP.PRODUCT_GROUP)
        DisableInputBox(Me)
        cmdSave.Enabled = False
        cmdUndo.Enabled = False
        chkActive.Enabled = False
        If PRODUCT_GROUP_NAMETextBox.Text <> "" Then
            If PRODUCT_GROUPBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
                cmdDelete.Enabled = True
                cmdEdit.Enabled = True
            End If
        End If
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        EnableInputBox(Me)

        tmpSaveMode = "Insert"
        PRODUCT_GROUPBindingSource.AddNew()
        PRODUCT_GROUPBindingNavigator.Enabled = False
        'PRODUCT_GROUPTableAdapter.SP_GENERATE_PRODUCT_GROUP_NO(PRODUCT_GROUP_CODETextBox.Text)

        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False

        chkActive.Enabled = True
        chkActive.Checked = True
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        EnableInputBox(Me)
        PRODUCT_GROUPBindingNavigator.Enabled = False
        tmpSaveMode = "Update"

        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        chkActive.Enabled = True
        tmpEditNo = PRODUCT_GROUP_CODETextBox.Text
        tmpEditName = PRODUCT_GROUP_NAMETextBox.Text
        If PRODUCT_GROUP_NAMETextBox.Text <> "" Then
            If PRODUCT_GROUPBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
            End If
        End If
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        PRODUCT_GROUPBindingSource.CancelEdit()
        PRODUCT_GROUPBindingNavigator.Enabled = True
        tmpSaveMode = ""
        DisableInputBox(Me)

        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        chkActive.Enabled = False

        PRODUCT_GROUPBindingSource_CurrentChanged(Nothing, Nothing)
    End Sub

    Private Sub PRODUCT_GROUPBindingSource_CurrentChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PRODUCT_GROUPBindingSource.CurrentChanged
        If PRODUCT_GROUP_NAMETextBox.Text <> "" Then
            If PRODUCT_GROUPBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
                cmdDelete.Enabled = True
                cmdEdit.Enabled = True
            End If
        End If
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Me.Validate()

        If PRODUCT_GROUP_CODETextBox.Text = "" Or PRODUCT_GROUP_NAMETextBox.Text = "" Or PROFIT_PERCENTAGETextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpkey = PRODUCT_GROUP_NAMETextBox.Text

        If tmpSaveMode = "Insert" Then
            'PRODUCT_GROUPTableAdapter.SP_CHECK_MASTER_CODE("PRODUCT_GROUP", PRODUCT_GROUP_CODETextBox.Text, tmpResult)
            'PRODUCT_GROUPTableAdapter.SP_CHECK_PRODUCT_GROUP_NAME(PRODUCT_GROUP_NAMETextBox.Text, tmpNoOfPCName)
            If tmpNoOfPCName > 0 Or tmpResult > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Nama Produk Group sudah ada di dalam database." & vbCrLf & _
                       "Silahkan isi nama lokasi yang lain!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Product Group Name already exist in Database." & vbCrLf & _
                           "Please enter another Location Name!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                While tmpResult > 0
                    'PRODUCT_GROUPTableAdapter.SP_GENERATE_PRODUCT_GROUP_NO(PRODUCT_GROUP_CODETextBox.Text)
                    'PRODUCT_GROUPTableAdapter.SP_CHECK_MASTER_CODE("PRODUCT_GROUP", PRODUCT_GROUP_CODETextBox.Text, tmpResult)
                End While
                Exit Sub
            End If

            'PRODUCT_GROUPTableAdapter.SP_PRODUCT_GROUP(0, _
            '                                             PRODUCT_GROUP_CODETextBox.Text, _
            '                                             PRODUCT_GROUP_NAMETextBox.Text, _
            '                                             PROFIT_PERCENTAGETextBox.Text, _
            '                                             USER_ID, _
            '                                             Now, _
            '                                             0, _
            '                                             DateSerial(4000, 12, 31), _
            '                                             DateSerial(4000, 12, 31), _
            '                                             Now, _
            '                                             "I")
        ElseIf tmpSaveMode = "Update" Then
            If tmpEditNo <> PRODUCT_GROUP_CODETextBox.Text Then
                'PRODUCT_GROUPTableAdapter.SP_CHECK_MASTER_CODE("PRODUCT_GROUP", PRODUCT_GROUP_CODETextBox.Text, tmpResult)
                If tmpResult > 0 Then
                    If Language = "Indonesian" Then
                        MsgBox("No Transaksi telah digunakan oleh user lain." & vbCrLf & _
                                                   "Nomor transaksi baru akan digunakan untuk transaksi ini.", _
                                                   MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("Receipt Number is used by another User." & vbCrLf & _
                                                            "New Receipt Number will be assigned to this transaction.", _
                                                            MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    PRODUCT_GROUP_CODETextBox.Focus()
                    Exit Sub
                End If
            End If

            If tmpEditName = PRODUCT_GROUP_NAMETextBox.Text Then
                tmpNoOfPCName = 0
            Else
                'PRODUCT_GROUPTableAdapter.SP_CHECK_PRODUCT_GROUP_NAME(PRODUCT_GROUP_NAMETextBox.Text, tmpNoOfPCName)
            End If

            If tmpNoOfPCName > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Nama indikasi sudah ada di dalam database." & vbCrLf & _
                       "Silahkan isi nama lokasi yang lain!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Indikasi Name already exist in Database." & vbCrLf & _
                           "Please enter another indikasi Name!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                PRODUCT_GROUP_NAMETextBox.Focus()
                Exit Sub
            End If

            If chkActive.Checked = True Then
                tmpEndDate = DateSerial(4000, 12, 31)
            Else
                tmpEndDate = Now
            End If

            'PRODUCT_GROUPTableAdapter.SP_PRODUCT_GROUP(PRODUCT_GROUPBindingSource.Current("PRODUCT_GROUP_ID"), _
            '                                             PRODUCT_GROUP_CODETextBox.Text, _
            '                                             PRODUCT_GROUP_NAMETextBox.Text, _
            '                                             PROFIT_PERCENTAGETextBox.Text, _
            '                                             PRODUCT_GROUPBindingSource.Current("USER_INPUT"), _
            '                                             PRODUCT_GROUPBindingSource.Current("INPUT_DATE"), _
            '                                             USER_ID, _
            '                                             Now, _
            '                                             tmpEndDate, _
            '                                             PRODUCT_GROUPBindingSource.Current("EFFECTIVE_START_DATE"), _
            '                                             "U")
        End If

        tmpSaveMode = ""

        DisableInputBox(Me)
        PRODUCT_GROUPBindingSource.CancelEdit()

        PRODUCT_GROUPBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        chkActive.Enabled = False

        'Me.PRODUCT_GROUPTableAdapter.Fill(DS_PRODUCT_GROUP.PRODUCT_GROUP)
        Me.PRODUCT_GROUPBindingSource.Position = PRODUCT_GROUPBindingSource.Find("PRODUCT_GROUP_NAME", tmpkey)

        If Language = "Indonesian" Then
            MsgBox("Data berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
        End If

        PRODUCT_GROUPBindingSource_CurrentChanged(Nothing, Nothing)

    End Sub

    Private Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If MsgBox("Delete PRODUCT GROUP?" & vbCrLf & PRODUCT_GROUP_NAMETextBox.Text, _
                  MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub

        Dim tmpUserProGroup As Integer
        'PRODUCT_GROUPTableAdapter.SP_CHECK_USED_PRODUCT_GROUP(PRODUCT_GROUP_NAMETextBox.Text, tmpUserProGroup)
        If tmpUserProGroup > 0 Then
            If Language = "Indonesian" Then
                MsgBox("Grup produk ini telah terdapat di produk." & vbCrLf & _
                      "Anda tidak dapat menghapus data ini !", _
                      MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("This product group is used by Product(s)." & vbCrLf & _
                        "You can not delete this data !", _
                        MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        'PRODUCT_GROUPTableAdapter.SP_PRODUCT_GROUP(PRODUCT_GROUPBindingSource.Current("PRODUCT_GROUP_ID"), _
        '                                                 PRODUCT_GROUPBindingSource.Current("PRODUCT_GROUP_CODE"), _
        '                                                 PRODUCT_GROUPBindingSource.Current("PRODUCT_GROUP_NAME"), _
        '                                                 PRODUCT_GROUPBindingSource.Current("PROFIT_PERCENTAGE"), _
        '                                                 PRODUCT_GROUPBindingSource.Current("USER_INPUT"), _
        '                                                 PRODUCT_GROUPBindingSource.Current("INPUT_DATE"), _
        '                                                 USER_ID, _
        '                                                 Now, _
        '                                                 PRODUCT_GROUPBindingSource.Current("EFFECTIVE_END_DATE"), _
        '                                                 PRODUCT_GROUPBindingSource.Current("EFFECTIVE_END_DATE"), _
        '                                                 "D")

        PRODUCT_GROUPBindingSource.RemoveCurrent()
        'Me.PRODUCT_GROUPTableAdapter.Fill(DS_PRODUCT_GROUP.PRODUCT_GROUP)
        Me.PRODUCT_GROUPBindingSource.Position = PRODUCT_GROUPBindingSource.Find("PRODUCT_GROUP_NAME", tmpkey)

        Try
            If PRODUCT_GROUPBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
                cmdDelete.Enabled = True
                cmdEdit.Enabled = True
            End If
        Catch
        End Try

        If Language = "Indonesian" Then
            MsgBox("Data successfully deleted !", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully deleted !", MsgBoxStyle.Information, "DMI Retail")
        End If

        chkActive.Enabled = False
    End Sub

    Private Sub PROFIT_PERCENTAGETextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PROFIT_PERCENTAGETextBox.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Asc(e.KeyChar) < Asc("0") Or Asc(e.KeyChar) > Asc("9") Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub PROFIT_PERCENTAGETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PROFIT_PERCENTAGETextBox.TextChanged
        If PROFIT_PERCENTAGETextBox.Text = "" Then
            PROFIT_PERCENTAGETextBox.Text = "0"
        End If
    End Sub
End Class