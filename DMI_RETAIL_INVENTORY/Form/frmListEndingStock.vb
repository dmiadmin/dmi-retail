﻿Imports System.Data.SqlClient
Imports System.Data
Public Class frmListEndingStock
    Dim tmpTotal As Integer

    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
   

    Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Me.SP_LIST_ENDING_STOCKTableAdapter.Fill(Me.DS_LIST_ENDING_STOCK.SP_LIST_ENDING_STOCK, New System.Nullable(Of Date)(CType(dtpPeriod.Text, Date)), txtFilter.Text)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        Me.Cursor = Cursors.WaitCursor

        'SP_LIST_ENDING_STOCKTableAdapter.Fill(Me.DS_LIST_ENDING_STOCK.SP_LIST_ENDING_STOCK, dtpPeriod.Value, txtFilter.Text)
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC	@return_value = [dbo].[SP_LIST_ENDING_STOCK]" & vbCrLf & _
              "		@PERIOD = N'" & dtpPeriod.Value & "'," & vbCrLf & _
              "		@PRODUCT_NAME = N'" & txtFilter.Text & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.dvgListEndingStock.DataSource = dt

        Me.Cursor = Cursors.Default

        For x As Integer = 0 To dvgListEndingStock.RowCount - 1
            tmpTotal = tmpTotal + dvgListEndingStock.Item("PRICE_PER_UNIT", x).Value
            tmpTotal = tmpTotal
        Next
        If Language = "Indonesian" Then
            lblList.Text = "Total Daftar   " & dvgListEndingStock.RowCount
            lblTotal.Text = "Total Harga  " & FormatNumber(tmpTotal, 0)
        Else
            lblList.Text = "Total List : " & dvgListEndingStock.RowCount
            lblTotal.Text = "Total Price : " & FormatNumber(tmpTotal, 0)
        End If

        txtFilter.Enabled = True
    End Sub

    'Private Sub frmListEndingStock_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
    '    If e.KeyCode = Keys.F12 Then
    '        cmdReport_Click(Nothing, Nothing)
    '    End If
    'End Sub

    Private Sub frmListEndingStock_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Language = "Indonesian" Then
            lblList.Text = "Total Daftar   " & dvgListEndingStock.RowCount
            lblTotal.Text = "Total Harga  " & FormatNumber(tmpTotal, 0)
            Me.Text = "Stok Akhir"
            Label1.Text = "Periode"
            Label2.Text = "Nama Produk"
            cmdGenerate.Text = "Proses"
            cmdReport.Text = "Cetak"
            dvgListEndingStock.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            dvgListEndingStock.Columns("TRANSACTION_DATE").HeaderText = "Tanggal Transaksi"
            dvgListEndingStock.Columns("QUANTITY").HeaderText = "Jumlah"
            dvgListEndingStock.Columns("PRICE_PER_UNIT").HeaderText = "Harga Satuan"
        Else
            lblList.Text = "Total List   " & dvgListEndingStock.RowCount
            lblTotal.Text = "Total Price  " & FormatNumber(tmpTotal, 0)
        End If

        txtFilter.Enabled = False
        tmpTotal = 0
    End Sub

    'Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
    '    Dim RPV As New frmRepEndingStock
    '    RPV.period = dtpPeriod.Value
    '    RPV.filter = txtFilter.Text
    '    RPV.ReportViewer1.ZoomPercent = 100
    '    RPV.WindowState = FormWindowState.Maximized
    '    RPV.Show()
    'End Sub

    Private Sub txtFilter_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFilter.TextChanged
        'SPLISTENDINGSTOCKBindingSource.Filter = "PRODUCT_NAME LIKE '%" & txtFilter.Text.ToUpper & "%'"
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC	@return_value = [dbo].[SP_LIST_ENDING_STOCK]" & vbCrLf & _
              "		@PERIOD = N'" & dtpPeriod.Value & "'," & vbCrLf & _
              "		@PRODUCT_NAME = N'" & txtFilter.Text & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.dvgListEndingStock.DataSource = dt

        If Language = "Indonesian" Then
            lblList.Text = "Total Daftar   " & dvgListEndingStock.RowCount
            lblTotal.Text = "Total Harga  " & FormatNumber(tmpTotal, 0)
        Else
            lblList.Text = "Total List : " & dvgListEndingStock.RowCount
            lblTotal.Text = "Total Price : " & FormatNumber(tmpTotal, 0)
        End If
    End Sub
End Class