﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportPurchaseDetail
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReportPurchaseDetail))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DS_PURCHASE = New DMI_RETAIL_REPORT.DS_PURCHASE()
        Me.SP_PURCHASE_REPORT_DETAILBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_PURCHASE_REPORT_DETAILTableAdapter = New DMI_RETAIL_REPORT.DS_PURCHASETableAdapters.SP_PURCHASE_REPORT_DETAILTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_REPORT.DS_PURCHASETableAdapters.TableAdapterManager()
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.dgvRptPurchaseDetail = New System.Windows.Forms.DataGridView()
        Me.PURCHASE_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RECEIPT_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VENDOR_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PAYMENT_METHOD_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRODUCT_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QUANTITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRICE_PER_UNIT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DISCOUNT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TAX = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblTax = New System.Windows.Forms.Label()
        Me.lblTot = New System.Windows.Forms.Label()
        Me.lblDisc = New System.Windows.Forms.Label()
        Me.lblQty = New System.Windows.Forms.Label()
        Me.lblNoTrans = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.cmdReport = New System.Windows.Forms.Button()
        CType(Me.DS_PURCHASE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_PURCHASE_REPORT_DETAILBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_PURCHASE_REPORT_DETAILBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator.SuspendLayout()
        CType(Me.dgvRptPurchaseDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'DS_PURCHASE
        '
        Me.DS_PURCHASE.DataSetName = "DS_PURCHASE"
        Me.DS_PURCHASE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SP_PURCHASE_REPORT_DETAILBindingSource
        '
        Me.SP_PURCHASE_REPORT_DETAILBindingSource.DataMember = "SP_PURCHASE_REPORT_DETAIL"
        Me.SP_PURCHASE_REPORT_DETAILBindingSource.DataSource = Me.DS_PURCHASE
        '
        'SP_PURCHASE_REPORT_DETAILTableAdapter
        '
        Me.SP_PURCHASE_REPORT_DETAILTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.PRODUCTTableAdapter = Nothing
        Me.TableAdapterManager.PURCHASE_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.PURCHASETableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_REPORT.DS_PURCHASETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.VENDORTableAdapter = Nothing
        Me.TableAdapterManager.WAREHOUSETableAdapter = Nothing
        '
        'SP_PURCHASE_REPORT_DETAILBindingNavigator
        '
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator.AddNewItem = Nothing
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator.BindingSource = Me.SP_PURCHASE_REPORT_DETAILBindingSource
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator.DeleteItem = Nothing
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator.Name = "SP_PURCHASE_REPORT_DETAILBindingNavigator"
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator.Size = New System.Drawing.Size(944, 25)
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator.TabIndex = 0
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'dgvRptPurchaseDetail
        '
        Me.dgvRptPurchaseDetail.AllowUserToAddRows = False
        Me.dgvRptPurchaseDetail.AllowUserToDeleteRows = False
        Me.dgvRptPurchaseDetail.AutoGenerateColumns = False
        Me.dgvRptPurchaseDetail.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRptPurchaseDetail.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvRptPurchaseDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRptPurchaseDetail.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PURCHASE_DATE, Me.RECEIPT_NO, Me.VENDOR_NAME, Me.PAYMENT_METHOD_NAME, Me.PRODUCT_NAME, Me.QUANTITY, Me.PRICE_PER_UNIT, Me.DISCOUNT, Me.TAX, Me.TOTAL, Me.DataGridViewTextBoxColumn11})
        Me.dgvRptPurchaseDetail.DataSource = Me.SP_PURCHASE_REPORT_DETAILBindingSource
        Me.dgvRptPurchaseDetail.Location = New System.Drawing.Point(6, 12)
        Me.dgvRptPurchaseDetail.Name = "dgvRptPurchaseDetail"
        Me.dgvRptPurchaseDetail.ReadOnly = True
        Me.dgvRptPurchaseDetail.Size = New System.Drawing.Size(907, 228)
        Me.dgvRptPurchaseDetail.TabIndex = 2
        '
        'PURCHASE_DATE
        '
        Me.PURCHASE_DATE.DataPropertyName = "PURCHASE_DATE"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "dd-MMM-yyyy"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.PURCHASE_DATE.DefaultCellStyle = DataGridViewCellStyle2
        Me.PURCHASE_DATE.HeaderText = "Purchase Date"
        Me.PURCHASE_DATE.Name = "PURCHASE_DATE"
        Me.PURCHASE_DATE.ReadOnly = True
        '
        'RECEIPT_NO
        '
        Me.RECEIPT_NO.DataPropertyName = "RECEIPT_NO"
        Me.RECEIPT_NO.HeaderText = "Receipt No"
        Me.RECEIPT_NO.Name = "RECEIPT_NO"
        Me.RECEIPT_NO.ReadOnly = True
        '
        'VENDOR_NAME
        '
        Me.VENDOR_NAME.DataPropertyName = "VENDOR_NAME"
        Me.VENDOR_NAME.HeaderText = "Vendor"
        Me.VENDOR_NAME.Name = "VENDOR_NAME"
        Me.VENDOR_NAME.ReadOnly = True
        '
        'PAYMENT_METHOD_NAME
        '
        Me.PAYMENT_METHOD_NAME.DataPropertyName = "PAYMENT_METHOD_NAME"
        Me.PAYMENT_METHOD_NAME.HeaderText = "Payment Method"
        Me.PAYMENT_METHOD_NAME.Name = "PAYMENT_METHOD_NAME"
        Me.PAYMENT_METHOD_NAME.ReadOnly = True
        Me.PAYMENT_METHOD_NAME.Width = 75
        '
        'PRODUCT_NAME
        '
        Me.PRODUCT_NAME.DataPropertyName = "PRODUCT_NAME"
        Me.PRODUCT_NAME.HeaderText = "Product"
        Me.PRODUCT_NAME.Name = "PRODUCT_NAME"
        Me.PRODUCT_NAME.ReadOnly = True
        '
        'QUANTITY
        '
        Me.QUANTITY.DataPropertyName = "QUANTITY"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.QUANTITY.DefaultCellStyle = DataGridViewCellStyle3
        Me.QUANTITY.HeaderText = "Qty"
        Me.QUANTITY.Name = "QUANTITY"
        Me.QUANTITY.ReadOnly = True
        Me.QUANTITY.Width = 50
        '
        'PRICE_PER_UNIT
        '
        Me.PRICE_PER_UNIT.DataPropertyName = "PRICE_PER_UNIT"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.PRICE_PER_UNIT.DefaultCellStyle = DataGridViewCellStyle4
        Me.PRICE_PER_UNIT.HeaderText = "Price"
        Me.PRICE_PER_UNIT.Name = "PRICE_PER_UNIT"
        Me.PRICE_PER_UNIT.ReadOnly = True
        Me.PRICE_PER_UNIT.Width = 75
        '
        'DISCOUNT
        '
        Me.DISCOUNT.DataPropertyName = "DISCOUNT"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N0"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.DISCOUNT.DefaultCellStyle = DataGridViewCellStyle5
        Me.DISCOUNT.HeaderText = "Disc."
        Me.DISCOUNT.Name = "DISCOUNT"
        Me.DISCOUNT.ReadOnly = True
        Me.DISCOUNT.Width = 75
        '
        'TAX
        '
        Me.TAX.DataPropertyName = "TAX"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.TAX.DefaultCellStyle = DataGridViewCellStyle6
        Me.TAX.HeaderText = "Tax"
        Me.TAX.Name = "TAX"
        Me.TAX.ReadOnly = True
        Me.TAX.Width = 50
        '
        'TOTAL
        '
        Me.TOTAL.DataPropertyName = "TOTAL"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N0"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.TOTAL.DefaultCellStyle = DataGridViewCellStyle7
        Me.TOTAL.HeaderText = "Total"
        Me.TOTAL.Name = "TOTAL"
        Me.TOTAL.ReadOnly = True
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "PERIOD"
        Me.DataGridViewTextBoxColumn11.HeaderText = "PERIOD"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvRptPurchaseDetail)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 85)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(919, 246)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblTax)
        Me.GroupBox2.Controls.Add(Me.lblTot)
        Me.GroupBox2.Controls.Add(Me.lblDisc)
        Me.GroupBox2.Controls.Add(Me.lblQty)
        Me.GroupBox2.Controls.Add(Me.lblNoTrans)
        Me.GroupBox2.Location = New System.Drawing.Point(13, 337)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(919, 71)
        Me.GroupBox2.TabIndex = 10
        Me.GroupBox2.TabStop = False
        '
        'lblTax
        '
        Me.lblTax.AutoSize = True
        Me.lblTax.Location = New System.Drawing.Point(406, 44)
        Me.lblTax.Name = "lblTax"
        Me.lblTax.Size = New System.Drawing.Size(45, 15)
        Me.lblTax.TabIndex = 8
        Me.lblTax.Text = "Label5"
        '
        'lblTot
        '
        Me.lblTot.AutoSize = True
        Me.lblTot.Location = New System.Drawing.Point(706, 15)
        Me.lblTot.Name = "lblTot"
        Me.lblTot.Size = New System.Drawing.Size(45, 15)
        Me.lblTot.TabIndex = 7
        Me.lblTot.Text = "Label4"
        '
        'lblDisc
        '
        Me.lblDisc.AutoSize = True
        Me.lblDisc.Location = New System.Drawing.Point(406, 15)
        Me.lblDisc.Name = "lblDisc"
        Me.lblDisc.Size = New System.Drawing.Size(45, 15)
        Me.lblDisc.TabIndex = 6
        Me.lblDisc.Text = "Label3"
        '
        'lblQty
        '
        Me.lblQty.AutoSize = True
        Me.lblQty.Location = New System.Drawing.Point(18, 44)
        Me.lblQty.Name = "lblQty"
        Me.lblQty.Size = New System.Drawing.Size(45, 15)
        Me.lblQty.TabIndex = 5
        Me.lblQty.Text = "Label2"
        '
        'lblNoTrans
        '
        Me.lblNoTrans.AutoSize = True
        Me.lblNoTrans.Location = New System.Drawing.Point(18, 15)
        Me.lblNoTrans.Name = "lblNoTrans"
        Me.lblNoTrans.Size = New System.Drawing.Size(45, 15)
        Me.lblNoTrans.TabIndex = 3
        Me.lblNoTrans.Text = "Label1"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cmdReport)
        Me.GroupBox3.Location = New System.Drawing.Point(14, 32)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(918, 53)
        Me.GroupBox3.TabIndex = 11
        Me.GroupBox3.TabStop = False
        '
        'cmdReport
        '
        Me.cmdReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdReport.Image = Global.DMI_RETAIL_REPORT.My.Resources.Resources.Printer_small
        Me.cmdReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdReport.Location = New System.Drawing.Point(804, 17)
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(90, 24)
        Me.cmdReport.TabIndex = 14
        Me.cmdReport.Text = "Print [F12]"
        Me.cmdReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.cmdReport.UseVisualStyleBackColor = True
        '
        'frmReportPurchaseDetail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(944, 417)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.SP_PURCHASE_REPORT_DETAILBindingNavigator)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmReportPurchaseDetail"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Report Purchase Detail"
        CType(Me.DS_PURCHASE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_PURCHASE_REPORT_DETAILBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_PURCHASE_REPORT_DETAILBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator.ResumeLayout(False)
        Me.SP_PURCHASE_REPORT_DETAILBindingNavigator.PerformLayout()
        CType(Me.dgvRptPurchaseDetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DS_PURCHASE As DMI_RETAIL_REPORT.DS_PURCHASE
    Friend WithEvents SP_PURCHASE_REPORT_DETAILBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_PURCHASE_REPORT_DETAILTableAdapter As DMI_RETAIL_REPORT.DS_PURCHASETableAdapters.SP_PURCHASE_REPORT_DETAILTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_REPORT.DS_PURCHASETableAdapters.TableAdapterManager
    Friend WithEvents SP_PURCHASE_REPORT_DETAILBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents dgvRptPurchaseDetail As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblTot As System.Windows.Forms.Label
    Friend WithEvents lblDisc As System.Windows.Forms.Label
    Friend WithEvents lblQty As System.Windows.Forms.Label
    Friend WithEvents lblNoTrans As System.Windows.Forms.Label
    Friend WithEvents PURCHASE_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RECEIPT_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VENDOR_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PAYMENT_METHOD_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRODUCT_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QUANTITY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRICE_PER_UNIT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DISCOUNT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TAX As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblTax As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdReport As System.Windows.Forms.Button
End Class
