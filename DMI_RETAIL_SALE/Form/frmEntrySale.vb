﻿Imports System.Data.SqlClient

Public Class frmEntrySale
    Dim tmpDataGridViewLoaded, tmpFormClosing As Boolean
    Dim tmpProductID, tmpNoOfReceiptNo As Integer
    Public tmpCustomer, tmpReceiptNo, tmpSaveMode, tmpSaleId, tmpSaleOrderNo As String
    Dim tmpInput As String
    Dim tmpChange, tmpSplit As Boolean
    Dim tmpCategory, tmptotalpayment, tmpReceivableID As Integer
    Dim tmpstatus, tmpReceivablePaymentNo As String
    Dim tmpParent, tmpQtyGrouping, tmpQtyProduct As Integer
    Dim tmpOrder As String
    Public tmpTotItem, tmpSaleOrderID, tmpWarehouse As Integer
    Public tmpSaleOrderDate As DateTime

    Public xCon As New SqlConnection
    Public sqladapter As New SqlDataAdapter
    Public DT As New DataTable
    Public sql As String
    Public ds As New BindingSource
    Public xCommand As New SqlCommand
    Public xdreader As SqlDataReader
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    Private Sub frmEntrySale_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If tmpChange = True And REMARKTextBox.Enabled = True Then
            tmpFormClosing = True
            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah anda ingin menyimpan perubahan " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do you want to save the changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If
            If tmpvar = 6 Then
                cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        Else
            Me.Dispose()
        End If
    End Sub
    'Sub connection()
    '    Conn.Close()
    '    Conn.ConnectionString = "Data Source="& ServerName &";Initial Catalog=" & DatabaseName & ";Persist Security Info=True;User ID=sa;Password=SA;"
    '    '"Provider=SQLOLEDB;Data Source=" & mdlGeneral.ServerName & "\sqlexpress;Initial Catalog=" & DatabaseName & ";" & _
    '    '"Trusted_Connection=Yes;Connect Timeout=30"
    '    Conn.Open()
    'End Sub

    Public Sub Get_Price(ByVal CODE As String, ByVal QTY As Integer, ByVal Price As Integer)
        connection2()
        Price = 0
        'Conn.ConnectionString = "Data Source="& ServerName &";Initial Catalog=" & DatabaseName & ";Persist Security Info=True;User ID=sa;Password=SA;"
        sql = "DECLARE	@return_value int EXEC	@return_value = [dbo].[SP_GET_SPECIAL_PRICE] "
        sql &= "@PRODUCT_CODE ='" & CODE & "', "
        sql &= "@QTY = '" & QTY & "' "
        sql &= "SELECT	'Return Value' = @return_value"
        xdreader = New SqlCommand(sql, Xconn2).ExecuteReader
        If xdreader.Read = True Then
            Price = xdreader.Item(0)
            dgvSaleDetail.CurrentRow.Cells("PRICE_PER_UNIT").Value = xdreader.Item(0)
        End If
    End Sub

    Public Sub Get_Price_Normal(ByVal CODE As String, ByVal QTY As Integer, ByVal Price As Integer)
        connection2()
        Price = 0
        'Conn.ConnectionString = "Data Source="& ServerName &";Initial Catalog=" & DatabaseName & ";Persist Security Info=True;User ID=sa;Password=SA;"
        sql = "DECLARE	@return_value int EXEC	@return_value = [dbo].[SP_GET_NORMAL_PRICE] "
        sql &= "@PRODUCT_CODE ='" & CODE & "', "
        sql &= "@QTY = '" & QTY & "' "
        sql &= "SELECT	'Return Value' = @return_value"
        xdreader = New SqlCommand(sql, Xconn2).ExecuteReader
        If xdreader.Read = True Then
            Price = xdreader.Item(0)
            dgvSaleDetail.CurrentRow.Cells("PRICE_PER_UNIT").Value = xdreader.Item(0)
        End If
    End Sub

    Public Function CekQTY(ByVal QTY As Integer, ByVal CODE As String) As Boolean
        connection2()
        'Conn.ConnectionString = "Data Source="& ServerName &";Initial Catalog=" & DatabaseName & ";Persist Security Info=True;User ID=sa;Password=SA;"
        sql = "DECLARE	@return_value int EXEC	@return_value = [dbo].[SP_GET_QTY_SPECIAL] "
        sql &= "@QTY ='" & QTY & "', "
        sql &= "@PRODUCT_CODE = '" & CODE & "' "
        xdreader = New SqlCommand(sql, Xconn2).ExecuteReader
        xdreader.Read()
        If xdreader.HasRows Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function CEKQTYKOSONG(ByVal CODE As String) As Boolean
        connection2()
        'Conn.ConnectionString = "Data Source="& ServerName &";Initial Catalog=" & DatabaseName & ";Persist Security Info=True;User ID=sa;Password=SA;"
        sql = "DECLARE	@return_value int EXEC	@return_value = [dbo].[SP_CHECK_QTY_NOL] "
        sql &= "@PRODUCT_CODE = '" & CODE & "' "
        xdreader = New SqlCommand(sql, Xconn2).ExecuteReader
        xdreader.Read()
        If xdreader.HasRows Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub GETSPECIALPRICE(ByVal CODE As String)
        connection2()
        'Conn.ConnectionString = "Data Source="& ServerName &";Initial Catalog=" & DatabaseName & ";Persist Security Info=True;User ID=sa;Password=SA;"
        sql = "DECLARE	@return_value int EXEC	@return_value = [dbo].[SP_PRICE_SPECIAL] "
        sql &= "@PRODUCT_CODE ='" & CODE & "' "
        sql &= "SELECT	'Return Value' = @return_value"
        xdreader = New SqlCommand(sql, Xconn2).ExecuteReader
        If xdreader.Read = True Then
            dgvSaleDetail.CurrentRow.Cells("PRICE_PER_UNIT").Value = xdreader.Item(0)
        End If
    End Sub

    Public Sub GET_PRICE_NORMAL_NULLQTY(ByVal CODE As String)
        connection2()
        'Conn.ConnectionString = "Data Source="& ServerName &";Initial Catalog=" & DatabaseName & ";Persist Security Info=True;User ID=sa;Password=SA;"
        sql = "DECLARE	@return_value int EXEC	@return_value = [dbo].[SP_NORMAL_PRICE_NULLQTY] "
        sql &= "@PRODUCT_CODE ='" & CODE & "' "
        sql &= "SELECT	'Return Value' = @return_value"
        xdreader = New SqlCommand(sql, Xconn2).ExecuteReader
        If xdreader.Read = True Then
            dgvSaleDetail.CurrentRow.Cells("PRICE_PER_UNIT").Value = xdreader.Item(0)
        End If
    End Sub

    Public Function CekQTY_Null(ByVal CODE As String) As Boolean
        connection2()
        'Conn.ConnectionString = "Data Source="& ServerName &";Initial Catalog=" & DatabaseName & ";Persist Security Info=True;User ID=sa;Password=SA;"
        sql = "DECLARE	@return_value int EXEC	@return_value = [dbo].[SP_GET_QTY_NOL] "
        sql &= "@PRODUCT_CODE = '" & CODE & "' "
        xdreader = New SqlCommand(sql, Xconn2).ExecuteReader
        xdreader.Read()
        If xdreader.HasRows Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub frmEntrySale_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If tmpSaveMode = "Update" Then
            If e.KeyCode = Keys.F1 Then Exit Sub
            If e.KeyCode = Keys.F2 Then Exit Sub
            If e.KeyCode = Keys.F3 Then Exit Sub
            If e.KeyCode = Keys.F5 Then Exit Sub
            If e.KeyCode = Keys.F6 Then Exit Sub
            If e.KeyCode = Keys.F7 Then Exit Sub
            If e.KeyCode = Keys.F10 Then Exit Sub
        Else

            If e.KeyCode = Keys.F1 Then
                REMARKTextBox.Focus()
            ElseIf e.KeyCode = Keys.F2 Then
                TAXPercentageTEXTBOX.Focus()
            ElseIf e.KeyCode = Keys.F3 Then
                DOWN_PAYMENTTextBox.Focus()
            ElseIf e.KeyCode = Keys.F5 Then

                CUSTOMER_IDComboBox.Enabled = False
                tmpInput = "Browse"
                dgvSaleDetail.CancelEdit()
                mdlGeneral.tmpSearchMode = "SALE - Product Code"
                Dim frmSearchProduct As New frmSearchProduct
                frmSearchProduct.ShowDialog(Me)

                If tmpSearchResult = "" Then
                    tmpInput = ""
                    Exit Sub
                End If

                If txtSaleOrderNo.Text <> "" Then
                    dgvSaleDetail.Rows.Clear()
                    txtSaleOrderNo.Text = ""
                    dtpSaleOrderDate.Value = Now
                End If

                PRODUCTBindingSource.Filter = "PRODUCT_ID = " & tmpSearchResult
                dgvSaleDetail.Rows.Insert(dgvSaleDetail.RowCount - 1)
                dgvSaleDetail.CurrentCell = dgvSaleDetail.Rows(dgvSaleDetail.RowCount - 1).Cells(0)
                dgvSaleDetail.Item(0, dgvSaleDetail.RowCount - 1).Value = _
                    PRODUCTBindingSource.Current("PRODUCT_CODE")
                dgvSaleDetail.Item(0, dgvSaleDetail.RowCount - 1).Value = ""
            ElseIf e.KeyCode = Keys.F6 Then
                CUSTOMER_IDComboBox.Enabled = False
                tmpInput = "Browse"
                dgvSaleDetail.CancelEdit()
                mdlGeneral.tmpSearchMode = "SALE - Product Name"
                Dim frmSearchProduct As New frmSearchProduct
                frmSearchProduct.ShowDialog(Me)
                If tmpSearchResult = "" Then
                    tmpInput = ""
                    Exit Sub
                End If

                If txtSaleOrderNo.Text <> "" Then
                    dgvSaleDetail.Rows.Clear()
                    txtSaleOrderNo.Text = ""
                    dtpSaleOrderDate.Value = Now
                End If

                PRODUCTBindingSource.Filter = "PRODUCT_ID = " & tmpSearchResult
                dgvSaleDetail.Rows.Insert(dgvSaleDetail.RowCount - 1)
                dgvSaleDetail.CurrentCell = dgvSaleDetail.Rows(dgvSaleDetail.RowCount - 1).Cells(0)
                dgvSaleDetail.Item(0, dgvSaleDetail.RowCount - 1).Value = _
                    PRODUCTBindingSource.Current("PRODUCT_CODE")
                dgvSaleDetail.Item(0, dgvSaleDetail.RowCount - 1).Value = ""
            ElseIf e.KeyCode = Keys.F7 Then
                If cmdAdd.Enabled = False Then Exit Sub
                cmdAdd_Click(Nothing, Nothing)
            ElseIf e.KeyCode = Keys.F10 Then
                If cmdSave.Enabled = False Then Exit Sub
                cmdSave_Click(Nothing, Nothing)
            End If

        End If

        If e.KeyCode = Keys.F12 Then
            If cmdPrint.Enabled = False Then Exit Sub
            cmdPrint_Click(Nothing, Nothing)
        ElseIf e.KeyCode = Keys.F9 Then
            If cmdUndo.Enabled = False Then Exit Sub
            cmdUndo_Click(Nothing, Nothing)
        ElseIf e.KeyCode = Keys.F11 Then
            If cmdDelete.Enabled = False Then Exit Sub
            cmdDelete_Click(Nothing, Nothing)
        End If
    End Sub
    Private Function checkLockPrice() As Boolean
        Dim tmpBool As Boolean = False
        connection()
        RsAccessPrivilege = New SqlCommand("SELECT [VALUE] FROM PARAMETER WHERE [DESCRIPTION]='SALE_COLUMN_PRICE'", xConn).ExecuteReader
        RsAccessPrivilege.Read()
        If RsAccessPrivilege.HasRows Then
            If RsAccessPrivilege.Item(0) = "True" Then
                tmpBool = True
            End If
        End If
        Return tmpBool
    End Function
    Private Sub frmEntrySale_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_SALE.SALESMAN' table. You can move, or remove it, as needed.
        Me.SALESMANTableAdapter.Fill(Me.DS_SALE.SALESMAN)
        'TODO: This line of code loads data into the 'DS_SALE1.WAREHOUSE' table. You can move, or remove it, as needed.
        Me.WAREHOUSETableAdapter.Fill(Me.DS_SALE1.WAREHOUSE)
        'TODO: This line of code loads data into the 'DS_SALE.WAREHOUSE' table. You can move, or remove it, as needed.
        Me.WAREHOUSETableAdapter.Fill(Me.DS_SALE.WAREHOUSE)
        'TODO: This line of code loads data into the 'DS_SALE.PAYMENT_METHOD' table. You can move, or remove it, as needed.
        Me.PAYMENT_METHODTableAdapter.Fill(Me.DS_SALE.PAYMENT_METHOD)
        'TODO: This line of code loads data into the 'DS_PRODUCT_WAREHOUSE.PRODUCT_WAREHOUSE' table. You can move, or remove it, as needed.
        Me.PRODUCT_WAREHOUSETableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.PRODUCT_WAREHOUSE)
        'TODO: This line of code loads data into the 'DS_SALE.VIEW_LIST_SALE_DETAIL' table. You can move, or remove it, as needed.
        Me.VIEW_LIST_SALE_DETAILTableAdapter.Fill(Me.DS_SALE.VIEW_LIST_SALE_DETAIL)
        'TODO: This line of code loads data into the 'DS_SALE.VIEW_LIST_SALE' table. You can move, or remove it, as needed.
        Me.VIEW_LIST_SALETableAdapter.Fill(Me.DS_SALE.VIEW_LIST_SALE)
        'TODO: This line of code loads data into the 'DS_SALE.PRODUCT' table. You can move, or remove it, as needed.
        Me.PRODUCTTableAdapter.Fill(Me.DS_SALE.PRODUCT)
        'TODO: This line of code loads data into the 'DS_SALE.CUSTOMER' table. You can move, or remove it, as needed.
        Me.CUSTOMERTableAdapter.Fill(Me.DS_SALE.CUSTOMER)

        accFormName = Me.Text
        Call AccessPrivilege()

        Dim tmpGridViewTotal As Double = 0
        tmpFormClosing = False

        ''''Get discount Customer''''
        If RECEIPT_NOTextBox.Text <> "" Then
            Dim tmpCustomer_id As Integer = CUSTOMERBindingSource.Current("CUSTOMER_ID")
            If IsDBNull(CUSTOMERBindingSource.Current("DISCOUNT")) Then
                tmpDiscount = 0
            Else
                SALETableAdapter.SP_GET_DISCOUNT_CUSTOMER(tmpCustomer_id, tmpDiscount)
            End If
        Else
            tmpDiscount = 0
        End If

        For y As Integer = 0 To dgvSaleDetail.RowCount - 2
            dgvSaleDetail.Item("DISCOUNT", y).Value = tmpDiscount
            dgvSaleDetail.Item("DISCOUNT2", y).Value = dgvSaleDetail.Item("PRICE_PER_UNIT", y).Value * tmpDiscount / 100
        Next

        ''''validate language''''
        If Language = "Indonesian" Then
            lblReceipNo.Text = "No Faktur"
            lblSaleDate.Text = "Tanggal Jual"
            lblCustomer.Text = "Pelanggan"
            lblMethod.Text = "Cara Pembayaran"
            lblWarehouse.Text = "Gudang"
            lblMaturity.Text = "Jatuh tempo"
            lblremark.Text = "Keterangan" & vbCrLf & "[F1]"
            lblTax.Text = "Pajak [F2]"
            lblPayment.Text = "Pembayaran [F3]"

            lblF5.Text = "F5 : Cari Produk berdasarkan Kode"
            lblF6.Text = "F6 : Cari Produk berdasarkan Nama"
            lblDel.Text = "Del : Hapus Produk dari daftar"
            cmdAdd.Text = "Tambah" & vbCrLf & "[F7]"
            cmdEdit.Text = "Ubah" & vbCrLf & "[F8]"
            cmdUndo.Text = "Batal" & vbCrLf & "[F9]"
            cmdSave.Text = "Simpan" & vbCrLf & "[F10]"
            cmdDelete.Text = "Hapus" & vbCrLf & "[F11]"
            cmdPrint.Text = "Simpan " & "&" & vbCrLf & "Cetak" & vbCrLf & "[F12]"
            lblNoOfItem.Text = "Jumlah Item :"
            dgvSaleDetail.Columns("PRODUCT_CODE").HeaderText = "Kode Produk"
            dgvSaleDetail.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            dgvSaleDetail.Columns("QUANTITY").HeaderText = "Jumlah"
            dgvSaleDetail.Columns("PRICE_PER_UNIT").HeaderText = "Harga per Unit"
            dgvSaleDetail.Columns("DELETE").HeaderText = "Hapus"
            Me.Text = "Input Penjualan"
        End If
        If checkLockPrice() = True Then
            dgvSaleDetail.Columns("PRICE_PER_UNIT").ReadOnly = True
        Else
            dgvSaleDetail.Columns("PRICE_PER_UNIT").ReadOnly = False
        End If
        ''''update from list''''
        If tmpSaveMode = "Update" Then

            cmdSearchSaleOrder.Enabled = False

            If Language = "Indonesian" Then
                cmdPrint.Text = "Cetak" & vbCrLf & "[F12]"
            Else
                cmdPrint.Text = "Print" & vbCrLf & "[F12]"
            End If

            dgvSaleDetail.Rows.Clear()
            dgvSaleDetail.RowCount = 1
            VIEW_LIST_SALE_DETAILBindingSource.Filter = "SALE_ID = " & tmpSaleId
            RECEIPT_NOTextBox.Text = tmpReceiptNo

            If tmpCustomer = 0 Then
                CUSTOMER_IDComboBox.Text = ""
            Else
                CUSTOMERBindingSource.Position = CUSTOMERBindingSource.Find("CUSTOMER_ID", tmpCustomer)
            End If

            If Not IsDBNull(tmpSaleOrderID) Then
                txtSaleOrderNo.Text = tmpSaleOrderNo
                dtpSaleOrderDate.Value = tmpSaleOrderDate
            End If

            If tmpWarehouse = 0 Then
                WAREHOUSE_IDComboBox.Text = ""
            Else
                WAREHOUSEBindingSource.Position = WAREHOUSEBindingSource.Find("WAREHOUSE_ID", tmpWarehouse)
            End If

            dgvSaleDetail.Rows.Add(VIEW_LIST_SALE_DETAILBindingSource.Count)
            AMOUNT_DISCOUNTTextBox.Text = "0"
            For X As Integer = 0 To (VIEW_LIST_SALE_DETAILBindingSource.Count) - 1
                VIEW_LIST_SALE_DETAILBindingSource.Position = X

                dgvSaleDetail.Item(0, X).Value = VIEW_LIST_SALE_DETAILBindingSource.Current("PRODUCT_CODE")
                dgvSaleDetail.Item(1, X).Value = VIEW_LIST_SALE_DETAILBindingSource.Current("PRODUCT_NAME")
                dgvSaleDetail.Item(2, X).Value = VIEW_LIST_SALE_DETAILBindingSource.Current("QUANTITY")
                dgvSaleDetail.Item(3, X).Value = VIEW_LIST_SALE_DETAILBindingSource.Current("PRICE_PER_UNIT")
                dgvSaleDetail.Item(5, X).Value = VIEW_LIST_SALE_DETAILBindingSource.Current("DISCOUNT")
                dgvSaleDetail.Item(6, X).Value = VIEW_LIST_SALE_DETAILBindingSource.Current("TOTAL")
                dgvSaleDetail.Rows(X).ReadOnly = True
                dgvSaleDetail.Columns(7).Visible = False

                dgvSaleDetail.Item(4, X).Value = _
                       FormatNumber(CInt(dgvSaleDetail.Item(5, X).Value) / _
                       (CInt(dgvSaleDetail.Item(2, X).Value) * _
                       CInt(dgvSaleDetail.Item(3, X).Value)) * 100, 2)
                AMOUNT_DISCOUNTTextBox.Text = AMOUNT_DISCOUNTTextBox.Text + VIEW_LIST_SALE_DETAILBindingSource.Current("DISCOUNT")
                tmpGridViewTotal = tmpGridViewTotal + VIEW_LIST_SALE_DETAILBindingSource.Current("TOTAL")
            Next

            dgvSaleDetail.AllowUserToAddRows = False
            dgvSaleDetail.AllowUserToDeleteRows = False
            dgvSaleDetail.AllowUserToOrderColumns = False
            lblNoOfItem.Text = "No of Item(s) : " & dgvSaleDetail.RowCount
            TAXPercentageTEXTBOX.Text = FormatNumber(TAX_AMOUNTTextBox.Text / (GRAND_TOTALTextBox.Text - TAX_AMOUNTTextBox.Text) * 100)
            txtDiscount2.Text = FormatNumber((txtDisc2.Text / tmpGridViewTotal) * 100, 2)
            txtDiscount3.Text = FormatNumber((txtDisc3.Text / (tmpGridViewTotal - txtDisc2.Text)) * 100, 2)
            lblChange.Text = FormatNumber(IIf(DOWN_PAYMENTTextBox.Text.Length = 0, 0, DOWN_PAYMENTTextBox.Text) - _
                             GRAND_TOTALTextBox.Text, 0)
            DisableInputBox(Me)

        Else
            cmbSALESMAN_NAME.SelectedValue = -1
            SALETableAdapter.SP_GENERATE_SALE_RECEIPT_NO(RECEIPT_NOTextBox.Text)
            'CUSTOMERBindingSource.Position = -1


            tmpDataGridViewLoaded = True

            cmdAdd.Enabled = False
            cmdEdit.Enabled = False
            cmdUndo.Enabled = True
            cmdSave.Enabled = True
            cmdDelete.Enabled = False

            txtDTP.Text = Format(SALE_DATEDateTimePicker.Value, "dd-MMM-yyyy")

            ''Default Parameter Customer
            If SALETableAdapter.SP_SELECT_PARAMETER("SALE CUSTOMER") = "0" Then
                CUSTOMER_IDComboBox.SelectedValue = 0
            Else
                CUSTOMERBindingSource.Position = _
                    CUSTOMERBindingSource.Find("CUSTOMER_ID", _
                                               SALETableAdapter.SP_SELECT_PARAMETER("SALE CUSTOMER"))
                CUSTOMER_IDComboBox.Text = CUSTOMERBindingSource.Current("CUSTOMER_NAME")
            End If

            If PAYMENT_METHODBindingSource Is Nothing Or PAYMENT_METHODBindingSource.Count < 1 Then
                If Language = "Indonesian" Then
                    MsgBox("Tidak ada data Metode Pembayaran." & vbCrLf & "Masukkan setidaknya 1 (satu) Metode Pembayaran!", _
                         MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("There is no Payment Method data." & vbCrLf & "Please enter at least one Payment Method data!", _
                         MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            Else
                ''Default Parameter Payment Method
                If SALETableAdapter.SP_SELECT_PARAMETER("SALE PAYMENT METHOD") = "0" Then
                    PAYMENT_METHOD_IDComboBox.SelectedIndex = -1
                Else
                    PAYMENT_METHODBindingSource.Position = _
                        PAYMENT_METHODBindingSource.Find("PAYMENT_METHOD_ID", _
                                                         SALETableAdapter.SP_SELECT_PARAMETER("SALE PAYMENT METHOD"))
                End If
            End If

            ''Default Parameter Warehouse
            If SALETableAdapter.SP_SELECT_PARAMETER("SALE WAREHOUSE") = "0" Then
                WAREHOUSE_IDComboBox.SelectedIndex = -1
            Else
                WAREHOUSEBindingSource.Position = _
                    WAREHOUSEBindingSource.Find("WAREHOUSE_ID", _
                                                SALETableAdapter.SP_SELECT_PARAMETER("SALE WAREHOUSE"))
            End If

            tmpChange = False
        End If

    End Sub

    Private Sub dgvSaleDetail_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvSaleDetail.CellContentClick
        If dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value = "" Then Exit Sub

        If dgvSaleDetail.CurrentCell.ColumnIndex = 7 Then
            dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex)
            CalculationAll(dgvSaleDetail, _
                                     5, _
                                     6, _
                                     AMOUNT_DISCOUNTTextBox, _
                                     TAXPercentageTEXTBOX, _
                                     TAX_AMOUNTTextBox, _
                                     DOWN_PAYMENTTextBox, _
                                     GRAND_TOTALTextBox)
            CalculationAllSale(dgvSaleDetail, _
                  5, _
                  6, _
                  AMOUNT_DISCOUNTTextBox, _
                  TAXPercentageTEXTBOX, _
                  TAX_AMOUNTTextBox, _
                  DOWN_PAYMENTTextBox, _
                  txtDiscount2, _
                  txtDiscount3, _
                  txtDisc2, _
                  txtDisc3, _
                  GRAND_TOTALTextBox)
            lblNoOfItem.Text = "No of Item(s) : " & dgvSaleDetail.RowCount - 1


        End If

        If dgvSaleDetail.Item("PRODUCT_CODE", 0).Value = "" Then
            TAXPercentageTEXTBOX.Text = "0"
            TAX_AMOUNTTextBox.Text = "0"
        End If
    End Sub

    Private Sub dgvSaleDetail_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvSaleDetail.CellValueChanged
        Dim tmpWarehouseID As Integer

        If tmpSaveMode = "Update" Then Exit Sub

        If tmpDataGridViewLoaded Then


            If Not ValidateComboBox(WAREHOUSE_IDComboBox) Then
                If Language = "Indonesian" Then
                    MsgBox("Gudang yang anda masukkan tidak terdapat dalam daftar.", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("You input Warehouse which is not in the list.", MsgBoxStyle.Critical, "DMI Retail")
                End If
                WAREHOUSE_IDComboBox.Text = ""
            End If


            If dgvSaleDetail.CurrentCell.ColumnIndex = 2 Or dgvSaleDetail.CurrentCell.ColumnIndex = 3 Or dgvSaleDetail.CurrentCell.ColumnIndex = 4 Or dgvSaleDetail.CurrentCell.ColumnIndex = 5 Then
                If Not IsNumeric(dgvSaleDetail.CurrentCell.Value) Then
                    If Language = "Indonesian" Then
                        MsgBox("Anda memasukan nilai yang salah !" & vbCrLf & _
                          "Untuk Kolom : " & _
                           dgvSaleDetail.Columns(dgvSaleDetail.CurrentCell.ColumnIndex).HeaderText & vbCrLf & _
                           "Baris Nomor : " & _
                           dgvSaleDetail.CurrentCell.RowIndex + 1, MsgBoxStyle.Critical, "DMI Retail")
                        'give value "0" to empty fields or unexpected fields
                        dgvSaleDetail.CurrentCell.Value = 0
                    Else
                        MsgBox("You entered unexpected value !" & vbCrLf & _
                          "For Column : " & _
                           dgvSaleDetail.Columns(dgvSaleDetail.CurrentCell.ColumnIndex).HeaderText & vbCrLf & _
                           "Row number : " & _
                           dgvSaleDetail.CurrentCell.RowIndex + 1, MsgBoxStyle.Critical, "DMI Retail")
                        'give value "0" to empty fields or unexpected fields
                        dgvSaleDetail.CurrentCell.Value = 0
                    End If
                    Exit Sub
                End If
            End If


            If CUSTOMER_IDComboBox.Text <> "" Then
                If IsDBNull(CUSTOMERBindingSource.Current("DISCOUNT")) Then
                    tmpDiscount = 0
                Else
                    SALETableAdapter.SP_GET_DISCOUNT_CUSTOMER(CUSTOMERBindingSource.Current("CUSTOMER_ID"), tmpDiscount)
                End If

            End If


            If tmpFillingDGV = False Then
                If dgvSaleDetail.CurrentCell.ColumnIndex = 0 Then
                    Dim tmpProductName As String = ""
                    Dim tmpUnitPrice As Integer = 0
                    Dim tmpProfitPercent As Integer = 0
                    Dim tmpProductIdPercent, tmpTotalPrice As Integer

                    PRODUCTTableAdapter.SP_GET_PRODUCT_NAME(dgvSaleDetail.CurrentCell.Value, tmpProductName)


                    PRODUCTTableAdapter.SP_GET_SELLING_PRICE(dgvSaleDetail.CurrentCell.Value, tmpUnitPrice)



                    Try
                        PRODUCTTableAdapter.SP_GET_PRODUCT_ID(dgvSaleDetail.CurrentCell.Value, tmpProductIdPercent)
                    Catch
                    End Try


                    '''''VALIDATION FOR GET TOTAL PRICE'''''
                    tmpTotalPrice = ((tmpProfitPercent / 100) * tmpUnitPrice) + tmpUnitPrice

                    If tmpProductName = "" Then
                        If dgvSaleDetail.CurrentCell.Value <> "" Then
                            If Language = "Indonesian" Then
                                MsgBox("Produk tidak ditemukan!", MsgBoxStyle.Critical, "DMI Retail")
                            Else
                                MsgBox("Product not found!", MsgBoxStyle.Critical, "DMI Retail")
                            End If
                            dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex)
                        End If
                        Exit Sub
                    End If

                    PRODUCTTableAdapter.SP_GET_PRODUCT_ID(dgvSaleDetail.CurrentCell.Value, tmpProductID)
                    SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.SP_PRODUCT_WAREHOUSE_DETAIL, _
                                                             tmpProductID)
                    If WAREHOUSEBindingSource Is Nothing Or WAREHOUSEBindingSource.Count < 1 Then
                        tmpWarehouseID = 0
                    Else
                        If Not ValidateComboBox(WAREHOUSE_IDComboBox) Then
                            tmpWarehouseID = 0
                        Else
                            tmpWarehouseID = IIf(WAREHOUSE_IDComboBox.Text = "", 0, WAREHOUSEBindingSource.Current("WAREHOUSE_ID"))
                        End If
                    End If
                    SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Filter = "WAREHOUSE_ID = " & tmpWarehouseID



                    SALETableAdapter.SP_GET_CATEGORY_PRODUCT(dgvSaleDetail.CurrentCell.Value, tmpCategory)
                    If tmpCategory = 1 Then

                        If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Count < 1 Then
                            If SALETableAdapter.SP_SELECT_PARAMETER("AUTO SPLIT PRODUCT") = "True" Then

                                PRODUCTTableAdapter.SP_GET_PRODUCT_ID(dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value, tmpProductID)
                                tmpSplit = SALETableAdapter.SP_CHECK_CHILD_PRODUCT(tmpProductID)

                                If tmpSplit = True Then
                                    Dim ChkWarehouse As Boolean
                                    Try
                                        While SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Count < 1

                                            SALETableAdapter.SP_GET_PARENT_PRODUCT_ID(tmpProductID, tmpParent)
                                            ChkWarehouse = WAREHOUSETableAdapter.SP_CHECK_PRODUCT_WAREHOUSE(tmpWarehouseID, tmpParent)

                                            SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.SP_PRODUCT_WAREHOUSE_DETAIL, _
                                                                                         tmpParent)

                                            If ChkWarehouse = True Then Exit While
                                            If tmpParent = 0 Then Exit While
                                            tmpProductID = tmpParent
                                        End While
                                    Catch ex As Exception
                                        If Language = "Indonesian" Then
                                            MsgBox("Produk ini tidak ada." & vbCrLf & _
                                               "Penambahan di batalkan!", MsgBoxStyle.Critical, "DMI Retail")
                                        Else
                                            MsgBox("This Product has no balance." & vbCrLf & _
                                               "Adding this Product is cancelled!", MsgBoxStyle.Critical, "DMI Retail")
                                        End If
                                        dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex - 1)
                                        Exit Sub
                                    End Try

                                    If ChkWarehouse = False Then
                                        If Language = "Indonesian" Then
                                            MsgBox("Produk ini tidak ada." & vbCrLf & _
                                               "Penambahan di batalkan!", MsgBoxStyle.Critical, "DMI Retail")
                                        Else
                                            MsgBox("This Product has no balance." & vbCrLf & _
                                               "Adding this Product is cancelled!", MsgBoxStyle.Critical, "DMI Retail")
                                        End If
                                        If tmpInput = "Browse" Then
                                            dgvSaleDetail.EndEdit()
                                            tmpInput = ""
                                            dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex - 1)
                                            'If dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value = "" Then
                                            'dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex - 1)
                                            'End If
                                        Else
                                            dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex)
                                        End If
                                        Exit Sub
                                    End If
                                Else

                                    ''''Comment Out by Edo 23-Maret-2011''''
                                    'SALETableAdapter.SP_GET_PARENT_PRODUCT_ID(tmpProductID, tmpParent)
                                    'Dim ChkWarehouse As Boolean = WAREHOUSETableAdapter.SP_CHECK_PRODUCT_WAREHOUSE(WAREHOUSEBindingSource.Current("WAREHOUSE_ID"), tmpParent)
                                    'If ChkWarehouse = False Then
                                    If Language = "Indonesian" Then
                                        MsgBox("Produk ini tidak ada." & vbCrLf & _
                                           "Penambahan di batalkan!", MsgBoxStyle.Critical, "DMI Retail")
                                    Else
                                        MsgBox("This Product has no balance." & vbCrLf & _
                                           "Adding this Product is cancelled!", MsgBoxStyle.Critical, "DMI Retail")
                                    End If
                                    If tmpInput = "Browse" Then
                                        dgvSaleDetail.EndEdit()
                                        tmpInput = ""
                                        dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex - 1)
                                        'If dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value = "" Then
                                        'dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex - 1)
                                        'End If
                                    Else
                                        dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex)
                                    End If
                                    Exit Sub
                                    'Else

                                    'End If

                                End If
                            Else
                                If Language = "Indonesian" Then
                                    MsgBox("Produk ini tidak ada." & vbCrLf & _
                                       "Penambahan di batalkan!", MsgBoxStyle.Critical, "DMI Retail")
                                Else
                                    MsgBox("This Product has no balance." & vbCrLf & _
                                       "Adding this Product is cancelled!", MsgBoxStyle.Critical, "DMI Retail")
                                End If
                                If tmpInput = "Browse" Then
                                    dgvSaleDetail.EndEdit()
                                    tmpInput = ""
                                    dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex - 1)
                                    'If dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value = "" Then
                                    'dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex - 1)
                                    'End If
                                Else
                                    dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex)
                                End If
                                Exit Sub
                            End If
                        Else
                            tmpQuantity = SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY")
                            If tmpQuantity <= 0 Then
                                If SALETableAdapter.SP_SELECT_PARAMETER("AUTO SPLIT PRODUCT") = "True" Then
                                    PRODUCTTableAdapter.SP_GET_PRODUCT_ID(dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value, tmpProductID)
                                    tmpSplit = SALETableAdapter.SP_CHECK_CHILD_PRODUCT(tmpProductID)
                                    If tmpSplit = True Then
                                        Try
                                            SALETableAdapter.SP_GET_PARENT_PRODUCT_ID(tmpProductID, tmpParent)
                                        Catch ex As Exception
                                            If Language = "Indonesian" Then
                                                MsgBox("Produk ini mempunyai lebih dari satu Parent !" & vbCrLf & _
                                                "Untuk memecah produk silahkan gunakan form Pemecahan Produk !", MsgBoxStyle.Critical, "DMI Retail")
                                            Else
                                                MsgBox("This product have more than one parent !" & vbCrLf & _
                                                "For split this product please use form Split Product !", MsgBoxStyle.Critical, "DMI Retail")
                                            End If
                                            dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex - 1)
                                            Exit Sub
                                        End Try
                                        Dim ChkWarehouse As Boolean = WAREHOUSETableAdapter.SP_CHECK_PRODUCT_WAREHOUSE(tmpWarehouseID, tmpParent)
                                        If ChkWarehouse = False Then
                                            tmpQuantity = SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY")
                                            If tmpQuantity = 0 Then
                                                If Language = "Indonesian" Then
                                                    MsgBox("Produk ini tidak ada." & vbCrLf & _
                                                       "Penambahan di batalkan!", MsgBoxStyle.Critical, "DMI Retail")
                                                Else
                                                    MsgBox("This Product has no balance." & vbCrLf & _
                                                       "Adding this Product is cancelled!", MsgBoxStyle.Critical, "DMI Retail")
                                                End If
                                                dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.RowCount - 2)
                                                Exit Sub
                                            End If
                                        Else

                                        End If
                                    Else
                                        tmpQuantity = SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY")
                                        If tmpQuantity <= 0 Then
                                            If Language = "Indonesian" Then
                                                MsgBox("Produk ini tidak ada." & vbCrLf & _
                                                   "Penambahan di batalkan!", MsgBoxStyle.Critical, "DMI Retail")
                                            Else
                                                MsgBox("This Product has no balance." & vbCrLf & _
                                                   "Adding this Product is cancelled!", MsgBoxStyle.Critical, "DMI Retail")
                                            End If
                                            dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.RowCount - 2)
                                            Exit Sub
                                        End If
                                    End If
                                Else
                                    tmpQuantity = SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY")
                                    If tmpQuantity <= 0 Then
                                        If Language = "Indonesian" Then
                                            MsgBox("Produk ini tidak ada." & vbCrLf & _
                                               "Penambahan di batalkan!", MsgBoxStyle.Critical, "DMI Retail")
                                        Else
                                            MsgBox("This Product has no balance." & vbCrLf & _
                                               "Adding this Product is cancelled!", MsgBoxStyle.Critical, "DMI Retail")
                                        End If
                                        dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.RowCount - 2)
                                        Exit Sub
                                    End If
                                End If
                            End If
                            tmpQuantity = SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY")
                            'If tmpQuantity <= 0 Then
                            '    If Language = "Indonesian" Then
                            '        MsgBox("Produk ini tidak ada." & vbCrLf & _
                            '           "Penambahan di batalkan!", MsgBoxStyle.Critical, "DMI Retail")
                            '    Else
                            '        MsgBox("This Product has no balance." & vbCrLf & _
                            '           "Adding this Product is cancelled!", MsgBoxStyle.Critical, "DMI Retail")
                            '    End If
                            '    dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.RowCount - 2)
                            '    Exit Sub
                            'End If
                        End If



                    Else
                        'tmpQuantity = SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY")
                        'If tmpQuantity = 0 Then
                        '    If Language = "Indonesian" Then
                        '        MsgBox("Produk ini tidak ada." & vbCrLf & _
                        '           "Penambahan di batalkan!", MsgBoxStyle.Critical, "DMI Retail")
                        '    Else
                        '        MsgBox("This Product has no balance." & vbCrLf & _
                        '           "Adding this Product is cancelled!", MsgBoxStyle.Critical, "DMI Retail")
                        '    End If
                        '    dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.RowCount - 2)
                        '    Exit Sub
                        'End If
                    End If


                    tmpFillingDGV = True
                    AddDataToGrid(dgvSaleDetail, _
                                  dgvSaleDetail.CurrentCell.Value, _
                                  0, _
                                  tmpProductName, _
                                  1, _
                                  2, _
                                  tmpUnitPrice, _
                                  3, _
                                  4, _
                                  5, _
                                  6)
                    CalculationAll(dgvSaleDetail, _
                                     5, _
                                     6, _
                                     AMOUNT_DISCOUNTTextBox, _
                                     TAXPercentageTEXTBOX, _
                                     TAX_AMOUNTTextBox, _
                                     DOWN_PAYMENTTextBox, _
                                     GRAND_TOTALTextBox)
                    lblNoOfItem.Text = "No of Item(s) : " & dgvSaleDetail.RowCount - 1

                    
                    ''Fill Tax Amount when adding Product
                    Dim tmpGrandTotal As Integer = 0
                    For x As Integer = 0 To dgvSaleDetail.RowCount - 1
                        tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvSaleDetail.Item(6, x).Value)
                    Next
                    TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
                    TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)


                ElseIf dgvSaleDetail.CurrentCell.ColumnIndex = 4 Then
                    If tmpFormClosing Then
                        If dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value = "" Then
                            dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex)
                            Exit Sub
                        End If
                    Else
                        If dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value = "" Then
                            dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex)
                            Exit Sub
                        End If
                    End If
                    'Exit Sub
                    'If dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value = "" Then
                    '    dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex)
                    '    Exit Sub
                    'End If
                    'If dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value = "" Then
                    '    If dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value = "" Then
                    '        dgvSaleDetail.CancelEdit()
                    '    Else
                    '        dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex)
                    '    End If
                    '    Exit Sub
                    'End If

                    dgvSaleDetail.Item(5, dgvSaleDetail.CurrentCell.RowIndex).Value = _
                    CInt(dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value) * _
                    CInt(dgvSaleDetail.Item(3, dgvSaleDetail.CurrentCell.RowIndex).Value) * _
                    CInt(dgvSaleDetail.Item(4, dgvSaleDetail.CurrentCell.RowIndex).Value) / 100

                    'If CekQTY(dgvSaleDetail.CurrentRow.Cells("QUANTITY").Value, dgvSaleDetail.CurrentRow.Cells("PRODUCT_CODE").Value) = True Then
                    'Get_Price(dgvSaleDetail.CurrentRow.Cells("PRODUCT_CODE").Value, dgvSaleDetail.CurrentRow.Cells("QUANTITY").Value, dgvSaleDetail.CurrentRow.Cells("PRICE_PER_UNIT").Value)
                    'Else
                    'PRODUCTTableAdapter.SP_GET_SELLING_PRICE(dgvSaleDetail.CurrentCell.Value, tmpUnitPrice)
                    'End If

                    Calculation(dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value, _
                            dgvSaleDetail.Item(3, dgvSaleDetail.CurrentCell.RowIndex).Value, _
                            dgvSaleDetail.Item(5, dgvSaleDetail.CurrentCell.RowIndex).Value, _
                            dgvSaleDetail.Item(6, dgvSaleDetail.CurrentCell.RowIndex).Value)
                    CalculationAll(dgvSaleDetail, _
                                 5, _
                                 6, _
                                 AMOUNT_DISCOUNTTextBox, _
                                 TAXPercentageTEXTBOX, _
                                 TAX_AMOUNTTextBox, _
                                 DOWN_PAYMENTTextBox, _
                                 GRAND_TOTALTextBox)
                    lblNoOfItem.Text = "No of Item(s) : " & dgvSaleDetail.RowCount - 1

                    ''Fill Tax Amount when adding Product
                    Dim tmpGrandTotal As Integer = 0
                    For x As Integer = 0 To dgvSaleDetail.RowCount - 1
                        tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvSaleDetail.Item(6, x).Value)
                    Next
                    TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
                    TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)




                    If CInt(dgvSaleDetail.Item(4, dgvSaleDetail.CurrentCell.RowIndex).Value) > 100 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih besar dari 100%" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not greater than 100%" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvSaleDetail.Item(4, dgvSaleDetail.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                    If CInt(dgvSaleDetail.Item(4, dgvSaleDetail.CurrentCell.RowIndex).Value) < 0 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih kecil dari 0" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not smaller than 0" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvSaleDetail.Item(4, dgvSaleDetail.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If


                ElseIf dgvSaleDetail.CurrentCell.ColumnIndex = 5 Then

                    If tmpFormClosing Then
                        If dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value = "" Then
                            dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex)
                            Exit Sub
                        End If
                    Else
                        If dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value = "" Then
                            dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex)
                            Exit Sub
                        End If
                    End If

                    'If dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value = "" Then
                    '    If dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value = "" Then
                    '        dgvSaleDetail.CancelEdit()
                    '    Else
                    '        dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex)
                    '    End If
                    '    Exit Sub
                    'End If

                    dgvSaleDetail.Item(4, dgvSaleDetail.CurrentCell.RowIndex).Value = _
                        FormatNumber(CInt(dgvSaleDetail.Item(5, dgvSaleDetail.CurrentCell.RowIndex).Value) / _
                        (CInt(dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value) * _
                        CInt(dgvSaleDetail.Item(3, dgvSaleDetail.CurrentCell.RowIndex).Value)) * 100, 2)

                    Calculation(dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value, _
                                dgvSaleDetail.Item(3, dgvSaleDetail.CurrentCell.RowIndex).Value, _
                                dgvSaleDetail.Item(5, dgvSaleDetail.CurrentCell.RowIndex).Value, _
                                dgvSaleDetail.Item(6, dgvSaleDetail.CurrentCell.RowIndex).Value)
                    CalculationAll(dgvSaleDetail, _
                                     5, _
                                     6, _
                                     AMOUNT_DISCOUNTTextBox, _
                                     TAXPercentageTEXTBOX, _
                                     TAX_AMOUNTTextBox, _
                                     DOWN_PAYMENTTextBox, _
                                     GRAND_TOTALTextBox)
                    lblNoOfItem.Text = "No of Item(s) : " & dgvSaleDetail.RowCount - 1

                    ''Fill Tax Amount when adding Product
                    Dim tmpGrandTotal As Integer = 0
                    For x As Integer = 0 To dgvSaleDetail.RowCount - 1
                        tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvSaleDetail.Item(6, x).Value)
                    Next
                    TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
                    TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)


                    If CInt(dgvSaleDetail.Item("DISCOUNT", dgvSaleDetail.CurrentCell.RowIndex).Value) > 100 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih besar dari 100%" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not greater than 100%" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvSaleDetail.Item("DISCOUNT", dgvSaleDetail.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                    If CInt(dgvSaleDetail.Item(5, dgvSaleDetail.CurrentCell.RowIndex).Value) < 0 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih kecil dari 0" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not smaller than 0" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvSaleDetail.Item(5, dgvSaleDetail.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If




                ElseIf dgvSaleDetail.CurrentCell.ColumnIndex = 2 Or _
                    dgvSaleDetail.CurrentCell.ColumnIndex = 3 Or _
                    dgvSaleDetail.CurrentCell.ColumnIndex = 4 Or _
                    dgvSaleDetail.CurrentCell.ColumnIndex = 5 Then

                    If tmpFormClosing Then
                        If dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value = "" Then
                            dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex)
                            Exit Sub
                        End If
                    Else
                        If dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value = "" Then
                            dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex)
                            Exit Sub
                        End If
                    End If

                    If dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value = "" Then
                        If dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value = "" Then
                            dgvSaleDetail.CancelEdit()
                        Else
                            dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex)
                        End If
                        Exit Sub
                    End If

                    'Formula untuk Harga Spesial
                    If CekQTY_Null(dgvSaleDetail.CurrentRow.Cells("PRODUCT_CODE").Value) Then
                        GET_PRICE_NORMAL_NULLQTY(dgvSaleDetail.CurrentRow.Cells("PRODUCT_CODE").Value)
                    Else
                        If CekQTY(dgvSaleDetail.CurrentRow.Cells("QUANTITY").Value, dgvSaleDetail.CurrentRow.Cells("PRODUCT_CODE").Value) = True Then
                            Get_Price(dgvSaleDetail.CurrentRow.Cells("PRODUCT_CODE").Value, dgvSaleDetail.CurrentRow.Cells("QUANTITY").Value, dgvSaleDetail.CurrentRow.Cells("PRICE_PER_UNIT").Value)
                        Else
                            Get_Price_Normal(dgvSaleDetail.CurrentRow.Cells("PRODUCT_CODE").Value, dgvSaleDetail.CurrentRow.Cells("QUANTITY").Value, dgvSaleDetail.CurrentRow.Cells("PRICE_PER_UNIT").Value)
                        End If
                    End If





                    If tmpCategory = 1 Then
                        If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Count < 1 Then
                            If SALETableAdapter.SP_SELECT_PARAMETER("AUTO SPLIT PRODUCT") = "True" Then

                                PRODUCTTableAdapter.SP_GET_PRODUCT_ID(dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value, tmpProductID)
                                tmpSplit = SALETableAdapter.SP_CHECK_CHILD_PRODUCT(tmpProductID)

                                If tmpSplit = True Then

                                    Try
                                        SALETableAdapter.SP_GET_PARENT_PRODUCT_ID(tmpProductID, tmpParent)
                                    Catch ex As Exception
                                        If Language = "Indonesian" Then
                                            MsgBox("Produk ini mempunyai lebih dari satu Parent !" & vbCrLf & _
                                            "Untuk memecah produk silahkan gunakan form Pemecahan Produk !", MsgBoxStyle.Critical, "DMI Retail")
                                        Else
                                            MsgBox("This product have more than one parent !" & vbCrLf & _
                                            "For split this product please use form Split Product !", MsgBoxStyle.Critical, "DMI Retail")
                                        End If
                                        dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex - 1)
                                        Exit Sub
                                    End Try

                                    '' check Parent Product in Warehouse
                                    Dim ChkWarehouse As Boolean = WAREHOUSETableAdapter.SP_CHECK_PRODUCT_WAREHOUSE(tmpWarehouseID, tmpParent)
                                    If ChkWarehouse = True Then

                                        Try
                                            SALETableAdapter.SP_GET_PARENT_PRODUCT_ID(tmpProductID, tmpParent)
                                        Catch ex As Exception
                                            If Language = "Indonesian" Then
                                                MsgBox("Produk ini mempunyai lebih dari satu Parent !" & vbCrLf & _
                                                "Untuk memecah produk silahkan gunakan form Pemecahan Produk !", MsgBoxStyle.Critical, "DMI Retail")
                                            Else
                                                MsgBox("This product have more than one parent !" & vbCrLf & _
                                                "For split this product please use form Split Product !", MsgBoxStyle.Critical, "DMI Retail")
                                            End If
                                            dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex - 1)
                                            Exit Sub
                                        End Try
                                        PRODUCTTableAdapter.SP_GET_QUANTITY_GROUPING_PRODUCT(tmpProductID, tmpQtyGrouping)
                                        tmpQtyProduct = SALETableAdapter.SP_GET_QUANTITY_PRODUCT_WAREHOUSE(tmpParent, tmpWarehouseID)

                                        If ((tmpQtyProduct * tmpQtyGrouping) + tmpQuantity) >= CInt(dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value) Then

                                        Else

                                            If dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value > tmpQuantity Then
                                                If Language = "Indonesian" Then
                                                    MsgBox("Produk ini mempunyai " & ((tmpQtyProduct * tmpQtyGrouping) + tmpQuantity) & " quantity." & vbCrLf & _
                                                   "Produk yang dijual melebihi jumlah yang diperkenankan.", _
                                                   MsgBoxStyle.Critical, "DMI Retail")
                                                Else
                                                    MsgBox("This Product has " & ((tmpQtyProduct * tmpQtyGrouping) + tmpQuantity) & " quantity." & vbCrLf & _
                                                   "Selling a Product with quantity exceed the balance is not allowed.", _
                                                   MsgBoxStyle.Critical, "DMI Retail")
                                                End If
                                                dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value = tmpQuantity
                                                Exit Sub
                                            End If

                                        End If

                                    Else
                                        If dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value > tmpQuantity Then
                                            If Language = "Indonesian" Then
                                                MsgBox("Produk ini mempunyai " & tmpQuantity & " quantity." & vbCrLf & _
                                               "Produk yang dijual melebihi jumlah yang diperkenankan.", _
                                               MsgBoxStyle.Critical, "DMI Retail")
                                            Else
                                                MsgBox("This Product has " & tmpQuantity & " quantity." & vbCrLf & _
                                               "Selling a Product with quantity exceed the balance is not allowed.", _
                                               MsgBoxStyle.Critical, "DMI Retail")
                                            End If
                                            dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value = tmpQuantity
                                            Exit Sub
                                        End If
                                    End If
                                Else
                                    ''''Comment Out By Edo 23 Maret 2011''''
                                    If dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value > tmpQuantity Then
                                        If Language = "Indonesian" Then
                                            MsgBox("Produk ini mempunyai " & tmpQuantity & " quantity." & vbCrLf & _
                                           "Produk yang dijual melebihi jumlah yang diperkenankan.", _
                                           MsgBoxStyle.Critical, "DMI Retail")
                                        Else
                                            MsgBox("This Product has " & tmpQuantity & " quantity." & vbCrLf & _
                                           "Selling a Product with quantity exceed the balance is not allowed.", _
                                           MsgBoxStyle.Critical, "DMI Retail")
                                        End If
                                        dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value = tmpQuantity
                                        Exit Sub
                                    End If
                                End If

                            Else
                                If dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value > tmpQuantity Then
                                    If Language = "Indonesian" Then
                                        MsgBox("Produk ini mempunyai " & tmpQuantity & " quantity." & vbCrLf & _
                                       "Produk yang dijual melebihi jumlah yang diperkenankan.", _
                                       MsgBoxStyle.Critical, "DMI Retail")
                                    Else
                                        MsgBox("This Product has " & tmpQuantity & " quantity." & vbCrLf & _
                                       "Selling a Product with quantity exceed the balance is not allowed.", _
                                       MsgBoxStyle.Critical, "DMI Retail")
                                    End If
                                    dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value = tmpQuantity
                                    Exit Sub

                                End If

                            End If
                        Else

                            'SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.SP_PRODUCT_WAREHOUSE_DETAIL, _
                            '                                             tmpProductID)
                            'If WAREHOUSEBindingSource Is Nothing Or WAREHOUSEBindingSource.Count < 1 Then
                            '    tmpWarehouseID = 0
                            'Else
                            '    If Not ValidateComboBox(WAREHOUSE_IDComboBox) Then
                            '        tmpWarehouseID = 0
                            '    Else
                            '        tmpWarehouseID = IIf(WAREHOUSE_IDComboBox.Text = "", 0, WAREHOUSEBindingSource.Current("WAREHOUSE_ID"))
                            '    End If
                            'End If
                            'SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Filter = "WAREHOUSE_ID = " & tmpWarehouseID

                            If tmpQuantity < CInt(dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value) Then
                                If SALETableAdapter.SP_SELECT_PARAMETER("AUTO SPLIT PRODUCT") = "True" Then

                                    PRODUCTTableAdapter.SP_GET_PRODUCT_ID(dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value, tmpProductID)
                                    tmpSplit = SALETableAdapter.SP_CHECK_CHILD_PRODUCT(tmpProductID)

                                    If tmpSplit = True Then

                                        Try
                                            SALETableAdapter.SP_GET_PARENT_PRODUCT_ID(tmpProductID, tmpParent)
                                        Catch ex As Exception
                                            If Language = "Indonesian" Then
                                                MsgBox("Produk ini mempunyai lebih dari satu Parent !" & vbCrLf & _
                                                "Untuk memecah produk silahkan gunakan form Pemecahan Produk !", MsgBoxStyle.Critical, "DMI Retail")
                                            Else
                                                MsgBox("This product have more than one parent !" & vbCrLf & _
                                                "For split this product please use form Split Product !", MsgBoxStyle.Critical, "DMI Retail")
                                            End If
                                            dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex)
                                            Exit Sub
                                        End Try

                                        '' check Parent Product in Warehouse
                                        Dim ChkWarehouse As Boolean
                                        Dim tmpAll As Integer = 0
                                        Dim tmpTotal As Integer = 0
                                        While tmpAll <= CInt(dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value)
                                            Try
                                                SALETableAdapter.SP_GET_PARENT_PRODUCT_ID(tmpProductID, tmpParent)
                                            Catch
                                                If Language = "Indonesian" Then
                                                    MsgBox("Produk ini mempunyai " & tmpQuantity & " quantity." & vbCrLf & _
                                                   "Produk yang dijual melebihi jumlah yang diperkenankan.", _
                                                   MsgBoxStyle.Critical, "DMI Retail")
                                                Else
                                                    MsgBox("This Product has " & tmpQuantity & " quantity." & vbCrLf & _
                                                   "Selling a Product with quantity exceed the balance is not allowed.", _
                                                   MsgBoxStyle.Critical, "DMI Retail")
                                                End If
                                                dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value = tmpQuantity
                                                Exit Sub
                                            End Try
                                            ChkWarehouse = WAREHOUSETableAdapter.SP_CHECK_PRODUCT_WAREHOUSE(tmpWarehouseID, tmpParent)

                                            SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.SP_PRODUCT_WAREHOUSE_DETAIL, _
                                                                                         tmpParent)

                                            PRODUCTTableAdapter.SP_GET_QUANTITY_GROUPING_PRODUCT(tmpProductID, tmpQtyGrouping)
                                            tmpQtyProduct = SALETableAdapter.SP_GET_QUANTITY_PRODUCT_WAREHOUSE(tmpParent, tmpWarehouseID)

                                            tmpAll = ((tmpQtyProduct * tmpQtyGrouping) + tmpQuantity)
                                            If ChkWarehouse = True Then Exit While
                                            If tmpParent = 0 Then Exit While
                                            tmpProductID = tmpParent
                                        End While
                                        If ChkWarehouse = True Then

                                            Try
                                                While SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Count < 1
                                                    SALETableAdapter.SP_GET_PARENT_PRODUCT_ID(tmpProductID, tmpParent)
                                                    'ChkWarehouse = WAREHOUSETableAdapter.SP_CHECK_PRODUCT_WAREHOUSE(tmpWarehouseID, tmpParent)

                                                    SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.SP_PRODUCT_WAREHOUSE_DETAIL, _
                                                                                                 tmpParent)

                                                    If ChkWarehouse = True Then Exit While
                                                    If tmpParent = 0 Then Exit While
                                                    tmpProductID = tmpParent
                                                End While
                                            Catch ex As Exception
                                                If Language = "Indonesian" Then
                                                    MsgBox("Produk ini mempunyai lebih dari satu Parent !" & vbCrLf & _
                                                    "Untuk memecah produk silahkan gunakan form Pemecahan Produk !", MsgBoxStyle.Critical, "DMI Retail")
                                                Else
                                                    MsgBox("This product have more than one parent !" & vbCrLf & _
                                                    "For split this product please use form Split Product !", MsgBoxStyle.Critical, "DMI Retail")
                                                End If
                                                dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex)
                                                Exit Sub
                                            End Try

                                            tmpTotal = 0
                                            While tmpTotal <= CInt(dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value)
                                                'While chkParent = True

                                                While tmpAll <= CInt(dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value)
                                                    Try
                                                        SALETableAdapter.SP_GET_PARENT_PRODUCT_ID(tmpProductID, tmpParent)
                                                    Catch
                                                        If Language = "Indonesian" Then
                                                            MsgBox("Produk ini mempunyai " & tmpTotal & " quantity." & vbCrLf & _
                                                           "Produk yang dijual melebihi jumlah yang diperkenankan.", _
                                                           MsgBoxStyle.Critical, "DMI Retail")
                                                        Else
                                                            MsgBox("This Product has " & tmpTotal & " quantity." & vbCrLf & _
                                                           "Selling a Product with quantity exceed the balance is not allowed.", _
                                                           MsgBoxStyle.Critical, "DMI Retail")
                                                        End If
                                                        dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value = tmpTotal
                                                        Exit Sub
                                                    End Try

                                                    ChkWarehouse = WAREHOUSETableAdapter.SP_CHECK_PRODUCT_WAREHOUSE(tmpWarehouseID, tmpParent)

                                                    SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.SP_PRODUCT_WAREHOUSE_DETAIL, _
                                                                                                 tmpParent)
                                                    tmpProductID = tmpParent

                                                    If ChkWarehouse = True Then Exit While
                                                    If tmpParent = 0 Then Exit While

                                                End While
                                                Try
                                                    SALETableAdapter.SP_GET_MAXISMUM_SPLIT_PRODUCT(tmpParent, tmpAll)
                                                Catch ex As Exception
                                                    If Language = "Indonesian" Then
                                                        MsgBox("Produk ini mempunyai " & tmpTotal & " quantity." & vbCrLf & _
                                                       "Produk yang dijual melebihi jumlah yang diperkenankan.", _
                                                       MsgBoxStyle.Critical, "DMI Retail")
                                                    Else
                                                        MsgBox("This Product has " & tmpTotal & " quantity." & vbCrLf & _
                                                       "Selling a Product with quantity exceed the balance is not allowed.", _
                                                       MsgBoxStyle.Critical, "DMI Retail")
                                                    End If
                                                    dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value = tmpTotal
                                                    Exit Sub
                                                End Try


                                                tmpTotal = tmpTotal + tmpAll

                                            End While

                                            If tmpTotal >= CInt(dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value) Then

                                            Else

                                                If dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value > tmpQuantity Then
                                                    If Language = "Indonesian" Then
                                                        MsgBox("Produk ini mempunyai " & ((tmpQtyProduct * tmpQtyGrouping) + tmpQuantity) & " quantity." & vbCrLf & _
                                                       "Produk yang dijual melebihi jumlah yang diperkenankan.", _
                                                       MsgBoxStyle.Critical, "DMI Retail")
                                                    Else
                                                        MsgBox("This Product has " & ((tmpQtyProduct * tmpQtyGrouping) + tmpQuantity) & " quantity." & vbCrLf & _
                                                       "Selling a Product with quantity exceed the balance is not allowed.", _
                                                       MsgBoxStyle.Critical, "DMI Retail")
                                                    End If
                                                    dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value = tmpQuantity
                                                    Exit Sub
                                                End If

                                            End If

                                        Else
                                            If dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value > tmpQuantity Then
                                                If Language = "Indonesian" Then
                                                    MsgBox("Produk ini mempunyai " & tmpQuantity & " quantity." & vbCrLf & _
                                                   "Produk yang dijual melebihi jumlah yang diperkenankan.", _
                                                   MsgBoxStyle.Critical, "DMI Retail")
                                                Else
                                                    MsgBox("This Product has " & tmpQuantity & " quantity." & vbCrLf & _
                                                   "Selling a Product with quantity exceed the balance is not allowed.", _
                                                   MsgBoxStyle.Critical, "DMI Retail")
                                                End If
                                                dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value = tmpQuantity
                                                Exit Sub
                                            End If
                                        End If
                                    Else
                                        ''''Comment Out By Edo 23 Maret 2011''''
                                        If dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value > tmpQuantity Then
                                            If Language = "Indonesian" Then
                                                MsgBox("Produk ini mempunyai " & tmpQuantity & " quantity." & vbCrLf & _
                                               "Produk yang dijual melebihi jumlah yang diperkenankan.", _
                                               MsgBoxStyle.Critical, "DMI Retail")
                                            Else
                                                MsgBox("This Product has " & tmpQuantity & " quantity." & vbCrLf & _
                                               "Selling a Product with quantity exceed the balance is not allowed.", _
                                               MsgBoxStyle.Critical, "DMI Retail")
                                            End If
                                            dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value = tmpQuantity
                                            Exit Sub
                                        End If
                                    End If

                                Else
                                    If dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value > tmpQuantity Then
                                        If Language = "Indonesian" Then
                                            MsgBox("Produk ini mempunyai " & tmpQuantity & " quantity." & vbCrLf & _
                                           "Produk yang dijual melebihi jumlah yang diperkenankan.", _
                                           MsgBoxStyle.Critical, "DMI Retail")
                                        Else
                                            MsgBox("This Product has " & tmpQuantity & " quantity." & vbCrLf & _
                                           "Selling a Product with quantity exceed the balance is not allowed.", _
                                           MsgBoxStyle.Critical, "DMI Retail")
                                        End If
                                        dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value = tmpQuantity
                                        Exit Sub

                                    End If

                                End If
                            End If
                        End If

                    End If


                    If dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value > 9999 Then
                        If Language = "Indonesian" Then
                            MsgBox("Produk yang dijual melebihi jumlah yang diperkenankan.", _
                           MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Selling a Product with quantity exceed the balance is not allowed.", _
                           MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value = tmpQuantity
                        Exit Sub
                    End If

                    If dgvSaleDetail.Item(3, dgvSaleDetail.CurrentCell.RowIndex).Value > 999999999 Then
                        If Language = "Indonesian" Then
                            MsgBox("Harga produk melebihi jumlah yang diperkenankan.", _
                           MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Price Product with quantity exceed the balance is not allowed.", _
                           MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvSaleDetail.Item(3, dgvSaleDetail.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                    dgvSaleDetail.Item(5, dgvSaleDetail.CurrentCell.RowIndex).Value = _
                        CInt(dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value) * _
                        CInt(dgvSaleDetail.Item(3, dgvSaleDetail.CurrentCell.RowIndex).Value) * _
                        CInt(dgvSaleDetail.Item(4, dgvSaleDetail.CurrentCell.RowIndex).Value) / 100

                    Calculation(dgvSaleDetail.Item(2, dgvSaleDetail.CurrentCell.RowIndex).Value, _
                                dgvSaleDetail.Item(3, dgvSaleDetail.CurrentCell.RowIndex).Value, _
                                dgvSaleDetail.Item(5, dgvSaleDetail.CurrentCell.RowIndex).Value, _
                                dgvSaleDetail.Item(6, dgvSaleDetail.CurrentCell.RowIndex).Value)
                    CalculationAll(dgvSaleDetail, _
                                     5, _
                                     6, _
                                     AMOUNT_DISCOUNTTextBox, _
                                     TAXPercentageTEXTBOX, _
                                     TAX_AMOUNTTextBox, _
                                     DOWN_PAYMENTTextBox, _
                                     GRAND_TOTALTextBox)
                    lblNoOfItem.Text = "No of Item(s) : " & dgvSaleDetail.RowCount - 1

                    ''Fill Tax Amount when adding Product
                    Dim tmpGrandTotal As Integer = 0
                    For x As Integer = 0 To dgvSaleDetail.RowCount - 1
                        tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvSaleDetail.Item(6, x).Value)
                    Next
                    TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
                    TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)


                    If CInt(dgvSaleDetail.Item(4, dgvSaleDetail.CurrentCell.RowIndex).Value) > 100 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih besar dari 100%" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not greater than 100%" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvSaleDetail.Item(4, dgvSaleDetail.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                    If CInt(dgvSaleDetail.Item(4, dgvSaleDetail.CurrentCell.RowIndex).Value) < 0 Then
                        If Language = "Indonesian" Then
                            MsgBox("Discount seharusnya tidak lebih kecil dari 0" & vbCrLf & _
                          "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                        Else
                            MsgBox("Discount should not smaller than 0" & vbCrLf & _
                          "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                        End If
                        dgvSaleDetail.Item(4, dgvSaleDetail.CurrentCell.RowIndex).Value = 0
                        Exit Sub
                    End If

                ElseIf dgvSaleDetail.CurrentCell.ColumnIndex = 2 Then
                    If CekQTY(dgvSaleDetail.CurrentRow.Cells("QUANTITY").Value, dgvSaleDetail.CurrentRow.Cells("PRODUCT_CODE").Value) = True Then
                        Get_Price(dgvSaleDetail.CurrentRow.Cells("PRODUCT_CODE").Value, dgvSaleDetail.CurrentRow.Cells("QUANTITY").Value, dgvSaleDetail.CurrentRow.Cells("PRICE_PER_UNIT").Value)
                    End If

                End If
            End If
        End If
        'If CekQTY_Null(dgvSaleDetail.CurrentRow.Cells("PRODUCT_CODE").Value) = True Then
        '    GET_PRICE_NORMAL_NULLQTY(dgvSaleDetail.CurrentRow.Cells("PRODUCT_CODE").Value)
        'Else
        '    If CekQTY(dgvSaleDetail.CurrentRow.Cells("QUANTITY").Value, dgvSaleDetail.CurrentRow.Cells("PRODUCT_CODE").Value) = True Then
        '        Get_Price(dgvSaleDetail.CurrentRow.Cells("PRODUCT_CODE").Value, dgvSaleDetail.CurrentRow.Cells("QUANTITY").Value, dgvSaleDetail.CurrentRow.Cells("PRICE_PER_UNIT").Value)
        '    Else
        '        Get_Price_Normal(dgvSaleDetail.CurrentRow.Cells("PRODUCT_CODE").Value, dgvSaleDetail.CurrentRow.Cells("QUANTITY").Value, dgvSaleDetail.CurrentRow.Cells("PRICE_PER_UNIT").Value)
        '    End If
        'End If
        'dgvSaleDetail.Rows(dgvSaleDetail.RowCount - 1).Cells("PRICE_PER_UNIT").Value = ""
        tmpChange = True
    End Sub
    Private Sub dgvSaleDetail_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvSaleDetail.KeyUp
        If tmpSaveMode = "Update" Then
            If e.KeyData = 13 Then Exit Sub
            If e.KeyCode = Keys.Delete Then Exit Sub

        Else

            If e.KeyData = 13 Then
                If dgvSaleDetail.RowCount = 1 Then Exit Sub
                dgvSaleDetail.CurrentCell = _
                    dgvSaleDetail.Rows(dgvSaleDetail.RowCount - 1).Cells(0)
            ElseIf e.KeyCode = Keys.Delete Then
                If dgvSaleDetail.CurrentRow.Index = dgvSaleDetail.RowCount - 1 Then Exit Sub
                dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex)
                CalculationAll(dgvSaleDetail, _
                             5, _
                             6, _
                             AMOUNT_DISCOUNTTextBox, _
                             TAXPercentageTEXTBOX, _
                             TAX_AMOUNTTextBox, _
                             DOWN_PAYMENTTextBox, _
                             GRAND_TOTALTextBox)
                lblNoOfItem.Text = "No of Item(s) : " & dgvSaleDetail.RowCount - 1

                If dgvSaleDetail.Item("PRODUCT_CODE", 0).Value = "" Then
                    TAXPercentageTEXTBOX.Text = "0"
                    TAX_AMOUNTTextBox.Text = "0"
                End If
            End If

        End If
    End Sub

    Private Sub DOWN_PAYMENTTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DOWN_PAYMENTTextBox.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""
    End Sub

    Private Sub GRAND_TOTALTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles GRAND_TOTALTextBox.KeyPress
        e.KeyChar = ""
    End Sub

    Private Sub AMOUNT_DISCOUNTTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles AMOUNT_DISCOUNTTextBox.KeyPress
        e.KeyChar = ""
    End Sub

    Private Sub TAXPercentageTEXTBOX_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""
    End Sub

    Private Sub TAXPercentageTEXTBOX_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim tmpGrandTotal As Integer = 0
        For x As Integer = 0 To dgvSaleDetail.RowCount - 1
            tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvSaleDetail.Item(6, x).Value)
        Next
        TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
        TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        Me.Close()
        tmpChange = False
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Dim tmpDate As DateTime
        Dim tmpWarehouseID As Integer

        If SALEBindingSource.Count > 200 Then
            If Language = "Indonesian" Then
                MsgBox("Aplikasi DMI Retail yang Anda gunakan tidak sesuai dengan lisensi." & vbCrLf & _
                       "Silahkan hubungi DMI-Soft untuk pemakaian Aplikasi DMI Retail lebih lanjut.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            ElseIf Language = "English" Then
                MsgBox("DMI Retail you are using is not licensed." & vbCrLf & _
                       "Please contact DMI-Soft to continue using this Software", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
            Me.Close()
        End If

        If dgvSaleDetail.Item(0, 0).Value = "" Or dgvSaleDetail.Item(1, 0).Value = "" Or _
            dgvSaleDetail.Item(2, 0).Value = "0" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak ada Produk untuk dijual !" & vbCrLf & _
                   "Masukkan produk, atau Anda dapat membatalkan transaksi ini.", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Product to Sale !" & vbCrLf & _
                   "You can input product to Sale or cancel this transaction.", MsgBoxStyle.Critical, "DMI Retail")
            End If

        End If


        ''Check Total and Tax
        If Val(TAX_AMOUNTTextBox.Text) < 0 Or Val(TAXPercentageTEXTBOX.Text) > 100 Then
            If Language = "Indonesian" Then
                MsgBox("Nilai pajak yang anda masukan salah !" & vbCrLf & _
               "Silahkan masukan pajak yang benar !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You entered wrong tax's value !" & vbCrLf & _
                "Please enter the correct Tax !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            TAXPercentageTEXTBOX.Focus()
            Exit Sub
        End If

        ''Check Discount 1
        For n = 0 To dgvSaleDetail.RowCount - 2
            If CInt(dgvSaleDetail.Item("DISCOUNT", n).Value) > 100 Then
                If Language = "Indonesian" Then
                    MsgBox("Discount seharusnya tidak lebih besar dari 100%" & vbCrLf & _
                  "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Discount should not greater than 100%" & vbCrLf & _
                  "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                dgvSaleDetail.Item("DISCOUNT", n).Value = 0
                If dgvSaleDetail.Item(0, dgvSaleDetail.CurrentCell.RowIndex).Value = "" Then
                    dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex)
                    Exit Sub
                End If
                Exit Sub
            End If

            If CInt(dgvSaleDetail.Item(5, n).Value) < 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Discount seharusnya tidak lebih kecil dari 0" & vbCrLf & _
                  "Tolong masukan nilai yang benar!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Discount should not smaller than 0" & vbCrLf & _
                  "Please enter the correct amount!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                dgvSaleDetail.Item(5, n).Value = 0
                Exit Sub
            End If
        Next

        ''Check Discount 2 & 3
        If Val(txtDiscount2.Text) < 0 Or Val(txtDiscount2.Text) > 100 Then
            If Language = "Indonesian" Then
                MsgBox("Nilai Discount yang anda masukan salah !" & vbCrLf & _
               "Silahkan masukan Discount yang benar !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You entered wrong Discount's value !" & vbCrLf & _
                "Please enter the correct Discount !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtDiscount2.Focus()
            Exit Sub
        End If

        If Val(txtDiscount3.Text) < 0 Or Val(txtDiscount3.Text) > 100 Then
            If Language = "Indonesian" Then
                MsgBox("Nilai Discount yang anda masukan salah !" & vbCrLf & _
               "Silahkan masukan Discount yang benar !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You entered wrong Discount's value !" & vbCrLf & _
                "Please enter the correct Discount !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtDiscount3.Focus()
            Exit Sub
        End If

        ''''check Garand Total''''
        If Val(GRAND_TOTALTextBox.Text) < 0 Then
            If Language = "Indonesian" Then
                MsgBox("Grand Total seharusnya tidak lebih kecil dari '0' !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Grand Total not smaller than '0' !", MsgBoxStyle.Critical, "DMI Retail")
            End If

            GRAND_TOTALTextBox.Focus()
            Exit Sub
        End If

        ''Check Maturity Date
        If SALE_DATEDateTimePicker.Value > MATURITY_DATEDateTimePicker.Value Then
            If Language = "Indonesian" Then
                MsgBox("Tanggal jatuh tempo seharusnya lebih besar dari tanggal penjualan!", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Maturity Date should greater than Sale Date!", MsgBoxStyle.Critical, "DMI Retail")
            End If
            MATURITY_DATEDateTimePicker.Value = Now
            Exit Sub
        End If

        ''Check Transaction in Closing Period
        Dim tmpCheck As String = ""
        SALETableAdapter.SP_CHECK_CLOSING_PERIOD(SALE_DATEDateTimePicker.Value, tmpCheck)
        If tmpCheck = "TRUE" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menyimpan transaksi ini!" & vbCrLf & "Transaksi ini sudah masuk dalam Tutup Buku", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Save this Transaction!" & vbCrLf & "This Transaction has been in Closed Period", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If



        tmpDate = SALE_DATEDateTimePicker.Value.Year & "/" & _
                  SALE_DATEDateTimePicker.Value.Month & "/" & _
                  SALE_DATEDateTimePicker.Value.Day & " " & _
                  Now.Hour & ":" & Now.Minute & ":" & Now.Second

        If DOWN_PAYMENTTextBox.Text = "" Then DOWN_PAYMENTTextBox.Text = "0"

        ''check Grand Total
        If GRAND_TOTALTextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            GRAND_TOTALTextBox.Focus()
            Exit Sub
        End If

        ''Check Payment Method
        If PAYMENT_METHOD_IDComboBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            tmpFormClosing = True
            PAYMENT_METHOD_IDComboBox.Focus()
            Exit Sub
        End If

        ''Check Quantity in DgvSale
        For n = 0 To dgvSaleDetail.RowCount - 2
            PRODUCTTableAdapter.SP_GET_PRODUCT_ID(dgvSaleDetail.Item("PRODUCT_CODE", n).Value, tmpProductID)
            If dgvSaleDetail.Item("QUANTITY", n).Value = "0" Then
                If Language = "Indonesian" Then
                    MsgBox("Silahkan isi kolom Jumlah !" & vbCrLf & _
                     "Untuk Kolom : Jumlah " & vbCrLf & _
                     "Baris Nomor : " & n + 1, _
                          MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Please fill Quantity fields !" & vbCrLf & _
                     "For Column : Quantity" & vbCrLf & _
                     "Row number : " & n + 1, _
                          MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If

            '''''Validation fro check selling price <= modal '''''

        Next

        ''Check Receipt No and Data product in dgvSale
        If RECEIPT_NOTextBox.Text = "" Or dgvSaleDetail.RowCount = 1 Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        ''check value combobox
        If Not ValidateAllComboBox(Me) Then
            If Language = "Indonesian" Then
                MsgBox("Nilai yang anda masukan tidak terdapat dalam daftar.", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You input value which is not in the list.", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        ''''CHECK PAYMENT GROUP''''
        Dim tmpPaymentGroup As String = ""
        SALETableAdapter.SP_GET_PAYMENT_GROUP(PAYMENT_METHODBindingSource.Current("PAYMENT_METHOD_ID"), tmpPaymentGroup)
        If tmpPaymentGroup = "Cash" Then
            ''check DP
            If CInt(DOWN_PAYMENTTextBox.Text) < CInt(GRAND_TOTALTextBox.Text) Then
                If Language = "Indonesian" Then
                    MsgBox("Pembayaran seharusnya tidak lebih kecil dari Grand Total!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Payment should not smaller than Grand Total!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If
        ElseIf tmpPaymentGroup = "Credit" Then
            If CInt(DOWN_PAYMENTTextBox.Text) > CInt(GRAND_TOTALTextBox.Text) Then
                If Language = "Indonesian" Then
                    MsgBox("Uang Muka seharusnya tidak lebih besar dari Grand Total!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Down Payment should not bigger than Grand Total!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If

            '''''CUSTOMER COMMENT BY EDO'''''
            'If CUSTOMER_IDComboBox.Text = "" Then
            '    If Language = "Indonesian" Then
            '        MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            '    Else
            '        MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            '    End If
            '    CUSTOMER_IDComboBox.Focus()
            '    Exit Sub
            'End If
        End If


        ''Check Exist Receipt No
        SALETableAdapter.SP_SALE_CHECK_RECEIPT_NO(RECEIPT_NOTextBox.Text, tmpNoOfReceiptNo)
        If tmpNoOfReceiptNo > 0 Then
            If Language = "Indonesian" Then
                MsgBox("No Transaksi telah digunakan oleh user lain." & vbCrLf & _
                                           "Nomor transaksi baru akan digunakan untuk transaksi ini.", _
                                           MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Receipt Number is used by another User." & vbCrLf & _
                                                    "New Receipt Number will be assigned to this transaction.", _
                                                    MsgBoxStyle.Critical, "DMI Retail")
            End If
        End If

        While tmpNoOfReceiptNo > 0
            SALETableAdapter.SP_GENERATE_SALE_RECEIPT_NO(RECEIPT_NOTextBox.Text)
            SALETableAdapter.SP_SALE_CHECK_RECEIPT_NO(RECEIPT_NOTextBox.Text, tmpNoOfReceiptNo)
        End While

        DOWN_PAYMENTTextBox.Text = IIf(DOWN_PAYMENTTextBox.Text.Length = 0, "0", DOWN_PAYMENTTextBox.Text)
        Dim tmpCustomerID As Integer = 0
        If CUSTOMER_IDComboBox.Text = "" Then
            tmpCustomerID = 0
        Else
            tmpCustomerID = CUSTOMERBindingSource.Current("CUSTOMER_ID")
        End If

        'tmpWarehouseID = IIf(WAREHOUSE_IDComboBox.Text = "", 0, WAREHOUSEBindingSource.Current("WAREHOUSE_ID"))

        ''Check Closing Period Running
        If SALETableAdapter.SP_CLOSED_PERIOD_PROCESS("G", SALE_DATEDateTimePicker.Value) = "TRUE" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menyimpan transaksi ini!" & vbCrLf & "Proses Tutup Buku sedang berlangsung", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Save this Transaction!" & vbCrLf & "The Closing Period is Running", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        ''get warehouse_id
        If WAREHOUSEBindingSource Is Nothing Or WAREHOUSEBindingSource.Count < 1 Then
            tmpWarehouseID = 0
        Else
            If Not ValidateComboBox(WAREHOUSE_IDComboBox) Then
                tmpWarehouseID = 0
            Else
                tmpWarehouseID = IIf(WAREHOUSE_IDComboBox.Text = "", 0, WAREHOUSEBindingSource.Current("WAREHOUSE_ID"))
            End If
        End If




        For x As Integer = 0 To dgvSaleDetail.RowCount - 2

            ''get Product_id
            PRODUCTTableAdapter.SP_GET_PRODUCT_ID(dgvSaleDetail.Item(0, x).Value, tmpProductID)
            ''Get Category
            SALETableAdapter.SP_GET_CATEGORY_PRODUCT(dgvSaleDetail.Item("PRODUCT_CODE", x).Value, tmpCategory)
            'Dim tmpParent As Integer
            'SALETableAdapter.SP_GET_PARENT_PRODUCT_ID(tmpProductID, tmpParent)


            If tmpCategory = 1 Then

                If PRODUCTBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                    If Language = "Indonesian" Then
                        MsgBox("Tidak dapat menyimpan transaksi ini!" & vbCrLf & "Produk telah dihapus", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("Cannot Save this Transaction!" & vbCrLf & "Product has been deleted", MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    Exit Sub
                End If

            End If



            ''Get Detail From Product Warehouse
            SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.SP_PRODUCT_WAREHOUSE_DETAIL, _
                                          tmpProductID)
            SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Filter = "WAREHOUSE_ID = " & tmpWarehouseID

            If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Count < 1 Then
                If tmpQuantity < CInt(dgvSaleDetail.Item("QUANTITY", x).Value) Then
                    While tmpQuantity < CInt(dgvSaleDetail.Item("QUANTITY", x).Value)
                        SplitMultiLevelGroupingProduct(tmpProductID, tmpWarehouseID, dgvSaleDetail.Item("QUANTITY", x).Value)
                        SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.SP_PRODUCT_WAREHOUSE_DETAIL, tmpProductID)
                        SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Filter = "WAREHOUSE_ID = " & tmpWarehouseID
                        tmpQuantity = SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY")
                    End While
                End If
            Else
                tmpQuantity = SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY")
                If tmpQuantity > CInt(dgvSaleDetail.Item("QUANTITY", x).Value) Then
                    While tmpQuantity < CInt(dgvSaleDetail.Item("QUANTITY", x).Value)
                        SplitMultiLevelGroupingProduct(tmpProductID, tmpWarehouseID, dgvSaleDetail.Item("QUANTITY", x).Value)
                        SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.SP_PRODUCT_WAREHOUSE_DETAIL, tmpProductID)
                        SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Filter = "WAREHOUSE_ID = " & tmpWarehouseID
                        tmpQuantity = SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY")
                    End While
                End If
            End If



            ''Check Product in warehouse
            '        If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Count < 1 Then

            '            ''Check Quantity in Product Warehouse
            '            If tmpQuantity < CInt(dgvSaleDetail.Item("QUANTITY", x).Value) Then

            '                ''Check Auto Split Product
            '                If SALETableAdapter.SP_SELECT_PARAMETER("AUTO SPLIT PRODUCT") = "True" Then


            '                    Dim tmpParent As Integer
            '                    Try
            '                        SALETableAdapter.SP_GET_PARENT_PRODUCT_ID(tmpProductID, tmpParent)
            '                    Catch ex As Exception
            '                        If Language = "Indonesian" Then
            '                            MsgBox("Produk ini mempunyai lebih dari satu Parent !" & vbCrLf & _
            '                            "Untuk memecah produk silahkan gunakan form Pemecahan Produk !", MsgBoxStyle.Critical, "DMI Retail")
            '                        Else
            '                            MsgBox("This product have more than one parent !" & vbCrLf & _
            '                            "For split this product please use form Split Product !", MsgBoxStyle.Critical, "DMI Retail")
            '                        End If
            '                        dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex - 1)
            '                        Exit Sub
            '                    End Try

            '                    '' check Parent Product in Warehouse
            '                    Dim ChkWarehouse As Boolean = WAREHOUSETableAdapter.SP_CHECK_PRODUCT_WAREHOUSE(tmpWarehouseID, tmpParent)
            '                    If ChkWarehouse = False Then
            '                        If Language = "Indonesian" Then
            '                            MsgBox("Tidak dapat menyimpan produk ini!" & vbCrLf & "Induk Produk dari : " & dgvSaleDetail.Item(1, x).Value & vbCrLf & " tidak terdapat di " & _
            '                                WAREHOUSE_IDComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
            '                        Else
            '                            MsgBox("Cannot save this transaction!" & vbCrLf & "Parent Product from : " & dgvSaleDetail.Item(1, x).Value & vbCrLf & " has no balance in " & _
            '                           WAREHOUSE_IDComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
            '                        End If
            '                        Exit Sub
            '                    Else

            '                        Try
            '                            SALETableAdapter.SP_GET_PARENT_PRODUCT_ID(tmpProductID, tmpParent)
            '                        Catch ex As Exception
            '                            If Language = "Indonesian" Then
            '                                MsgBox("Produk ini mempunyai lebih dari satu Parent !" & vbCrLf & _
            '                                "Untuk memecah produk silahkan gunakan form Pemecahan Produk !", MsgBoxStyle.Critical, "DMI Retail")
            '                            Else
            '                                MsgBox("This product have more than one parent !" & vbCrLf & _
            '                                "For split this product please use form Split Product !", MsgBoxStyle.Critical, "DMI Retail")
            '                            End If
            '                            dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex - 1)
            '                            Exit Sub
            '                        End Try
            '                        PRODUCTTableAdapter.SP_GET_QUANTITY_GROUPING_PRODUCT(tmpProductID, tmpQtyGrouping)
            '                        tmpQtyProduct = SALETableAdapter.SP_GET_QUANTITY_PRODUCT_WAREHOUSE(tmpParent, tmpWarehouseID)
            '                        SP_SPLIT_PRODUCTTableAdapter.Fill(DS_SALE.SP_SPLIT_PRODUCT, tmpParent)

            '                        Dim tmpAmount As Integer = CInt(dgvSaleDetail.Item("QUANTITY", x).Value) - tmpQuantity

            '                        ''Check All Quantity
            '                        If (tmpQtyProduct * tmpQtyGrouping) + tmpQuantity >= CInt(dgvSaleDetail.Item(2, x).Value) Then

            '                            Dim tmpParameter As Integer = CInt(dgvSaleDetail.Item("QUANTITY", x).Value) - tmpQuantity

            '                            Dim Qty As Integer = 0
            '                            For d As Integer = 0 To tmpParameter - 1

            '                                If Qty < tmpAmount Then
            '                                    ''split product
            '                                    SALETableAdapter.SP_PRODUCT_WAREHOUSE("I", _
            '                                                      0, _
            '                                                      tmpParent, _
            '                                                      tmpWarehouseID, _
            '                                                      1 * (-1), _
            '                                                      USER_ID, _
            '                                                      Now, _
            '                                                      0, _
            '                                                      DateSerial(4000, 12, 31))


            '                                    ''INSERT OR UPDATE QUANTITY TO WAREHOUSE SELECTED
            '                                    For Y As Integer = 0 To SP_SPLIT_PRODUCTBindingSource.Count - 1
            '                                        Dim tmpProductId1 As Integer
            '                                        SP_SPLIT_PRODUCTBindingSource.Position = Y

            '                                        PRODUCTTableAdapter.SP_GET_PRODUCT_ID(SP_SPLIT_PRODUCTBindingSource.Current("PRODUCT_CODE"), tmpProductId1)
            '                                        SALETableAdapter.SP_PRODUCT_WAREHOUSE("I", _
            '                                                                             0, _
            '                                                                             tmpProductId1, _
            '                                                                             tmpWarehouseID, _
            '                                                                             SP_SPLIT_PRODUCTBindingSource.Current("QUANTITY"), _
            '                                                                             USER_ID, _
            '                                                                             Now, _
            '                                                                             0, _
            '                                                                             DateSerial(4000, 12, 31))



            '                                        ''INSERT TABLE PODUCT_SPLIT
            '                                        SALETableAdapter.SP_SPLIT_PROCESS("I", 0, _
            '                                                                               Now, _
            '                                                                               tmpParent, _
            '                                                                               tmpProductId1, _
            '                                                                               1, _
            '                                                                               1, _
            '                                                                               tmpWarehouseID, _
            '                                                                               tmpWarehouseID, _
            '                                                                               "AUTO", _
            '                                                                               USER_ID, _
            '                                                                               Now, _
            '                                                                               0, _
            '                                                                               DateSerial(4000, 12, 31))

            '                                    Next
            '                                    Qty = Qty + tmpQtyGrouping
            '                                End If

            '                            Next

            '                        Else
            '                            If Language = "Indonesian" Then
            '                                MsgBox("Tidak dapat menyimpan produk ini!" & vbCrLf & "Jumlah dari : " & dgvSaleDetail.Item(1, x).Value & vbCrLf & " kurang mencukupi ", MsgBoxStyle.Critical, "DMI Retail")
            '                            Else
            '                                MsgBox("Cannot save this transaction!" & vbCrLf & "Quantity from : " & dgvSaleDetail.Item(1, x).Value & vbCrLf & " no balance ", MsgBoxStyle.Critical, "DMI Retail")
            '                            End If
            '                            Exit Sub
            '                        End If
            '                    End If


            '                Else
            '                    If Language = "Indonesian" Then
            '                        MsgBox("Tidak dapat menyimpan produk ini!" & vbCrLf & dgvSaleDetail.Item(1, x).Value & " tidak terdapat di " & _
            '                             WAREHOUSE_IDComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
            '                    Else
            '                        MsgBox("Cannot save this transaction!" & vbCrLf & dgvSaleDetail.Item(1, x).Value & " has no balance in " & _
            '                            WAREHOUSE_IDComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
            '                    End If
            '                    Exit Sub
            '                End If
            '            Else
            '                If Language = "Indonesian" Then
            '                    MsgBox("Tidak dapat menyimpan produk ini!" & vbCrLf & dgvSaleDetail.Item(1, x).Value & " tidak terdapat di " & _
            '                         WAREHOUSE_IDComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
            '                Else
            '                    MsgBox("Cannot save this transaction!" & vbCrLf & dgvSaleDetail.Item(1, x).Value & " has no balance in " & _
            '                        WAREHOUSE_IDComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
            '                End If
            '                Exit Sub
            '            End If
            '        Else

            '            If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY") < CInt(dgvSaleDetail.Item("QUANTITY", x).Value) Then


            '                If SALETableAdapter.SP_SELECT_PARAMETER("AUTO SPLIT PRODUCT") = "True" Then

            '                    'PRODUCTTableAdapter.SP_GET_PRODUCT_ID(dgvSaleDetail.Item(0, x).Value, tmpProductID)
            '                    tmpSplit = SALETableAdapter.SP_CHECK_CHILD_PRODUCT(tmpProductID)

            '                    If tmpSplit = True Then

            '                        Dim tmpParent As Integer
            '                        Try
            '                            SALETableAdapter.SP_GET_PARENT_PRODUCT_ID(tmpProductID, tmpParent)
            '                        Catch ex As Exception
            '                            If Language = "Indonesian" Then
            '                                MsgBox("Produk ini mempunyai lebih dari satu Parent !" & vbCrLf & _
            '                                "Untuk memecah produk silahkan gunakan form Pemecahan Produk !", MsgBoxStyle.Critical, "DMI Retail")
            '                            Else
            '                                MsgBox("This product have more than one parent !" & vbCrLf & _
            '                                "For split this product please use form Split Product !", MsgBoxStyle.Critical, "DMI Retail")
            '                            End If
            '                            dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex - 1)
            '                            Exit Sub
            '                        End Try

            '                        '' check Parent Product in Warehouse
            '                        Dim ChkWarehouse As Boolean = WAREHOUSETableAdapter.SP_CHECK_PRODUCT_WAREHOUSE(tmpWarehouseID, tmpParent)
            '                        If ChkWarehouse = False Then
            '                            If Language = "Indonesian" Then
            '                                MsgBox("Tidak dapat menyimpan produk ini!" & vbCrLf & "Induk Produk dari : " & dgvSaleDetail.Item(1, x).Value & vbCrLf & " tidak terdapat di " & _
            '                                    WAREHOUSE_IDComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
            '                            Else
            '                                MsgBox("Cannot save this transaction!" & vbCrLf & "Parent Product from : " & dgvSaleDetail.Item(1, x).Value & vbCrLf & " has no balance in " & _
            '                               WAREHOUSE_IDComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
            '                            End If
            '                            Exit Sub
            '                        Else

            '                            Try
            '                                SALETableAdapter.SP_GET_PARENT_PRODUCT_ID(tmpProductID, tmpParent)
            '                            Catch ex As Exception
            '                                If Language = "Indonesian" Then
            '                                    MsgBox("Produk ini mempunyai lebih dari satu Parent !" & vbCrLf & _
            '                                    "Untuk memecah produk silahkan gunakan form Pemecahan Produk !", MsgBoxStyle.Critical, "DMI Retail")
            '                                Else
            '                                    MsgBox("This product have more than one parent !" & vbCrLf & _
            '                                    "For split this product please use form Split Product !", MsgBoxStyle.Critical, "DMI Retail")
            '                                End If
            '                                dgvSaleDetail.Rows.RemoveAt(dgvSaleDetail.CurrentCell.RowIndex - 1)
            '                                Exit Sub
            '                            End Try
            '                            PRODUCTTableAdapter.SP_GET_QUANTITY_GROUPING_PRODUCT(tmpProductID, tmpQtyGrouping)
            '                            tmpQtyProduct = SALETableAdapter.SP_GET_QUANTITY_PRODUCT_WAREHOUSE(tmpParent, tmpWarehouseID)
            '                            SP_SPLIT_PRODUCTTableAdapter.Fill(DS_SALE.SP_SPLIT_PRODUCT, tmpParent)

            '                            Dim tmpAmount As Integer = CInt(dgvSaleDetail.Item("QUANTITY", x).Value) - tmpQuantity

            '                            If (tmpQtyProduct * tmpQtyGrouping) + tmpQuantity >= CInt(dgvSaleDetail.Item(2, x).Value) Then

            '                                Dim tmpParameter As Integer = CInt(dgvSaleDetail.Item("QUANTITY", x).Value) - tmpQuantity

            '                                Dim Qty As Integer = 0
            '                                For d As Integer = 0 To tmpParameter - 1

            '                                    If Qty < tmpAmount Then
            '                                        ''split product
            '                                        SALETableAdapter.SP_PRODUCT_WAREHOUSE("I", _
            '                                                          0, _
            '                                                          tmpParent, _
            '                                                          tmpWarehouseID, _
            '                                                          1 * (-1), _
            '                                                          USER_ID, _
            '                                                          Now, _
            '                                                          0, _
            '                                                          DateSerial(4000, 12, 31))


            '                                        ''INSERT OR UPDATE QUANTITY TO WAREHOUSE SELECTED
            '                                        For Y As Integer = 0 To SP_SPLIT_PRODUCTBindingSource.Count - 1
            '                                            Dim tmpProductId1 As Integer
            '                                            SP_SPLIT_PRODUCTBindingSource.Position = Y

            '                                            PRODUCTTableAdapter.SP_GET_PRODUCT_ID(SP_SPLIT_PRODUCTBindingSource.Current("PRODUCT_CODE"), tmpProductId1)
            '                                            SALETableAdapter.SP_PRODUCT_WAREHOUSE("I", _
            '                                                                                 0, _
            '                                                                                 tmpProductId1, _
            '                                                                                 tmpWarehouseID, _
            '                                                                                 SP_SPLIT_PRODUCTBindingSource.Current("QUANTITY"), _
            '                                                                                 USER_ID, _
            '                                                                                 Now, _
            '                                                                                 0, _
            '                                                                                 DateSerial(4000, 12, 31))



            '                                            'INSERT TABLE PODUCT_SPLIT
            '                                            SALETableAdapter.SP_SPLIT_PROCESS("I", 0, _
            '                                                                                   Now, _
            '                                                                                   tmpParent, _
            '                                                                                   tmpProductId1, _
            '                                                                                   1, _
            '                                                                                   1, _
            '                                                                                   tmpWarehouseID, _
            '                                                                                   tmpWarehouseID, _
            '                                                                                   "AUTO", _
            '                                                                                   USER_ID, _
            '                                                                                   Now, _
            '                                                                                   0, _
            '                                                                                   DateSerial(4000, 12, 31))

            '                                        Next
            '                                        Qty = Qty + tmpQtyGrouping
            '                                    End If

            '                                Next

            '                            Else
            '                                If Language = "Indonesian" Then
            '                                    MsgBox("Tidak dapat menyimpan produk ini!" & vbCrLf & "Jumlah dari : " & dgvSaleDetail.Item(1, x).Value & vbCrLf & " kurang mencukupi ", MsgBoxStyle.Critical, "DMI Retail")
            '                                Else
            '                                    MsgBox("Cannot save this transaction!" & vbCrLf & "Quantity from : " & dgvSaleDetail.Item(1, x).Value & vbCrLf & " no balance ", MsgBoxStyle.Critical, "DMI Retail")
            '                                End If
            '                                Exit Sub
            '                            End If
            '                        End If


            '                    Else
            '                        If Language = "Indonesian" Then
            '                            MsgBox("Tidak dapat menyimpan produk ini!" & vbCrLf & dgvSaleDetail.Item(1, x).Value & " tidak terdapat di " & _
            '                                 WAREHOUSE_IDComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
            '                        Else
            '                            MsgBox("Cannot save this transaction!" & vbCrLf & dgvSaleDetail.Item(1, x).Value & " has no balance in " & _
            '                                WAREHOUSE_IDComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
            '                        End If
            '                        Exit Sub
            '                    End If
            '                Else
            '                    If Language = "Indonesian" Then
            '                        MsgBox("Tidak dapat menyimpan produk ini!" & vbCrLf & dgvSaleDetail.Item(1, x).Value & " tidak terdapat di " & _
            '                             WAREHOUSE_IDComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
            '                    Else
            '                        MsgBox("Cannot save this transaction!" & vbCrLf & dgvSaleDetail.Item(1, x).Value & " has no balance in " & _
            '                            WAREHOUSE_IDComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
            '                    End If
            '                    Exit Sub
            '                End If

            '            End If

            '        End If


        Next

        If txtSaleOrderNo.Text = "" Then
            tmpSaleOrderID = 0
        End If

        SALETableAdapter.SP_SALE("I", _
                                         0, _
                                         RECEIPT_NOTextBox.Text, _
                                         tmpDate, _
                                         tmpCustomerID, _
                                         DOWN_PAYMENTTextBox.Text, _
                                         AMOUNT_DISCOUNTTextBox.Text, _
                                         TAX_AMOUNTTextBox.Text, _
                                         GRAND_TOTALTextBox.Text, _
                                         REMARKTextBox.Text, _
                                         PAYMENT_METHODBindingSource.Current("PAYMENT_METHOD_ID"), _
                                         "", _
                                         mdlGeneral.USER_ID, _
                                         Now, _
                                         0, _
                                         DateSerial(4000, 12, 31), _
                                         WAREHOUSE_IDComboBox.SelectedValue, _
                                         MATURITY_DATEDateTimePicker.Value, _
                                         txtDisc2.Text, _
                                         txtDisc3.Text,
                                         cmbSALESMAN_NAME.SelectedValue, _
                                         tmpSaleOrderID)

        For x As Integer = 0 To dgvSaleDetail.RowCount - 2
            PRODUCTTableAdapter.SP_GET_PRODUCT_ID(dgvSaleDetail.Item(0, x).Value, tmpProductID)
            SALE_DETAILTableAdapter.SP_SALE_DETAIL("I", _
                                                           0, _
                                                           0, _
                                                           RECEIPT_NOTextBox.Text, _
                                                           tmpProductID, _
                                                           CInt(dgvSaleDetail.Item(3, x).Value), _
                                                           CInt(dgvSaleDetail.Item(2, x).Value), _
                                                           CInt(dgvSaleDetail.Item(5, x).Value), _
                                                           CInt(dgvSaleDetail.Item(6, x).Value), _
                                                           mdlGeneral.USER_ID, _
                                                           Now, _
                                                           0, _
                                                           DateSerial(4000, 12, 31))
            If txtSaleOrderNo.Text <> "" Then
                Dim tmpQuantitySaleDetail, tmpQuantitySaleOrder As Integer
                SALETableAdapter.SP_GET_PARAMETER_QUANTITY_SALE_ORDER(tmpSaleOrderID, tmpProductID, tmpQuantitySaleDetail, tmpQuantitySaleOrder)
                If tmpQuantitySaleDetail >= tmpQuantitySaleOrder Then
                    SALETableAdapter.SP_SALE_ORDER_DETAIL("U", _
                                                            tmpSaleOrderID, _
                                                            0, _
                                                            tmpProductID, 0, 0, 0, 0, 0, 0, 0, _
                                                            "Open", _
                                                            "Close", _
                                                            "Close", _
                                                            0, Now, _
                                                            USER_ID, _
                                                            Now)
                Else
                    SALETableAdapter.SP_SALE_ORDER_DETAIL("U", _
                                                            tmpSaleOrderID, _
                                                            0, _
                                                            tmpProductID, 0, 0, 0, 0, 0, 0, 0, _
                                                            "Open", _
                                                            "Open", _
                                                            "Close", _
                                                            0, Now, _
                                                            USER_ID, _
                                                            Now)
                End If
            End If

            PRODUCT_WAREHOUSETableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                               0, _
                                                               tmpProductID, _
                                                               tmpWarehouseID, _
                                                               CInt(dgvSaleDetail.Item(2, x).Value) * (-1), _
                                                               mdlGeneral.USER_ID, _
                                                               Now, _
                                                               0, _
                                                               DateSerial(4000, 12, 31))
        Next

        ''If PAYMENT_METHOD_IDComboBox.Text <> "Cash" Then
        SALETableAdapter.SP_RECEIVABLE("I", _
                                        0, _
                                        0, _
                                        CInt(GRAND_TOTALTextBox.Text), _
                                        MATURITY_DATEDateTimePicker.Value, _
                                        mdlGeneral.USER_ID, _
                                        Now, _
                                        0, _
                                        DateSerial(4000, 12, 31))
        ''End If

        ''''Insert Receivable Payment if Payment method group Credit''''
        SALETableAdapter.SP_GET_SALE_ID(RECEIPT_NOTextBox.Text, tmpReceivableID)
        SALETableAdapter.SP_RECEIVABLE_PAYMENT_GENERATE_NUMBER(tmpReceivablePaymentNo)
        tmptotalpayment = CInt(GRAND_TOTALTextBox.Text) - CInt(DOWN_PAYMENTTextBox.Text)
        If tmpPaymentGroup = "Credit" And CDbl(DOWN_PAYMENTTextBox.Text) > 0 Then
            SALETableAdapter.SP_RECEIVABLE_PAYMENT("I", _
                                                             0, _
                                                             tmpReceivablePaymentNo, _
                                                             tmpReceivableID, _
                                                             tmpDate, _
                                                             CInt(DOWN_PAYMENTTextBox.Text), _
                                                             PAYMENT_METHODBindingSource.Current("PAYMENT_METHOD_ID"), _
                                                             "Auto", _
                                                             tmptotalpayment, _
                                                             mdlGeneral.USER_ID, _
                                                             Now, _
                                                             0, _
                                                             DateSerial(4000, 12, 31))
        End If


        tmpReceiptNoForPrinting = RECEIPT_NOTextBox.Text
        If Language = "Indonesian" Then
            MsgBox("Data Berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
        End If
        tmpstatus = "Success"
        tmpFormClosing = True
        cmdAdd_Click(Nothing, Nothing)
        txtDisc2.Text = "0"
        txtDisc3.Text = "0"
        txtDiscount2.Text = "0.00"
        txtDiscount3.Text = "0.00"
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click

        SALETableAdapter.SP_GENERATE_SALE_RECEIPT_NO(RECEIPT_NOTextBox.Text)
        dgvSaleDetail.Rows.Clear()
        lblNoOfItem.Text = "No of Item(s) :  0"
        REMARKTextBox.Text = ""
        txtSaleOrderNo.Text = ""
        AMOUNT_DISCOUNTTextBox.Text = "0"
        TAXPercentageTEXTBOX.Text = "0.00"
        TAX_AMOUNTTextBox.Text = "0"
        DOWN_PAYMENTTextBox.Text = "0"
        GRAND_TOTALTextBox.Text = "0"
        CUSTOMER_IDComboBox.Enabled = True

        ''Default Parameter Customer
        If SALETableAdapter.SP_SELECT_PARAMETER("SALE CUSTOMER") = "0" Then
            CUSTOMER_IDComboBox.SelectedValue = -1
        Else
            CUSTOMERBindingSource.Position = _
                CUSTOMERBindingSource.Find("CUSTOMER_ID", _
                                           SALETableAdapter.SP_SELECT_PARAMETER("SALE CUSTOMER"))
            CUSTOMER_IDComboBox.Text = CUSTOMERBindingSource.Current("CUSTOMER_NAME")
        End If

        ''Default Parameter Payment Method
        If SALETableAdapter.SP_SELECT_PARAMETER("SALE PAYMENT METHOD") = "0" Then
            PAYMENT_METHOD_IDComboBox.SelectedIndex = -1
        Else
            PAYMENT_METHODBindingSource.Position = _
                PAYMENT_METHODBindingSource.Find("PAYMENT_METHOD_ID", _
                                                    SALETableAdapter.SP_SELECT_PARAMETER("SALE PAYMENT METHOD"))
        End If



        ''Default Parameter Warehouse
        If SALETableAdapter.SP_SELECT_PARAMETER("SALE WAREHOUSE") = "0" Then
            WAREHOUSE_IDComboBox.SelectedIndex = -1
        Else
            WAREHOUSEBindingSource.Position = _
                WAREHOUSEBindingSource.Find("WAREHOUSE_ID", _
                                            SALETableAdapter.SP_SELECT_PARAMETER("SALE WAREHOUSE"))
        End If


        dgvSaleDetail.Focus()
        tmpChange = False
        tmpFormClosing = False
    End Sub

    Private Sub TAX_AMOUNTTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""

    End Sub

    Private Sub TAX_AMOUNTTextBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        TAX_AMOUNTTextBox.Text = IIf(TAX_AMOUNTTextBox.Text = "", "0", TAX_AMOUNTTextBox.Text)
        Dim tmpGrandTotal As Integer = 0
        For x As Integer = 0 To dgvSaleDetail.RowCount - 1
            tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvSaleDetail.Item(6, x).Value)
        Next
        If TAX_AMOUNTTextBox.Text = "0" Then
            TAXPercentageTEXTBOX.Text = "0.00"
        Else
            TAXPercentageTEXTBOX.Text = FormatNumber((TAX_AMOUNTTextBox.Text / tmpGrandTotal) * 100)
        End If
    End Sub

    Private Sub txtDisc2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDisc2.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Asc(e.KeyChar) < Asc("0") Or Asc(e.KeyChar) > Asc("9") Then
            e.KeyChar = ""
        End If
    End Sub

    'Private Sub txtDisc2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDisc2.LostFocus
    '    If AMOUNT_DISCOUNTTextBox.Text <> "0" Then
    '        Dim tmpGrandTotal As Integer = 0
    '        For x As Integer = 0 To dgvSaleDetail.RowCount - 1
    '            tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvSaleDetail.Item(6, x).Value)
    '        Next
    '        If txtDisc2.Text = "0" Then
    '            txtDiscount2.Text = "0.00"
    '        Else
    '            txtDiscount2.Text = FormatNumber((txtDisc2.Text / tmpGrandTotal) * 100)
    '        End If
    '        tmpChange = True
    '    Else
    '        txtDisc2.Text = "0"
    '    End If
    'End Sub

    Private Sub txtDiscount2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDiscount2.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Asc(e.KeyChar) < Asc("0") Or Asc(e.KeyChar) > Asc("9") Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub txtDiscount2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDiscount2.LostFocus
        If GRAND_TOTALTextBox.Text <> "0" Then
            Dim tmpGrandTotal As Integer = 0
            For x As Integer = 0 To dgvSaleDetail.RowCount - 1
                tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvSaleDetail.Item(6, x).Value)
            Next
            txtDiscount2.Text = IIf(txtDiscount2.Text = "", "0", txtDiscount2.Text)
            txtDisc2.Text = FormatNumber((CInt(txtDiscount2.Text) / 100) * tmpGrandTotal, 0)
        Else
            txtDiscount2.Text = "0.00"
        End If
    End Sub

    Private Sub txtDisc2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDisc2.TextChanged
        If GRAND_TOTALTextBox.Text <> "0" Then

            Dim tmpGrandTotal As Integer = 0
            For x As Integer = 0 To dgvSaleDetail.RowCount - 1
                tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvSaleDetail.Item(6, x).Value)
            Next
            If txtDisc2.Text = "" Then
                txtDisc2.Text = "0"
            End If

            If txtDisc2.Text = "0" Then
                txtDiscount2.Text = "0.00"
            Else
                txtDiscount2.Text = FormatNumber((txtDisc2.Text / tmpGrandTotal) * 100)
            End If

            CalculationAllSale(dgvSaleDetail, _
                  5, _
                  6, _
                  AMOUNT_DISCOUNTTextBox, _
                  TAXPercentageTEXTBOX, _
                  TAX_AMOUNTTextBox, _
                  DOWN_PAYMENTTextBox, _
                  txtDiscount2, _
                  txtDiscount3, _
                  txtDisc2, _
                  txtDisc3, _
                  GRAND_TOTALTextBox)
        Else
            txtDisc2.Text = "0"
        End If

        If Val(txtDiscount2.Text) > 100 Then
            'txtDiscount2.Text = "0.00"
            'txtDisc2.Text = "0"
            txtDiscount2.Focus()
            Exit Sub
        End If

        tmpChange = True
    End Sub


    Private Sub txtDiscount2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDiscount2.TextChanged
        If GRAND_TOTALTextBox.Text = "0" Then
            If txtDisc2.Text = "0" Then
                txtDiscount2.Text = "0.00"
            End If
        End If
    End Sub

    Private Sub txtDisc3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDisc3.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Asc(e.KeyChar) < Asc("0") Or Asc(e.KeyChar) > Asc("9") Then
            e.KeyChar = ""
        End If
    End Sub
    'Private Sub txtDisc3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDisc3.LostFocus
    '    If txtDisc2.Text <> "0" Then
    '        Dim tmpGrandTotal As Integer = 0
    '        For x As Integer = 0 To dgvSaleDetail.RowCount - 1
    '            tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvSaleDetail.Item(6, x).Value)
    '        Next
    '        If txtDisc3.Text = "0" Then
    '            txtDiscount3.Text = "0.00"
    '        Else
    '            txtDiscount3.Text = FormatNumber((txtDisc3.Text / (tmpGrandTotal - CInt(txtDisc2.Text))) * 100, 2)
    '        End If
    '        tmpChange = True
    '    Else
    '        txtDisc3.Text = "0"
    '    End If
    'End Sub

    Private Sub txtDisc3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDisc3.TextChanged
        If txtDisc2.Text <> "0" Then
            Dim tmpGrandTotal As Integer = 0


            tmpGrandTotal = 0
            For x As Integer = 0 To dgvSaleDetail.RowCount - 1
                tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvSaleDetail.Item(6, x).Value)
            Next

            If txtDisc3.Text = "" Then
                txtDisc3.Text = "0"
            End If
            If txtDisc3.Text = "0" Then
                txtDiscount3.Text = "0.00"
            Else
                txtDiscount3.Text = FormatNumber((txtDisc3.Text / (tmpGrandTotal - CInt(txtDisc2.Text))) * 100, 2)
            End If
            tmpChange = True



            CalculationAllSale(dgvSaleDetail, _
                5, _
                6, _
                AMOUNT_DISCOUNTTextBox, _
                TAXPercentageTEXTBOX, _
                TAX_AMOUNTTextBox, _
                DOWN_PAYMENTTextBox, _
                txtDiscount2, _
                txtDiscount3, _
                txtDisc2, _
                txtDisc3, _
                GRAND_TOTALTextBox)
        Else
            txtDisc3.Text = "0"

        End If
        If Val(txtDiscount3.Text) > 100 Then
            'txtDiscount3.Text = "0.00"
            'txtDisc3.Text = "0"
            txtDiscount3.Focus()
        End If
        tmpChange = True
    End Sub

    Private Sub txtDiscount3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDiscount3.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Asc(e.KeyChar) < Asc("0") Or Asc(e.KeyChar) > Asc("9") Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub txtDiscount3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDiscount3.LostFocus
        If txtDisc2.Text <> "0" Then
            Dim tmpGrandTotal As Integer = 0
            For x As Integer = 0 To dgvSaleDetail.RowCount - 1
                tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvSaleDetail.Item(6, x).Value)
            Next
            txtDiscount3.Text = IIf(txtDiscount3.Text = "", "0", txtDiscount3.Text)
            txtDisc3.Text = FormatNumber((CInt(txtDiscount3.Text) / 100) * (tmpGrandTotal - CInt(txtDisc2.Text)), 0)
        Else
            txtDiscount3.Text = "0.00"
        End If
    End Sub

    Private Sub txtDiscount3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDiscount3.TextChanged
        If txtDisc2.Text = "0" Then
            If txtDisc2.Text = "0" Then
                txtDiscount3.Text = "0.00"
            End If
        End If
    End Sub

    Private Sub TAX_AMOUNTTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If GRAND_TOTALTextBox.Text = "0" Then
            TAX_AMOUNTTextBox.Text = "0"
        Else
            CalculationAllSale(dgvSaleDetail, _
                         5, _
                         6, _
                         AMOUNT_DISCOUNTTextBox, _
                         TAXPercentageTEXTBOX, _
                         TAX_AMOUNTTextBox, _
                         DOWN_PAYMENTTextBox, _
                         txtDiscount2, _
                         txtDiscount3, _
                         txtDisc2, _
                         txtDisc3, _
                         GRAND_TOTALTextBox)
            lblNoOfItem.Text = "No of Item(s) : " & dgvSaleDetail.RowCount - 1
        End If
        tmpChange = True
    End Sub

    Private Sub DOWN_PAYMENTTextBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles DOWN_PAYMENTTextBox.LostFocus
        Try
            DOWN_PAYMENTTextBox.Text = FormatNumber(DOWN_PAYMENTTextBox.Text, 0)
        Catch
        End Try
    End Sub

    Private Sub DOWN_PAYMENTTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DOWN_PAYMENTTextBox.TextChanged
        If GRAND_TOTALTextBox.Text = "" Then
            lblChange.Text = ""
            Exit Sub
        End If

        If DOWN_PAYMENTTextBox.Text = "" Then
            DOWN_PAYMENTTextBox.Text = "0"
        End If

        If tmpDataGridViewLoaded Then _
            lblChange.Text = FormatNumber(IIf(DOWN_PAYMENTTextBox.Text.Length = 0, 0, DOWN_PAYMENTTextBox.Text) - _
                             GRAND_TOTALTextBox.Text, 0)
        tmpChange = True
    End Sub

    Private Sub GRAND_TOTALTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GRAND_TOTALTextBox.TextChanged
        If GRAND_TOTALTextBox.Text = "" Then
            'lblChange.Text = ""
            'Exit Sub
            Dim tmpTotal As Integer
            For x As Integer = 0 To dgvSaleDetail.RowCount - 1
                tmpTotal = tmpTotal + dgvSaleDetail.Item("TOTAL", x).Value
            Next
            GRAND_TOTALTextBox.Text = FormatNumber(CInt(tmpTotal) + CInt(TAX_AMOUNTTextBox.Text), 0)
        End If

        If Val(GRAND_TOTALTextBox.Text) < 0 Then
            txtDisc2.Text = "0"
            txtDiscount2.Text = "0,00"
            txtDisc3.Text = "0"
            txtDiscount3.Text = "0,00"
        End If

        If tmpDataGridViewLoaded Then _
            lblChange.Text = FormatNumber(IIf(DOWN_PAYMENTTextBox.Text.Length = 0, 0, DOWN_PAYMENTTextBox.Text) - _
                             GRAND_TOTALTextBox.Text, 0)
        tmpChange = True
    End Sub

    Private Sub cmdPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrint.Click
        If Trim(RsAccessPrivilege.Item("ALLOW_PRINT").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If tmpSaveMode = "Update" Then
            SP_SALE_INVOICETableAdapter.Fill(DS_SALE.SP_SALE_INVOICE, tmpReceiptNoForPrinting, USER_ID)
            rpvSaleInvoice.RefreshReport()
        Else
            cmdSave_Click(Nothing, Nothing)

            If tmpstatus = "Success" Then
                SP_SALE_INVOICETableAdapter.Fill(DS_SALE.SP_SALE_INVOICE, tmpReceiptNoForPrinting, USER_ID)
                rpvSaleInvoice.RefreshReport()
            Else
                If Language = "Indonesian" Then
                    MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If
        End If

    End Sub

    Private Sub rpvSaleInvoice_RenderingComplete(ByVal sender As Object, ByVal e As Microsoft.Reporting.WinForms.RenderingCompleteEventArgs) Handles rpvSaleInvoice.RenderingComplete
        rpvSaleInvoice.PrintDialog()
    End Sub

    Private Sub RECEIPT_NOTextBox_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles RECEIPT_NOTextBox.KeyDown
        If e.KeyCode = Keys.Delete Then
            e.Handled = True
        End If
    End Sub

    Private Sub RECEIPT_NOTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles RECEIPT_NOTextBox.KeyPress
        If Asc(e.KeyChar) = 177 Then
            e.KeyChar = ""
        Else
            e.KeyChar = ""
        End If

    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If RECEIPT_NOTextBox.Text = "" Then Exit Sub

        If Trim(RsAccessPrivilege.Item("ALLOW_DELETE").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        Dim tmpCheck As String = ""
        SALETableAdapter.SP_CHECK_CLOSING_PERIOD(SALE_DATEDateTimePicker.Value, tmpCheck)
        If tmpCheck = "TRUE" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menghapus transaksi ini !" & vbCrLf & "Transaksi ini telah masuk dalam proses Tutup Buku", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Delete this Transaction!" & vbCrLf & "This Transaction has been in Closed Period", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If


        If Language = "Indonesian" Then
            If MsgBox("Batalkan transaksi ini?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = _
                MsgBoxResult.No Then Exit Sub
        Else
            If MsgBox("Void this transaction?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = _
               MsgBoxResult.No Then Exit Sub
        End If


        Dim tmpPaymentGroup As String = ""
        ''''Get Payment Group''''
        SALETableAdapter.SP_GET_PAYMENT_GROUP(PAYMENT_METHODBindingSource.Current("PAYMENT_METHOD_ID"), tmpPaymentGroup)

        Dim tmpSaleDateNew As DateTime = SALE_DATEDateTimePicker.Value
        Dim tmpPaymentDate As DateTime
        Dim tmpSaleIdNew As Integer = tmpSaleId
        ''''check payment group''''
        If tmpPaymentGroup = "Credit" Then
            ''''get payment date''''
            tmpPaymentDate = SALETableAdapter.SP_CHECK_TRANSAKSION_RECEIVABLE(tmpSaleIdNew)
            ''''check sale date & payment date''''
            If tmpPaymentDate <> tmpSaleDateNew Then
                If Language = "Indonesian" Then
                    MsgBox("Tidak dapat menghapus transaksi ini !" & vbCrLf & "Sudah ada pembayaran piutang lain", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Cannot delete this transaction !" & vbCrLf & "There have been other accounts receivable payments", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If

        End If

        If txtSaleOrderNo.Text = "" Then
            tmpSaleOrderID = 0
        Else
            SALETableAdapter.SP_SALE_ORDER_DETAIL("D", _
                                                    0, _
                                                    0, _
                                                    0, _
                                                    0, _
                                                    0, _
                                                    0, _
                                                    0, _
                                                    0, _
                                                    0, _
                                                    0, _
                                                    "", _
                                                    "", _
                                                    "", _
                                                    USER_ID, _
                                                    Now, _
                                                    USER_ID, _
                                                    Now)
        End If

        SALETableAdapter.SP_SALE("V", _
                                 0, _
                                 RECEIPT_NOTextBox.Text, _
                                 SALE_DATEDateTimePicker.Value, _
                                 0, _
                                 0, _
                                 0, _
                                 0, _
                                 0, _
                                 "", _
                                 0, _
                                 "", _
                                 0, _
                                 DateSerial(4000, 12, 31), _
                                 USER_ID, _
                                 Now,
                                 0, _
                                 MATURITY_DATEDateTimePicker.Value, _
                                 0, _
                                 0,
                                 cmbSALESMAN_NAME.SelectedValue, _
                                 tmpSaleOrderID)

        For x As Integer = 0 To dgvSaleDetail.RowCount - 1
            PRODUCTTableAdapter.SP_GET_PRODUCT_ID(dgvSaleDetail.Item(0, x).Value, tmpProductID)
            PRODUCT_WAREHOUSETableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                   0, _
                                                   tmpProductID, _
                                                   tmpWarehouse, _
                                                   CInt(dgvSaleDetail.Item(2, x).Value), _
                                                   mdlGeneral.USER_ID, _
                                                   Now, _
                                                   0, _
                                                   DateSerial(4000, 12, 31))
        Next

        If Language = "Indonesian" Then
            MsgBox("Transaksi ini telah di batalkan", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("This Transaction has been Voided", MsgBoxStyle.Information, "DMI Retail")
        End If
        Me.Close()
    End Sub

    Private Sub AMOUNT_DISCOUNTTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AMOUNT_DISCOUNTTextBox.TextChanged
        If AMOUNT_DISCOUNTTextBox.Text = "" Then
            Dim tmpDisc As Integer

            For x As Integer = 0 To dgvSaleDetail.RowCount - 1
                tmpDisc = tmpDisc + dgvSaleDetail.Item("DISCOUNT2", x).Value

            Next

            AMOUNT_DISCOUNTTextBox.Text = FormatNumber(CInt(tmpDisc), 0)
        End If
        tmpChange = True
    End Sub

    Private Sub cmdSearchName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchName.Click
        If tmpSaveMode = "Update" Then Exit Sub
        mdlGeneral.tmpSearchMode = "Customer - Customer Name"
        Dim frmSearchCustomer As New frmSearchCustomer
        frmSearchCustomer.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        CUSTOMERBindingSource.Position = CUSTOMERBindingSource.Find("CUSTOMER_ID", tmpSearchResult)
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        tmpChange = False
    End Sub

    Private Sub RECEIPT_NOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RECEIPT_NOTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub SALE_DATEDateTimePicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SALE_DATEDateTimePicker.ValueChanged
        tmpChange = True
    End Sub

    Private Sub CUSTOMER_IDComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CUSTOMER_IDComboBox.SelectedIndexChanged
        tmpChange = True
    End Sub

    Private Sub PAYMENT_METHOD_IDComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PAYMENT_METHOD_IDComboBox.SelectedIndexChanged
        tmpChange = True
    End Sub

    Private Sub REMARKTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles REMARKTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub TAXPercentageTEXTBOX_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If GRAND_TOTALTextBox.Text = "0" Then
            If TAX_AMOUNTTextBox.Text = "0" Then
                TAXPercentageTEXTBOX.Text = "0.00"
            End If
        End If
        tmpChange = True
    End Sub

    Private Sub WAREHOUSE_IDComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WAREHOUSE_IDComboBox.SelectedIndexChanged
        tmpChange = True
    End Sub

    Private Sub txtDTP_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDTP.KeyDown
        If e.KeyCode = Keys.Delete Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtDTP_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDTP.KeyPress
        e.KeyChar = ""
    End Sub

    Private Sub CUSTOMERBindingSource_CurrentChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CUSTOMERBindingSource.CurrentChanged
        If tmpFormClosing Then Exit Sub
        If CUSTOMER_IDComboBox.Text <> "" Then
            Dim tmpCustomer_id As Integer = CUSTOMERBindingSource.Current("CUSTOMER_ID")
            If IsDBNull(CUSTOMERBindingSource.Current("DISCOUNT")) Then
                tmpDiscount = 0
            Else
                SALETableAdapter.SP_GET_DISCOUNT_CUSTOMER(tmpCustomer_id, tmpDiscount)
            End If
        Else
            tmpDiscount = 0
        End If
        For y As Integer = 0 To dgvSaleDetail.RowCount - 2
            dgvSaleDetail.Item("DISCOUNT", y).Value = tmpDiscount
            dgvSaleDetail.Item("DISCOUNT2", y).Value = dgvSaleDetail.Item("PRICE_PER_UNIT", y).Value * tmpDiscount / 100
            dgvSaleDetail.Item("TOTAL", y).Value = dgvSaleDetail.Item("PRICE_PER_UNIT", y).Value - dgvSaleDetail.Item("DISCOUNT2", y).Value
        Next

    End Sub


    'Private Sub WAREHOUSEBindingSource_CurrentChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles WAREHOUSEBindingSource.CurrentChanged
    '    If WAREHOUSE_IDComboBox.Text <> "" Then
    '        If WAREHOUSEBindingSource.Current("WAREHOUSE_ID") = -1 Then
    '            If Language = "Indonesian" Then
    '                MsgBox("Silahkan pilih gudang lain ." & vbCrLf & _
    '               "Ini adalah Gudang barang rusak !", MsgBoxStyle.Critical, "DMI Retail")
    '            Else
    '                MsgBox("Please enter other warehouse ." & vbCrLf & _
    '                    "This Warehouse is Rummage !", MsgBoxStyle.Critical, "DMI Retail")
    '            End If
    '            WAREHOUSEBindingSource.Position = 0
    '            Exit Sub
    '        End If
    '    End If
    'End Sub

    Private Sub CUSTOMER_IDComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CUSTOMER_IDComboBox.TextChanged
        If tmpFormClosing Then Exit Sub

        If CUSTOMER_IDComboBox.Text <> "" Then
            Dim tmpCustomer_id As Integer = CUSTOMERBindingSource.Current("CUSTOMER_ID")
            If IsDBNull(CUSTOMERBindingSource.Current("DISCOUNT")) Then
                tmpDiscount = 0
            Else
                SALETableAdapter.SP_GET_DISCOUNT_CUSTOMER(tmpCustomer_id, tmpDiscount)
            End If
        Else
            tmpDiscount = 0
        End If

        For y As Integer = 0 To dgvSaleDetail.RowCount - 2
            dgvSaleDetail.Item("DISCOUNT", y).Value = tmpDiscount
            dgvSaleDetail.Item("DISCOUNT2", y).Value = dgvSaleDetail.Item("PRICE_PER_UNIT", y).Value * tmpDiscount / 100
            dgvSaleDetail.Item("TOTAL", y).Value = dgvSaleDetail.Item("PRICE_PER_UNIT", y).Value - dgvSaleDetail.Item("DISCOUNT2", y).Value
        Next

    End Sub

    Private Sub CalculationAllSale(ByRef pDataGridView As DataGridView, _
                             ByVal pDiscCol As Integer, _
                             ByVal pTotalCol As Integer, _
                             ByRef pDiscountTextBox As TextBox, _
                             ByRef pTaxPercentageTextBox As TextBox, _
                             ByRef pTaxTextBox As TextBox, _
                             ByRef pDPTextBox As TextBox, _
                             ByRef pDiscount2 As TextBox, _
                             ByRef pDiscount3 As TextBox, _
                             ByRef pDisc2textBox As TextBox, _
                             ByRef pDisc3textBox As TextBox, _
                             ByRef pGrandTotalTextBox As TextBox)

        Dim tmpDiscTotal, tmpTotal As Integer

        'pDiscountTextBox.Text = "0"
        pTaxPercentageTextBox.Text = FormatNumber(IIf(pTaxPercentageTextBox.Text = "", "0.00", _
                                                      pTaxPercentageTextBox.Text), 2)
        'pTaxTextBox.Text = FormatNumber(CInt(IIf(pTaxTextBox.Text = "", "0", pTaxTextBox.Text)), 0)
        'pDPTextBox.Text = FormatNumber(CInt(IIf(pDPTextBox.Text = "", "0", pDPTextBox.Text)), 0)
        'pGrandTotalTextBox.Text = "0"

        For x As Integer = 0 To pDataGridView.RowCount - 1
            tmpDiscTotal = tmpDiscTotal + pDataGridView.Item(pDiscCol, x).Value
            tmpTotal = tmpTotal + pDataGridView.Item(pTotalCol, x).Value
        Next

        If pTaxTextBox.Text = "" Then pTaxTextBox.Text = "0"
        If pDisc2textBox.Text = "" Then pDisc2textBox.Text = "0"
        If pDisc3textBox.Text = "" Then pDisc3textBox.Text = "0"
        'pTaxTextBox.Text = (tmpTotal * pTaxPercentageTextBox.Text) / 100
        pDiscountTextBox.Text = FormatNumber(CInt(tmpDiscTotal), 0)
        pGrandTotalTextBox.Text = FormatNumber(CInt(tmpTotal + CInt(pTaxTextBox.Text) - CInt(pDisc2textBox.Text) - CInt(pDisc3textBox.Text)), 0)
    End Sub

    Private Sub TAX_AMOUNTTextBox_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TAX_AMOUNTTextBox.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""

    End Sub

    Private Sub TAX_AMOUNTTextBox_LostFocus1(ByVal sender As Object, ByVal e As System.EventArgs) Handles TAX_AMOUNTTextBox.LostFocus
        TAX_AMOUNTTextBox.Text = IIf(TAX_AMOUNTTextBox.Text = "", "0", TAX_AMOUNTTextBox.Text)
        Dim tmpGrandTotal As Integer = 0
        For x As Integer = 0 To dgvSaleDetail.RowCount - 1
            tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvSaleDetail.Item(6, x).Value)
        Next
        If TAX_AMOUNTTextBox.Text = "0" Then
            TAXPercentageTEXTBOX.Text = "0.00"
        Else
            TAXPercentageTEXTBOX.Text = FormatNumber((TAX_AMOUNTTextBox.Text / tmpGrandTotal) * 100)
        End If
    End Sub


    Private Sub TAX_AMOUNTTextBox_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TAX_AMOUNTTextBox.TextChanged
        If GRAND_TOTALTextBox.Text = "0" Then
            TAX_AMOUNTTextBox.Text = "0"
        Else
            CalculationAllSale(dgvSaleDetail, _
                         5, _
                         6, _
                         AMOUNT_DISCOUNTTextBox, _
                         TAXPercentageTEXTBOX, _
                         TAX_AMOUNTTextBox, _
                         DOWN_PAYMENTTextBox, _
                         txtDiscount2, _
                         txtDiscount3, _
                         txtDisc2, _
                         txtDisc3, _
                         GRAND_TOTALTextBox)
            lblNoOfItem.Text = "No of Item(s) : " & dgvSaleDetail.RowCount - 1
        End If
        tmpChange = True
    End Sub

    Private Sub TAXPercentageTEXTBOX_LostFocus1(ByVal sender As Object, ByVal e As System.EventArgs) Handles TAXPercentageTEXTBOX.LostFocus
        If GRAND_TOTALTextBox.Text <> "0" Then
            Dim tmpGrandTotal As Integer = 0
            For x As Integer = 0 To dgvSaleDetail.RowCount - 1
                tmpGrandTotal = CInt(tmpGrandTotal) + CInt(dgvSaleDetail.Item(6, x).Value)
            Next
            TAXPercentageTEXTBOX.Text = IIf(TAXPercentageTEXTBOX.Text = "", "0", TAXPercentageTEXTBOX.Text)
            TAX_AMOUNTTextBox.Text = FormatNumber((CInt(TAXPercentageTEXTBOX.Text) / 100) * tmpGrandTotal, 0)
        Else
            TAXPercentageTEXTBOX.Text = "0.00"
        End If

    End Sub

    Private Sub TAXPercentageTEXTBOX_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TAXPercentageTEXTBOX.TextChanged
        If GRAND_TOTALTextBox.Text = "0" Then
            If TAX_AMOUNTTextBox.Text = "0" Then
                TAXPercentageTEXTBOX.Text = "0.00"
            End If
        End If
        tmpChange = True
    End Sub

    ''''' show up form search sale order ''''''
    Private Sub cmdSearchSaleOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchSaleOrder.Click
        Dim popSearchSaleOrder As New frmSearchSaleOrder
        dgvSaleDetail.Rows.Clear()
        popSearchSaleOrder.ShowDialog()
        Try
            CUSTOMERBindingSource.Position = CUSTOMERBindingSource.Find("CUSTOMER_NAME", popSearchSaleOrder.tmpCustomerName)
            SALESMANBindingSource.Position = SALESMANBindingSource.Find("SALESMAN_NAME", popSearchSaleOrder.tmpSalesmanName)
            cmbSALESMAN_NAME.Text = popSearchSaleOrder.tmpSalesmanName
            CUSTOMER_IDComboBox.Text = popSearchSaleOrder.tmpCustomerName
            txtSaleOrderNo.Text = popSearchSaleOrder.tmpSaleOrderNo
            dtpSaleOrderDate.Value = popSearchSaleOrder.tmpSaleOrderDate
            tmpSaleOrderID = popSearchSaleOrder.SP_SELECT_SALE_ORDER_HEADERBindingSource.Current("SALE_ORDER_ID")
            tmpTotItem = popSearchSaleOrder.SP_SELECT_SALE_ORDER_HEADERBindingSource.Current("NO_OF_ITEM")
        Catch
            dgvSaleDetail.Rows.Clear()
            txtSaleOrderNo.Text = ""
            dtpSaleOrderDate.Value = Now
            Exit Sub
        End Try
        For X As Integer = 0 To tmpTotItem - 1
            tmpFillingDGV = True
            Me.SP_SELECT_SALE_ORDER_DETAILTableAdapter.Fill(DS_SALE1.SP_SELECT_SALE_ORDER_DETAIL, tmpSaleOrderID)
            SP_SELECT_SALE_ORDER_DETAILBindingSource.Position = X

            dgvSaleDetail.Rows.Add()
            AddDataToGridOrder(dgvSaleDetail, _
                               SP_SELECT_SALE_ORDER_DETAILBindingSource.Current("PRODUCT_CODE"), _
                               0, _
                               SP_SELECT_SALE_ORDER_DETAILBindingSource.Current("PRODUCT_NAME"), _
                               1, _
                               SP_SELECT_SALE_ORDER_DETAILBindingSource.Current("QUANTITY"), _
                               2, _
                               SP_SELECT_SALE_ORDER_DETAILBindingSource.Current("PRICE_PER_UNIT"), _
                               3, _
                               SP_SELECT_SALE_ORDER_DETAILBindingSource.Current("DISC_PERCENTAGE"), _
                               4, _
                               SP_SELECT_SALE_ORDER_DETAILBindingSource.Current("DISC_AMOUNT"), _
                               5, _
                               6)
            CalculationAll(dgvSaleDetail, _
                                   5, _
                                   6, _
                                   AMOUNT_DISCOUNTTextBox, _
                                   TAXPercentageTEXTBOX, _
                                   TAX_AMOUNTTextBox, _
                                   DOWN_PAYMENTTextBox, _
                                   GRAND_TOTALTextBox)
        Next




    End Sub

    Private Sub AddDataToGridOrder(ByRef pDataGridView As DataGridView, _
                            ByVal pCode As String, ByVal pCodeCol As Integer, _
                            ByVal pName As String, ByVal pNameCol As Integer, _
                            ByVal pQuantity As String, ByVal pQuantityCol As Integer, _
                            ByVal pUnitPrice As String, ByVal pUnitPriceCol As Integer, _
                            ByVal pDiscPer As Integer, ByVal pDiscPerCol As Integer, _
                            ByVal pDiscAmount As Integer, ByVal pDiscAmountCol As Integer, _
                            ByVal pTotalCol As Integer)

        If pDataGridView.RowCount > 2 Then
            For x As Integer = 0 To pDataGridView.RowCount - 3
                If pDataGridView.Item(pCodeCol, x).Value.ToString.ToUpper = pCode.ToUpper Then
                    tmpFillingDGV = True
                    If tmpQuantity = pDataGridView.Item(pQuantityCol, x).Value Then
                        MsgBox("Selling a Product with quantity exceed the balance is not allowed." & vbCrLf & _
                               "Adding this Product is cancelled!", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        pDataGridView.Item(pQuantityCol, x).Value = _
                            CInt(pDataGridView.Item(pQuantityCol, x).Value) + 1
                    End If
                    pDataGridView.Rows.RemoveAt(pDataGridView.RowCount - 2)
                    Calculation(pDataGridView.Item(pQuantityCol, x).Value, _
                                pDataGridView.Item(pUnitPriceCol, x).Value, _
                                pDataGridView.Item(pDiscAmountCol, x).Value, _
                                pDataGridView.Item(pTotalCol, x).Value)
                    tmpFillingDGV = False
                    pDataGridView.CurrentCell = _
                        pDataGridView.Rows(pDataGridView.RowCount - 1).Cells(0)
                    Exit Sub
                End If
            Next
            For y As Integer = 0 To pDataGridView.RowCount - 2
                pDataGridView.Item(0, y).ReadOnly = True
            Next
        End If

        If pDataGridView.RowCount = 1 Then pDataGridView.Rows.Add(1)
        pDataGridView.Item(pCodeCol, pDataGridView.RowCount - 2).Value = pCode
        pDataGridView.Item(pNameCol, pDataGridView.RowCount - 2).Value = pName
        pDataGridView.Item(pQuantityCol, pDataGridView.RowCount - 2).Value = CInt(pQuantity)
        pDataGridView.Item(pUnitPriceCol, pDataGridView.RowCount - 2).Value = CInt(pUnitPrice)
        pDataGridView.Item(pDiscPerCol, pDataGridView.RowCount - 2).Value = CInt(pDiscPer)
        pDataGridView.Item(pDiscAmountCol, pDataGridView.RowCount - 2).Value = CInt(pDiscAmount)
        pDataGridView.Item(pTotalCol, pDataGridView.RowCount - 2).Value = _
            (CInt(pDataGridView.Item(pQuantityCol, pDataGridView.RowCount - 2).Value) * _
            CInt(pDataGridView.Item(pUnitPriceCol, pDataGridView.RowCount - 2).Value)) - _
            CInt(pDataGridView.Item(pDiscAmountCol, pDataGridView.RowCount - 2).Value)
        tmpFillingDGV = False

        pDataGridView.CurrentCell = pDataGridView.Rows(pDataGridView.RowCount - 1).Cells(0)

    End Sub


    'Private Sub txtSaleOrderNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSaleOrderNo.TextChanged

    '    'For X As Integer = 0 To tmpTotItem - 1
    '    '    tmpFillingDGV = True
    '    '    Me.SP_SELECT_SALE_ORDER_DETAILTableAdapter.Fill(DS_SALE.SP_SELECT_SALE_ORDER_DETAIL, tmpSaleOrderID)
    '    '    SP_SELECT_SALE_ORDER_DETAILBindingSource.Position = X
    '    '    AddDataToGridOrder(dgvSaleDetail, _
    '    '                       SP_SELECT_SALE_ORDER_DETAILBindingSource.Current("PRODUCT_CODE"), _
    '    '                       0, _
    '    '                       SP_SELECT_SALE_ORDER_DETAILBindingSource.Current("PRODUCT_NAME"), _
    '    '                       1, _
    '    '                       SP_SELECT_SALE_ORDER_DETAILBindingSource.Current("QUANTITY"), _
    '    '                       2, _
    '    '                       SP_SELECT_SALE_ORDER_DETAILBindingSource.Current("PRICE_PER_UNIT"), _
    '    '                       3, _
    '    '                       SP_SELECT_SALE_ORDER_DETAILBindingSource.Current("DISC_PERCENTAGE"), _
    '    '                       4, _
    '    '                       SP_SELECT_SALE_ORDER_DETAILBindingSource.Current("DISC_AMOUNT"), _
    '    '                       5, _
    '    '                       6)
    '    '    '    '    dgvSaleDetail.Item("TOTAL", X).Value = SP_SELECT_SALE_ORDER_DETAILBindingSource.Current("TOTAL")

    '    'Next
    '    'For X As Integer = 0 To tmpTotItem - 1
    '    '    Me.SP_SELECT_SALE_ORDER_DETAILTableAdapter.Fill(DS_SALE.SP_SELECT_SALE_ORDER_DETAIL, tmpSaleOrderID)
    '    '    dgvSaleDetail.Rows.Add()
    '    '    SP_SELECT_SALE_ORDER_DETAILBindingSource.Position = X
    '    '    dgvSaleDetail.Item(0, X).Value = SP_SELECT_SALE_ORDER_DETAILBindingSource.Current("PRODUCT_CODE")
    '    '    dgvSaleDetail.Item(1, X).Value = SP_SELECT_SALE_ORDER_DETAILBindingSource.Current("PRODUCT_NAME")
    '    '    dgvSaleDetail.Item(2, X).Value = SP_SELECT_SALE_ORDER_DETAILBindingSource.Current("QUANTITY")
    '    '    dgvSaleDetail.Item(3, X).Value = SP_SELECT_SALE_ORDER_DETAILBindingSource.Current("PRICE_PER_UNIT")
    '    '    dgvSaleDetail.Item(4, X).Value = SP_SELECT_SALE_ORDER_DETAILBindingSource.Current("DISC_PERCENTAGE")
    '    '    dgvSaleDetail.Item(5, X).Value = SP_SELECT_SALE_ORDER_DETAILBindingSource.Current("DISC_AMOUNT")
    '    '    dgvSaleDetail.Item("TOTAL", X).Value = SP_SELECT_SALE_ORDER_DETAILBindingSource.Current("TOTAL")

    '    'Next
    'End Sub


End Class
