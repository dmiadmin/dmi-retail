﻿Imports Microsoft.Win32
Imports System.Data.SqlClient
Imports System.Data
Imports System.IO

Module mdlGeneral
    Public RsAccessPrivilege As SqlDataReader
    Public tmpMandatoryColor As Color = Color.FromArgb(255, 255, 255, 192)
    Public USER_ID As String
    Public tmpSearchMode As String
    Public tmpSearchResult As String
    Public tmpFillingDGV As Boolean
    Public tmpReceiptNoForPrinting As String
    Public User_Name As String
    Public User_Type As String
    Public tmpDiscount As Integer
    Public Conn As SqlConnection
    Public Conn2 As String
    Public MinNoOfPassword As Integer = 5
    Public tmpvar As Integer
    Public tmpQuantity As Integer
    Public tmpConfig() As String
    Public Language, ServerName, DatabaseName As String
    Public tmpForm As Form
    Public tmpRecent As Form
    Public tmpReport As Form
    Public tmpMain As Form
    Public tmpType As String
    Public xComm As SqlCommand
    Public accFormName As String
    Public xConn, xConn1, Xconn2 As New SqlConnection
    Public xAdoAdapter As New SqlDataAdapter

    Public Function EnableInputBox(ByRef pForm As Form)
        Dim x As Integer
        For x = 0 To pForm.Controls.Count - 1
            If TypeOf pForm.Controls.Item(x) Is TextBox Or _
            TypeOf pForm.Controls.Item(x) Is ComboBox Or _
            TypeOf pForm.Controls.Item(x) Is DateTimePicker Then
                pForm.Controls.Item(x).Enabled = True
                If pForm.Controls.Item(x).Tag = "M" Then pForm.Controls.Item(x).BackColor = tmpMandatoryColor
            End If
            If TypeOf pForm.Controls.Item(x) Is GroupBox Then
                For Each ctrlControl In pForm.Controls.Item(x).Controls
                    If TypeOf ctrlControl Is TextBox Or _
                    TypeOf ctrlControl Is ComboBox Or _
                    TypeOf ctrlControl Is DateTimePicker Then
                        ctrlControl.Enabled = True
                        If ctrlControl.Tag = "M" Then ctrlControl.BackColor = tmpMandatoryColor
                    End If
                Next
            End If
            If TypeOf pForm.Controls.Item(x) Is TabControl Then
                Dim tabControl As TabControl = pForm.Controls.Item(x)
                Dim z As Integer
                For z = 0 To tabControl.TabCount() - 1
                    Dim TabPage As TabPage = tabControl.TabPages(z)
                    Dim y As Integer
                    For y = 0 To TabPage.Controls.Count - 1
                        If TypeOf TabPage.Controls.Item(y) Is TextBox Or _
                        TypeOf TabPage.Controls.Item(y) Is ComboBox Or _
                        TypeOf TabPage.Controls.Item(y) Is DateTimePicker Then
                            TabPage.Controls.Item(y).Enabled = True
                            If TabPage.Controls.Item(y).Tag = "M" Then _
                            TabPage.Controls.Item(y).BackColor = tmpMandatoryColor
                        End If
                    Next
                Next
            End If
        Next
        Return Nothing
    End Function

    Public Function DisableInputBox(ByRef pForm As Form)
        Dim x As Integer
        For x = 0 To pForm.Controls.Count - 1
            If TypeOf pForm.Controls.Item(x) Is TextBox Or _
            TypeOf pForm.Controls.Item(x) Is ComboBox Or _
            TypeOf pForm.Controls.Item(x) Is DateTimePicker Then
                pForm.Controls.Item(x).Enabled = False
                pForm.Controls.Item(x).BackColor = Color.Empty
            End If
            If TypeOf pForm.Controls.Item(x) Is GroupBox Then
                For Each ctrlControl In pForm.Controls.Item(x).Controls
                    If TypeOf ctrlControl Is TextBox Or _
                    TypeOf ctrlControl Is ComboBox Or _
                    TypeOf ctrlControl Is DateTimePicker Then
                        ctrlControl.Enabled = False
                        ctrlControl.BackColor = Color.Empty
                    End If
                Next
            End If
            If TypeOf pForm.Controls.Item(x) Is TabControl Then
                Dim tabControl As TabControl = pForm.Controls.Item(x)
                Dim z As Integer
                For z = 0 To tabControl.TabCount() - 1
                    Dim TabPage As TabPage = tabControl.TabPages(z)
                    Dim y As Integer
                    For y = 0 To TabPage.Controls.Count - 1
                        If TypeOf TabPage.Controls.Item(y) Is TextBox Or _
                        TypeOf TabPage.Controls.Item(y) Is ComboBox Or _
                        TypeOf TabPage.Controls.Item(y) Is DateTimePicker Then
                            TabPage.Controls.Item(y).Enabled = False
                            TabPage.Controls.Item(y).BackColor = Color.Empty
                        End If
                    Next
                Next
            End If
        Next
        Return Nothing
    End Function

    Public Sub AddDataToGrid(ByRef pDataGridView As DataGridView, _
                             ByVal pCode As String, ByVal pCodeCol As Integer, _
                             ByVal pName As String, ByVal pNameCol As Integer, _
                             ByVal pQuantityCol As Integer, _
                             ByVal pUnitPrice As String, ByVal pUnitPriceCol As Integer, _
                             ByVal pDiscPerCol As Integer, _
                             ByVal pDiscAmountCol As Integer, _
                             ByVal pTotalCol As Integer)

        If pDataGridView.RowCount > 2 Then
            For x As Integer = 0 To pDataGridView.RowCount - 3
                If pDataGridView.Item(pCodeCol, x).Value.ToString.ToUpper = pCode.ToUpper Then
                    tmpFillingDGV = True
                    If tmpQuantity = pDataGridView.Item(pQuantityCol, x).Value Then
                        MsgBox("Selling a Product with quantity exceed the balance is not allowed." & vbCrLf & _
                               "Adding this Product is cancelled!", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        pDataGridView.Item(pQuantityCol, x).Value = _
                            CInt(pDataGridView.Item(pQuantityCol, x).Value) + 1
                    End If
                    pDataGridView.Rows.RemoveAt(pDataGridView.RowCount - 2)
                    Calculation(pDataGridView.Item(pQuantityCol, x).Value, _
                                pDataGridView.Item(pUnitPriceCol, x).Value, _
                                pDataGridView.Item(pDiscAmountCol, x).Value, _
                                pDataGridView.Item(pTotalCol, x).Value)
                    tmpFillingDGV = False
                    pDataGridView.CurrentCell = _
                        pDataGridView.Rows(pDataGridView.RowCount - 1).Cells(0)
                    Exit Sub
                End If
            Next
            For y As Integer = 0 To pDataGridView.RowCount - 2
                pDataGridView.Item(0, y).ReadOnly = True
            Next
        End If

        If pDataGridView.RowCount = 1 Then pDataGridView.Rows.Add(1)
        pDataGridView.Item(pCodeCol, pDataGridView.RowCount - 2).Value = pCode
        pDataGridView.Item(pNameCol, pDataGridView.RowCount - 2).Value = pName
        pDataGridView.Item(pQuantityCol, pDataGridView.RowCount - 2).Value = "1"
        pDataGridView.Item(pUnitPriceCol, pDataGridView.RowCount - 2).Value = CInt(pUnitPrice)
        pDataGridView.Item(pDiscPerCol, pDataGridView.RowCount - 2).Value = tmpDiscount
        pDataGridView.Item(pDiscAmountCol, pDataGridView.RowCount - 2).Value = pUnitPrice * tmpDiscount / 100
        pDataGridView.Item(pTotalCol, pDataGridView.RowCount - 2).Value = _
            (CInt(pDataGridView.Item(pQuantityCol, pDataGridView.RowCount - 2).Value) * _
            CInt(pDataGridView.Item(pUnitPriceCol, pDataGridView.RowCount - 2).Value)) - _
            CInt(pDataGridView.Item(pDiscAmountCol, pDataGridView.RowCount - 2).Value)
        tmpFillingDGV = False

        pDataGridView.CurrentCell = pDataGridView.Rows(pDataGridView.RowCount - 1).Cells(0)

    End Sub

    Public Sub AddDataToGrid2(ByRef pDataGridView As DataGridView, _
                             ByVal pCode As String, ByVal pCodeCol As Integer, _
                             ByVal pName As String, ByVal pNameCol As Integer, _
                             ByVal pQuantityCol As Integer)

        If pDataGridView.RowCount > 2 Then
            For x As Integer = 0 To pDataGridView.RowCount - 3
                If pDataGridView.Item(pCodeCol, x).Value.ToString.ToUpper = pCode.ToUpper Then
                    tmpFillingDGV = True
                    If tmpQuantity = pDataGridView.Item(pQuantityCol, x).Value Then
                        MsgBox("Selling a Product with quantity exceed the balance is not allowed." & vbCrLf & _
                               "Adding this Product is cancelled!", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        pDataGridView.Item(pQuantityCol, x).Value = _
                            CInt(pDataGridView.Item(pQuantityCol, x).Value) + 1
                    End If
                    pDataGridView.Rows.RemoveAt(pDataGridView.RowCount - 2)

                    tmpFillingDGV = False
                    pDataGridView.CurrentCell = _
                        pDataGridView.Rows(pDataGridView.RowCount - 1).Cells(1)
                    Exit Sub
                End If
            Next
            For y As Integer = 0 To pDataGridView.RowCount - 2
                pDataGridView.Item(0, y).ReadOnly = True
            Next
        End If

        If pDataGridView.RowCount = 1 Then pDataGridView.Rows.Add(1)
        pDataGridView.Item(pCodeCol, pDataGridView.RowCount - 2).Value = pCode
        pDataGridView.Item(pNameCol, pDataGridView.RowCount - 2).Value = pName
        pDataGridView.Item(pQuantityCol, pDataGridView.RowCount - 2).Value = "1"

        tmpFillingDGV = False

        pDataGridView.CurrentCell = pDataGridView.Rows(pDataGridView.RowCount - 1).Cells(1)

    End Sub

    Public Sub Calculation(ByVal pQuantity As Integer, _
                           ByVal pUnitPrice As Integer, _
                           ByVal pDiscount As Integer, _
                           ByRef pSubTotal As Integer)
        pSubTotal = CInt((pQuantity * pUnitPrice) - pDiscount)
    End Sub

    Public Sub CalculationAll(ByRef pDataGridView As DataGridView, _
                                ByVal pDiscCol As Integer, _
                                ByVal pTotalCol As Integer, _
                                ByRef pDiscountTextBox As TextBox, _
                                ByRef pTaxPercentageTextBox As TextBox, _
                                ByRef pTaxTextBox As TextBox, _
                                ByRef pDPTextBox As TextBox, _
                                ByRef pGrandTotalTextBox As TextBox)

        Dim tmpDiscTotal, tmpTotal As Integer

        'pDiscountTextBox.Text = "0"
        pTaxPercentageTextBox.Text = FormatNumber(IIf(pTaxPercentageTextBox.Text = "", "0.00", _
                                                      pTaxPercentageTextBox.Text), 2)
        'pTaxTextBox.Text = FormatNumber(CInt(IIf(pTaxTextBox.Text = "", "0", pTaxTextBox.Text)), 0)
        'pDPTextBox.Text = FormatNumber(CInt(IIf(pDPTextBox.Text = "", "0", pDPTextBox.Text)), 0)
        'pGrandTotalTextBox.Text = "0"

        For x As Integer = 0 To pDataGridView.RowCount - 1
            tmpDiscTotal = tmpDiscTotal + pDataGridView.Item(pDiscCol, x).Value
            tmpTotal = tmpTotal + pDataGridView.Item(pTotalCol, x).Value
        Next

        If pTaxTextBox.Text = "" Then pTaxTextBox.Text = "0"
        'pTaxTextBox.Text = (tmpTotal * pTaxPercentageTextBox.Text) / 100
        pDiscountTextBox.Text = FormatNumber(CInt(tmpDiscTotal), 0)
        pGrandTotalTextBox.Text = FormatNumber(CInt(tmpTotal + CInt(pTaxTextBox.Text)), 0)
    End Sub

    Public Function ValidateComboBox(ByVal pComboBox As ComboBox) As Boolean
        If pComboBox.Text = "" And pComboBox.Tag = "" Then
            ValidateComboBox = True
            Exit Function
        End If

        Dim tmpBindingSource As BindingSource
        Dim tmpText As String
        Dim tmpPosition As Integer

        tmpText = pComboBox.Text

        If pComboBox.DataSource Is Nothing Then
            For x As Integer = 0 To pComboBox.Items.Count - 1
                If tmpText = pComboBox.Items(x) Then
                    ValidateComboBox = True
                    Exit Function
                End If
            Next
        Else
            tmpBindingSource = pComboBox.DataSource
            tmpPosition = tmpBindingSource.Position

            For x As Integer = 0 To pComboBox.Items.Count - 1
                tmpBindingSource.Position = x
                If tmpText = tmpBindingSource.Current(pComboBox.DisplayMember) Then
                    ValidateComboBox = True
                    Exit Function
                End If
            Next
        End If

        pComboBox.Text = tmpText
        ValidateComboBox = False
    End Function

    Public Function ValidateAllComboBox(ByVal pForm As Form) As Boolean
        Dim x As Integer
        For x = 0 To pForm.Controls.Count - 1
            If TypeOf pForm.Controls.Item(x) Is ComboBox Then
                If Not ValidateComboBox(pForm.Controls.Item(x)) Then
                    ValidateAllComboBox = False
                    Exit Function
                End If
            End If
            If TypeOf pForm.Controls.Item(x) Is GroupBox Then
                For Each ctrlControl In pForm.Controls.Item(x).Controls
                    If TypeOf ctrlControl Is ComboBox Then
                        If Not ValidateComboBox(ctrlControl) Then
                            ValidateAllComboBox = False
                            Exit Function
                        End If
                    End If
                Next
            End If
            If TypeOf pForm.Controls.Item(x) Is TabControl Then
                Dim tabControl As TabControl = pForm.Controls.Item(x)
                Dim z As Integer
                For z = 0 To tabControl.TabCount() - 1
                    Dim TabPage As TabPage = tabControl.TabPages(z)
                    Dim y As Integer
                    For y = 0 To TabPage.Controls.Count - 1
                        If TypeOf TabPage.Controls.Item(y) Is ComboBox Then
                            If Not ValidateComboBox(TabPage.Controls.Item(y)) Then
                                ValidateAllComboBox = False
                                Exit Function
                            End If
                        End If
                        If TypeOf TabPage.Controls.Item(y) Is GroupBox Then
                            For Each ctrlControl In TabPage.Controls.Item(y).Controls
                                If TypeOf ctrlControl Is ComboBox Then
                                    If Not ValidateComboBox(ctrlControl) Then
                                        ValidateAllComboBox = False
                                        Exit Function
                                    End If
                                End If
                            Next
                        End If
                    Next
                Next
            End If
        Next
        ValidateAllComboBox = True
    End Function

    Public Function SplitMultiLevelGroupingProduct(ByVal pProductId As Integer, _
                                                  ByVal pWarehouseId As Integer, _
                                                  ByVal pQuantity As Integer) As String
        Dim tmpAccumulatedQuantity As Integer = 0
        Dim tmpResult As String = ""
        Dim tmpParentId As Integer
        Dim sql As String
        Dim cmd As New SqlCommand
        Dim xdreader As SqlDataReader

        sql = "SELECT PARENT_PRODUCT_ID FROM GROUPING_PRODUCT WHERE CHILD_PRODUCT_ID = " & pProductId & ""
        cmd = New SqlCommand(sql, xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdreader = cmd.ExecuteReader()
        If xdreader.Read = True Then
            If xdreader.FieldCount > 1 Then
                tmpResult = "MORE THAN ONE PARENTS"
                Return tmpResult
                Exit Function
            End If

            Dim tmpContent As Integer
            xdreader.Close()
            sql = "SELECT PARENT_PRODUCT_ID, QUANTITY FROM GROUPING_PRODUCT WHERE CHILD_PRODUCT_ID = " & pProductId & ""
            cmd = New SqlCommand(sql, xConn)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xdreader = cmd.ExecuteReader()
            xdreader.Read()
            tmpContent = xdreader.Item("QUANTITY").ToString
            tmpParentId = xdreader.Item("PARENT_PRODUCT_ID").ToString

            Dim tmpParentQuantity As Integer
            xdreader.Close()
            sql = "SELECT QUANTITY FROM PRODUCT_WAREHOUSE WHERE PRODUCT_ID = " & tmpParentId & " AND WAREHOUSE_ID = " & pWarehouseId & ""
            cmd = New SqlCommand(sql, xConn)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xdreader = cmd.ExecuteReader()
            If xdreader.Read Then
                tmpParentQuantity = xdreader.Item("QUANTITY").ToString
            Else
                tmpParentQuantity = 0
            End If

            tmpAccumulatedQuantity = tmpContent * tmpParentQuantity
            Dim tbAccumatedQuantity As Double
            tbAccumatedQuantity = (pQuantity / tmpContent)

            If tmpAccumulatedQuantity >= pQuantity Then
                SplitProduct(tmpParentId, RoundUp(tbAccumatedQuantity), pWarehouseId)
            Else
                SplitMultiLevelGroupingProduct(tmpParentId, pWarehouseId, RoundUp(tbAccumatedQuantity))
                SplitMultiLevelGroupingProduct(pProductId, pWarehouseId, 1)
            End If
        Else
            tmpResult = "NO PARENT FOUND"
            Return tmpResult
            Exit Function
        End If

        Return tmpResult
    End Function

    Public Function SplitProduct(ByVal pProductId As Integer, _
                                 ByVal pQuantity As Integer, _
                                 ByVal pWarehouseId As Integer) As String

        'Dim RsTableSplitProduct As ADODB.Recordset
        Dim Rs As ADODB.Recordset
        Rs = New ADODB.Recordset
        Dim sql As String
        Dim cmd, cmd1, cmd2 As New SqlCommand
        Dim xdreader As SqlDataReader

        sql = "EXEC SP_PRODUCT_WAREHOUSE" & vbCrLf & _
                "@METHOD = 'I'," & vbCrLf & _
                "@PRODUCT_WAREHOUSE_ID = " & pWarehouseId & "," & vbCrLf & _
                "@PRODUCT_ID = " & pProductId & "," & vbCrLf & _
                "@WAREHOUSE_ID = " & pWarehouseId & "," & vbCrLf & _
                "@QUANTITY = " & (pQuantity * (-1)) & "," & vbCrLf & _
                "@USER_ID_INPUT = " & USER_ID & "," & vbCrLf & _
                "@INPUT_DATE = '" & Now & "'," & vbCrLf & _
                "@USER_ID_UPDATE = 0," & vbCrLf & _
                "@UPDATE_DATE = '" & DateSerial(4000, 12, 31) & "'"
        cmd = New SqlCommand(sql, xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        cmd.ExecuteNonQuery()

        Dim RsChild As ADODB.Recordset
        RsChild = New ADODB.Recordset

        sql = "SELECT CHILD_PRODUCT_ID, QUANTITY FROM GROUPING_PRODUCT WHERE PARENT_PRODUCT_ID = " & pProductId & ""
        cmd = New SqlCommand(sql, xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdreader = cmd.ExecuteReader()
        While Not xdreader.Read = False
            sql = "EXEC SP_PRODUCT_WAREHOUSE" & vbCrLf & _
                    "@METHOD = 'I'," & vbCrLf & _
                    "@PRODUCT_WAREHOUSE_ID = " & pWarehouseId & "," & vbCrLf & _
                    "@PRODUCT_ID = " & xdreader.Item("CHILD_PRODUCT_ID").ToString & "," & vbCrLf & _
                    "@WAREHOUSE_ID = " & pWarehouseId & "," & vbCrLf & _
                    "@QUANTITY = " & (xdreader.Item("QUANTITY").ToString * pQuantity) & "," & vbCrLf & _
                    "@USER_ID_INPUT = " & USER_ID & "," & vbCrLf & _
                    "@INPUT_DATE = '" & Now & "'," & vbCrLf & _
                    "@USER_ID_UPDATE = 0" & "," & vbCrLf & _
                    "@UPDATE_DATE = '" & DateSerial(4000, 12, 31) & "'"
            cmd1 = New SqlCommand(sql, xConn1)
            If xConn1.State = ConnectionState.Open Then xConn1.Close() : xConn1.Open()
            cmd1.ExecuteNonQuery()

            sql = "EXEC SP_SPLIT_PROCESS" & vbCrLf & _
                                     "@METHOD = 'I'," & vbCrLf & _
                                     "@PRODUCT_SPLIT_ID = 0," & vbCrLf & _
                                     "@DATE_SPLIT = '" & Now & "'," & vbCrLf & _
                                     "@PARENT_ID = " & pProductId & "," & vbCrLf & _
                                     "@CHILD_ID = " & xdreader.Item("CHILD_PRODUCT_ID").ToString & "," & vbCrLf & _
                                     "@QUANTITY_PARENT = " & pQuantity & "," & vbCrLf & _
                                     "@QUANTITY_CHILD = " & (xdreader.Item("QUANTITY").ToString * pQuantity) & "," & vbCrLf & _
                                     "@WAREHOUSE_SOURCE_ID = " & pWarehouseId & "," & vbCrLf & _
                                     "@WAREHOUSE_DESTINATION_ID = " & pWarehouseId & "," & vbCrLf & _
                                     "@DESCRIPTION = 'AUTO'," & vbCrLf & _
                                     "@USER_ID_INPUT = " & USER_ID & "," & vbCrLf & _
                                     "@INPUT_DATE = '" & Now & "'," & vbCrLf & _
                                     "@USER_ID_UPDATE = 0," & vbCrLf & _
                                     "@UPDATE_DATE = '" & DateSerial(4000, 12, 31) & "'"
            cmd2 = New SqlCommand(sql, Xconn2)
            If Xconn2.State = ConnectionState.Open Then Xconn2.Close() : Xconn2.Open()
            cmd2.ExecuteNonQuery()

            xdreader.NextResult()
        End While

        Return ""
    End Function

    Public Function RoundUp(ByVal pDec As Double) As Integer
        If pDec > Int(pDec) Then
            RoundUp = Int(pDec) + 1
        ElseIf Int(pDec) > pDec Then
            RoundUp = Int(pDec) - 1
        Else
            RoundUp = pDec
        End If
    End Function

End Module