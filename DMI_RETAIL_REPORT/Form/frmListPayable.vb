﻿Imports System.Data.SqlClient
Imports System.Data
Public Class frmListPayable
    Dim tmpAmount As Integer
    Public tmpPeriod3 As Date

    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub


    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click

        Me.Cursor = Cursors.WaitCursor
        'SP_LIST_PAYABLETableAdapter.Fill(Me.DS_PURCHASE.SP_LIST_PAYABLE, dtpPeriod.Value, dtpPeriod2.Value)
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC 	@return_value = [dbo].[SP_LIST_PAYABLE]" & vbCrLf & _
              "         @PERIOD1 = N'" & dtpPeriod.Value & "'," & vbCrLf & _
              "    		@PERIOD2 = N'" & dtpPeriod2.Value & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.dgvLIST_PAYABLE.DataSource = dt
        Me.Cursor = Cursors.Default

        For x As Integer = 0 To dgvLIST_PAYABLE.RowCount - 1
            tmpAmount = tmpAmount + dgvLIST_PAYABLE.Item("AMOUNT", x).Value
            tmpAmount = tmpAmount
        Next

        If Language = "Indonesian" Then
            lblTotalAmount.Text = "Jumlah Nominal : " & FormatNumber(tmpAmount, 0)
            LblTotalInvoice.Text = "Jumlah Faktur : " & dgvLIST_PAYABLE.RowCount
            dgvLIST_PAYABLE.Columns("INVOICE").HeaderText = "No Faktur"
            dgvLIST_PAYABLE.Columns("TRANSACTION_DATE").HeaderText = "Tgl Transaksi"
            dgvLIST_PAYABLE.Columns("MATURITY_DATE").HeaderText = "Tgl Jatuh Tempo"
            dgvLIST_PAYABLE.Columns("AMOUNT").HeaderText = "Jumlah"
            dgvLIST_PAYABLE.Columns("DESCRIPTION").HeaderText = "Keterangan"
            dgvLIST_PAYABLE.Columns("BALANCE").HeaderText = "Saldo"
            dgvLIST_PAYABLE.Columns("PAYABLE_ID").Visible = False
            dgvLIST_PAYABLE.Columns("PERIOD1").Visible = False
            dgvLIST_PAYABLE.Columns("PERIOD2").Visible = False
            dgvLIST_PAYABLE.Columns("TRANSACTION_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_PAYABLE.Columns("MATURITY_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_PAYABLE.Columns("AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_PAYABLE.Columns("BALANCE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_PAYABLE.Columns("TRANSACTION_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvLIST_PAYABLE.Columns("MATURITY_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvLIST_PAYABLE.Columns("AMOUNT").DefaultCellStyle.Format = "n0"
            dgvLIST_PAYABLE.Columns("BALANCE").DefaultCellStyle.Format = "n0"
        Else
            LblTotalInvoice.Text = "Total Invoice : " & dgvLIST_PAYABLE.RowCount
            lblTotalAmount.Text = "Total Amount : " & FormatNumber(tmpAmount, 0)
            dgvLIST_PAYABLE.Columns("INVOICE").HeaderText = "Invoice No"
            dgvLIST_PAYABLE.Columns("TRANSACTION_DATE").HeaderText = "Transaction Date"
            dgvLIST_PAYABLE.Columns("MATURITY_DATE").HeaderText = "Maturity Date"
            dgvLIST_PAYABLE.Columns("AMOUNT").HeaderText = "Amount"
            dgvLIST_PAYABLE.Columns("DESCRIPTION").HeaderText = "Description"
            dgvLIST_PAYABLE.Columns("BALANCE").HeaderText = "Balance"
            dgvLIST_PAYABLE.Columns("PAYABLE_ID").Visible = False
            dgvLIST_PAYABLE.Columns("PERIOD1").Visible = False
            dgvLIST_PAYABLE.Columns("PERIOD2").Visible = False
            dgvLIST_PAYABLE.Columns("TRANSACTION_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_PAYABLE.Columns("MATURITY_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_PAYABLE.Columns("AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_PAYABLE.Columns("BALANCE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_PAYABLE.Columns("TRANSACTION_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvLIST_PAYABLE.Columns("MATURITY_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvLIST_PAYABLE.Columns("AMOUNT").DefaultCellStyle.Format = "n0"
            dgvLIST_PAYABLE.Columns("BALANCE").DefaultCellStyle.Format = "n0"
        End If

        tmpAmount = 0
    End Sub

    Private Sub frmListPayable_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F12 Then
            cmdReport_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub frmListPayable_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Language = "Indonesian" Then
            Me.Text = "Daftar Hutang"
            lblPeriod.Text = "Periode"
            cmdGenerate.Text = "Proses"
            cmdReport.Text = "Cetak"

            'dgvLIST_PAYABLE.Columns("INVOICE").HeaderText = "No Faktur"
            'dgvLIST_PAYABLE.Columns("TRANSACTION_DATE").HeaderText = "Tgl Transaksi"
            'dgvLIST_PAYABLE.Columns("MATURITY_DATE").HeaderText = "Tgl Jatuh Tempo"
            'dgvLIST_PAYABLE.Columns("AMOUNT").HeaderText = "Jumlah"
            'dgvLIST_PAYABLE.Columns("DESCRIPTION").HeaderText = "Keterangan"

            lblTotalAmount.Text = "Jumlah Nominal : " & FormatNumber(tmpAmount, 0)
            LblTotalInvoice.Text = "Jumlah Faktur : " & dgvLIST_PAYABLE.RowCount
        Else
            LblTotalInvoice.Text = "Total Invoice : " & dgvLIST_PAYABLE.RowCount
            lblTotalAmount.Text = "Total Amount : " & FormatNumber(tmpAmount, 0)
        End If

        dtpPeriod.Value = DateSerial(Today.Year, Today.Month, 1)
        dtpPeriod2.Value = DateSerial(Today.Year, Today.Month + 1, 0)

        tmpPeriod3 = DateSerial(4000, 12, 31)
        tmpAmount = 0
    End Sub


    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        Dim RPV As New frmRepActivePayable
        RPV.date1 = dtpPeriod.Value
        RPV.date2 = dtpPeriod2.Value
        RPV.ReportViewer1.ZoomPercent = 100
        RPV.WindowState = FormWindowState.Maximized
        RPV.Show()
    End Sub

    Private Sub dtpPeriod_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpPeriod.ValueChanged
        If dgvLIST_PAYABLE.RowCount > 0 Then
            'SP_LIST_PAYABLETableAdapter.Fill(Me.DS_PURCHASE.SP_LIST_PAYABLE, tmpPeriod3, tmpPeriod3)
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = "DECLARE	@return_value int" & vbCrLf & _
                  "EXEC 	@return_value = [dbo].[SP_LIST_PAYABLE]" & vbCrLf & _
                  "         @PERIOD1 = N'" & tmpPeriod3 & "'," & vbCrLf & _
                  "    		@PERIOD2 = N'" & tmpPeriod3 & "'" & vbCrLf & _
                  "SELECT	'Return Value' = @return_value"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.dgvLIST_PAYABLE.DataSource = dt
        End If
    End Sub

    Private Sub dtpPeriod2_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpPeriod2.ValueChanged
        If dgvLIST_PAYABLE.RowCount > 0 Then
            'SP_LIST_PAYABLETableAdapter.Fill(Me.DS_PURCHASE.SP_LIST_PAYABLE, tmpPeriod3, tmpPeriod3)
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = "DECLARE	@return_value int" & vbCrLf & _
                  "EXEC 	@return_value = [dbo].[SP_LIST_PAYABLE]" & vbCrLf & _
                  "         @PERIOD1 = N'" & tmpPeriod3 & "'," & vbCrLf & _
                  "    		@PERIOD2 = N'" & tmpPeriod3 & "'" & vbCrLf & _
                  "SELECT	'Return Value' = @return_value"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.dgvLIST_PAYABLE.DataSource = dt
        End If
    End Sub
End Class