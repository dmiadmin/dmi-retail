﻿Imports System.Data.SqlClient

Public Class frmListUser
    Dim SP_LIST_USERBindingSource As New BindingSource
    Public tmpUserResult As String

    Private Sub frmListUser_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If frmEntryUser.tmpClickMode = "Search" Then
            If e.KeyCode = Keys.Escape Then
                tmpUserResult = ""
                Me.Close()
            ElseIf e.KeyCode = Keys.Enter Then
                dgvUser_DoubleClick(Nothing, Nothing)
            End If
        Else
            If e.KeyCode = Keys.F12 Then
                cmdPrint_Click(Nothing, Nothing)
            ElseIf e.KeyCode = Keys.Enter Then
                dgvUser_DoubleClick(Nothing, Nothing)
            End If
        End If
    End Sub

    Private Sub GetData()
        dt = New DataTable
        sqladapter = New SqlDataAdapter("DECLARE	@return_value int " & _
                                        "EXEC @return_value = [dbo].[SP_LIST_USER]" & _
                                        "SELECT 'Return Value' = @return_value", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        SP_LIST_USERBindingSource.DataSource = dt
        SP_LIST_USERBindingNavigator.BindingSource = SP_LIST_USERBindingSource
        dgvUser.DataSource = SP_LIST_USERBindingSource
    End Sub

    Private Sub frmListUser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetData()
        If Language = "Indonesian" Then
            Me.Text = "Daftar User"
            CheckBox2.Text = "Tidak Terhapus"
            cmdPrint.Text = "Cetak"

            Label1.Text = "Jumlah User : " & dgvUser.RowCount

            dgvUser.Columns("USER_NAME").HeaderText = "Nama User"
            dgvUser.Columns("PASSWORD_HINT").HeaderText = "Petunjuk Sandi"
            dgvUser.Columns("ACTIVE").HeaderText = "Aktif"
            dgvUser.Columns("NUMBER_OF_WRONG_PASSWORD").HeaderText = "Maks. Kesalahan Sandi"
            dgvUser.Columns("ACCESS_PRIVILEGE_NAME").HeaderText = "Hak Akses"
            dgvUser.Columns("LAST_LOGIN").HeaderText = "Login Terakhir"
        Else
            Label1.Text = "No of User(s) : " & dgvUser.RowCount
            dgvUser.Columns("USER_NAME").HeaderText = "Username"
            dgvUser.Columns("PASSWORD_HINT").HeaderText = "Password Hint"
            dgvUser.Columns("ACTIVE").HeaderText = "Active"
            dgvUser.Columns("NUMBER_OF_WRONG_PASSWORD").HeaderText = "Number of Wrong Password"
            dgvUser.Columns("ACCESS_PRIVILEGE_NAME").HeaderText = "Access Priviledge"
            dgvUser.Columns("LAST_LOGIN").HeaderText = "Last Login"
        End If

        dgvUser.Columns("USER_ID").Visible = False
        dgvUser.Columns("EFFECTIVE_START_DATE").Visible = False
        dgvUser.Columns("EFFECTIVE_END_DATE").Visible = False

        If frmEntryUser.tmpClickMode = "Search" Then
            Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedDialog
            Me.CenterToParent()
            SP_LIST_USERBindingSource.Position = 0
            cmdPrint.Enabled = False
        Else
            Me.FormBorderStyle = Windows.Forms.FormBorderStyle.Fixed3D
            cmdPrint.Enabled = True
        End If

    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked = True Then
            SP_LIST_USERBindingSource.Filter = "EFFECTIVE_END_DATE > '" & Now & "'"
        Else
            SP_LIST_USERBindingSource.Filter = ""
        End If
    End Sub

    Private Sub dgvUser_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvUser.DoubleClick
        frmEntryUser.MdiParent = frmMain
        frmEntryUser.Show()
        frmEntryUser.BringToFront()
        tmpRecentForm = New frmEntryUser

        frmEntryUser.USERBindingSource.Position = frmEntryUser.USERBindingSource.Find("USER_ID", _
                                                    SP_LIST_USERBindingSource.Current("USER_ID"))

        Me.Dispose()
    End Sub

    Private Sub cmdPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrint.Click
        Dim RPV As New frmRepUser
        RPV.WindowState = FormWindowState.Maximized
        RPV.Show()
    End Sub
End Class