﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListAdjustment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListAdjustment))
        Me.dtpPeriod = New System.Windows.Forms.DateTimePicker()
        Me.SP_LIST_ADJUSTMENTDataGridView = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmdReport = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dtpPeriod2 = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmdGenerate = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblNoItem = New System.Windows.Forms.Label()
        CType(Me.SP_LIST_ADJUSTMENTDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'dtpPeriod
        '
        Me.dtpPeriod.CustomFormat = "dd-MMM-yyyy"
        Me.dtpPeriod.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPeriod.Location = New System.Drawing.Point(279, 15)
        Me.dtpPeriod.Name = "dtpPeriod"
        Me.dtpPeriod.Size = New System.Drawing.Size(116, 22)
        Me.dtpPeriod.TabIndex = 0
        '
        'SP_LIST_ADJUSTMENTDataGridView
        '
        Me.SP_LIST_ADJUSTMENTDataGridView.AllowUserToAddRows = False
        Me.SP_LIST_ADJUSTMENTDataGridView.AllowUserToDeleteRows = False
        Me.SP_LIST_ADJUSTMENTDataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SP_LIST_ADJUSTMENTDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.SP_LIST_ADJUSTMENTDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.SP_LIST_ADJUSTMENTDataGridView.Location = New System.Drawing.Point(6, 18)
        Me.SP_LIST_ADJUSTMENTDataGridView.Name = "SP_LIST_ADJUSTMENTDataGridView"
        Me.SP_LIST_ADJUSTMENTDataGridView.ReadOnly = True
        Me.SP_LIST_ADJUSTMENTDataGridView.Size = New System.Drawing.Size(768, 242)
        Me.SP_LIST_ADJUSTMENTDataGridView.TabIndex = 5
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmdReport)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.dtpPeriod2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cmdGenerate)
        Me.GroupBox1.Controls.Add(Me.dtpPeriod)
        Me.GroupBox1.Location = New System.Drawing.Point(18, 11)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(781, 89)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        '
        'cmdReport
        '
        Me.cmdReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdReport.Image = Global.DMI_RETAIL_INVENTORY.My.Resources.Resources.Printer_small
        Me.cmdReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdReport.Location = New System.Drawing.Point(685, 59)
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(90, 24)
        Me.cmdReport.TabIndex = 16
        Me.cmdReport.Text = "Print [F12]"
        Me.cmdReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.cmdReport.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(408, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(23, 15)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "To"
        '
        'dtpPeriod2
        '
        Me.dtpPeriod2.CustomFormat = "dd-MMM-yyyy"
        Me.dtpPeriod2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPeriod2.Location = New System.Drawing.Point(435, 15)
        Me.dtpPeriod2.Name = "dtpPeriod2"
        Me.dtpPeriod2.Size = New System.Drawing.Size(117, 22)
        Me.dtpPeriod2.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(194, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(79, 15)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "From Period"
        '
        'cmdGenerate
        '
        Me.cmdGenerate.Image = CType(resources.GetObject("cmdGenerate.Image"), System.Drawing.Image)
        Me.cmdGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdGenerate.Location = New System.Drawing.Point(330, 47)
        Me.cmdGenerate.Name = "cmdGenerate"
        Me.cmdGenerate.Size = New System.Drawing.Size(99, 33)
        Me.cmdGenerate.TabIndex = 3
        Me.cmdGenerate.Text = "Generate"
        Me.cmdGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdGenerate.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.SP_LIST_ADJUSTMENTDataGridView)
        Me.GroupBox2.Location = New System.Drawing.Point(18, 107)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(780, 266)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.TabStop = False
        '
        'lblNoItem
        '
        Me.lblNoItem.AutoSize = True
        Me.lblNoItem.Location = New System.Drawing.Point(36, 380)
        Me.lblNoItem.Name = "lblNoItem"
        Me.lblNoItem.Size = New System.Drawing.Size(45, 15)
        Me.lblNoItem.TabIndex = 8
        Me.lblNoItem.Text = "Label3"
        Me.lblNoItem.Visible = False
        '
        'frmListAdjustment
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(811, 412)
        Me.Controls.Add(Me.lblNoItem)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmListAdjustment"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "List Adjustment"
        Me.Text = "List Adjustment"
        CType(Me.SP_LIST_ADJUSTMENTDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dtpPeriod As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmdGenerate As System.Windows.Forms.Button
    Friend WithEvents SP_LIST_ADJUSTMENTDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtpPeriod2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblNoItem As System.Windows.Forms.Label
    Friend WithEvents cmdReport As System.Windows.Forms.Button
End Class
