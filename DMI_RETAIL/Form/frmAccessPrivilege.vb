﻿Imports System.Data.SqlClient

Public Class frmAccessPrivilege
    Dim tmpSaveMode, tmpKey As String
    Dim tmpChange As Boolean
    Dim tmpAdd As String = ""
    Dim tmpEdit As String = ""
    Dim tmpOpen As String = ""
    Dim tmpDelete As String = ""
    Dim tmpPrint As String = ""
    Public ACCESS_PRIVILEGEBindingSource As New BindingSource

    Private Sub GetAccessPrivilegeData()
        dt = New DataTable
        sqladapter = New SqlDataAdapter("SELECT ACCESS_PRIVILEGE_ID, ACCESS_PRIVILEGE_NAME, " & _
                                        "EFFECTIVE_START_DATE, EFFECTIVE_END_DATE, USER_ID_INPUT, " & _
                                        "INPUT_DATE, USER_ID_UPDATE, UPDATE_DATE " & _
                                        "FROM ACCESS_PRIVILEGE " & _
                                        "ORDER BY ACCESS_PRIVILEGE_NAME", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        ACCESS_PRIVILEGEBindingSource.DataSource = dt
        ACCESS_PRIVILEGEBindingNavigator.BindingSource = ACCESS_PRIVILEGEBindingSource
    End Sub

    Private Sub GetAccessRightData()
        If ACCESS_PRIVILEGE_NAMETextBox.Text <> "" Then
            For i As Integer = 0 To dgvPrivilege.RowCount - 1
                'ACCESS_PRIVILEGETableAdapter.SP_GET_ACCESS_PRIVILEGE(ACCESS_PRIVILEGEBindingSource.Current("ACCESS_PRIVILEGE_ID"), dgvPRIVILEGE.Item("FORM_NAME", i).Value, tmpAdd, tmpEdit, tmpOpen, tmpDelete, tmpPrint)
                If xdreader.HasRows Then

                    tmpAdd = Trim(xdreader.Item("TAMBAH"))
                    tmpEdit = Trim(xdreader.Item("EDIT"))
                    tmpDelete = Trim(xdreader.Item("HAPUS"))
                    tmpOpen = Trim(xdreader.Item("BUKA"))
                    tmpPrint = Trim(xdreader.Item("CETAK"))
                    If tmpAdd = "No" Then
                        dgvPrivilege.Item("ALLOW_ADD", i).Value = False
                    Else
                        dgvPrivilege.Item("ALLOW_ADD", i).Value = True
                    End If
                    If tmpEdit = "No" Then
                        dgvPrivilege.Item("ALLOW_EDIT", i).Value = False
                    Else
                        dgvPrivilege.Item("ALLOW_EDIT", i).Value = True
                    End If
                    If tmpDelete = "No" Then
                        dgvPrivilege.Item("ALLOW_DELETE", i).Value = False
                    Else
                        dgvPrivilege.Item("ALLOW_DELETE", i).Value = True
                    End If
                    If tmpOpen = "No" Then
                        dgvPrivilege.Item("ALLOW_OPEN", i).Value = False
                    Else
                        dgvPrivilege.Item("ALLOW_OPEN", i).Value = True
                    End If
                    If tmpPrint = "No" Then
                        dgvPrivilege.Item("ALLOW_PRINT", i).Value = False
                    Else
                        dgvPrivilege.Item("ALLOW_PRINT", i).Value = True
                    End If

                End If
            Next
            If ACCESS_PRIVILEGEBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                lblStatus.Visible = True
                lblStatus.Text = "DELETED"
                cmdDelete.Enabled = False
                cmdEdit.Enabled = False
            Else
                lblStatus.Visible = False
                cmdDelete.Enabled = True
                cmdEdit.Enabled = True
            End If
        End If
    End Sub

    Private Sub frmAccessPrivilege_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        AccessPrivilege(Me.Text)
        GetAccessPrivilegeData()

        ACCESS_PRIVILEGE_NAMETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", ACCESS_PRIVILEGEBindingSource, "ACCESS_PRIVILEGE_NAME", True))
        If Language = "Indonesian" Then
            cmdAdd.Text = "Tambah"
            cmdEdit.Text = "Ubah"
            cmdUndo.Text = "Batal"
            cmdDelete.Text = "Hapus"
            cmdSave.Text = "Simpan"
            Label1.Text = "Nama Akses"
            dgvPrivilege.Columns("FORM_NAME").HeaderText = "Nama Form"
            dgvPrivilege.Columns("ALLOW_ADD").HeaderText = "Tambah"
            dgvPrivilege.Columns("ALLOW_EDIT").HeaderText = "Ubah"
            dgvPrivilege.Columns("ALLOW_EDIT").HeaderText = "Hapus"
        End If
        'Dim account As New DMI_RETAIL_MASTER.frmEntryAccount
        'Dim customer As New DMI_RETAIL_MASTER.frmEntryCustomer
        'Dim location As New DMI_RETAIL_MASTER.frmEntryLocation
        'Dim vendor As New DMI_RETAIL_MASTER.frmEntryVendor
        'Dim warehouse As New DMI_RETAIL_MASTER.frmEntryWarehouse
        'Dim product As New DMI_RETAIL_MASTER.frmEntryProduct
        'Dim salesman As New DMI_RETAIL_MASTER.frmEntrySalesman

        'Dim entrypurchase As New DMI_RETAIL_PURCHASE.frmEntryPurchase
        'Dim entryreturnpurchase As New DMI_RETAIL_PURCHASE.frmEntryReturnPurchase
        'Dim listpurchase As New DMI_RETAIL_PURCHASE.frmListPurchase
        'Dim listpurchasereturn As New DMI_RETAIL_PURCHASE.frmListPurchaseReturn

        'Dim listsale As New DMI_RETAIL_SALE.frmListSale
        'Dim listsalereturn As New DMI_RETAIL_SALE.frmListSaleReturn
        'Dim entryreturnsale As New DMI_RETAIL_SALE.frmEntryReturnSale
        'Dim entrysale As New DMI_RETAIL_SALE.frmEntrySale

        'Dim listpayablepayment As New DMI_RETAIL_HUTANG_PIUTANG.frmListPayablePayment
        'Dim listreceivablepayment As New DMI_RETAIL_HUTANG_PIUTANG.frmListReceivablePayment
        'Dim payablepayment As New DMI_RETAIL_HUTANG_PIUTANG.frmPayablePayment
        'Dim payabletransaction As New DMI_RETAIL_HUTANG_PIUTANG.frmPayableTransaction
        'Dim receivablepayment As New DMI_RETAIL_HUTANG_PIUTANG.frmReceivablePayment
        'Dim receivabletransaction As New DMI_RETAIL_HUTANG_PIUTANG.frmReceivableTransaction

        'Dim entryadjustment As New DMI_RETAIL_INVENTORY.frmEntryAdjustment
        'Dim entrygroupingproduct As New DMI_RETAIL_INVENTORY.frmEntryGroupingProduct
        'Dim entryproductdistribution As New DMI_RETAIL_INVENTORY.frmEntryProductDistribution
        'Dim listadjustment As New DMI_RETAIL_INVENTORY.frmListAdjustment
        'Dim listendingstock As New DMI_RETAIL_INVENTORY.frmListEndingStock
        'Dim listgroupingproduct As New DMI_RETAIL_INVENTORY.frmListGroupingProduct
        'Dim listopeningbalance As New DMI_RETAIL_INVENTORY.frmListOpeningBalance
        'Dim listproductdistribution As New DMI_RETAIL_INVENTORY.frmListProductDistribution
        'Dim listproductstock As New DMI_RETAIL_INVENTORY.frmListProductStock
        'Dim listproductwarehouse As New DMI_RETAIL_INVENTORY.frmListProductWarehouse
        'Dim listsplitproduct As New DMI_RETAIL_INVENTORY.frmListSplitProduct
        'Dim liststockcard As New DMI_RETAIL_INVENTORY.frmListStockCard
        'Dim splitproduk As New DMI_RETAIL_INVENTORY.frmSplitProduct
        'Dim entryopeningbalance As New DMI_RETAIL_INVENTORY.frmEntryOpeningBalance

        'Dim frmBalanceSheet As New DMI_RETAIL_REPORT.frmBalanceSheet
        'Dim frmListDailySoldProduct As New DMI_RETAIL_REPORT.frmListDailySoldProduct
        'Dim frmListMovingProduct As New DMI_RETAIL_REPORT.frmListMovingProduct
        'Dim frmListPayable As New DMI_RETAIL_REPORT.frmListPayable
        'Dim frmListReceivable As New DMI_RETAIL_REPORT.frmListReceivable
        'Dim frmListReportSale As New DMI_RETAIL_REPORT.frmListReportSale
        'Dim frmListSalesmanInvoice As New DMI_RETAIL_REPORT.frmListSalesmanInvoice
        'Dim frmListSalesmanProduct As New DMI_RETAIL_REPORT.frmListSalesmanProduct
        'Dim frmListPurchasePrice As New DMI_RETAIL_REPORT.frmListPurchasePrice
        'Dim frmListProductMovement As New DMI_RETAIL_REPORT.frmListProductMovement
        'Dim frmReportPurchaseGeneral As New DMI_RETAIL_REPORT.frmReportPurchaseGeneral
        'Dim frmReportPurchaseByVendor As New DMI_RETAIL_REPORT.frmReportPurchaseByVendor
        'Dim frmReportPurchaseByPaymentMethod As New DMI_RETAIL_REPORT.frmReportPurchaseByPaymentMethod
        'Dim frmReportPurchaseByStatus As New DMI_RETAIL_REPORT.frmReportPurchaseByStatus
        'Dim frmReportSaleByCustomer As New DMI_RETAIL_REPORT.frmReportSaleByCustomer
        'Dim frmReportSaleByPaymentMethod As New DMI_RETAIL_REPORT.frmReportSaleByPaymentMethod
        'Dim frmReportSaleByStatus As New DMI_RETAIL_REPORT.frmReportSaleByStatus
        'Dim frmBalanceSheetFinance As New DMI_RETAIL_REPORT.frmBalanceSheetFinance
        'Dim frmReportProfitLoss As New DMI_RETAIL_REPORT.frmReportProfitLoss

        dgvPrivilege.Rows.Add(71)
        dgvPrivilege.Item(0, 0).Value = "Access Privilege"
        dgvPrivilege.Item(0, 1).Value = "Stock Balance"
        dgvPrivilege.Item(0, 2).Value = "Change Password"
        dgvPrivilege.Item(0, 3).Value = "Closing Period - Monthly"
        dgvPrivilege.Item(0, 4).Value = "Closing Period - Yearly"
        dgvPrivilege.Item(0, 5).Value = "Company Information"
        dgvPrivilege.Item(0, 6).Value = "Configuration"
        dgvPrivilege.Item(0, 7).Value = "Backup Database"
        dgvPrivilege.Item(0, 8).Value = "Restore Database"
        dgvPrivilege.Item(0, 9).Value = "Entry Account"
        dgvPrivilege.Item(0, 10).Value = "Entry Adjustment"
        dgvPrivilege.Item(0, 11).Value = "Entry Customer"
        dgvPrivilege.Item(0, 12).Value = "Grouping Product"
        dgvPrivilege.Item(0, 13).Value = "Entry Location"
        dgvPrivilege.Item(0, 14).Value = "Entry Payment Method"
        dgvPrivilege.Item(0, 15).Value = "Entry Product Distribution"
        dgvPrivilege.Item(0, 16).Value = "Entry Purchase"
        dgvPrivilege.Item(0, 17).Value = "Entry Return Purchase"
        dgvPrivilege.Item(0, 18).Value = "Entry Return Sale"
        dgvPrivilege.Item(0, 19).Value = "Entry Sale"
        dgvPrivilege.Item(0, 20).Value = "Entry Unit"
        dgvPrivilege.Item(0, 21).Value = "Entry User"
        dgvPrivilege.Item(0, 22).Value = "Entry Vendor"
        dgvPrivilege.Item(0, 23).Value = "Entry Warehouse"
        dgvPrivilege.Item(0, 24).Value = "Entry Password"
        dgvPrivilege.Item(0, 25).Value = "List Adjustment"
        dgvPrivilege.Item(0, 26).Value = "Daily Invoice"
        dgvPrivilege.Item(0, 27).Value = "Daily Sales"
        dgvPrivilege.Item(0, 28).Value = "Ending Stock"
        dgvPrivilege.Item(0, 29).Value = "List Grouping Product"
        dgvPrivilege.Item(0, 30).Value = "List Invoice Purchase"
        dgvPrivilege.Item(0, 31).Value = "List Invoice Sale"
        dgvPrivilege.Item(0, 32).Value = "List Sold Product"
        dgvPrivilege.Item(0, 33).Value = "List Opening Balance"
        dgvPrivilege.Item(0, 34).Value = "List All Payable"
        dgvPrivilege.Item(0, 35).Value = "List Payable Payment"
        dgvPrivilege.Item(0, 36).Value = "List Product Distribution"
        dgvPrivilege.Item(0, 37).Value = "List Product Stock"
        dgvPrivilege.Item(0, 38).Value = "List Product Warehouse"
        dgvPrivilege.Item(0, 39).Value = "List Purchase"
        dgvPrivilege.Item(0, 40).Value = "List Purchase Return"
        dgvPrivilege.Item(0, 41).Value = "List All Receivable"
        dgvPrivilege.Item(0, 42).Value = "List Receivable Payment"
        dgvPrivilege.Item(0, 43).Value = "List Sale Report"
        dgvPrivilege.Item(0, 44).Value = "Notification Safe Stock"
        dgvPrivilege.Item(0, 45).Value = "List Sales"
        dgvPrivilege.Item(0, 46).Value = "List Sale Return"
        dgvPrivilege.Item(0, 47).Value = "List Split Product"
        dgvPrivilege.Item(0, 48).Value = "Stock Card"
        dgvPrivilege.Item(0, 49).Value = "Notification"
        dgvPrivilege.Item(0, 50).Value = "Payable Payment"
        dgvPrivilege.Item(0, 51).Value = "Payable Transaction"
        dgvPrivilege.Item(0, 52).Value = "Receivable Payment"
        dgvPrivilege.Item(0, 53).Value = "Receivable Transaction"
        dgvPrivilege.Item(0, 54).Value = "Entry Split Product"
        dgvPrivilege.Item(0, 55).Value = "Entry Opening Balance"
        dgvPrivilege.Item(0, 56).Value = "Entry Product"
        dgvPrivilege.Item(0, 57).Value = "List Selling by Salesman"
        dgvPrivilege.Item(0, 58).Value = "List Product Sold by Salesman"
        dgvPrivilege.Item(0, 59).Value = "Entry Salesman"
        dgvPrivilege.Item(0, 60).Value = "List Purchasing Price"
        dgvPrivilege.Item(0, 61).Value = "List Product Movement"
        dgvPrivilege.Item(0, 62).Value = "Report Purchase General"
        dgvPrivilege.Item(0, 63).Value = "Report Purchase By Vendor"
        dgvPrivilege.Item(0, 64).Value = "Report Purchase By Payment Method"
        dgvPrivilege.Item(0, 65).Value = "Report Purchase By Status"
        dgvPrivilege.Item(0, 66).Value = "Report Sale By Customer"
        dgvPrivilege.Item(0, 67).Value = "Report Sale By Payment Method"
        dgvPrivilege.Item(0, 68).Value = "Report Sale By Status"
        dgvPrivilege.Item(0, 69).Value = "Balance Sheet Statement"
        dgvPrivilege.Item(0, 70).Value = "Profit & Loss Statement"

        GetAccessRightData()

        cmdCheckAll.Enabled = False
        dgvPrivilege.ReadOnly = True
        DisableInputBox(Me)
        ACCESS_PRIVILEGEBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        tmpChange = False
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        If ACCESS_PRIVILEGE_NAMETextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If
        tmpKey = ACCESS_PRIVILEGE_NAMETextBox.Text

        If tmpSaveMode = "Insert" Then
            xComm = New SqlCommand("SP_ACCESS_PRIVILEGE", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "I")
            xComm.Parameters.AddWithValue("@ACCESS_PRIVILEGE_ID", 0)
            xComm.Parameters.AddWithValue("@ACCESS_PRIVILEGE_NAME", ACCESS_PRIVILEGE_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", Now)
            xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", DateSerial(4000, 12, 31))
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", UserId)
            xComm.Parameters.AddWithValue("@INPUT_DATE", Now)
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", 0)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", DateSerial(4000, 12, 31))
            xComm.ExecuteNonQuery()
            For i As Integer = 0 To dgvPrivilege.RowCount - 1
                Dim tmpAdd, tmpEdit, tmpDel, tmpOpen, tmpPrint As String
                If dgvPrivilege.Item("ALLOW_ADD", i).Value = True Then
                    tmpAdd = "Yes"
                Else
                    tmpAdd = "No"
                End If

                If dgvPrivilege.Item("ALLOW_EDIT", i).Value = True Then
                    tmpEdit = "Yes"
                Else
                    tmpEdit = "No"
                End If

                If dgvPrivilege.Item("ALLOW_DELETE", i).Value = True Then
                    tmpDel = "Yes"
                Else
                    tmpDel = "No"
                End If

                If dgvPrivilege.Item("ALLOW_OPEN", i).Value = True Then
                    tmpOpen = "Yes"
                Else
                    tmpOpen = "No"
                End If

                If dgvPrivilege.Item("ALLOW_PRINT", i).Value = True Then
                    tmpPrint = "Yes"
                Else
                    tmpPrint = "No"
                End If
                xComm = New SqlCommand("SP_ACCESS_RIGHT", xConn)
                xComm.CommandType = CommandType.StoredProcedure
                xComm.Parameters.AddWithValue("@METHOD", "I")
                xComm.Parameters.AddWithValue("@ACCESS_RIGHT_ID", 0)
                xComm.Parameters.AddWithValue("@ACCESS_PRIVILEGE_ID", 0)
                xComm.Parameters.AddWithValue("@FORM_NAME", dgvPrivilege.Item("FORM_NAME", i).Value)
                xComm.Parameters.AddWithValue("@ALLOW_ADD", tmpAdd)
                xComm.Parameters.AddWithValue("@ALLOW_EDIT", tmpEdit)
                xComm.Parameters.AddWithValue("@ALLOW_DELETE", tmpDel)
                xComm.Parameters.AddWithValue("@ALLOW_OPEN", tmpOpen)
                xComm.Parameters.AddWithValue("@ALLOW_PRINT", tmpPrint)
                xComm.Parameters.AddWithValue("@USER_ID_INPUT", UserId)
                xComm.Parameters.AddWithValue("@INPUT_DATE", Now)
                xComm.Parameters.AddWithValue("@USER_ID_UPDATE", 0)
                xComm.Parameters.AddWithValue("@UPDATE_DATE", DateSerial(4000, 12, 31))
                xComm.ExecuteNonQuery()
            Next
        ElseIf tmpSaveMode = "Update" Then
            xComm = New SqlCommand("SP_ACCESS_PRIVILEGE", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "U")
            xComm.Parameters.AddWithValue("@ACCESS_PRIVILEGE_ID", ACCESS_PRIVILEGEBindingSource.Current("ACCESS_PRIVILEGE_ID"))
            xComm.Parameters.AddWithValue("@ACCESS_PRIVILEGE_NAME", ACCESS_PRIVILEGE_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", ACCESS_PRIVILEGEBindingSource.Current("EFFECTIVE_START_DATE"))
            xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", ACCESS_PRIVILEGEBindingSource.Current("EFFECTIFE_END_DATE"))
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", ACCESS_PRIVILEGEBindingSource.Current("USER_ID_INPUT"))
            xComm.Parameters.AddWithValue("@INPUT_DATE", ACCESS_PRIVILEGEBindingSource.Current("INPUT_DATE"))
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", UserId)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", Now)
            xComm.ExecuteNonQuery()
            For i As Integer = 0 To dgvPrivilege.RowCount - 1
                Dim tmpAdd, tmpEdit, tmpDel, tmpOpen, tmpPrint As String
                If dgvPrivilege.Item("ALLOW_ADD", i).Value = True Then
                    tmpAdd = "Yes"
                Else
                    tmpAdd = "No"
                End If

                If dgvPrivilege.Item("ALLOW_EDIT", i).Value = True Then
                    tmpEdit = "Yes"
                Else
                    tmpEdit = "No"
                End If

                If dgvPrivilege.Item("ALLOW_DELETE", i).Value = True Then
                    tmpDel = "Yes"
                Else
                    tmpDel = "No"
                End If

                If dgvPrivilege.Item("ALLOW_OPEN", i).Value = True Then
                    tmpOpen = "Yes"
                Else
                    tmpOpen = "No"
                End If

                If dgvPrivilege.Item("ALLOW_PRINT", i).Value = True Then
                    tmpPrint = "Yes"
                Else
                    tmpPrint = "No"
                End If
                xComm = New SqlCommand("SP_ACCESS_RIGHT", xConn)
                xComm.CommandType = CommandType.StoredProcedure
                xComm.Parameters.AddWithValue("@METHOD", "U")
                xComm.Parameters.AddWithValue("@ACCESS_RIGHT_ID", 0)
                xComm.Parameters.AddWithValue("@ACCESS_PRIVILEGE_ID", ACCESS_PRIVILEGEBindingSource.Current("ACCESS_PRIVILEGE_ID"))
                xComm.Parameters.AddWithValue("@FORM_NAME", dgvPrivilege.Item("FORM_NAME", i).Value)
                xComm.Parameters.AddWithValue("@ALLOW_ADD", tmpAdd)
                xComm.Parameters.AddWithValue("@ALLOW_EDIT", tmpEdit)
                xComm.Parameters.AddWithValue("@ALLOW_DELETE", tmpDel)
                xComm.Parameters.AddWithValue("@ALLOW_OPEN", tmpOpen)
                xComm.Parameters.AddWithValue("@ALLOW_PRINT", tmpPrint)
                xComm.Parameters.AddWithValue("@USER_ID_INPUT", UserId)
                xComm.Parameters.AddWithValue("@INPUT_DATE", Now)
                xComm.Parameters.AddWithValue("@USER_ID_UPDATE", 0)
                xComm.Parameters.AddWithValue("@UPDATE_DATE", DateSerial(4000, 12, 31))
                xComm.ExecuteNonQuery()
            Next
        End If
        tmpSaveMode = ""

        DisableInputBox(Me)
        ACCESS_PRIVILEGEBindingSource.CancelEdit()
        dgvPrivilege.ReadOnly = True
        ACCESS_PRIVILEGEBindingNavigator.Enabled = True
        cmdSearch.Visible = True
        cmdAdd.Enabled = False
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        cmdCheckAll.Enabled = False
        GetAccessPrivilegeData()
        'Me.ACCESS_PRIVILEGETableAdapter.Fill(Me.DS_ACCESS_PRIVILEGE.ACCESS_PRIVILEGE)
        'ACCESS_PRIVILEGEBindingSource_CurrentChanged1(Nothing, Nothing)
        ACCESS_PRIVILEGEBindingSource.Position = ACCESS_PRIVILEGEBindingSource.Find("ACCESS_PRIVILEGE_NAME", tmpKey)
        If Language = "Indonesian" Then
            MsgBox("Data Berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
        End If

    End Sub

    Private Sub cmdCheckAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCheckAll.Click
        If cmdCheckAll.Text = "Check All" Then
            For i As Integer = 0 To dgvPrivilege.RowCount - 1
                dgvPrivilege.Item("ALLOW_ADD", i).Value = True
                dgvPrivilege.Item("ALLOW_EDIT", i).Value = True
                dgvPrivilege.Item("ALLOW_DELETE", i).Value = True
                dgvPrivilege.Item("ALLOW_OPEN", i).Value = True
                dgvPrivilege.Item("ALLOW_PRINT", i).Value = True
            Next
            cmdCheckAll.Text = "Uncheck All"
        ElseIf cmdCheckAll.Text = "Uncheck All" Then
            For i As Integer = 0 To dgvPrivilege.RowCount - 1
                dgvPrivilege.Item("ALLOW_ADD", i).Value = False
                dgvPrivilege.Item("ALLOW_EDIT", i).Value = False
                dgvPrivilege.Item("ALLOW_DELETE", i).Value = False
                dgvPrivilege.Item("ALLOW_OPEN", i).Value = False
                dgvPrivilege.Item("ALLOW_PRINT", i).Value = False
            Next
            cmdCheckAll.Text = "Check All"
        End If
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If AllowAdd = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Insert"

        EnableInputBox(Me)
        ACCESS_PRIVILEGEBindingSource.AddNew()

        lblStatus.Visible = False
        ACCESS_PRIVILEGEBindingNavigator.Enabled = False
        cmdSearch.Visible = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        tmpChange = False
        dgvPrivilege.ReadOnly = False
        ACCESS_PRIVILEGE_NAMETextBox.Enabled = True
        cmdCheckAll.Enabled = True
        For i As Integer = 0 To dgvPrivilege.RowCount - 1
            dgvPrivilege.Item("ALLOW_ADD", i).Value = False
            dgvPrivilege.Item("ALLOW_EDIT", i).Value = False
            dgvPrivilege.Item("ALLOW_DELETE", i).Value = False
            dgvPrivilege.Item("ALLOW_OPEN", i).Value = False
            dgvPrivilege.Item("ALLOW_PRINT", i).Value = False
        Next
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        If AllowEdit = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Update"

        EnableInputBox(Me)
        dgvPrivilege.ReadOnly = False
        ACCESS_PRIVILEGEBindingNavigator.Enabled = False
        cmdSearch.Visible = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        tmpChange = False
        cmdCheckAll.Enabled = True
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        tmpSaveMode = ""

        DisableInputBox(Me)
        ACCESS_PRIVILEGEBindingSource.CancelEdit()
        dgvPrivilege.ReadOnly = True
        ACCESS_PRIVILEGEBindingNavigator.Enabled = True
        cmdSearch.Visible = True
        cmdCheckAll.Enabled = False
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        tmpChange = False
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If AllowDelete = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If
        xComm = New SqlCommand("GET_ACCESS_PRIVILEGE_USER", xConn)
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@ACCESS_PRIVILEGE", ACCESS_PRIVILEGEBindingSource.Current("ACCESS_PRIVILEDGE_ID"))
        xdreader = xComm.ExecuteReader
        xdreader.Read()
        Dim chkPriviledgeUser As Boolean
        If xdreader.HasRows Then
            chkPriviledgeUser = True
        Else
            chkPriviledgeUser = False
        End If
        'ACCESS_PRIVILEDGETableAdapter.GET_ACCESS_PRIVILEDGE_USER(ACCESS_PRIVILEDGEBindingSource.Current("ACCESS_PRIVILEDGE_ID"))
        If chkPriviledgeUser = True Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menghapus Akses ini !" & vbCrLf & "Akses ini telah digunakan di User yang lain !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot delete this Access Priviledge !" & vbCrLf & _
                       "Product is used by another User !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If Language = "Indonesian" Then
            If MsgBox("Hapus Akses ?" & vbCrLf & ACCESS_PRIVILEGE_NAMETextBox.Text, _
                  MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
        Else
            If MsgBox("Delete Access Priviledge ?" & vbCrLf & ACCESS_PRIVILEGE_NAMETextBox.Text, _
                  MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
        End If
        xComm = New SqlCommand("SP_ACCESS_PRIVILEGE", xConn)
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@METHOD", "D")
        xComm.Parameters.AddWithValue("@ACCESS_PRIVILEGE_ID", ACCESS_PRIVILEGEBindingSource.Current("ACCESS_PRIVILEGE_ID"))
        xComm.Parameters.AddWithValue("@ACCESS_PRIVILEGE_NAME", ACCESS_PRIVILEGE_NAMETextBox.Text)
        xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", Now)
        xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", Now)
        xComm.Parameters.AddWithValue("@USER_ID_INPUT", UserId)
        xComm.Parameters.AddWithValue("@INPUT_DATE", Now)
        xComm.Parameters.AddWithValue("@USER_ID_UPDATE", 0)
        xComm.Parameters.AddWithValue("@UPDATE_DATE", DateSerial(4000, 12, 31))
        xComm.ExecuteNonQuery()
        ' Me.ACCESS_PRIVILEDGETableAdapter.Fill(Me.DS_ACCESS_PRIVILEDGE.ACCESS_PRIVILEDGE)
        GetAccessPrivilegeData()
        Try
            If ACCESS_PRIVILEGEBindingSource.Current("EFFECTIFE_END_DATE") < Now Then
                lblStatus.Visible = True
                lblStatus.Text = "DELETED"
                cmdDelete.Enabled = False
                cmdEdit.Enabled = False
            Else
                lblStatus.Visible = False
            End If
        Catch
        End Try

        ACCESS_PRIVILEGEBindingSource.Position = ACCESS_PRIVILEGEBindingSource.Find("ACCESS_PRIVILEGE_NAME", ACCESS_PRIVILEGE_NAMETextBox.Text)
        MsgBox("Data succesful Deleted", MsgBoxStyle.Information, "DMI Retail")
    End Sub

    Private Sub cmdSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        frmListAccessPrivilege.ShowDialog()
    End Sub

    Private Sub BindingNavigatorPositionItem_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BindingNavigatorPositionItem.TextChanged
        If ACCESS_PRIVILEGE_NAMETextBox.Text <> "" Then
            For i As Integer = 0 To dgvPrivilege.RowCount - 1
                Dim tmpAdd As String = ""
                Dim tmpEdit As String = ""
                Dim tmpOpen As String = ""
                Dim tmpDelete As String = ""
                Dim tmpPrint As String = ""

                xComm = New SqlCommand("SP_GET_ACCESS_PRIVILEGE", xConn)
                xComm.CommandType = CommandType.StoredProcedure
                xComm.Parameters.AddWithValue("@ACCESS_PRIVILEGE_ID", ACCESS_PRIVILEGEBindingSource.Current("ACCESS_PRIVILEGE_ID"))

                xComm.Parameters.AddWithValue("@FORM_NAME", dgvPrivilege.Item("FORM_NAME", i).Value)
                If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                xdreader = xComm.ExecuteReader
                xdreader.Read()
                'ACCESS_PRIVILEDGETableAdapter.SP_GET_ACCESS_PRIVILEDGE(ACCESS_PRIVILEDGEBindingSource.Current("ACCESS_PRIVILEDGE_ID"), dgvPriviledge.Item("FORM_NAME", i).Value, tmpAdd, tmpEdit, tmpOpen, tmpDelete, tmpPrint)
                If xdreader.HasRows Then

                    tmpAdd = Trim(xdreader.Item("TAMBAH"))
                    tmpEdit = Trim(xdreader.Item("EDIT"))
                    tmpDelete = Trim(xdreader.Item("HAPUS"))
                    tmpOpen = Trim(xdreader.Item("BUKA"))
                    tmpPrint = Trim(xdreader.Item("CETAK"))
                    If tmpAdd = "No" Then
                        dgvPrivilege.Item("ALLOW_ADD", i).Value = False
                    Else
                        dgvPrivilege.Item("ALLOW_ADD", i).Value = True
                    End If
                    If tmpEdit = "No" Then
                        dgvPrivilege.Item("ALLOW_EDIT", i).Value = False
                    Else
                        dgvPrivilege.Item("ALLOW_EDIT", i).Value = True
                    End If
                    If tmpDelete = "No" Then
                        dgvPrivilege.Item("ALLOW_DELETE", i).Value = False
                    Else
                        dgvPrivilege.Item("ALLOW_DELETE", i).Value = True
                    End If
                    If tmpOpen = "No" Then
                        dgvPrivilege.Item("ALLOW_OPEN", i).Value = False
                    Else
                        dgvPrivilege.Item("ALLOW_OPEN", i).Value = True
                    End If
                    If tmpPrint = "No" Then
                        dgvPrivilege.Item("ALLOW_PRINT", i).Value = False
                    Else
                        dgvPrivilege.Item("ALLOW_PRINT", i).Value = True
                    End If

                End If
            Next
            If ACCESS_PRIVILEGEBindingSource.Current("EFFECTIFE_END_DATE") < Now Then
                lblStatus.Visible = True
                lblStatus.Text = "DELETED"
                cmdDelete.Enabled = False
                cmdEdit.Enabled = False
            Else
                lblStatus.Visible = False
                cmdDelete.Enabled = True
                cmdEdit.Enabled = True
            End If
        End If

        cmdCheckAll.Enabled = False
        dgvPrivilege.ReadOnly = True
        DisableInputBox(Me)
        ACCESS_PRIVILEGEBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        tmpChange = False
    End Sub

End Class