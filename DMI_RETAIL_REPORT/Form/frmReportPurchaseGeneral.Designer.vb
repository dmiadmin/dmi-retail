﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportPurchaseGeneral
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReportPurchaseGeneral))
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblTo = New System.Windows.Forms.Label()
        Me.dtpPeriod2 = New System.Windows.Forms.DateTimePicker()
        Me.cmdReport = New System.Windows.Forms.Button()
        Me.dtpPeriod = New System.Windows.Forms.DateTimePicker()
        Me.cmdGenerate = New System.Windows.Forms.Button()
        Me.lblPeriod = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dgvRptPurchaseGeneral = New System.Windows.Forms.DataGridView()
        Me.PURCHASE_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INVOICE_TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NO_OF_ITEM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GRAND_TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PERIOD1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PERIOD2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SP_PURCHASE_REPORT_MONTHLYBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_PURCHASE = New DMI_RETAIL_REPORT.DS_PURCHASE()
        Me.SP_PURCHASE_REPORT_DAILYBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblInvoice = New System.Windows.Forms.Label()
        Me.lblItem = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.lblNoOfTrans = New System.Windows.Forms.Label()
        Me.SP_PURCHASE_REPORT_DAILYTableAdapter = New DMI_RETAIL_REPORT.DS_PURCHASETableAdapters.SP_PURCHASE_REPORT_DAILYTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_REPORT.DS_PURCHASETableAdapters.TableAdapterManager()
        Me.SP_PURCHASE_REPORT_MONTHLYTableAdapter = New DMI_RETAIL_REPORT.DS_PURCHASETableAdapters.SP_PURCHASE_REPORT_MONTHLYTableAdapter()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvRptPurchaseGeneral, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_PURCHASE_REPORT_MONTHLYBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_PURCHASE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_PURCHASE_REPORT_DAILYBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblTo)
        Me.GroupBox1.Controls.Add(Me.dtpPeriod2)
        Me.GroupBox1.Controls.Add(Me.cmdReport)
        Me.GroupBox1.Controls.Add(Me.dtpPeriod)
        Me.GroupBox1.Controls.Add(Me.cmdGenerate)
        Me.GroupBox1.Controls.Add(Me.lblPeriod)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(547, 126)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        '
        'lblTo
        '
        Me.lblTo.Location = New System.Drawing.Point(281, 30)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(32, 19)
        Me.lblTo.TabIndex = 15
        Me.lblTo.Text = "To"
        Me.lblTo.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'dtpPeriod2
        '
        Me.dtpPeriod2.Location = New System.Drawing.Point(325, 27)
        Me.dtpPeriod2.Name = "dtpPeriod2"
        Me.dtpPeriod2.ShowUpDown = True
        Me.dtpPeriod2.Size = New System.Drawing.Size(143, 22)
        Me.dtpPeriod2.TabIndex = 14
        '
        'cmdReport
        '
        Me.cmdReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdReport.Image = Global.DMI_RETAIL_REPORT.My.Resources.Resources.Printer_small
        Me.cmdReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdReport.Location = New System.Drawing.Point(451, 96)
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(90, 24)
        Me.cmdReport.TabIndex = 13
        Me.cmdReport.Text = "Print [F12]"
        Me.cmdReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.cmdReport.UseVisualStyleBackColor = True
        '
        'dtpPeriod
        '
        Me.dtpPeriod.Location = New System.Drawing.Point(129, 27)
        Me.dtpPeriod.Name = "dtpPeriod"
        Me.dtpPeriod.ShowUpDown = True
        Me.dtpPeriod.Size = New System.Drawing.Size(143, 22)
        Me.dtpPeriod.TabIndex = 3
        '
        'cmdGenerate
        '
        Me.cmdGenerate.Image = CType(resources.GetObject("cmdGenerate.Image"), System.Drawing.Image)
        Me.cmdGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdGenerate.Location = New System.Drawing.Point(228, 65)
        Me.cmdGenerate.Name = "cmdGenerate"
        Me.cmdGenerate.Size = New System.Drawing.Size(99, 39)
        Me.cmdGenerate.TabIndex = 9
        Me.cmdGenerate.Text = "Generate"
        Me.cmdGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdGenerate.UseVisualStyleBackColor = True
        '
        'lblPeriod
        '
        Me.lblPeriod.Location = New System.Drawing.Point(7, 30)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(116, 19)
        Me.lblPeriod.TabIndex = 4
        Me.lblPeriod.Text = "Time Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dgvRptPurchaseGeneral)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 132)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(546, 299)
        Me.GroupBox2.TabIndex = 14
        Me.GroupBox2.TabStop = False
        '
        'dgvRptPurchaseGeneral
        '
        Me.dgvRptPurchaseGeneral.AllowUserToAddRows = False
        Me.dgvRptPurchaseGeneral.AllowUserToDeleteRows = False
        Me.dgvRptPurchaseGeneral.AutoGenerateColumns = False
        Me.dgvRptPurchaseGeneral.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRptPurchaseGeneral.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvRptPurchaseGeneral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRptPurchaseGeneral.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PURCHASE_DATE, Me.INVOICE_TOTAL, Me.NO_OF_ITEM, Me.GRAND_TOTAL, Me.PERIOD1DataGridViewTextBoxColumn, Me.PERIOD2DataGridViewTextBoxColumn})
        Me.dgvRptPurchaseGeneral.DataSource = Me.SP_PURCHASE_REPORT_MONTHLYBindingSource
        Me.dgvRptPurchaseGeneral.Location = New System.Drawing.Point(9, 17)
        Me.dgvRptPurchaseGeneral.Name = "dgvRptPurchaseGeneral"
        Me.dgvRptPurchaseGeneral.ReadOnly = True
        Me.dgvRptPurchaseGeneral.Size = New System.Drawing.Size(531, 276)
        Me.dgvRptPurchaseGeneral.TabIndex = 0
        '
        'PURCHASE_DATE
        '
        Me.PURCHASE_DATE.DataPropertyName = "PURCHASE_DATE"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "dd-MMM-yyyy"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.PURCHASE_DATE.DefaultCellStyle = DataGridViewCellStyle7
        Me.PURCHASE_DATE.HeaderText = "Purchase Date"
        Me.PURCHASE_DATE.Name = "PURCHASE_DATE"
        Me.PURCHASE_DATE.ReadOnly = True
        '
        'INVOICE_TOTAL
        '
        Me.INVOICE_TOTAL.DataPropertyName = "INVOICE_TOTAL"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.INVOICE_TOTAL.DefaultCellStyle = DataGridViewCellStyle8
        Me.INVOICE_TOTAL.HeaderText = "Total Invoice"
        Me.INVOICE_TOTAL.Name = "INVOICE_TOTAL"
        Me.INVOICE_TOTAL.ReadOnly = True
        '
        'NO_OF_ITEM
        '
        Me.NO_OF_ITEM.DataPropertyName = "NO_OF_ITEM"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.NO_OF_ITEM.DefaultCellStyle = DataGridViewCellStyle9
        Me.NO_OF_ITEM.HeaderText = "Total Item"
        Me.NO_OF_ITEM.Name = "NO_OF_ITEM"
        Me.NO_OF_ITEM.ReadOnly = True
        '
        'GRAND_TOTAL
        '
        Me.GRAND_TOTAL.DataPropertyName = "GRAND_TOTAL"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N0"
        DataGridViewCellStyle10.NullValue = Nothing
        Me.GRAND_TOTAL.DefaultCellStyle = DataGridViewCellStyle10
        Me.GRAND_TOTAL.HeaderText = "Grand Total"
        Me.GRAND_TOTAL.Name = "GRAND_TOTAL"
        Me.GRAND_TOTAL.ReadOnly = True
        '
        'PERIOD1DataGridViewTextBoxColumn
        '
        Me.PERIOD1DataGridViewTextBoxColumn.DataPropertyName = "PERIOD1"
        Me.PERIOD1DataGridViewTextBoxColumn.HeaderText = "PERIOD1"
        Me.PERIOD1DataGridViewTextBoxColumn.Name = "PERIOD1DataGridViewTextBoxColumn"
        Me.PERIOD1DataGridViewTextBoxColumn.ReadOnly = True
        Me.PERIOD1DataGridViewTextBoxColumn.Visible = False
        '
        'PERIOD2DataGridViewTextBoxColumn
        '
        Me.PERIOD2DataGridViewTextBoxColumn.DataPropertyName = "PERIOD2"
        Me.PERIOD2DataGridViewTextBoxColumn.HeaderText = "PERIOD2"
        Me.PERIOD2DataGridViewTextBoxColumn.Name = "PERIOD2DataGridViewTextBoxColumn"
        Me.PERIOD2DataGridViewTextBoxColumn.ReadOnly = True
        Me.PERIOD2DataGridViewTextBoxColumn.Visible = False
        '
        'SP_PURCHASE_REPORT_MONTHLYBindingSource
        '
        Me.SP_PURCHASE_REPORT_MONTHLYBindingSource.DataMember = "SP_PURCHASE_REPORT_MONTHLY"
        Me.SP_PURCHASE_REPORT_MONTHLYBindingSource.DataSource = Me.DS_PURCHASE
        '
        'DS_PURCHASE
        '
        Me.DS_PURCHASE.DataSetName = "DS_PURCHASE"
        Me.DS_PURCHASE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SP_PURCHASE_REPORT_DAILYBindingSource
        '
        Me.SP_PURCHASE_REPORT_DAILYBindingSource.DataMember = "SP_PURCHASE_REPORT_DAILY"
        Me.SP_PURCHASE_REPORT_DAILYBindingSource.DataSource = Me.DS_PURCHASE
        '
        'lblInvoice
        '
        Me.lblInvoice.AutoSize = True
        Me.lblInvoice.Location = New System.Drawing.Point(361, 520)
        Me.lblInvoice.Name = "lblInvoice"
        Me.lblInvoice.Size = New System.Drawing.Size(45, 15)
        Me.lblInvoice.TabIndex = 23
        Me.lblInvoice.Text = "Label1"
        '
        'lblItem
        '
        Me.lblItem.AutoSize = True
        Me.lblItem.Location = New System.Drawing.Point(13, 520)
        Me.lblItem.Name = "lblItem"
        Me.lblItem.Size = New System.Drawing.Size(45, 15)
        Me.lblItem.TabIndex = 22
        Me.lblItem.Text = "Label1"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(361, 489)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(48, 15)
        Me.lblTotal.TabIndex = 21
        Me.lblTotal.Text = "Label2"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblNoOfTrans
        '
        Me.lblNoOfTrans.AutoSize = True
        Me.lblNoOfTrans.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoOfTrans.Location = New System.Drawing.Point(13, 489)
        Me.lblNoOfTrans.Name = "lblNoOfTrans"
        Me.lblNoOfTrans.Size = New System.Drawing.Size(48, 15)
        Me.lblNoOfTrans.TabIndex = 20
        Me.lblNoOfTrans.Text = "Label1"
        '
        'SP_PURCHASE_REPORT_DAILYTableAdapter
        '
        Me.SP_PURCHASE_REPORT_DAILYTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.PRODUCTTableAdapter = Nothing
        Me.TableAdapterManager.PURCHASE_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.PURCHASETableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_REPORT.DS_PURCHASETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.VENDORTableAdapter = Nothing
        Me.TableAdapterManager.WAREHOUSETableAdapter = Nothing
        '
        'SP_PURCHASE_REPORT_MONTHLYTableAdapter
        '
        Me.SP_PURCHASE_REPORT_MONTHLYTableAdapter.ClearBeforeFill = True
        '
        'frmReportPurchaseGeneral
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(570, 544)
        Me.Controls.Add(Me.lblInvoice)
        Me.Controls.Add(Me.lblItem)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.lblNoOfTrans)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmReportPurchaseGeneral"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Report Purchase General"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgvRptPurchaseGeneral, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_PURCHASE_REPORT_MONTHLYBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_PURCHASE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_PURCHASE_REPORT_DAILYBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents dtpPeriod2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmdReport As System.Windows.Forms.Button
    Friend WithEvents dtpPeriod As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmdGenerate As System.Windows.Forms.Button
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents DS_PURCHASE As DMI_RETAIL_REPORT.DS_PURCHASE
    Friend WithEvents SP_PURCHASE_REPORT_DAILYBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_PURCHASE_REPORT_DAILYTableAdapter As DMI_RETAIL_REPORT.DS_PURCHASETableAdapters.SP_PURCHASE_REPORT_DAILYTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_REPORT.DS_PURCHASETableAdapters.TableAdapterManager
    Friend WithEvents dgvRptPurchaseGeneral As System.Windows.Forms.DataGridView
    Friend WithEvents SP_PURCHASE_REPORT_MONTHLYBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_PURCHASE_REPORT_MONTHLYTableAdapter As DMI_RETAIL_REPORT.DS_PURCHASETableAdapters.SP_PURCHASE_REPORT_MONTHLYTableAdapter
    Friend WithEvents lblInvoice As System.Windows.Forms.Label
    Friend WithEvents lblItem As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents lblNoOfTrans As System.Windows.Forms.Label
    Friend WithEvents PURCHASE_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INVOICE_TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NO_OF_ITEM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GRAND_TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PERIOD1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PERIOD2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
