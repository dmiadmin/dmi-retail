﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSearchReceivableInvoice
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSearchReceivableInvoice))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblNoOfInvoice = New System.Windows.Forms.Label()
        Me.DS_PAYMENT_TRANSACTION = New DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTION()
        Me.SP_RECEIVABLE_INVOICEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_RECEIVABLE_INVOICETableAdapter = New DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.SP_RECEIVABLE_INVOICETableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.TableAdapterManager()
        Me.SP_RECEIVABLE_INVOICEBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.dgvRECEIVABLE_INVOICE = New System.Windows.Forms.DataGridView()
        Me.CUSTOMER_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RECEIPT_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RECEIPT_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL_RECEIVABLE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MATURITY_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblCustomer = New System.Windows.Forms.Label()
        Me.txtCustomerName = New System.Windows.Forms.TextBox()
        Me.txtInvoiceNo = New System.Windows.Forms.TextBox()
        Me.lblInvoice = New System.Windows.Forms.Label()
        CType(Me.DS_PAYMENT_TRANSACTION, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_RECEIVABLE_INVOICEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_RECEIVABLE_INVOICEBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SP_RECEIVABLE_INVOICEBindingNavigator.SuspendLayout()
        CType(Me.dgvRECEIVABLE_INVOICE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblNoOfInvoice
        '
        Me.lblNoOfInvoice.AutoSize = True
        Me.lblNoOfInvoice.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoOfInvoice.Location = New System.Drawing.Point(18, 363)
        Me.lblNoOfInvoice.Name = "lblNoOfInvoice"
        Me.lblNoOfInvoice.Size = New System.Drawing.Size(100, 15)
        Me.lblNoOfInvoice.TabIndex = 2
        Me.lblNoOfInvoice.Text = "No of Invoice :"
        '
        'DS_PAYMENT_TRANSACTION
        '
        Me.DS_PAYMENT_TRANSACTION.DataSetName = "DS_PAYMENT_TRANSACTION"
        Me.DS_PAYMENT_TRANSACTION.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SP_RECEIVABLE_INVOICEBindingSource
        '
        Me.SP_RECEIVABLE_INVOICEBindingSource.DataMember = "SP_RECEIVABLE_INVOICE"
        Me.SP_RECEIVABLE_INVOICEBindingSource.DataSource = Me.DS_PAYMENT_TRANSACTION
        '
        'SP_RECEIVABLE_INVOICETableAdapter
        '
        Me.SP_RECEIVABLE_INVOICETableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.CUSTOMERTableAdapter = Nothing
        Me.TableAdapterManager.PAYABLE_PAYMENTTableAdapter = Nothing
        Me.TableAdapterManager.PAYABLETableAdapter = Nothing
        Me.TableAdapterManager.PAYMENT_METHODTableAdapter = Nothing
        Me.TableAdapterManager.RECEIVABLE_PAYMENTTableAdapter = Nothing
        Me.TableAdapterManager.RECEIVABLETableAdapter = Nothing
        Me.TableAdapterManager.SALETableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'SP_RECEIVABLE_INVOICEBindingNavigator
        '
        Me.SP_RECEIVABLE_INVOICEBindingNavigator.AddNewItem = Nothing
        Me.SP_RECEIVABLE_INVOICEBindingNavigator.BindingSource = Me.SP_RECEIVABLE_INVOICEBindingSource
        Me.SP_RECEIVABLE_INVOICEBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.SP_RECEIVABLE_INVOICEBindingNavigator.DeleteItem = Nothing
        Me.SP_RECEIVABLE_INVOICEBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.SP_RECEIVABLE_INVOICEBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.SP_RECEIVABLE_INVOICEBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.SP_RECEIVABLE_INVOICEBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.SP_RECEIVABLE_INVOICEBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.SP_RECEIVABLE_INVOICEBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.SP_RECEIVABLE_INVOICEBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.SP_RECEIVABLE_INVOICEBindingNavigator.Name = "SP_RECEIVABLE_INVOICEBindingNavigator"
        Me.SP_RECEIVABLE_INVOICEBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.SP_RECEIVABLE_INVOICEBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.SP_RECEIVABLE_INVOICEBindingNavigator.Size = New System.Drawing.Size(590, 25)
        Me.SP_RECEIVABLE_INVOICEBindingNavigator.TabIndex = 3
        Me.SP_RECEIVABLE_INVOICEBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'dgvRECEIVABLE_INVOICE
        '
        Me.dgvRECEIVABLE_INVOICE.AllowUserToAddRows = False
        Me.dgvRECEIVABLE_INVOICE.AllowUserToDeleteRows = False
        Me.dgvRECEIVABLE_INVOICE.AllowUserToResizeRows = False
        Me.dgvRECEIVABLE_INVOICE.AutoGenerateColumns = False
        Me.dgvRECEIVABLE_INVOICE.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRECEIVABLE_INVOICE.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvRECEIVABLE_INVOICE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvRECEIVABLE_INVOICE.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CUSTOMER_NAME, Me.RECEIPT_ID, Me.RECEIPT_NO, Me.TOTAL_RECEIVABLE, Me.MATURITY_DATE})
        Me.dgvRECEIVABLE_INVOICE.DataSource = Me.SP_RECEIVABLE_INVOICEBindingSource
        Me.dgvRECEIVABLE_INVOICE.Location = New System.Drawing.Point(12, 94)
        Me.dgvRECEIVABLE_INVOICE.Name = "dgvRECEIVABLE_INVOICE"
        Me.dgvRECEIVABLE_INVOICE.ReadOnly = True
        Me.dgvRECEIVABLE_INVOICE.RowHeadersWidth = 30
        Me.dgvRECEIVABLE_INVOICE.Size = New System.Drawing.Size(566, 259)
        Me.dgvRECEIVABLE_INVOICE.TabIndex = 3
        '
        'CUSTOMER_NAME
        '
        Me.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME"
        Me.CUSTOMER_NAME.HeaderText = "Customer Name"
        Me.CUSTOMER_NAME.Name = "CUSTOMER_NAME"
        Me.CUSTOMER_NAME.ReadOnly = True
        Me.CUSTOMER_NAME.Width = 160
        '
        'RECEIPT_ID
        '
        Me.RECEIPT_ID.DataPropertyName = "RECEIPT_ID"
        Me.RECEIPT_ID.HeaderText = "RECEIPT_ID"
        Me.RECEIPT_ID.Name = "RECEIPT_ID"
        Me.RECEIPT_ID.ReadOnly = True
        Me.RECEIPT_ID.Visible = False
        '
        'RECEIPT_NO
        '
        Me.RECEIPT_NO.DataPropertyName = "RECEIPT_NO"
        Me.RECEIPT_NO.HeaderText = "Invoice No."
        Me.RECEIPT_NO.Name = "RECEIPT_NO"
        Me.RECEIPT_NO.ReadOnly = True
        Me.RECEIPT_NO.Width = 110
        '
        'TOTAL_RECEIVABLE
        '
        Me.TOTAL_RECEIVABLE.DataPropertyName = "TOTAL_RECEIVABLE"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.TOTAL_RECEIVABLE.DefaultCellStyle = DataGridViewCellStyle2
        Me.TOTAL_RECEIVABLE.HeaderText = "Total Amount"
        Me.TOTAL_RECEIVABLE.Name = "TOTAL_RECEIVABLE"
        Me.TOTAL_RECEIVABLE.ReadOnly = True
        Me.TOTAL_RECEIVABLE.Width = 120
        '
        'MATURITY_DATE
        '
        Me.MATURITY_DATE.DataPropertyName = "MATURITY_DATE"
        DataGridViewCellStyle3.Format = "dd-MMM-yyyy"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.MATURITY_DATE.DefaultCellStyle = DataGridViewCellStyle3
        Me.MATURITY_DATE.HeaderText = "Maturity Date"
        Me.MATURITY_DATE.Name = "MATURITY_DATE"
        Me.MATURITY_DATE.ReadOnly = True
        Me.MATURITY_DATE.Width = 120
        '
        'lblCustomer
        '
        Me.lblCustomer.Location = New System.Drawing.Point(12, 41)
        Me.lblCustomer.Name = "lblCustomer"
        Me.lblCustomer.Size = New System.Drawing.Size(109, 19)
        Me.lblCustomer.TabIndex = 4
        Me.lblCustomer.Text = "Customer Name"
        Me.lblCustomer.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtCustomerName
        '
        Me.txtCustomerName.Location = New System.Drawing.Point(127, 38)
        Me.txtCustomerName.Name = "txtCustomerName"
        Me.txtCustomerName.Size = New System.Drawing.Size(166, 22)
        Me.txtCustomerName.TabIndex = 1
        '
        'txtInvoiceNo
        '
        Me.txtInvoiceNo.Location = New System.Drawing.Point(127, 66)
        Me.txtInvoiceNo.Name = "txtInvoiceNo"
        Me.txtInvoiceNo.Size = New System.Drawing.Size(166, 22)
        Me.txtInvoiceNo.TabIndex = 2
        '
        'lblInvoice
        '
        Me.lblInvoice.Location = New System.Drawing.Point(12, 69)
        Me.lblInvoice.Name = "lblInvoice"
        Me.lblInvoice.Size = New System.Drawing.Size(109, 19)
        Me.lblInvoice.TabIndex = 7
        Me.lblInvoice.Text = "Invoice No."
        Me.lblInvoice.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'frmSearchReceivableInvoice
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(590, 396)
        Me.Controls.Add(Me.lblInvoice)
        Me.Controls.Add(Me.txtInvoiceNo)
        Me.Controls.Add(Me.txtCustomerName)
        Me.Controls.Add(Me.lblCustomer)
        Me.Controls.Add(Me.dgvRECEIVABLE_INVOICE)
        Me.Controls.Add(Me.SP_RECEIVABLE_INVOICEBindingNavigator)
        Me.Controls.Add(Me.lblNoOfInvoice)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmSearchReceivableInvoice"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "List Receivable Invoice"
        CType(Me.DS_PAYMENT_TRANSACTION, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_RECEIVABLE_INVOICEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_RECEIVABLE_INVOICEBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SP_RECEIVABLE_INVOICEBindingNavigator.ResumeLayout(False)
        Me.SP_RECEIVABLE_INVOICEBindingNavigator.PerformLayout()
        CType(Me.dgvRECEIVABLE_INVOICE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblNoOfInvoice As System.Windows.Forms.Label
    Friend WithEvents DS_PAYMENT_TRANSACTION As DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTION
    Friend WithEvents SP_RECEIVABLE_INVOICEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_RECEIVABLE_INVOICETableAdapter As DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.SP_RECEIVABLE_INVOICETableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_HUTANG_PIUTANG.DS_PAYMENT_TRANSACTIONTableAdapters.TableAdapterManager
    Friend WithEvents SP_RECEIVABLE_INVOICEBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents dgvRECEIVABLE_INVOICE As System.Windows.Forms.DataGridView
    Friend WithEvents lblCustomer As System.Windows.Forms.Label
    Friend WithEvents txtCustomerName As System.Windows.Forms.TextBox
    Friend WithEvents txtInvoiceNo As System.Windows.Forms.TextBox
    Friend WithEvents lblInvoice As System.Windows.Forms.Label
    Friend WithEvents CUSTOMER_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RECEIPT_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RECEIPT_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL_RECEIVABLE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MATURITY_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
