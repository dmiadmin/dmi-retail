﻿Public Class frmPayablePayment

    Dim tmpPositionVendor, tmpReceipt, tmpGrandTotal As String
    Dim tmpMaturity As Date
    Dim tmpTotal, tmpBalance As Integer
    Dim tmpChange As Boolean
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    Private Sub frmPayablePayment_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If tmpChange = True Then
            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah Anda ingin menyimpan perubahan " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If
            If tmpvar = 6 Then
                cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        End If
    End Sub


    Private Sub frmPayablePayment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_PAYMENT_TRANSACTION.PAYABLE_PAYMENT' table. You can move, or remove it, as needed.
        Me.PAYABLE_PAYMENTTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.PAYABLE_PAYMENT)
        'TODO: This line of code loads data into the 'DS_PAYMENT_TRANSACTION.PAYMENT_METHOD' table. You can move, or remove it, as needed.
        Me.PAYMENT_METHODTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.PAYMENT_METHOD)

        accFormName = Me.Text

        If Language = "Indonesian" Then
            lblVendor.Text = "Nama Supplier :"
            lblReceiptNo.Text = "No Faktur :"
            lblDescription.Text = "Keterangan :"
            lblMarturity.Text = "Jatuh Tempo :"
            lblPayMethod.Text = "Cara Pembayaran :"
            lblPayAmount.Text = "Jumlah Pembayaran :"
            lblPayDate.Text = "Tgl Pembayaran :"
            lblPayNo.Text = "No Pembayaran :"
            lblBalance.Text = "Saldo :"
            cmdAdd.Text = "Tambah"
            cmdEdit.Text = "Ubah"
            cmdUndo.Text = "Batal"
            cmdSave.Text = "Simpan"
            cmdDelete.Text = "Hapus"
            Me.Text = "Input Pembayaran Hutang"
        End If

        DisableInputBox(Me)

        GroupBox1.Enabled = True

        SP_PAYABLE_PAYMENT_DETAILBindingNavigator.Enabled = True

        PAYMENT_METHODComboBox.DataSource = SPPAYABLEPAYMENTHEADERSPPAYABLEPAYMENTDETAILBindingSource
        PAYMENT_METHODComboBox.DisplayMember = "PAYMENT_METHOD"

        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = False

        If PAYMENT_AMOUNTTextBox.Text <> "" Then
            cmdDelete.Enabled = True
        End If

        If PAYMENT_METHODBindingSource Is Nothing Or PAYMENT_METHODBindingSource.Count < 1 Then
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data Metode Pembayaran." & vbCrLf & _
                  "Masukkan setidaknya satu data Metode Pembayaran!", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Payment Method data." & vbCrLf & _
                  "Please input at least one Payment Method data!", MsgBoxStyle.Critical, "DMI Retail")
            End If
        End If

        tmpChange = False
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If Trim(RsAccessPrivilege.Item("ALLOW_ADD").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        EnableInputBox(Me)

        SP_PAYABLE_PAYMENT_DETAILBindingSource.Position = SP_PAYABLE_PAYMENT_DETAILBindingSource.Count
        SP_PAYABLE_PAYMENT_DETAILBindingSource.AddNew()

        SP_PAYABLE_PAYMENT_DETAILTableAdapter.SP_PAYABLE_PAYMENT_GENERATE_NUMBER(txtPaymentNo.Text)

        PAYMENT_METHODComboBox.DataSource = PAYMENT_METHODBindingSource
        PAYMENT_METHODComboBox.ValueMember = "PAYMENT_METHOD_ID"
        PAYMENT_METHODComboBox.DisplayMember = "PAYMENT_METHOD_NAME"

        SP_PAYABLE_PAYMENT_DETAILBindingNavigator.Enabled = False

        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False

        tmpChange = False
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        DisableInputBox(Me)

        'PAYMENT_AMOUNTTextBox.Text = ""

        If RECEIPT_NOTextBox.Text <> "" Then

            Me.SP_PAYABLE_PAYMENT_DETAILTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_PAYABLE_PAYMENT_DETAIL, _
                                                                tmpSearchResult)

            'tmpTotal = 0
            'tmpBalance = 0
            'If PAYMENT_AMOUNTTextBox.Text <> "" Then
            '    For x As Integer = 1 To SP_PAYABLE_PAYMENT_DETAILBindingSource.Count
            '        SP_PAYABLE_PAYMENT_DETAILBindingSource.Position = x
            '        tmpTotal = tmpTotal + SP_PAYABLE_PAYMENT_DETAILBindingSource.Current("PAYMENT_AMOUNT")
            '    Next

            '    tmpBalance = CInt(SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("TOTAL_PAYABLE")) - tmpTotal
            '    BALANCETextBox.Text = tmpBalance
            'Else
            '    BALANCETextBox.Text = SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("TOTAL_PAYABLE")
            'End If
            BALANCETextBox.Text = SP_PAYABLE_PAYMENT_DETAILTableAdapter.SP_PAYABLE_PAYMENT_GET_BALANCE _
                (SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("PAYABLE_ID"))
        End If

        PAYMENT_METHODComboBox.Text = ""

        cmdAdd.Enabled = True
        cmdEdit.Enabled = False
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = False

        If PAYMENT_AMOUNTTextBox.Text <> "" Then cmdDelete.Enabled = True

        SP_PAYABLE_PAYMENT_DETAILBindingNavigator.Enabled = True

        tmpChange = False
    End Sub

    Private Sub cmdSearchVendor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchVendor.Click
        mdlGeneral.tmpSearchMode = "Vendor - Vendor Name"
        frmSearchPayableInvoice.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub

        SP_PAYABLE_PAYMENT_HEADERTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_PAYABLE_PAYMENT_HEADER, tmpSearchResult)
        cmdAdd.Enabled = True
        tmpChange = False
    End Sub

    Private Sub cmdSearchReceipt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearchReceipt.Click
        mdlGeneral.tmpSearchMode = "Vendor - Receipt No"
        frmSearchPayableInvoice.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub

        SP_PAYABLE_PAYMENT_HEADERTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_PAYABLE_PAYMENT_HEADER, tmpSearchResult)
        cmdAdd.Enabled = True
        tmpChange = False
    End Sub

    Private Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Dim TMPDATE As Date
        TMPDATE = DateSerial(PAYMENT_DATEDateTimePicker.Value.Year, PAYMENT_DATEDateTimePicker.Value.Month, PAYMENT_DATEDateTimePicker.Value.Day)

        If VENDOR_NAMETextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Tolong isi Nama Supplier !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please fill Vendor Name !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        ElseIf RECEIPT_NOTextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Tolong isi No Faktur !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please fill Receipt No !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        ElseIf PAYMENT_AMOUNTTextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Tolong isi Jumlah Pembayaran !" & vbCrLf & _
                      "atau tekan Batal jika anda ingin membatalkan pembayaran ini !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please fill Paymount Amount !" & vbCrLf & _
                      "Or click Undo if you want to cancel this payment !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        ElseIf PAYMENT_METHODComboBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Tolong isi Cara Pembayaran !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please fill Payment Method !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        ElseIf CInt(PAYMENT_AMOUNTTextBox.Text) > CInt(BALANCETextBox.Text) Then
            If Language = "Indonesian" Then
                MsgBox("Pembayaran lebih besar dari Saldo Hutang !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Payment is bigger than the Balance !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpBalance = CInt(BALANCETextBox.Text) - CInt(PAYMENT_AMOUNTTextBox.Text)

        PAYABLE_PAYMENTTableAdapter.SP_PAYABLE_PAYMENT("I", _
                                                       0, _
                                                       txtPaymentNo.Text, _
                                                       SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("PAYABLE_ID"), _
                                                       TMPDATE, _
                                                       CInt(PAYMENT_AMOUNTTextBox.Text), _
                                                       PAYMENT_METHODBindingSource.Current("PAYMENT_METHOD_ID"), _
                                                       DESCRIPTIONTextBox.Text, _
                                                       tmpBalance, _
                                                       mdlGeneral.USER_ID, _
                                                       Now, _
                                                       0, _
                                                       DateSerial(4000, 12, 31))

        tmpPositionVendor = VENDOR_NAMETextBox.Text
        tmpReceipt = RECEIPT_NOTextBox.Text
        tmpGrandTotal = TOTAL_PAYABLETextBox.Text
        tmpMaturity = DateSerial(dtpMATURITY_DATE.Value.Year, dtpMATURITY_DATE.Value.Month, dtpMATURITY_DATE.Value.Day)

        If Language = "Indonesian" Then
            MsgBox("Data berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data Succesfully Saved.", MsgBoxStyle.Information, "DMI Retail")
        End If


        Me.PAYMENT_METHODTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.PAYMENT_METHOD)
        Me.PAYABLE_PAYMENTTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.PAYABLE_PAYMENT)
        Me.SP_PAYABLE_PAYMENT_DETAILTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_PAYABLE_PAYMENT_DETAIL, _
                                                      SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("RECEIPT_ID"))

        BALANCETextBox.Text = SP_PAYABLE_PAYMENT_DETAILTableAdapter.SP_PAYABLE_PAYMENT_GET_BALANCE _
            (SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("PAYABLE_ID"))

        VENDOR_NAMETextBox.Text = tmpPositionVendor
        RECEIPT_NOTextBox.Text = tmpReceipt
        dtpMATURITY_DATE.Value = tmpMaturity
        TOTAL_PAYABLETextBox.Text = tmpGrandTotal

        'If RECEIPT_NOTextBox.Text <> "" Then

        '    tmpTotal = 0
        '    tmpBalance = 0
        '    If PAYMENT_AMOUNTTextBox.Text <> "" Then
        '        For x As Integer = 1 To SP_PAYABLE_PAYMENT_DETAILBindingSource.Count
        '            SP_PAYABLE_PAYMENT_DETAILBindingSource.Position = x
        '            tmpTotal = tmpTotal + SP_PAYABLE_PAYMENT_DETAILBindingSource.Current("PAYMENT_AMOUNT")
        '        Next

        '        tmpBalance = CInt(SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("TOTAL_PAYABLE")) - tmpTotal
        '        BALANCETextBox.Text = tmpBalance
        '    Else
        '        BALANCETextBox.Text = SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("TOTAL_PAYABLE")
        '    End If

        '    Me.SP_PAYABLE_PAYMENT_DETAILTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_PAYABLE_PAYMENT_DETAIL, _
        '                                                         SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("RECEIPT_ID"))
        'End If

        DisableInputBox(Me)

        SP_PAYABLE_PAYMENT_DETAILBindingNavigator.Enabled = True

        cmdAdd.Enabled = True
        cmdEdit.Enabled = False
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True

        tmpChange = False
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim tmpCheck As String = ""

        If Trim(RsAccessPrivilege.Item("ALLOW_DELETE").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        PAYABLE_PAYMENTTableAdapter.SP_CHECK_CLOSING_PERIOD(PAYMENT_DATEDateTimePicker.Value, tmpCheck)

        If tmpCheck = "TRUE" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menghapus transaksi in!" & vbCrLf & "Transaksi ini telah masuk dalam proses Tutup Buku", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Delete this Transaction!" & vbCrLf & "This Transaction has been in Closed Period", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If


        If SP_PAYABLE_PAYMENT_DETAILBindingSource.Count < 1 Then Exit Sub

        If Language = "Indonesian" Then
            If MsgBox("Apakah anda yakin ingin menghapus pembayaran ini ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, _
                      "DMI Retail") = MsgBoxResult.No Then Exit Sub
        Else
            If MsgBox("Are you sure want to delete this payment ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, _
                      "DMI Retail") = MsgBoxResult.No Then Exit Sub
        End If

        PAYABLE_PAYMENTTableAdapter.SP_PAYABLE_PAYMENT("D", _
                                                       SP_PAYABLE_PAYMENT_DETAILBindingSource.Current("PAYABLE_PAYMENT_ID"), _
                                                       SP_PAYABLE_PAYMENT_DETAILBindingSource.Current("PAYABLE_PAYMENT_NUMBER"), _
                                                       SP_PAYABLE_PAYMENT_DETAILBindingSource.Current("PAYABLE_ID"), _
                                                       SP_PAYABLE_PAYMENT_DETAILBindingSource.Current("PAYMENT_DATE"), _
                                                       SP_PAYABLE_PAYMENT_DETAILBindingSource.Current("PAYMENT_AMOUNT"), _
                                                       SP_PAYABLE_PAYMENT_DETAILBindingSource.Current("PAYMENT_METHOD"), _
                                                       SP_PAYABLE_PAYMENT_DETAILBindingSource.Current("DESCRIPTION"), _
                                                       PAYABLE_PAYMENTBindingSource.Current("BALANCE"), _
                                                       PAYABLE_PAYMENTBindingSource.Current("USER_ID_INPUT"), _
                                                      PAYABLE_PAYMENTBindingSource.Current("INPUT_DATE"), _
                                                      PAYABLE_PAYMENTBindingSource.Current("USER_ID_UPDATE"), _
                                                       PAYABLE_PAYMENTBindingSource.Current("UPDATE_DATE"))

        If Language = "Indonesian" Then
            MsgBox("Data berhasil di hapus.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data Succesfully Deleted.", MsgBoxStyle.Information, "DMI Retail")
        End If

        SP_PAYABLE_PAYMENT_DETAILTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_PAYABLE_PAYMENT_DETAIL, _
                                                   SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("RECEIPT_ID"))

        tmpChange = False

    End Sub

    Private Sub PAYMENT_AMOUNTTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PAYMENT_AMOUNTTextBox.KeyPress
       If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""

        tmpChange = True
    End Sub

    Private Sub PAYMENT_AMOUNTTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PAYMENT_AMOUNTTextBox.TextChanged
        '   If PAYMENT_AMOUNTTextBox.Text<>""
        '   BALANCETextBox.Text = tmpBalance - CInt(PAYMENT_AMOUNTTextBox.Text)

        'If TOTAL_PAYABLETextBox.Text <> "" Then
        '    tmpTotal = 0
        '    tmpBalance = 0
        ''    For x As Integer = 1 To SP_PAYABLE_PAYMENT_DETAILBindingSource.Count
        ''        SP_PAYABLE_PAYMENT_DETAILBindingSource.Position = x
        ''        tmpTotal = tmpTotal + SP_PAYABLE_PAYMENT_DETAILBindingSource.Current("PAYMENT_AMOUNT")
        ''    Next

        'If PAYMENT_AMOUNTTextBox.Text <> "" Then
        '    tmpBalance = CInt(SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("TOTAL_PAYABLE")) - CInt(PAYMENT_AMOUNTTextBox.Text)
        '    BALANCETextBox.Text = tmpBalance
        'Else

        '    Try
        '        SP_PAYABLE_PAYMENT_DETAILBindingSource.Position = SP_PAYABLE_PAYMENT_DETAILBindingSource.Count
        '        BALANCETextBox.Text = SP_PAYABLE_PAYMENT_DETAILBindingSource.Current("BALANCE")
        '    Catch
        '        BALANCETextBox.Text = SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("TOTAL_PAYABLE")
        '    End Try

        'End If
        'End If
        If PAYMENT_AMOUNTTextBox.Text = "" Then
            PAYMENT_AMOUNTTextBox.Text = "0"
        End If

    End Sub

    Private Sub RECEIPT_NOTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RECEIPT_NOTextBox.TextChanged
        'Me.SP_PAYABLE_PAYMENT_DETAILTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_PAYABLE_PAYMENT_DETAIL, _
        '                                                         SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("RECEIPT_ID"))

        'If RECEIPT_NOTextBox.Text <> "" Then

        '    tmpTotal = 0
        '    tmpBalance = 0
        '    If PAYMENT_AMOUNTTextBox.Text <> "" Then
        '        For x As Integer = 1 To SP_PAYABLE_PAYMENT_DETAILBindingSource.Count
        '            SP_PAYABLE_PAYMENT_DETAILBindingSource.Position = x
        '            tmpTotal = tmpTotal + SP_PAYABLE_PAYMENT_DETAILBindingSource.Current("PAYMENT_AMOUNT")
        '        Next

        '        tmpBalance = CInt(SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("TOTAL_PAYABLE")) - tmpTotal
        '        BALANCETextBox.Text = tmpBalance
        '    Else
        '        BALANCETextBox.Text = SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("TOTAL_PAYABLE")
        '    End If
        'End If

        'Try
        '    BALANCETextBox.Text = SP_PAYABLE_PAYMENT_DETAILBindingSource.Current("BALANCE")
        'Catch
        'End Try

        Me.SP_PAYABLE_PAYMENT_DETAILTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_PAYABLE_PAYMENT_DETAIL, _
                                                               SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("RECEIPT_ID"))

        If RECEIPT_NOTextBox.Text <> "" Then

            tmpTotal = 0
            tmpBalance = 0
            If PAYMENT_AMOUNTTextBox.Text <> "" Then
                BALANCETextBox.Text = FormatNumber(SP_PAYABLE_PAYMENT_DETAILTableAdapter.SP_PAYABLE_PAYMENT_GET_BALANCE _
                (SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("PAYABLE_ID")), 0)
            Else
                BALANCETextBox.Text = FormatNumber(SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("TOTAL_PAYABLE"), 0)
            End If
        End If

    End Sub

    Private Sub txtPaymentNo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPaymentNo.KeyDown
        If e.KeyCode = Keys.Delete Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtPaymentNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPaymentNo.KeyPress
        e.KeyChar = ""
    End Sub

    Private Sub SP_PAYABLE_PAYMENT_HEADERBindingSource_CurrentChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SP_PAYABLE_PAYMENT_HEADERBindingSource.CurrentChanged
        Try
            Me.SP_PAYABLE_PAYMENT_DETAILTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_PAYABLE_PAYMENT_DETAIL, _
                                                               SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("RECEIPT_ID"))
        Catch

        End Try

        If RECEIPT_NOTextBox.Text <> "" Then

            tmpTotal = 0
            tmpBalance = 0
            If PAYMENT_AMOUNTTextBox.Text <> "" Then
                BALANCETextBox.Text = SP_PAYABLE_PAYMENT_DETAILTableAdapter.SP_PAYABLE_PAYMENT_GET_BALANCE _
                (SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("PAYABLE_ID"))
            Else
                BALANCETextBox.Text = SP_PAYABLE_PAYMENT_HEADERBindingSource.Current("TOTAL_PAYABLE")
            End If
        End If
    End Sub

End Class