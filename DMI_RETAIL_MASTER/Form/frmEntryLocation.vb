﻿Imports System.Data.SqlClient

Public Class frmEntryLocation
    Dim tmpSaveMode As String
    Dim tmpKey As String
    Dim tmpNoOfLocationName, tmpResult As Integer
    Dim tmpLocationName, tmpEditCode As String
    Dim tmpChange As Boolean
    Dim tmpEndDate As DateTime
    Public LOCATIONBindingSource As New BindingSource

    Private Sub frmEntryLocation_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If tmpChange = True And LOCATION_NAMETextBox.Enabled = True Then
            tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            If tmpvar = 6 Then
                cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub GetData()
        sql = "SELECT LOCATION_ID, LOCATION_NAME, DESCRIPTION, EFFECTIVE_START_DATE, " & _
              "EFFECTIVE_END_DATE, USER_ID_INPUT, INPUT_DATE, USER_ID_UPDATE, UPDATE_DATE, " & _
              "LOCATION_CODE FROM LOCATION"
        dt = New DataTable
        sqladapter = New SqlDataAdapter(sql, xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        LOCATIONBindingSource.DataSource = dt
        LOCATIONBindingNavigator.BindingSource = LOCATIONBindingSource

    End Sub

    Private Sub frmEntryLocation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_LOCATION.LOCATION' table. You can move, or remove it, as needed.
        'Me.LOCATIONTableAdapter.Fill(Me.DS_LOCATION.LOCATION)
        accFormName = Me.Text
        GetData()
        LOCATION_CODETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", LOCATIONBindingSource, "LOCATION_CODE", True))
        LOCATION_NAMETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", LOCATIONBindingSource, "LOCATION_NAME", True))
        DESCRIPTIONTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", LOCATIONBindingSource, "DESCRIPTION", True))

        If LOCATIONBindingSource.Count > 0 Then
            If LOCATIONBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
                cmdDelete.Enabled = True
                cmdEdit.Enabled = True
            End If
        End If

        DisableInputBox(Me)

        cmdSearch.Visible = True
        LOCATIONBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        chkActive.Enabled = False
        tmpChange = False

        If Language = "Indonesian" Then
            Me.Text = "Input Lokasi"
            lblLocationNm.Text = "Nama Lokasi"
            lblDescription.Text = "Keterangan"
            cmdAdd.Text = "Tambah"
            cmdDelete.Text = "Hapus"
            cmdSave.Text = "Simpan"
            cmdEdit.Text = "Ubah"
            cmdUndo.Text = "Batal"
        End If

    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If Trim(RsAccessPrivilege.Item("ALLOW_ADD").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Insert"

        chkActive.Enabled = True
        chkActive.Checked = True
        EnableInputBox(Me)
        LOCATIONBindingSource.AddNew()
        xComm = New SqlCommand("SP_GENERATE_MASTER_CODE", xConn)
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@DOCUMENT_TYPE", "LOCATION")
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdreader = xComm.ExecuteReader
        xdreader.Read()
        If xdreader.HasRows Then
            LOCATION_CODETextBox.Text = xdreader.Item(0)
        End If
        'LOCATIONTableAdapter.SP_GENERATE_MASTER_CODE("LOCATION", LOCATION_CODETextBox.Text)

        cmdSearch.Visible = False
        LOCATIONBindingNavigator.Enabled = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        tmpChange = False
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        If LOCATION_NAMETextBox.Text = "" Then Exit Sub
        If Trim(RsAccessPrivilege.Item("ALLOW_EDIT").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Update"

        EnableInputBox(Me)

        cmdSearch.Visible = False
        LOCATIONBindingNavigator.Enabled = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        chkActive.Enabled = True
        tmpEditCode = LOCATION_CODETextBox.Text
        tmpLocationName = LOCATION_NAMETextBox.Text
        tmpChange = False

        If LOCATIONBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
            chkActive.Checked = False
        Else
            chkActive.Checked = True
        End If

    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        tmpSaveMode = ""

        DisableInputBox(Me)
        LOCATIONBindingSource.CancelEdit()

        cmdSearch.Visible = True
        LOCATIONBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        chkActive.Enabled = False
        tmpChange = False
        LOCATIONBindingSource_CurrentChanged(Nothing, Nothing)

    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Me.Validate()

        If LOCATION_NAMETextBox.Text = "" Or LOCATION_CODETextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpKey = LOCATION_NAMETextBox.Text

        If tmpSaveMode = "Insert" Then
            xComm = New SqlCommand("SP_CHECK_MASTER_CODE", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@DOCUMENT_TYPE", "LOCATION")
            xComm.Parameters.AddWithValue("@DOC_NO", LOCATION_CODETextBox.Text)
            xdreader = xComm.ExecuteReader
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xdreader.Read()
            tmpResult = xdreader.Item(0)

            xComm = New SqlCommand("SP_CHECK_LOCATION_NAME", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@LOCATION_NAME", LOCATION_NAMETextBox.Text)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xdreader = xComm.ExecuteReader
            xdreader.Read()
            tmpNoOfLocationName = xdreader.Item(0)
            'LOCATIONTableAdapter.SP_CHECK_MASTER_CODE("LOCATION", LOCATION_CODETextBox.Text, tmpResult)
            'LOCATIONTableAdapter.SP_CHECK_LOCATION_NAME(LOCATION_NAMETextBox.Text, tmpNoOfLocationName)
            If tmpNoOfLocationName > 0 Or tmpResult > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Nama lokasi sudah ada di dalam database." & vbCrLf & _
                       "Silahkan isi nama lokasi yang lain!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Location Name already exist in Database." & vbCrLf & _
                           "Please enter another Location Name!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                While tmpResult > 0
                    xComm = New SqlCommand("SP_GENERATE_MASTER_CODE", xConn)
                    xComm.CommandType = CommandType.StoredProcedure
                    xComm.Parameters.AddWithValue("@DOCUMENT_TYPE", "LOCATION")
                    If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                    xdreader = xComm.ExecuteReader
                    xdreader.Read()
                    If xdreader.HasRows Then
                        LOCATION_CODETextBox.Text = xdreader.Item(0)
                    End If

                    xComm = New SqlCommand("SP_CHECK_MASTER_CODE", xConn)
                    xComm.CommandType = CommandType.StoredProcedure
                    xComm.Parameters.AddWithValue("@DOCUMENT_TYPE", "LOCATION")
                    xComm.Parameters.AddWithValue("@DOC_NO", LOCATION_CODETextBox.Text)
                    If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                    xdreader = xComm.ExecuteReader
                    xdreader.Read()
                    tmpResult = xdreader.Item(0)
                    'LOCATIONTableAdapter.SP_GENERATE_MASTER_CODE("LOCATION", LOCATION_CODETextBox.Text)
                    'LOCATIONTableAdapter.SP_CHECK_MASTER_CODE("LOCATION", LOCATION_CODETextBox.Text, tmpResult)
                End While
                Exit Sub
            End If

            xComm = New SqlCommand("SP_LOCATION", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "I")
            xComm.Parameters.AddWithValue("@LOCATION_ID", 0)
            xComm.Parameters.AddWithValue("@LOCATION_CODE", LOCATION_CODETextBox.Text)
            xComm.Parameters.AddWithValue("@LOCATION_NAME", LOCATION_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@DESCRIPTION", DESCRIPTIONTextBox.Text)
            xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", Now)
            xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", DateSerial(4000, 12, 31))
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", UserId)
            xComm.Parameters.AddWithValue("@INPUT_DATE", Now)
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", 0)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", DateSerial(4000, 12, 31))
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()
            'LOCATIONTableAdapter.SP_LOCATION("I", _
            '                                 0, _
            '                                 LOCATION_CODETextBox.Text,
            '                                 LOCATION_NAMETextBox.Text, _
            '                                 DESCRIPTIONTextBox.Text, _
            '                                 DateSerial(Today.Year, Today.Month, Today.Day), _
            '                                 DateSerial(4000, 12, 31), _
            '                                 mdlGeneral.USER_ID, _
            '                                 Now, _
            '                                 0, _
            '                                 DateSerial(4000, 12, 31))
        ElseIf tmpSaveMode = "Update" Then
            If tmpEditCode <> LOCATION_CODETextBox.Text Then
                xComm = New SqlCommand("SP_CHECK_MASTER_CODE", xConn)
                xComm.CommandType = CommandType.StoredProcedure
                xComm.Parameters.AddWithValue("@DOCUMENT_TYPE", "LOCATION")
                xComm.Parameters.AddWithValue("@DOC_NO", LOCATION_CODETextBox.Text)
                If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                xdreader = xComm.ExecuteReader
                xdreader.Read()
                tmpResult = xdreader.Item(0)
                ' LOCATIONTableAdapter.SP_CHECK_MASTER_CODE("LOCATION", LOCATION_CODETextBox.Text, tmpResult)
                If tmpResult > 0 Then
                    If Language = "Indonesian" Then
                        MsgBox("No Transaksi telah digunakan oleh user lain." & vbCrLf & _
                                                   "Nomor transaksi baru akan digunakan untuk transaksi ini.", _
                                                   MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("Receipt Number is used by another User." & vbCrLf & _
                                                            "New Receipt Number will be assigned to this transaction.", _
                                                            MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    LOCATION_CODETextBox.Focus()
                    Exit Sub
                End If
            End If

            If chkActive.Checked = True Then
                tmpEndDate = DateSerial(4000, 12, 31)
            Else
                tmpEndDate = Now
            End If

            If tmpLocationName = LOCATION_NAMETextBox.Text Then
                tmpNoOfLocationName = 0
            Else
                xComm = New SqlCommand("SP_CHECK_LOCATION_NAME", xConn)
                xComm.CommandType = CommandType.StoredProcedure
                xComm.Parameters.AddWithValue("@LOCATION_NAMA", LOCATION_NAMETextBox.Text)
                If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                xdreader = xComm.ExecuteReader
                xdreader.Read()
                tmpNoOfLocationName = xdreader.Item(0)
                'LOCATIONTableAdapter.SP_CHECK_LOCATION_NAME(LOCATION_NAMETextBox.Text, tmpNoOfLocationName)
            End If
            If tmpNoOfLocationName > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Nama lokasi sudah ada di dalam database." & vbCrLf & _
                       "Silahkan isi nama lokasi yang lain!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Location Name already exist in Database." & vbCrLf & _
                           "Please enter another Location Name!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If
            xComm = New SqlCommand("SP_LOCATION", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "U")
            xComm.Parameters.AddWithValue("@LOCATION_ID", LOCATIONBindingSource.Current("LOCATION_ID"))
            xComm.Parameters.AddWithValue("@LOCATION_CODE", LOCATION_CODETextBox.Text)
            xComm.Parameters.AddWithValue("@LOCATION_NAME", LOCATION_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@DESCRIPTION", DESCRIPTIONTextBox.Text)
            xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", LOCATIONBindingSource.Current("EFFECTIVE_START_DATE"))
            xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", tmpEndDate)
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", LOCATIONBindingSource.Current("USER_ID_INPUT"))
            xComm.Parameters.AddWithValue("@INPUT_DATE", LOCATIONBindingSource.Current("INPUT_DATE"))
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", UserId)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", Now)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()
            'LOCATIONTableAdapter.SP_LOCATION("U", _
            '                                LOCATIONBindingSource.Current("LOCATION_ID"), _
            '                                LOCATION_CODETextBox.Text,
            '                                LOCATION_NAMETextBox.Text, _
            '                                DESCRIPTIONTextBox.Text, _
            '                                LOCATIONBindingSource.Current("EFFECTIVE_START_DATE"), _
            '                                tmpEndDate, _
            '                                LOCATIONBindingSource.Current("USER_ID_INPUT"), _
            '                                LOCATIONBindingSource.Current("INPUT_DATE"), _
            '                                mdlGeneral.USER_ID, _
            '                                Now)
        End If

        tmpSaveMode = ""

        DisableInputBox(Me)
        LOCATIONBindingSource.CancelEdit()

        cmdSearch.Visible = True
        LOCATIONBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        chkActive.Enabled = False
        getdata()
        'Me.LOCATIONTableAdapter.Fill(Me.DS_LOCATION.LOCATION)
        Me.LOCATIONBindingSource.Position = LOCATIONBindingSource.Find("LOCATION_NAME", tmpKey)

        If Language = "Indonesian" Then
            MsgBox("Data berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
        End If
        tmpChange = False
        LOCATIONBindingSource_CurrentChanged(Nothing, Nothing)

    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If LOCATION_NAMETextBox.Text = "" Then Exit Sub
        If Trim(RsAccessPrivilege.Item("ALLOW_DELETE").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If Language = "Indonesian" Then
            If MsgBox("Hapus Lokasi?" & vbCrLf & LOCATION_NAMETextBox.Text, _
                      MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
        Else
            If MsgBox("Delete LOCATION?" & vbCrLf & LOCATION_NAMETextBox.Text, _
                  MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
        End If


        Dim tmpUsedLocation As Integer
        xComm = New SqlCommand("SP_CHECK_USED_LOCATION", xConn)
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@LOCATION_NAME", LOCATION_NAMETextBox.Text)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdreader = xComm.ExecuteReader
        xdreader.Read()
        tmpUsedLocation = xdreader.Item(0)
        'LOCATIONTableAdapter.SP_CHECK_USED_LOCATION(LOCATION_NAMETextBox.Text, tmpUsedLocation)

        If tmpUsedLocation > 0 Then
            If Language = "Indonesian" Then
                MsgBox("Lokasi ini telah terdapat produk." & vbCrLf & _
                      "Anda tidak dapat menghapus data ini !", _
                      MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("This Location is used by Product(s)." & vbCrLf & _
                "You can not delete this data !", _
                MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If
        xComm = New SqlCommand("SP_LOCATION", xConn)
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@METHOD", "D")
        xComm.Parameters.AddWithValue("@LOCATION_ID", LOCATIONBindingSource.Current("LOCATION_ID"))
        xComm.Parameters.AddWithValue("@LOCATION_CODE", LOCATIONBindingSource.Current("LOCATION_CODE"))
        xComm.Parameters.AddWithValue("@LOCATION_NAME", LOCATIONBindingSource.Current("LOCATION_NAME"))
        xComm.Parameters.AddWithValue("@DESCRIPTION", LOCATIONBindingSource.Current("DESCRIPTION"))
        xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", LOCATIONBindingSource.Current("EFFECTIVE_START_DATE"))
        xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", Now)
        xComm.Parameters.AddWithValue("@USER_ID_INPUT", LOCATIONBindingSource.Current("USER_ID_INPUT"))
        xComm.Parameters.AddWithValue("@INPUT_DATE", LOCATIONBindingSource.Current("INPUT_DATE"))
        xComm.Parameters.AddWithValue("@USER_ID_UPDATE", LOCATIONBindingSource.Current("USER_ID_UPDATE"))
        xComm.Parameters.AddWithValue("@UPDATE_DATE", LOCATIONBindingSource.Current("UPDATE_DATE"))
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xComm.ExecuteNonQuery()
        'LOCATIONTableAdapter.SP_LOCATION("D", _
        '                                 LOCATIONBindingSource.Current("LOCATION_ID"), _
        '                                 LOCATIONBindingSource.Current("LOCATION_CODE"),
        '                                 LOCATIONBindingSource.Current("LOCATION_NAME"), _
        '                                 LOCATIONBindingSource.Current("DESCRIPTION"), _
        '                                 LOCATIONBindingSource.Current("EFFECTIVE_START_DATE"), _
        '                                 Now, _
        '                                 LOCATIONBindingSource.Current("USER_ID_INPUT"), _
        '                                 LOCATIONBindingSource.Current("INPUT_DATE"), _
        '                                 LOCATIONBindingSource.Current("USER_ID_UPDATE"), _
        '                                 LOCATIONBindingSource.Current("UPDATE_DATE"))

        LOCATIONBindingSource.RemoveCurrent()
        GetData()
        'Me.LOCATIONTableAdapter.Fill(Me.DS_LOCATION.LOCATION)
        Me.LOCATIONBindingSource.Position = LOCATIONBindingSource.Find("LOCATION_NAME", tmpKey)

        Try
            If LOCATIONBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
                cmdDelete.Enabled = True
                cmdEdit.Enabled = True
            End If
        Catch
        End Try

        If Language = "Indonesian" Then
            MsgBox("Data berhasil dihapus !", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully deleted !", MsgBoxStyle.Information, "DMI Retail")
        End If

        tmpChange = False
        chkActive.Enabled = False

    End Sub

    Private Sub LOCATION_NAMETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LOCATION_NAMETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub DESCRIPTIONTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DESCRIPTIONTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub LOCATIONBindingSource_CurrentChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If LOCATION_NAMETextBox.Text <> "" And LOCATION_NAMETextBox.Enabled = False Then
            If LOCATIONBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
                cmdDelete.Enabled = True
                cmdEdit.Enabled = True
            End If
        End If
    End Sub

    Private Sub cmdSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        frmSearchLocation.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        LOCATIONBindingSource.Position = LOCATIONBindingSource.Find("LOCATION_ID", tmpSearchResult)
    End Sub

    Private Sub BindingNavigatorPositionItem_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BindingNavigatorPositionItem.TextChanged
        Try
            If LOCATIONBindingSource.Count > 0 Then
                If LOCATIONBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                    chkActive.Checked = False
                Else
                    chkActive.Checked = True
                    cmdDelete.Enabled = True
                    cmdEdit.Enabled = True
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        AccessPrivilege(Me.Tag)
    End Sub
End Class