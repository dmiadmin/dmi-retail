﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEntrySale
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEntrySale))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.SP_SALE_INVOICEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_SALE = New DMI_RETAIL_SALE.DS_SALE()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dtpSaleOrderDate = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmdSearchSaleOrder = New System.Windows.Forms.PictureBox()
        Me.txtSaleOrderNo = New System.Windows.Forms.TextBox()
        Me.lblSaleOrderNo = New System.Windows.Forms.Label()
        Me.lblReceipNo = New System.Windows.Forms.Label()
        Me.lblCustomer = New System.Windows.Forms.Label()
        Me.lblWarehouse = New System.Windows.Forms.Label()
        Me.lblMethod = New System.Windows.Forms.Label()
        Me.lblMaturity = New System.Windows.Forms.Label()
        Me.MATURITY_DATEDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.SALEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.WAREHOUSE_IDComboBox = New System.Windows.Forms.ComboBox()
        Me.WAREHOUSEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DS_SALE1 = New DMI_RETAIL_SALE.DS_SALE()
        Me.txtDTP = New System.Windows.Forms.TextBox()
        Me.PAYMENT_METHOD_IDComboBox = New System.Windows.Forms.ComboBox()
        Me.PAYMENT_METHODBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblSaleDate = New System.Windows.Forms.Label()
        Me.cmdSearchName = New System.Windows.Forms.PictureBox()
        Me.RECEIPT_NOTextBox = New System.Windows.Forms.TextBox()
        Me.SALE_DATEDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.CUSTOMER_IDComboBox = New System.Windows.Forms.ComboBox()
        Me.CUSTOMERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SALEWAREHOUSEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DOWN_PAYMENTTextBox = New System.Windows.Forms.TextBox()
        Me.AMOUNT_DISCOUNTTextBox = New System.Windows.Forms.TextBox()
        Me.GRAND_TOTALTextBox = New System.Windows.Forms.TextBox()
        Me.REMARKTextBox = New System.Windows.Forms.TextBox()
        Me.SALETableAdapter = New DMI_RETAIL_SALE.DS_SALETableAdapters.SALETableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_SALE.DS_SALETableAdapters.TableAdapterManager()
        Me.CUSTOMERTableAdapter = New DMI_RETAIL_SALE.DS_SALETableAdapters.CUSTOMERTableAdapter()
        Me.PAYMENT_METHODTableAdapter = New DMI_RETAIL_SALE.DS_SALETableAdapters.PAYMENT_METHODTableAdapter()
        Me.PRODUCTTableAdapter = New DMI_RETAIL_SALE.DS_SALETableAdapters.PRODUCTTableAdapter()
        Me.SALE_DETAILTableAdapter = New DMI_RETAIL_SALE.DS_SALETableAdapters.SALE_DETAILTableAdapter()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblTax = New System.Windows.Forms.Label()
        Me.TAX_AMOUNTTextBox = New System.Windows.Forms.TextBox()
        Me.TAXPercentageTEXTBOX = New System.Windows.Forms.TextBox()
        Me.lblSalesman = New System.Windows.Forms.Label()
        Me.cmbSALESMAN_NAME = New System.Windows.Forms.ComboBox()
        Me.SALESMANBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.txtDiscount3 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtDisc3 = New System.Windows.Forms.TextBox()
        Me.txtDiscount2 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtDisc2 = New System.Windows.Forms.TextBox()
        Me.lblPayment = New System.Windows.Forms.Label()
        Me.lblGrandTotal = New System.Windows.Forms.Label()
        Me.lblDiscount = New System.Windows.Forms.Label()
        Me.lblChange = New System.Windows.Forms.Label()
        Me.lblChg = New System.Windows.Forms.Label()
        Me.lblremark = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.lblDel = New System.Windows.Forms.Label()
        Me.lblF6 = New System.Windows.Forms.Label()
        Me.lblF5 = New System.Windows.Forms.Label()
        Me.lblNoOfItem = New System.Windows.Forms.Label()
        Me.dgvSaleDetail = New System.Windows.Forms.DataGridView()
        Me.PRODUCT_CODE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRODUCT_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QUANTITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRICE_PER_UNIT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DISCOUNT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DISCOUNT2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DELETE = New System.Windows.Forms.DataGridViewImageColumn()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.PRODUCTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SALE_DETAILBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.cmdPrint = New System.Windows.Forms.Button()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.SALEBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.rpvSaleInvoice = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.SP_SALE_INVOICETableAdapter = New DMI_RETAIL_SALE.DS_SALETableAdapters.SP_SALE_INVOICETableAdapter()
        Me.VIEW_LIST_SALEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VIEW_LIST_SALETableAdapter = New DMI_RETAIL_SALE.DS_SALETableAdapters.VIEW_LIST_SALETableAdapter()
        Me.VIEW_LIST_SALE_DETAILBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VIEW_LIST_SALE_DETAILTableAdapter = New DMI_RETAIL_SALE.DS_SALETableAdapters.VIEW_LIST_SALE_DETAILTableAdapter()
        Me.DS_PRODUCT_WAREHOUSE = New DMI_RETAIL_SALE.DS_PRODUCT_WAREHOUSE()
        Me.PRODUCT_WAREHOUSEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PRODUCT_WAREHOUSETableAdapter = New DMI_RETAIL_SALE.DS_PRODUCT_WAREHOUSETableAdapters.PRODUCT_WAREHOUSETableAdapter()
        Me.TableAdapterManager1 = New DMI_RETAIL_SALE.DS_PRODUCT_WAREHOUSETableAdapters.TableAdapterManager()
        Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter = New DMI_RETAIL_SALE.DS_PRODUCT_WAREHOUSETableAdapters.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter()
        Me.WAREHOUSETableAdapter = New DMI_RETAIL_SALE.DS_SALETableAdapters.WAREHOUSETableAdapter()
        Me.SP_SPLIT_PRODUCTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_SPLIT_PRODUCTTableAdapter = New DMI_RETAIL_SALE.DS_SALETableAdapters.SP_SPLIT_PRODUCTTableAdapter()
        Me.SALESMANTableAdapter = New DMI_RETAIL_SALE.DS_SALETableAdapters.SALESMANTableAdapter()
        Me.SP_SELECT_SALE_ORDER_DETAILBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_SELECT_SALE_ORDER_DETAILTableAdapter = New DMI_RETAIL_SALE.DS_SALETableAdapters.SP_SELECT_SALE_ORDER_DETAILTableAdapter()
        CType(Me.SP_SALE_INVOICEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_SALE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cmdSearchSaleOrder, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SALEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_SALE1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PAYMENT_METHODBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CUSTOMERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SALEWAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.SALESMANBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvSaleDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SALE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SALEBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SALEBindingNavigator.SuspendLayout()
        CType(Me.VIEW_LIST_SALEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VIEW_LIST_SALE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DS_PRODUCT_WAREHOUSE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PRODUCT_WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_SPLIT_PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_SELECT_SALE_ORDER_DETAILBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SP_SALE_INVOICEBindingSource
        '
        Me.SP_SALE_INVOICEBindingSource.DataMember = "SP_SALE_INVOICE"
        Me.SP_SALE_INVOICEBindingSource.DataSource = Me.DS_SALE
        '
        'DS_SALE
        '
        Me.DS_SALE.DataSetName = "DS_SALE"
        Me.DS_SALE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dtpSaleOrderDate)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.cmdSearchSaleOrder)
        Me.GroupBox1.Controls.Add(Me.txtSaleOrderNo)
        Me.GroupBox1.Controls.Add(Me.lblSaleOrderNo)
        Me.GroupBox1.Controls.Add(Me.lblReceipNo)
        Me.GroupBox1.Controls.Add(Me.lblCustomer)
        Me.GroupBox1.Controls.Add(Me.lblWarehouse)
        Me.GroupBox1.Controls.Add(Me.lblMethod)
        Me.GroupBox1.Controls.Add(Me.lblMaturity)
        Me.GroupBox1.Controls.Add(Me.MATURITY_DATEDateTimePicker)
        Me.GroupBox1.Controls.Add(Me.WAREHOUSE_IDComboBox)
        Me.GroupBox1.Controls.Add(Me.txtDTP)
        Me.GroupBox1.Controls.Add(Me.PAYMENT_METHOD_IDComboBox)
        Me.GroupBox1.Controls.Add(Me.lblSaleDate)
        Me.GroupBox1.Controls.Add(Me.cmdSearchName)
        Me.GroupBox1.Controls.Add(Me.RECEIPT_NOTextBox)
        Me.GroupBox1.Controls.Add(Me.SALE_DATEDateTimePicker)
        Me.GroupBox1.Controls.Add(Me.CUSTOMER_IDComboBox)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 28)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(750, 138)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        '
        'dtpSaleOrderDate
        '
        Me.dtpSaleOrderDate.CustomFormat = "dd-MMM-yyy"
        Me.dtpSaleOrderDate.Enabled = False
        Me.dtpSaleOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpSaleOrderDate.Location = New System.Drawing.Point(553, 95)
        Me.dtpSaleOrderDate.Name = "dtpSaleOrderDate"
        Me.dtpSaleOrderDate.Size = New System.Drawing.Size(149, 20)
        Me.dtpSaleOrderDate.TabIndex = 38
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(439, 100)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(108, 14)
        Me.Label3.TabIndex = 37
        Me.Label3.Text = "Sale Order Date"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cmdSearchSaleOrder
        '
        Me.cmdSearchSaleOrder.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchSaleOrder.Image = CType(resources.GetObject("cmdSearchSaleOrder.Image"), System.Drawing.Image)
        Me.cmdSearchSaleOrder.Location = New System.Drawing.Point(673, 66)
        Me.cmdSearchSaleOrder.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchSaleOrder.Name = "cmdSearchSaleOrder"
        Me.cmdSearchSaleOrder.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearchSaleOrder.TabIndex = 36
        Me.cmdSearchSaleOrder.TabStop = False
        '
        'txtSaleOrderNo
        '
        Me.txtSaleOrderNo.Location = New System.Drawing.Point(551, 66)
        Me.txtSaleOrderNo.Name = "txtSaleOrderNo"
        Me.txtSaleOrderNo.ReadOnly = True
        Me.txtSaleOrderNo.Size = New System.Drawing.Size(116, 20)
        Me.txtSaleOrderNo.TabIndex = 35
        '
        'lblSaleOrderNo
        '
        Me.lblSaleOrderNo.Location = New System.Drawing.Point(457, 71)
        Me.lblSaleOrderNo.Name = "lblSaleOrderNo"
        Me.lblSaleOrderNo.Size = New System.Drawing.Size(88, 14)
        Me.lblSaleOrderNo.TabIndex = 34
        Me.lblSaleOrderNo.Text = "Sale Order No"
        Me.lblSaleOrderNo.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblReceipNo
        '
        Me.lblReceipNo.Location = New System.Drawing.Point(31, 17)
        Me.lblReceipNo.Name = "lblReceipNo"
        Me.lblReceipNo.Size = New System.Drawing.Size(67, 14)
        Me.lblReceipNo.TabIndex = 30
        Me.lblReceipNo.Text = "Receipt No"
        Me.lblReceipNo.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblCustomer
        '
        Me.lblCustomer.Location = New System.Drawing.Point(10, 72)
        Me.lblCustomer.Name = "lblCustomer"
        Me.lblCustomer.Size = New System.Drawing.Size(88, 14)
        Me.lblCustomer.TabIndex = 32
        Me.lblCustomer.Text = "Customer"
        Me.lblCustomer.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblWarehouse
        '
        Me.lblWarehouse.Location = New System.Drawing.Point(18, 99)
        Me.lblWarehouse.Name = "lblWarehouse"
        Me.lblWarehouse.Size = New System.Drawing.Size(80, 14)
        Me.lblWarehouse.TabIndex = 33
        Me.lblWarehouse.Text = "Warehouse"
        Me.lblWarehouse.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblMethod
        '
        Me.lblMethod.Location = New System.Drawing.Point(430, 19)
        Me.lblMethod.Name = "lblMethod"
        Me.lblMethod.Size = New System.Drawing.Size(115, 14)
        Me.lblMethod.TabIndex = 31
        Me.lblMethod.Text = "Payment Method"
        Me.lblMethod.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblMaturity
        '
        Me.lblMaturity.Location = New System.Drawing.Point(433, 45)
        Me.lblMaturity.Name = "lblMaturity"
        Me.lblMaturity.Size = New System.Drawing.Size(112, 14)
        Me.lblMaturity.TabIndex = 28
        Me.lblMaturity.Text = "Maturity Date"
        Me.lblMaturity.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'MATURITY_DATEDateTimePicker
        '
        Me.MATURITY_DATEDateTimePicker.CustomFormat = "dd-MMM-yyy"
        Me.MATURITY_DATEDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.SALEBindingSource, "MATURITY_DATE", True))
        Me.MATURITY_DATEDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.MATURITY_DATEDateTimePicker.Location = New System.Drawing.Point(551, 40)
        Me.MATURITY_DATEDateTimePicker.Name = "MATURITY_DATEDateTimePicker"
        Me.MATURITY_DATEDateTimePicker.Size = New System.Drawing.Size(149, 20)
        Me.MATURITY_DATEDateTimePicker.TabIndex = 6
        '
        'SALEBindingSource
        '
        Me.SALEBindingSource.DataMember = "SALE"
        Me.SALEBindingSource.DataSource = Me.DS_SALE
        '
        'WAREHOUSE_IDComboBox
        '
        Me.WAREHOUSE_IDComboBox.DataSource = Me.WAREHOUSEBindingSource
        Me.WAREHOUSE_IDComboBox.DisplayMember = "WAREHOUSE_NAME"
        Me.WAREHOUSE_IDComboBox.FormattingEnabled = True
        Me.WAREHOUSE_IDComboBox.Location = New System.Drawing.Point(104, 96)
        Me.WAREHOUSE_IDComboBox.Name = "WAREHOUSE_IDComboBox"
        Me.WAREHOUSE_IDComboBox.Size = New System.Drawing.Size(117, 22)
        Me.WAREHOUSE_IDComboBox.TabIndex = 5
        Me.WAREHOUSE_IDComboBox.ValueMember = "WAREHOUSE_ID"
        '
        'WAREHOUSEBindingSource
        '
        Me.WAREHOUSEBindingSource.DataMember = "WAREHOUSE"
        Me.WAREHOUSEBindingSource.DataSource = Me.DS_SALE1
        '
        'DS_SALE1
        '
        Me.DS_SALE1.DataSetName = "DS_SALE"
        Me.DS_SALE1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'txtDTP
        '
        Me.txtDTP.Location = New System.Drawing.Point(104, 42)
        Me.txtDTP.Name = "txtDTP"
        Me.txtDTP.Size = New System.Drawing.Size(101, 20)
        Me.txtDTP.TabIndex = 2
        '
        'PAYMENT_METHOD_IDComboBox
        '
        Me.PAYMENT_METHOD_IDComboBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PAYMENT_METHOD_IDComboBox.DataSource = Me.PAYMENT_METHODBindingSource
        Me.PAYMENT_METHOD_IDComboBox.DisplayMember = "PAYMENT_METHOD_NAME"
        Me.PAYMENT_METHOD_IDComboBox.FormattingEnabled = True
        Me.PAYMENT_METHOD_IDComboBox.Location = New System.Drawing.Point(551, 14)
        Me.PAYMENT_METHOD_IDComboBox.Name = "PAYMENT_METHOD_IDComboBox"
        Me.PAYMENT_METHOD_IDComboBox.Size = New System.Drawing.Size(149, 22)
        Me.PAYMENT_METHOD_IDComboBox.TabIndex = 4
        Me.PAYMENT_METHOD_IDComboBox.ValueMember = "PAYMENT_METHOD_ID"
        '
        'PAYMENT_METHODBindingSource
        '
        Me.PAYMENT_METHODBindingSource.DataMember = "PAYMENT_METHOD"
        Me.PAYMENT_METHODBindingSource.DataSource = Me.DS_SALE
        '
        'lblSaleDate
        '
        Me.lblSaleDate.Location = New System.Drawing.Point(9, 45)
        Me.lblSaleDate.Name = "lblSaleDate"
        Me.lblSaleDate.Size = New System.Drawing.Size(89, 14)
        Me.lblSaleDate.TabIndex = 29
        Me.lblSaleDate.Text = "Sale Date"
        Me.lblSaleDate.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cmdSearchName
        '
        Me.cmdSearchName.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchName.Image = CType(resources.GetObject("cmdSearchName.Image"), System.Drawing.Image)
        Me.cmdSearchName.Location = New System.Drawing.Point(281, 67)
        Me.cmdSearchName.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchName.Name = "cmdSearchName"
        Me.cmdSearchName.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearchName.TabIndex = 18
        Me.cmdSearchName.TabStop = False
        '
        'RECEIPT_NOTextBox
        '
        Me.RECEIPT_NOTextBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.RECEIPT_NOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SALEBindingSource, "RECEIPT_NO", True))
        Me.RECEIPT_NOTextBox.Location = New System.Drawing.Point(104, 16)
        Me.RECEIPT_NOTextBox.Name = "RECEIPT_NOTextBox"
        Me.RECEIPT_NOTextBox.Size = New System.Drawing.Size(117, 20)
        Me.RECEIPT_NOTextBox.TabIndex = 1
        Me.RECEIPT_NOTextBox.Tag = "M"
        '
        'SALE_DATEDateTimePicker
        '
        Me.SALE_DATEDateTimePicker.CustomFormat = "dd-MMM-yyyy"
        Me.SALE_DATEDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.SALEBindingSource, "SALE_DATE", True))
        Me.SALE_DATEDateTimePicker.Enabled = False
        Me.SALE_DATEDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.SALE_DATEDateTimePicker.Location = New System.Drawing.Point(104, 42)
        Me.SALE_DATEDateTimePicker.Name = "SALE_DATEDateTimePicker"
        Me.SALE_DATEDateTimePicker.Size = New System.Drawing.Size(117, 20)
        Me.SALE_DATEDateTimePicker.TabIndex = 4
        Me.SALE_DATEDateTimePicker.Visible = False
        '
        'CUSTOMER_IDComboBox
        '
        Me.CUSTOMER_IDComboBox.DataSource = Me.CUSTOMERBindingSource
        Me.CUSTOMER_IDComboBox.DisplayMember = "CUSTOMER_NAME"
        Me.CUSTOMER_IDComboBox.FormattingEnabled = True
        Me.CUSTOMER_IDComboBox.Location = New System.Drawing.Point(104, 68)
        Me.CUSTOMER_IDComboBox.Name = "CUSTOMER_IDComboBox"
        Me.CUSTOMER_IDComboBox.Size = New System.Drawing.Size(170, 22)
        Me.CUSTOMER_IDComboBox.TabIndex = 3
        Me.CUSTOMER_IDComboBox.ValueMember = "CUSTOMER_ID"
        '
        'CUSTOMERBindingSource
        '
        Me.CUSTOMERBindingSource.DataMember = "CUSTOMER"
        Me.CUSTOMERBindingSource.DataSource = Me.DS_SALE
        '
        'SALEWAREHOUSEBindingSource
        '
        Me.SALEWAREHOUSEBindingSource.DataMember = "SALE_WAREHOUSE"
        Me.SALEWAREHOUSEBindingSource.DataSource = Me.SALEBindingSource
        '
        'DOWN_PAYMENTTextBox
        '
        Me.DOWN_PAYMENTTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SALEBindingSource, "DOWN_PAYMENT", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.DOWN_PAYMENTTextBox.Location = New System.Drawing.Point(604, 45)
        Me.DOWN_PAYMENTTextBox.MaxLength = 9
        Me.DOWN_PAYMENTTextBox.Name = "DOWN_PAYMENTTextBox"
        Me.DOWN_PAYMENTTextBox.Size = New System.Drawing.Size(113, 20)
        Me.DOWN_PAYMENTTextBox.TabIndex = 18
        Me.DOWN_PAYMENTTextBox.Text = "0"
        Me.DOWN_PAYMENTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'AMOUNT_DISCOUNTTextBox
        '
        Me.AMOUNT_DISCOUNTTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SALEBindingSource, "AMOUNT_DISCOUNT", True))
        Me.AMOUNT_DISCOUNTTextBox.Location = New System.Drawing.Point(394, 19)
        Me.AMOUNT_DISCOUNTTextBox.Name = "AMOUNT_DISCOUNTTextBox"
        Me.AMOUNT_DISCOUNTTextBox.Size = New System.Drawing.Size(113, 20)
        Me.AMOUNT_DISCOUNTTextBox.TabIndex = 10
        Me.AMOUNT_DISCOUNTTextBox.Text = "0"
        Me.AMOUNT_DISCOUNTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GRAND_TOTALTextBox
        '
        Me.GRAND_TOTALTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SALEBindingSource, "GRAND_TOTAL", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, Nothing, "N0"))
        Me.GRAND_TOTALTextBox.Location = New System.Drawing.Point(604, 19)
        Me.GRAND_TOTALTextBox.Name = "GRAND_TOTALTextBox"
        Me.GRAND_TOTALTextBox.Size = New System.Drawing.Size(113, 20)
        Me.GRAND_TOTALTextBox.TabIndex = 17
        Me.GRAND_TOTALTextBox.Text = "0"
        Me.GRAND_TOTALTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'REMARKTextBox
        '
        Me.REMARKTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SALEBindingSource, "REMARK", True))
        Me.REMARKTextBox.Location = New System.Drawing.Point(80, 19)
        Me.REMARKTextBox.Multiline = True
        Me.REMARKTextBox.Name = "REMARKTextBox"
        Me.REMARKTextBox.Size = New System.Drawing.Size(197, 51)
        Me.REMARKTextBox.TabIndex = 8
        '
        'SALETableAdapter
        '
        Me.SALETableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.CUSTOMERTableAdapter = Me.CUSTOMERTableAdapter
        Me.TableAdapterManager.PAYMENT_METHODTableAdapter = Me.PAYMENT_METHODTableAdapter
        Me.TableAdapterManager.PRODUCTTableAdapter = Me.PRODUCTTableAdapter
        Me.TableAdapterManager.RECEIVABLETableAdapter = Nothing
        Me.TableAdapterManager.SALE_DETAILTableAdapter = Me.SALE_DETAILTableAdapter
        Me.TableAdapterManager.SALESMANTableAdapter = Nothing
        Me.TableAdapterManager.SALETableAdapter = Me.SALETableAdapter
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_SALE.DS_SALETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.WAREHOUSETableAdapter = Nothing
        '
        'CUSTOMERTableAdapter
        '
        Me.CUSTOMERTableAdapter.ClearBeforeFill = True
        '
        'PAYMENT_METHODTableAdapter
        '
        Me.PAYMENT_METHODTableAdapter.ClearBeforeFill = True
        '
        'PRODUCTTableAdapter
        '
        Me.PRODUCTTableAdapter.ClearBeforeFill = True
        '
        'SALE_DETAILTableAdapter
        '
        Me.SALE_DETAILTableAdapter.ClearBeforeFill = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblTax)
        Me.GroupBox2.Controls.Add(Me.TAX_AMOUNTTextBox)
        Me.GroupBox2.Controls.Add(Me.TAXPercentageTEXTBOX)
        Me.GroupBox2.Controls.Add(Me.lblSalesman)
        Me.GroupBox2.Controls.Add(Me.cmbSALESMAN_NAME)
        Me.GroupBox2.Controls.Add(Me.txtDiscount3)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.txtDisc3)
        Me.GroupBox2.Controls.Add(Me.txtDiscount2)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.txtDisc2)
        Me.GroupBox2.Controls.Add(Me.lblPayment)
        Me.GroupBox2.Controls.Add(Me.lblGrandTotal)
        Me.GroupBox2.Controls.Add(Me.lblDiscount)
        Me.GroupBox2.Controls.Add(Me.lblChange)
        Me.GroupBox2.Controls.Add(Me.lblChg)
        Me.GroupBox2.Controls.Add(Me.lblremark)
        Me.GroupBox2.Controls.Add(Me.REMARKTextBox)
        Me.GroupBox2.Controls.Add(Me.GRAND_TOTALTextBox)
        Me.GroupBox2.Controls.Add(Me.AMOUNT_DISCOUNTTextBox)
        Me.GroupBox2.Controls.Add(Me.DOWN_PAYMENTTextBox)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 457)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(750, 127)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'lblTax
        '
        Me.lblTax.Location = New System.Drawing.Point(281, 100)
        Me.lblTax.Name = "lblTax"
        Me.lblTax.Size = New System.Drawing.Size(59, 14)
        Me.lblTax.TabIndex = 49
        Me.lblTax.Text = "Tax [F2]"
        Me.lblTax.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'TAX_AMOUNTTextBox
        '
        Me.TAX_AMOUNTTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SALEBindingSource, "AMOUNT_DISCOUNT", True))
        Me.TAX_AMOUNTTextBox.Location = New System.Drawing.Point(394, 97)
        Me.TAX_AMOUNTTextBox.MaxLength = 9
        Me.TAX_AMOUNTTextBox.Name = "TAX_AMOUNTTextBox"
        Me.TAX_AMOUNTTextBox.Size = New System.Drawing.Size(113, 20)
        Me.TAX_AMOUNTTextBox.TabIndex = 16
        Me.TAX_AMOUNTTextBox.Text = "0"
        Me.TAX_AMOUNTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TAXPercentageTEXTBOX
        '
        Me.TAXPercentageTEXTBOX.Location = New System.Drawing.Point(344, 97)
        Me.TAXPercentageTEXTBOX.MaxLength = 4
        Me.TAXPercentageTEXTBOX.Name = "TAXPercentageTEXTBOX"
        Me.TAXPercentageTEXTBOX.Size = New System.Drawing.Size(44, 20)
        Me.TAXPercentageTEXTBOX.TabIndex = 15
        Me.TAXPercentageTEXTBOX.Text = "0.00"
        Me.TAXPercentageTEXTBOX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblSalesman
        '
        Me.lblSalesman.AutoSize = True
        Me.lblSalesman.Location = New System.Drawing.Point(15, 79)
        Me.lblSalesman.Name = "lblSalesman"
        Me.lblSalesman.Size = New System.Drawing.Size(59, 14)
        Me.lblSalesman.TabIndex = 46
        Me.lblSalesman.Text = "Salesman"
        '
        'cmbSALESMAN_NAME
        '
        Me.cmbSALESMAN_NAME.DataSource = Me.SALESMANBindingSource
        Me.cmbSALESMAN_NAME.DisplayMember = "SALESMAN_NAME"
        Me.cmbSALESMAN_NAME.FormattingEnabled = True
        Me.cmbSALESMAN_NAME.Location = New System.Drawing.Point(80, 76)
        Me.cmbSALESMAN_NAME.Name = "cmbSALESMAN_NAME"
        Me.cmbSALESMAN_NAME.Size = New System.Drawing.Size(172, 22)
        Me.cmbSALESMAN_NAME.TabIndex = 9
        Me.cmbSALESMAN_NAME.ValueMember = "SALESMAN_ID"
        '
        'SALESMANBindingSource
        '
        Me.SALESMANBindingSource.DataMember = "SALESMAN"
        Me.SALESMANBindingSource.DataSource = Me.DS_SALE
        '
        'txtDiscount3
        '
        Me.txtDiscount3.Location = New System.Drawing.Point(344, 71)
        Me.txtDiscount3.MaxLength = 5
        Me.txtDiscount3.Name = "txtDiscount3"
        Me.txtDiscount3.Size = New System.Drawing.Size(44, 20)
        Me.txtDiscount3.TabIndex = 13
        Me.txtDiscount3.Text = "0.00"
        Me.txtDiscount3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(281, 73)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 14)
        Me.Label2.TabIndex = 43
        Me.Label2.Text = "Disc 3"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtDisc3
        '
        Me.txtDisc3.Location = New System.Drawing.Point(394, 71)
        Me.txtDisc3.MaxLength = 9
        Me.txtDisc3.Name = "txtDisc3"
        Me.txtDisc3.Size = New System.Drawing.Size(113, 20)
        Me.txtDisc3.TabIndex = 14
        Me.txtDisc3.Text = "0"
        Me.txtDisc3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDiscount2
        '
        Me.txtDiscount2.Location = New System.Drawing.Point(344, 45)
        Me.txtDiscount2.MaxLength = 5
        Me.txtDiscount2.Name = "txtDiscount2"
        Me.txtDiscount2.Size = New System.Drawing.Size(44, 20)
        Me.txtDiscount2.TabIndex = 11
        Me.txtDiscount2.Text = "0.00"
        Me.txtDiscount2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(281, 47)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 14)
        Me.Label1.TabIndex = 40
        Me.Label1.Text = "Disc 2"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtDisc2
        '
        Me.txtDisc2.Location = New System.Drawing.Point(394, 45)
        Me.txtDisc2.MaxLength = 9
        Me.txtDisc2.Name = "txtDisc2"
        Me.txtDisc2.Size = New System.Drawing.Size(113, 20)
        Me.txtDisc2.TabIndex = 12
        Me.txtDisc2.Text = "0"
        Me.txtDisc2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPayment
        '
        Me.lblPayment.Location = New System.Drawing.Point(513, 48)
        Me.lblPayment.Name = "lblPayment"
        Me.lblPayment.Size = New System.Drawing.Size(85, 14)
        Me.lblPayment.TabIndex = 35
        Me.lblPayment.Text = "Payment [F3]"
        Me.lblPayment.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblGrandTotal
        '
        Me.lblGrandTotal.Location = New System.Drawing.Point(513, 22)
        Me.lblGrandTotal.Name = "lblGrandTotal"
        Me.lblGrandTotal.Size = New System.Drawing.Size(85, 14)
        Me.lblGrandTotal.TabIndex = 37
        Me.lblGrandTotal.Text = "Grand Total"
        Me.lblGrandTotal.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblDiscount
        '
        Me.lblDiscount.Location = New System.Drawing.Point(314, 22)
        Me.lblDiscount.Name = "lblDiscount"
        Me.lblDiscount.Size = New System.Drawing.Size(75, 14)
        Me.lblDiscount.TabIndex = 38
        Me.lblDiscount.Text = "Discount 1"
        Me.lblDiscount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblChange
        '
        Me.lblChange.AutoSize = True
        Me.lblChange.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChange.Location = New System.Drawing.Point(604, 76)
        Me.lblChange.Name = "lblChange"
        Me.lblChange.Size = New System.Drawing.Size(15, 15)
        Me.lblChange.TabIndex = 16
        Me.lblChange.Text = "0"
        '
        'lblChg
        '
        Me.lblChg.Location = New System.Drawing.Point(548, 76)
        Me.lblChg.Name = "lblChg"
        Me.lblChg.Size = New System.Drawing.Size(48, 14)
        Me.lblChg.TabIndex = 15
        Me.lblChg.Text = "Change"
        Me.lblChg.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblremark
        '
        Me.lblremark.Location = New System.Drawing.Point(4, 22)
        Me.lblremark.Name = "lblremark"
        Me.lblremark.Size = New System.Drawing.Size(70, 43)
        Me.lblremark.TabIndex = 34
        Me.lblremark.Text = "Remark" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F1]"
        Me.lblremark.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lblDel)
        Me.GroupBox3.Controls.Add(Me.lblF6)
        Me.GroupBox3.Controls.Add(Me.lblF5)
        Me.GroupBox3.Controls.Add(Me.lblNoOfItem)
        Me.GroupBox3.Controls.Add(Me.dgvSaleDetail)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 170)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(750, 282)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        '
        'lblDel
        '
        Me.lblDel.Location = New System.Drawing.Point(513, 19)
        Me.lblDel.Name = "lblDel"
        Me.lblDel.Size = New System.Drawing.Size(187, 14)
        Me.lblDel.TabIndex = 36
        Me.lblDel.Text = "Del : Remove Product from List"
        Me.lblDel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblF6
        '
        Me.lblF6.Location = New System.Drawing.Point(254, 19)
        Me.lblF6.Name = "lblF6"
        Me.lblF6.Size = New System.Drawing.Size(180, 14)
        Me.lblF6.TabIndex = 35
        Me.lblF6.Text = "F6 : Browse Product by Name"
        Me.lblF6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblF5
        '
        Me.lblF5.Location = New System.Drawing.Point(6, 19)
        Me.lblF5.Name = "lblF5"
        Me.lblF5.Size = New System.Drawing.Size(175, 14)
        Me.lblF5.TabIndex = 34
        Me.lblF5.Text = "F5 : Browse Product by Code"
        Me.lblF5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblNoOfItem
        '
        Me.lblNoOfItem.AutoSize = True
        Me.lblNoOfItem.Location = New System.Drawing.Point(6, 257)
        Me.lblNoOfItem.Name = "lblNoOfItem"
        Me.lblNoOfItem.Size = New System.Drawing.Size(96, 14)
        Me.lblNoOfItem.TabIndex = 18
        Me.lblNoOfItem.Text = "No of Item(s) :  0"
        '
        'dgvSaleDetail
        '
        Me.dgvSaleDetail.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSaleDetail.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSaleDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSaleDetail.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PRODUCT_CODE, Me.PRODUCT_NAME, Me.QUANTITY, Me.PRICE_PER_UNIT, Me.DISCOUNT, Me.DISCOUNT2, Me.TOTAL, Me.DELETE})
        Me.dgvSaleDetail.Location = New System.Drawing.Point(6, 36)
        Me.dgvSaleDetail.Name = "dgvSaleDetail"
        Me.dgvSaleDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvSaleDetail.Size = New System.Drawing.Size(737, 211)
        Me.dgvSaleDetail.TabIndex = 7
        '
        'PRODUCT_CODE
        '
        Me.PRODUCT_CODE.HeaderText = "Product Code"
        Me.PRODUCT_CODE.Name = "PRODUCT_CODE"
        Me.PRODUCT_CODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'PRODUCT_NAME
        '
        Me.PRODUCT_NAME.HeaderText = "Product Name"
        Me.PRODUCT_NAME.Name = "PRODUCT_NAME"
        Me.PRODUCT_NAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.PRODUCT_NAME.Width = 180
        '
        'QUANTITY
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.QUANTITY.DefaultCellStyle = DataGridViewCellStyle2
        Me.QUANTITY.HeaderText = "Quantity"
        Me.QUANTITY.Name = "QUANTITY"
        Me.QUANTITY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.QUANTITY.Width = 60
        '
        'PRICE_PER_UNIT
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N0"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.PRICE_PER_UNIT.DefaultCellStyle = DataGridViewCellStyle3
        Me.PRICE_PER_UNIT.HeaderText = "Unit Price"
        Me.PRICE_PER_UNIT.Name = "PRICE_PER_UNIT"
        Me.PRICE_PER_UNIT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.PRICE_PER_UNIT.Width = 75
        '
        'DISCOUNT
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.DISCOUNT.DefaultCellStyle = DataGridViewCellStyle4
        Me.DISCOUNT.HeaderText = "Discount (%)"
        Me.DISCOUNT.Name = "DISCOUNT"
        Me.DISCOUNT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DISCOUNT.Width = 60
        '
        'DISCOUNT2
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N0"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.DISCOUNT2.DefaultCellStyle = DataGridViewCellStyle5
        Me.DISCOUNT2.HeaderText = "Discount (Rp)"
        Me.DISCOUNT2.Name = "DISCOUNT2"
        Me.DISCOUNT2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DISCOUNT2.Width = 75
        '
        'TOTAL
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N0"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.TOTAL.DefaultCellStyle = DataGridViewCellStyle6
        Me.TOTAL.HeaderText = "Total"
        Me.TOTAL.Name = "TOTAL"
        Me.TOTAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.TOTAL.Width = 80
        '
        'DELETE
        '
        Me.DELETE.HeaderText = "Del"
        Me.DELETE.Image = Global.DMI_RETAIL_SALE.My.Resources.Resources.BindingNavigatorDeleteItem_Image
        Me.DELETE.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch
        Me.DELETE.Name = "DELETE"
        Me.DELETE.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DELETE.Width = 40
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.cmdUndo)
        Me.GroupBox4.Controls.Add(Me.cmdSave)
        Me.GroupBox4.Controls.Add(Me.cmdEdit)
        Me.GroupBox4.Controls.Add(Me.cmdDelete)
        Me.GroupBox4.Controls.Add(Me.cmdAdd)
        Me.GroupBox4.Location = New System.Drawing.Point(123, 591)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox4.Size = New System.Drawing.Size(515, 55)
        Me.GroupBox4.TabIndex = 2
        Me.GroupBox4.TabStop = False
        '
        'cmdUndo
        '
        Me.cmdUndo.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(216, 11)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(83, 38)
        Me.cmdUndo.TabIndex = 21
        Me.cmdUndo.Text = "&Undo" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F9]"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSave.Location = New System.Drawing.Point(317, 11)
        Me.cmdSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(83, 38)
        Me.cmdSave.TabIndex = 22
        Me.cmdSave.Text = "&Save" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F10]"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(115, 11)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(83, 38)
        Me.cmdEdit.TabIndex = 20
        Me.cmdEdit.Text = "&Edit" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F8]"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(418, 11)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(83, 38)
        Me.cmdDelete.TabIndex = 23
        Me.cmdDelete.Text = "&Delete" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F11]"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(14, 11)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(83, 38)
        Me.cmdAdd.TabIndex = 19
        Me.cmdAdd.Text = "&Add" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F7]"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'PRODUCTBindingSource
        '
        Me.PRODUCTBindingSource.DataMember = "PRODUCT"
        Me.PRODUCTBindingSource.DataSource = Me.DS_SALE
        '
        'SALE_DETAILBindingSource
        '
        Me.SALE_DETAILBindingSource.DataMember = "SALE_DETAIL"
        Me.SALE_DETAILBindingSource.DataSource = Me.DS_SALE
        '
        'cmdPrint
        '
        Me.cmdPrint.AutoSize = True
        Me.cmdPrint.Font = New System.Drawing.Font("Lucida Bright", 7.0!)
        Me.cmdPrint.Image = Global.DMI_RETAIL_SALE.My.Resources.Resources.Printer
        Me.cmdPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdPrint.Location = New System.Drawing.Point(659, 599)
        Me.cmdPrint.Name = "cmdPrint"
        Me.cmdPrint.Size = New System.Drawing.Size(96, 52)
        Me.cmdPrint.TabIndex = 24
        Me.cmdPrint.Text = "Save &&" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "&Print" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "[F12]"
        Me.cmdPrint.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.cmdPrint.UseVisualStyleBackColor = True
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'SALEBindingNavigator
        '
        Me.SALEBindingNavigator.AddNewItem = Nothing
        Me.SALEBindingNavigator.BindingSource = Me.SALEBindingSource
        Me.SALEBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.SALEBindingNavigator.DeleteItem = Nothing
        Me.SALEBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.SALEBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.SALEBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.SALEBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.SALEBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.SALEBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.SALEBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.SALEBindingNavigator.Name = "SALEBindingNavigator"
        Me.SALEBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.SALEBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.SALEBindingNavigator.Size = New System.Drawing.Size(766, 25)
        Me.SALEBindingNavigator.TabIndex = 1
        Me.SALEBindingNavigator.Text = "BindingNavigator1"
        '
        'rpvSaleInvoice
        '
        ReportDataSource1.Name = "DS_SALE_SP_SALE_INVOICE"
        ReportDataSource1.Value = Me.SP_SALE_INVOICEBindingSource
        Me.rpvSaleInvoice.LocalReport.DataSources.Add(ReportDataSource1)
        Me.rpvSaleInvoice.LocalReport.ReportEmbeddedResource = "DMI_RETAIL_SALE.repEntrySale.rdlc"
        Me.rpvSaleInvoice.Location = New System.Drawing.Point(42, 599)
        Me.rpvSaleInvoice.Name = "rpvSaleInvoice"
        Me.rpvSaleInvoice.Size = New System.Drawing.Size(36, 27)
        Me.rpvSaleInvoice.TabIndex = 19
        Me.rpvSaleInvoice.Visible = False
        '
        'SP_SALE_INVOICETableAdapter
        '
        Me.SP_SALE_INVOICETableAdapter.ClearBeforeFill = True
        '
        'VIEW_LIST_SALEBindingSource
        '
        Me.VIEW_LIST_SALEBindingSource.DataMember = "VIEW_LIST_SALE"
        Me.VIEW_LIST_SALEBindingSource.DataSource = Me.DS_SALE
        '
        'VIEW_LIST_SALETableAdapter
        '
        Me.VIEW_LIST_SALETableAdapter.ClearBeforeFill = True
        '
        'VIEW_LIST_SALE_DETAILBindingSource
        '
        Me.VIEW_LIST_SALE_DETAILBindingSource.DataMember = "VIEW_LIST_SALE_DETAIL"
        Me.VIEW_LIST_SALE_DETAILBindingSource.DataSource = Me.DS_SALE
        '
        'VIEW_LIST_SALE_DETAILTableAdapter
        '
        Me.VIEW_LIST_SALE_DETAILTableAdapter.ClearBeforeFill = True
        '
        'DS_PRODUCT_WAREHOUSE
        '
        Me.DS_PRODUCT_WAREHOUSE.DataSetName = "DS_PRODUCT_WAREHOUSE"
        Me.DS_PRODUCT_WAREHOUSE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PRODUCT_WAREHOUSEBindingSource
        '
        Me.PRODUCT_WAREHOUSEBindingSource.DataMember = "PRODUCT_WAREHOUSE"
        Me.PRODUCT_WAREHOUSEBindingSource.DataSource = Me.DS_PRODUCT_WAREHOUSE
        '
        'PRODUCT_WAREHOUSETableAdapter
        '
        Me.PRODUCT_WAREHOUSETableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager1
        '
        Me.TableAdapterManager1.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager1.PRODUCT_WAREHOUSETableAdapter = Me.PRODUCT_WAREHOUSETableAdapter
        Me.TableAdapterManager1.UpdateOrder = DMI_RETAIL_SALE.DS_PRODUCT_WAREHOUSETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'SP_PRODUCT_WAREHOUSE_DETAILBindingSource
        '
        Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource.DataMember = "SP_PRODUCT_WAREHOUSE_DETAIL"
        Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource.DataSource = Me.DS_PRODUCT_WAREHOUSE
        '
        'SP_PRODUCT_WAREHOUSE_DETAILTableAdapter
        '
        Me.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.ClearBeforeFill = True
        '
        'WAREHOUSETableAdapter
        '
        Me.WAREHOUSETableAdapter.ClearBeforeFill = True
        '
        'SP_SPLIT_PRODUCTBindingSource
        '
        Me.SP_SPLIT_PRODUCTBindingSource.DataMember = "SP_SPLIT_PRODUCT"
        Me.SP_SPLIT_PRODUCTBindingSource.DataSource = Me.DS_SALE
        '
        'SP_SPLIT_PRODUCTTableAdapter
        '
        Me.SP_SPLIT_PRODUCTTableAdapter.ClearBeforeFill = True
        '
        'SALESMANTableAdapter
        '
        Me.SALESMANTableAdapter.ClearBeforeFill = True
        '
        'SP_SELECT_SALE_ORDER_DETAILBindingSource
        '
        Me.SP_SELECT_SALE_ORDER_DETAILBindingSource.DataMember = "SP_SELECT_SALE_ORDER_DETAIL"
        Me.SP_SELECT_SALE_ORDER_DETAILBindingSource.DataSource = Me.DS_SALE1
        '
        'SP_SELECT_SALE_ORDER_DETAILTableAdapter
        '
        Me.SP_SELECT_SALE_ORDER_DETAILTableAdapter.ClearBeforeFill = True
        '
        'frmEntrySale
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(766, 655)
        Me.Controls.Add(Me.rpvSaleInvoice)
        Me.Controls.Add(Me.cmdPrint)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.SALEBindingNavigator)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 8.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmEntrySale"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Entry Sale"
        Me.Text = "Entry Sale"
        CType(Me.SP_SALE_INVOICEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_SALE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cmdSearchSaleOrder, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SALEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_SALE1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PAYMENT_METHODBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CUSTOMERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SALEWAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.SALESMANBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.dgvSaleDetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SALE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SALEBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SALEBindingNavigator.ResumeLayout(False)
        Me.SALEBindingNavigator.PerformLayout()
        CType(Me.VIEW_LIST_SALEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VIEW_LIST_SALE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DS_PRODUCT_WAREHOUSE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PRODUCT_WAREHOUSEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_PRODUCT_WAREHOUSE_DETAILBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_SPLIT_PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_SELECT_SALE_ORDER_DETAILBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents DS_SALE As DMI_RETAIL_SALE.DS_SALE
    Friend WithEvents SALEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SALETableAdapter As DMI_RETAIL_SALE.DS_SALETableAdapters.SALETableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_SALE.DS_SALETableAdapters.TableAdapterManager
    Friend WithEvents RECEIPT_NOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SALE_DATEDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents CUSTOMER_IDComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents DOWN_PAYMENTTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AMOUNT_DISCOUNTTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GRAND_TOTALTextBox As System.Windows.Forms.TextBox
    Friend WithEvents REMARKTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CUSTOMERTableAdapter As DMI_RETAIL_SALE.DS_SALETableAdapters.CUSTOMERTableAdapter
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents CUSTOMERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvSaleDetail As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents PRODUCTTableAdapter As DMI_RETAIL_SALE.DS_SALETableAdapters.PRODUCTTableAdapter
    Friend WithEvents PRODUCTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SALE_DETAILTableAdapter As DMI_RETAIL_SALE.DS_SALETableAdapters.SALE_DETAILTableAdapter
    Friend WithEvents SALE_DETAILBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents lblChg As System.Windows.Forms.Label
    Friend WithEvents lblChange As System.Windows.Forms.Label
    Friend WithEvents cmdPrint As System.Windows.Forms.Button
    Friend WithEvents lblNoOfItem As System.Windows.Forms.Label
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SALEBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents rpvSaleInvoice As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents SP_SALE_INVOICEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_SALE_INVOICETableAdapter As DMI_RETAIL_SALE.DS_SALETableAdapters.SP_SALE_INVOICETableAdapter
    Friend WithEvents VIEW_LIST_SALEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VIEW_LIST_SALETableAdapter As DMI_RETAIL_SALE.DS_SALETableAdapters.VIEW_LIST_SALETableAdapter
    Friend WithEvents VIEW_LIST_SALE_DETAILBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VIEW_LIST_SALE_DETAILTableAdapter As DMI_RETAIL_SALE.DS_SALETableAdapters.VIEW_LIST_SALE_DETAILTableAdapter
    Friend WithEvents DS_PRODUCT_WAREHOUSE As DMI_RETAIL_SALE.DS_PRODUCT_WAREHOUSE
    Friend WithEvents PRODUCT_WAREHOUSEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PRODUCT_WAREHOUSETableAdapter As DMI_RETAIL_SALE.DS_PRODUCT_WAREHOUSETableAdapters.PRODUCT_WAREHOUSETableAdapter
    Friend WithEvents TableAdapterManager1 As DMI_RETAIL_SALE.DS_PRODUCT_WAREHOUSETableAdapters.TableAdapterManager
    Friend WithEvents SP_PRODUCT_WAREHOUSE_DETAILBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_PRODUCT_WAREHOUSE_DETAILTableAdapter As DMI_RETAIL_SALE.DS_PRODUCT_WAREHOUSETableAdapters.SP_PRODUCT_WAREHOUSE_DETAILTableAdapter
    Friend WithEvents cmdSearchName As System.Windows.Forms.PictureBox
    Friend WithEvents PAYMENT_METHODTableAdapter As DMI_RETAIL_SALE.DS_SALETableAdapters.PAYMENT_METHODTableAdapter
    Friend WithEvents PAYMENT_METHODBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PAYMENT_METHOD_IDComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents txtDTP As System.Windows.Forms.TextBox
    Friend WithEvents SALEWAREHOUSEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents WAREHOUSETableAdapter As DMI_RETAIL_SALE.DS_SALETableAdapters.WAREHOUSETableAdapter
    Friend WithEvents WAREHOUSE_IDComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents DS_SALE1 As DMI_RETAIL_SALE.DS_SALE
    Friend WithEvents WAREHOUSEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents lblReceipNo As System.Windows.Forms.Label
    Friend WithEvents lblCustomer As System.Windows.Forms.Label
    Friend WithEvents lblWarehouse As System.Windows.Forms.Label
    Friend WithEvents lblMethod As System.Windows.Forms.Label
    Friend WithEvents lblMaturity As System.Windows.Forms.Label
    Friend WithEvents MATURITY_DATEDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblSaleDate As System.Windows.Forms.Label
    Friend WithEvents lblPayment As System.Windows.Forms.Label
    Friend WithEvents lblGrandTotal As System.Windows.Forms.Label
    Friend WithEvents lblDiscount As System.Windows.Forms.Label
    Friend WithEvents lblremark As System.Windows.Forms.Label
    Friend WithEvents lblDel As System.Windows.Forms.Label
    Friend WithEvents lblF6 As System.Windows.Forms.Label
    Friend WithEvents lblF5 As System.Windows.Forms.Label
    Friend WithEvents PRODUCT_CODE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRODUCT_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QUANTITY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRICE_PER_UNIT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DISCOUNT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DISCOUNT2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DELETE As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents SP_SPLIT_PRODUCTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_SPLIT_PRODUCTTableAdapter As DMI_RETAIL_SALE.DS_SALETableAdapters.SP_SPLIT_PRODUCTTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtDisc2 As System.Windows.Forms.TextBox
    Friend WithEvents txtDiscount3 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDisc3 As System.Windows.Forms.TextBox
    Friend WithEvents txtDiscount2 As System.Windows.Forms.TextBox
    Friend WithEvents SALESMANBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SALESMANTableAdapter As DMI_RETAIL_SALE.DS_SALETableAdapters.SALESMANTableAdapter
    Friend WithEvents cmbSALESMAN_NAME As System.Windows.Forms.ComboBox
    Friend WithEvents lblSalesman As System.Windows.Forms.Label
    Friend WithEvents lblTax As System.Windows.Forms.Label
    Friend WithEvents TAX_AMOUNTTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TAXPercentageTEXTBOX As System.Windows.Forms.TextBox
    Friend WithEvents cmdSearchSaleOrder As System.Windows.Forms.PictureBox
    Friend WithEvents txtSaleOrderNo As System.Windows.Forms.TextBox
    Friend WithEvents lblSaleOrderNo As System.Windows.Forms.Label
    Friend WithEvents dtpSaleOrderDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents SP_SELECT_SALE_ORDER_DETAILBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_SELECT_SALE_ORDER_DETAILTableAdapter As DMI_RETAIL_SALE.DS_SALETableAdapters.SP_SELECT_SALE_ORDER_DETAILTableAdapter
End Class
