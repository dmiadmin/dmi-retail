﻿Imports System.Data.SqlClient
Imports System.Data
Public Class frmListProductDistribution


    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
   

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "DECLARE @return_value int" & vbCrLf & _
              "EXEC    @return_value = [dbo].[SP_PRODUCT_DISTRIBUTION_LIST]" & vbCrLf & _
              "                @PERIOD = N'" & dtpPeriod.Value & "'," & vbCrLf & _
              "                @PRODUCT_NAME = N'" & txtProductName.Text & "'" & vbCrLf & _
              "SELECT 'Return Value' = @return_value"

        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.SP_PRODUCT_DISTRIBUTION_LISTDataGridView.DataSource = dt
        If Language = "Indonesian" Then
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("PRODUCT_DISTRIBUTION_DATE").HeaderText = "Tanggal Transaksi"
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("PRODUCT_DISTRIBUTION_NUMBER").HeaderText = "Nomor Transaksi"
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("SOURCE_NAME").HeaderText = "Asal"
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("DESTINATION_NAME").HeaderText = "Tujuan"
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("QUANTITY").HeaderText = "Jumlah"
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("PRODUCT_ID").Visible = False
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("SOURCE_ID").Visible = False
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("DESTINATION_ID").Visible = False
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("PRODUCT_DISTRIBUTION_ID").Visible = False
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("PRODUCT_DISTRIBUTION_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("QUANTITY").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("PRODUCT_DISTRIBUTION_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("PRODUCT_NAME").Width = 200
        Else
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("PRODUCT_DISTRIBUTION_DATE").HeaderText = "Transaction Date"
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("PRODUCT_DISTRIBUTION_NUMBER").HeaderText = "Nomor Transaksi"
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("SOURCE_NAME").HeaderText = "Asal"
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("DESTINATION_NAME").HeaderText = "Tujuan"
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("PRODUCT_NAME").HeaderText = "Product Name"
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("QUANTITY").HeaderText = "Qty"
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("PRODUCT_ID").Visible = False
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("SOURCE_ID").Visible = False
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("DESTINATION_ID").Visible = False
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("PRODUCT_DISTRIBUTION_ID").Visible = False
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("PRODUCT_DISTRIBUTION_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("QUANTITY").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("PRODUCT_DISTRIBUTION_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("PRODUCT_NAME").Width = 200
            'SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("").DefaultCellStyle.Format = ""
            'SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            'SP_PRODUCT_DISTRIBUTION_LISTDataGridView.Columns("").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End If
        Try
            
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtProductName.Text = ""
            txtProductName.Focus()
        End Try
    End Sub

    Private Sub txtProductName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProductName.TextChanged
        Try
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
             sql = "DECLARE @return_value int" & vbCrLf & _
             "EXEC    @return_value = [dbo].[SP_PRODUCT_DISTRIBUTION_LIST]" & vbCrLf & _
             "                @PERIOD = N'" & dtpPeriod.Value & "'," & vbCrLf & _
             "                @PRODUCT_NAME = N'" & txtProductName.Text & "'" & vbCrLf & _
             "SELECT 'Return Value' = @return_value"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.SP_PRODUCT_DISTRIBUTION_LISTDataGridView.DataSource = dt
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtProductName.Text = ""
            txtProductName.Focus()
        End Try
    End Sub

    'Private Sub frmListProductDistribution_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
    '    If e.KeyCode = Keys.F12 Then
    '        cmdReport_Click(Nothing, Nothing)
    '    End If
    'End Sub

    Private Sub frmListProductDistribution_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Language = "Indonesian" Then
            Label1.Text = "Periode"
            Label2.Text = "Nama Produk"
            cmdGenerate.Text = "PROSES"
            cmdReport.Text = "Cetak"
            Me.Text = "Daftar Distribusi Produk"
        End If
    End Sub

    'Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
    '    Dim RPV As New frmRepProductDistribution
    '    RPV.period = dtpPeriod.Value
    '    RPV.prodname = txtProductName.Text
    '    RPV.ReportViewer1.ZoomPercent = 100
    '    RPV.WindowState = FormWindowState.Maximized
    '    RPV.Show()
    'End Sub

End Class