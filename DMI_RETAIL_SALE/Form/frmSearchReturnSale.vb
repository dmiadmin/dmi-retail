﻿Public Class frmSearchReturnSale
    Public tmpCustomuer, tmpPayment, tmpReceiptNo As String
    Public tmpDate As DateTime
    Public tmpSaleId, tmpTotal As Integer

    Private Sub frmSearchReturnSale_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_RETURN_SALE.SP_SEARCH_SALE_RETURN' table. You can move, or remove it, as needed.
        Me.SP_SEARCH_SALE_RETURNTableAdapter.Fill(Me.DS_RETURN_SALE.SP_SEARCH_SALE_RETURN)

        If Language = "Indonesian" Then
            Me.Text = "Pencarian Retur Penjualan"

            dgvSearchReturnSale.Columns("CUSTOMER_NAME").HeaderText = "Nama Pelanggan"
            dgvSearchReturnSale.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            dgvSearchReturnSale.Columns("SALE_DATE").HeaderText = "Tgl Penjualan"
            dgvSearchReturnSale.Columns("PAYMENT_METHOD_NAME").HeaderText = "Cara Pembayaran"

            lblCustomer.Text = "Nama Pelanggan"
            lblReceipt.Text = "No Faktur"
        End If

    End Sub

    Private Sub txtCustomer_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCustomer.TextChanged
        Try
            txtReceiptNo.Text = ""
            SP_SEARCH_SALE_RETURNBindingSource.Filter = "CUSTOMER_NAME like '%" & txtCustomer.Text & "%'"
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtCustomer.Text = ""
            txtCustomer.Focus()
        End Try
    End Sub

    Private Sub txtReceiptNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtReceiptNo.TextChanged
        Try
            txtCustomer.Text = ""
            SP_SEARCH_SALE_RETURNBindingSource.Filter = "RECEIPT_NO like '%" & txtReceiptNo.Text & "%'"
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtReceiptNo.Text = ""
            txtReceiptNo.Focus()
        End Try
    End Sub

    Private Sub dgvSearchReturnSale_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSearchReturnSale.DoubleClick
        Try
            tmpCustomuer = IIf(IsDBNull(SP_SEARCH_SALE_RETURNBindingSource.Current("CUSTOMER_NAME")),
                               "",
                               SP_SEARCH_SALE_RETURNBindingSource.Current("CUSTOMER_NAME"))
            tmpPayment = IIf(IsDBNull(SP_SEARCH_SALE_RETURNBindingSource.Current("PAYMENT_METHOD_NAME")),
                             "",
                             SP_SEARCH_SALE_RETURNBindingSource.Current("PAYMENT_METHOD_NAME"))
            tmpReceiptNo = IIf(IsDBNull(SP_SEARCH_SALE_RETURNBindingSource.Current("RECEIPT_NO")),
                               "",
                               SP_SEARCH_SALE_RETURNBindingSource.Current("RECEIPT_NO"))
            tmpDate = SP_SEARCH_SALE_RETURNBindingSource.Current("SALE_DATE")
            tmpSaleId = SP_SEARCH_SALE_RETURNBindingSource.Current("SALE_ID")
            tmpTotal = SP_SEARCH_SALE_RETURNBindingSource.Current("TOTAL")
        Catch
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data Retur Penjualan !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Sale Return data !", MsgBoxStyle.Critical, "DMI Retail")
            End If
        End Try

        Me.Close()
    End Sub

    Private Sub dgvSearchReturnSale_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvSearchReturnSale.KeyDown
        If e.KeyCode = Keys.Enter Then
            Try
                tmpCustomuer = IIf(IsDBNull(SP_SEARCH_SALE_RETURNBindingSource.Current("CUSTOMER_NAME")),
                                   "",
                                   SP_SEARCH_SALE_RETURNBindingSource.Current("CUSTOMER_NAME"))
                tmpPayment = IIf(IsDBNull(SP_SEARCH_SALE_RETURNBindingSource.Current("PAYMENT_METHOD_NAME")),
                                 "",
                                 SP_SEARCH_SALE_RETURNBindingSource.Current("PAYMENT_METHOD_NAME"))
                tmpReceiptNo = IIf(IsDBNull(SP_SEARCH_SALE_RETURNBindingSource.Current("RECEIPT_NO")),
                                   "",
                                   SP_SEARCH_SALE_RETURNBindingSource.Current("RECEIPT_NO"))
                tmpDate = SP_SEARCH_SALE_RETURNBindingSource.Current("SALE_DATE")
                tmpSaleId = SP_SEARCH_SALE_RETURNBindingSource.Current("SALE_ID")
                tmpTotal = SP_SEARCH_SALE_RETURNBindingSource.Current("TOTAL")
            Catch
                If Language = "Indonesian" Then
                    MsgBox("Tidak ada data Retur Penjualan !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("There is no Sale Return data !", MsgBoxStyle.Critical, "DMI Retail")
                End If
            End Try

        ElseIf e.KeyCode = Keys.Escape Then
            tmpCustomuer = ""
            tmpPayment = ""
            tmpReceiptNo = ""
            tmpDate = ""
            tmpSaleId = ""
            tmpTotal = ""
        End If

        Me.Close()
    End Sub
End Class