﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEntryUser
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEntryUser))
        Me.USERBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdUndo = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmdSearchName = New System.Windows.Forms.PictureBox()
        Me.ACCESS_PRIVILEDGE_NAMEComboBox = New System.Windows.Forms.ComboBox()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.lblPasswordHint = New System.Windows.Forms.Label()
        Me.lblAcountType = New System.Windows.Forms.Label()
        Me.lblConfirmPass = New System.Windows.Forms.Label()
        Me.lblPassword = New System.Windows.Forms.Label()
        Me.lblUser = New System.Windows.Forms.Label()
        Me.ACTIVECheckBox = New System.Windows.Forms.CheckBox()
        Me.PASSWORD_HINTTextBox = New System.Windows.Forms.TextBox()
        Me.CONFIRM_PASSWORDTextBox = New System.Windows.Forms.TextBox()
        Me.USER_NAMETextBox = New System.Windows.Forms.TextBox()
        Me.PASSWORDTextBox = New System.Windows.Forms.TextBox()
        CType(Me.USERBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.USERBindingNavigator.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'USERBindingNavigator
        '
        Me.USERBindingNavigator.AddNewItem = Nothing
        Me.USERBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.USERBindingNavigator.DeleteItem = Nothing
        Me.USERBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.USERBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.USERBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.USERBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.USERBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.USERBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.USERBindingNavigator.Name = "USERBindingNavigator"
        Me.USERBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.USERBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.USERBindingNavigator.Size = New System.Drawing.Size(540, 25)
        Me.USERBindingNavigator.TabIndex = 0
        Me.USERBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdUndo)
        Me.GroupBox2.Controls.Add(Me.cmdSave)
        Me.GroupBox2.Controls.Add(Me.cmdEdit)
        Me.GroupBox2.Controls.Add(Me.cmdDelete)
        Me.GroupBox2.Controls.Add(Me.cmdAdd)
        Me.GroupBox2.Location = New System.Drawing.Point(13, 240)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Size = New System.Drawing.Size(515, 55)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        '
        'cmdUndo
        '
        Me.cmdUndo.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUndo.Image = CType(resources.GetObject("cmdUndo.Image"), System.Drawing.Image)
        Me.cmdUndo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUndo.Location = New System.Drawing.Point(220, 17)
        Me.cmdUndo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUndo.Name = "cmdUndo"
        Me.cmdUndo.Size = New System.Drawing.Size(74, 29)
        Me.cmdUndo.TabIndex = 20
        Me.cmdUndo.Text = "&Undo"
        Me.cmdUndo.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSave.Location = New System.Drawing.Point(321, 17)
        Me.cmdSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(74, 29)
        Me.cmdSave.TabIndex = 21
        Me.cmdSave.Text = "&Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.Location = New System.Drawing.Point(119, 17)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(74, 29)
        Me.cmdEdit.TabIndex = 19
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.Location = New System.Drawing.Point(422, 17)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(74, 29)
        Me.cmdDelete.TabIndex = 22
        Me.cmdDelete.Text = "&Delete"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdAdd
        '
        Me.cmdAdd.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.Location = New System.Drawing.Point(18, 17)
        Me.cmdAdd.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(74, 29)
        Me.cmdAdd.TabIndex = 18
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmdSearchName)
        Me.GroupBox1.Controls.Add(Me.ACCESS_PRIVILEDGE_NAMEComboBox)
        Me.GroupBox1.Controls.Add(Me.lblStatus)
        Me.GroupBox1.Controls.Add(Me.lblPasswordHint)
        Me.GroupBox1.Controls.Add(Me.lblAcountType)
        Me.GroupBox1.Controls.Add(Me.lblConfirmPass)
        Me.GroupBox1.Controls.Add(Me.lblPassword)
        Me.GroupBox1.Controls.Add(Me.lblUser)
        Me.GroupBox1.Controls.Add(Me.ACTIVECheckBox)
        Me.GroupBox1.Controls.Add(Me.PASSWORD_HINTTextBox)
        Me.GroupBox1.Controls.Add(Me.CONFIRM_PASSWORDTextBox)
        Me.GroupBox1.Controls.Add(Me.USER_NAMETextBox)
        Me.GroupBox1.Controls.Add(Me.PASSWORDTextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 31)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(515, 203)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        '
        'cmdSearchName
        '
        Me.cmdSearchName.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearchName.Image = CType(resources.GetObject("cmdSearchName.Image"), System.Drawing.Image)
        Me.cmdSearchName.Location = New System.Drawing.Point(377, 21)
        Me.cmdSearchName.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdSearchName.Name = "cmdSearchName"
        Me.cmdSearchName.Size = New System.Drawing.Size(44, 23)
        Me.cmdSearchName.TabIndex = 35
        Me.cmdSearchName.TabStop = False
        '
        'ACCESS_PRIVILEDGE_NAMEComboBox
        '
        Me.ACCESS_PRIVILEDGE_NAMEComboBox.DisplayMember = "ACCESS_PRIVILEDGE_ID"
        Me.ACCESS_PRIVILEDGE_NAMEComboBox.FormattingEnabled = True
        Me.ACCESS_PRIVILEDGE_NAMEComboBox.Location = New System.Drawing.Point(206, 106)
        Me.ACCESS_PRIVILEDGE_NAMEComboBox.Name = "ACCESS_PRIVILEDGE_NAMEComboBox"
        Me.ACCESS_PRIVILEDGE_NAMEComboBox.Size = New System.Drawing.Size(200, 23)
        Me.ACCESS_PRIVILEDGE_NAMEComboBox.TabIndex = 16
        Me.ACCESS_PRIVILEDGE_NAMEComboBox.Tag = "M"
        Me.ACCESS_PRIVILEDGE_NAMEComboBox.ValueMember = "ACCESS_PRIVILEDGE_ID"
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Lucida Bright", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(395, 166)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(114, 26)
        Me.lblStatus.TabIndex = 23
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblStatus.Visible = False
        '
        'lblPasswordHint
        '
        Me.lblPasswordHint.Location = New System.Drawing.Point(90, 138)
        Me.lblPasswordHint.Name = "lblPasswordHint"
        Me.lblPasswordHint.Size = New System.Drawing.Size(103, 15)
        Me.lblPasswordHint.TabIndex = 22
        Me.lblPasswordHint.Text = "Password Hint"
        Me.lblPasswordHint.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblAcountType
        '
        Me.lblAcountType.Location = New System.Drawing.Point(90, 109)
        Me.lblAcountType.Name = "lblAcountType"
        Me.lblAcountType.Size = New System.Drawing.Size(103, 15)
        Me.lblAcountType.TabIndex = 21
        Me.lblAcountType.Text = "Acount Type"
        Me.lblAcountType.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblConfirmPass
        '
        Me.lblConfirmPass.Location = New System.Drawing.Point(69, 81)
        Me.lblConfirmPass.Name = "lblConfirmPass"
        Me.lblConfirmPass.Size = New System.Drawing.Size(124, 15)
        Me.lblConfirmPass.TabIndex = 20
        Me.lblConfirmPass.Text = "Confirm Password"
        Me.lblConfirmPass.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblPassword
        '
        Me.lblPassword.Location = New System.Drawing.Point(90, 53)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(103, 15)
        Me.lblPassword.TabIndex = 19
        Me.lblPassword.Text = "Password"
        Me.lblPassword.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblUser
        '
        Me.lblUser.Location = New System.Drawing.Point(122, 25)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(71, 13)
        Me.lblUser.TabIndex = 18
        Me.lblUser.Text = "User Name"
        Me.lblUser.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'ACTIVECheckBox
        '
        Me.ACTIVECheckBox.Location = New System.Drawing.Point(206, 168)
        Me.ACTIVECheckBox.Name = "ACTIVECheckBox"
        Me.ACTIVECheckBox.Size = New System.Drawing.Size(104, 24)
        Me.ACTIVECheckBox.TabIndex = 19
        Me.ACTIVECheckBox.Text = "Active"
        Me.ACTIVECheckBox.UseVisualStyleBackColor = True
        '
        'PASSWORD_HINTTextBox
        '
        Me.PASSWORD_HINTTextBox.Location = New System.Drawing.Point(206, 135)
        Me.PASSWORD_HINTTextBox.Name = "PASSWORD_HINTTextBox"
        Me.PASSWORD_HINTTextBox.Size = New System.Drawing.Size(200, 22)
        Me.PASSWORD_HINTTextBox.TabIndex = 18
        '
        'CONFIRM_PASSWORDTextBox
        '
        Me.CONFIRM_PASSWORDTextBox.Location = New System.Drawing.Point(206, 78)
        Me.CONFIRM_PASSWORDTextBox.MaxLength = 10
        Me.CONFIRM_PASSWORDTextBox.Name = "CONFIRM_PASSWORDTextBox"
        Me.CONFIRM_PASSWORDTextBox.Size = New System.Drawing.Size(200, 22)
        Me.CONFIRM_PASSWORDTextBox.TabIndex = 14
        Me.CONFIRM_PASSWORDTextBox.Tag = "M"
        Me.CONFIRM_PASSWORDTextBox.UseSystemPasswordChar = True
        '
        'USER_NAMETextBox
        '
        Me.USER_NAMETextBox.Location = New System.Drawing.Point(206, 22)
        Me.USER_NAMETextBox.MaxLength = 10
        Me.USER_NAMETextBox.Name = "USER_NAMETextBox"
        Me.USER_NAMETextBox.Size = New System.Drawing.Size(164, 22)
        Me.USER_NAMETextBox.TabIndex = 10
        Me.USER_NAMETextBox.Tag = "M"
        '
        'PASSWORDTextBox
        '
        Me.PASSWORDTextBox.Location = New System.Drawing.Point(206, 50)
        Me.PASSWORDTextBox.MaxLength = 10
        Me.PASSWORDTextBox.Name = "PASSWORDTextBox"
        Me.PASSWORDTextBox.Size = New System.Drawing.Size(200, 22)
        Me.PASSWORDTextBox.TabIndex = 12
        Me.PASSWORDTextBox.Tag = "M"
        Me.PASSWORDTextBox.UseSystemPasswordChar = True
        '
        'frmEntryUser
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(540, 306)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.USERBindingNavigator)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmEntryUser"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "Entry User"
        Me.Text = "Entry User"
        CType(Me.USERBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.USERBindingNavigator.ResumeLayout(False)
        Me.USERBindingNavigator.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cmdSearchName, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents USERBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdUndo As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents USER_NAMETextBox As System.Windows.Forms.TextBox
    Friend WithEvents PASSWORDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CONFIRM_PASSWORDTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PASSWORD_HINTTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ACTIVECheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents lblPasswordHint As System.Windows.Forms.Label
    Friend WithEvents lblAcountType As System.Windows.Forms.Label
    Friend WithEvents lblConfirmPass As System.Windows.Forms.Label
    Friend WithEvents lblPassword As System.Windows.Forms.Label
    Friend WithEvents lblUser As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents ACCESS_PRIVILEDGE_NAMEComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents cmdSearchName As System.Windows.Forms.PictureBox
    Friend WithEvents ACCESS_PRIVILEDGEBindingSource As System.Windows.Forms.BindingSource
End Class
