﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSearchSalesman
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSearchSalesman))
        Me.DS_SALE = New DMI_RETAIL_REPORT.DS_SALE()
        Me.SALESMANBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SALESMANTableAdapter = New DMI_RETAIL_REPORT.DS_SALETableAdapters.SALESMANTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_REPORT.DS_SALETableAdapters.TableAdapterManager()
        Me.SALESMANBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.dgvSearchSalesman = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtSalesman = New System.Windows.Forms.TextBox()
        Me.lblSalesman = New System.Windows.Forms.Label()
        Me.SALESMAN_CODE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALESMAN_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALESMAN_ADDRESS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALESMAN_ADDRESS_CITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALESMAN_ADDRESS_STATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALESMAN_PHONE1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALESMAN_PHONE2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SALESMAN_PHONE3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EFFECTIVE_START_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EFFECTIVE_END_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.USER_ID_INPUT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.INPUT_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.USER_ID_UPDATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UPDATE_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        CType(Me.DS_SALE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SALESMANBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SALESMANBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SALESMANBindingNavigator.SuspendLayout()
        CType(Me.dgvSearchSalesman, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'DS_SALE
        '
        Me.DS_SALE.DataSetName = "DS_SALE"
        Me.DS_SALE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SALESMANBindingSource
        '
        Me.SALESMANBindingSource.DataMember = "SALESMAN"
        Me.SALESMANBindingSource.DataSource = Me.DS_SALE
        '
        'SALESMANTableAdapter
        '
        Me.SALESMANTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.CUSTOMERTableAdapter = Nothing
        Me.TableAdapterManager.PAYMENT_METHODTableAdapter = Nothing
        Me.TableAdapterManager.PRODUCTTableAdapter = Nothing
        Me.TableAdapterManager.RECEIVABLETableAdapter = Nothing
        Me.TableAdapterManager.SALE_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.SALESMANTableAdapter = Me.SALESMANTableAdapter
        Me.TableAdapterManager.SALETableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_REPORT.DS_SALETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.WAREHOUSETableAdapter = Nothing
        '
        'SALESMANBindingNavigator
        '
        Me.SALESMANBindingNavigator.AddNewItem = Nothing
        Me.SALESMANBindingNavigator.BindingSource = Me.SALESMANBindingSource
        Me.SALESMANBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.SALESMANBindingNavigator.DeleteItem = Nothing
        Me.SALESMANBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.SALESMANBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.SALESMANBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.SALESMANBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.SALESMANBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.SALESMANBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.SALESMANBindingNavigator.Name = "SALESMANBindingNavigator"
        Me.SALESMANBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.SALESMANBindingNavigator.Size = New System.Drawing.Size(761, 25)
        Me.SALESMANBindingNavigator.TabIndex = 0
        Me.SALESMANBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'dgvSearchSalesman
        '
        Me.dgvSearchSalesman.AllowUserToAddRows = False
        Me.dgvSearchSalesman.AllowUserToDeleteRows = False
        Me.dgvSearchSalesman.AutoGenerateColumns = False
        Me.dgvSearchSalesman.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSearchSalesman.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSearchSalesman.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSearchSalesman.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SALESMAN_CODE, Me.SALESMAN_NAME, Me.SALESMAN_ADDRESS, Me.SALESMAN_ADDRESS_CITY, Me.SALESMAN_ADDRESS_STATE, Me.SALESMAN_PHONE1, Me.SALESMAN_PHONE2, Me.SALESMAN_PHONE3, Me.EFFECTIVE_START_DATE, Me.EFFECTIVE_END_DATE, Me.USER_ID_INPUT, Me.INPUT_DATE, Me.USER_ID_UPDATE, Me.UPDATE_DATE})
        Me.dgvSearchSalesman.DataSource = Me.SALESMANBindingSource
        Me.dgvSearchSalesman.Location = New System.Drawing.Point(18, 21)
        Me.dgvSearchSalesman.Name = "dgvSearchSalesman"
        Me.dgvSearchSalesman.ReadOnly = True
        Me.dgvSearchSalesman.Size = New System.Drawing.Size(707, 224)
        Me.dgvSearchSalesman.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvSearchSalesman)
        Me.GroupBox1.Location = New System.Drawing.Point(10, 106)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(742, 262)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtSalesman)
        Me.GroupBox2.Controls.Add(Me.lblSalesman)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 35)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(739, 71)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        '
        'txtSalesman
        '
        Me.txtSalesman.Location = New System.Drawing.Point(191, 29)
        Me.txtSalesman.Name = "txtSalesman"
        Me.txtSalesman.Size = New System.Drawing.Size(158, 22)
        Me.txtSalesman.TabIndex = 1
        '
        'lblSalesman
        '
        Me.lblSalesman.AutoSize = True
        Me.lblSalesman.Location = New System.Drawing.Point(48, 32)
        Me.lblSalesman.Name = "lblSalesman"
        Me.lblSalesman.Size = New System.Drawing.Size(100, 15)
        Me.lblSalesman.TabIndex = 0
        Me.lblSalesman.Text = "Salesman Name"
        '
        'SALESMAN_CODE
        '
        Me.SALESMAN_CODE.DataPropertyName = "SALESMAN_CODE"
        Me.SALESMAN_CODE.HeaderText = "Salesman Code"
        Me.SALESMAN_CODE.Name = "SALESMAN_CODE"
        Me.SALESMAN_CODE.ReadOnly = True
        Me.SALESMAN_CODE.Width = 75
        '
        'SALESMAN_NAME
        '
        Me.SALESMAN_NAME.DataPropertyName = "SALESMAN_NAME"
        Me.SALESMAN_NAME.HeaderText = "Name"
        Me.SALESMAN_NAME.Name = "SALESMAN_NAME"
        Me.SALESMAN_NAME.ReadOnly = True
        '
        'SALESMAN_ADDRESS
        '
        Me.SALESMAN_ADDRESS.DataPropertyName = "SALESMAN_ADDRESS"
        Me.SALESMAN_ADDRESS.HeaderText = "Address"
        Me.SALESMAN_ADDRESS.Name = "SALESMAN_ADDRESS"
        Me.SALESMAN_ADDRESS.ReadOnly = True
        '
        'SALESMAN_ADDRESS_CITY
        '
        Me.SALESMAN_ADDRESS_CITY.DataPropertyName = "SALESMAN_ADDRESS_CITY"
        Me.SALESMAN_ADDRESS_CITY.HeaderText = "City"
        Me.SALESMAN_ADDRESS_CITY.Name = "SALESMAN_ADDRESS_CITY"
        Me.SALESMAN_ADDRESS_CITY.ReadOnly = True
        Me.SALESMAN_ADDRESS_CITY.Width = 75
        '
        'SALESMAN_ADDRESS_STATE
        '
        Me.SALESMAN_ADDRESS_STATE.DataPropertyName = "SALESMAN_ADDRESS_STATE"
        Me.SALESMAN_ADDRESS_STATE.HeaderText = "State"
        Me.SALESMAN_ADDRESS_STATE.Name = "SALESMAN_ADDRESS_STATE"
        Me.SALESMAN_ADDRESS_STATE.ReadOnly = True
        Me.SALESMAN_ADDRESS_STATE.Width = 75
        '
        'SALESMAN_PHONE1
        '
        Me.SALESMAN_PHONE1.DataPropertyName = "SALESMAN_PHONE1"
        Me.SALESMAN_PHONE1.HeaderText = "Phone 1"
        Me.SALESMAN_PHONE1.Name = "SALESMAN_PHONE1"
        Me.SALESMAN_PHONE1.ReadOnly = True
        Me.SALESMAN_PHONE1.Width = 75
        '
        'SALESMAN_PHONE2
        '
        Me.SALESMAN_PHONE2.DataPropertyName = "SALESMAN_PHONE2"
        Me.SALESMAN_PHONE2.HeaderText = "Phone 2"
        Me.SALESMAN_PHONE2.Name = "SALESMAN_PHONE2"
        Me.SALESMAN_PHONE2.ReadOnly = True
        Me.SALESMAN_PHONE2.Width = 75
        '
        'SALESMAN_PHONE3
        '
        Me.SALESMAN_PHONE3.DataPropertyName = "SALESMAN_PHONE3"
        Me.SALESMAN_PHONE3.HeaderText = "Phone 3"
        Me.SALESMAN_PHONE3.Name = "SALESMAN_PHONE3"
        Me.SALESMAN_PHONE3.ReadOnly = True
        Me.SALESMAN_PHONE3.Width = 75
        '
        'EFFECTIVE_START_DATE
        '
        Me.EFFECTIVE_START_DATE.DataPropertyName = "EFFECTIVE_START_DATE"
        Me.EFFECTIVE_START_DATE.HeaderText = "EFFECTIVE_START_DATE"
        Me.EFFECTIVE_START_DATE.Name = "EFFECTIVE_START_DATE"
        Me.EFFECTIVE_START_DATE.ReadOnly = True
        Me.EFFECTIVE_START_DATE.Visible = False
        '
        'EFFECTIVE_END_DATE
        '
        Me.EFFECTIVE_END_DATE.DataPropertyName = "EFFECTIVE_END_DATE"
        Me.EFFECTIVE_END_DATE.HeaderText = "EFFECTIVE_END_DATE"
        Me.EFFECTIVE_END_DATE.Name = "EFFECTIVE_END_DATE"
        Me.EFFECTIVE_END_DATE.ReadOnly = True
        Me.EFFECTIVE_END_DATE.Visible = False
        '
        'USER_ID_INPUT
        '
        Me.USER_ID_INPUT.DataPropertyName = "USER_ID_INPUT"
        Me.USER_ID_INPUT.HeaderText = "USER_ID_INPUT"
        Me.USER_ID_INPUT.Name = "USER_ID_INPUT"
        Me.USER_ID_INPUT.ReadOnly = True
        Me.USER_ID_INPUT.Visible = False
        '
        'INPUT_DATE
        '
        Me.INPUT_DATE.DataPropertyName = "INPUT_DATE"
        Me.INPUT_DATE.HeaderText = "INPUT_DATE"
        Me.INPUT_DATE.Name = "INPUT_DATE"
        Me.INPUT_DATE.ReadOnly = True
        Me.INPUT_DATE.Visible = False
        '
        'USER_ID_UPDATE
        '
        Me.USER_ID_UPDATE.DataPropertyName = "USER_ID_UPDATE"
        Me.USER_ID_UPDATE.HeaderText = "USER_ID_UPDATE"
        Me.USER_ID_UPDATE.Name = "USER_ID_UPDATE"
        Me.USER_ID_UPDATE.ReadOnly = True
        Me.USER_ID_UPDATE.Visible = False
        '
        'UPDATE_DATE
        '
        Me.UPDATE_DATE.DataPropertyName = "UPDATE_DATE"
        Me.UPDATE_DATE.HeaderText = "UPDATE_DATE"
        Me.UPDATE_DATE.Name = "UPDATE_DATE"
        Me.UPDATE_DATE.ReadOnly = True
        Me.UPDATE_DATE.Visible = False
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'frmSearchSalesman
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(761, 380)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.SALESMANBindingNavigator)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmSearchSalesman"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Search Salesman"
        CType(Me.DS_SALE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SALESMANBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SALESMANBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SALESMANBindingNavigator.ResumeLayout(False)
        Me.SALESMANBindingNavigator.PerformLayout()
        CType(Me.dgvSearchSalesman, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DS_SALE As DMI_RETAIL_REPORT.DS_SALE
    Friend WithEvents SALESMANBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SALESMANTableAdapter As DMI_RETAIL_REPORT.DS_SALETableAdapters.SALESMANTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_REPORT.DS_SALETableAdapters.TableAdapterManager
    Friend WithEvents SALESMANBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents dgvSearchSalesman As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtSalesman As System.Windows.Forms.TextBox
    Friend WithEvents lblSalesman As System.Windows.Forms.Label
    Friend WithEvents SALESMAN_CODE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALESMAN_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALESMAN_ADDRESS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALESMAN_ADDRESS_CITY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALESMAN_ADDRESS_STATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALESMAN_PHONE1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALESMAN_PHONE2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SALESMAN_PHONE3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EFFECTIVE_START_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EFFECTIVE_END_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents USER_ID_INPUT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents INPUT_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents USER_ID_UPDATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UPDATE_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
