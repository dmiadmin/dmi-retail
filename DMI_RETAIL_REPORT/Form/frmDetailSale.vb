﻿Public Class frmDetailSale
    Dim tmpQTy, tmpDisc, tmpTot, tmpDisc2, tmpDisc3 As Integer
    Public tmpDatePassing As Date
    Private Sub frmDetailSale_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If tmpType = "M" Then
            dgvDetailReportSale.Columns("SALE_DATE").Visible = False
        Else
            dgvDetailReportSale.Columns("SALE_DATE").Visible = True
        End If

        Me.SP_DETAIL_REPORT_SALETableAdapter.Fill(DS_SALE.SP_DETAIL_REPORT_SALE, tmpType, tmpDatePassing)

        tmpQTy = 0
        tmpDisc = 0
        tmpDisc2 = 0
        tmpDisc3 = 0
        tmpTot = 0
        For x As Integer = 0 To dgvDetailReportSale.RowCount - 1
            tmpQTy = tmpQTy + dgvDetailReportSale.Item("QUANTITY", x).Value
            tmpQTy = tmpQTy
            tmpDisc = tmpDisc + dgvDetailReportSale.Item("DISCOUNT1", x).Value
            tmpDisc = tmpDisc
            tmpTot = tmpTot + (dgvDetailReportSale.Item("QUANTITY", x).Value * dgvDetailReportSale.Item("PRICE_PER_UNIT", x).Value)
            tmpTot = tmpTot
            tmpDisc2 = tmpDisc2 + dgvDetailReportSale.Item("DISCOUNT2", x).Value
            tmpDisc2 = tmpDisc2
            tmpDisc3 = tmpDisc3 + dgvDetailReportSale.Item("DISCOUNT3", x).Value
            tmpDisc3 = tmpDisc3
        Next

        lblNoTrans.Text = "No Of Transaction : " & dgvDetailReportSale.RowCount
        lblQty.Text = "Total Qty : " & tmpQTy
        lblDisc.Text = "Total Disc1 : " & FormatNumber(tmpDisc, 0)
        lblDisc2.Text = "Total Disc2 : " & FormatNumber(tmpDisc2, 0)
        lblDisc3.Text = "Total Disc3 : " & FormatNumber(tmpDisc3, 0)
        lblTot.Text = "Grand Total : " & FormatNumber(tmpTot, 0)
    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        Dim RPV As New frmRepSaleDetail
        RPV.tmpDatePassing = tmpDatePassing
        RPV.ReportViewer1.ZoomPercent = 100
        RPV.WindowState = FormWindowState.Maximized
        RPV.Show()
    End Sub
End Class