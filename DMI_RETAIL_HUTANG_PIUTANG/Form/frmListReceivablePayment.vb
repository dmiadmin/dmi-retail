﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frmListReceivablePayment
    Dim tmpAmount As Integer
    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter

    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        Me.Cursor = Cursors.WaitCursor
        ' SP_LIST_RECEIVABLE_PAYMENTTableAdapter.Fill(Me.DS_PAYMENT_TRANSACTION.SP_LIST_RECEIVABLE_PAYMENT, dtpPeriod.Value)
        Dim dt As New DataTable
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
                                "EXEC	@return_value = [dbo].[SP_LIST_RECEIVABLE_PAYMENT]" & vbCrLf & _
                                "		@PERIOD = '" & dtpPeriod.Value & "'," & vbCrLf & _
                                "		@CUSTOMER_NAME = N'" & txtCustomer.Text & "'" & vbCrLf & _
                                "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xAdoAdapter.Fill(dt)
        Me.dgvLIST_RECEIVABLE_PAYMENTDataGridView.DataSource = dt
        Me.Cursor = Cursors.Default

        For x As Integer = 0 To dgvLIST_RECEIVABLE_PAYMENTDataGridView.RowCount - 1
            tmpAmount = tmpAmount + dgvLIST_RECEIVABLE_PAYMENTDataGridView.Item("PAYMENT_AMOUNT", x).Value
            tmpAmount = tmpAmount
        Next

        If Language = "Indonesian" Then
            lblCustomerName.Text = "Nama Pelanggan :"
            lblNoOfTrans.Text = "Jumlah Transaksi : " & dgvLIST_RECEIVABLE_PAYMENTDataGridView.RowCount
            lblTotalAmount.Text = "Jumlah Nominal : " & FormatNumber(tmpAmount, 0)
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("RECEIVABLE_ID").Visible = False
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("REP_PERIOD").Visible = False
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("PAYMENT_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("PAYMENT_AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("TOTAL_RECEIVABLE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("PAYMENT_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("PAYMENT_AMOUNT").DefaultCellStyle.Format = "n0"
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("TOTAL_RECEIVABLE").DefaultCellStyle.Format = "n0"
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("RECEIVABLE_PAYMENT_NUMBER").Width = 75
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("RECEIPT_NO").Width = 75
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("CUSTOMER_NAME").Width = 110
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("TOTAL_RECEIVABLE").Width = 90
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("PAYMENT_AMOUNT").Width = 85
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("PAYMENT_METHOD_ID").Width = 90
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("CUSTOMER_NAME").HeaderText = "Nama Pelanggan"
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("TOTAL_RECEIVABLE").HeaderText = "Total Hutang"
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("PAYMENT_DATE").HeaderText = "Tgl Pembayaran"
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("PAYMENT_AMOUNT").HeaderText = "Jumlah Pembayaran"
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("PAYMENT_METHOD_ID").HeaderText = "Cara Pembayaran"
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("RECEIVABLE_PAYMENT_NUMBER").HeaderText = "No.Pembayaran"
        Else
            lblNoOfTrans.Text = "No Of Transaction : " & dgvLIST_RECEIVABLE_PAYMENTDataGridView.RowCount
            lblTotalAmount.Text = "Jumlah Nominal : " & FormatNumber(tmpAmount, 0)
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("RECEIVABLE_ID").Visible = False
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("REP_PERIOD").Visible = False
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("PAYMENT_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("PAYMENT_AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("TOTAL_RECEIVABLE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("PAYMENT_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("PAYMENT_AMOUNT").DefaultCellStyle.Format = "n0"
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("TOTAL_RECEIVABLE").DefaultCellStyle.Format = "n0"
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("RECEIVABLE_PAYMENT_NUMBER").Width = 75
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("RECEIPT_NO").Width = 75
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("CUSTOMER_NAME").Width = 110
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("TOTAL_RECEIVABLE").Width = 90
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("PAYMENT_AMOUNT").Width = 85
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("PAYMENT_METHOD_ID").Width = 90
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("RECEIPT_NO").HeaderText = "Receipt No"
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("CUSTOMER_NAME").HeaderText = "Customer"
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("TOTAL_RECEIVABLE").HeaderText = "Total Receivable"
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("PAYMENT_DATE").HeaderText = "Payment Date"
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("PAYMENT_AMOUNT").HeaderText = "Payment Amount"
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("PAYMENT_METHOD_ID").HeaderText = "Payment Method"
            dgvLIST_RECEIVABLE_PAYMENTDataGridView.Columns("RECEIVABLE_PAYMENT_NUMBER").HeaderText = "Payment No"
        End If

        tmpAmount = 0
    End Sub

    'Private Sub frmListReceivablePayment_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
    '    If e.KeyCode = Keys.F12 Then
    '        cmdReport_Click(Nothing, Nothing)
    '    End If
    'End Sub

    Private Sub frmListReceivablePayment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dtpPeriod.Value = DateSerial(Today.Year, Today.Month, 1)

        For x As Integer = 0 To dgvLIST_RECEIVABLE_PAYMENTDataGridView.RowCount - 1
            tmpAmount = tmpAmount + dgvLIST_RECEIVABLE_PAYMENTDataGridView.Item("PAYMENT_AMOUNT", x).Value
            tmpAmount = tmpAmount
        Next

        If Language = "Indonesian" Then
            Me.Text = "Daftar Pembayaran Piutang"
            lblPeriod.Text = "Periode"
            cmdGenerate.Text = "Proses"
            cmdReport.Text = "Cetak"
            lblCustomerName.Text = "Pelanggan : "

            lblNoOfTrans.Text = "Jumlah Transaksi : " & dgvLIST_RECEIVABLE_PAYMENTDataGridView.RowCount
            lblTotalAmount.Text = "Jumlah Nominal : " & FormatNumber(tmpAmount, 0)
        Else
            lblNoOfTrans.Text = "No Of Transaction : " & dgvLIST_RECEIVABLE_PAYMENTDataGridView.RowCount
            lblTotalAmount.Text = "Jumlah Nominal : " & FormatNumber(tmpAmount, 0)
        End If

        tmpAmount = 0
    End Sub

    Private Sub txtCustomer_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCustomer.TextChanged
        Try
            Dim xDS_Receivable As New DataSet
            With xComm
                .Connection = xConn
                .CommandText = "DECLARE	@return_value int" & vbCrLf & _
                                "EXEC	@return_value = [dbo].[SP_LIST_RECEIVABLE_PAYMENT]" & vbCrLf & _
                                "		@PERIOD = '" & dtpPeriod.Value & "'," & vbCrLf & _
                                "		@CUSTOMER_NAME = N'" & txtCustomer.Text & "'" & vbCrLf & _
                                "SELECT	'Return Value' = @return_value"

                .CommandType = CommandType.Text
            End With
            xAdoAdapter.SelectCommand = xComm
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            Me.xAdoAdapter.Fill(xDS_Receivable, "RECEIVABLE_PAYMENT")
            Me.dgvLIST_RECEIVABLE_PAYMENTDataGridView.DataSource = xDS_Receivable.Tables("RECEIVABLE_PAYMENT")
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtCustomer.Text = ""
            txtCustomer.Focus()
        End Try

        For x As Integer = 0 To dgvLIST_RECEIVABLE_PAYMENTDataGridView.RowCount - 1
            tmpAmount = tmpAmount + dgvLIST_RECEIVABLE_PAYMENTDataGridView.Item("PAYMENT_AMOUNT", x).Value
            tmpAmount = tmpAmount
        Next

        If Language = "Indonesian" Then
            lblNoOfTrans.Text = "Jumlah Transaksi : " & dgvLIST_RECEIVABLE_PAYMENTDataGridView.RowCount
            lblTotalAmount.Text = "Jumlah Nominal : " & FormatNumber(tmpAmount, 0)
        Else
            lblNoOfTrans.Text = "No Of Transaction : " & dgvLIST_RECEIVABLE_PAYMENTDataGridView.RowCount
            lblTotalAmount.Text = "Jumlah Nominal : " & FormatNumber(tmpAmount, 0)
        End If

        tmpAmount = 0
    End Sub

    'Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
    '    Dim objf As New frmRepReceivablePayment
    '    objf.period = dtpPeriod.Value
    '    objf.customername = txtCustomer.Text
    '    objf.ReportViewer1.ShowRefreshButton = False
    '    objf.ReportViewer1.ZoomPercent = 100
    '    objf.WindowState = FormWindowState.Maximized
    '    objf.Show()
    'End Sub

End Class