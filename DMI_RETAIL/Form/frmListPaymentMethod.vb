﻿Imports System.Data.SqlClient

Public Class frmListPaymentMethod
    Public PAYMENT_METHODBindingSource As New BindingSource

    Private Sub GetData()
        dt = New DataTable
        sqladapter = New SqlDataAdapter("SELECT PAYMENT_METHOD_ID, PAYMENT_METHOD_NAME, DESCRIPTION, " & _
                                        "EFFECTIVE_START_DATE, EFFECTIVE_END_DATE, USER_ID_INPUT, " & _
                                        "INPUT_DATE, USER_ID_UPDATE, UPDATE_DATE, PAYMENT_GROUP " & _
                                        "FROM PAYMENT_METHOD", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        PAYMENT_METHODBindingSource.DataSource = dt
        PAYMENT_METHODBindingNavigator.BindingSource = PAYMENT_METHODBindingSource
        dgvListPaymentMethod.DataSource = PAYMENT_METHODBindingSource
        dgvListPaymentMethod.Columns("PAYMENT_METHOD_ID").Visible = False
        dgvListPaymentMethod.Columns("EFFECTIVE_START_DATE").Visible = False
        dgvListPaymentMethod.Columns("EFFECTIVE_END_DATE").Visible = False
        dgvListPaymentMethod.Columns("USER_ID_INPUT").Visible = False
        dgvListPaymentMethod.Columns("INPUT_DATE").Visible = False
        dgvListPaymentMethod.Columns("USER_ID_UPDATE").Visible = False
        dgvListPaymentMethod.Columns("UPDATE_DATE").Visible = False
    End Sub

    Private Sub frmListPaymentMethod_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetData()
        If Language = "Indonesian" Then
            Me.Text = "Daftar Metode pembayaran"
            lblPayment.Text = "Metode pembayaran"
            chkDeleted.Text = "Aktif saja"
            dgvListPaymentMethod.Columns("PAYMENT_METHOD_NAME").HeaderText = "Metode Pembayaran"
            dgvListPaymentMethod.Columns("PAYMENT_GROUP").HeaderText = "Group Pembayaran"
            dgvListPaymentMethod.Columns("DESCRIPTION").HeaderText = "Keterangan"
        Else
            dgvListPaymentMethod.Columns("PAYMENT_METHOD_NAME").HeaderText = "Payment Method Name"
            dgvListPaymentMethod.Columns("PAYMENT_GROUP").HeaderText = "Payment Group"
            dgvListPaymentMethod.Columns("DESCRIPTION").HeaderText = "Description"
        End If

    End Sub

    Private Sub txtPaymentMethod_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPaymentMethod.TextChanged
        Try
            PAYMENT_METHODBindingSource.Filter = "PAYMENT_METHOD_NAME LIKE '%" & txtPaymentMethod.Text.ToUpper & "%'"
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtPaymentMethod.Text = ""
            txtPaymentMethod.Focus()
        End Try
    End Sub

    Private Sub dgvListPaymentMethod_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvListPaymentMethod.DoubleClick

        frmEntryPaymentMethod.MdiParent = frmMain
        frmEntryPaymentMethod.Show()
        frmEntryPaymentMethod.BringToFront()
        tmpRecentForm = New frmEntryPaymentMethod
        frmEntryPaymentMethod.PAYMENT_METHODBindingSource.Position = frmEntryPaymentMethod.PAYMENT_METHODBindingSource.Find("PAYMENT_METHOD_ID", PAYMENT_METHODBindingSource.Current("PAYMENT_METHOD_ID"))
        Me.Close()
    End Sub

    Private Sub dgvListPaymentMethod_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvListPaymentMethod.KeyDown
        If e.KeyCode = Keys.Enter Then
            frmEntryPaymentMethod.MdiParent = frmMain
            frmEntryPaymentMethod.Show()
            frmEntryPaymentMethod.BringToFront()
            tmpRecentForm = New frmEntryPaymentMethod
            frmEntryPaymentMethod.PAYMENT_METHODBindingSource.Position = frmEntryPaymentMethod.PAYMENT_METHODBindingSource.Find("PAYMENT_METHOD_ID", PAYMENT_METHODBindingSource.Current("PAYMENT_METHOD_ID"))
            Me.Close()
        ElseIf e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub chkDeleted_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDeleted.CheckedChanged
        If chkDeleted.Checked = True Then
            PAYMENT_METHODBindingSource.Filter = "EFFECTIVE_END_DATE > '" & Now & "'"
        Else
            PAYMENT_METHODBindingSource.Filter = ""
        End If

    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        'Dim objf As frmRepPaymentMethod

        frmRepPaymentMethod.WindowState = FormWindowState.Maximized
        frmRepPaymentMethod.Show()
    End Sub

End Class