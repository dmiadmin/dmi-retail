﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSearchPurchaseReturn
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSearchPurchaseReturn))
        Me.lblVendor = New System.Windows.Forms.Label()
        Me.lblReceipt = New System.Windows.Forms.Label()
        Me.txtVendorName = New System.Windows.Forms.TextBox()
        Me.txtReceiptNo = New System.Windows.Forms.TextBox()
        Me.DS_RETURN_PURCHASE = New DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASE()
        Me.SP_SEARCH_PURCHASE_RETURNBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_SEARCH_PURCHASE_RETURNTableAdapter = New DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASETableAdapters.SP_SEARCH_PURCHASE_RETURNTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASETableAdapters.TableAdapterManager()
        Me.dgvSearcReturnPurchase = New System.Windows.Forms.DataGridView()
        Me.VENDOR_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VENDOR_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PURCHASE_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RECEIPT_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PURCHASE_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PAYMENT_METHOD_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GRAND_TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        CType(Me.DS_RETURN_PURCHASE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_SEARCH_PURCHASE_RETURNBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSearcReturnPurchase, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblVendor
        '
        Me.lblVendor.Location = New System.Drawing.Point(6, 24)
        Me.lblVendor.Name = "lblVendor"
        Me.lblVendor.Size = New System.Drawing.Size(113, 19)
        Me.lblVendor.TabIndex = 0
        Me.lblVendor.Text = "Vendor Name"
        Me.lblVendor.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblReceipt
        '
        Me.lblReceipt.Location = New System.Drawing.Point(9, 61)
        Me.lblReceipt.Name = "lblReceipt"
        Me.lblReceipt.Size = New System.Drawing.Size(110, 19)
        Me.lblReceipt.TabIndex = 1
        Me.lblReceipt.Text = "Receipt No"
        Me.lblReceipt.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtVendorName
        '
        Me.txtVendorName.Location = New System.Drawing.Point(125, 21)
        Me.txtVendorName.Name = "txtVendorName"
        Me.txtVendorName.Size = New System.Drawing.Size(140, 22)
        Me.txtVendorName.TabIndex = 2
        '
        'txtReceiptNo
        '
        Me.txtReceiptNo.Location = New System.Drawing.Point(125, 58)
        Me.txtReceiptNo.Name = "txtReceiptNo"
        Me.txtReceiptNo.Size = New System.Drawing.Size(140, 22)
        Me.txtReceiptNo.TabIndex = 3
        '
        'DS_RETURN_PURCHASE
        '
        Me.DS_RETURN_PURCHASE.DataSetName = "DS_RETURN_PURCHASE"
        Me.DS_RETURN_PURCHASE.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SP_SEARCH_PURCHASE_RETURNBindingSource
        '
        Me.SP_SEARCH_PURCHASE_RETURNBindingSource.DataMember = "SP_SEARCH_PURCHASE_RETURN"
        Me.SP_SEARCH_PURCHASE_RETURNBindingSource.DataSource = Me.DS_RETURN_PURCHASE
        '
        'SP_SEARCH_PURCHASE_RETURNTableAdapter
        '
        Me.SP_SEARCH_PURCHASE_RETURNTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.PURCHASE_RETURN_DETAILTableAdapter = Nothing
        Me.TableAdapterManager.PURCHASE_RETURNTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASETableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'dgvSearcReturnPurchase
        '
        Me.dgvSearcReturnPurchase.AllowUserToAddRows = False
        Me.dgvSearcReturnPurchase.AllowUserToDeleteRows = False
        Me.dgvSearcReturnPurchase.AutoGenerateColumns = False
        Me.dgvSearcReturnPurchase.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSearcReturnPurchase.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSearcReturnPurchase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSearcReturnPurchase.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.VENDOR_ID, Me.VENDOR_NAME, Me.PURCHASE_ID, Me.RECEIPT_NO, Me.PURCHASE_DATE, Me.PAYMENT_METHOD_NAME, Me.GRAND_TOTAL, Me.TOTAL})
        Me.dgvSearcReturnPurchase.DataSource = Me.SP_SEARCH_PURCHASE_RETURNBindingSource
        Me.dgvSearcReturnPurchase.Location = New System.Drawing.Point(16, 19)
        Me.dgvSearcReturnPurchase.Name = "dgvSearcReturnPurchase"
        Me.dgvSearcReturnPurchase.ReadOnly = True
        Me.dgvSearcReturnPurchase.RowHeadersWidth = 32
        Me.dgvSearcReturnPurchase.Size = New System.Drawing.Size(652, 175)
        Me.dgvSearcReturnPurchase.TabIndex = 5
        '
        'VENDOR_ID
        '
        Me.VENDOR_ID.DataPropertyName = "VENDOR_ID"
        Me.VENDOR_ID.HeaderText = "VENDOR_ID"
        Me.VENDOR_ID.Name = "VENDOR_ID"
        Me.VENDOR_ID.ReadOnly = True
        Me.VENDOR_ID.Visible = False
        '
        'VENDOR_NAME
        '
        Me.VENDOR_NAME.DataPropertyName = "VENDOR_NAME"
        Me.VENDOR_NAME.HeaderText = "Vendor Name"
        Me.VENDOR_NAME.Name = "VENDOR_NAME"
        Me.VENDOR_NAME.ReadOnly = True
        '
        'PURCHASE_ID
        '
        Me.PURCHASE_ID.DataPropertyName = "PURCHASE_ID"
        Me.PURCHASE_ID.HeaderText = "PURCHASE_ID"
        Me.PURCHASE_ID.Name = "PURCHASE_ID"
        Me.PURCHASE_ID.ReadOnly = True
        Me.PURCHASE_ID.Visible = False
        '
        'RECEIPT_NO
        '
        Me.RECEIPT_NO.DataPropertyName = "RECEIPT_NO"
        Me.RECEIPT_NO.HeaderText = "Receipt No"
        Me.RECEIPT_NO.Name = "RECEIPT_NO"
        Me.RECEIPT_NO.ReadOnly = True
        '
        'PURCHASE_DATE
        '
        Me.PURCHASE_DATE.DataPropertyName = "PURCHASE_DATE"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "dd-MMM-yyyy"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.PURCHASE_DATE.DefaultCellStyle = DataGridViewCellStyle2
        Me.PURCHASE_DATE.HeaderText = "Purchase Date"
        Me.PURCHASE_DATE.Name = "PURCHASE_DATE"
        Me.PURCHASE_DATE.ReadOnly = True
        '
        'PAYMENT_METHOD_NAME
        '
        Me.PAYMENT_METHOD_NAME.DataPropertyName = "PAYMENT_METHOD_NAME"
        Me.PAYMENT_METHOD_NAME.HeaderText = "Payment Method"
        Me.PAYMENT_METHOD_NAME.Name = "PAYMENT_METHOD_NAME"
        Me.PAYMENT_METHOD_NAME.ReadOnly = True
        '
        'GRAND_TOTAL
        '
        Me.GRAND_TOTAL.DataPropertyName = "GRAND_TOTAL"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N0"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.GRAND_TOTAL.DefaultCellStyle = DataGridViewCellStyle3
        Me.GRAND_TOTAL.HeaderText = "Grand Total"
        Me.GRAND_TOTAL.Name = "GRAND_TOTAL"
        Me.GRAND_TOTAL.ReadOnly = True
        '
        'TOTAL
        '
        Me.TOTAL.DataPropertyName = "TOTAL"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.TOTAL.DefaultCellStyle = DataGridViewCellStyle4
        Me.TOTAL.HeaderText = "Total"
        Me.TOTAL.Name = "TOTAL"
        Me.TOTAL.ReadOnly = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvSearcReturnPurchase)
        Me.GroupBox1.Location = New System.Drawing.Point(19, 105)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(684, 212)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtReceiptNo)
        Me.GroupBox2.Controls.Add(Me.txtVendorName)
        Me.GroupBox2.Controls.Add(Me.lblReceipt)
        Me.GroupBox2.Controls.Add(Me.lblVendor)
        Me.GroupBox2.Location = New System.Drawing.Point(19, 5)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(684, 94)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.TabStop = False
        '
        'frmSearchPurchaseReturn
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(720, 332)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimizeBox = False
        Me.Name = "frmSearchPurchaseReturn"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Search Purchase Return"
        CType(Me.DS_RETURN_PURCHASE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_SEARCH_PURCHASE_RETURNBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSearcReturnPurchase, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblVendor As System.Windows.Forms.Label
    Friend WithEvents lblReceipt As System.Windows.Forms.Label
    Friend WithEvents txtVendorName As System.Windows.Forms.TextBox
    Friend WithEvents txtReceiptNo As System.Windows.Forms.TextBox
    Friend WithEvents DS_RETURN_PURCHASE As DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASE
    Friend WithEvents SP_SEARCH_PURCHASE_RETURNBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_SEARCH_PURCHASE_RETURNTableAdapter As DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASETableAdapters.SP_SEARCH_PURCHASE_RETURNTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_PURCHASE.DS_RETURN_PURCHASETableAdapters.TableAdapterManager
    Friend WithEvents dgvSearcReturnPurchase As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents VENDOR_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VENDOR_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PURCHASE_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RECEIPT_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PURCHASE_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PAYMENT_METHOD_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GRAND_TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTAL As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
