﻿Public Class frmListSafeStock

    Dim tmpProductId, tmpCategory As Integer

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_PRODUCT_STOCK.SP_NOTIFICATION_PRODUCT' table. You can move, or remove it, as needed.
        ' Me.SP_NOTIFICATION_PRODUCTTableAdapter.Fill(Me.DS_PRODUCT_STOCK.SP_NOTIFICATION_PRODUCT)
        If Language = "Indonesian" Then
            Me.Text = "Pemberitahuan Stock Aman"
            Label1.Text = "Produk"
            cmdGenerate.Text = "PROSES"
            SP_NOTIFICATION_PRODUCTDataGridView.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            SP_NOTIFICATION_PRODUCTDataGridView.Columns("QUANTITY").HeaderText = "Jumlah"
            SP_NOTIFICATION_PRODUCTDataGridView.Columns("SAFE_STOCK").HeaderText = "Stok Aman"
            SP_NOTIFICATION_PRODUCTDataGridView.Columns("VENDOR_NAME").HeaderText = "Nama Supplier"
            SP_NOTIFICATION_PRODUCTDataGridView.Columns("VENDOR_CANTACT_PERSON").HeaderText = "Kontak Person Supplier"
            SP_NOTIFICATION_PRODUCTDataGridView.Columns("VENDOR_PHONE").HeaderText = "Telpon Supplier"

        End If
        PRODUCT_CODEComboBox.Text = ""
    End Sub

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        If Not ValidateAllComboBox(Me) Then
            If Language = "Indonesian" Then
                MsgBox("Nilai yang anda masukan tidak terdapat dalam daftar.", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You input value which is not in the list.", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If
        Cursor = Cursors.WaitCursor
        If Not ValidateComboBox(PRODUCT_CODEComboBox) Then
            PRODUCT_CODEComboBox.Text = 0
        Else
            tmpProductId = IIf(PRODUCT_CODEComboBox.Text.Length = 0, 0, SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID"))
        End If

        'SP_NOTIFICATION_PRODUCTTableAdapter.Fill(DS_PRODUCT_STOCK.SP_NOTIFICATION_PRODUCT, PRODUCT_CODEComboBox.Text)
        Cursor = Cursors.Default
        
    End Sub

    Private Sub cmdSearchName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchName.Click
        mdlGeneral.tmpSearchMode = "PRODUCT LIST - Product Name"
        Dim frmSearchProduct As New DMI_RETAIL_MASTER.frmSearchProduct
        frmSearchProduct.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        SP_LIST_PRODUCT_GOODSBindingSource.Position = SP_LIST_PRODUCT_GOODSBindingSource.Find("PRODUCT_ID", tmpSearchResult)
        PRODUCT_CODEComboBox.Text = SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_CODE")
    End Sub

    Private Sub PRODUCT_CODEComboBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles PRODUCT_CODEComboBox.LostFocus
        If PRODUCT_CODEComboBox.Text = "" Then Exit Sub
        Try
            'SP_LIST_PRODUCT_GOODSTableAdapter.SP_GET_CATEGORY_PRODUCT(PRODUCT_CODEComboBox.Text, tmpCategory)
        Catch
        End Try
        If tmpCategory <> 1 Then
            If Language = "Indonesian" Then
                MsgBox("Produk yang anda masukan bukan barang !" & vbCrLf & _
                   "Gunakan Form Pencarian untuk menemukan produk yang benar !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You input Product which is not Goods !" & vbCrLf & _
                   "Use the Search Form to find the right Product !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            PRODUCT_NAMETextBox.Text = ""
            PRODUCT_CODEComboBox.Text = ""
            PRODUCT_CODEComboBox.Focus()

        End If
    End Sub

End Class