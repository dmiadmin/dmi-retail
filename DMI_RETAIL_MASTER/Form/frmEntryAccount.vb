﻿Imports System.Data.SqlClient

Public Class frmEntryAccount
    Dim tmpSaveMode As String
    Dim tmpKey, tmpNoAcc, tmpNameAcc As String
    Dim tmpUsedAccount As Integer
    Dim tmpChange As Boolean
    Dim tmpBindPosition As Integer
    Public ACCOUNTBindingSource As New BindingSource

    Private Sub GetData()
        sql = "SELECT ACCOUNT_ID, ACCOUNT_NUMBER, ACCOUNT_NAME, ACCOUNT_TYPE, " & _
              "EFFECTIVE_START_DATE, EFFECTIVE_END_DATE, USER_ID_INPUT, INPUT_DATE, " & _
              "USER_ID_UPDATE, UPDATE_DATE, DESCRIPTION FROM ACCOUNT"
        dt = New DataTable
        sqladapter = New SqlDataAdapter(sql, xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        ACCOUNTBindingSource.DataSource = dt
        ACCOUNTBindingNavigator.BindingSource = ACCOUNTBindingSource

    End Sub

    Private Sub frmEntryAccount_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If tmpChange = True And DESCRIPTIONTextBox.Enabled = True Then
            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah Anda ingin menyimpan perubahan " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If
            If tmpvar = 6 Then
                cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub frmEntryAccount_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_ACCOUNT.ACCOUNT' table. You can move, or remove it, as needed.
        'Me.ACCOUNTTableAdapter.Fill(Me.DS_ACCOUNT.ACCOUNT)

        accFormName = Me.Text
        GetData()
        ACCOUNT_NUMBERTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", ACCOUNTBindingSource, "ACCOUNT_NUMBER", True))
        ACCOUNT_NAMETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", ACCOUNTBindingSource, "ACCOUNT_NAME", True))
        DESCRIPTIONTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", ACCOUNTBindingSource, "DESCRIPTION", True))
        ACCOUNT_TYPEComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", ACCOUNTBindingSource, "ACCOUNT_TYPE", True))
        DisableInputBox(Me)

        cmdSearch.Visible = True
        ACCOUNTBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        chkActive.Enabled = False

        If ACCOUNT_NUMBERTextBox.Text = "" Then
            cmdEdit.Enabled = False
            cmdDelete.Enabled = False
        Else
            cmdEdit.Enabled = True
            cmdDelete.Enabled = True
        End If
        tmpChange = False

        If ACCOUNT_NAMETextBox.Text <> "" And ACCOUNT_NAMETextBox.Enabled = False Then
            If ACCOUNTBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
                cmdDelete.Enabled = True
                cmdEdit.Enabled = True
            End If
        End If

        If Language = "Indonesian" Then
            cmdAdd.Text = "Tambah"
            cmdDelete.Text = "Hapus"
            cmdEdit.Text = "Ubah"
            cmdSave.Text = "Simpan"
            cmdUndo.Text = "Batal"
            lblAccountNo.Text = " Nomor Akun"
            lblAccouuntNm.Text = "Nama Akun"
            lblDescription.Text = "Keterangan"
            lblAcountType.Text = "Tipe Akun"
            Me.Text = "Input Akun"
        End If

    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If Trim(RsAccessPrivilege.Item("ALLOW_ADD").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Insert"

        EnableInputBox(Me)

        ACCOUNTBindingSource.AddNew()

        chkActive.Enabled = True
        chkActive.Checked = True
        cmdSearch.Visible = False
        ACCOUNTBindingNavigator.Enabled = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        tmpChange = False

        tmpBindPosition = ACCOUNTBindingSource.Position
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        If Trim(RsAccessPrivilege.Item("ALLOW_EDIT").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Update"

        EnableInputBox(Me)

        ACCOUNT_NUMBERTextBox.Enabled = False

        cmdSearch.Visible = False
        ACCOUNTBindingNavigator.Enabled = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        tmpChange = False
        chkActive.Enabled = True

        tmpBindPosition = ACCOUNTBindingSource.Position
        tmpNoAcc = ACCOUNT_NUMBERTextBox.Text
        tmpNameAcc = ACCOUNT_NAMETextBox.Text
        If ACCOUNTBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
            chkActive.Checked = False
        Else
            chkActive.Checked = True
        End If

    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        tmpSaveMode = ""

        DisableInputBox(Me)
        ACCOUNTBindingSource.CancelEdit()

        cmdSearch.Visible = True
        ACCOUNTBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        chkActive.Enabled = False

        If ACCOUNT_NUMBERTextBox.Text = "" Then
            cmdEdit.Enabled = False
            cmdDelete.Enabled = False
        Else
            cmdEdit.Enabled = True
            cmdDelete.Enabled = True
        End If
        tmpChange = False

        ACCOUNTBindingSource.Position = tmpBindPosition
        ACCOUNTBindingSource_CurrentChanged(Nothing, Nothing)
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If Trim(RsAccessPrivilege.Item("ALLOW_DELETE").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If ACCOUNTBindingSource.Count < 1 Then Exit Sub

        If Language = "Indonesian" Then
            If MsgBox("Hapus Account ?" & vbCrLf & ACCOUNT_NAMETextBox.Text, _
              MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
            xComm = New SqlCommand("SP_CHECK_ACCOUNT_USED", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@ACCOUNT_NUMBER", ACCOUNT_NUMBERTextBox.Text)
            xdreader = xComm.ExecuteReader
            xdreader.Read()
            tmpUsedAccount = xdreader.Item(0)
            'ACCOUNTTableAdapter.SP_CHECK_ACCOUNT_USED(ACCOUNT_NUMBERTextBox.Text, tmpUsedAccount)
            If tmpUsedAccount > 0 Then
                MsgBox("Akun ini telah digunakan di transaksi yang lain" & vbCrLf & "Anda tidak dapat menghapus data ini !", _
                       MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If
        Else
            If MsgBox("Delete Account ?" & vbCrLf & ACCOUNT_NAMETextBox.Text, _
              MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
            xComm = New SqlCommand("SP_CHECK_ACCOUNT_USED", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@ACCOUNT_NUMBER", ACCOUNT_NUMBERTextBox.Text)
            xdreader = xComm.ExecuteReader
            xdreader.Read()
            tmpUsedAccount = xdreader.Item(0)
            'ACCOUNTTableAdapter.SP_CHECK_ACCOUNT_USED(ACCOUNT_NUMBERTextBox.Text, tmpUsedAccount)
            If tmpUsedAccount > 0 Then
                MsgBox("This Account is used by another transaction !" & vbCrLf & "You can not delete this data !", _
                        MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If
        End If
        xComm = New SqlCommand("SP_ACCOUNT", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@METHOD", "D")
        xComm.Parameters.AddWithValue("@ACCOUNT_ID", ACCOUNTBindingSource.Current("ACCOUNT_ID"))
        xComm.Parameters.AddWithValue("@ACCOUNT_NUMBER", ACCOUNT_NUMBERTextBox.Text)
        xComm.Parameters.AddWithValue("@ACCOUNT_NAME", ACCOUNTBindingSource.Current("ACCOUNT_NAME"))
        xComm.Parameters.AddWithValue("@ACCOUNT_TYPE", ACCOUNTBindingSource.Current("ACCOUNT_TYPE"))
        xComm.Parameters.AddWithValue("@DESCRIPTION", ACCOUNTBindingSource.Current("DESCRIPTION"))
        xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", ACCOUNTBindingSource.Current("EFFECTIVE_START_DATE"))
        xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", Now)
        xComm.Parameters.AddWithValue("@USER_ID_INPUT", ACCOUNTBindingSource.Current("USER_ID_INPUT"))
        xComm.Parameters.AddWithValue("@INPUT_DATE", ACCOUNTBindingSource.Current("INPUT_DATE"))
        xComm.Parameters.AddWithValue("@USER_ID_UPDATE", ACCOUNTBindingSource.Current("USER_ID_UPDATE"))
        xComm.Parameters.AddWithValue("@UPDATE_DATE", ACCOUNTBindingSource.Current("UPDATE_DATE"))
        xComm.ExecuteNonQuery()
        'ACCOUNTTableAdapter.SP_ACCOUNT("D", _
        '                               ACCOUNTBindingSource.Current("ACCOUNT_ID"), _
        '                               ACCOUNT_NUMBERTextBox.Text, _
        '                               ACCOUNTBindingSource.Current("ACCOUNT_NAME"), _
        '                               ACCOUNTBindingSource.Current("ACCOUNT_TYPE"), _
        '                               ACCOUNTBindingSource.Current("DESCRIPTION"), _
        '                               ACCOUNTBindingSource.Current("EFFECTIVE_START_DATE"), _
        '                               Now, _
        '                               ACCOUNTBindingSource.Current("USER_ID_INPUT"), _
        '                               ACCOUNTBindingSource.Current("INPUT_DATE"), _
        '                               ACCOUNTBindingSource.Current("USER_ID_UPDATE"), _
        '                               ACCOUNTBindingSource.Current("UPDATE_DATE"))

        ACCOUNTBindingSource.RemoveCurrent()

        If Language = "Indonesian" Then
            MsgBox("Data berhasil dihapus !", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully deleted !", MsgBoxStyle.Information, "DMI Retail")
        End If


        cmdSearch.Visible = True
        ACCOUNTBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        chkActive.Enabled = False
        getdata()
        'Me.ACCOUNTTableAdapter.Fill(Me.DS_ACCOUNT.ACCOUNT)
        ACCOUNTBindingSource.Position = ACCOUNTBindingSource.Find("ACCOUNT_NUMBER", tmpKey)
        Try
            If ACCOUNTBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                cmdDelete.Enabled = True
                cmdEdit.Enabled = True
            End If
        Catch
        End Try

        If ACCOUNT_NUMBERTextBox.Text = "" Then
            cmdEdit.Enabled = False
        Else
            cmdEdit.Enabled = True
            cmdDelete.Enabled = True
        End If

        'ACCOUNTTableAdapter.Fill(Me.DS_ACCOUNT.ACCOUNT)
        tmpChange = False

        ACCOUNTBindingSource.Position = tmpBindPosition
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        If ACCOUNT_NUMBERTextBox.Text = "" Or ACCOUNT_NAMETextBox.Text = "" Or ACCOUNT_TYPEComboBox.SelectedItem = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please fill all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        Dim tmpAccNo, tmpAccName As Integer
        xComm = New SqlCommand("SP_CHECK_ACCOUNT_NUMBER", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@ACCOUNT_NUMBER", ACCOUNT_NUMBERTextBox.Text)
        xdreader = xComm.ExecuteReader
        xdreader.Read()
        tmpAccNo = xdreader.Item(0)

        xComm = New SqlCommand("SP_CHECK_ACCOUNT_NAME", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@ACCOUNT_NAME", ACCOUNT_NAMETextBox.Text)
        xdreader = xComm.ExecuteReader
        xdreader.Read()
        tmpAccName = xdreader.Item(0)
        'ACCOUNTTableAdapter.SP_CHECK_ACCOUNT_NUMBER(ACCOUNT_NUMBERTextBox.Text, tmpAccNo)
        'ACCOUNTTableAdapter.SP_CHECK_ACCOUNT_NAME(ACCOUNT_NAMETextBox.Text, tmpAccName)

        If tmpSaveMode = "Insert" Then
            If tmpAccNo > 0 Or tmpAccName > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Nomor Account atau Nama Account sudah ada di database !" & vbCrLf & _
                           "Tolong isi Nomor Account atau Nama Account yang berbeda !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Either Account Number or Account Name already exists in Database !" & vbCrLf & _
                           "Please Fill Different Account Name or Account Number !", MsgBoxStyle.Critical, "DMI Retail")
                End If
                ACCOUNT_NUMBERTextBox.Focus()
                Exit Sub
            End If

            tmpKey = ACCOUNT_NUMBERTextBox.Text
            xComm = New SqlCommand("SP_ACCOUNT", xConn)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "I")
            xComm.Parameters.AddWithValue("@ACCOUNT_ID", 0)
            xComm.Parameters.AddWithValue("@ACCOUNT_NUMBER", ACCOUNT_NUMBERTextBox.Text)
            xComm.Parameters.AddWithValue("@ACCOUNT_NAME", ACCOUNT_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@ACCOUNT_TYPE", ACCOUNT_TYPEComboBox.SelectedItem)
            xComm.Parameters.AddWithValue("@DESCRIPTION", DESCRIPTIONTextBox.Text)
            xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", Now)
            xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", DateSerial(4000, 12, 31))
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", mdlGeneral.UserId)
            xComm.Parameters.AddWithValue("@INPUT_DATE", Now)
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", 0)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", DateSerial(4000, 12, 31))
            xComm.ExecuteNonQuery()
            'ACCOUNTTableAdapter.SP_ACCOUNT("I", _
            '                               0, _
            '                               ACCOUNT_NUMBERTextBox.Text, _
            '                               ACCOUNT_NAMETextBox.Text, _
            '                               ACCOUNT_TYPEComboBox.SelectedItem, _
            '                               DESCRIPTIONTextBox.Text, _
            '                               DateSerial(Today.Year, Today.Month, Today.Day), _
            '                               DateSerial(4000, 12, 31), _
            '                               mdlGeneral.USER_ID, _
            '                               Now, _
            '                               0, _
            '                               DateSerial(4000, 12, 31))

        ElseIf tmpSaveMode = "Update" Then
            If ACCOUNT_NAMETextBox.Text <> tmpNameAcc Or ACCOUNT_NUMBERTextBox.Text <> tmpNoAcc Then
                If tmpAccNo > 0 Or tmpAccName > 0 Then
                    If Language = "Indonesian" Then
                        MsgBox("Nomor Account atau Nama Account sudah ada di database !" & vbCrLf & _
                               "Tolong isi Nomor Account atau Nama Account yang berbeda !", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("Either Account Number or Account Name already exists in Database !" & vbCrLf & _
                               "Please Fill Different Account Name or Account Number !", MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    ACCOUNT_NUMBERTextBox.Focus()
                    Exit Sub
                End If
            End If

            ''''validate conditions for active acount''''
            Dim tmpEndDate As DateTime
            If chkActive.Checked = True Then
                tmpEndDate = DateSerial(4000, 12, 31)
            Else
                tmpEndDate = Now
            End If

            tmpKey = ACCOUNT_NUMBERTextBox.Text

            xComm = New SqlCommand("SP_ACCOUNT", xConn)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "U")
            xComm.Parameters.AddWithValue("@ACCOUNT_ID", ACCOUNTBindingSource.Current("ACCOUNT_ID"))
            xComm.Parameters.AddWithValue("@ACCOUNT_NUMBER", ACCOUNTBindingSource.Current("ACCOUNT_NUMBER"))
            xComm.Parameters.AddWithValue("@ACCOUNT_NAME", ACCOUNT_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@ACCOUNT_TYPE", ACCOUNT_TYPEComboBox.SelectedItem)
            xComm.Parameters.AddWithValue("@DESCRIPTION", DESCRIPTIONTextBox.Text)
            xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", ACCOUNTBindingSource.Current("EFFECTIVE_START_DATE"))
            xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", tmpEndDate)
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", ACCOUNTBindingSource.Current("USER_ID_INPUT"))
            xComm.Parameters.AddWithValue("@INPUT_DATE", ACCOUNTBindingSource.Current("INPUT_DATE"))
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", UserId)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", Now)
            xComm.ExecuteNonQuery()
            'ACCOUNTTableAdapter.SP_ACCOUNT("U", _
            '                               ACCOUNTBindingSource.Current("ACCOUNT_ID"), _
            '                               ACCOUNTBindingSource.Current("ACCOUNT_NUMBER"), _
            '                               ACCOUNT_NAMETextBox.Text, _
            '                               ACCOUNT_TYPEComboBox.SelectedItem, _
            '                               DESCRIPTIONTextBox.Text, _
            '                               ACCOUNTBindingSource.Current("EFFECTIVE_START_DATE"), _
            '                               tmpEndDate, _
            '                               ACCOUNTBindingSource.Current("USER_ID_INPUT"), _
            '                               ACCOUNTBindingSource.Current("INPUT_DATE"), _
            '                               mdlGeneral.USER_ID, _
            '                               Now)
        End If
        getdata()
        'ACCOUNTTableAdapter.Fill(Me.DS_ACCOUNT.ACCOUNT)

        tmpSaveMode = ""

        DisableInputBox(Me)
        ACCOUNTBindingSource.CancelEdit()

        cmdSearch.Visible = True
        ACCOUNTBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        chkActive.Enabled = False

        If ACCOUNT_NUMBERTextBox.Text = "" Then
            cmdEdit.Enabled = False
            cmdDelete.Enabled = False
        Else
            cmdEdit.Enabled = True
            cmdDelete.Enabled = True
        End If

        ACCOUNTBindingSource.Position = ACCOUNTBindingSource.Find("ACCOUNT_NUMBER", tmpKey)
        If Language = "Indonesian" Then
            MsgBox("Data berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
        End If

        tmpChange = False

        ACCOUNTBindingSource_CurrentChanged(Nothing, Nothing)
        ACCOUNTBindingSource.Position = tmpBindPosition
    End Sub

    Private Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        mdlGeneral.tmpSearchMode = "Account-AccountName"
        frmSearchAccount.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        ACCOUNTBindingSource.Position = ACCOUNTBindingSource.Find("ACCOUNT_ID", tmpSearchResult)

        tmpChange = False

        'ACCOUNT_NUMBERTextBox.Text = ACCOUNTBindingSource.Current("ACCOUNT_NUMBER")
    End Sub

    Private Sub ACCOUNT_NAMETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ACCOUNT_NAMETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub ACCOUNT_NUMBERTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ACCOUNT_NUMBERTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub DESCRIPTIONTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DESCRIPTIONTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub ACCOUNT_TYPEComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ACCOUNT_TYPEComboBox.SelectedIndexChanged
        tmpChange = True
    End Sub


    Private Sub ACCOUNTBindingSource_CurrentChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ACCOUNT_NAMETextBox.Text <> "" And ACCOUNT_NAMETextBox.Enabled = False Then
            If ACCOUNTBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
                cmdDelete.Enabled = True
                cmdEdit.Enabled = True
            End If
        End If
    End Sub

    Private Sub BindingNavigatorPositionItem_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BindingNavigatorPositionItem.TextChanged
        Try
            If ACCOUNT_NAMETextBox.Text <> "" And ACCOUNT_NAMETextBox.Enabled = False Then
                If ACCOUNTBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                    chkActive.Checked = False
                Else
                    chkActive.Checked = True
                    cmdDelete.Enabled = True
                    cmdEdit.Enabled = True
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        AccessPrivilege(Me.Tag)
    End Sub
End Class