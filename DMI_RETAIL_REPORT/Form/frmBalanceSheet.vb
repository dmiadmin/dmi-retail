﻿Imports System.Data.SqlClient
Imports System.Data
Public Class frmBalanceSheet

    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub


    Private Sub frmBalanceSheet_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F12 Then
            cmdReport_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub frmBalanceSheet_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        tmpQuantity = 0
        dtpPeriod.Value = DateSerial(Date.Now.Year, Date.Now.Month, "01")
        For n As Integer = 0 To dgvENDING_STOCKDataGridView.RowCount - 1
            tmpQuantity = tmpQuantity + dgvENDING_STOCKDataGridView.Item("QUANTITY", n).Value
            tmpQuantity = tmpQuantity
        Next

        If Language = "Indonesian" Then
            Me.Text = "Saldo Stok"
            lblPeriod.Text = "Periode"
            lblProductName.Text = "Nama Produk"
            cmdGenerate.Text = "Proses"
            cmdReport.Text = "Cetak"

            'dgvENDING_STOCKDataGridView.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            'dgvENDING_STOCKDataGridView.Columns("TRANSACTION_DATE").HeaderText = "Tgl Transaksi"
            'dgvENDING_STOCKDataGridView.Columns("QUANTITY").HeaderText = "Jumlah"
            'dgvENDING_STOCKDataGridView.Columns("PRICE_PER_UNIT").HeaderText = "Harga Satuan"

            lblNoOfProduct.Text = "Jumlah Saldo Produk : " & dgvENDING_STOCKDataGridView.RowCount
            lblQty.Text = "Jumlah Total : " & tmpQuantity
        Else
            lblNoOfProduct.Text = "Total Product(s) Balance : " & dgvENDING_STOCKDataGridView.RowCount
            lblQty.Text = "Total Quantity : " & tmpQuantity
        End If

        txtProductName.Enabled = False
    End Sub

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        Dim tmpnow, tmpDate As Date

        tmpnow = DateSerial(dtpPeriod.Value.Year, dtpPeriod.Value.Month, 1)

        tmpDate = DateSerial(tmpnow.Year, _
                             tmpnow.Month + 1, _
                             tmpnow.Day - 1)


        'SP_LIST_ENDING_STOCKTableAdapter.Fill(Me.DS_LIST_ENDING_STOCK.SP_LIST_ENDING_STOCK, tmpDate, txtProductName.Text)
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC	    @return_value = [dbo].[SP_LIST_ENDING_STOCK]" & vbCrLf & _
              "         @PERIOD = N'" & dtpPeriod.Value & "'," & vbCrLf & _
              "    		@PRODUCT_NAME = N'" & txtProductName.Text & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.dgvENDING_STOCKDataGridView.DataSource = dt

        tmpQuantity = 0
        For n As Integer = 0 To dgvENDING_STOCKDataGridView.RowCount - 1
            tmpQuantity = tmpQuantity + dgvENDING_STOCKDataGridView.Item("QUANTITY", n).Value
            tmpQuantity = tmpQuantity
        Next

        If Language = "Indonesian" Then
            lblNoOfProduct.Text = "Jumlah Saldo Produk : " & dgvENDING_STOCKDataGridView.RowCount
            lblQty.Text = "Jumlah Total : " & tmpQuantity
            dgvENDING_STOCKDataGridView.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            dgvENDING_STOCKDataGridView.Columns("TRANSACTION_DATE").HeaderText = "Tgl Transaksi"
            dgvENDING_STOCKDataGridView.Columns("QUANTITY").HeaderText = "Jumlah"
            dgvENDING_STOCKDataGridView.Columns("PRICE_PER_UNIT").HeaderText = "Harga Satuan"
            dgvENDING_STOCKDataGridView.Columns("PERIOD").Visible = False
            dgvENDING_STOCKDataGridView.Columns("TRANSACTION_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvENDING_STOCKDataGridView.Columns("QUANTITY").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvENDING_STOCKDataGridView.Columns("PRICE_PER_UNIT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvENDING_STOCKDataGridView.Columns("TRANSACTION_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvENDING_STOCKDataGridView.Columns("PRICE_PER_UNIT").DefaultCellStyle.Format = "n0"
            dgvENDING_STOCKDataGridView.Columns("PRODUCT_NAME").Width = 175
        Else
            dgvENDING_STOCKDataGridView.Columns("PRODUCT_NAME").HeaderText = "Product Name"
            dgvENDING_STOCKDataGridView.Columns("TRANSACTION_DATE").HeaderText = "Transaction Date"
            dgvENDING_STOCKDataGridView.Columns("QUANTITY").HeaderText = "Quantity"
            dgvENDING_STOCKDataGridView.Columns("PRICE_PER_UNIT").HeaderText = "Price Per Unit"
            dgvENDING_STOCKDataGridView.Columns("PERIOD").Visible = False
            dgvENDING_STOCKDataGridView.Columns("TRANSACTION_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvENDING_STOCKDataGridView.Columns("QUANTITY").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvENDING_STOCKDataGridView.Columns("PRICE_PER_UNIT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvENDING_STOCKDataGridView.Columns("TRANSACTION_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvENDING_STOCKDataGridView.Columns("PRICE_PER_UNIT").DefaultCellStyle.Format = "n0"
            dgvENDING_STOCKDataGridView.Columns("PRODUCT_NAME").Width = 175
            lblNoOfProduct.Text = "Total Product(s) Balance : " & dgvENDING_STOCKDataGridView.RowCount
            lblQty.Text = "Total Quantity : " & tmpQuantity
        End If

        txtProductName.Enabled = True
    End Sub

    Private Sub txtProductName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProductName.TextChanged
        Try
            'SP_LIST_ENDING_STOCKBindingSource.Filter = "PRODUCT_NAME LIKE '%" & txtProductName.Text & "%'"
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = "DECLARE	@return_value int" & vbCrLf & _
                  "EXEC	    @return_value = [dbo].[SP_LIST_ENDING_STOCK]" & vbCrLf & _
                  "         @PERIOD = N'" & dtpPeriod.Value & "'," & vbCrLf & _
                  "    		@PRODUCT_NAME = N'" & txtProductName.Text & "'" & vbCrLf & _
                  "SELECT	'Return Value' = @return_value"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.dgvENDING_STOCKDataGridView.DataSource = dt
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtProductName.Text = ""
            txtProductName.Focus()
        End Try
    End Sub


    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        Dim RPV As New frmRepBalanceSheet
        RPV.year = dtpPeriod.Value.Year
        RPV.month = dtpPeriod.Value.Month
        RPV.productname = txtProductName.Text
        RPV.ReportViewer1.ShowRefreshButton = False
        RPV.ReportViewer1.ZoomPercent = 100
        RPV.WindowState = FormWindowState.Maximized
        RPV.Show()
    End Sub


End Class