﻿Imports System.Data.SqlClient

Public Class frmSearchLocation
    Public LOCATIONBindingSource As New BindingSource

    Private Sub GetData()
        sql = "SELECT LOCATION_ID, LOCATION_NAME,DESCRIPTION FROM LOCATION"
        sqladapter = New SqlDataAdapter(sql, xConn)
        dt = New DataTable
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        LOCATIONBindingSource.DataSource = dt
        LOCATIONBindingNavigator.BindingSource = LOCATIONBindingSource
        dgvSearchLocation.DataSource = LOCATIONBindingSource
        dgvSearchLocation.Columns("LOCATION_ID").Visible = False
    End Sub

    Private Sub frmSearchLocation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_LOCATION.LOCATION' table. You can move, or remove it, as needed.
        'Me.LOCATIONTableAdapter.Fill(Me.DS_LOCATION.LOCATION)
        tmpSearchResult = ""
        txtLocation.Text = ""
        txtLocation.Focus()
        getdata()
        If Language = "Indonesian" Then
            Me.Text = "Pencarian Lokasi"
            lblLocation.Text = "Nama Lokasi"
            dgvSearchLocation.Columns("LOCATION_NAME").HeaderText = "Nama Lokasi"
            dgvSearchLocation.Columns("DESCRIPTION").HeaderText = "Keterangan"
        Else
            dgvSearchLocation.Columns("LOCATION_NAME").HeaderText = "Location Name"
            dgvSearchLocation.Columns("DESCRIPTION").HeaderText = "Description"
        End If
    End Sub

    Private Sub frmSearchLocation_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
        txtLocation.Focus()
    End Sub

    Private Sub txtLocation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLocation.TextChanged
        Try
            LOCATIONBindingSource.Filter = "LOCATION_NAME LIKE '%" & txtLocation.Text.ToUpper & "%'"
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtLocation.Text = ""
            txtLocation.Focus()
        End Try
    End Sub

    Private Sub dgvSearchLocation_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSearchLocation.DoubleClick
        Try
            tmpSearchResult = LOCATIONBindingSource.Current("LOCATION_ID")
        Catch
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data Lokasi !" & vbCrLf & _
                       "Silahkan input setidaknya satu data Lokasi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Warehouse data !" & vbCrLf & _
                       "Please input at least one Location data !", MsgBoxStyle.Critical, "DMI Retail")
            End If
        End Try
        Me.Close()
    End Sub

    Private Sub dgvSearchLocation_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvSearchLocation.KeyDown
        If e.KeyCode = Keys.Enter Then
            Try
                tmpSearchResult = LOCATIONBindingSource.Current("LOCATION_ID")
            Catch
                If Language = "Indonesian" Then
                    MsgBox("Tidak ada data Lokasi !" & vbCrLf & _
                           "Silahkan input setidaknya satu data Lokasi !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("There is no Warehouse data !" & vbCrLf & _
                           "Please input at least one Location data !", MsgBoxStyle.Critical, "DMI Retail")
                End If
            End Try
            Me.Close()
        ElseIf e.KeyCode = Keys.Escape Then
            tmpSearchResult = ""
            Me.Close()
        End If
    End Sub
End Class