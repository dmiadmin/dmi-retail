﻿Imports System.Data.SqlClient

Public Class frmEntryUser
    Dim tmpSaveMode As String
    Dim tmpKey As String
    Dim tmpChange As Boolean
    Dim tmpActive As String
    Dim tmpPosition As Integer
    Public tmpClickMode As String = ""
    Public USERBindingSource As New BindingSource
    Public AccessBindingSource As New BindingSource

    Private Sub frmEntryUser_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If tmpChange = True And USER_NAMETextBox.Enabled = True Then
            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah Anda ingin menyimpan perubahan " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If
            If tmpvar = 6 Then
                cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        End If
        tmpClickMode = ""
    End Sub

    Private Sub GetData()
        dt = New DataTable
        sqladapter = New SqlDataAdapter("SELECT USER_ID, USER_NAME, PASSWORD, NUMBER_OF_WRONG_PASSWORD, " & _
                                        "ACTIVE, LAST_LOGIN, EFFECTIVE_START_DATE, EFFECTIVE_END_DATE, " & _
                                        "USER_ID_INPUT, INPUT_DATE, USER_ID_UPDATE, UPDATE_DATE, " & _
                                        "PASSWORD_HINT, ACCESS_PRIVILEDGE, (SELECT ACCESS_PRIVILEDGE_NAME " & _
                                        "FROM ACCESS_PRIVILEDGE WHERE " & _
                                        "ACCESS_PRIVILEDGE_ID=[USER].ACCESS_PRIVILEDGE) ACCESS_PRIVILEDGE_NAME " & _
                                        "FROM [USER]", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        USERBindingSource.DataSource = dt
        USERBindingNavigator.BindingSource = USERBindingSource
    End Sub

    Private Sub GetAccess()
        dt = New DataTable
        sqladapter = New SqlDataAdapter("SELECT ACCESS_PRIVILEDGE_ID, ACCESS_PRIVILEDGE_NAME, " & _
                                        "EFFECTIVE_START_DATE, EFFECTIFE_END_DATE, USER_ID_INPUT, " & _
                                        "INPUT_DATE, USER_ID_UPDATE, UPDATE_DATE FROM ACCESS_PRIVILEDGE " & _
                                        "WHERE (EFFECTIFE_END_DATE > { fn NOW() }) " & _
                                        "ORDER BY ACCESS_PRIVILEDGE_NAME", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        AccessBindingSource.DataSource = dt
        ACCESS_PRIVILEDGE_NAMEComboBox.DataSource = AccessBindingSource
        ACCESS_PRIVILEDGE_NAMEComboBox.ValueMember = "ACCESS_PRIVILEDGE_ID"
        ACCESS_PRIVILEDGE_NAMEComboBox.DisplayMember = "ACCESS_PRIVILEDGE_NAME"
    End Sub

    Private Sub frmEntryUser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        AccessPrivilege(Me.Text)
        GetData()
        GetAccess()

        USER_NAMETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", USERBindingSource, "USER_NAME", True))
        PASSWORDTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", USERBindingSource, "PASSWORD", True))
        CONFIRM_PASSWORDTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", USERBindingSource, "PASSWORD", True))
        ACCESS_PRIVILEDGE_NAMEComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", USERBindingSource, "ACCESS_PRIVILEDGE_NAME", True))
        PASSWORD_HINTTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", USERBindingSource, "PASSWORD_HINT", True))

        If Language = "Indonesian" Then
            lblAcountType.Text = "Tipe Akun"
            lblUser.Text = "Nama User"
            lblPassword.Text = "Sandi"
            lblConfirmPass.Text = "Kofirmasi Sandi"
            lblPasswordHint.Text = "Petunjuk Sandi"
            Me.Text = "Input User"
            cmdAdd.Text = "Tambah"
            cmdEdit.Text = "Ubah"
            cmdUndo.Text = "Batal"
            cmdSave.Text = "Simpan"
            cmdDelete.Text = "Hapus"
        End If

        If USER_NAMETextBox.Text <> "" Then
            If USERBindingSource.Current("ACTIVE") = "YES" Then
                ACTIVECheckBox.Checked = True
            Else
                ACTIVECheckBox.Checked = False
            End If
            AccessBindingSource.Position = AccessBindingSource.Find("ACCESS_PRIVILEDGE_ID", USERBindingSource.Current("ACCESS_PRIVILEDGE"))

            If USERBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                lblStatus.Visible = True
                lblStatus.Text = "DELETED"
                cmdDelete.Enabled = False
                cmdEdit.Enabled = False
            Else
                cmdDelete.Enabled = True
                cmdEdit.Enabled = True
                lblStatus.Visible = False
            End If
        End If

        DisableInputBox(Me)
        ACTIVECheckBox.Enabled = False
        USERBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        tmpChange = False
        tmpClickMode = ""
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If AllowAdd = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Insert"
        tmpPosition = USERBindingSource.Position

        EnableInputBox(Me)
        USERBindingSource.AddNew()

        ACCESS_PRIVILEDGE_NAMEComboBox.Text = ""
        lblStatus.Text = ""
        USERBindingNavigator.Enabled = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        cmdSearchName.Visible = False

        tmpChange = False
        ACTIVECheckBox.Checked = True
        ACTIVECheckBox.Enabled = True
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        If AllowEdit = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If USER_NAMETextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak ada user untuk di Ubah !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no User to edit !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Update"
        tmpPosition = USERBindingSource.Position

        EnableInputBox(Me)

        USERBindingNavigator.Enabled = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        cmdSearchName.Visible = False

        tmpChange = False
        ACTIVECheckBox.Enabled = True
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        tmpSaveMode = ""

        DisableInputBox(Me)
        USERBindingSource.CancelEdit()

        USERBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        cmdSearchName.Visible = True

        tmpChange = False
        ACTIVECheckBox.Enabled = False
        USERBindingSource_CurrentChanged(Nothing, Nothing)
        USERBindingSource.Position = tmpPosition
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Me.Validate()

        If PASSWORDTextBox.Text.Length < 5 Then
            If Language = "Indonesian" Then
                MsgBox("Pastikan bahwa panjang sandi lebih dari 5 karakter !")
            Else
                MsgBox("Please ensure the password length more than 5 character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            PASSWORDTextBox.Focus()
            Exit Sub
        End If

        If USER_NAMETextBox.Text = "" Or PASSWORDTextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If PASSWORDTextBox.Text <> CONFIRM_PASSWORDTextBox.Text Then
            If Language = "Indonesian" Then
                MsgBox("Konfirmasi Password Salah." & vbCrLf & _
                   "Pastikan bahwa Sandi dan Konfirmas sama persis !", _
                   MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("The password was not correctly confirmed." & vbCrLf & _
                   "Please ensure that the password and confirmation match exactly!", _
                   MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If PASSWORDTextBox.Text.Length < mdlGeneral.MinNoOfPassword Then
            If Language = "Indonesian" Then
                MsgBox("Sandi paling sedikit " & MinNoOfPassword & ".", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Minimum digit of password is " & MinNoOfPassword & ".", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If Not ValidateAllComboBox(Me) Then
            If Language = "Indonesian" Then
                MsgBox("Nilai yang anda yang masukan tidak ada di daftar.", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You input value which is not in the list.", MsgBoxStyle.Critical, "DMI Retail")
            End If
            ACCESS_PRIVILEDGE_NAMEComboBox.Focus()
            Exit Sub
        End If

        tmpKey = USER_NAMETextBox.Text

        If ACTIVECheckBox.Checked = True Then
            tmpActive = "YES"
        ElseIf ACTIVECheckBox.Checked = False Then
            tmpActive = "NO"
        End If

        If tmpSaveMode = "Insert" Then
            xComm = New SqlCommand("SP_USER", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "I")
            xComm.Parameters.AddWithValue("@USER_ID", 0)
            xComm.Parameters.AddWithValue("@USER_NAME", USER_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@PASSWORD", PASSWORDTextBox.Text)
            xComm.Parameters.AddWithValue("@ACCESS_PRIVILEDGE", AccessBindingSource.Current("ACCESS_PRIVILEDGE_ID"))
            xComm.Parameters.AddWithValue("@NUMBER_OF_WRONG_PASSWORD", 0)
            xComm.Parameters.AddWithValue("@ACTIVE", tmpActive)
            xComm.Parameters.AddWithValue("@LAST_LOGIN", DateSerial(Today.Year, Today.Month, Today.Day))
            xComm.Parameters.AddWithValue("@PASSWORD_HINT", PASSWORD_HINTTextBox.Text)
            xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", DateSerial(Today.Year, Today.Month, Today.Day))
            xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", DateSerial(4000, 12, 31))
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", UserId)
            xComm.Parameters.AddWithValue("@INPUT_DATE", Now)
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", 0)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", DateSerial(4000, 12, 31))
            xComm.ExecuteNonQuery()
        ElseIf tmpSaveMode = "Update" Then
            xComm = New SqlCommand("SP_USER", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "U")
            xComm.Parameters.AddWithValue("@USER_ID", USERBindingSource.Current("USER_ID"))
            xComm.Parameters.AddWithValue("@USER_NAME", USER_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@PASSWORD", PASSWORDTextBox.Text)
            xComm.Parameters.AddWithValue("@ACCESS_PRIVILEDGE", AccessBindingSource.Current("ACCESS_PRIVILEDGE_ID"))
            xComm.Parameters.AddWithValue("@NUMBER_OF_WRONG_PASSWORD", USERBindingSource.Current("NUMBER_OF_WRONG_PASSWORD"))
            xComm.Parameters.AddWithValue("@ACTIVE", tmpActive)
            xComm.Parameters.AddWithValue("@LAST_LOGIN", USERBindingSource.Current("LAST_LOGIN"))
            xComm.Parameters.AddWithValue("@PASSWORD_HINT", USERBindingSource.Current("PASSWORD_HINT"))
            xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", USERBindingSource.Current("EFFECTIVE_START_DATE"))
            xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", USERBindingSource.Current("EFFECTIVE_END_DATE"))
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", USERBindingSource.Current("USER_ID_INPUT"))
            xComm.Parameters.AddWithValue("@INPUT_DATE", USERBindingSource.Current("INPUT_DATE"))
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", UserId)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", Now)
            xComm.ExecuteNonQuery()
        End If

        tmpSaveMode = ""

        DisableInputBox(Me)
        USERBindingSource.CancelEdit()

        USERBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        cmdSearchName.Visible = True
        ACTIVECheckBox.Enabled = False

        GetData()
        USERBindingSource.Position = USERBindingSource.Find("USER_NAME", tmpKey)
        If Language = "Indonesian" Then
            MsgBox("Data Berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
        End If
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If AllowDelete = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If USER_NAMETextBox.Text = "" Then
            MsgBox("There is no user to delete !", MsgBoxStyle.Critical, "DMI Retail")
            Exit Sub
        End If

        If Language = "Indonesian" Then
            If MsgBox("Hapus User ?" & vbCrLf & USER_NAMETextBox.Text, _
                  MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
        Else
            If MsgBox("Delete User ?" & vbCrLf & USER_NAMETextBox.Text, _
                  MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
        End If
        xComm = New SqlCommand("SP_USER", xConn)
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@METHOD", "D")
        xComm.Parameters.AddWithValue("@USER_ID", USERBindingSource.Current("USER_ID"))
        xComm.Parameters.AddWithValue("@USER_NAME", USERBindingSource.Current("USER_NAME"))
        xComm.Parameters.AddWithValue("@PASSWORD", USERBindingSource.Current("PASSWORD"))
        xComm.Parameters.AddWithValue("@ACCESS_PRIVILEDGE", AccessBindingSource.Current("ACCESS_PRIVILEDGE_ID"))
        xComm.Parameters.AddWithValue("@NUMBER_OF_WRONG_PASSWORD", USERBindingSource.Current("NUMBER_OF_WRONG_PASSWORD"))
        xComm.Parameters.AddWithValue("@ACTIVE", USERBindingSource.Current("ACTIVE"))
        xComm.Parameters.AddWithValue("@LAST_LOGIN", USERBindingSource.Current("LAST_LOGIN"))
        xComm.Parameters.AddWithValue("@PASSWORD_HINT", USERBindingSource.Current("PASSWORD_HINT"))
        xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", USERBindingSource.Current("EFFECTIVE_START_DATE"))
        xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", USERBindingSource.Current("EFFECTIVE_END_DATE"))
        xComm.Parameters.AddWithValue("@USER_ID_INPUT", USERBindingSource.Current("USER_ID_INPUT"))
        xComm.Parameters.AddWithValue("@INPUT_DATE", USERBindingSource.Current("INPUT_DATE"))
        xComm.Parameters.AddWithValue("@USER_ID_UPDATE", USERBindingSource.Current("USER_ID_UPDATE"))
        xComm.Parameters.AddWithValue("@UPDATE_DATE", USERBindingSource.Current("UPDATE_DATE"))
        xComm.ExecuteNonQuery()

        USERBindingSource.RemoveCurrent()
        USERBindingSource.Position = 0
        GetData()
    End Sub

    Private Sub USER_NAMETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles USER_NAMETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub PASSWORDTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PASSWORDTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub CONFIRM_PASSWORDTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONFIRM_PASSWORDTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub ACCESS_PRIVILEDGEComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tmpChange = True
    End Sub

    Private Sub PASSWORD_HINTTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PASSWORD_HINTTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub USERBindingSource_CurrentChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If USER_NAMETextBox.Text <> "" And USER_NAMETextBox.Enabled = False Then

            If USERBindingSource.Current("ACTIVE") = "YES" Then
                ACTIVECheckBox.Checked = True
            Else
                ACTIVECheckBox.Checked = False
            End If


            If USERBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                lblStatus.Visible = True
                lblStatus.Text = "DELETED"
                cmdDelete.Enabled = False
                cmdEdit.Enabled = False
            Else
                cmdDelete.Enabled = True
                cmdEdit.Enabled = True
                lblStatus.Visible = False
            End If

            AccessBindingSource.Position = AccessBindingSource.Find("ACCESS_PRIVILEDGE_ID", USERBindingSource.Current("ACCESS_PRIVILEDGE"))

        End If

    End Sub

    Private Sub cmdSearchName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchName.Click
        tmpClickMode = "Search"

        frmListUser.ShowDialog()

        tmpClickMode = ""

        If frmListUser.tmpUserResult = "" Then Exit Sub

    End Sub

    Private Sub BindingNavigatorPositionItem_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BindingNavigatorPositionItem.TextChanged
        If USER_NAMETextBox.Text <> "" And USER_NAMETextBox.Enabled = False Then

            If USERBindingSource.Current("ACTIVE") = "YES" Then
                ACTIVECheckBox.Checked = True
            Else
                ACTIVECheckBox.Checked = False
            End If


            If USERBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                lblStatus.Visible = True
                lblStatus.Text = "DELETED"
                cmdDelete.Enabled = False
                cmdEdit.Enabled = False
            Else
                cmdDelete.Enabled = True
                cmdEdit.Enabled = True
                lblStatus.Visible = False
            End If

            'USERTableAdapter.SP_GET_ACCESS_PRIVILEGE_NAME(access.Current("ACCESS_PRIVILEDGE_ID"), ACCESS_PRIVILEDGE_NAMEComboBox.Text)

            'Dim tmpAcces As Integer
            'USERTableAdapter.SP_GET_ACCESS_PRIVILEGE_NAME(USERBindingSource.Current("USER_ID"), tmpAcces)
            AccessBindingSource.Position = AccessBindingSource.Find("ACCESS_PRIVILEDGE_ID", USERBindingSource.Current("ACCESS_PRIVILEDGE"))

        End If
    End Sub

End Class