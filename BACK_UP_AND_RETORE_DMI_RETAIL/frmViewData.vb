﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Data
Public Class frmViewData
    Dim xConn As New SqlConnection
    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Public Language, ServerName, DatabaseName As String
    Public tmpConfig() As String
    Sub connection()
        If File.Exists(Application.StartupPath & "\Config.inf") Then
            tmpConfig = File.ReadAllLines(Application.StartupPath & "\Config.inf")
            For x As Integer = 0 To tmpConfig.Count - 1
                If tmpConfig(x).StartsWith("[Language]") Then
                    Language = tmpConfig(x).Split("=")(1)
                ElseIf tmpConfig(x).StartsWith("[Server Name]") Then
                    ServerName = tmpConfig(x).Split("=")(1)
                ElseIf tmpConfig(x).StartsWith("[Database Name]") Then
                    DatabaseName = tmpConfig(x).Split("=")(1)
                End If
            Next
        End If
        xConn.Close()
        xConn.ConnectionString = "Data Source=" & ServerName & ";Initial Catalog=" & DatabaseName & ";Persist Security Info=True;User ID=sa;Password=SA;"
        xConn.Open()
    End Sub

    Private Sub frmViewData_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F5 Then
            Button1_Click(Nothing, Nothing)
        ElseIf e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F1 Then
            TextBox1.Text = ""
        ElseIf e.KeyCode = Keys.F2 Then
            TextBox1.Text = "SELECT * FROM "
        ElseIf e.KeyCode = Keys.F3 Then
            TextBox1.Text = "INSERT INTO "
        End If
    End Sub

    Private Sub frmViewData_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        subMenu.MaximizeBox = True
        Call connection()

        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA .TABLES "
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.dgvTableDb.DataSource = dt
        dgvTableDb.Columns("TABLE_NAME").Width = 250
        dgvTableDb.Columns("TABLE_NAME").HeaderText = "Table Name"
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim dt As New DataTable
        Dim sql As String

        Call connection()
        Try
            Sql = TextBox1.Text
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.dgvQuery.DataSource = dt
        Catch ex As Exception
            sql = TextBox1.Text
            xComm = New SqlCommand(sql, xConn)
            xComm.ExecuteNonQuery()
        End Try
    End Sub

    Private Sub dgvTableDb_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTableDb.CellDoubleClick
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "SELECT * FROM " & dgvTableDb.Item(0, e.RowIndex).Value & ""
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.dgvQuery.DataSource = dt
    End Sub

End Class