﻿Imports System.Data.SqlClient
Imports System.Data
Public Class frmReportPurchaseByStatus

    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
  

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        'Me.SP_PURCHASE_REPORT_BY_STATUSTableAdapter.Fill(Me.DS_PURCHASE.SP_PURCHASE_REPORT_BY_STATUS, dtpPeriod1.Value, dtpPeriod2.Value)
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC 	@return_value = [dbo].[SP_PURCHASE_REPORT_BY_STATUS]" & vbCrLf & _
              "         @PERIOD1 = N'" & dtpPeriod1.Value & "'," & vbCrLf & _
              "    		@PERIOD2 = N'" & dtpPeriod2.Value & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.dgvRptPurchaseByStatus.DataSource = dt
        If Language = "Indonesian" Then
            dgvRptPurchaseByStatus.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            dgvRptPurchaseByStatus.Columns("PURCHASE_DATE").HeaderText = "Tgl. Pembelian"
            dgvRptPurchaseByStatus.Columns("PAYMENT_METHOD").HeaderText = "Cara Pembayaran"
            dgvRptPurchaseByStatus.Columns("WAREHOUSE").HeaderText = "Gudang"
            dgvRptPurchaseByStatus.Columns("VENDOR").HeaderText = "Supplier"
            dgvRptPurchaseByStatus.Columns("NUMBER_OF_ITEM").HeaderText = "Jumlah Item"
            dgvRptPurchaseByStatus.Columns("DOWN_PAYMENT").HeaderText = "Uang Muka"
            dgvRptPurchaseByStatus.Columns("GRAND_TOTAL").HeaderText = "Grand Total"
            dgvRptPurchaseByStatus.Columns("USER_UPDATE").HeaderText = "User Update"
            dgvRptPurchaseByStatus.Columns("DOWN_PAYMENT").Width = 50
            dgvRptPurchaseByStatus.Columns("NUMBER_OF_ITEM").Width = 50
            dgvRptPurchaseByStatus.Columns("PURCHASE_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptPurchaseByStatus.Columns("NUMBER_OF_ITEM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptPurchaseByStatus.Columns("DOWN_PAYMENT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptPurchaseByStatus.Columns("GRAND_TOTAL").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptPurchaseByStatus.Columns("PURCHASE_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvRptPurchaseByStatus.Columns("GRAND_TOTAL").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvRptPurchaseByStatus.Columns("DATE_FROM").Visible = False
            dgvRptPurchaseByStatus.Columns("DATE_TO").Visible = False
        Else
            dgvRptPurchaseByStatus.Columns("RECEIPT_NO").HeaderText = "Receipt No"
            dgvRptPurchaseByStatus.Columns("PURCHASE_DATE").HeaderText = "Purchase Date"
            dgvRptPurchaseByStatus.Columns("PAYMENT_METHOD").HeaderText = "Payment Method"
            dgvRptPurchaseByStatus.Columns("WAREHOUSE").HeaderText = "Warehouse"
            dgvRptPurchaseByStatus.Columns("VENDOR").HeaderText = "Vendor"
            dgvRptPurchaseByStatus.Columns("NUMBER_OF_ITEM").HeaderText = "No Of Item"
            dgvRptPurchaseByStatus.Columns("DOWN_PAYMENT").HeaderText = "DP"
            dgvRptPurchaseByStatus.Columns("GRAND_TOTAL").HeaderText = "Grand Total"
            dgvRptPurchaseByStatus.Columns("USER_UPDATE").HeaderText = "User Update"
            dgvRptPurchaseByStatus.Columns("DOWN_PAYMENT").Width = 50
            dgvRptPurchaseByStatus.Columns("NUMBER_OF_ITEM").Width = 50
            dgvRptPurchaseByStatus.Columns("PURCHASE_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptPurchaseByStatus.Columns("NUMBER_OF_ITEM").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptPurchaseByStatus.Columns("DOWN_PAYMENT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptPurchaseByStatus.Columns("GRAND_TOTAL").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvRptPurchaseByStatus.Columns("PURCHASE_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvRptPurchaseByStatus.Columns("GRAND_TOTAL").DefaultCellStyle.Format = "dd-MMM-yyyy"
            dgvRptPurchaseByStatus.Columns("DATE_FROM").Visible = False
            dgvRptPurchaseByStatus.Columns("DATE_TO").Visible = False
        End If
    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        Dim objf As New frmRepPurchaseByStatus
        objf.date1 = dtpPeriod1.Value
        objf.date2 = dtpPeriod2.Value
        objf.ReportViewer1.ShowRefreshButton = False
        objf.ReportViewer1.ZoomPercent = 100
        objf.WindowState = FormWindowState.Maximized
        objf.Show()
    End Sub

    Private Sub frmReportPurchaseByStatus_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dtpPeriod1.Value = DateSerial(Today.Year, Today.Month, 1)
        dtpPeriod2.Value = DateSerial(Today.Year, Today.Month + 1, 0)

        If Language = "Indonesian" Then
            lblFrom.Text = "Periode"
            cmdGenerate.Text = "Proses"
            cmdReport.Text = "Cetak"

            'dgvRptPurchaseByStatus.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            'dgvRptPurchaseByStatus.Columns("PURCHASE_DATE").HeaderText = "Tgl. Pembelian"
            'dgvRptPurchaseByStatus.Columns("PAYMENT_METHOD").HeaderText = "Cara Pembayaran"
            'dgvRptPurchaseByStatus.Columns("WAREHOUSE").HeaderText = "Gudang"
            'dgvRptPurchaseByStatus.Columns("VENDOR").HeaderText = "Supplier"
            'dgvRptPurchaseByStatus.Columns("NUMBER_OF_ITEM").HeaderText = "Jumlah Item"
            'dgvRptPurchaseByStatus.Columns("DOWN_PAYMENT").HeaderText = "Uang Muka"

        End If

    End Sub
End Class