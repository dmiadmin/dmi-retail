﻿Imports System.Data.SqlClient
Imports ClassDatabase

Public Class frmEntryPaymentMethod
    Dim tmpSaveMode, tmpCode, tmpPaymentMethodName, tmpGroup As String
    Dim tmpNoOfPaymentMethod, tmpUsedMethod As Integer
    Dim tmpChange As Boolean
    Dim tmpEndDate As DateTime
    Dim ObjClassDatabase As New ClassDatabase.ClassDbConnection
    Public PAYMENT_METHODBindingSource As New BindingSource

    Private Sub frmEntryPaymentMethod_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If tmpChange = True And DESCRIPTIONTextBox.Enabled = True Then
            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah Anda ingin menyimpan perubahan " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If
            If tmpvar = 6 Then
                cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub GetData()
        dt = New DataTable
        sqladapter = New SqlDataAdapter("SELECT PAYMENT_METHOD_ID, PAYMENT_METHOD_NAME, DESCRIPTION, " & _
                                        "EFFECTIVE_START_DATE, EFFECTIVE_END_DATE, USER_ID_INPUT, INPUT_DATE, " & _
                                        "USER_ID_UPDATE, UPDATE_DATE, PAYMENT_GROUP FROM PAYMENT_METHOD", xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        PAYMENT_METHODBindingSource.DataSource = dt
        PAYMENT_METHODBindingNavigator.BindingSource = PAYMENT_METHODBindingSource
    End Sub

    Private Sub frmPaymentMethod_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        AccessPrivilege(Me.Text)
        GetData()

        PAYMENT_METHOD_NAMETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", PAYMENT_METHODBindingSource, "PAYMENT_METHOD_NAME", True))
        PAYMENT_GROUPComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", PAYMENT_METHODBindingSource, "PAYMENT_GROUP", True))
        DESCRIPTIONTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", PAYMENT_METHODBindingSource, "DESCRIPTION", True))

        If Language = "Indonesian" Then
            lblDescription.Text = "Keterangan"
            lblGroup.Text = "Kelompok Pembayaran"
            lblMethod.Text = "Nama Cara Pembayaran"
            cmdAdd.Text = "Tambah"
            cmdEdit.Text = "Ubah"
            cmdUndo.Text = "Batal"
            cmdSave.Text = "Simpan"
            cmdDelete.Text = "Hapus"
            Me.Text = "Input Cara Pembayaran"
        End If

        If PAYMENT_METHOD_NAMETextBox.Text <> "" Then
            If PAYMENT_METHODBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
                cmdDelete.Enabled = True
                cmdEdit.Enabled = True
            End If
        End If

        DisableInputBox(Me)

        PAYMENT_METHODBindingNavigator.Enabled = True
        chkActive.Enabled = False
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True

        tmpChange = False

    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If AllowAdd = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Insert"

        EnableInputBox(Me)
        PAYMENT_METHODBindingSource.AddNew()
        chkActive.Enabled = True
        chkActive.Checked = True
        PAYMENT_METHODBindingNavigator.Enabled = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        cmdSearch.Visible = False
        tmpChange = False

    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        If PAYMENT_METHOD_NAMETextBox.Text = "" Then Exit Sub

        If AllowEdit = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Update"

        EnableInputBox(Me)

        PAYMENT_METHODBindingNavigator.Enabled = False
        cmdSearch.Visible = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        chkActive.Enabled = True
        tmpChange = False
        tmpPaymentMethodName = PAYMENT_METHOD_NAMETextBox.Text

        If PAYMENT_METHODBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
            chkActive.Checked = False
        Else
            chkActive.Checked = True
        End If
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        tmpSaveMode = ""

        DisableInputBox(Me)
        PAYMENT_METHODBindingSource.CancelEdit()

        PAYMENT_METHODBindingNavigator.Enabled = True
        cmdSearch.Visible = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        chkActive.Enabled = False
        tmpChange = False

    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Me.Validate()

        If PAYMENT_METHOD_NAMETextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If PAYMENT_GROUPComboBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If Not ValidateComboBox(PAYMENT_GROUPComboBox) Then
            If Language = "Indonesian" Then
                MsgBox("Grup Pembayaran yang anda masukkan tidak terdapat dalam daftar.", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You input Payment Group which is not in the list.", MsgBoxStyle.Critical, "DMI Retail")
            End If
            PAYMENT_GROUPComboBox.Focus()
            Exit Sub
        End If

        tmpCode = PAYMENT_METHOD_NAMETextBox.Text

        If tmpSaveMode = "Insert" Then
            xComm = New SqlCommand("SP_CHECK_PAYMENT_METHOD_NAME", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@PAYMENT_METHOD_NAME", PAYMENT_METHOD_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@OUTPUT", tmpNoOfPaymentMethod)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xdreader = xComm.ExecuteReader
            xdreader.Read()
            If tmpNoOfPaymentMethod > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Nama sudah ada di database." & vbCrLf & _
                     "Silahkan masukan nama lain!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Name already exists in Database." & vbCrLf & _
                     "Please enter another Name!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If

            xComm = New SqlCommand("SP_PAYMENT_METHOD", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "I")
            xComm.Parameters.AddWithValue("@PAYMENT_METHOD_ID", 0)
            xComm.Parameters.AddWithValue("@PAYMENT_METHOD_NAME", PAYMENT_METHOD_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@PAYMENT_GROUP", PAYMENT_GROUPComboBox.Text)
            xComm.Parameters.AddWithValue("@DESCRIPTION", DESCRIPTIONTextBox.Text)
            xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", DateSerial(Today.Year, Today.Month, Today.Day))
            xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", DateSerial(4000, 12, 31))
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", UserId)
            xComm.Parameters.AddWithValue("@INPUT_DATE", Now)
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", 0)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", DateSerial(4000, 12, 31))
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()
        ElseIf tmpSaveMode = "Update" Then
            If tmpPaymentMethodName = PAYMENT_METHOD_NAMETextBox.Text Then
                tmpNoOfPaymentMethod = 0
            Else
                xComm = New SqlCommand("SP_CHECK_PAYMENT_METHOD_NAME", xConn)
                xComm.CommandType = CommandType.StoredProcedure
                xComm.Parameters.AddWithValue("@PAYMENT_METHOD_NAME", PAYMENT_METHOD_NAMETextBox.Text)
                xComm.Parameters.AddWithValue("@OUTPUT", tmpNoOfPaymentMethod)
                If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
                xdreader = xComm.ExecuteReader
                xdreader.Read()
            End If
            If tmpNoOfPaymentMethod > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Nama sudah ada di database." & vbCrLf & _
                     "Silahkan masukan nama lain!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Name already exists in Database." & vbCrLf & _
                     "Please enter another Name!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If

            If chkActive.Checked Then tmpEndDate = DateSerial(4000, 12, 31) Else tmpEndDate = Now
            xComm = New SqlCommand("SP_PAYMENT_METHOD", xConn)
            xComm.CommandType = CommandType.StoredProcedure
            xComm.Parameters.AddWithValue("@METHOD", "U")
            xComm.Parameters.AddWithValue("@PAYMENT_METHOD_ID", PAYMENT_METHODBindingSource.Current("PAYMENT_METHOD_ID"))
            xComm.Parameters.AddWithValue("@PAYMENT_METHOD_NAME", PAYMENT_METHOD_NAMETextBox.Text)
            xComm.Parameters.AddWithValue("@PAYMENT_GROUP", PAYMENT_GROUPComboBox.Text)
            xComm.Parameters.AddWithValue("@DESCRIPTION", DESCRIPTIONTextBox.Text)
            xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", PAYMENT_METHODBindingSource.Current("EFFECTIVE_START_DATE"))
            xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", tmpEndDate)
            xComm.Parameters.AddWithValue("@USER_ID_INPUT", PAYMENT_METHODBindingSource.Current("USER_ID_INPUT"))
            xComm.Parameters.AddWithValue("@INPUT_DATE", PAYMENT_METHODBindingSource.Current("INPUT_DATE"))
            xComm.Parameters.AddWithValue("@USER_ID_UPDATE", UserId)
            xComm.Parameters.AddWithValue("@UPDATE_DATE", Now)
            If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
            xComm.ExecuteNonQuery()
        End If

        tmpSaveMode = ""

        DisableInputBox(Me)
        PAYMENT_METHODBindingSource.CancelEdit()

        PAYMENT_METHODBindingNavigator.Enabled = True
        cmdSearch.Visible = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = True
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        chkActive.Enabled = False

        GetData()
        If Language = "Indonesian" Then
            MsgBox("Data Berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
        End If

        PAYMENT_METHODBindingSource_CurrentChanged(Nothing, Nothing)
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If PAYMENT_METHOD_NAMETextBox.Text = "" Then Exit Sub
        If AllowDelete = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If Language = "Indonesian" Then
            If MsgBox("Hapus Cara Pembayaran?" & vbCrLf & PAYMENT_METHOD_NAMETextBox.Text, _
                      MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
        Else

            If MsgBox("Delete PAYMENT_METHOD?" & vbCrLf & PAYMENT_METHOD_NAMETextBox.Text, _
                      MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
        End If

        xComm = New SqlCommand("SP_CHECK_PAYMENT_METHOD_USED", xConn)
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@PAYMENT_METHOD_NAME", PAYMENT_METHOD_NAMETextBox.Text)
        xComm.Parameters.AddWithValue("@RESULT", tmpUsedMethod)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xdreader = xComm.ExecuteReader
        xdreader.Read()

        If tmpUsedMethod > 0 Then
            If Language = "Indonesian" Then
                MsgBox("Metode ini sudah dipakai di transaksi lain !" & vbCrLf & _
                MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("This Method is used by another transaction !" & vbCrLf & _
                MsgBoxStyle.Critical, "DMI Retail")
            End If
        End If

        xComm = New SqlCommand("SP_PAYMENT_METHOD", xConn)
        xComm.CommandType = CommandType.StoredProcedure
        xComm.Parameters.AddWithValue("@METHOD", "D")
        xComm.Parameters.AddWithValue("@PAYMENT_METHOD_ID", PAYMENT_METHODBindingSource.Current("PAYMENT_METHOD_ID"))
        xComm.Parameters.AddWithValue("@PAYMENT_METHOD_NAME", PAYMENT_METHODBindingSource.Current("PAYMENT_METHOD_NAME"))
        xComm.Parameters.AddWithValue("@PAYMENT_GROUP", PAYMENT_METHODBindingSource.Current("PAYMENT_GROUP"))
        xComm.Parameters.AddWithValue("@DESCRIPTION", PAYMENT_METHODBindingSource.Current("DESCRIPTION"))
        xComm.Parameters.AddWithValue("@EFFECTIVE_START_DATE", PAYMENT_METHODBindingSource.Current("EFFECTIVE_START_DATE"))
        xComm.Parameters.AddWithValue("@EFFECTIVE_END_DATE", Now)
        xComm.Parameters.AddWithValue("@USER_ID_INPUT", PAYMENT_METHODBindingSource.Current("USER_ID_INPUT"))
        xComm.Parameters.AddWithValue("@INPUT_DATE", PAYMENT_METHODBindingSource.Current("INPUT_DATE"))
        xComm.Parameters.AddWithValue("@USER_ID_UPDATE", PAYMENT_METHODBindingSource.Current("USER_ID_UPDATE"))
        xComm.Parameters.AddWithValue("@UPDATE_DATE", PAYMENT_METHODBindingSource.Current("UPDATE_DATE"))
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xComm.ExecuteNonQuery()

        PAYMENT_METHODBindingSource.RemoveCurrent()
        PAYMENT_METHODBindingSource.Position = 0

        Try
            If PAYMENT_METHODBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
                cmdDelete.Enabled = True
                cmdEdit.Enabled = True
            End If
        Catch
        End Try

        GetData()
        If Language = "Indonesian" Then
            MsgBox("Data Berhasil dihapus.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully Deleted.", MsgBoxStyle.Information, "DMI Retail")
        End If
    End Sub

    Private Sub PAYMENT_METHOD_NAMETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PAYMENT_METHOD_NAMETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub PAYMENT_GROUPComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PAYMENT_GROUPComboBox.SelectedIndexChanged
        tmpChange = True
    End Sub

    Private Sub DESCRIPTIONTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DESCRIPTIONTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub PAYMENT_METHODBindingSource_CurrentChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If PAYMENT_METHOD_NAMETextBox.Text <> "" And PAYMENT_METHOD_NAMETextBox.Enabled = False Then
            If PAYMENT_METHODBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
                cmdDelete.Enabled = True
                cmdEdit.Enabled = True
            End If
        End If
    End Sub

    Private Sub cmdSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        frmSearchPaymentMethod.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        PAYMENT_METHODBindingSource.Position = PAYMENT_METHODBindingSource.Find("PAYMENT_METHOD_ID", tmpSearchResult)
    End Sub

    Private Sub BindingNavigatorPositionItem_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BindingNavigatorPositionItem.TextChanged
        If PAYMENT_METHOD_NAMETextBox.Text <> "" And PAYMENT_METHOD_NAMETextBox.Enabled = False Then
            If PAYMENT_METHODBindingSource.Current("EFFECTIVE_END_DATE") < Now Then
                chkActive.Checked = False
            Else
                chkActive.Checked = True
                cmdDelete.Enabled = True
                cmdEdit.Enabled = True
            End If
        End If
    End Sub

End Class