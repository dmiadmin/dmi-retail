﻿Public Class frmEntryAdjustment
    Dim tmpSaveMode As String
    Dim tmpKey As String
    Dim tmpAdjustmentNo As String
    Dim tmpChange, tmpNoData As Boolean
    Dim tmpCategory As Integer
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    Private Sub frmEntryAdjustment_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If tmpChange = True And DESCRIPTIONTextBox.Enabled = True Then
            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah Anda ingin menyimpan perubahan " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If
            If tmpvar = 6 Then
                cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        Else
            Me.Dispose()
        End If
    End Sub

    Private Sub frmEntryAdjustment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_PRODUCT_WAREHOUSE.SP_LIST_PRODUCT_GOODS' table. You can move, or remove it, as needed.
        Me.SP_LIST_PRODUCT_GOODSTableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.SP_LIST_PRODUCT_GOODS, Now)
        'TODO: This line of code loads data into the 'DS_PARAMETER.PARAMETER' table. You can move, or remove it, as needed.
        Me.PARAMETERTableAdapter.Fill(Me.DS_PARAMETER.PARAMETER)
        'TODO: This line of code loads data into the 'DS_ADJUSTMENT.WAREHOUSE' table. You can move, or remove it, as needed.
        Me.WAREHOUSETableAdapter.Fill(Me.DS_ADJUSTMENT.WAREHOUSE)
        'TODO: This line of code loads data into the 'DS_ADJUSTMENT.PRODUCT_WAREHOUSE' table. You can move, or remove it, as needed.
        Me.PRODUCT_WAREHOUSETableAdapter.Fill(Me.DS_ADJUSTMENT.PRODUCT_WAREHOUSE)
        'TODO: This line of code loads data into the 'DS_ADJUSTMENT.PRODUCT' table. You can move, or remove it, as needed.
        Me.PRODUCTTableAdapter.Fill(Me.DS_ADJUSTMENT.PRODUCT)
        'TODO: This line of code loads data into the 'DS_ADJUSTMENT.ADJUSTMENT' table. You can move, or remove it, as needed.
        Me.ADJUSTMENTTableAdapter.Fill(Me.DS_ADJUSTMENT.ADJUSTMENT)

        accFormName = Me.Text
        Call AccessPrivilege()
        If Language = "Indonesian" Then
            lblAdjustmenNo.Text = "No Penyesuaian"
            Me.Text = "Input Penyesuaian"
            lblDate.Text = "Tanggal"
            lblDescription.Text = "Keterangan"
            lblProduct.Text = "Produk"
            lblQuantity.Text = "Quantity"
            lblUnitPrice.Text = "Harga Jual"
            lblWarehouse.Text = "Gudang"
            cmdAdd.Text = "Tambah"
            cmdSave.Text = "Simpan"
            cmdEdit.Text = "Ubah"
            cmdUndo.Text = "Batal"
            cmdDelete.Text = "Hapus"

            If SP_LIST_PRODUCT_GOODSBindingSource Is Nothing Or SP_LIST_PRODUCT_GOODSBindingSource.Count < 1 Then
                MsgBox("Tidak ada data produk." & vbCrLf & "Tolong masukan salah satu data produk!", _
                            MsgBoxStyle.Critical, "DMI Retail")
                tmpNoData = True
            End If

            'If WAREHOUSEBindingSource Is Nothing Or WAREHOUSEBindingSource.Count < 1 Then
            '    MsgBox("Tidak ada data Gudang." & vbCrLf & "Silakan input setidaknya satu Gudang untuk melanjutkan transaksi ini!", _
            '          MsgBoxStyle.Critical, "DMI Retail")
            '    tmpNoData = True
            'End If

        Else

            If SP_LIST_PRODUCT_GOODSBindingSource Is Nothing Or SP_LIST_PRODUCT_GOODSBindingSource.Count < 1 Then
                MsgBox("There is no Product data." & vbCrLf & "Please enter at least one Product data to continue this process!", _
                       MsgBoxStyle.Critical, "DMI Retail")
                tmpNoData = True
            End If

            'If WAREHOUSEBindingSource Is Nothing Or WAREHOUSEBindingSource.Count < 1 Then
            '    MsgBox("There is no Warehouse data." & vbCrLf & "Please enter at least one Product data to continue this process!", _
            '          MsgBoxStyle.Critical, "DMI Retail")
            '    tmpNoData = True
            'End If

        End If

        DisableInputBox(Me)
        WAREHOUSE_IDComboBox.DataSource = WAREHOUSEBindingSource1
        cmdSearchProduct.Visible = False
        ADJUSTMENTBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = False
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        tmpChange = False
        'txtDTP.Text = Format(ADJUSTMENT_DATEDateTimePicker.Value, "dd-MMM-yyyy")
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click

        If Trim(RsAccessPrivilege.Item("ALLOW_ADD").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Insert"

        tmpKey = ADJUSTMENT_NOTextBox.Text

        WAREHOUSE_IDComboBox.DataSource = WAREHOUSEBindingSource
       
        EnableInputBox(Me)
        ADJUSTMENTBindingSource.AddNew()
        ADJUSTMENT_DATEDateTimePicker.Value = Now
        ADJUSTMENTTableAdapter.SP_ADJUSTMENT_GENERATE_NUMBER(ADJUSTMENT_NOTextBox.Text)

        QUANTITYTextBox.Text = "0"
        PRICE_PER_UNITTextBox.Text = "0"

        cmdSearchProduct.Visible = True
        ADJUSTMENTBindingNavigator.Enabled = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False
        tmpChange = False
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        If ADJUSTMENT_NOTextBox.Text = "" Then Exit Sub

        If Trim(RsAccessPrivilege.Item("ALLOW_EDIT").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpSaveMode = "Update"

        tmpKey = ADJUSTMENT_NOTextBox.Text

        WAREHOUSE_IDComboBox.DataSource = WAREHOUSEBindingSource

        EnableInputBox(Me)

        cmdSearchProduct.Visible = True
        ADJUSTMENTBindingNavigator.Enabled = False
        cmdAdd.Enabled = False
        cmdEdit.Enabled = False
        cmdUndo.Enabled = True
        cmdSave.Enabled = True
        cmdDelete.Enabled = False

        tmpAdjustmentNo = ADJUSTMENT_NOTextBox.Text
        tmpChange = False
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        tmpSaveMode = ""

        WAREHOUSE_IDComboBox.DataSource = WAREHOUSEBindingSource1

        DisableInputBox(Me)
        ADJUSTMENTBindingSource.CancelEdit()

        cmdSearchProduct.Visible = False
        ADJUSTMENTBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = False
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True
        tmpChange = False

        Me.ADJUSTMENTBindingSource.Position = ADJUSTMENTBindingSource.Find("ADJUSTMENT_NO", tmpKey)
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click

        Me.Validate()

        Dim tmpDate As DateTime
        Dim tmpWarehouseID, tmpValQuantity, tmpPrice As Integer
        Dim tmpCheck As String = ""

        If Language = "Indonesian" Then
            ADJUSTMENTTableAdapter.SP_CHECK_CLOSING_PERIOD(ADJUSTMENT_DATEDateTimePicker.Value, tmpCheck)
            If tmpCheck = "TRUE" Then
                MsgBox("Tidak dapat menyimpan transaksi ini !" & vbCrLf & "Transaksi ini telah masuk dalam transaksi Tutup Buku", MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If

            ADJUSTMENTTableAdapter.SP_GET_CATEGORY_PRODUCT(PRODUCT_IDComboBox.Text, tmpCategory)
            If tmpCategory <> 1 Then
                MsgBox("Produk yang anda input bukan barang !" & vbCrLf & _
                    "Silahkan gunakan Form Pencarian untuk menemukan produk yang benar !", MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If
        Else
            ADJUSTMENTTableAdapter.SP_CHECK_CLOSING_PERIOD(ADJUSTMENT_DATEDateTimePicker.Value, tmpCheck)
            If tmpCheck = "TRUE" Then
                MsgBox("Cannot Save this Transaction!" & vbCrLf & "This Transaction has been in Closed Period", MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If

            ADJUSTMENTTableAdapter.SP_GET_CATEGORY_PRODUCT(PRODUCT_IDComboBox.Text, tmpCategory)
            If tmpCategory <> 1 Then
                MsgBox("You input Product which is not Goods !" & vbCrLf & _
                         "Please use the Search Form to find the right Products !", MsgBoxStyle.Critical, "DMI Retail")
                Exit Sub
            End If
        End If
        tmpValQuantity = Val(QUANTITYTextBox.Text)
        tmpPrice = Val(PRICE_PER_UNITTextBox.Text)
        If tmpValQuantity = 0 Or tmpPrice = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpDate = ADJUSTMENT_DATEDateTimePicker.Value.Year & "/" & _
                  ADJUSTMENT_DATEDateTimePicker.Value.Month & "/" & _
                  ADJUSTMENT_DATEDateTimePicker.Value.Day & " " & _
                  Now.Hour & ":" & Now.Minute & ":" & Now.Second



        If ADJUSTMENT_NOTextBox.Text = "" Or PRODUCT_IDComboBox.Text = "" Or QUANTITYTextBox.Text = "" Or _
            PRICE_PER_UNITTextBox.Text = "" Or PRODUCT_NAMETextBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.SP_PRODUCT_WAREHOUSE_DETAIL, _
                                                     SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID"))

        If WAREHOUSE_IDComboBox.Text = "" Then
            tmpWarehouseID = 0
        Else
            tmpWarehouseID = WAREHOUSEBindingSource.Current("WAREHOUSE_ID")
        End If

        SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Filter = "WAREHOUSE_ID = " & tmpWarehouseID

        If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Count < 1 Then
            tmpQuantity = 0
        Else
            tmpQuantity = SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY")
        End If
        If tmpQuantity + CInt(QUANTITYTextBox.Text) < 0 Then
            If Language = "Indonesian" Then
                MsgBox("Produk ini mempunyai " & tmpQuantity & " jumlah." & vbCrLf & _
                                   "Penyesuaian produk dengan jumlah melebihi saldo tidak di perkenankan.", _
                                   MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("This Product has " & tmpQuantity & " quantity." & vbCrLf & _
                   "Adjusting a Product with quantity exceed the balance is not allowed.", _
                   MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If ADJUSTMENTTableAdapter.SP_CLOSED_PERIOD_PROCESS("G", ADJUSTMENT_DATEDateTimePicker.Value) = "TRUE" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menyimpan transaksi ini!" & vbCrLf & "Proses Tutup Buku sedang berlangsung", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Save this Transaction!" & vbCrLf & "The Closing Period is Running", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpKey = ADJUSTMENT_NOTextBox.Text

        If tmpSaveMode = "Insert" Then

            If ADJUSTMENTTableAdapter.FC_ADJUSTMENT_CHECK_NO(ADJUSTMENT_NOTextBox.Text) > 0 Then
                If Language = "Indonesian" Then
                    MsgBox("Nomor penyesuaian telah dipakai di database." & vbCrLf & _
                       "Silahkan masukan nomor lain!", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("Adjustment Number already exist in Database." & vbCrLf & _
                           "Please enter another Number!", MsgBoxStyle.Critical, "DMI Retail")
                End If
                Exit Sub
            End If

            ADJUSTMENTTableAdapter.SP_ADJUSTMENT("I", _
                                                 0, _
                                                 ADJUSTMENT_NOTextBox.Text, _
                                                 tmpDate, _
                                                 SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID"), _
                                                 QUANTITYTextBox.Text, _
                                                 PRICE_PER_UNITTextBox.Text, _
                                                 tmpWarehouseID, _
                                                 DESCRIPTIONTextBox.Text, _
                                                 USER_ID, _
                                                 Now, _
                                                 0, _
                                                 DateSerial(4000, 12, 31))

            PRODUCT_WAREHOUSETableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                               0, _
                                                               SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID"), _
                                                               tmpWarehouseID, _
                                                               QUANTITYTextBox.Text, _
                                                               USER_ID, _
                                                               Now, _
                                                               0, _
                                                               DateSerial(4000, 12, 31))

        ElseIf tmpSaveMode = "Update" Then

            If tmpAdjustmentNo <> ADJUSTMENT_NOTextBox.Text Then
                If ADJUSTMENTTableAdapter.FC_ADJUSTMENT_CHECK_NO(ADJUSTMENT_NOTextBox.Text) > 0 Then
                    If Language = "Indonesian" Then
                        MsgBox("Nomor penyesuaian telah dipakai di database." & vbCrLf & _
                           "Silahkan masukan nomor lain!", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("Adjustment Number already exist in Database." & vbCrLf & _
                               "Please enter another Number!", MsgBoxStyle.Critical, "DMI Retail")
                    End If
                    Exit Sub
                End If
            End If

            ADJUSTMENTTableAdapter.SP_ADJUSTMENT("U", _
                                                 ADJUSTMENTBindingSource.Current("ADJUSTMENT_ID"), _
                                                 ADJUSTMENT_NOTextBox.Text, _
                                                 tmpDate, _
                                                 SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID"), _
                                                 QUANTITYTextBox.Text, _
                                                 PRICE_PER_UNITTextBox.Text, _
                                                 tmpWarehouseID, _
                                                 DESCRIPTIONTextBox.Text, _
                                                 ADJUSTMENTBindingSource.Current("USER_ID_INPUT"), _
                                                 ADJUSTMENTBindingSource.Current("INPUT_DATE"), _
                                                 USER_ID, _
                                                 Now)

            PRODUCT_WAREHOUSETableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                               0, _
                                                               SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID"), _
                                                               ADJUSTMENTBindingSource.Current("WAREHOUSE_ID"), _
                                                               ADJUSTMENTBindingSource.Current("QUANTITY") * (-1), _
                                                               USER_ID, _
                                                               Now, _
                                                               0, _
                                                               DateSerial(4000, 12, 31))

            PRODUCT_WAREHOUSETableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                               0, _
                                                               SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID"), _
                                                               tmpWarehouseID, _
                                                               QUANTITYTextBox.Text, _
                                                               USER_ID, _
                                                               Now, _
                                                               0, _
                                                               DateSerial(4000, 12, 31))
        End If

        tmpSaveMode = ""

        DisableInputBox(Me)
        ADJUSTMENTBindingSource.CancelEdit()

        cmdSearchProduct.Visible = False
        ADJUSTMENTBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = False
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True

        Me.ADJUSTMENTTableAdapter.Fill(Me.DS_ADJUSTMENT.ADJUSTMENT)
        Me.ADJUSTMENTBindingSource.Position = ADJUSTMENTBindingSource.Find("ADJUSTMENT_NO", tmpKey)

        If Language = "Indonesian" Then
            MsgBox("Data berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
        End If

    End Sub


    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If ADJUSTMENT_NOTextBox.Text = "" Then Exit Sub

        If Trim(RsAccessPrivilege.Item("ALLOW_DELETE").ToString) = "No" Then
            If Language = "Indonesian" Then
                MsgBox("Anda tidak dapat mengkases fungsi ini." & vbCrLf & "Silahkan hubungi System Administrator.", _
                    MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You are not allowed to access this function." & vbCrLf & "Please contact your System Administrator.", _
                       MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        tmpKey = ADJUSTMENT_NOTextBox.Text

        Dim tmpCheck As String = ""
        ADJUSTMENTTableAdapter.SP_CHECK_CLOSING_PERIOD(ADJUSTMENT_DATEDateTimePicker.Value, tmpCheck)
        If tmpCheck = "TRUE" Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menghapus transaksi in!" & vbCrLf & "Transaksi ini telah masuk dalam transaksi Tutup Buku", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot Delete this Transaction!" & vbCrLf & "This Transaction has been in Closed Period", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If Language = "Indonesian" Then
            If MsgBox("Hapus Penyesuaian?" & vbCrLf & ADJUSTMENT_NOTextBox.Text, _
                  MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
        Else
            If MsgBox("Delete Adjustment?" & vbCrLf & ADJUSTMENT_NOTextBox.Text, _
                      MsgBoxStyle.Question + MsgBoxStyle.YesNo, "DMI Retail") = MsgBoxResult.No Then Exit Sub
        End If
        PRODUCT_WAREHOUSETableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                               0, _
                                                               SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID"), _
                                                               ADJUSTMENTBindingSource.Current("WAREHOUSE_ID"), _
                                                               ADJUSTMENTBindingSource.Current("QUANTITY") * (-1), _
                                                               ADJUSTMENTBindingSource.Current("USER_ID_INPUT"), _
                                                               ADJUSTMENTBindingSource.Current("INPUT_DATE"), _
                                                               USER_ID, _
                                                               Now)

        ADJUSTMENTTableAdapter.SP_ADJUSTMENT("V", _
                                             ADJUSTMENTBindingSource.Current("ADJUSTMENT_ID"), _
                                             ADJUSTMENTBindingSource.Current("ADJUSTMENT_NO"), _
                                             ADJUSTMENTBindingSource.Current("ADJUSTMENT_DATE"), _
                                             ADJUSTMENTBindingSource.Current("PRODUCT_ID"), _
                                             ADJUSTMENTBindingSource.Current("QUANTITY"), _
                                             ADJUSTMENTBindingSource.Current("PRICE_PER_UNIT"), _
                                             ADJUSTMENTBindingSource.Current("WAREHOUSE_ID"), _
                                             ADJUSTMENTBindingSource.Current("DESCRIPTION"), _
                                             ADJUSTMENTBindingSource.Current("USER_ID_INPUT"), _
                                             ADJUSTMENTBindingSource.Current("INPUT_DATE"), _
                                             ADJUSTMENTBindingSource.Current("USER_ID_UPDATE"), _
                                             ADJUSTMENTBindingSource.Current("UPDATE_DATE"))

        ADJUSTMENTBindingSource.RemoveCurrent()
        ADJUSTMENTBindingSource.Position = 0
        WAREHOUSE_IDComboBox.DataSource = WAREHOUSEBindingSource1

        If Language = "Indonesian" Then
            MsgBox("Data berhasil dihapus !", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully deleted !", MsgBoxStyle.Information, "DMI Retail")
        End If

        cmdSearchProduct.Visible = False
        ADJUSTMENTBindingNavigator.Enabled = True
        cmdAdd.Enabled = True
        cmdEdit.Enabled = False
        cmdUndo.Enabled = False
        cmdSave.Enabled = False
        cmdDelete.Enabled = True

        Try
            If ADJUSTMENTBindingSource.Current("VOID") = "TRUE" Then
                lblStatus.Visible = True
                lblStatus.Text = "DELETED"
                cmdDelete.Enabled = False
                cmdEdit.Enabled = False
            Else
                lblStatus.Visible = False
            End If
        Catch
        End Try

        If ADJUSTMENT_NOTextBox.Text = "" Then
            cmdEdit.Enabled = False
            cmdDelete.Enabled = False
        Else
            cmdEdit.Enabled = True
            cmdDelete.Enabled = True
        End If

        Me.ADJUSTMENTTableAdapter.Fill(Me.DS_ADJUSTMENT.ADJUSTMENT)
        Me.ADJUSTMENTBindingSource.Position = ADJUSTMENTBindingSource.Find("ADJUSTMENT_NO", tmpKey)

    End Sub

    Private Sub PRODUCT_IDComboBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles PRODUCT_IDComboBox.LostFocus
        If PRODUCT_IDComboBox.Text = "" Then Exit Sub
        Try
            SP_LIST_PRODUCT_GOODSTableAdapter.SP_GET_CATEGORY_PRODUCT(PRODUCT_IDComboBox.Text, tmpCategory)
        Catch
        End Try
        If tmpCategory <> 1 Then
            If Language = "Indonesian" Then
                MsgBox("Anda memasukan produk yang bukan barang !" & vbCrLf & _
                      "Gunakan Form Pencarian untuk menemukan produk yang tepat !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You input Product which is not Goods !" & vbCrLf & _
                       "Use the Search Form to find the right Product !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            PRODUCT_NAMETextBox.Text = ""
            PRODUCT_IDComboBox.Text = ""

        End If
    End Sub

    Private Sub PRODUCT_IDComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PRODUCT_IDComboBox.TextChanged
        PRODUCTTableAdapter.SP_GET_PRODUCT_NAME(PRODUCT_IDComboBox.Text, PRODUCT_NAMETextBox.Text)
    End Sub

    Private Sub cmdSearchProduct_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchProduct.Click
        mdlGeneral.tmpSearchMode = "Entry Adjustment - Product Name"
        frmSearchProduct.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub
        SP_LIST_PRODUCT_GOODSBindingSource.Position = SP_LIST_PRODUCT_GOODSBindingSource.Find("PRODUCT_ID", tmpSearchResult)
        PRODUCT_IDComboBox.Text = SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_CODE")
    End Sub

    Private Sub QUANTITYTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles QUANTITYTextBox.KeyPress
        If e.KeyChar = "-" Then Exit Sub
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""
    End Sub

    Private Sub PRICE_PER_UNITTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PRICE_PER_UNITTextBox.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Not IsNumeric(e.KeyChar) Then e.KeyChar = ""
    End Sub

    Private Sub ADJUSTMENT_NOTextBox_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ADJUSTMENT_NOTextBox.KeyDown
        If e.KeyCode = Keys.Delete Then
            e.Handled = True
        End If
    End Sub

    Private Sub ADJUSTMENT_NOTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ADJUSTMENT_NOTextBox.KeyPress
        e.KeyChar = ""
    End Sub

    Private Sub ADJUSTMENT_NOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ADJUSTMENT_NOTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub ADJUSTMENT_DATEDateTimePicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ADJUSTMENT_DATEDateTimePicker.ValueChanged
        txtDTP.Text = Format(ADJUSTMENT_DATEDateTimePicker.Value, "dd-MMM-yyyy")
        tmpChange = True
    End Sub

    Private Sub PRODUCT_IDComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PRODUCT_IDComboBox.SelectedIndexChanged
        tmpChange = True
    End Sub

    Private Sub PRODUCT_NAMETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PRODUCT_NAMETextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub QUANTITYTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QUANTITYTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub PRICE_PER_UNITTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PRICE_PER_UNITTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub DESCRIPTIONTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DESCRIPTIONTextBox.TextChanged
        tmpChange = True
    End Sub

    Private Sub txtDTP_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDTP.KeyDown
        If e.KeyCode = Keys.Delete Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtDTP_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDTP.KeyPress
        e.KeyChar = ""
    End Sub

    Private Sub frmEntryAdjustment_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
        If tmpNoData Then Me.Close()
    End Sub

    Private Sub ADJUSTMENTBindingSource_CurrentChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ADJUSTMENTBindingSource.CurrentChanged
        Try
            If ADJUSTMENT_NOTextBox.Text <> "" And ADJUSTMENTBindingSource.Current("VOID") = "TRUE" Then
                lblStatus.Visible = True
                lblStatus.Text = "DELETED"
                cmdDelete.Enabled = False
                cmdEdit.Enabled = False
            Else
                lblStatus.Visible = False
                cmdDelete.Enabled = True
            End If
        Catch
            lblStatus.Visible = False
            cmdDelete.Enabled = True
        End Try
    End Sub
End Class