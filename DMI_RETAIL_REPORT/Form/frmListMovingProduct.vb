﻿Imports System.Data.SqlClient
Imports System.Data
Public Class frmListMovingProduct

    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
  

    Private Sub frmListMovingProduct_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F12 Then
            cmdReport_Click(Nothing, Nothing)
        End If
    End Sub


    Private Sub frmListMovingProduct_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim tmpTotal As Integer

        dtpPeriod.Value = DateSerial(Today.Year, Today.Month, 1)

        For n As Integer = 0 To SP_MOVING_PRODUCTDataGridView.RowCount - 1
            tmpTotal = tmpTotal + SP_MOVING_PRODUCTDataGridView.Item("TOTAL", n).Value
            tmpTotal = tmpTotal
        Next

        If Language = "Indonesian" Then
            Me.Text = "Daftar Produk Terjual"

            cmdReport.Text = "Cetak"
            cmdGenerate.Text = "Proses"

            'SP_MOVING_PRODUCTDataGridView.Columns("PRODUCT_CODE").HeaderText = "Kode Produk"
            'SP_MOVING_PRODUCTDataGridView.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            'SP_MOVING_PRODUCTDataGridView.Columns("TOTAL").HeaderText = "Total Penjualan"

            lblPeriod.Text = "Periode"
            lblNoOfProduct.Text = "Jumlah Produk : " & SP_MOVING_PRODUCTDataGridView.RowCount
            lblTotal.Text = "Total Penjualan : " & tmpTotal
        Else
            lblNoOfProduct.Text = "No of Product(s) : " & SP_MOVING_PRODUCTDataGridView.RowCount
            lblTotal.Text = "Total Sold : " & tmpTotal
        End If
        txtProductName.Enabled = False
    End Sub


    Private Sub cmdGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        Cursor = Cursors.WaitCursor
        'SP_MOVING_PRODUCTTableAdapter.Fill(Me.DS_PRODUCT_STOCK.SP_MOVING_PRODUCT, dtpPeriod.Value, txtProductName.Text)
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC	    @return_value = [dbo].[SP_MOVING_PRODUCT]" & vbCrLf & _
              "         @PERIOD = N'" & dtpPeriod.Value & "'," & vbCrLf & _
              "    		@PRODUCT_NAME = N'" & txtProductName.Text & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.SP_MOVING_PRODUCTDataGridView.DataSource = dt
        Cursor = Cursors.Default

        Dim tmpTotal As Integer = 0
        For n As Integer = 0 To SP_MOVING_PRODUCTDataGridView.RowCount - 1
            tmpTotal = tmpTotal + SP_MOVING_PRODUCTDataGridView.Item("TOTAL", n).Value
            tmpTotal = tmpTotal
        Next

        If Language = "Indonesian" Then
            lblNoOfProduct.Text = "Jumlah Produk : " & SP_MOVING_PRODUCTDataGridView.RowCount
            lblTotal.Text = "Total Penjualan : " & tmpTotal
            SP_MOVING_PRODUCTDataGridView.Columns("PRODUCT_CODE").HeaderText = "Kode Produk"
            SP_MOVING_PRODUCTDataGridView.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            SP_MOVING_PRODUCTDataGridView.Columns("TOTAL").HeaderText = "Total Penjualan"
            SP_MOVING_PRODUCTDataGridView.Columns("TOTAL").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            SP_MOVING_PRODUCTDataGridView.Columns("PERIOD").Visible = False
            SP_MOVING_PRODUCTDataGridView.Columns("PRODUCT_ID").Visible = False
            SP_MOVING_PRODUCTDataGridView.Columns("PRODUCT_NAME").Width = 200
            SP_MOVING_PRODUCTDataGridView.Columns("PRODUCT_CODE").Width = 150
        Else
            lblNoOfProduct.Text = "No of Product(s) : " & SP_MOVING_PRODUCTDataGridView.RowCount
            lblTotal.Text = "Total Sold : " & tmpTotal
            SP_MOVING_PRODUCTDataGridView.Columns("PRODUCT_CODE").HeaderText = "Product Code"
            SP_MOVING_PRODUCTDataGridView.Columns("PRODUCT_NAME").HeaderText = "Product Name"
            SP_MOVING_PRODUCTDataGridView.Columns("TOTAL").HeaderText = "Total"
            SP_MOVING_PRODUCTDataGridView.Columns("TOTAL").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            SP_MOVING_PRODUCTDataGridView.Columns("PERIOD").Visible = False
            SP_MOVING_PRODUCTDataGridView.Columns("PRODUCT_ID").Visible = False
            SP_MOVING_PRODUCTDataGridView.Columns("PRODUCT_NAME").Width = 200
            SP_MOVING_PRODUCTDataGridView.Columns("PRODUCT_CODE").Width = 150
        End If
        txtProductName.Enabled = True
    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        Dim RPV As New frmRepSoldProduct
        RPV.period = dtpPeriod.Value
        RPV.productname = txtProductName.Text
        RPV.ReportViewer1.ZoomPercent = 100
        RPV.WindowState = FormWindowState.Maximized
        RPV.Show()
    End Sub

    Private Sub txtProductName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtProductName.TextChanged
        'SP_MOVING_PRODUCTBindingSource.Filter = "PRODUCT_NAME LIKE '%" & txtProductName.Text.ToUpper & "%'"
        Dim dt As New DataTable
        Call connection()
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC	    @return_value = [dbo].[SP_MOVING_PRODUCT]" & vbCrLf & _
              "         @PERIOD = N'" & dtpPeriod.Value & "'," & vbCrLf & _
              "    		@PRODUCT_NAME = N'" & txtProductName.Text & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        xAdoAdapter.Fill(dt)
        Me.SP_MOVING_PRODUCTDataGridView.DataSource = dt
        Cursor = Cursors.Default

        Dim tmpTotal As Integer = 0
        For n As Integer = 0 To SP_MOVING_PRODUCTDataGridView.RowCount - 1
            tmpTotal = tmpTotal + SP_MOVING_PRODUCTDataGridView.Item("TOTAL", n).Value
            tmpTotal = tmpTotal
        Next

        If Language = "Indonesian" Then
            lblNoOfProduct.Text = "Jumlah Produk : " & SP_MOVING_PRODUCTDataGridView.RowCount
            lblTotal.Text = "Total Penjualan : " & tmpTotal
        Else
            lblNoOfProduct.Text = "No of Product(s) : " & SP_MOVING_PRODUCTDataGridView.RowCount
            lblTotal.Text = "Total Sold : " & tmpTotal
        End If
    End Sub
End Class