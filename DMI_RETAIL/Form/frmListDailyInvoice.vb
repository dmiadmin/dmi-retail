﻿Public Class frmListDailyInvoice
    Dim tmpInvoice, tmpPrice, tmpQty, tmpVoidInvoice, tmpVoidPrice, tmpVoidQty As Integer

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        txtProductName.Text = ""

        Me.Cursor = Cursors.WaitCursor

        'SP_LIST_DAILY_INVOICETableAdapter.Fill(Me.DS_SALE.SP_LIST_DAILY_INVOICE, dtpInvoice.Value)

        ''Stored Procedure to get Total Non-Void Invoice
        'SP_LIST_DAILY_INVOICETableAdapter.SP_LABEL_DAILY_INVOICE("NI", dtpInvoice.Value, tmpInvoice)
        If tmpInvoice = 0 Then
            tmpPrice = 0
            tmpQty = 0
        Else
            ''Stored Procedure to get Total Non-Void Sold Price
            'SP_LIST_DAILY_INVOICETableAdapter.SP_LABEL_DAILY_INVOICE("NP", dtpInvoice.Value, tmpPrice)
            ''Stored Procedure to get Total Non-Void Quantity
            'SP_LIST_DAILY_INVOICETableAdapter.SP_LABEL_DAILY_INVOICE("NQ", dtpInvoice.Value, tmpQty)
        End If
        
        ''Stored Procedure to get Total Invoice including Void Invoice
        'SP_LIST_DAILY_INVOICETableAdapter.SP_LABEL_DAILY_INVOICE("VI", dtpInvoice.Value, tmpVoidInvoice)
        If tmpVoidInvoice = 0 Then
            tmpVoidPrice = 0
            tmpVoidQty = 0
        Else
            ''Stored Procedure to get Total Sold Price including Void Invoice
            'SP_LIST_DAILY_INVOICETableAdapter.SP_LABEL_DAILY_INVOICE("VP", dtpInvoice.Value, tmpVoidPrice)
            ''Stored Procedure to get Total Quantity including Void Invoice
            'SP_LIST_DAILY_INVOICETableAdapter.SP_LABEL_DAILY_INVOICE("VQ", dtpInvoice.Value, tmpVoidQty)
        End If

        Me.Cursor = Cursors.Default

        If Language = "Indonesian" Then
            lblVoidPrice.Text = "Total Harga Jual : " & FormatNumber(tmpVoidPrice, 0)
            lblVoidInvoice.Text = "Jumlah Faktur : " & FormatNumber(tmpVoidInvoice, 0)
            lblVoidQuantity.Text = "Jumlah item : " & FormatNumber(tmpVoidQty, 0)

            lblTotalPrice.Text = "Total Harga Jual : " & FormatNumber(tmpPrice, 0)
            lblTotalInvoice.Text = "Jumlah Faktur : " & FormatNumber(tmpInvoice, 0)
            lblQuantity.Text = "Jumlah item : " & FormatNumber(tmpQty, 0)

            lblSumTrans.Text = "Jumlah Transaksi : " & SP_LIST_DAILY_INVOICEDataGridView.RowCount & " Baris"

        Else

            lblVoidPrice.Text = "Total Sold Price : " & FormatNumber(tmpVoidPrice, 0)
            lblVoidInvoice.Text = "No of Invoice : " & FormatNumber(tmpVoidInvoice, 0)
            lblVoidQuantity.Text = "Total Quantity : " & FormatNumber(tmpVoidQty, 0)

            lblTotalPrice.Text = "Total Sold Price : " & FormatNumber(tmpPrice, 0)
            lblTotalInvoice.Text = "No of Invoice : " & FormatNumber(tmpInvoice, 0)
            lblQuantity.Text = "Total Quantity : " & FormatNumber(tmpQty, 0)

            lblSumTrans.Text = "No of Transaction(s) : " & SP_LIST_DAILY_INVOICEDataGridView.RowCount & " Row(s)"
        End If

    End Sub

    Private Sub txtProductName_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtProductName.KeyUp
        If e.KeyCode = Keys.Enter Then SP_LIST_DAILY_INVOICEDataGridView.Focus()
    End Sub

    Private Sub txtProductName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtProductName.TextChanged
        Try
            SP_LIST_DAILY_INVOICEBindingSource.Filter = "PRODUCT_NAME LIKE '%" & txtProductName.Text.ToUpper & "%'"
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtProductName.Text = ""
            txtProductName.Focus()
        End Try
    End Sub

    Private Sub dtpInvoice_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpInvoice.ValueChanged
        txtProductName.Text = ""
    End Sub

    Private Sub frmListDailyInvoice_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F12 Then
            cmdReport_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub frmListDailyInvoice_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Language = "Indonesian" Then
            Me.Text = "Faktur Harian"

            SP_LIST_DAILY_INVOICEDataGridView.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            SP_LIST_DAILY_INVOICEDataGridView.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            SP_LIST_DAILY_INVOICEDataGridView.Columns("PRICE_PER_UNIT").HeaderText = "Harga Satuan"
            SP_LIST_DAILY_INVOICEDataGridView.Columns("DISCOUNT").HeaderText = "Discount"
            SP_LIST_DAILY_INVOICEDataGridView.Columns("TOTAL").HeaderText = "Total Harga"
            SP_LIST_DAILY_INVOICEDataGridView.Columns("QUANTITY").HeaderText = "Jumlah"
            SP_LIST_DAILY_INVOICEDataGridView.Columns("VOID").HeaderText = "Batal"

            lblAllTrans.Text = "Semua Transaksi" & vbCrLf & "(Aktif + Batal)"
            lblActiveTrans.Text = "Transaksi Aktif"

            lblPeriod.Text = "Periode"
            lblProductName.Text = "Nama Produk"
            cmdGenerate.Text = "Proses"
            cmdReport.Text = "Cetak"

            lblVoidPrice.Text = "Total Harga Jual : " & FormatNumber(tmpVoidPrice, 0)
            lblVoidInvoice.Text = "Jumlah Faktur : " & FormatNumber(tmpVoidInvoice, 0)
            lblVoidQuantity.Text = "Jumlah item : " & FormatNumber(tmpVoidQty, 0)

            lblTotalPrice.Text = "Total Harga Jual : " & FormatNumber(tmpPrice, 0)
            lblTotalInvoice.Text = "Jumlah Faktur : " & FormatNumber(tmpInvoice, 0)
            lblQuantity.Text = "Jumlah item : " & FormatNumber(tmpQty, 0)

            lblSumTrans.Text = "Jumlah Transaksi : " & SP_LIST_DAILY_INVOICEDataGridView.RowCount & " Baris"

        Else

            lblVoidPrice.Text = "Total Sold Price : " & FormatNumber(tmpVoidPrice, 0)
            lblVoidInvoice.Text = "No of Invoice : " & FormatNumber(tmpVoidInvoice, 0)
            lblVoidQuantity.Text = "Total Quantity : " & FormatNumber(tmpVoidQty, 0)

            lblTotalPrice.Text = "Total Sold Price : " & FormatNumber(tmpPrice, 0)
            lblTotalInvoice.Text = "No of Invoice : " & FormatNumber(tmpInvoice, 0)
            lblQuantity.Text = "Total Quantity : " & FormatNumber(tmpQty, 0)

            lblSumTrans.Text = "No of Transaction(s) : " & SP_LIST_DAILY_INVOICEDataGridView.RowCount & " Row(s)"
        End If
    End Sub

    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        Dim RPV As New frmRepDailyInvoice
        RPV.ReportViewer1.ZoomPercent = 100
        RPV.WindowState = FormWindowState.Maximized
        RPV.Show()
    End Sub

End Class