﻿Public Class frmListDeliveryOrder
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        Me.SP_DELIVERY_ORDER_LISTTableAdapter.Fill(Me.DS_DELIVERY_SALE_ORDER.SP_DELIVERY_ORDER_LIST, DateTimePicker1.Value, DateTimePicker2.Value)
    End Sub

    Private Sub frmListDeliveryOrder_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DateTimePicker1.Value = DateSerial(Today.Year, Today.Month, 1)
        DateTimePicker2.Value = DateSerial(Today.Year, Today.Month + 1, 0)
    End Sub

    'Private Sub cmdPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrint.Click
    '    Dim RPV As New frmRepDeliveryOrder
    '    RPV.date1 = DateTimePicker1.Value
    '    RPV.date2 = DateTimePicker2.Value
    '    RPV.ReportViewer1.ZoomPercent = 100
    '    RPV.WindowState = FormWindowState.Maximized
    '    RPV.Show()
    'End Sub

End Class