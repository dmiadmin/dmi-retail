﻿Imports System.Data.SqlClient
Public Class frmSearchPaymentMethod
    Public PAYMENT_METHODBindingSource As New BindingSource

    Private Sub getdata()
        dt = New DataTable
        sqladapter = New SqlDataAdapter("SELECT PAYMENT_METHOD_ID, PAYMENT_METHOD_NAME, DESCRIPTION, EFFECTIVE_START_DATE, EFFECTIVE_END_DATE, USER_ID_INPUT, INPUT_DATE, USER_ID_UPDATE, UPDATE_DATE, PAYMENT_GROUP FROM PAYMENT_METHOD", xConn)
        sqladapter.Fill(dt)
        PAYMENT_METHODBindingSource.DataSource = dt
        PAYMENT_METHODBindingNavigator.BindingSource = PAYMENT_METHODBindingSource
        dgvSearchPaymentMethod.DataSource = PAYMENT_METHODBindingSource
        dgvSearchPaymentMethod.Columns("PAYMENT_METHOD_ID").Visible = False
        dgvSearchPaymentMethod.Columns("EFFECTIVE_START_DATE").Visible = False
        dgvSearchPaymentMethod.Columns("EFFECTIVE_END_DATE").Visible = False
        dgvSearchPaymentMethod.Columns("USER_ID_INPUT").Visible = False
        dgvSearchPaymentMethod.Columns("INPUT_DATE").Visible = False
        dgvSearchPaymentMethod.Columns("USER_ID_UPDATE").Visible = False
        dgvSearchPaymentMethod.Columns("UPDATE_DATE").Visible = False
    End Sub
    Private Sub frmSearchPaymentMethod_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_PAYMENT_METHOD.PAYMENT_METHOD' table. You can move, or remove it, as needed.
        'Me.PAYMENT_METHODTableAdapter.Fill(Me.DS_PAYMENT_METHOD.PAYMENT_METHOD)
        getdata()
        tmpSearchResult = ""
        txtPaymentMethod.Focus()

        If Language = "Indonesian" Then
            Me.Text = "Pencarian Metode pembayaran"
            lblPayment.Text = "Metode pembayaran"
            dgvSearchPaymentMethod.Columns("PAYMENT_METHOD_NAME").HeaderText = "Metode Pembayaran"
            dgvSearchPaymentMethod.Columns("PAYMENT_GROUP").HeaderText = "Group Pembayaran"
            dgvSearchPaymentMethod.Columns("DESCRIPTION").HeaderText = "Keterangan"
        Else
            dgvSearchPaymentMethod.Columns("PAYMENT_METHOD_NAME").HeaderText = "Payment Method Name"
            dgvSearchPaymentMethod.Columns("PAYMENT_GROUP").HeaderText = "Payment group"
            dgvSearchPaymentMethod.Columns("DESCRIPTION").HeaderText = "Description"
        End If
    End Sub

    Private Sub txtPaymentMethod_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPaymentMethod.TextChanged
        Try
            PAYMENT_METHODBindingSource.Filter = "PAYMENT_METHOD_NAME LIKE '%" & txtPaymentMethod.Text.ToUpper & "%'"
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtPaymentMethod.Text = ""
            txtPaymentMethod.Focus()
        End Try
    End Sub


    Private Sub dgvSearchPaymentMethod_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSearchPaymentMethod.DoubleClick
        Try
            tmpSearchResult = PAYMENT_METHODBindingSource.Current("PAYMENT_METHOD_ID")
        Catch
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data cara pembayaran !" & vbCrLf & _
                       "Silahkan input setidaknya satu data cara pembayaran !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no payment method data !" & vbCrLf & _
                       "Please input at least one payment method data !", MsgBoxStyle.Critical, "DMI Retail")
            End If
        End Try
        Me.Close()
    End Sub
End Class