﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frmListProductWarehouse
    Dim tmpCategory As Integer
    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter

    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
   
    'Private Sub frmListProductWarehouse_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
    '    If e.KeyCode = Keys.F12 Then
    '        cmdReport_Click(Nothing, Nothing)
    '    End If
    'End Sub

    Private Sub frmListProductWarehouse_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_PRODUCT_WAREHOUSE.SP_LIST_PRODUCT_GOODS' table. You can move, or remove it, as needed.
        'Me.SP_LIST_PRODUCT_GOODSTableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.SP_LIST_PRODUCT_GOODS, Now)
        'TODO: This line of code loads data into the 'DS_PRODUCT_WAREHOUSE.PRODUCT' table. You can move, or remove it, as needed.
        'Me.PRODUCTTableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.PRODUCT)
        If Language = "Indonesian" Then
            Me.Text = "Stock Gudang"
            Label1.Text = "Nama Produk"
            cmdGenerate.Text = "Proses"
            lblTotalQuantity.Text = "Jumlah Total : "
            cmdReport.Text = "Cetak"
            CheckBox1.Text = "Termasuk Gudang Barang Sisa"

            'SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("PRODUCT_CODE").HeaderText = "Kode Produk"
            'SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            'SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("WAREHOUSE_NAME").HeaderText = "Gudang"
            'SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("QUANTITY").HeaderText = "Jumlah"

        End If
        txtFilter.Enabled = False
        CheckBox1.Enabled = False
    End Sub

    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        If Not ValidateAllComboBox(Me) Then
            If Language = "Indonesian" Then
                MsgBox("Anda memasukkan nilai yang tidak terdaftar.", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You input value which is not in the list.", MsgBoxStyle.Critical, "DMI Retail")
            End If

            Exit Sub
        End If

        Cursor = Cursors.WaitCursor
        'SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_PRODUCT_WAREHOUSE.SP_PRODUCT_WAREHOUSE_DETAIL, _
        '0)

        If CheckBox1.Checked = True Then
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = "DECLARE	@return_value int" & vbCrLf & _
                  "EXEC	    @return_value = [dbo].[SP_PRODUCT_WAREHOUSE_DETAIL]" & vbCrLf & _
                  "		    @PRODUCT_ID = 0" & vbCrLf & _
                  "SELECT	'Return Value' = @return_value"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.SP_PRODUCT_WAREHOUSE_DETAILDataGridView.DataSource = dt
        Else
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = " SELECT PRODUCT_ID," & _
                        "  (SELECT PRODUCT_CODE FROM PRODUCT WHERE PRODUCT_ID = PRODUCT_WAREHOUSE.PRODUCT_ID) PRODUCT_CODE," & _
                        "  (SELECT PRODUCT_NAME FROM PRODUCT WHERE PRODUCT_ID = PRODUCT_WAREHOUSE.PRODUCT_ID) PRODUCT_NAME," & _
                        "  WAREHOUSE_ID," & _
                        "  (SELECT WAREHOUSE_NAME FROM WAREHOUSE WHERE WAREHOUSE_ID = PRODUCT_WAREHOUSE.WAREHOUSE_ID) WAREHOUSE_NAME," & _
                        "  SUM(QUANTITY) QUANTITY" & _
                        "  FROM PRODUCT_WAREHOUSE" & _
                        "  WHERE PRODUCT_ID IN (SELECT PRODUCT_ID FROM PRODUCT" & _
                                            "  WHERE CATEGORY = 1 ) AND" & _
                                " WAREHOUSE_ID <> -1" & _
                        "  GROUP BY PRODUCT_ID, WAREHOUSE_ID" & _
                        "  ORDER BY PRODUCT_NAME , WAREHOUSE_ID"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.SP_PRODUCT_WAREHOUSE_DETAILDataGridView.DataSource = dt
        End If

        Cursor = Cursors.Default

        Dim tmpQty As Integer
        For n As Integer = 0 To SP_PRODUCT_WAREHOUSE_DETAILDataGridView.RowCount - 1
            tmpQty = tmpQty + SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Item("QUANTITY", n).Value
            tmpQty = tmpQty
        Next
        If Language = "Indonesian" Then
            SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("PRODUCT_CODE").HeaderText = "Kode Produk"
            SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("WAREHOUSE_NAME").HeaderText = "Gudang"
            SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("QUANTITY").HeaderText = "Jumlah"
            SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("PRODUCT_ID").Visible = False
            SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("WAREHOUSE_ID").Visible = False
            SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("PRODUCT_NAME").Width = 300
            SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("QUANTITY").Width = 75
            SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("QUANTITY").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            lblTotalQuantity.Text = "Jumlah Total : " & tmpQty
        Else
            SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("PRODUCT_CODE").HeaderText = "Product Code"
            SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("PRODUCT_NAME").HeaderText = "Product Name"
            SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("WAREHOUSE_NAME").HeaderText = "Warehouse"
            SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("QUANTITY").HeaderText = "Quantity"
            SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("PRODUCT_ID").Visible = False
            SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("WAREHOUSE_ID").Visible = False
            SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("PRODUCT_NAME").Width = 300
            SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("QUANTITY").Width = 75
            SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Columns("QUANTITY").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            lblTotalQuantity.Text = "Total Quantity : " & tmpQty
        End If
        txtFilter.Enabled = True
        CheckBox1.Enabled = True
    End Sub


    Private Sub frmListProductWarehouse_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
        'If Me.Visible = True Then           ''''And SP_LIST_PRODUCT_GOODSBindingSource.Count < 1
        '    If Language = "Indonesian" Then
        '        MsgBox("Tidak ada Produk untuk diproses." & vbCrLf & _
        '           "Form ini akan ditutup!", MsgBoxStyle.Critical, "DMI Retail")
        '    Else
        '        MsgBox("There is no Product to be Processed." & vbCrLf & _
        '           "This form will be closed!", MsgBoxStyle.Critical, "DMI Retail")
        '    End If

        '    Me.Close()
        'End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = "DECLARE	@return_value int" & vbCrLf & _
                  "EXEC	    @return_value = [dbo].[SP_PRODUCT_WAREHOUSE_REPORT]" & vbCrLf & _
                  "		    @PRODUCT_NAME = N'" & txtFilter.Text & "'" & vbCrLf & _
                  "SELECT	'Return Value' = @return_value"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.SP_PRODUCT_WAREHOUSE_DETAILDataGridView.DataSource = dt
        Else
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = " SELECT PRODUCT_ID," & _
                        "  (SELECT PRODUCT_CODE FROM PRODUCT WHERE PRODUCT_ID = PRODUCT_WAREHOUSE.PRODUCT_ID) PRODUCT_CODE," & _
                        "  (SELECT PRODUCT_NAME FROM PRODUCT WHERE PRODUCT_ID = PRODUCT_WAREHOUSE.PRODUCT_ID) PRODUCT_NAME," & _
                        "  WAREHOUSE_ID," & _
                        "  (SELECT WAREHOUSE_NAME FROM WAREHOUSE WHERE WAREHOUSE_ID = PRODUCT_WAREHOUSE.WAREHOUSE_ID) WAREHOUSE_NAME," & _
                        "  SUM(QUANTITY) QUANTITY" & _
                        "  FROM PRODUCT_WAREHOUSE" & _
                        "  WHERE PRODUCT_ID IN (SELECT PRODUCT_ID FROM PRODUCT" & _
                                            "  WHERE CATEGORY = 1 ) AND" & _
                                " WAREHOUSE_ID <> -1" & _
                                " AND PRODUCT_ID IN (SELECT PRODUCT_ID FROM PRODUCT" & _
                                                    " WHERE PRODUCT_NAME LIKE '%" & txtFilter.Text & "%')" & _
                        "  GROUP BY PRODUCT_ID, WAREHOUSE_ID" & _
                        "  ORDER BY PRODUCT_NAME , WAREHOUSE_ID"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.SP_PRODUCT_WAREHOUSE_DETAILDataGridView.DataSource = dt
        End If

        Dim tmpQty As Integer
        For n As Integer = 0 To SP_PRODUCT_WAREHOUSE_DETAILDataGridView.RowCount - 1
            tmpQty = tmpQty + SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Item("QUANTITY", n).Value
            tmpQty = tmpQty
        Next
        If Language = "Indonesian" Then
            lblTotalQuantity.Text = "Jumlah Total : " & tmpQty
        Else
            lblTotalQuantity.Text = "Total Quantity : " & tmpQty
        End If
    End Sub

    Private Sub txtFilter_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFilter.TextChanged
        If CheckBox1.Checked = True Then
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = "DECLARE	@return_value int" & vbCrLf & _
                  "EXEC	    @return_value = [dbo].[SP_PRODUCT_WAREHOUSE_REPORT]" & vbCrLf & _
                  "		    @PRODUCT_NAME = N'" & txtFilter.Text & "'" & vbCrLf & _
                  "SELECT	'Return Value' = @return_value"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.SP_PRODUCT_WAREHOUSE_DETAILDataGridView.DataSource = dt
        Else
            Dim dt As New DataTable
            Call connection()
            Dim sql As String
            sql = " SELECT PRODUCT_ID," & _
                        "  (SELECT PRODUCT_CODE FROM PRODUCT WHERE PRODUCT_ID = PRODUCT_WAREHOUSE.PRODUCT_ID) PRODUCT_CODE," & _
                        "  (SELECT PRODUCT_NAME FROM PRODUCT WHERE PRODUCT_ID = PRODUCT_WAREHOUSE.PRODUCT_ID) PRODUCT_NAME," & _
                        "  WAREHOUSE_ID," & _
                        "  (SELECT WAREHOUSE_NAME FROM WAREHOUSE WHERE WAREHOUSE_ID = PRODUCT_WAREHOUSE.WAREHOUSE_ID) WAREHOUSE_NAME," & _
                        "  SUM(QUANTITY) QUANTITY" & _
                        "  FROM PRODUCT_WAREHOUSE" & _
                        "  WHERE PRODUCT_ID IN (SELECT PRODUCT_ID FROM PRODUCT" & _
                                            "  WHERE CATEGORY = 1 ) AND" & _
                                " WAREHOUSE_ID <> -1" & _
                                " AND PRODUCT_ID IN (SELECT PRODUCT_ID FROM PRODUCT" & _
                                                    " WHERE PRODUCT_NAME LIKE '%" & txtFilter.Text & "%')" & _
                        "  GROUP BY PRODUCT_ID, WAREHOUSE_ID" & _
                        "  ORDER BY PRODUCT_NAME , WAREHOUSE_ID"
            xAdoAdapter = New SqlDataAdapter(sql, xConn)
            xAdoAdapter.Fill(dt)
            Me.SP_PRODUCT_WAREHOUSE_DETAILDataGridView.DataSource = dt
        End If

        Dim tmpQty As Integer
        For n As Integer = 0 To SP_PRODUCT_WAREHOUSE_DETAILDataGridView.RowCount - 1
            tmpQty = tmpQty + SP_PRODUCT_WAREHOUSE_DETAILDataGridView.Item("QUANTITY", n).Value
            tmpQty = tmpQty
        Next
        If Language = "Indonesian" Then
            lblTotalQuantity.Text = "Jumlah Total : " & tmpQty
        Else
            lblTotalQuantity.Text = "Total Quantity : " & tmpQty
        End If
    End Sub
End Class