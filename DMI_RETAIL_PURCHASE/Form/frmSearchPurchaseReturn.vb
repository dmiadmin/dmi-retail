﻿Public Class frmSearchPurchaseReturn
    Public tmpVendorName, tmpReceiptNo, tmpPaymentMethhod As String
    Public tmpPurchaseDate As DateTime
    Public tmpPurchaseID, tmpTotal As Integer

    Private Sub frmSearchPurchaseReturn_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_RETURN_PURCHASE.SP_SEARCH_PURCHASE_RETURN' table. You can move, or remove it, as needed.
        Me.SP_SEARCH_PURCHASE_RETURNTableAdapter.Fill(Me.DS_RETURN_PURCHASE.SP_SEARCH_PURCHASE_RETURN)

        txtVendorName.Text = ""
        txtReceiptNo.Text = ""
        txtVendorName.Focus()

        If Language = "Indonesian" Then
            Me.Text = "Pencarian Retur Pembelian"

            lblReceipt.Text = "No Faktur"
            lblVendor.Text = "Nama Supplier"

            dgvSearcReturnPurchase.Columns("VENDOR_NAME").HeaderText = "Nama Supplier"
            dgvSearcReturnPurchase.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            dgvSearcReturnPurchase.Columns("PURCHASE_DATE").HeaderText = "Tgl Pembelian"
            dgvSearcReturnPurchase.Columns("PAYMENT_METHOD_NAME").HeaderText = "Cara Pembayaran"

        End If
    End Sub


    Private Sub txtVendorName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtVendorName.TextChanged
        Try
            txtReceiptNo.Text = ""
            SP_SEARCH_PURCHASE_RETURNBindingSource.Filter = "VENDOR_NAME LIKE '%" & txtVendorName.Text & "%'"
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtVendorName.Text = ""
            txtVendorName.Focus()
        End Try
    End Sub

   

    Private Sub dgvSearcReturnPurchase_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSearcReturnPurchase.DoubleClick
        Try
            tmpVendorName = SP_SEARCH_PURCHASE_RETURNBindingSource.Current("VENDOR_NAME")
            tmpReceiptNo = SP_SEARCH_PURCHASE_RETURNBindingSource.Current("RECEIPT_NO")
            tmpPaymentMethhod = SP_SEARCH_PURCHASE_RETURNBindingSource.Current("PAYMENT_METHOD_NAME")
            tmpPurchaseDate = SP_SEARCH_PURCHASE_RETURNBindingSource.Current("PURCHASE_DATE")
            tmpPurchaseID = SP_SEARCH_PURCHASE_RETURNBindingSource.Current("PURCHASE_ID")
            tmpTotal = SP_SEARCH_PURCHASE_RETURNBindingSource.Current("TOTAL")
        Catch
            If Language = "Indonesian" Then
                MsgBox("Tidak ada data Retur Pembelian !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("There is no Purchase Return data!", MsgBoxStyle.Critical, "DMI Retail")
            End If
        End Try

        Me.Close()

    End Sub

    Private Sub txtReceiptNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtReceiptNo.TextChanged
        Try
            txtVendorName.Text = ""
            SP_SEARCH_PURCHASE_RETURNBindingSource.Filter = "RECEIPT_NO LIKE '%" & txtReceiptNo.Text & "%'"
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtReceiptNo.Text = ""
            txtReceiptNo.Focus()
        End Try
    End Sub

    Private Sub dgvSearcReturnPurchase_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvSearcReturnPurchase.KeyDown
        If e.KeyCode = Keys.Enter Then
            Try
                tmpVendorName = SP_SEARCH_PURCHASE_RETURNBindingSource.Current("VENDOR_NAME")
                tmpReceiptNo = SP_SEARCH_PURCHASE_RETURNBindingSource.Current("RECEIPT_NO")
                tmpPaymentMethhod = SP_SEARCH_PURCHASE_RETURNBindingSource.Current("PAYMENT_METHOD_NAME")
                tmpPurchaseDate = SP_SEARCH_PURCHASE_RETURNBindingSource.Current("PURCHASE_DATE")
                tmpPurchaseID = SP_SEARCH_PURCHASE_RETURNBindingSource.Current("PURCHASE_ID")
                tmpTotal = SP_SEARCH_PURCHASE_RETURNBindingSource.Current("TOTAL")
            Catch
                If Language = "Indonesian" Then
                    MsgBox("Tidak ada data Retur Pembelian !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("There is no Purchase Return data!", MsgBoxStyle.Critical, "DMI Retail")
                End If
            End Try

        ElseIf e.KeyCode = Keys.Escape Then
            tmpVendorName = ""
            tmpReceiptNo = ""
            tmpPaymentMethhod = ""
            tmpPurchaseDate = ""
            tmpPurchaseID = ""
            tmpTotal = ""
        End If

        Me.Close()
    End Sub
End Class