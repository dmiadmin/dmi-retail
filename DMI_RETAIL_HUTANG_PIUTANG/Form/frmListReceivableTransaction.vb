﻿Imports System.Data.SqlClient
Imports System.Data

Public Class frmListReceivableTransaction
    Dim xComm As New SqlCommand
    Dim xAdoAdapter As New SqlDataAdapter

    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub

    'Private Sub frmListReceivableTransaction_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
    '    If e.KeyCode = Keys.F12 Then
    '        cmdReport_Click(Nothing, Nothing)
    '    End If
    'End Sub

    Private Sub frmListReceivableTransaction_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Language = "Indonesian" Then
            cmdGenerate.Text = "Proses"
            cmdReport.Text = "Cetak"

            Label1.Text = "Jumlah Transaksi = " & SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.RowCount
        Else
            Label1.Text = "No of Transaction(s) = " & SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.RowCount
        End If
    End Sub

    Private Sub cmdGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        Me.Cursor = Cursors.WaitCursor
        Dim dt As New DataTable
        Dim sql As String
        sql = "DECLARE	@return_value int" & vbCrLf & _
              "EXEC	    @return_value = [dbo].[SP_LIST_RECEIVABLE_TRANSACTION]" & vbCrLf & _
              "		    @PERIOD = N'" & dtpPeriod.Value & "'" & vbCrLf & _
              "SELECT	'Return Value' = @return_value"
        xAdoAdapter = New SqlDataAdapter(sql, xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        xAdoAdapter.Fill(dt)
        Me.SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.DataSource = dt
        If Language = "Indonesian" Then
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("RECEIVABLE_TRANSACTION_ID").Visible = False
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("PERIOD").Visible = False
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("RECEIVABLE_TRANSACTION_NUMBER").HeaderText = "No Transaksi"
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("ACCOUNT_NAME").HeaderText = "Nama Akun"
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("TRANSACTION_DATE").HeaderText = "Tgl Transaksi"
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("TRANSACTION_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("TRANSACTION_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("AMOUNT").HeaderText = "Jumlah"
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("AMOUNT").DefaultCellStyle.Format = "n0"
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("DESCRIPTION").HeaderText = "Keterangan"
        Else
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("RECEIVABLE_TRANSACTION_ID").Visible = False
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("PERIOD").Visible = False
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("RECEIVABLE_TRANSACTION_NUMBER").HeaderText = "Transaction No"
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("ACCOUNT_NAME").HeaderText = "Account Name"
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("TRANSACTION_DATE").HeaderText = "Transaction Date"
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("TRANSACTION_DATE").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("TRANSACTION_DATE").DefaultCellStyle.Format = "dd-MMM-yyyy"
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("AMOUNT").HeaderText = "Amount"
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("AMOUNT").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("AMOUNT").DefaultCellStyle.Format = "n0"
            SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.Columns("DESCRIPTION").HeaderText = "Description"

            Label1.Text = "No of Transaction(s) = " & SP_LIST_RECEIVABLE_TRANSACTIONDataGridView.RowCount
        End If
        Me.Cursor = Cursors.Default
    End Sub

    'Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
    '    Dim RPV As New frmRepReceivableTrans
    '    RPV.period = dtpPeriod.Value
    '    RPV.ReportViewer1.ShowRefreshButton = False
    '    RPV.ReportViewer1.ZoomPercent = 100
    '    RPV.WindowState = FormWindowState.Maximized
    '    RPV.Show()
    'End Sub

End Class