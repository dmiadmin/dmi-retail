﻿Imports System.IO
Public Class frmRestore
    Dim tmpUpdate(), dmiRestore As String
    Dim Path_Name As String = Application.StartupPath
    Dim Path_Data As String = Replace(Path_Name, Mid(Path_Name, 51, 71), "")
    Dim PathDb As String = Path_Data & "\DMI_RETAIL\bin\Debug\DB_DMI_RETAIL.mdf"
    Dim pathConfig As String = Path_Data & "\DMI_RETAIL\bin\Debug\config.inf"

    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        SaveFileDialog1.Filter = "Database Backup File|*.mdf"
        SaveFileDialog1.FileName = ""
        SaveFileDialog1.ShowDialog()
        txtPathRestore.Text = SaveFileDialog1.FileName
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click

        ' My.Computer.FileSystem.RenameFile(dmiRestore, "DB_DMI_RETAIL.mdf")
        Try
            File.Delete(Application.StartupPath & "\DB_DMI_RETAIL.mdf")
            System.IO.File.Copy(txtPathRestore.Text, _
                          Application.StartupPath & "\DB_DMI_RETAIL.mdf")
            MsgBox("Restore Successed.", MsgBoxStyle.Information, "DMI Retail")
        Catch
            MsgBox("Restore Failed.", MsgBoxStyle.Information, "DMI Retail")
        End Try
        Me.Close()
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        Me.Close()
    End Sub
End Class