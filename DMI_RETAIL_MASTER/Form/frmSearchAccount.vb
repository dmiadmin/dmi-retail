﻿Imports System.Data.SqlClient

Public Class frmSearchAccount
    Public ACCOUNTBindingSource As New BindingSource
    Dim tmpAccountType As String
    Public VIEW_RECEIVABLE_ACCOUNTBindingSource As New BindingSource
    Public VIEW_PAYABLE_ACCOUNTBindingSource As New BindingSource

    Private Sub GetData()
        sql = "SELECT ACCOUNT_ID, ACCOUNT_NUMBER, ACCOUNT_NAME  FROM ACCOUNT"
        sqladapter = New SqlDataAdapter(sql, xConn)
        dt = New DataTable
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        ACCOUNTBindingSource.DataSource = dt
        VIEW_PAYABLE_ACCOUNTBindingNavigator.BindingSource = ACCOUNTBindingSource
        dgvSearchAccount.DataSource = ACCOUNTBindingSource
        dgvSearchAccount.Columns("ACCOUNT_ID").Visible = False

    End Sub

    Private Sub GetPayable()
        sql = "SELECT ACCOUNT_ID, ACCOUNT_NUMBER, ACCOUNT_NAME FROM dbo.VIEW_PAYABLE_ACCOUNT"
        sqladapter = New SqlDataAdapter(sql, xConn)
        dt = New DataTable
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        VIEW_PAYABLE_ACCOUNTBindingSource.DataSource = dt
    End Sub

    Private Sub GetReceivable()
        sql = "SELECT ACCOUNT_ID, ACCOUNT_NUMBER, ACCOUNT_NAME FROM dbo.VIEW_RECEIVABLE_ACCOUNT"
        sqladapter = New SqlDataAdapter(sql, xConn)
        dt = New DataTable
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        VIEW_RECEIVABLE_ACCOUNTBindingSource.DataSource = dt
    End Sub

    Private Sub frmSearchAccount_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_ACCOUNT.ACCOUNT' table. You can move, or remove it, as needed.
        'Me.ACCOUNTTableAdapter.Fill(Me.DS_ACCOUNT.ACCOUNT)
        'TODO: This line of code loads data into the 'DS_ACCOUNT.VIEW_RECEIVABLE_ACCOUNT' table. You can move, or remove it, as needed.
        'Me.VIEW_RECEIVABLE_ACCOUNTTableAdapter.Fill(Me.DS_ACCOUNT.VIEW_RECEIVABLE_ACCOUNT)
        'TODO: This line of code loads data into the 'DS_ACCOUNT.VIEW_PAYABLE_ACCOUNT' table. You can move, or remove it, as needed.
        'Me.VIEW_PAYABLE_ACCOUNTTableAdapter.Fill(Me.DS_ACCOUNT.VIEW_PAYABLE_ACCOUNT)
        GetData()
        GetPayable()
        GetReceivable()
        tmpSearchResult = ""

        If mdlGeneral.tmpSearchMode = "PayableAcc-AccountName" Then
            tmpAccountType = "Payable"
        ElseIf mdlGeneral.tmpSearchMode = "ReceivableAcc-AccountName" Then
            tmpAccountType = "Receivable"
        ElseIf mdlGeneral.tmpSearchMode = "Account-AccountName" Then
            tmpAccountType = "AllAccount"
        End If

        dgvSearchAccount.Columns(0).Visible = False

        If tmpAccountType = "Payable" Then
            dgvSearchAccount.DataSource = VIEW_PAYABLE_ACCOUNTBindingSource
            VIEW_PAYABLE_ACCOUNTBindingNavigator.BindingSource = VIEW_PAYABLE_ACCOUNTBindingSource
            Label1.Visible = True
            If Language = "Indonesian" Then
                Label1.Text = "Hutang"
            Else
                Label1.Text = "Payable"
            End If

        ElseIf tmpAccountType = "Receivable" Then
            dgvSearchAccount.DataSource = VIEW_RECEIVABLE_ACCOUNTBindingSource
            VIEW_PAYABLE_ACCOUNTBindingNavigator.BindingSource = VIEW_RECEIVABLE_ACCOUNTBindingSource
            Label1.Visible = True
            If Language = "Indonesian" Then
                Label1.Text = "Piutang"
            Else
                Label1.Text = "Receivable"
            End If

        ElseIf tmpAccountType = "AllAccount" Then
            dgvSearchAccount.DataSource = ACCOUNTBindingSource
            VIEW_PAYABLE_ACCOUNTBindingNavigator.BindingSource = ACCOUNTBindingSource
            Label1.Visible = True
            If Language = "Indonesian" Then
                Label1.Text = "Semua Akun"
            Else
                Label1.Text = "All Account"
            End If
        End If

        If Language = "Indonesian" Then
            Me.Text = "Pencarian Akun"

            lblAccountName.Text = "Nama Akun :"

            dgvSearchAccount.Columns("ACCOUNT_NUMBER").HeaderText = "No. Akun"
            dgvSearchAccount.Columns("ACCOUNT_NAME").HeaderText = "Nama Akun"
        Else
            dgvSearchAccount.Columns("ACCOUNT_NUMBER").HeaderText = "Account Number"
            dgvSearchAccount.Columns("ACCOUNT_NAME").HeaderText = "Account Name"
        End If

    End Sub

    Private Sub dgvSearchAccount_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSearchAccount.DoubleClick
        If tmpAccountType = "Payable" Then
            Try
                tmpSearchResult = VIEW_PAYABLE_ACCOUNTBindingSource.Current("ACCOUNT_ID")
            Catch
                If Language = "Indonesian" Then
                    MsgBox("Tidak ada data Akun Hutang !" & vbCrLf & _
                           "Silahkan input data Akun Hutang terlebih dahulu !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("There is no Payable Account !" & vbCrLf & _
                           "Please input at least one Payable Account Data !", MsgBoxStyle.Critical, "DMI Retail")
                End If
            End Try
            Me.Close()

        ElseIf tmpAccountType = "Receivable" Then
            Try
                tmpSearchResult = VIEW_RECEIVABLE_ACCOUNTBindingSource.Current("ACCOUNT_ID")
            Catch
                If Language = "Indonesian" Then
                    MsgBox("Tidak ada data Akun Piutang !" & vbCrLf & _
                           "Silahkan input data Akun Piutang terlebih dahulu !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("There is no Receivable Account !" & vbCrLf & _
                           "Please input at least one Receivable Account Data !", MsgBoxStyle.Critical, "DMI Retail")
                End If
            End Try
            Me.Close()

        ElseIf tmpAccountType = "AllAccount" Then
            Try
                tmpSearchResult = ACCOUNTBindingSource.Current("ACCOUNT_ID")
            Catch
                If Language = "Indonesian" Then
                    MsgBox("Tidak ada data Akun !" & vbCrLf & _
                           "Silahkan input setidaknya satu data Akun !", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("There is no Account exists !" & vbCrLf & _
                           "Please input at least one Account Data !", MsgBoxStyle.Critical, "DMI Retail")
                End If
            End Try
            Me.Close()

        End If
    End Sub

    Private Sub dgvSearchAccount_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvSearchAccount.KeyDown
        If tmpAccountType = "Payable" Then
            If e.KeyCode = Keys.Enter Then
                Try
                    tmpSearchResult = VIEW_PAYABLE_ACCOUNTBindingSource.Current("ACCOUNT_ID")
                Catch
                    If Language = "Indonesian" Then
                        MsgBox("Tidak ada data Akun Hutang !" & vbCrLf & _
                               "Silahkan input data Akun Hutang terlebih dahulu !", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("There is no Payable Account !" & vbCrLf & _
                               "Please input at least one Payable Account Data !", MsgBoxStyle.Critical, "DMI Retail")
                    End If
                End Try

                Me.Close()

            ElseIf e.KeyCode = Keys.Escape Then
                tmpSearchResult = ""
                Me.Close()
            End If

        ElseIf tmpAccountType = "Receivable" Then
            If e.KeyCode = Keys.Enter Then
                Try
                    tmpSearchResult = VIEW_RECEIVABLE_ACCOUNTBindingSource.Current("ACCOUNT_ID")
                Catch
                    If Language = "Indonesian" Then
                        MsgBox("Tidak ada data Akun Piutang !" & vbCrLf & _
                               "Silahkan input data Akun Piutang terlebih dahulu !", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("There is no Receivable Account !" & vbCrLf & _
                               "Please input at least one Receivable Account Data !", MsgBoxStyle.Critical, "DMI Retail")
                    End If
                End Try

                Me.Close()

            ElseIf e.KeyCode = Keys.Escape Then
                tmpSearchResult = ""
                Me.Close()
            End If

        ElseIf tmpAccountType = "AllAccount" Then
            If e.KeyCode = Keys.Enter Then
                Try
                    tmpSearchResult = ACCOUNTBindingSource.Current("ACCOUNT_ID")
                Catch
                    If Language = "Indonesian" Then
                        MsgBox("Tidak ada data Akun !" & vbCrLf & _
                               "Silahkan input setidaknya satu data Akun !", MsgBoxStyle.Critical, "DMI Retail")
                    Else
                        MsgBox("There is no Account exists !" & vbCrLf & _
                               "Please input at least one Account Data !", MsgBoxStyle.Critical, "DMI Retail")
                    End If
                End Try

                Me.Close()

            ElseIf e.KeyCode = Keys.Escape Then
                tmpSearchResult = ""
                Me.Close()
            End If
        End If
    End Sub

    Private Sub ACCOUNT_NAMETextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ACCOUNT_NAMETextBox.TextChanged
        Try
            If tmpAccountType = "Payable" Then VIEW_PAYABLE_ACCOUNTBindingSource.Filter = "ACCOUNT_NAME LIKE '%" & ACCOUNT_NAMETextBox.Text.ToUpper & "%'"

            If tmpAccountType = "Receivable" Then VIEW_RECEIVABLE_ACCOUNTBindingSource.Filter = "ACCOUNT_NAME LIKE '%" & ACCOUNT_NAMETextBox.Text.ToUpper & "%'"

            If tmpAccountType = "AllAccount" Then ACCOUNTBindingSource.Filter = "ACCOUNT_NAME LIKE '%" & ACCOUNT_NAMETextBox.Text.ToUpper & "%'"
        Catch
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            ACCOUNT_NAMETextBox.Text = ""
            ACCOUNT_NAMETextBox.Focus()
        End Try
    End Sub

    Private Sub ACCOUNT_NAMETextBox_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ACCOUNT_NAMETextBox.KeyUp
        If e.KeyCode = Keys.Enter Then
            dgvSearchAccount.Focus()
            Exit Sub
        ElseIf e.KeyCode = Keys.Escape Then
            tmpSearchResult = ""
            Me.Close()
        End If
    End Sub

    Private Sub frmSearchAccount_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
        ACCOUNT_NAMETextBox.Focus()
    End Sub

End Class