﻿Public Class frmSearchDeliveryOrder


    Private Sub frmSearchDeliveryOrder_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_DELIVERY_SALE_ORDER.SP_SEARCH_DELIVERY_ORDER' table. You can move, or remove it, as needed.
        Me.SP_SEARCH_DELIVERY_ORDERTableAdapter.Fill(Me.DS_DELIVERY_SALE_ORDER.SP_SEARCH_DELIVERY_ORDER)

    End Sub

    Private Sub txtCustomer_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCustomer.TextChanged
        Try
            txtInvoiceNo.Text = ""
            SP_SEARCH_DELIVERY_ORDERBindingSource.Filter = "CUSTOMER_NAME LIKE '%" & txtCustomer.Text.ToUpper & "%'"
        Catch ex As Exception
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtCustomer.Text = ""
            txtCustomer.Focus()
        End Try
    End Sub

    Private Sub txtInvoiceNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInvoiceNo.TextChanged
        Try
            txtCustomer.Text = ""
            SP_SEARCH_DELIVERY_ORDERBindingSource.Filter = "RECEIPT_NO LIKE '%" & txtInvoiceNo.Text.ToUpper & "%'"
        Catch ex As Exception
            If Language = "Indonesian" Then
                MsgBox("Anda salah memasukkan huruf !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Wrong Input Character !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            txtInvoiceNo.Text = ""
            txtInvoiceNo.Focus()
        End Try
    End Sub

    Private Sub dgvSearchDO_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSearchDO.DoubleClick
        Try
            
            Me.Close()
        Catch
        End Try


    End Sub
End Class