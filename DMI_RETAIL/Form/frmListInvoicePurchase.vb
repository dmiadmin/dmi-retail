﻿Public Class frmListInvoicePurchase


    Private Sub cmdGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenerate.Click
        Me.Cursor = Cursors.WaitCursor
        Me.Cursor = Cursors.Default

        If Language = "Indonesian" Then

            SP_GET_INVOICE_PURCHASEDataGridView.Columns("PURCHASE_DATE").HeaderText = "Tgl Pembelian"
            SP_GET_INVOICE_PURCHASEDataGridView.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            SP_GET_INVOICE_PURCHASEDataGridView.Columns("VENDOR_NAME").HeaderText = "Nama Supplier"
            SP_GET_INVOICE_PURCHASEDataGridView.Columns("MATURITY_DATE").HeaderText = "Tgl Jatuh Tempo"
            SP_GET_INVOICE_PURCHASEDataGridView.Columns("BALANCE").HeaderText = "Saldo"

        End If
    End Sub

    Private Sub frmListInvoicePurchase_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Language = "Indonesian" Then
            Me.Text = "Daftar Faktur Pembelian"
            cmdGenerate.Text = "Proses"

            SP_GET_INVOICE_PURCHASEDataGridView.Columns("PURCHASE_DATE").HeaderText = "Tgl Pembelian"
            SP_GET_INVOICE_PURCHASEDataGridView.Columns("RECEIPT_NO").HeaderText = "No Faktur"
            SP_GET_INVOICE_PURCHASEDataGridView.Columns("VENDOR_NAME").HeaderText = "Nama Supplier"
            SP_GET_INVOICE_PURCHASEDataGridView.Columns("MATURITY_DATE").HeaderText = "Tgl Jatuh Tempo"
            SP_GET_INVOICE_PURCHASEDataGridView.Columns("BALANCE").HeaderText = "Saldo"
        End If
    End Sub
End Class