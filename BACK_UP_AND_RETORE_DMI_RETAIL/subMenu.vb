﻿Public Class subMenu
    Dim frmTemporary As Form
    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        If Not frmTemporary Is Nothing Then frmTemporary.Close()
        frmDataBackUp.MdiParent = Me
        frmDataBackUp.Show()
        frmTemporary = frmDataBackUp
    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        If Not frmTemporary Is Nothing Then frmTemporary.Close()
        frmRestore.MdiParent = Me
        frmRestore.Show()
        frmTemporary = frmRestore
    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        If Not frmTemporary Is Nothing Then frmTemporary.Close()
        frmDeleteTransaction.MdiParent = Me
        frmDeleteTransaction.Show()
        frmTemporary = frmDeleteTransaction
    End Sub

    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click
        If Not frmTemporary Is Nothing Then frmTemporary.Close()
        frmShrink.MdiParent = Me
        frmShrink.Show()
        frmTemporary = frmShrink
    End Sub

    Private Sub ToolStripLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripLabel1.Click
        If Not frmTemporary Is Nothing Then frmTemporary.Close()
        frmViewData.MdiParent = Me
        frmViewData.Show()
        frmTemporary = frmViewData
    End Sub

    Private Sub subMenu_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.PageUp Then
            ToolStripLabel1.Visible = True
        ElseIf e.KeyCode = Keys.PageDown Then
            ToolStripLabel1.Visible = False
            Me.MaximizeBox = False
        End If
    End Sub

    Private Sub subMenu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class