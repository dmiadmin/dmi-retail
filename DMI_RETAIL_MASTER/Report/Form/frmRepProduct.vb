﻿Imports System.Data.SqlClient

Public Class frmRepProduct
    Private Sub frmRepProduct_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dt = New DataTable
        sql = "SELECT (SELECT COMPANY_NAME FROM COMPANY_INFORMATION)COMPANY_NAME," & _
                                        "(SELECT ADDRESS_LINE_1 FROM COMPANY_INFORMATION)ADDRESS_LINE_1," & _
                                        "(SELECT ADDRESS_LINE_3 FROM COMPANY_INFORMATION)ADDRESS_LINE_3," & _
                                        "(SELECT REGENCY FROM COMPANY_INFORMATION)REGENCY," & _
                                        "(SELECT PHONE_1 FROM COMPANY_INFORMATION)PHONE_1," & _
                                        "(SELECT FAX FROM COMPANY_INFORMATION)FAX," & _
                                        "PRODUCT_ID ,PRODUCT_CODE ,PRODUCT_NAME ,[DESCRIPTION],SELLING_PRICE ," & _
                                        "(SELECT LOCATION_NAME FROM LOCATION WHERE LOCATION_ID = PRODUCT .LOCATION_ID ) LOCATION_NAME," & _
                                        "SAFE_STOCK ," & _
                                        "CATEGORY = CASE CATEGORY " & _
                                        "WHEN 0 THEN 'SERVICES' " & _
                                        "WHEN 1 THEN 'PRODUCT' " & _
                                        "WHEN 2 THEN 'NON STOCK' " & _
                                        "END," & _
                                        "EFFECTIVE_START_DATE ," & _
                                        "EFFECTIVE_END_DATE FROM dbo.PRODUCT ORDER BY PRODUCT_NAME"
        sqladapter = New SqlDataAdapter(sql, xConn)
        If xConn.State = ConnectionState.Open Then xConn.Close() : xConn.Open()
        sqladapter.Fill(dt)
        Dim rpt As New rptProduct
        rpt.SetDataSource(dt)
        CrystalReportViewer1.ReportSource = rpt
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        CrystalReportViewer1.Refresh()
    End Sub
End Class