﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListInvoiceSale
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListInvoiceSale))
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SP_GET_INVOICE_SALEDataGridView = New System.Windows.Forms.DataGridView()
        Me.SP_GET_INVOICE_SALEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.dtpPeriod = New System.Windows.Forms.DateTimePicker()
        Me.cmdGenerate = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.SALE_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RECEIPT_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CUSTOMER_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BALANCE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.SP_GET_INVOICE_SALEDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_GET_INVOICE_SALEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'SP_GET_INVOICE_SALEDataGridView
        '
        Me.SP_GET_INVOICE_SALEDataGridView.AllowUserToAddRows = False
        Me.SP_GET_INVOICE_SALEDataGridView.AllowUserToDeleteRows = False
        Me.SP_GET_INVOICE_SALEDataGridView.AutoGenerateColumns = False
        Me.SP_GET_INVOICE_SALEDataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SP_GET_INVOICE_SALEDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.SP_GET_INVOICE_SALEDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.SP_GET_INVOICE_SALEDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SALE_DATE, Me.RECEIPT_NO, Me.CUSTOMER_NAME, Me.BALANCE})
        Me.SP_GET_INVOICE_SALEDataGridView.DataSource = Me.SP_GET_INVOICE_SALEBindingSource
        Me.SP_GET_INVOICE_SALEDataGridView.Location = New System.Drawing.Point(6, 21)
        Me.SP_GET_INVOICE_SALEDataGridView.Name = "SP_GET_INVOICE_SALEDataGridView"
        Me.SP_GET_INVOICE_SALEDataGridView.ReadOnly = True
        Me.SP_GET_INVOICE_SALEDataGridView.RowHeadersWidth = 32
        Me.SP_GET_INVOICE_SALEDataGridView.Size = New System.Drawing.Size(594, 255)
        Me.SP_GET_INVOICE_SALEDataGridView.TabIndex = 1
        '
        'SP_GET_INVOICE_SALEBindingSource
        '
        Me.SP_GET_INVOICE_SALEBindingSource.DataMember = "SP_GET_INVOICE_SALE"
        '
        'dtpPeriod
        '
        Me.dtpPeriod.CustomFormat = "MMMM- yyyy"
        Me.dtpPeriod.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPeriod.Location = New System.Drawing.Point(231, 18)
        Me.dtpPeriod.Name = "dtpPeriod"
        Me.dtpPeriod.ShowUpDown = True
        Me.dtpPeriod.Size = New System.Drawing.Size(146, 22)
        Me.dtpPeriod.TabIndex = 2
        '
        'SP_GET_INVOICE_SALETableAdapter
        '
        '
        'TableAdapterManager
        '
        '
        'cmdGenerate
        '
        Me.cmdGenerate.Image = CType(resources.GetObject("cmdGenerate.Image"), System.Drawing.Image)
        Me.cmdGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdGenerate.Location = New System.Drawing.Point(254, 51)
        Me.cmdGenerate.Name = "cmdGenerate"
        Me.cmdGenerate.Size = New System.Drawing.Size(99, 39)
        Me.cmdGenerate.TabIndex = 3
        Me.cmdGenerate.Text = "Generate"
        Me.cmdGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdGenerate.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmdGenerate)
        Me.GroupBox1.Controls.Add(Me.dtpPeriod)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(606, 106)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.SP_GET_INVOICE_SALEDataGridView)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 124)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(606, 282)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        '
        'SALE_DATE
        '
        Me.SALE_DATE.DataPropertyName = "SALE_DATE"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "dd-MMM-yyyy"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.SALE_DATE.DefaultCellStyle = DataGridViewCellStyle2
        Me.SALE_DATE.HeaderText = "Sale Date"
        Me.SALE_DATE.Name = "SALE_DATE"
        Me.SALE_DATE.ReadOnly = True
        Me.SALE_DATE.Width = 110
        '
        'RECEIPT_NO
        '
        Me.RECEIPT_NO.DataPropertyName = "RECEIPT_NO"
        Me.RECEIPT_NO.HeaderText = "Receipt No"
        Me.RECEIPT_NO.Name = "RECEIPT_NO"
        Me.RECEIPT_NO.ReadOnly = True
        Me.RECEIPT_NO.Width = 110
        '
        'CUSTOMER_NAME
        '
        Me.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME"
        Me.CUSTOMER_NAME.HeaderText = "Customer Name"
        Me.CUSTOMER_NAME.Name = "CUSTOMER_NAME"
        Me.CUSTOMER_NAME.ReadOnly = True
        Me.CUSTOMER_NAME.Width = 150
        '
        'BALANCE
        '
        Me.BALANCE.DataPropertyName = "BALANCE"
        Me.BALANCE.HeaderText = "Balance"
        Me.BALANCE.Name = "BALANCE"
        Me.BALANCE.ReadOnly = True
        '
        'frmListInvoiceSale
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(630, 418)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmListInvoiceSale"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "List Invoice Sale"
        CType(Me.SP_GET_INVOICE_SALEDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_GET_INVOICE_SALEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SP_GET_INVOICE_SALEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_GET_INVOICE_SALEDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dtpPeriod As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmdGenerate As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents SALE_DATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RECEIPT_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CUSTOMER_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BALANCE As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
