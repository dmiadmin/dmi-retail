﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListGroupingProduct
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListGroupingProduct))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DS_GROUPING_PRODUCT = New DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCT()
        Me.SP_LIST_GROUPING_PRODUCTBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SP_LIST_GROUPING_PRODUCTTableAdapter = New DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.SP_LIST_GROUPING_PRODUCTTableAdapter()
        Me.TableAdapterManager = New DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.TableAdapterManager()
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.dgvListGroupingProduct = New System.Windows.Forms.DataGridView()
        Me.PARENT_PRODUCT_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PARENT_PRODUCT_CODE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PARENT_PRODUCT_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CHILD_PRODUCT_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CHILD_PRODUCT_CODE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CHILD_PRODUCT_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QUANTITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblProductName = New System.Windows.Forms.Label()
        Me.lblProductCode = New System.Windows.Forms.Label()
        Me.txtProductCode = New System.Windows.Forms.TextBox()
        Me.txtProductName = New System.Windows.Forms.TextBox()
        Me.lblNoOfGrouping = New System.Windows.Forms.Label()
        Me.cmdReport = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        CType(Me.DS_GROUPING_PRODUCT, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_LIST_GROUPING_PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SP_LIST_GROUPING_PRODUCTBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator.SuspendLayout()
        CType(Me.dgvListGroupingProduct, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'DS_GROUPING_PRODUCT
        '
        Me.DS_GROUPING_PRODUCT.DataSetName = "DS_GROUPING_PRODUCT"
        Me.DS_GROUPING_PRODUCT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SP_LIST_GROUPING_PRODUCTBindingSource
        '
        Me.SP_LIST_GROUPING_PRODUCTBindingSource.DataMember = "SP_LIST_GROUPING_PRODUCT"
        Me.SP_LIST_GROUPING_PRODUCTBindingSource.DataSource = Me.DS_GROUPING_PRODUCT
        '
        'SP_LIST_GROUPING_PRODUCTTableAdapter
        '
        Me.SP_LIST_GROUPING_PRODUCTTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.GROUPING_PRODUCTTableAdapter = Nothing
        Me.TableAdapterManager.PRODUCT_UNITTableAdapter = Nothing
        Me.TableAdapterManager.PRODUCT1TableAdapter = Nothing
        Me.TableAdapterManager.PRODUCTTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'SP_LIST_GROUPING_PRODUCTBindingNavigator
        '
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator.AddNewItem = Nothing
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator.BindingSource = Me.SP_LIST_GROUPING_PRODUCTBindingSource
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator.DeleteItem = Nothing
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator.Name = "SP_LIST_GROUPING_PRODUCTBindingNavigator"
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator.Size = New System.Drawing.Size(625, 25)
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator.TabIndex = 0
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'dgvListGroupingProduct
        '
        Me.dgvListGroupingProduct.AllowUserToAddRows = False
        Me.dgvListGroupingProduct.AllowUserToDeleteRows = False
        Me.dgvListGroupingProduct.AutoGenerateColumns = False
        Me.dgvListGroupingProduct.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvListGroupingProduct.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvListGroupingProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvListGroupingProduct.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PARENT_PRODUCT_ID, Me.PARENT_PRODUCT_CODE, Me.PARENT_PRODUCT_NAME, Me.CHILD_PRODUCT_ID, Me.CHILD_PRODUCT_CODE, Me.CHILD_PRODUCT_NAME, Me.QUANTITY})
        Me.dgvListGroupingProduct.DataSource = Me.SP_LIST_GROUPING_PRODUCTBindingSource
        Me.dgvListGroupingProduct.Location = New System.Drawing.Point(12, 117)
        Me.dgvListGroupingProduct.Name = "dgvListGroupingProduct"
        Me.dgvListGroupingProduct.ReadOnly = True
        Me.dgvListGroupingProduct.RowHeadersWidth = 24
        Me.dgvListGroupingProduct.Size = New System.Drawing.Size(601, 236)
        Me.dgvListGroupingProduct.TabIndex = 1
        '
        'PARENT_PRODUCT_ID
        '
        Me.PARENT_PRODUCT_ID.DataPropertyName = "PARENT_PRODUCT_ID"
        Me.PARENT_PRODUCT_ID.HeaderText = "PARENT_PRODUCT_ID"
        Me.PARENT_PRODUCT_ID.Name = "PARENT_PRODUCT_ID"
        Me.PARENT_PRODUCT_ID.ReadOnly = True
        Me.PARENT_PRODUCT_ID.Visible = False
        '
        'PARENT_PRODUCT_CODE
        '
        Me.PARENT_PRODUCT_CODE.DataPropertyName = "PARENT_PRODUCT_CODE"
        Me.PARENT_PRODUCT_CODE.HeaderText = "Parent Code"
        Me.PARENT_PRODUCT_CODE.Name = "PARENT_PRODUCT_CODE"
        Me.PARENT_PRODUCT_CODE.ReadOnly = True
        Me.PARENT_PRODUCT_CODE.Width = 110
        '
        'PARENT_PRODUCT_NAME
        '
        Me.PARENT_PRODUCT_NAME.DataPropertyName = "PARENT_PRODUCT_NAME"
        Me.PARENT_PRODUCT_NAME.HeaderText = "Parent Product Name"
        Me.PARENT_PRODUCT_NAME.Name = "PARENT_PRODUCT_NAME"
        Me.PARENT_PRODUCT_NAME.ReadOnly = True
        Me.PARENT_PRODUCT_NAME.Width = 140
        '
        'CHILD_PRODUCT_ID
        '
        Me.CHILD_PRODUCT_ID.DataPropertyName = "CHILD_PRODUCT_ID"
        Me.CHILD_PRODUCT_ID.HeaderText = "CHILD_PRODUCT_ID"
        Me.CHILD_PRODUCT_ID.Name = "CHILD_PRODUCT_ID"
        Me.CHILD_PRODUCT_ID.ReadOnly = True
        Me.CHILD_PRODUCT_ID.Visible = False
        '
        'CHILD_PRODUCT_CODE
        '
        Me.CHILD_PRODUCT_CODE.DataPropertyName = "CHILD_PRODUCT_CODE"
        Me.CHILD_PRODUCT_CODE.HeaderText = "Child Code"
        Me.CHILD_PRODUCT_CODE.Name = "CHILD_PRODUCT_CODE"
        Me.CHILD_PRODUCT_CODE.ReadOnly = True
        Me.CHILD_PRODUCT_CODE.Width = 110
        '
        'CHILD_PRODUCT_NAME
        '
        Me.CHILD_PRODUCT_NAME.DataPropertyName = "CHILD_PRODUCT_NAME"
        Me.CHILD_PRODUCT_NAME.HeaderText = "Child Product Name"
        Me.CHILD_PRODUCT_NAME.Name = "CHILD_PRODUCT_NAME"
        Me.CHILD_PRODUCT_NAME.ReadOnly = True
        Me.CHILD_PRODUCT_NAME.Width = 140
        '
        'QUANTITY
        '
        Me.QUANTITY.DataPropertyName = "QUANTITY"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.QUANTITY.DefaultCellStyle = DataGridViewCellStyle2
        Me.QUANTITY.HeaderText = "Qty."
        Me.QUANTITY.Name = "QUANTITY"
        Me.QUANTITY.ReadOnly = True
        Me.QUANTITY.Width = 50
        '
        'lblProductName
        '
        Me.lblProductName.AutoSize = True
        Me.lblProductName.Font = New System.Drawing.Font("Lucida Bright", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProductName.Location = New System.Drawing.Point(33, 53)
        Me.lblProductName.Name = "lblProductName"
        Me.lblProductName.Size = New System.Drawing.Size(84, 14)
        Me.lblProductName.TabIndex = 2
        Me.lblProductName.Text = "Product Name"
        '
        'lblProductCode
        '
        Me.lblProductCode.AutoSize = True
        Me.lblProductCode.Font = New System.Drawing.Font("Lucida Bright", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProductCode.Location = New System.Drawing.Point(36, 24)
        Me.lblProductCode.Name = "lblProductCode"
        Me.lblProductCode.Size = New System.Drawing.Size(81, 14)
        Me.lblProductCode.TabIndex = 3
        Me.lblProductCode.Text = "Product Code"
        '
        'txtProductCode
        '
        Me.txtProductCode.Font = New System.Drawing.Font("Lucida Bright", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProductCode.Location = New System.Drawing.Point(123, 21)
        Me.txtProductCode.Name = "txtProductCode"
        Me.txtProductCode.Size = New System.Drawing.Size(145, 20)
        Me.txtProductCode.TabIndex = 4
        '
        'txtProductName
        '
        Me.txtProductName.Font = New System.Drawing.Font("Lucida Bright", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProductName.Location = New System.Drawing.Point(123, 50)
        Me.txtProductName.Name = "txtProductName"
        Me.txtProductName.Size = New System.Drawing.Size(181, 20)
        Me.txtProductName.TabIndex = 5
        '
        'lblNoOfGrouping
        '
        Me.lblNoOfGrouping.AutoSize = True
        Me.lblNoOfGrouping.Font = New System.Drawing.Font("Lucida Bright", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoOfGrouping.Location = New System.Drawing.Point(30, 363)
        Me.lblNoOfGrouping.Name = "lblNoOfGrouping"
        Me.lblNoOfGrouping.Size = New System.Drawing.Size(48, 15)
        Me.lblNoOfGrouping.TabIndex = 6
        Me.lblNoOfGrouping.Text = "Label1"
        '
        'cmdReport
        '
        Me.cmdReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdReport.Image = Global.DMI_RETAIL_INVENTORY.My.Resources.Resources.Printer_small
        Me.cmdReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdReport.Location = New System.Drawing.Point(505, 53)
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(90, 24)
        Me.cmdReport.TabIndex = 23
        Me.cmdReport.Text = "Print [F12]"
        Me.cmdReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.cmdReport.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblProductCode)
        Me.GroupBox1.Controls.Add(Me.cmdReport)
        Me.GroupBox1.Controls.Add(Me.txtProductCode)
        Me.GroupBox1.Controls.Add(Me.txtProductName)
        Me.GroupBox1.Controls.Add(Me.lblProductName)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 28)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(601, 83)
        Me.GroupBox1.TabIndex = 24
        Me.GroupBox1.TabStop = False
        '
        'frmListGroupingProduct
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(625, 396)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblNoOfGrouping)
        Me.Controls.Add(Me.dgvListGroupingProduct)
        Me.Controls.Add(Me.SP_LIST_GROUPING_PRODUCTBindingNavigator)
        Me.Font = New System.Drawing.Font("Lucida Bright", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmListGroupingProduct"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Tag = "List Grouping Product"
        Me.Text = "List Grouping Product"
        CType(Me.DS_GROUPING_PRODUCT, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_LIST_GROUPING_PRODUCTBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SP_LIST_GROUPING_PRODUCTBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator.ResumeLayout(False)
        Me.SP_LIST_GROUPING_PRODUCTBindingNavigator.PerformLayout()
        CType(Me.dgvListGroupingProduct, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DS_GROUPING_PRODUCT As DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCT
    Friend WithEvents SP_LIST_GROUPING_PRODUCTBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SP_LIST_GROUPING_PRODUCTTableAdapter As DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.SP_LIST_GROUPING_PRODUCTTableAdapter
    Friend WithEvents TableAdapterManager As DMI_RETAIL_INVENTORY.DS_GROUPING_PRODUCTTableAdapters.TableAdapterManager
    Friend WithEvents SP_LIST_GROUPING_PRODUCTBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents dgvListGroupingProduct As System.Windows.Forms.DataGridView
    Friend WithEvents lblProductName As System.Windows.Forms.Label
    Friend WithEvents lblProductCode As System.Windows.Forms.Label
    Friend WithEvents txtProductCode As System.Windows.Forms.TextBox
    Friend WithEvents txtProductName As System.Windows.Forms.TextBox
    Friend WithEvents lblNoOfGrouping As System.Windows.Forms.Label
    Friend WithEvents PARENT_PRODUCT_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PARENT_PRODUCT_CODE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PARENT_PRODUCT_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CHILD_PRODUCT_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CHILD_PRODUCT_CODE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CHILD_PRODUCT_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QUANTITY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdReport As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
End Class
