﻿Public Class frmSplitProduct

    Dim tmpPosition As String
    Dim tmpCategory As Integer
    Dim tmpChange As Boolean
    Public Sub OpenForm(ByVal pUserType As String, ByVal pUserid As String, ByRef ptmpForm As Form, ByVal plang As String)
        mdlGeneral.User_Type = pUserType
        mdlGeneral.Language = plang
        mdlGeneral.USER_ID = pUserid
        ptmpForm.BringToFront()
        ptmpForm.Show()
    End Sub
    Private Sub frmSplitProduct_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If tmpChange = True Then
            If Language = "Indonesian" Then
                tmpvar = MsgBox("Apakah Anda ingin menyimpan perubahan " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            Else
                tmpvar = MsgBox("Do You Want To Save The Changes " + Me.Text + " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel, "DMI Retail")
            End If
            If tmpvar = 6 Then
                cmdSave_Click(Nothing, Nothing)
            ElseIf tmpvar = 2 Then
                e.Cancel = True
            End If
        End If
    End Sub


    Private Sub frmSplitProduct_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DS_GROUPING_PRODUCT.SP_LIST_PRODUCT_GOODS' table. You can move, or remove it, as needed.
        Me.SP_LIST_PRODUCT_GOODSTableAdapter.Fill(Me.DS_GROUPING_PRODUCT.SP_LIST_PRODUCT_GOODS, Now)
        'TODO: This line of code loads data into the 'DS_GROUPING_PRODUCT.WAREHOUSE' table. You can move, or remove it, as needed.
        Me.WAREHOUSETableAdapter.Fill(Me.DS_GROUPING_PRODUCT.WAREHOUSE)
        'TODO: This line of code loads data into the 'DS_GROUPING_PRODUCT.PRODUCT' table. You can move, or remove it, as needed.
        Me.PRODUCTTableAdapter.Fill(Me.DS_GROUPING_PRODUCT.PRODUCT)


        If Language = "Indonesian" Then

            lblParent.Text = "Produk Induk"
            lblFrom.Text = "Dari"
            lblSplited.Text = "Jumlah pemecahan produk"
            lblTo.Text = "Ke"

            cmdAdd.Text = "Tambah"
            cmdEdit.Text = "Ubah"
            cmdSave.Text = "Simpan"
            cmdUndo.Text = "Batal"
            cmdDelete.Text = "Hapus"

            dgvSplitProduct.Columns("PRODUCT_CODE").HeaderText = "Kode Produk"
            dgvSplitProduct.Columns("PRODUCT_NAME").HeaderText = "Nama Produk"
            dgvSplitProduct.Columns("QUANTITY").HeaderText = "Jumlah"

            Me.Text = "Input Pemecahan Produk"
        End If

        Call clear()

        tmpChange = False
    End Sub


    Private Sub cmdSearchParent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchParent.Click


        mdlGeneral.tmpSearchMode = "PRODUCT LIST - Product Name"
        frmSearchProduct.ShowDialog(Me)
        If tmpSearchResult = "" Then Exit Sub

        SP_LIST_PRODUCT_GOODSBindingSource.Position = SP_LIST_PRODUCT_GOODSBindingSource.Find("PRODUCT_ID", tmpSearchResult)
        Dim tmpSplit As Boolean = False
        tmpSplit = SP_SPLIT_PRODUCTTableAdapter.Fill(DS_GROUPING_PRODUCT.SP_SPLIT_PRODUCT, SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID"))
        If tmpSplit = True Then
            SP_LIST_PRODUCT_GOODSBindingSource.Position = SP_LIST_PRODUCT_GOODSBindingSource.Find("PRODUCT_ID", tmpSearchResult)
            PRODUCT_CODEComboBox.Text = SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_CODE")
            SP_SPLIT_PRODUCTTableAdapter.Fill(DS_GROUPING_PRODUCT.SP_SPLIT_PRODUCT, SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID"))
        Else
            If Language = "Indonesian" Then
                MsgBox("Produk ini tidak mempunyai produk turunan" & vbCrLf & "Silahkan pilih produk lain", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("This Product has not been children product" & vbCrLf & "Please select other product", MsgBoxStyle.Critical, "DMI Retail")
            End If
            PRODUCT_CODEComboBox.Text = ""
            PRODUCT_NAMETextBox.Text = ""
            Exit Sub
        End If

        tmpChange = False
    End Sub

    Private Sub PRODUCT_CODEComboBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles PRODUCT_CODEComboBox.LostFocus
        If PRODUCT_CODEComboBox.Text = "" Then
            Exit Sub
        End If

        SP_LIST_PRODUCT_GOODSTableAdapter.SP_GET_CATEGORY_PRODUCT(PRODUCT_CODEComboBox.Text, tmpCategory)

        If tmpCategory <> 1 Then
            If Language = "Indonesian" Then
                MsgBox("Produk yang anda input bukan barang !" & vbCrLf & _
                   "Gunakan Form Pencarian untuk mencari produk yang tepat !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("You input Product which is not Goods !" & vbCrLf & _
                   "Use the Search Form to find the right Product !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            PRODUCT_NAMETextBox.Text = ""
            PRODUCT_CODEComboBox.Text = ""

        End If

    End Sub

    Private Sub PRODUCT_CODEComboBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PRODUCT_CODEComboBox.TextChanged

        PRODUCTTableAdapter.SP_GET_PRODUCT_NAME(PRODUCT_CODEComboBox.Text, PRODUCT_NAMETextBox.Text)
        If PRODUCT_NAMETextBox.Text <> "" Then
            SP_LIST_PRODUCT_GOODSBindingSource.Position = SP_LIST_PRODUCT_GOODSBindingSource.Find("PRODUCT_CODE", PRODUCT_CODEComboBox.Text)
            SP_SPLIT_PRODUCTTableAdapter.Fill(DS_GROUPING_PRODUCT.SP_SPLIT_PRODUCT, SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID"))
        End If

        If PRODUCT_CODEComboBox.Text <> "" Then
            Dim tmpSplit As Boolean = False
            tmpSplit = SP_SPLIT_PRODUCTTableAdapter.Fill(DS_GROUPING_PRODUCT.SP_SPLIT_PRODUCT, SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID"))
            If tmpSplit = False Then
                If Language = "Indonesian" Then
                    MsgBox("Produk ini tidak mempunyai produk turunan" & vbCrLf & "Silahkan pilih produk lain", MsgBoxStyle.Critical, "DMI Retail")
                Else
                    MsgBox("This Product has not been children product" & vbCrLf & "Please select other product", MsgBoxStyle.Critical, "DMI Retail")
                End If
                PRODUCT_CODEComboBox.Text = ""
                PRODUCT_NAMETextBox.Text = ""
                Exit Sub
            End If
        End If

        tmpChange = True
    End Sub


    Private Sub txtQuantity_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtQuantity.KeyPress
        If Asc(e.KeyChar) = 8 Then Exit Sub
        If Asc(e.KeyChar) < Asc("0") Or Asc(e.KeyChar) > Asc("9") Then
            e.KeyChar = ""
        End If

        tmpChange = True
    End Sub

    Sub clear()
        Me.SP_SPLIT_PRODUCTTableAdapter.Fill(DS_GROUPING_PRODUCT.SP_SPLIT_PRODUCT, 0)
        PRODUCT_NAMETextBox.Text = ""
        PRODUCT_CODEComboBox.Text = ""
        WAREHOUSE_NAMEComboBox.Text = ""
        WAREHOUSE_NAMEComboBox1.Text = ""
        txtQuantity.Text = ""
    End Sub

    Private Sub WAREHOUSEBindingSource_CurrentChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles WAREHOUSEBindingSource.CurrentChanged
        If WAREHOUSE_NAMEComboBox.Text <> "" Then
            If WAREHOUSEBindingSource.Current("WAREHOUSE_ID") = -1 Then
                If Language = "Indonesian" Then
                    MsgBox("Silahkan pilih gudang lain ." & vbCrLf & _
                   "Ini adalah Gudang barang rusak !", MsgBoxStyle.Critical, "DMI Retail")

                Else
                    MsgBox("Please enter other warehouse ." & vbCrLf & _
                        "This Warehouse is Rummage !", MsgBoxStyle.Critical, "DMI Retail")
                End If
                WAREHOUSEBindingSource.Position = 0
                Exit Sub
            End If
        End If
    End Sub

    Private Sub WAREHOUSE1BindingSource1_CurrentChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles WAREHOUSE1BindingSource1.CurrentChanged
        If WAREHOUSE_NAMEComboBox1.Text <> "" Then
            If WAREHOUSEBindingSource.Current("WAREHOUSE_ID") = -1 Then
                If Language = "Indonesian" Then
                    MsgBox("Silahkan pilih gudang lain ." & vbCrLf & _
                   "Ini adalah Gudang barang rusak !", MsgBoxStyle.Critical, "DMI Retail")

                Else
                    MsgBox("Please enter other warehouse ." & vbCrLf & _
                        "This Warehouse is Rummage !", MsgBoxStyle.Critical, "DMI Retail")
                End If
                WAREHOUSEBindingSource.Position = 0
                Exit Sub
            End If
        End If
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Dim tmpProductId, tmpProductId1 As Integer
        Dim tmpWarehouseID As Integer
        Dim tmpWarehouse As Integer

        'PRODUCTBindingSource.Position = PRODUCTBindingSource.Find("PRODUCT_ID", PRODUCT_CODEComboBox.Text)
        PRODUCTTableAdapter.SP_GET_PRODUCT_ID(SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_CODE"), tmpProductId)

        If Not ValidateAllComboBox(Me) Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        Else
            tmpWarehouseID = IIf(WAREHOUSE_NAMEComboBox.Text = "", 0, WAREHOUSEBindingSource.Current("WAREHOUSE_ID"))
        End If



        SP_PRODUCT_WAREHOUSE_DETAILTableAdapter.Fill(Me.DS_GROUPING_PRODUCT.SP_PRODUCT_WAREHOUSE_DETAIL, _
                                                  tmpProductId)
        SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Filter = "WAREHOUSE_ID = " & tmpWarehouseID


        If PRODUCT_NAMETextBox.Text = "" Or PRODUCT_CODEComboBox.Text = "" Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If txtQuantity.Text = "" Or Val(txtQuantity.Text) = 0 Then
            If Language = "Indonesian" Then
                MsgBox("Silahkan mengisi semua kolom yang harus di isi !", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Please enter all required fields !", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Count < 1 Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menyimpan transaksi ini!" & vbCrLf & PRODUCT_NAMETextBox.Text & " tidak mempunya saldo di " & _
                WAREHOUSE_NAMEComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot save this transaction!" & vbCrLf & PRODUCT_NAMETextBox.Text & " has no balance in " & _
                WAREHOUSE_NAMEComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If

        If SP_PRODUCT_WAREHOUSE_DETAILBindingSource.Current("QUANTITY") < CInt(txtQuantity.Text) Then
            If Language = "Indonesian" Then
                MsgBox("Tidak dapat menyimpan transaksi ini!" & vbCrLf & PRODUCT_NAMETextBox.Text & " tidak mempunya saldo di " & _
                WAREHOUSE_NAMEComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
            Else
                MsgBox("Cannot save this transaction!" & vbCrLf & PRODUCT_NAMETextBox.Text & " has no balance in " & _
                WAREHOUSE_NAMEComboBox.Text & "!", MsgBoxStyle.Critical, "DMI Retail")
            End If
            Exit Sub
        End If


        If WAREHOUSE_NAMEComboBox1.Text = "" Then
            tmpWarehouse = 0
        Else
            tmpWarehouse = WAREHOUSE1BindingSource1.Current("WAREHOUSE_ID")
        End If

        PRODUCTTableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                               0, _
                                               SP_LIST_PRODUCT_GOODSBindingSource.Current("PRODUCT_ID"), _
                                               WAREHOUSEBindingSource.Current("WAREHOUSE_ID"), _
                                               CInt(txtQuantity.Text) * (-1), _
                                               USER_ID, _
                                               Now, _
                                               0, _
                                               DateSerial(4000, 12, 31))
        For X As Integer = 0 To dgvSplitProduct.RowCount - 1
            ''INSERT OR UPDATE QUANTITY TO WAREHOUSE SELECTED
            PRODUCTTableAdapter.SP_GET_PRODUCT_ID(dgvSplitProduct.Item(0, X).Value, tmpProductId1)
            Dim tmpQuantity As Integer = (dgvSplitProduct.Item("QUANTITY", X).Value * Val(txtQuantity.Text))
            PRODUCTTableAdapter.SP_PRODUCT_WAREHOUSE("I", _
                                                 0, _
                                                 tmpProductId1, _
                                                 tmpWarehouse, _
                                                 tmpQuantity, _
                                                 USER_ID, _
                                                 Now, _
                                                 0, _
                                                 DateSerial(4000, 12, 31))

            ''INSERT TABLE PODUCT_SPLIT
            PRODUCTTableAdapter.SP_SPLIT_PROCESS("I", 0, _
                                                   Now, _
                                                   tmpProductId, _
                                                   tmpProductId1, _
                                                   txtQuantity.Text, _
                                                   tmpQuantity, _
                                                   WAREHOUSEBindingSource.Current("WAREHOUSE_ID"), _
                                                   tmpWarehouse, _
                                                   "MANUAL", _
                                                   USER_ID, _
                                                   Now, _
                                                   0, _
                                                   DateSerial(4000, 12, 31))

        Next
        If Language = "Indonesian" Then
            MsgBox("Data Berhasil disimpan.", MsgBoxStyle.Information, "DMI Retail")
        Else
            MsgBox("Data successfully saved.", MsgBoxStyle.Information, "DMI Retail")
        End If
        Call clear()

        tmpChange = False
    End Sub

    Private Sub cmdUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUndo.Click
        Me.Close()
    End Sub
End Class